# Description of All Tests Available

Several tests are implemented in order to verify that the whole
generator is working. All the tests can be launched with the command:

```bash
make tests
```

But, they also can be launched individually.

## Non Regression Tests

```bash
make rtests
```

A number of C files generated have been saved in the folder
`tests/regression-tests`. This test verifies that the new generator built
produces the same result as previously.

## Behaviour Tests

```bash
make btests
```

These tests generate flight plans with the new and the old generator. Then,
the C code generated is compilated and simulated. The goal of these tests
is to ensure that the two flight plans behave similarly.

## New features Test

```bash
make nftests
```

Ensures that all the new features added are correctly working.

## Build Tests

```bash
make bdtests
```

Test to build all the paparazzi flight plans available. The file
`test/build-tests/non-working-fp.txt` contains the list of all the files
that did not work also with the old generator. If the generator behaves
like the old one, there are no errors displayed.

## Error Message Tests

```bash
make etests
```

These tests launch the generator with incorrect files and verify that it
display correct messages.

## C Light Tests

```bash
make ctests
```

Use `clightgen` on the files in `common-c-code` and verify that the result
produced is the same as the file in the folder `generated`. As the file
`CommonFP.v` has been modified for the verification of the generator. A
patch is applied on the newly generated file before the execution of the
`diff` command.
