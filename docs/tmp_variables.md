# Temprorary variables added during the generation

- `tmp_nav_block`: temporary variable to get the value of the block index.
- `tmp_nav_stage`: temporary variable to get the value of the stage index.
- `tmp_cond`: needed to compute the `&&` for the exception
- `tmp_RadOfDeg`: needed for the call of the function `RadOfDeg`
- `tmp_AltitudeMode`: needed for the call of the altitude functions
- `tmp_NavCond`: needed for the computation of the function `NavApproachingFrom`
