# Description of the Build Process

## Preliminary

The script `configure` realize several steps:

- It clones the [Paparazzi](https://github.com/paparazzi/paparazzi) project
and rename the module `env` into `PPRZEnd to be compatible with CompCert.
- It clones [CompCert](https://github.com/AbsInt/CompCert), configure it
  (set to install coqdev and clightgen), apply a patch to support recent
  version of Coq and then built it.

## Build Process

- All the CompCert extracted OCaml files are regrouped into the `extracted`
folder.
- Add OCaml configuration file in the `extracted` folder. CompCert will not
require compcert.ini file to work.
- CoqMakefile then build and verified all the Coq files and extract the
  OCaml code.
- Finally, `ocamlbuild` is used to compile the entire project.
