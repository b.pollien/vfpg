# Modifications of the current generator

The Ocaml code of the previous Flight Plan generator of
Paparazzi have been extracted in order to run a behavior test. This test
consists to run in parallel flight plans that contain functions that print
some state information (number of steps, stage executed, current block
id...). The test then verifies that the execution of the code generated
with the old and new generators produces the same event trace. As the new
generator implements new features, the two generators do not behave
similarly. In order to have the same trace, the previous generator has been
slightly modified with the modifications described below:

* An initialisation block has been added. This block is used in the new
  generator to execute the `on_enter` code of the first block defined by
  the user. This block is added in the previous generator in order to have
  the same numbering.
* When there is a `deroute`, the following stage is stored as the last
  stage. In the case of a `return`, we then jump to the next stage instead
  of the same `deroute` stage.
