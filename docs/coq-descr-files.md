# Coq Files Description

The folder `src` contains all the Coq sources of the generator and the
proof. The sources are divided into different subfolders:

- `common`: Contains common lemmas mainly about lists.
  - `CommonLemmas.v`: lemmas about Coq list.
  - `CommonStringLemmas.v`: lemmas about Coq String and conversion
  functions.
  - `CommonLemmasNat8.v`: lemmas about nat representable on 8 bits.
  - `CommonSSRLemmas.v`: lemmas about SSR seq and ssrnat.

- `syntax`: Contains the definitions of basic types and the flight
  plan.
  - `BasicsTypes.v`: All basic type definitions for the flight plan.
  - `FPNavigationMode.v`: Definitions of the flight plan navigation modes.
  - `FlightPlanGeneric.v`: Common definitions for the flight plan.
  - `FlightPlan.v`: Definition of the Flight Plan Coq structures.
  - `FlightPlanExtended.v`: Definition of the structure for the flight
    plan extended.
  - `FlightPlanSized.v`: Definition of dependent type for a well-sized
    flight plan.

- `semantics`: Contains the definitions of the semantics used for the
  verification.
  - `FPEnvironmentGeneric.v`: Generic definition for the FP Environments
    that the semantics will use.
  - `FPEnvironment.v`: Definition of the FP Environment.
  - `FPEnvironmentExtended.v`: Definition of the FPE Environment.
  - `FPEnvironmentSized.v`: Definition of the FPS Environment and the
    matching relation between FPE and FPS environments.
  - `FPEnvironmentClight.v`: Definition of the Clight Environment.
  - `FPEnvironmentDef.v`: Module type that regroups the specialisation of
    all environment modules (expect FPE that is specialized in FPS env).
  - `FPNavigationModeSem.v`: Semantics of the navigation mode.
  - `FPBigStepGeneric.v`: Generic definitions of semantics and
    bisimulation. This file also contains common definitions for the
    Flight plan semantics of FP and FPE.
  - `FPBigStep.v`: BigStep semantics of the flight plan.
  - `FPBigStepExtended.v`: BigStep semantics of the extended flight plan.
  - `FPBigStepSized.v`: BigStep semantics for the sized flight plan.
  - `FPBigStepClight.v`: Definition of the Clight step in the flight plan
    context.
  - `FPBigStepDef.v`: Module type that regroups the specialisation of
    all big step definitions.

- `generator`: Contains all the functions of the generators.
  - `TmpVariables.v`: Definition of all the temporary variables used in the
  Clight code generated.
  - `ClightGeneration.v`: Basics function for the generation of Clight
    code. Need the parameter functions `create_ident` and `arbitrary_ident`
    that generates a unique identifier for any string. This file defines
    axioms that these functions are injective. The file requires also
    `string_of_ident` parameter function that translates an ident into a
    string. This function is used to generate error messages.
  - `FBDerouteAnalysis.v`: Contain a function that generates warnings for
     wrong usage of the forbidden deroutes.
  - `FPExtended.v`: Definition of the function that converts the flight
    plan into the extended version.
  - `FPSizeVerification.v`: Functions that generate errors if they detect
    that the blocks are badly numbered or if there are more than 255
    blocks or stages.
  - `FPNavigationModeGen.v`: Functions for the generation of the code of
     navigation functions.
  - `GvarsVerification.v`: Declaration and verification that global
    variables defined by the preprocessing do not overlap with the
    reserved variables of the flight plan.
  - `Generator.v`: Functions for the generation of the Clight code from a
    flight plan.
  - `AuxFunctions.v`: Generation of the auxiliary functions such as
    `pre_call_block`, `post_call_block` and `forbidden_deroute`.

- `verification`: Contains the proof for the verification of the
  semantics preservation and other generator properties.
  - `MatchFPwFPE.v`: Definition of matching relation between FP and
    FPE environment.
  - `MatchFPSwFPC.v`: Definition of the matching relation between FPS
    and Clight environment.
  - `ClightLemmas.v`: Lemmas about Clight properties.
  - `GeneratorProperties.v`: Properties about the generator needed for the
    verification.
  - `FPSProp.v`: Properties about the generated code, proven on the FPS
    semantics.
  - `CommonFPDefinition.v`: Contains definitions of functions and lemmas
    referring to the CommonFP file generated from the common C code.
  - `CommonFPSimplified.v`: Contains a simplified version of CommonFP (all
    the built-in functions are removed).
  - `ExtractTraceGeneric.v`: Lemmas and properties about the extraction of
    trace in a generic environment.
  - `ExtractTraceFPEnv.v`: Lemmas and properties about the extraction of
    trace generated in the `fp_env`.
  - `CommonFPVerification.v`: Contains the lemmas that execute the Common C
    function using the Clight semantics.
  - `FPNavigationModeVerification.v`: Verification of the semantics
    preservation for the navigation mode.
  - `VerifFPToFPE.v`: File containing the bisimulation proof between
    FP and FPE semantics.
  - `VerifFPEToFPS.v`: File containing the semantics bisimulation
    proof between FPE and FPS semantics.
  - `VerifFPSToFPC.v`: File containing the bisimulation proof between
    FPS and Clight semantics.
  - `VerifFPToFPC.v`: File regrouping all the proof to verify the general
    bisimulation theorem between FP and Clight semantics.

- `parser`: Contains the files used to generate the XML to FPP parser used in the frontend of the VFPG.
  - `FlightPlanParsed.v`: Definition of the Flight Plan Parsed coq
  structure that will store the result of the parsing.
  - `FPPUtils.v`: Definitions of functions used to manipulate FPP.
  - `CoqLexer.v`: Definition of the lexer used before the parser.
  - `Parser.vy`: Grammar specification of a flight plan stored as a
  XML file. It is given to Menhir to generate a verified parser in coq.
  - `Importer.v`: Definition of the general parser function, taking a 
  string and generating a FPP (or a procedure structure)

Finally, the file `extraction.v` is the configuration file for the
extraction of the Coq to Caml code.
