#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "The number of parameters is incorrect !"
    echo "Expected the executable and the folder FP to test"
    echo "and the list of the non working files."
    exit 1
fi

HOME=`pwd`
BIN=$1
TESTS_FOLDER=$2
NONE_WORKING=$HOME/$3
OUT=$HOME/out/build-tests
TMP_NEW=$OUT/tmp_build_test_new.txt
TMP_OLD=$OUT/tmp_build_test_old.txt
TMP_GREP=$OUT/tmp_grep.txt


# Building the old generator
OLD_GEN=$HOME/ocaml-generator
cd $OLD_GEN
dune build
cd $HOME

echo "Running the build tests"

mkdir -p $OUT
RES=0

for FILE in $(find $TESTS_FOLDER -name *.xml)
do
    # Verify if the file is not in the none working files list
    grep $FILE $NONE_WORKING > $TMP_GREP
    WORKING=$?

    CORRECT=${FILE%.*}.h
    NEW=$OUT/${CORRECT##*/}
    touch $TMP_NEW
    ./$BIN $FILE $NEW >> $TMP_NEW 2>> $TMP_NEW

    if [ $? -ne 0 ] || [ ! -e $NEW ]; then
        # Test with the old generator
        # Generating flight plan with the old generator
        TMP_NAME=${FILE%.*}.old.h
        OLD=$OUT/${TMP_NAME##*/}
        cd $OLD_GEN
        touch $TMP_OLD
        ./run.sh $HOME/$FILE $OLD >> $TMP_OLD 2>> $TMP_OLD

        if [ $? -ne 0 ] || [ ! -e $OLD ]; then
            cd $HOME

            # Check if the file is not in the none working files list
            if [ $WORKING -ne 0 ]; then
                echo "Error: New and old generators cannot generate flight plan for $FILE"
                echo "This file is not in the none working list ($NONE_WORKING)"
                cat $TMP_NEW
                echo "****"
                cat $TMP_OLD
                echo "--------------------------------------------------------"
                RES=1
            fi
            rm $TMP $TMP_OLD $TMP_GREP
            continue
        else
            cd $HOME
            echo "-- Error during the generation with the new generator --"
            echo $FILE
            cat $TMP_NEW
            echo "--------------------------------------------------------"
            RES=1
            rm $TMP $TMP_OLD
            continue
        fi
    fi

    if [ $WORKING -eq 0 ]; then
        echo "WARNING: None working file $FILE is now working !"
    fi

    rm $TMP_NEW $TMP_GREP
done

if [ $RES -eq 0 ]; then
    echo "OK"
else
    echo "FAILED"
fi
