#ifndef STD_H
#define STD_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#define FALSE 0
#define TRUE 1
#define CARROT 0
#define WE 1
#define NS 2

#define INTENTIONAL_FALLTHRU

extern uint32_t last_wp;
extern float ground_alt;



/* Definition of all nav functions needed for the new features tests */

void nav_init_stage();
bool nav_cond(uint32_t i);
void nav_fun(uint32_t i);

bool call_five_time();
extern uint8_t value_set;
void set(uint8_t i);
void test_set(uint8_t i);

void nav_home();
float RadOfDeg(float val);
float Height(float val);
void NavSegment(uint8_t val1, uint8_t val2);
void NavAttitude(float val);
void NavVerticalAutoThrottleMode(float val);
void NavVerticalAltitudeMode(float val1, float val2);
bool NavApproaching(uint8_t val1, uint8_t val2);
void NavVerticalAutoPitchMode(float val);


#endif