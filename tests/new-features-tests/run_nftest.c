#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include "common_flight_plan.h"


void print_state(int32_t step) {
    printf("(stp:%i) (b:%i, s:%i, lb:%i, ls: %i):\n", step, \
                get_nav_block(), \
                get_nav_stage(),get_last_block(), get_last_stage());
    printf("\tlast_wp ->%i, stage_time -> %i, block_time -> %i\n", \
                                        last_wp, stage_time, block_time);
    assert(nav_block == get_nav_block());
    assert(last_block == get_last_block());
    assert(nav_stage == get_nav_stage());
    assert(last_stage == get_last_stage());
    assert(1 != 2);
}

void run_test(uint32_t nb_step){
    for(uint32_t step = 0; step < nb_step; step++){
        print_state(step);
        auto_nav();
        printf("\n");
    }
}

int main(int argc, char *argv[]) {
    run_test(100);

    return 0;
}