#include "std.h"
#include <stdlib.h>

uint32_t last_wp = 0;
float ground_alt = 0.0;

/* Definition of all nav functions needed for the behaviour tests */

void nav_init_stage() {
}

// Function that need to be call 5 time before returning true
bool call_five_time() {
    static int i = 0;
    if (i == 5) {
        return TRUE;
    }
    else{
        i++;
        return FALSE;
    }
}

uint8_t value_set = 0;

void set(uint8_t i) {
    printf("Set %i\n", i);
    value_set = i;
}

void test_set(uint8_t i) {
    printf("Test set %i: value is %i\n", i, value_set);
    if (value_set != i) {
        exit(1);
    }
}

// /* Generates a random nav condition */
bool nav_cond(uint32_t i) {
    return TRUE;
}

// /* Generates a random nav value */
float nav_value(uint32_t i) {
    float res = rand();
    return res;
}

// /* Generates call */
void nav_fun(uint32_t i) {
}

void nav_home() {
}


float RadOfDeg(float val) {
    float res = rand();
    return res;
}

float Height(float val) {
    float res = rand();
    return res;
}

void NavSegment(uint8_t val1, uint8_t val2) {
}

void NavAttitude(float val) {
}

void NavVerticalAutoThrottleMode(float val) {
}


void NavVerticalAltitudeMode(float val1, float val2) {
}

bool NavApproaching(uint8_t val1, uint8_t val2) {
    bool res = nav_cond(val1 + val2);
    return res;
}


void NavVerticalAutoPitchMode(float val) {
}
