/* This file has been generated by the VFPG from tests/regression-tests/while.xml */
/* Version v6.0_unstable-none--dirty */
/* Please DO NOT EDIT */

#ifndef FLIGHT_PLAN_H
#define FLIGHT_PLAN_H

#include "std.h"
#include "generated/modules.h"
#include "modules/core/abi.h"
#include "autopilot.h"


#define FLIGHT_PLAN_NAME "Small1"
#define NAV_DEFAULT_ALT 260 /* nominal altitude of the flight plan */
#define NAV_UTM_EAST0 360285
#define NAV_UTM_NORTH0 4813595
#define NAV_UTM_ZONE0 31
#define NAV_LAT0 434622300 /* 1e7deg */
#define NAV_LON0 12728900 /* 1e7deg */
#define NAV_ALT0 185000 /* mm above msl */
#define NAV_MSL0 51850 /* mm, EGM96 geoid-height (msl) over ellipsoid */
#define QFU 1.0
#define WP_dummy 0
#define WP_HOME 1
#define WAYPOINTS_UTM { \
 {0.0, 0.0, 260},\
 {0.0, 0.0, 260},\
};
#define WAYPOINTS_ENU { \
 {0.00, -0.00, 75.00}, /* ENU in meters  */ \
 {0.00, -0.00, 75.00}, /* ENU in meters  */ \
};
#define WAYPOINTS_LLA { \
 {.lat=434622299, .lon=12728900, .alt=260000}, /* 1e7deg, 1e7deg, mm (above NAV_MSL0, local msl=51.85m) */ \
 {.lat=434622299, .lon=12728900, .alt=260000}, /* 1e7deg, 1e7deg, mm (above NAV_MSL0, local msl=51.85m) */ \
};
#define WAYPOINTS_LLA_WGS84 { \
 {.lat=434622299, .lon=12728900, .alt=311850}, /* 1e7deg, 1e7deg, mm (above WGS84 ref ellipsoid) */ \
 {.lat=434622299, .lon=12728900, .alt=311850}, /* 1e7deg, 1e7deg, mm (above WGS84 ref ellipsoid) */ \
};
#define WAYPOINTS_GLOBAL { \
 FALSE, \
 FALSE, \
};
#define NB_WAYPOINT 2
#define FP_BLOCKS { \
 "INIT_BLOCK" , \
 "Wait GPS" , \
 "Fly" , \
 "HOME" , \
}
#define NB_BLOCKS 4
#define GROUND_ALT 185.
#define GROUND_ALT_CM 18500
#define SECURITY_HEIGHT 25.
#define SECURITY_ALT 210.
#define HOME_MODE_HEIGHT 25.
#define MAX_DIST_FROM_HOME 1500.


#ifndef FBW


#endif

#ifdef NAV_C


static inline void auto_nav_init(void) {
}

int _var_i_1;

int _var_i_1_to;

unsigned char const nb_blocks = 4;

void on_enter_block(unsigned char block)
{
  switch (block) {
    case 1:
      (pre_call_1());
      break;
    default:
      break;
    
  }
}

void on_exit_block(unsigned char block)
{
  switch (block) {
    case 1:
      (post_call_1());
      break;
    default:
      break;
    
  }
}

_Bool forbidden_deroute(unsigned char from, unsigned char to)
{
  switch (from) {
    default:
      break;
    
  }
  return (_Bool) 0;
}

static inline void auto_nav(void)
{
  unsigned char tmp_nav_block;
  unsigned char tmp_nav_stage;
  _Bool tmp_cond;
  float tmp_RadOfDeg;
  float tmp_AltitudeMode;
  _Bool tmp_NavCond;
  tmp_nav_block = get_nav_block();
  switch (tmp_nav_block) {
    case 0:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        default:
          set_nav_stage(0);
          NextBlock();
          break;
        
      }
      break;
    case 1:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          while_1_0:
          set_nav_stage(0);
          if (!((a==1))) {
            goto endwhile_1_8;
          } else {
            NextStage();
            break;
          }
        case 1:
          set_nav_stage(1);
          test = 1;
          NextStage();
        case 2:
          while_1_2:
          set_nav_stage(2);
          if (!((b==2))) {
            goto endwhile_1_7;
          } else {
            NextStage();
            break;
          }
        case 3:
          set_nav_stage(3);
          test = 2;
          NextStage();
        case 4:
          while_1_4:
          set_nav_stage(4);
          if (!((c==3))) {
            goto endwhile_1_6;
          } else {
            NextStage();
            break;
          }
        case 5:
          set_nav_stage(5);
          test = 3;
          NextStage();
        case 6:
          set_nav_stage(6);
          goto while_1_4;
          endwhile_1_6:
          set_nav_stage(7);
        case 7:
          set_nav_stage(7);
          goto while_1_2;
          endwhile_1_7:
          set_nav_stage(8);
        case 8:
          set_nav_stage(8);
          goto while_1_0;
          endwhile_1_8:
          set_nav_stage(9);
        default:
          set_nav_stage(9);
          NextBlock();
          break;
        
      }
      break;
    case 2:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          if (!(fly())) {
            NextStage();
          } else {
            break;
          }
        case 1:
          while_2_1:
          set_nav_stage(1);
          if (!((a==1))) {
            goto endwhile_2_4;
          } else {
            NextStage();
            break;
          }
        case 2:
          set_nav_stage(2);
          NextStage();
          nav_goto_block(2);
          break;
        case 3:
          set_nav_stage(3);
          test = 30;
          NextStage();
        case 4:
          set_nav_stage(4);
          goto while_2_1;
          endwhile_2_4:
          set_nav_stage(5);
        case 5:
          set_nav_stage(5);
          _var_i_1 = 1;
          NextStage();
        case 6:
          set_nav_stage(6);
          _var_i_1_to = 5;
          NextStage();
        case 7:
          while_2_7:
          set_nav_stage(7);
          if (!(_var_i_1 <= _var_i_1_to)) {
            goto endwhile_2_10;
          } else {
            NextStage();
            break;
          }
        case 8:
          set_nav_stage(8);
          tmp_RadOfDeg = RadOfDeg(0.000000);
          NavVerticalAutoThrottleMode(tmp_RadOfDeg);
          NavVerticalAltitudeMode((ground_alt+(50*_var_i_1)), 0);
          NavCircleWaypoint(1, 75);
          if ((stage_time>10)) {
            NextStage();
            break;
          }
          break;
        case 9:
          set_nav_stage(9);
          _var_i_1 = _var_i_1 + 1;
          NextStage();
        case 10:
          set_nav_stage(10);
          goto while_2_7;
          endwhile_2_10:
          set_nav_stage(11);
        default:
          set_nav_stage(11);
          NextBlock();
          break;
        
      }
      (post_call_2());
      break;
    default:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          nav_home();
          break;
        default:
          set_nav_stage(1);
          NextBlock();
          break;
        
      }
      break;
    
  }
}


#endif // NAV_C

#endif // FLIGHT_PLAN_H
