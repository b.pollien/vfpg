/* This file has been generated by the VFPG from tests/regression-tests/on_exit_enter.xml */
/* Version v6.0_unstable-none--dirty */
/* Please DO NOT EDIT */

#ifndef FLIGHT_PLAN_H
#define FLIGHT_PLAN_H

#include "std.h"
#include "generated/modules.h"
#include "modules/core/abi.h"
#include "autopilot.h"


#define FLIGHT_PLAN_NAME "Small1"
#define NAV_DEFAULT_ALT 260 /* nominal altitude of the flight plan */
#define NAV_UTM_EAST0 360285
#define NAV_UTM_NORTH0 4813595
#define NAV_UTM_ZONE0 31
#define NAV_LAT0 434622300 /* 1e7deg */
#define NAV_LON0 12728900 /* 1e7deg */
#define NAV_ALT0 185000 /* mm above msl */
#define NAV_MSL0 51850 /* mm, EGM96 geoid-height (msl) over ellipsoid */
#define QFU 1.0
#define WP_dummy 0
#define WP_HOME 1
#define WP_P1 2
#define WP_P2 3
#define WAYPOINTS_UTM { \
 {0.0, 0.0, 260},\
 {0.0, 0.0, 260},\
 {0.0, 0.0, 260},\
 {100.0, 100.0, 260},\
};
#define WAYPOINTS_ENU { \
 {0.00, -0.00, 75.00}, /* ENU in meters  */ \
 {0.00, -0.00, 75.00}, /* ENU in meters  */ \
 {0.00, -0.00, 75.00}, /* ENU in meters  */ \
 {97.92, 102.07, 75.00}, /* ENU in meters  */ \
};
#define WAYPOINTS_LLA { \
 {.lat=434622299, .lon=12728900, .alt=260000}, /* 1e7deg, 1e7deg, mm (above NAV_MSL0, local msl=51.85m) */ \
 {.lat=434622299, .lon=12728900, .alt=260000}, /* 1e7deg, 1e7deg, mm (above NAV_MSL0, local msl=51.85m) */ \
 {.lat=434622299, .lon=12728900, .alt=260000}, /* 1e7deg, 1e7deg, mm (above NAV_MSL0, local msl=51.85m) */ \
 {.lat=434631486, .lon=12741000, .alt=260000}, /* 1e7deg, 1e7deg, mm (above NAV_MSL0, local msl=51.85m) */ \
};
#define WAYPOINTS_LLA_WGS84 { \
 {.lat=434622299, .lon=12728900, .alt=311850}, /* 1e7deg, 1e7deg, mm (above WGS84 ref ellipsoid) */ \
 {.lat=434622299, .lon=12728900, .alt=311850}, /* 1e7deg, 1e7deg, mm (above WGS84 ref ellipsoid) */ \
 {.lat=434622299, .lon=12728900, .alt=311850}, /* 1e7deg, 1e7deg, mm (above WGS84 ref ellipsoid) */ \
 {.lat=434631486, .lon=12741000, .alt=311850}, /* 1e7deg, 1e7deg, mm (above WGS84 ref ellipsoid) */ \
};
#define WAYPOINTS_GLOBAL { \
 FALSE, \
 FALSE, \
 FALSE, \
 FALSE, \
};
#define NB_WAYPOINT 4
#define FP_BLOCKS { \
 "INIT_BLOCK" , \
 "Test Nav" , \
 "Wait GPS" , \
 "Take off" , \
 "Fly" , \
 "Land" , \
 "Standby" , \
 "HOME" , \
}
#define NB_BLOCKS 8
#define GROUND_ALT 185.
#define GROUND_ALT_CM 18500
#define SECURITY_HEIGHT 25.
#define SECURITY_ALT 210.
#define HOME_MODE_HEIGHT 25.
#define MAX_DIST_FROM_HOME 1500.


#ifndef FBW


#endif

#ifdef NAV_C


static inline void auto_nav_init(void) {
}

unsigned char const nb_blocks = 8;

void on_enter_block(unsigned char block)
{
  switch (block) {
    case 1:
      (enter_0());
      break;
    case 2:
      (enter_1());
      break;
    case 3:
      (enter_2());
      break;
    case 6:
      (enter_3());
      break;
    default:
      break;
    
  }
}

void on_exit_block(unsigned char block)
{
  switch (block) {
    case 1:
      (exit_0());
      break;
    case 2:
      (exit_1());
      break;
    case 5:
      (exit_2());
      break;
    case 6:
      (exit_3());
      break;
    default:
      break;
    
  }
}

_Bool forbidden_deroute(unsigned char from, unsigned char to)
{
  switch (from) {
    case 4:
      if (to == 2) {
        if (cond_forbi()) {
          return (_Bool) 1;
        }
      }
      if (to == 3) {
        return (_Bool) 1;
      }
      break;
    default:
      break;
    
  }
  return (_Bool) 0;
}

static inline void auto_nav(void)
{
  unsigned char tmp_nav_block;
  unsigned char tmp_nav_stage;
  _Bool tmp_cond;
  float tmp_RadOfDeg;
  float tmp_AltitudeMode;
  _Bool tmp_NavCond;
  tmp_nav_block = get_nav_block();
  switch (tmp_nav_block) {
    case 0:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        default:
          set_nav_stage(0);
          NextBlock();
          break;
        
      }
      break;
    case 1:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          nav_home();
          break;
        case 1:
          set_nav_stage(1);
          tmp_NavCond = NavApproachingFrom(2, 1, CARROT);
          if (tmp_NavCond) {
            last_wp = 2;
            NextStage();
            break;
          } else {
            NavSegment(1, 2);
            NavVerticalAutoPitchMode(0.750000 * 9600);
            NavGlide(1, 2);
          }
          break;
        default:
          set_nav_stage(2);
          NextBlock();
          break;
        
      }
      break;
    case 2:
      (test());
      tmp_nav_block = get_nav_block();
      if (tmp_nav_block != 4) {
        tmp_cond = (_Bool) excpetion2_loc();
      } else {
        tmp_cond = (_Bool) 0;
      }
      if (tmp_cond) {
        nav_goto_block(4);
        return;
      }
      tmp_nav_block = get_nav_block();
      if (tmp_nav_block != 2) {
        tmp_cond = (_Bool) excpetion3_loc();
      } else {
        tmp_cond = (_Bool) 0;
      }
      if (tmp_cond) {
        (test_exec());
        nav_goto_block(2);
        return;
      }
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          NextStage();
          nav_goto_block(3);
          break;
        case 1:
          set_nav_stage(1);
          test = 2;
          NextStage();
        case 2:
          set_nav_stage(2);
          if (!(test())) {
            NextStage();
          } else {
            break;
          }
        default:
          set_nav_stage(3);
          NextBlock();
          break;
        
      }
      (test);
      break;
    case 3:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          if (!(take_off())) {
            NextStage();
          } else {
            break;
          }
        default:
          set_nav_stage(1);
          NextBlock();
          break;
        
      }
      break;
    case 4:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          Return(0);
          break;
        default:
          set_nav_stage(1);
          NextBlock();
          break;
        
      }
      (post_call_2());
      break;
    case 5:
      (pre_call_2());
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          if (!(land())) {
            NextStage();
          } else {
            break;
          }
        case 1:
          set_nav_stage(1);
          Return(1);
          break;
        case 2:
          set_nav_stage(2);
          NextStage();
          nav_goto_block(5);
          break;
        default:
          set_nav_stage(3);
          NextBlock();
          break;
        
      }
      break;
    case 6:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          if (!(land())) {
            NextStage();
          } else {
            break;
          }
        case 1:
          set_nav_stage(1);
          Return(1);
          break;
        case 2:
          set_nav_stage(2);
          NextStage();
          nav_goto_block(5);
          break;
        default:
          set_nav_stage(3);
          NextBlock();
          break;
        
      }
      break;
    default:
      tmp_nav_stage = get_nav_stage();
      switch (tmp_nav_stage) {
        case 0:
          set_nav_stage(0);
          nav_home();
          break;
        default:
          set_nav_stage(1);
          NextBlock();
          break;
        
      }
      break;
    
  }
}


#endif // NAV_C

#endif // FLIGHT_PLAN_H
