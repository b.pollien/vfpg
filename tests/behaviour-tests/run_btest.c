#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#ifdef NEW
    #include "common_flight_plan.h"
#else
    #include "common_flight_plan.old.h"
    #include "flight_plan.old.h"
#endif


void print_state(int32_t seed, int32_t step) {
#ifdef NEW
    if (!display_for) {
        printf("(sd:%i, stp:%i) (b:%i, s:%i, lb:%i, ls: %i):\n", seed, step, \
                get_nav_block(), \
                get_nav_stage(),get_last_block(), get_last_stage());
    }
    else {
        printf("(sd:%i, stp:%i) (b:%i, lb:%i):\n", seed, step, \
                get_nav_block(), get_last_block());
    }
    printf("\tlast_wp -> %i, stage_time -> %i, block_time -> %i\n", \
                                        last_wp, stage_time, block_time);
    assert(nav_block == get_nav_block());
    assert(last_block == get_last_block());
    assert(nav_stage == get_nav_stage());
    assert(last_stage == get_last_stage());
    assert(1 != 2);
#else
    if (!display_for) {
        printf("(sd:%i, stp:%i) (b:%i, s:%i, lb:%i, ls: %i):\n", seed, step, \
                                    nav_block, \
                                    nav_stage, last_block, last_stage);
    } else {
        printf("(sd:%i, stp:%i) (b:%i, lb:%i):\n", seed, step, \
                                    nav_block, last_block);
    }
    
    printf("\tlast_wp -> %i, stage_time -> %i, block_time -> %i\n", \
                                        last_wp, stage_time, block_time);
#endif
}

void run_test(uint32_t nb_step){
    // Init var
    uint32_t seed = rand();
    printf("Seed: %i\n", seed);
    set_generator(seed);
#ifdef NEW
    set_nav_stage(0);
    set_nav_block(0); // Update the last block
    nav_goto_block(1);
    set_nav_block(0);
    set_nav_stage(0);
    stage_time = 0;
    block_time = 0;

#else
    nav_init_stage();
    nav_stage = 0; last_stage = 0;
    nav_block = 0; last_block = 0;
    stage_time = 0;
    block_time = 0;
#endif

    auto_nav_init();

    for(uint32_t step = 0; step < nb_step; step++){
        print_state(seed, step);
        auto_nav();
        printf("\n");
    }
}

int main(int argc, char *argv[]) {
    display_for = false;
    if(argc == 4) {
        display_for = true;
    }
    else if(argc != 3){
        fprintf(stderr,
        "Expected 2 or 3 arguments: nb test, nb step and if the stage must be displayed\n");
        return 1;
    }

    uint32_t nb_test = atoi(argv[1]);
    uint32_t nb_step = atoi(argv[2]);

    set_generator(0);

    for(uint32_t seed = 0; seed < nb_test; seed++) {
        printf("------TEST NB %i-----\n", seed);
        run_test(nb_step);
    }

    return 0;
}