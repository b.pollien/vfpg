#include "std.h"
#include <stdlib.h>

uint32_t last_wp = 0;
float ground_alt = 0.0;
uint8_t joystick_handler = 0;
bool display_for = false;

// Function that need to be call 5 time before returning true
bool call_five_time() {
    static int i = 0;
    printf("%i: call five time -> %i\n", gen_unique_id(), i);
    if (i == 5) {
        return TRUE;
    }
    else{
        i++;
        return FALSE;
    }
}

/* Functions to set the seed for the random generator */
void set_generator(uint32_t seed) {
    srand(seed);
}

/* Generates a random bool value */
bool gen_bool() {
    return rand() % 2 == 0;
}

/* Generates unique ID */
uint32_t gen_unique_id() {
    static uint32_t id = 0;
    if (display_for) {
        return 0;
    }

    id++;
    return id;
}

/* Definition of all nav functions needed for the behaviour tests */

void nav_init_stage() {
    if (!display_for) {
        printf("%i: nav_init_stage\n", gen_unique_id());
    }
}

/* Generates a random nav condition */
bool nav_cond(uint32_t i) {
    bool res = gen_bool();
    printf("%i: nav_cond %i -> %s\n", gen_unique_id(), i, res ? "true": "false");
    return res;
}

/* Generates a random nav value */
float nav_value(uint32_t i) {
    float res = rand();
    printf("%i: nav_value %i -> %f\n", gen_unique_id(), i, res);
    return res;
}

/* Generates call */
void nav_fun(uint32_t i) {
    printf("%i: call %i\n", gen_unique_id(), i);
}

void nav_home() {
    printf("%i: nav_home\n", gen_unique_id());
}

void NavKillThrottle() {
    printf("%i: NavKillThrottle\n", gen_unique_id());
}

float RadOfDeg(float val) {
    float res = rand();
    printf("%i: RadOfDeg %f -> %f\n", gen_unique_id(), val, res);
    return res;
}

float Height(float val) {
    float res = rand();
    printf("%i: Height %f -> %f\n", gen_unique_id(), val, res);
    return res;
}

void NavSegment(uint8_t val1, uint8_t val2) {
    printf("%i: NavSegment %i %i\n", gen_unique_id(), val1, val2);
}

void NavAttitude(float val) {
    printf("%i: NavAttitude %f\n", gen_unique_id(), val);
}

void NavVerticalAutoThrottleMode(float val) {
    printf("%i: NavVerticalAutoThrottleMode %f\n", gen_unique_id(), val);
}

void NavVerticalThrottleMode(float val) {
    printf("%i: NavVerticalThrottleMode %f\n", gen_unique_id(), val);
}

float GetPosHeight() {
    float res = rand();
    printf("%i: GetPosHeight-> %f\n", gen_unique_id(), res);
    return res;
}

void NavSetWaypointHere(uint8_t val) {
    printf("%i: NavSetWaypointHere %i\n", gen_unique_id(), val);
}

void NavGotoWaypoint(uint8_t val) {
    printf("%i: NavGotoWaypoint %i\n", gen_unique_id(), val);
}

void NavVerticalClimbMode(float val) {
    printf("%i: NavVerticalClimbMode %f\n", gen_unique_id(), val);
}

float WaypointAlt(uint8_t val) {
    float res = rand();
    printf("%i: WaypointAlt %i -> %f\n", gen_unique_id(), val, res);
    return res;
}

void NavCircleWaypoint(uint8_t val1, float val2) {
    printf("%i: NavCircleWaypoints %i %f\n", gen_unique_id(), val1, val2);
}

void NavVerticalAltitudeMode(float val1, float val2) {
    printf("%i: NavVerticalAltitudeMode %f %f\n", gen_unique_id(),\
                                                             val1, val2);
}

bool NavApproaching(uint8_t val1, float val2) {
    bool res = nav_cond(val1 + val2);
    printf("%i: NavApproaching %i %f -> %s\n", gen_unique_id(),\
                                        val1, val2, res ? "true": "false");
    return res;
}

bool NavApproachingFrom(uint8_t val1, uint8_t val2, float val3) {
    bool res = nav_cond(val1 + val2 + val3);
    printf("%i: NavApproachingFrom %i %i %f -> %s\n", gen_unique_id(), \
                                val1, val2, val3, res ? "true": "false");
    return res;
}

void NavVerticalAutoPitchMode(float val) {
    printf("%i: NavVerticalAutoPitchMode %f\n", gen_unique_id(), val);
}

bool NavGlide(uint8_t val1, uint8_t val2) {
    bool res = nav_cond(val1+val2);
    printf("%i: NavGlide %i %i -> %s\n", gen_unique_id(),\
                                        val1, val2, res ? "true": "false");
    return res;
}

void nav_eight_init() {
    printf("%i: nav_eight_init\n", gen_unique_id());
}

void Eight(uint8_t val1, uint8_t val2, float val3) {
    printf("%i: Eight %i %i %f\n", gen_unique_id(), \
                                val1, val2, val3);
}

void NavFollow(uint8_t val1, uint8_t val2, uint8_t val3) {
    printf("%i: NavFollow %i %i %i \n", gen_unique_id(), \
                                val1, val2, val3);
}

void NavHeading(float val) {
    printf("%i: NavHeading %f\n", gen_unique_id(), val);
}

void NavSetManual(float val1, float val2, float val3) {
    printf("%i: NavSetManual %f %f %f \n", gen_unique_id(), \
                                val1, val2, val3);
}

void nav_oval_init() {
    printf("%i: nav_oval_init\n", gen_unique_id());
}

void Oval(uint8_t val1, uint8_t val2, float val3 ) {
    printf("%i: Oval %i %i %f\n", gen_unique_id(), \
                                val1, val2, val3);
}

void NavSurveyRectangleInit(uint8_t val1, uint8_t val2, \
                                        uint8_t val3, uint8_t val4) {
    printf("%i: NavSurveyRectangleInit %i %i %i %i\n", gen_unique_id(), \
                                val1, val2, val3, val4);
}

void NavSurveyRectangle(uint8_t val1, uint8_t val2) {
    printf("%i: NavSurveyRectangle %i %i \n", gen_unique_id(), \
                                val1, val2);
}

void Goto3D(float val) {
    printf("%i: Goto3D %f\n", gen_unique_id(), val);
}

void AbiBindMsgJOYSTICK(uint8_t val1, abi_event *val2, uint8_t val3 ) {
    printf("%i: AbiBindMsgJOYSTICK %i %i %i\n", gen_unique_id(), \
                                val1, *val2, val3);
}

void NavGuided(uint8_t val1,  float x, float y, float z, float yaw) {
    printf("%i: NavGuided %i %f %f %f %f\n", gen_unique_id(), val1, x, y, z, yaw);
}

void RotorGuided(uint8_t val1, float x, float y, float z, float yaw, float params) {
    printf("%i: RotorGuided %i %f %f %f %f %f\n", gen_unique_id(), val1, x, y, z, yaw, params);
}