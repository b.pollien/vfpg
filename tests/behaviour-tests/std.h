#ifndef STD_H
#define STD_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define FALSE 0
#define TRUE 1
#define CARROT 0
#define WE 1
#define NS 2

#define ABI_BROADCAST 0

#define INTENTIONAL_FALLTHRU

typedef int8_t abi_event;

extern uint32_t last_wp;
extern float ground_alt;
extern uint8_t joystick_handler;

extern bool display_for;

/* Functions to set the seed for the random generator */
void set_generator(uint32_t seed);

/* Generates unique ID */
uint32_t gen_unique_id();

/* Definition of all nav functions needed for the behaviour tests */

bool call_five_time();
void nav_init_stage();
bool nav_cond(uint32_t i);
float nav_value(uint32_t i);
void nav_fun(uint32_t i);

void NavKillThrottle();
void nav_home();
float RadOfDeg(float val);
float Height(float val);
void NavSegment(uint8_t val1, uint8_t val2);
void NavAttitude(float val);
void NavVerticalAutoThrottleMode(float val);
void NavVerticalThrottleMode(float val);
float GetPosHeight();
void NavSetWaypointHere(uint8_t val);
void NavGotoWaypoint(uint8_t val);
void NavVerticalClimbMode(float val);
float WaypointAlt(uint8_t val);
void NavCircleWaypoint(uint8_t val1, float val2);
void NavVerticalAltitudeMode(float val1, float val2);
bool NavApproaching(uint8_t val1, float val2);
bool NavApproachingFrom(uint8_t val1, uint8_t val2, float val3);
void NavVerticalAutoPitchMode(float val);
bool NavGlide(uint8_t val1, uint8_t val2);
void nav_eight_init();
void Eight(uint8_t val1, uint8_t val2, float val3);
void NavFollow(uint8_t val1, uint8_t val2, uint8_t val3);
void NavHeading(float val);
void NavSetManual(float val1, float val2, float val3);
void nav_oval_init();
void Oval(uint8_t val1, uint8_t val2, float val3);
void NavSurveyRectangleInit(uint8_t val1, uint8_t val2, \
                                        uint8_t val3, uint8_t val4);
void NavSurveyRectangle(uint8_t val1, uint8_t val2);
void Goto3D(float val);
void AbiBindMsgJOYSTICK(uint8_t val1, abi_event *val2, uint8_t val3);
void NavGuided(uint8_t val1,  float x, float y, float z, float yaw);
void RotorGuided(uint8_t val1, float x, float y, float z, float yaw, float params);


#endif