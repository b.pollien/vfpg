#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "The number of parameters is incorrect !"
    echo "Expected the executable and the folder with the tests"
    exit 1
fi

BIN=$1
TESTS_FOLDER=$2
HOME=`pwd`
OUT=$HOME/out/regression-tests
TMP=$OUT/tmp_regression_file.txt
TMP_DIFF=$OUT/tmp_diff.txt

unameOut="$(uname -s)"
case "${unameOut}" in
    Darwin*)    SED_PARAM="''"; LANG=C;;
    *)          SED_PARAM=""
esac

TMP_OLD=$OUT/tmp_old_generator.txt

# Building the old generator
OLD_GEN=$HOME/ocaml-generator
cd $OLD_GEN
dune build
cd $HOME

echo "Running the non regression tests"

mkdir -p $OUT
RES=0

for FILE in $(find $TESTS_FOLDER -name *.xml)
do
    CORRECT=${FILE%.*}.h
    TO_TEST=$OUT/${CORRECT##*/}
    touch $TMP
    ./$BIN $FILE $TO_TEST >> $TMP 2>> $TMP

    if [ $? -ne 0 ]; then
        echo "------ Error during the generation ------"
         echo $FILE
        cat $TMP
        echo "--------------------------------------------"
        echo ""
        RES=1
        continue
    fi

    # Removing the version number
    sed -i $SED_PARAM 's/Version v.* /Version v6.0_unstable-none--dirty /g' $TO_TEST

    if [ ! -e $TO_TEST ]; then
        echo "---------- No file generated ------------"
        echo $FILE
        cat $TMP
        echo "-----------------------------------------"
        echo ""
        RES=1
        continue
    fi

    #TO REMOVE: save file of the old generation
    if [ ! -e $CORRECT ]; then
        echo "Regression test do not currently exist for $FILE"
        cd $OLD_GEN
        ./run.sh $HOME/$FILE $TO_TEST.old
        cd $HOME
        echo "To update the database, use the command:"
        echo "cp $TO_TEST $CORRECT"
        echo ""
        RES=1
        continue
    fi

    diff $CORRECT $TO_TEST > $TMP_DIFF
    if [ $? -ne 0 ]; then
        echo "Regression test error on" $CORRECT
        cat $TMP_DIFF
        echo "LOGS:"
        cat $TMP
        cd $OLD_GEN
        ./run.sh $HOME/$FILE $TO_TEST.old
        cd $HOME
        echo "To update the database, use the command:"
        echo "cp $TO_TEST $CORRECT"
        echo ""
        RES=1
    fi
    rm $TMP
done

if [ $RES -eq 0 ]; then
    echo "OK"
else
    echo "FAILED"
fi
