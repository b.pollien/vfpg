#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "The number of parameters is incorrect !"
    echo "Expected the executable and the folder with the tests"
    exit 1
fi

BIN=$1
HOME=`pwd`
cd $2
TESTS_FOLDER=`pwd`
cd $HOME
OUT=$HOME/out/behaviour-tests

TMP_DIFF=$OUT/tmp_diff_behaviour_test.txt

TMP_NEW=$OUT/tmp_new_generator.txt
TMP_OLD=$OUT/tmp_old_generator.txt
TRACE_NEW=$OUT/trace_new_generator.txt
TRACE_OLD=$OUT/trace_old_generator.txt

OLD_GEN=$HOME/ocaml-generator

# Building the old generator
cd $OLD_GEN
dune build
cd $HOME

# Add lastest version of common_flight_plan
cp $HOME/common-c-code/* $TESTS_FOLDER

# Generation of some needed file for the compilation
mkdir -p $OUT

touch $OUT/autopilot.h
mkdir -p $OUT/generated $OUT/modules/core
touch $OUT/generated/modules.h
touch $OUT/modules/core/abi.h

echo "Running the behavior tests"

RES=0

for FILE in $(find $TESTS_FOLDER/XML -name *.xml)
do
    # Generating flight plan with the new generator
    TMP_NAME=${FILE%.*}.h
    NEW=$OUT/${TMP_NAME##*/}
    touch $TMP_NEW
    ./$BIN $FILE $NEW >> $TMP_NEW 2>> $TMP_NEW

    if [ $? -ne 0 ]; then
        echo "---- Error during the generation with the new generator ----"
         echo $FILE
        cat $TMP_NEW
        echo "-------------------------------------------------------------"
        echo ""
        RES=1
        continue
    fi

    if [ ! -e $TO_TEST ]; then
        echo "------- No file generated with the new generator -------"
        echo $FILE
        cat $TMP_NEW
        echo "--------------------------------------------------------"
        echo ""
        RES=1
        continue
    fi

    # Generating flight plan with the old generator
    TMP_NAME=${FILE%.*}.old.h
    OLD=$OUT/${TMP_NAME##*/}
    cd $OLD_GEN
    touch $TMP_OLD
    ./run.sh $FILE $OLD >> $TMP_OLD 2>> $TMP_OLD
    cd $HOME

    if [ $? -ne 0 ]; then
        echo "---- Error during the generation with the old generator ----"
        echo $FILE
        cat $TMP_OLD
        echo "------------------------------------------------------------"
        echo ""
        RES=1
        continue
    fi

    if [ ! -e $OLD ]; then
        echo "------- No file generated with the old generator -------"
        echo $FILE
        cat $TMP_OLD
        echo "--------------------------------------------------------"
        echo ""
        RES=1
        continue
    fi

    # Renaming the flight plan
    cp $NEW $OUT/flight_plan.h
    cp $OLD $OUT/flight_plan.old.h

    # Compiling
    make clean -f $TESTS_FOLDER/Makefile OUT=$OUT --silent
    make all -f $TESTS_FOLDER/Makefile OUT=$OUT \
                TESTS_FOLDER=$TESTS_FOLDER --silent
    if [ $? -ne 0 ]; then
        echo "Error during the compilation: $FILE"
        exit 1
    fi

    # Generate the trace for the FP generated with the new generator
    $OUT/run_btest_new $(cat ${FILE%.*}.cfg) > $TRACE_NEW

    # Generate the trace for the FP generated with the old generator
    $OUT/run_btest_old $(cat ${FILE%.*}.cfg)> $TRACE_OLD

    # Save the trace
    if [[ $TRACE_B == 1 ]]; then
        TMP_NAME=${FILE%.*}.trace
        cp $TRACE_NEW $OUT/${TMP_NAME##*/}
    fi

    diff $TRACE_NEW $TRACE_OLD > $TMP_DIFF
    if [ $? -ne 0 ]; then
        echo "Behaviour test error on" $FILE
        cat $TMP_DIFF
        echo "See $TRACE_NEW"
        echo "or $TRACE_OLD"
        RES=1
        exit 1
    fi

    rm $TMP_NEW $TMP_OLD
done

if [ $RES -eq 0 ]; then
    echo "OK"
else
    echo "FAILED"
fi