#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "The number of parameters is incorrect !"
    echo "Expected the executable and the folder with the tests"
    exit 1
fi

GEN=$1
SCRIPT=$2
HOME=`pwd`
OUT=$HOME/out/c-light-generated
TMP_DIFF=$OUT/tmp_diff_c-light-generated.txt

echo "Running the c light generation tests"

mkdir -p $OUT
RES=0

$HOME/CompCert/clightgen common-c-code/common_flight_plan.c -DGEN -o $OUT/CommonFP.v
./tools/patch_common_fp.sh $OUT/CommonFP.v

diff $GEN/CommonFP.v $OUT/CommonFP.v > $TMP_DIFF
if [ $? -ne 0 ]; then
    echo "Clight generation error..."
    cat $TMP_DIFF
    RES=1
fi


if [ $RES -eq 0 ]; then
    echo "OK"
else
    echo "FAILED"
fi
