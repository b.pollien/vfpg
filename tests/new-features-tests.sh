#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "The number of parameters is incorrect !"
    echo "Expected the executable and the folder with the tests"
    exit 1
fi

BIN=$1
HOME=`pwd`
cd $2
TESTS_FOLDER=`pwd`
cd $HOME
OUT=$HOME/out/new-features-tests

TMP=$OUT/tmp_new_features.txt
TRACE=$OUT/trace_test_new_features.txt

# Add lastest version of common_flight_plan
cp $HOME/common-c-code/* $TESTS_FOLDER

# Generation of some needed file for the compilation
mkdir -p $OUT

touch $OUT/autopilot.h
mkdir -p $OUT/generated $OUT/modules/core
touch $OUT/generated/modules.h
touch $OUT/modules/core/abi.h

echo "Running the tests for the new features"


RES=0

for FILE in $(find $TESTS_FOLDER/XML -name *.xml)
do
    # Generating flight plan with the new generator
    TMP_NAME=${FILE%.*}.h
    NEW=$OUT/${TMP_NAME##*/}
    touch $TMP
    ./$BIN $FILE $NEW >> $TMP 2>> $TMP

    if [ $? -ne 0 ]; then
        echo "------ Error during the generation ------"
         echo $FILE
        cat $TMP
        echo "--------------------------------------------"
        echo ""
        RES=1
        rm $TMP
        continue
    fi

    if [ ! -e $TO_TEST ]; then
        echo "------------ No file generated ------------"
        echo $FILE
        cat $TMP
        echo "--------------------------------------------"
        echo ""
        RES=1
        rm $TMP
        continue
    fi

    # Renaming the flight plan
    cp $NEW $OUT/flight_plan.h

    # Compiling
    make clean -f $TESTS_FOLDER/Makefile OUT=$OUT --silent
    make all -f $TESTS_FOLDER/Makefile OUT=$OUT \
                TESTS_FOLDER=$TESTS_FOLDER --silent
    if [ $? -ne 0 ]; then
        echo "Error during the compilation: $FILE"
        exit 1
    fi

    # Generate the trace for the FP generated with the new generator
    $OUT/run_nftest > $TRACE

    if [ $? -ne 0 ]; then
        echo "New features test error on" $FILE
        cat $TRACE
        echo "See $TRACE"
        RES=1
        exit 1
    fi

    rm $TRACE $TMP
done

if [ $RES -eq 0 ]; then
    echo "OK"
else
    echo "FAILED"
fi