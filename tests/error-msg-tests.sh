#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "The number of parameters is incorrect !"
    echo "Expected the executable and the folder with the tests"
    exit 1
fi

BIN=$1
TESTS_FOLDER=$2
HOME=`pwd`
OUT=$HOME/out/error-msg-tests
TMP=$OUT/error-res.txt
TMP_DIFF=$OUT/error-res-diff.txt

echo "Running the error message tests"

mkdir -p $OUT
RES=0

for FILE in $(find $TESTS_FOLDER -name *.xml)
do
    RES_H=${FILE%.*}.h
    RES_H=$OUT/${RES_H##*/}
    rm -f $TMP
    touch $TMP
    ./$BIN $HOME/$FILE $RES_H >> $TMP 2>> $TMP

    # if [ $? -ne 0 ]; then
    #     echo "----------- Error not detected -------------"
    #     echo $FILE
    #     cat $TMP
    #     echo "--------------------------------------------"
    #     echo ""
    #     RES=1
    #     continue
    # fi

    RES_FILE=${FILE%.*}.res

    diff $TMP $RES_FILE > $TMP_DIFF
    if [ $? -ne 0 ]; then
        echo "Error message not expeced" $CORRECT
        cat $TMP_DIFF
        echo "LOGS:"
        cat $TMP
        RES=1
    fi

    rm $TMP $TMP_DIFF

done

if [ $RES -eq 0 ]; then
    echo "OK"
else
    echo "FAILED"
fi
