#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "The number of parameters is incorrect !"
    echo "Expected a flight plan and an output file"
    exit 1
fi


# Source of the clone of the paparazzi project
PAPARAZZI_HOME="$(pwd)/../paparazzi"
PAPARAZZI_SRC=$PAPARAZZI_HOME

dune exec ./dump_flight_plan.exe $1 $2