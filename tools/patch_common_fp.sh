#!/bin/bash

echo "From VFP Require Import ClightGeneration." | cat - $1 > temp && mv temp $1

unameOut="$(uname -s)"

# Change $ to #
case "${unameOut}" in
    Darwin*)    sed -i '' 's/\$\"/#\"/g' $1;;
    *)          sed -i 's/\$\"/#\"/g' $1
esac

# Replace the definition of _t'1 from a positive to an id.
case "${unameOut}" in
    Darwin*)    sed -i '' 's/Definition\ _t\x271\ :\ ident\ :=\ 128\%positive\./Definition\ _t\x271\ :\ ident\ :=\ \#\"t\x271\"\./g' $1;;
    *)          sed -i 's/Definition\ _t\x271\ :\ ident\ :=\ 128\%positive\./Definition\ _t\x271\ :\ ident\ :=\ \#\"t\x271\"\./g' $1
esac

# Use arbitrary_ident for nav_init_stage ident
case "${unameOut}" in
    Darwin*)    sed -i '' 's/Definition\ _nav_init_stage\ :\ ident\ :=\ \#\"nav_init_stage\"/Definition\ _nav_init_stage\ :\ ident\ :=\ \#\#\"nav_init_stage\"/g' $1;;
    *)          sed -i 's/Definition\ _nav_init_stage\ :\ ident\ :=\ \#\"nav_init_stage\"/Definition\ _nav_init_stage\ :\ ident\ :=\ \#\#\"nav_init_stage\"/g' $1
esac
