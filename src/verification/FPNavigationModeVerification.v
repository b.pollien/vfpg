From Coq Require Import Nat Arith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

From compcert Require Import  AST Ctypes
                        Cop Clight Clightdefs.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import ClightLemmas CommonStringLemmas
                        BasicTypes 
                        FPNavigationMode FPNavigationModeGen
                        FPNavigationModeSem
                        FPEnvironmentClight
                        CommonFPDefinition ClightGeneration
                        TmpVariables.

Local Open Scope clight_scope.
Local Open Scope nat_scope.

(* Local Open Scope string_scope.
Local Open Scope list_scope.

Local Open Scope bool_scope. *)

Set Implicit Arguments.

(** * Proofs about nav stage *)

Lemma no_nav_cond_impl_code:
  forall nav_mode,
  nav_cond_sem nav_mode = None
  -> gen_fp_nav_cond nav_mode = gen_fp_nav_code nav_mode.
Proof.
  move => nav_mode H.
  destruct nav_mode; inversion H; rewrite //=.
  destruct (get_go_from params); inversion H1.
Qed.

Lemma gen_fp_nav_pre_call_res:
  forall nav_mode, exists pre, 
    gen_fp_nav_pre_call nav_mode = Sskip
      \/ gen_fp_nav_pre_call nav_mode = parse_c_code pre.
Proof.
  rewrite /gen_fp_nav_pre_call => nav_mode.
  destruct (get_fp_pnav_call nav_mode) as [ncall|] eqn:Hncall;
    [destruct (get_nav_pre_call ncall) as [pre|] eqn:Hpre |].
  - exists pre. by right.
  - all: exists EmptyString; by left.
Qed.

Lemma gen_fp_nav_post_call_res:
  forall nav_mode, exists post, 
    gen_fp_nav_post_call nav_mode = Sskip
      \/ gen_fp_nav_post_call nav_mode = parse_c_code post.
Proof.
  rewrite /gen_fp_nav_post_call => nav_mode. 
  destruct (get_fp_pnav_call nav_mode) as [ncall|] eqn:Hncall;
    [destruct (get_nav_post_call ncall) as [post|] eqn:Hpost |].
  - exists post. by right.
  - all: exists EmptyString; by left.
Qed.

Lemma pre_call_gen_sem_none:
  forall nav_mode,
    gen_fp_nav_pre_call nav_mode = Sskip
    -> pre_call_nav_sem nav_mode = SKIP.
Proof.
  move => nav_mode H.
  destruct nav_mode eqn:Hm; try destruct params_call eqn:Hp;
    try destruct get_nav_pre_call eqn:Hpe;
    rewrite /gen_fp_nav_pre_call //= in H.
Qed.

Lemma post_call_gen_sem_none:
  forall nav_mode,
    gen_fp_nav_post_call nav_mode = Sskip
    -> post_call_nav_sem nav_mode = SKIP.
Proof.
  move => nav_mode H.
  destruct nav_mode eqn:Hm; try destruct params_call eqn:Hp;
    try destruct get_nav_post_call eqn:Hpe;
    rewrite /gen_fp_nav_post_call //= in H.
Qed.

Lemma nav_cond_impl_go:
  forall nav_mode cond,
    nav_cond_sem nav_mode = Some cond
    -> exists ps pm pc, nav_mode = GO ps pm pc.
Proof.
  destruct nav_mode; move => cond H; inversion H.
  by exists params, params_mode, params_call.
Qed.

Lemma pre_call_gen_sem_some:
  forall nav_mode pre,
    gen_fp_nav_pre_call nav_mode = parse_c_code pre
    -> pre_call_nav_sem nav_mode = C_CODE pre.
Proof.
  rewrite /gen_fp_nav_pre_call
            /pre_call_nav_sem => nav_mode pre H.
  destruct nav_mode eqn:Hm; rewrite //=;
    try destruct params_call eqn:Hp; rewrite //=; rewrite //= in H;
    try destruct get_nav_pre_call eqn:Hpe; inversion H as [H'];
    apply arbitrary_ident_injective in H'; by subst pre.
Qed.

Lemma post_call_gen_sem_some:
  forall nav_mode post,
    gen_fp_nav_post_call nav_mode = parse_c_code post
    -> post_call_nav_sem nav_mode = C_CODE post.
Proof.
  rewrite /gen_fp_nav_post_call
            /post_call_nav_sem => nav_mode post H.
  destruct nav_mode eqn:Hm; rewrite //=;
    try destruct params_call eqn:Hp; rewrite //=; rewrite //= in H;
    try destruct get_nav_post_call eqn:Hpe; inversion H as [H'];
    apply arbitrary_ident_injective in H'; by subst post.
Qed.

Lemma eq_exec_pre_nav_call:
  forall (ge: genv) f nav_mode k e le m,
    Smallstep.star step2 ge
    (State f (gen_fp_nav_pre_call nav_mode) k e le m)
    (fp_trace_to_trace [:: pre_call_nav_sem nav_mode])
    (State f Sskip k e le m).
Proof.
  move => ge f nav_mode k e le m.
  destruct (gen_fp_nav_pre_call_res nav_mode) as [pre [Hnone | Hpre]].
  - rewrite Hnone. rewrite (pre_call_gen_sem_none Hnone) //=.
    step_refl.
  - step_trans. rewrite Hpre.
    apply exec_arbitrary_C_code. step_refl.
    by rewrite (pre_call_gen_sem_some Hpre).
Qed.

Lemma eq_exec_post_nav_call:
  forall (ge: genv) f nav_mode k e le m,
    Smallstep.star step2 ge
    (State f (gen_fp_nav_post_call nav_mode) k e le m)
    (fp_trace_to_trace [:: post_call_nav_sem nav_mode])
    (State f Sskip k e le m).
Proof.
  move => ge f nav_mode k e le m.
  destruct (gen_fp_nav_post_call_res nav_mode) as [post [Hnone | Hpost]].
  - rewrite Hnone. rewrite (post_call_gen_sem_none Hnone) //=.
    step_refl.
  - step_trans. rewrite Hpost.
    apply exec_arbitrary_C_code. step_refl.
    by rewrite (post_call_gen_sem_some Hpost).
Qed.

Lemma nav_cond_sem_to_stmt:
  forall ps pm pc cond,
  nav_cond_sem (GO ps pm pc) = Some cond
  -> exists stmt_cond wp,
      nav_cond_stmt (GO ps pm pc) = Some (stmt_cond, wp).
Proof.
  move => ps pm pc cond H.
  destruct (nav_cond_stmt (GO ps pm pc)) as [[stmt_cond wp]|] eqn:H'.
  - by exists stmt_cond, wp.
  - rewrite /nav_cond_sem in H; rewrite /nav_cond_stmt in H'.
    destruct (get_go_from ps). inversion H; inversion H'.
    discriminate H'.
Qed.

Remark exists_string_int_of_nat:
  forall j, exists i,
    int_of_nat i = int_of_nat j
    /\ string_of_nat i = string_of_nat j.
Proof. move => j. by exists j. Qed.

Lemma eq_exec_nav_init_code:
  forall (ge: genv) fpe nav_mode k e le e2,
    Smallstep.star step2 ge
      (State fpe (FPNavigationModeGen.gen_fp_nav_init_call nav_mode)
          k e le e2)
      (fp_trace_to_trace [:: init_code_nav_sem nav_mode])
      (State fpe Sskip k e le e2).
Proof.
  move => ge fpe nav_mode k e le e2.

  destruct nav_mode; rewrite //=; try step_refl.

  (* Survey rectangle *)
  { remember (Tcons _ _) as ltypes.
    remember [:: gen_uint8_const _; _; _; _] as e_p.
    remember [:: _; _; _; _ ] as o_p.
    rewrite /FPNavigationModeGen.gen_fun_call_stmt
              /FPNavigationModeGen.init_code_nav_ident 
              /gen_fun_call_nav //=.
    destruct (get_nav_nav_params params_call);
      apply exec_arbitrary_fun_call; try by [].
    - subst ltypes e_p o_p; rewrite //=;
      repeat (split; try by []);
        try apply exists_string_int_of_nat;
        try by simpl_eqparam.
    - subst ltypes e_p o_p;rewrite //=;
      repeat (split; try by []);
        try apply exists_string_int_of_nat;
        try simpl_eqparam. }

  (* Eight *)
  { rewrite /FPNavigationModeGen.gen_fun_call_stmt
            /FPNavigationModeGen.init_code_nav_ident //=.
    destruct (get_nav_nav_params params_call);
      apply exec_arbitrary_fun_call; try by []. }

  (* Oval *)
  { rewrite /FPNavigationModeGen.gen_fun_call_stmt
            /FPNavigationModeGen.init_code_nav_ident //=.
    destruct (get_nav_nav_params params_call);
      apply exec_arbitrary_fun_call; try by []. }
Qed.

Lemma eq_exec_last_wp:
forall ps pm pc stmt_cond wp (ge: genv) f k e le m,
  nav_cond_stmt (GO ps pm pc) = Some (stmt_cond, wp)
  -> Smallstep.star step2 ge
      (State f
        (Sassign (gen_uint8_var last_wp_var) (gen_uint8_const wp)) 
          k e le m)
      (fp_trace_to_trace [:: C_CODE ("last_wp = "
                                  ++ string_of_nat (get_go_wp ps))%string])
      (State f Sskip k e le m).
Proof.
  move => ps pm pc stmt_cond wp ge f k e le m Hn.
  replace (fp_trace_to_trace [:: C_CODE _])
    with [::(code_event_optassign (Some last_wp_var_str)
                                        (string_of_nat wp) None)].
  apply exec_arbitrary_assign_C_code.
  - simpl_eqparam_ex wp.
  - destruct ps as [wp' [last|] hmode ap]; inversion Hn; by [].
Qed.

Lemma gen_fun_call_app_var:
  forall var func params,
    gen_fun_call_set_sem var func params
    = (var ++ " = " ++ gen_fun_call_sem func params)%string.
Proof.
  rewrite /gen_fun_call_set_sem /gen_fun_call_sem
    => var func params.
  rewrite -?string_app_assoc. by repeat apply eq_app_fst_string.
Qed.

Remark code_event_assign_eq_set:
  forall var func params,
    code_event (gen_fun_call_set_sem var func params)
    = code_event_assign var (gen_fun_call_sem func params).
Proof.
  rewrite /code_event_assign /code_event
             => var func params.
  by rewrite gen_fun_call_app_var.
Qed.

Lemma match_params_last:
  forall ltypes e_p params t p p',
    match_param p p'
    -> t = typeof p
    -> match_params ltypes e_p params
    -> match_params (add_last_tl ltypes t)
                          (e_p ++ [:: p])%list
                          (params ++ [:: p'])%list.
Proof.
  induction ltypes as [|t' lt IHlt]; move => e_p params t p p' Hp Ht Hps.
  - destruct e_p; destruct params; inversion Hps; simpl_eqparam.
  - destruct e_p; destruct params; inversion Hps; try by [].
    rewrite //=. rewrite //= in Hps. destruct Hps as [Hp' [Ht' Hps']].
    repeat split; try by []. by apply IHlt.
Qed.

Definition uint_9600: Decimal.uint :=
        (Decimal.D9 (Decimal.D6 (Decimal.D0 (Decimal.D0 Decimal.Nil)))).

Lemma match_params_9600_t:
  forall throttle,
  match_params (Tcons tfloat Tnil)
      [:: gen_mul_expr (parse_c_code_float throttle)
                          (gen_uint8_const (of_uint uint_9600))
                           tfloat]
      [:: (throttle ++ "*" ++ (string_of_nat (of_uint uint_9600)))%string].
Proof.
  move => throttle. simpl_eqparam_ex throttle.
  exists throttle, (string_of_nat (of_uint uint_9600)).
  repeat split; try by [].
  by exists (of_uint uint_9600).
Qed.

Lemma eq_exec_RadOfDeg:
  forall (ge:genv) f param k e le m,
    Smallstep.star step2 ge
      (State f (Scall (Some tmp_RadOfDeg)
                        RadOfDeg [:: parse_c_code_float param])
                  k e le m)
      (fp_trace_to_trace [:: radofdeg_sem param])
      (State f Sskip k e le m).
Proof.
  move => ge f param k e le m.
  rewrite //= code_event_assign_eq_set.
  apply exec_arbitrary_fun_call_res; try simpl_eqparam.
Qed.

Lemma eq_exec_gen_fp_pitch:
  forall (ge:genv) f type pitch k e le m,
    Smallstep.star step2 ge
      (State f (gen_fp_pitch type pitch) k e le m)
      (fp_trace_to_trace (gen_fp_pitch_sem type pitch))
      (State f Sskip k e le m).
Proof.
  move => ge f type pitch k e le m.

  destruct pitch as [t|p| ]; rewrite //=.
  - apply exec_arbitrary_fun_call. apply match_params_9600_t.
  - step_seq. apply eq_exec_RadOfDeg. step_skip_seq.
    apply exec_arbitrary_fun_call; simpl_eqparam.
    all: try by []. simpl_eqparam.
  - step_refl.
Qed.

Lemma eq_exec_gen_fun_call:
  forall (ge:genv) f k e le m func tres ltypes e_p params pc,
    match_params ltypes e_p params
    -> Smallstep.star step2 ge
          (State f (gen_fun_call_stmt (##func) tres ltypes e_p pc)
                      k e le m)
          [:: code_event (gen_fun_call_nav func params pc)]
          (State f Sskip k e le m).
Proof.
  rewrite /gen_fun_call_stmt /gen_fun_call_nav
    => ge f k e le m func tres ltypes e_p params pc H.

  destruct (get_nav_nav_params pc) as [p|];
    apply exec_arbitrary_fun_call; try by [].
  by apply match_params_last.
Qed.

Lemma eq_exec_gen_fp_vmode:
  forall (ge:genv) f type vmode k e le m,
    Smallstep.star step2 ge
      (State f (gen_fp_vmode type vmode) k e le m)
      (fp_trace_to_trace (gen_fp_vmode_sem type vmode))
      (State f Sskip k e le m).
Proof.
  move => ge f type vmode k e le m.

  destruct vmode as [c|a| | wp l_wp| t |]; rewrite //=.
  (* Climb *)
  apply exec_arbitrary_fun_call.
  split; try by []; by apply cast_possible_eq.

  (* Alt *)
  destruct a as [a | h | wp].
  - step_seq. step_skip_seq.
    apply exec_arbitrary_fun_call.
    all: try by []. simpl_eqparam_ex 0. step_refl.
  - step_seq. apply exec_arbitrary_fun_call_res; try by [].
    have H : match_params (Tcons tfloat Tnil)
        [:: parse_c_code_float h] [:: h].
    simpl_eqparam_ex 0. apply H.
    step_skip_seq. apply exec_arbitrary_fun_call.
    all: try by []. simpl_eqparam_ex 0.
  - step_seq. apply exec_arbitrary_fun_call_res; try by [].
    have H : match_params (Tcons tuchar Tnil) [:: gen_uint8_const wp]
       [:: string_of_nat wp] by simpl_eqparam_ex wp. apply H.
    step_skip_seq. apply exec_arbitrary_fun_call.
    all: try by []. simpl_eqparam_ex 0.

  (* XYZ *)
  step_refl.

  (* Glide *)
  apply exec_arbitrary_fun_call.
  simpl_eqparam_ex l_wp; try by []; try simpl_eqparam_ex wp; try by [].

  (* Throttle *)
  apply exec_arbitrary_fun_call.
  apply match_params_9600_t.

  (* No vmode *)
  step_refl.
Qed.

Lemma eq_exec_gen_fp_hmode:
  forall (ge:genv) f hmode params k e le m,
    Smallstep.star step2 ge
      (State f (gen_fp_hmode hmode params) k e le m)
      (fp_trace_to_trace [:: gen_fp_hmode_sem hmode params])
      (State f Sskip k e le m).
Proof.
  move => ge f hmode params k e le m.

  destruct hmode as [wp l_wp | wp].

  (* Route *)
  step_trans. apply eq_exec_gen_fun_call.
  all: try by []. 
  simpl_eqparam_ex l_wp; try by []; try simpl_eqparam_ex wp; try by [].
  step_refl.

  (* Direct *)
  step_trans. apply eq_exec_gen_fun_call.
  all: try by []. 
  simpl_eqparam_ex l_wp; try by []; try simpl_eqparam_ex wp; try by [].
  step_refl.
Qed.

Lemma eq_exec_gen_fp_nav_mode_params:
  forall (ge:genv) f pc pm k e le m,
    Smallstep.star step2 ge
      (State f (gen_fp_nav_mode_params pc pm) k e le m)
      (fp_trace_to_trace (gen_fp_nav_mode_params_sem pc pm))
      (State f Sskip k e le m).
Proof.
  move => ge f pc pm k e le m.
  step_seq. apply eq_exec_gen_fp_pitch. step_skip_seq.
  apply eq_exec_gen_fp_vmode. all: try by [].
  by simpl_trace.
Qed.

Lemma eq_exec_gen_fun_call_heading:
  forall (ge:genv) f k e le m pc,
    let func := heading_code pc in
      Smallstep.star step2 ge
        (State f (gen_fun_call_stmt (##func) tvoid (Tcons tfloat Tnil)
                                    [:: gen_float_tempvar tmp_RadOfDeg] pc)
                    k e le m)
        [:: code_event (gen_fun_call_nav func
                                          (tmp_RadOfDeg_str :: nil) pc)]
        (State f Sskip k e le m).
Proof.
  move => ge f k e le m pc func.
  apply eq_exec_gen_fun_call; simpl_eqparam.
Qed.

Lemma eq_exec_gen_fun_call_attitude:
  forall (ge:genv) f k e le m pc,
    let func := attitude_code pc in
      Smallstep.star step2 ge
        (State f (gen_fun_call_stmt (##func) tvoid (Tcons tfloat Tnil)
                                    [:: gen_float_tempvar tmp_RadOfDeg] pc)
                    k e le m)
        [:: code_event (gen_fun_call_nav func
                                          (tmp_RadOfDeg_str :: nil) pc)]
        (State f Sskip k e le m).
Proof.
  move => ge f k e le m pc func. 
  apply eq_exec_gen_fun_call; simpl_eqparam.
Qed.

Lemma eq_exec_gen_fp_nav_code:
  forall (ge: genv) f nav_mode k e le e2,
  Smallstep.star step2 ge
    (State f (gen_fp_nav_code nav_mode) k e le e2)
    (fp_trace_to_trace (gen_fp_nav_code_sem nav_mode))
    (State f Sskip k e le e2).
Proof.
  rewrite /gen_fp_nav_code /gen_fp_nav_code_sem
            => ge f nav_mode k e le e2.
  destruct nav_mode; rewrite //=.

  (* Heading *)
  { step_seq. apply eq_exec_RadOfDeg. step_skip_seq.
    step_seq. apply eq_exec_gen_fun_call_heading. step_skip_seq.
    apply eq_exec_gen_fp_nav_mode_params.
    all: try by []. }

  (* Attitude *)
  { step_seq. apply eq_exec_RadOfDeg. step_skip_seq.
    step_seq. apply eq_exec_gen_fun_call_attitude. step_skip_seq.
    apply eq_exec_gen_fp_nav_mode_params.
    all: try by []. }

  (* Manual *)
  { step_seq. apply eq_exec_gen_fun_call.
    all: try by []. simpl_eqparam_ex (get_manual_roll params).
    step_skip_seq. apply eq_exec_gen_fp_nav_mode_params.
    all: try by []. }

  (* Go *)
  { step_seq. apply eq_exec_gen_fp_hmode. step_skip_seq.
    apply eq_exec_gen_fp_nav_mode_params.
    all: try by []. simpl_trace. }

  (* XYZ *)
  { step_seq. apply exec_arbitrary_fun_call; simpl_eqparam. all: try by [].
   simpl_eqparam. step_skip_seq. apply eq_exec_gen_fp_nav_mode_params.
    by []. }

    (* Circle *)
  { step_seq. apply eq_exec_gen_fp_nav_mode_params. step_skip_seq.
    apply eq_exec_gen_fun_call. all: try by [].
    2: { rewrite /gen_fp_circle_sem //= trace_app //=. }
    simpl_eqparam_ex (get_circle_wp params). }

  (* Stay *)
  { step_seq. apply eq_exec_gen_fp_hmode. step_skip_seq.
    apply eq_exec_gen_fp_nav_mode_params.
    all: try by []. simpl_trace. }

  (* Follow *)
  { apply eq_exec_gen_fun_call; simpl_eqparam. }

  (* Rectangle *)
  { apply exec_arbitrary_fun_call. 
    simpl_eqparam_ex (get_srect_wp1 params);
      simpl_eqparam_ex (get_srect_wp2 params). }

  (* Eight *)
  { step_seq. apply eq_exec_gen_fp_nav_mode_params. step_skip_seq.
    apply exec_arbitrary_fun_call. all: try by [].
    2: { rewrite /gen_fp_circle_sem //= trace_app //=. }
    simpl_eqparam_ex (get_eight_center params);
      simpl_eqparam_ex (get_eight_turn_around params). }

  (* Oval *)
  { step_seq. apply eq_exec_gen_fp_nav_mode_params. step_skip_seq.
    apply exec_arbitrary_fun_call. all: try by [].
    2: { rewrite /gen_fp_circle_sem //= trace_app //=. }
    simpl_eqparam_ex (get_oval_p1 params);
      simpl_eqparam_ex (get_oval_p2 params). }

  (* Guided *)
  { apply eq_exec_gen_fun_call; simpl_eqparam. }

  (* Home *)
  { apply exec_arbitrary_fun_call. by []. }
Qed.
