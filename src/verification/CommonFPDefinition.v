From VFP Require Import BasicTypes ClightGeneration TmpVariables.
From GEN Require CommonFP.

From Coq Require Import Setoids.Setoid.

From compcert Require Import Coqlib AST Ctypes
                             Cop Clight Clightdefs.
From Coq.PArith Require Import BinPos.

Set Warnings "-parsing".
From mathcomp Require Import all_ssreflect.
Set Warnings "parsing".
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Clightdefs.ClightNotations.
Local Open Scope nat_scope.
Local Open Scope string_scope.
Local Open Scope clight_scope.

(** * Definition of expression from the CommonFP *)

Definition initFunct := gen_call_void_fun CommonFP._init_function.
Definition nextBlock := gen_call_void_fun CommonFP._NextBlock.
Definition nextStage := gen_call_void_fun CommonFP._NextStage.
Definition nextStageAndBreak :=  nextStage @+@ Sbreak.
Definition last_wp_var_str := "last_wp".
Definition last_wp_var := #last_wp_var_str.
Definition nextStageAndBreakFrom (wp: wp_id) :=
  (Sassign (gen_uint8_var last_wp_var) (gen_uint8_const wp))
  @+@ nextStageAndBreak.

Definition tmp_nav_block := create_ident tmp_nav_block_str.
Definition tmpNavBlock_stmt :=
  Scall
    (Some tmp_nav_block)
    (Evar CommonFP._get_nav_block
      (Tfunction Tnil tuchar cc_default)
    )
    nil.
Definition tmpNavBlock := gen_uint8_tempvar tmp_nav_block.

Definition tmp_nav_stage := create_ident tmp_nav_stage_str.
Definition tmpNavStage_stmt :=
  Scall
    (Some tmp_nav_stage)
    (Evar CommonFP._get_nav_stage
      (Tfunction Tnil tuchar cc_default)
    )
    nil.
Definition tmpNavStage := gen_uint8_tempvar tmp_nav_stage.

(** Generate the statement: set_nav_block(id_block); *)
Definition setNavBlock (id_block: nat) :=
  gen_call_iparams_fun CommonFP._set_nav_block id_block.

(** Generate the statement: set_nav_stage(id_stage); *)
Definition setNavStage (id_stage: nat) :=
  gen_call_iparams_fun CommonFP._set_nav_stage id_stage.

(** Generate a var corresponding to the orientation *)
Definition convertOrientation_str (o: orientation): string :=
  match o with
  | NS => "NS"
  | WE => "WE"
  end.

(** Convert an orientation into a expression *)
Definition convertOrientation (o: orientation): expr :=
  parse_c_code_uint8 (convertOrientation_str o).

Definition init_stage_str := "nav_init_stage".
Definition init_stage := C_CODE init_stage_str.

Definition block_time_str := "block_time".
Definition reset_time := C_CODE  (block_time_str ++ " = 0")%string.
