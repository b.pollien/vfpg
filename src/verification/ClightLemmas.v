From Coq Require Import Nat Arith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

From compcert Require Import Coqlib Integers AST Ctypes
                        Cop Clight Clightdefs Maps
                        Events.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas CommonStringLemmas
                        BasicTypes
                        FPNavigationMode FPNavigationModeSem
                        FlightPlanGeneric FlightPlanExtended
                        FlightPlanSized
                        CommonFPDefinition ClightGeneration
                        Generator GeneratorProperties
                        FPEnvironmentClight.
Local Open Scope nat_scope.


Set Implicit Arguments.

(** Tactics to execute Clight semantics *)

Ltac step_seq :=
  eapply Smallstep.star_left;
    [apply step_seq|eapply Smallstep.star_trans | ].

Ltac step_skip_seq :=
  eapply Smallstep.star_left; [apply step_skip_seq | |].

Ltac star_step H :=
  eapply Smallstep.star_left; [eapply H| | ].

Ltac simpl_trace := rewrite ?E0_left ?E0_right -?trace_app //=.

Ltac step_refl := apply Smallstep.star_refl.
Ltac step_trans := eapply Smallstep.star_trans.

(** * Lemmas about Clight properties *)

(** Evaluation of bool const *)
Lemma eval_expr_const_bool:
  forall ge e le m b,
    eval_expr ge e le m (gen_bool_const b) (create_bool_val b).
Proof.
  move => ge e le m b.
  econstructor; try constructor.
  by destruct b.
Qed.

(** Find symbol deterministic *)
Lemma find_symbol_genv:
forall (ge: genv) p_var b b',
  Globalenvs.Genv.find_symbol ge p_var = Some b
  -> Globalenvs.Genv.find_symbol ge p_var
       = Some b'
  -> b = b'.
Proof.
move => ge p_var b b' Hfs Hfs'.
rewrite Hfs in Hfs'. by inversion Hfs'.
Qed.

(** Evaluation of neg bool const *)
Lemma eval_expr_neg_const_bool:
  forall ge e le m b,
  eval_expr ge e le m (gen_neg_bool (gen_bool_const b))
                           (create_bool_val (~~ b)).
Proof.
  move => ge e le m b.
  econstructor. apply eval_expr_const_bool.
  by destruct b.
Qed.

(** Evaluation of bool const *)
Lemma eval_expr_const_uint8:
  forall ge e le m v,
    eval_expr ge e le m (gen_uint8_const v) (create_val v).
Proof.
  move => ge e le m v.
  econstructor; try constructor.
Qed.

(** Evaluation of bool tmp_var *)
Lemma eval_expr_tmp_bool:
  forall ge e le m var b,
      eval_expr ge e (PTree.set var (create_bool_val b) le)
                                            m (gen_bool_tempvar var)
                                            (create_bool_val b).
Proof.
  move => ge e le m b.
  econstructor; try constructor.
  apply PTree.gss.
Qed.

(** Evaluation of uint8 tmp_var *)
Lemma eval_expr_tmp_uint8:
  forall ge e le m var v,
      eval_expr ge e (PTree.set var (create_val v) le)
                                            m (Etempvar var tuchar)
                                            (create_val v).
Proof.
  move => ge e le m v.
  econstructor; try constructor.
  apply PTree.gss.
Qed.

Remark false_eq_0:
  Int.eq (int_of_bool false) Int.zero = true.
Proof. by []. Qed. 

Remark true_ne_0:
  Int.eq (int_of_bool true) Int.zero = false.
Proof. by []. Qed. 

(** Execution of the arbitrary C code *)

(* Properties that is satisfied when then optid corresponds to the result
    type of the function*)
Definition match_optid_tres (optid: option ident) (tres: type): Prop :=
  match optid with
  | Some _ => tres <> tvoid
  | None => tres = tvoid
  end.

(* Property describing the [expr] can be casted to the type [tres]*)
Definition cast_possible (e: expr) (tres: type): Prop :=
  (tres = typeof e)
  \/ (exists v, forall ge env le m,
          eval_expr ge env le m e v
          /\ sem_cast v (typeof e) tres m = Some v).

Remark cast_possible_eq:
  forall tres e,
    tres = typeof e
    -> cast_possible e tres.
Proof. intros; by left. Qed.

(* Properties that is satisfied when there is the same number of parameter
*)
Fixpoint match_param (e: expr) (p: string): Prop :=
  match e with
  | Evar ident _ => ident = ## p
  | Etempvar ident _ => ident = # p
  | Econst_int v _ =>
      exists i, int_of_nat i = v /\ string_of_nat i = p
  | Econst_float v _
      => exists i, float_of_nat i = v /\ string_of_nat i = p
  | Ebinop Omul e1 e2 _ =>
    exists s1 s2, p = (s1 ++ "*" ++ s2)%string
                  /\ match_param e1 s1 /\ match_param e2 s2
  | _ => False
  end.

(** Properties about the matching a list of params *)
Fixpoint match_params (ltypes: typelist) (e_p: list expr)
                              (p: list string): Prop :=
  match ltypes, e_p, p with
  | Tnil, [::], [::] => True
  | Tcons t lt, e :: le, p :: lp => 
    (cast_possible e t)
    /\ (match_param e p)
    /\ match_params lt le lp
  | _, _, _ => False
  end.

Definition match_optparams (ltypes: typelist) (e_p: list expr)
                                (p: option (list string)): Prop :=
  match ltypes, e_p, p with
  | Tnil, [::], None => True
  | _, _, Some params => match_params ltypes e_p params
  | _, _, _ => False
  end.

Ltac simpl_eqparam :=
  rewrite //=; repeat (split; try by []; 
    try by apply cast_possible_eq); try by apply cast_possible_eq.

Ltac simpl_eqparam_ex e :=
  simpl_eqparam; try by exists e.

(** Axiom for the execution of arbitrary C code using Scall stmt. The
  axiom states that if there is a call of arbitrary C code, then it can be
  converted into a trace. *)
Axiom step_arbitrary_call_gen:
  forall (ge: genv) f func k e le m tres ltypes e_p optparams optid optvar,
  match_optparams ltypes e_p optparams
  -> (exists var, optid = Some (# var) /\ optvar = Some var
                                      /\ tres <> tvoid)
      \/ (optid = None /\ optvar = None)
  -> step2 ge
      (State f
        (Scall optid (Evar ##func (Tfunction ltypes tres cc_default)) e_p)
          k e le m)
          [:: (code_event_optassign optvar func optparams)]
      (State f Sskip k e le m).

Lemma exec_arbitrary_call_gen:
  forall (ge: genv) f func k e le m tres ltypes e_p optparams optid optvar,
  match_optparams ltypes e_p optparams
  -> (exists var, optid = Some (# var) /\ optvar = Some var
                                                        /\ tres <> tvoid)
      \/ (optid = None /\ optvar = None)
  -> Smallstep.star step2 ge
      (State f
        (Scall optid (Evar ##func (Tfunction ltypes tres cc_default)) e_p)
          k e le m)
          [:: (code_event_optassign optvar func optparams)]
      (State f Sskip k e le m).
Proof.
  intros; apply Smallstep.star_one.
  by apply step_arbitrary_call_gen.
Qed.

(* Lemma to execute arbitrary C code statement*)
Lemma exec_arbitrary_C_code:
  forall (ge: genv) f func k e le m,
  Smallstep.star step2 ge
      (State f (Scall None (gen_void_fun ## func) [::]) k e le m)
          [:: (code_event func)]
      (State f Sskip k e le m).
Proof.
  move => ge f func k e le m.
  have H := (exec_arbitrary_call_gen ge f func k e le m)
                  tvoid Tnil [::] None None None.
  apply H; try by []; try by right.
Qed.

(* Lemma to execute arbitrary C code with assignation *)

Remark match_optparams_impl_params:
  forall ltypes e_p params,
    match_params ltypes e_p params
    -> match_optparams ltypes e_p (Some params).
Proof.
  move => [|ltype ltypes] [|expr expr_p] params H; try by [].
Qed.

Lemma exec_arbitrary_fun_call:
  forall ge f func k e le m tres ltypes e_p params,
  match_params ltypes e_p params
  -> Smallstep.star step2 ge
      (State f
          (Scall None (Evar ##func (Tfunction ltypes tres cc_default)) e_p)
          k e le m)
          [:: (code_event (gen_fun_call_sem func params))]
      (State f Sskip k e le m).
Proof.
  move => ge f func k e le m tres ltypes e_p params H.
  replace (code_event _ )
    with (code_event_optassign None func (Some params)).
  apply (exec_arbitrary_call_gen ge f func k e le m).
  - apply (match_optparams_impl_params H).
  - by right.
  - by [].
Qed.
 
Lemma exec_arbitrary_fun_call_res:
  forall ge f func k e le m tres ltypes e_p params var,
  tres <> tvoid
  -> match_params ltypes e_p params
  -> Smallstep.star step2 ge
      (State f
          (Scall (Some (#var))
             (Evar ##func (Tfunction ltypes tres cc_default)) e_p)
          k e le m)
          [:: (code_event_assign var (gen_fun_call_sem func params))]
      (State f Sskip k e le m).
Proof.
  move => ge f func k e le m tres ltypes e_p params var Hr Hp.
  replace (code_event_assign _ _)
    with (code_event_optassign (Some var) func (Some params)).
  apply exec_arbitrary_call_gen; try by [].
  - apply (match_optparams_impl_params Hp).
  - left. by exists var.
  - by [].
Qed.

(** Axiom for the execution of arbitrary C code using Sassign stmt. The
  axiom states that if there is an assign of arbitrary C code,
  then it can be converted into a trace. *)
Axiom step_arbitrary_assign_C_code:
  forall ge f k e le m var value type expr,
  match_params (Tcons type Tnil) [:: expr] [:: value]
  -> step2 ge
      (State f (Sassign (Evar #var type) expr)
          k e le m)
          [:: (code_event_optassign (Some var) value None)]
      (State f Sskip k e le m).

Lemma exec_arbitrary_assign_C_code:
  forall ge f k e le m var value type expr,
  match_params (Tcons type Tnil) [:: expr] [:: value]
  -> Smallstep.star step2 ge
      (State f (Sassign (Evar #var type) expr)
          k e le m)
          [:: (code_event_optassign (Some var) value None)]
      (State f Sskip k e le m).
Proof.
  intros. apply Smallstep.star_one.
  by apply step_arbitrary_assign_C_code.
Qed.

Lemma exec_arbitrary_assign_from_parse:
  forall ge f params k e le m,
  Smallstep.star step2 ge
    (State f (Sassign (gen_int_var_str (get_set_var params))
                          (parse_c_code_int (get_set_value params)))
        k e le m)
        [:: (code_event_assign_set params)]
    (State f Sskip k e le m).
Proof.
  move => ge f [var value] k e le m.
  apply exec_arbitrary_assign_C_code.
  split; try by []; by apply cast_possible_eq.
Qed.

Lemma eq_exec_assign_time:
  forall (ge: genv) f k e le m,
    Smallstep.star step2 ge
        (State f
          (Sassign (Evar CommonFP._block_time tushort)
              (Econst_int (Int.repr 0) tint))
            k e le m)
        (fp_trace_to_trace[:: reset_time])
      (State f Sskip k e le m).
Proof.
  move => ge f k e le m.
  replace (fp_trace_to_trace [:: reset_time])
    with [::(code_event_optassign (Some block_time_str)
                                        (string_of_nat 0) None)].
  apply exec_arbitrary_assign_C_code.
  - rewrite //= /cast_possible; split; try by []; try right.
    * eexists; intros; split; econstructor.
    * split; try left; try by []. by exists 0.
  - by [].
Qed.


(** * Lemmas about find labels *)

Section FIND_LABELS.

  (** Flight Plan Extended *)
  Variable fp: FP_E_WF.flight_plan_wf.
  Definition fpe: FP_E_WF.flight_plan := proj1_sig fp.
  Definition Hwf := proj2_sig fp.

  (** Function to execute*)
  Definition f := gen_fp_auto_nav fpe.

  (** ** Continuation for execution of the fligth plan *)

  Fixpoint blocks_to_stmt (blocks: list FP_E.fp_block): statement :=
    match blocks with
    | nil => Sskip
    | block :: blocks => Ssequence (gen_fp_block block)
                                   (blocks_to_stmt blocks)
    end.

  Definition blocks_cont_gen (blocks: list FP_E.fp_block) (k: cont): cont :=
  (Kseq (blocks_to_stmt blocks)) (Kswitch k).

  Definition blocks_cont (idb: block_id) (k: cont): cont :=
  blocks_cont_gen
    (drop idb.+1 ((FP_E.get_fp_blocks fpe)
      ++ (FP_E_WF.get_default_block fpe :: nil))) k.

  Definition b_cont_gen (block: FP_E.fp_block) (k:cont): cont :=
  let post' := FP_E_WF.get_block_post_call block in
  (Kseq (parse_c_code_option post' @+@ Sbreak) k).

  Definition b_cont (idb: block_id) (k:cont): cont :=
  b_cont_gen (FP_E.get_block fpe idb) k.

  Definition block_cont (idb: block_id) (k:cont): cont :=
  b_cont idb (blocks_cont idb k).

  Definition stage_cont_gen (block: FP_E.fp_block)
                                (blocks: list FP_E.fp_block)
                                (k: cont): cont :=
  Kswitch (b_cont_gen block (blocks_cont_gen blocks k)).

  Definition stage_cont (idb: block_id) (k:cont): cont :=
  Kswitch (block_cont idb k).

  (** ** Function that return None *)

  Lemma no_label_exceptions:
    forall label k exs, find_label label (gen_exceptions exs) k = None.
  Proof.
    move => label k; induction exs as [|ex exs IHexs]; try by [].

    destruct ex as [cond idb [code|]];
      by rewrite gen_exceptions_cons //= IHexs.
  Qed.

  Lemma no_label_parse_c_code_option:
    forall label k code,
      find_label label (parse_c_code_option code) k = None.
  Proof. 
    by move => label k [code|].
  Qed. 

  Lemma no_label_call:
    forall label params k,
      find_label label (gen_fp_call params) k = None.
  Proof. 
    by move => label [f [cond|] [] []] k.
  Qed.

  Lemma no_label_nav_init:
    forall label mode k,
        find_label label (FPNavigationModeGen.gen_fp_nav_init_call mode) k
          = None.
  Proof. 
    move => label mode k.

    destruct mode; try by [].

    all: try by destruct params_call as [t [p|] [pre|] [post|]].
    all: try by destruct params_mode.
  Qed.

  Lemma no_label_nav_pre_call:
      forall label mode k,
        find_label label (FPNavigationModeGen.gen_fp_nav_pre_call mode) k
          = None.
  Proof. 
    move => label mode k.

    destruct mode; try by [].

    all: try by destruct params_call as [t [p|] [pre|] [post|]].
    all: try by destruct params_mode.
  Qed.

  Lemma no_label_nav_until:
    forall label mode until k,
        find_label label ( FPNavigationModeGen.gen_fp_nav_until mode until)
        k = None.
  Proof.
    move => label mode until k.

    destruct until.

    all: destruct mode; try by [].

    all: try by destruct params_call as [t [p|] [pre|] [post|]].
  Qed.

  Lemma no_label_nav_post_call:
    forall label mode k,
      find_label label ( FPNavigationModeGen.gen_fp_nav_post_call mode) k
        = None.
  Proof.
    move => label mode k.

    destruct mode; try by [].

    all: try by destruct params_call as [t [p|] [pre|] [post|]].
  Qed.

  Lemma seq_lstmt_to_stmt:
    forall blocks default,
      seq_of_labeled_statement (gen_fp_blocks blocks default)
        = blocks_to_stmt (blocks ++ [:: default]).
  Proof.
    induction blocks as [|block blocks IHb]; move => default //.
    by rewrite /= IHb.
  Qed.

Ltac simpl_mode params_call  params_mode:=
    destruct params_call as [[] [p|] [pre|] [post|]];
      destruct params_mode as [pitch vmode];
      destruct pitch;
      destruct vmode as [c |alt| |wp lwp|t| ]; try by [];
      try destruct alt; try by [].

  Lemma no_label_nav_cond:
    forall label mode k,
      find_label label (FPNavigationModeGen.gen_fp_nav_cond mode) k = None.
  Proof.
    move => label mode k.

    destruct mode eqn:Hmode; try by [].

    (* Heading *)
    simpl_mode params_call params_mode.

    (* Attitude *)
    simpl_mode params_call params_mode.

    (* Manual *)
    simpl_mode params_call params_mode.

    (* Go *)
    { rewrite /FPNavigationModeGen.gen_fp_nav_cond
                /FPNavigationModeGen.nav_cond_stmt.
       destruct (get_go_from params).
       all: try rewrite //= no_label_nav_post_call
                          /FPNavigationModeGen.gen_fp_hmode
                          /FPNavigationModeGen.gen_fun_call_stmt.
       all: destruct (get_go_hmode params).
       all: destruct (get_nav_nav_params params_call).
       all: destruct (get_nav_pitch params_mode).
       all: destruct (get_nav_vmode params_mode)
              as [c |alt| |wp' lwp|t| ]; try by [].
       all: destruct alt; try by []. }

    (* XYZ *)
    simpl_mode params_call params_mode.

    (* Circle *)
    simpl_mode params_call params_mode.

    (* Stay *)
    { rewrite /FPNavigationModeGen.gen_fp_nav_code
                /FPNavigationModeGen.gen_fp_nav_cond
                /FPNavigationModeGen.nav_cond_stmt
                /FPNavigationModeGen.gen_fp_nav_code
                /FPNavigationModeGen.gen_fp_stay
                /FPNavigationModeGen.gen_fp_hmode.
      destruct (get_stay_hmode params).
      all: rewrite //= /FPNavigationModeGen.gen_fun_call_stmt.
      all: destruct (get_nav_nav_params params_call).
      all: destruct (get_nav_pitch params_mode).
      all: destruct (get_nav_vmode params_mode)
              as [c |alt| |wp' lwp|t| ]; try by [].
       all: try destruct alt; try by []. }

    (* Follow *)
    {  rewrite /FPNavigationModeGen.gen_fp_nav_cond
                /FPNavigationModeGen.nav_cond_stmt
                /FPNavigationModeGen.gen_fp_nav_code
                /FPNavigationModeGen.gen_fp_follow
                /FPNavigationModeGen.gen_fun_call_stmt.
        destruct (get_nav_nav_params params_call); try by []. }

    (* Eight *)
    simpl_mode params_call params_mode.

    (* Oval *)
    simpl_mode params_call params_mode.

    (* Guided *)
    {  rewrite /FPNavigationModeGen.gen_fp_nav_cond
                /FPNavigationModeGen.nav_cond_stmt
                /FPNavigationModeGen.gen_fp_nav_code
                /FPNavigationModeGen.gen_fp_guided
                /FPNavigationModeGen.gen_fun_call_stmt.
            destruct (get_nav_nav_params params_call); try by []. }
  Qed.

  (** ** Lemmas specific to while label *)
  Lemma find_while_label_block_ne_generic:
    forall stages idb d params k default,
      let l_while := get_while_label params in
      FP_E.get_while_block_id params <> idb
      -> (forall ids ids' params', 
              List.nth ids stages default
                  = FP_E.WHILE ids' params'
            -> FP_E.get_while_block_id params' = idb
                /\ FP_E.get_while_id params' = d + ids)
      -> find_label_ls l_while (gen_fp_stages stages) k = None.
  Proof.
    induction stages as [|stage stages IHs]; try by [].
    move => idb d params k default l_while Hparams Hget.

    have Hrec:
      forall ids ids' params',
          List.nth ids stages default = FP_E.WHILE ids' params'
          -> FP_E.get_while_block_id params' = idb
            /\ FP_E.get_while_id params' = d.+1 + ids.
    { move => ids ids' params' Hnth.
      have H := Hget (ids.+1) ids' params'.
      destruct H as [H H']; try by []; split; try by [].
        rewrite H'; ssrlia.  }

    destruct stage; rewrite //=.

    (* WHILE *)
    { have H:= Hget 0 id params0. destruct H as [H H']; try by [].
      have Hneg := Hparams. rewrite -H in Hparams.   
      destruct ident_eq as [Heq | Hne]; subst l_while.
      - by apply gen_while_label_ne_block in Hparams.
      - apply (IHs _ _ _ _ _ Hneg Hrec). }

    (* END WHILE *)
    { destruct ident_eq as [Heq | Hne].
      - by apply gen_while_ne_end in Heq. 
      - apply (IHs _ _ _ _ _ Hparams Hrec). }

    all: try apply (IHs _ _ _ _ _ Hparams Hrec).

    (* CALL *)
    rewrite no_label_call.
    apply (IHs _ _ _ _ _ Hparams Hrec).

    (* NAV INIT *)
    rewrite no_label_nav_init.
    apply (IHs _ _ _ _ _ Hparams Hrec).

    (* NAV *)
    rewrite no_label_nav_pre_call
              no_label_nav_cond
              no_label_nav_until
              no_label_nav_post_call.
    apply (IHs _ _ _ _ _ Hparams Hrec).
  Qed.

  Lemma find_while_label_block_ne:
    forall  block idb params k,
      let l_while := get_while_label params in
      FP_E.get_while_block_id params <> idb
      -> (forall ids ids' params', 
              let stages := (FP_E.get_block_stages block) in
              let default :=  (FP_E.DEFAULT
                                (Datatypes.length stages - 1)%coq_nat) in
              List.nth ids stages default = FP_E.WHILE ids' params'
            -> FP_E.get_while_block_id params' = idb
                /\ FP_E.get_while_id params' = ids)
      -> find_label_ls l_while
            (gen_fp_stages (FP_E.get_block_stages block)) k = None.
  Proof.
    move => block idb params k l_while Hparams Hget.

    eapply find_while_label_block_ne_generic with (d := 0).
    apply Hparams.
    intros. rewrite plus0n. eapply Hget. apply H.
  Qed.

  Definition res_while_label block blocks stages params k:=
     Some (
      setNavStage (FP_E.get_while_id params),
        Kseq
         (Sifthenelse 
            (gen_neg_bool (parse_c_code_cond (FP_E.get_while_cond params)))
            (Sgoto (get_end_while_label params))
              nextStageAndBreak)
        (Kseq
          (seq_of_labeled_statement (gen_fp_stages stages))
          (stage_cont_gen block blocks k))).

  Lemma find_while_label_block_eq_gen:
  forall block blocks stages idb ids params d k idd,
    let l_while := get_while_label params in
    let default :=  (FP_E.DEFAULT idd) in
      FP_E.get_while_block_id params = idb
      -> ids >= d
      ->  List.nth (ids - d) stages default = FP_E.WHILE ids params
      ->  (forall ids ids' params,
            List.nth ids stages default = FP_E.WHILE ids' params
            -> d + ids = FP_E_WF.get_while_id params
                /\ d + ids = ids'
                /\  idb =  FP_E_WF.get_while_block_id params)
      -> is_call_cont k
      -> find_label_ls l_while (gen_fp_stages stages) 
                                   (stage_cont_gen block blocks k)
             = res_while_label block blocks (drop (ids - d).+1 stages)
                                     params k.
  Proof.
    move => block blocks; induction stages as [|stage stages IHs];
      move => idb ids params d k idd l_while default Hp
                 Hids Hget Hwhile Hk.
    destruct (ids - d); rewrite //= in Hget.

    destruct (Nat.eq_dec (ids - d) 0) as [Heq | Hne].
    - destruct (Hwhile _ _ _ Hget) as [Hp' [Hids' Hidb]].
      rewrite Heq //= in Hget.
      rewrite /res_while_label Hget /l_while //= -Hp' Hids' Heq drop0.
      destruct ident_eq; try by [].
    - have Hd: (ids - d).-1 = ids -d.+1.
      {to_nat Hne; ssrlia. }
      rewrite reduce_nth in Hget; try by [].
      rewrite Hd in Hget.

      have Hids': d.+1 <= ids. {to_nat Hne; ssrlia. }

      have Hwhile': forall ids' ids'' params',
                List.nth ids' stages (FP_E.DEFAULT idd)
                    = FP_E.WHILE ids'' params'
                -> d.+1 + ids' = FP_E_WF.get_while_id params'
                    /\ d.+1 + ids' = ids''
                    /\ idb = FP_E_WF.get_while_block_id params'.
      { move => ids' ids'' params' Hnth.
         have Hw := Hwhile ids'.+1 ids'' params'.
         rewrite //= in Hw. destruct (Hw Hnth) as [Hp' [Hi' Hidb']].
         rewrite -Hp' -Hi' Hidb'.
         split; try by []; ssrlia. }

      replace (ids - d) with ((ids - d.+1).+1); last first.
        to_nat Hids'. ssrlia.
     
      destruct stage; rewrite //=.

    (* WHILE *)
    assert(H: get_while_label params <> get_while_label params0).
    { apply gen_while_label_ne_stage.
      destruct (Hwhile 0 id params0) as [Hp0 [Hp0' Hp0'']]; try by [].
      destruct (Hwhile' _ _ _ Hget) as [Hp1 [Hp1' Hp1'']].
      rewrite -Hp0 -Hp1. ssrlia. }
    rewrite /l_while. destruct (ident_eq) as [Heq' | Hne']; try by [].
    by apply (IHs _ _ _ _ _ _ Hp Hids' Hget Hwhile').

    (* END WHILE *)
    destruct ident_eq as [Heq | Hne'].
      by apply gen_while_ne_end in Heq.

    all: try by apply (IHs _ _ _ _ _ _ Hp Hids' Hget Hwhile'). 

    (* CALL *)
    rewrite no_label_call.
    by apply (IHs _ _ _ _ _ _ Hp Hids' Hget Hwhile').
    
    (* NAV INIT *)
    rewrite no_label_nav_init.
    by apply (IHs _ _ _ _ _ _ Hp Hids' Hget Hwhile'). 

    (* NAV *)
    rewrite no_label_nav_pre_call
              no_label_nav_cond
              no_label_nav_until
              no_label_nav_post_call.
    by apply (IHs _ _ _ _ _ _ Hp Hids' Hget Hwhile'). 
  Qed.

  Lemma find_while_label_block_eq:
  forall block blocks stages idb ids params k,
    let l_while := get_while_label params in
    let default :=  (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat) in
      FP_E.get_while_block_id params = idb
      ->  List.nth ids stages default = FP_E.WHILE ids params
      ->  (forall ids ids' params,
            List.nth ids stages default = FP_E.WHILE ids' params
            -> ids = FP_E_WF.get_while_id params
                /\ ids = ids'
                /\  idb
                       =  FP_E_WF.get_while_block_id params)
      -> is_call_cont k
      -> find_label_ls l_while (gen_fp_stages stages)
                                   (stage_cont_gen block blocks k)
          = res_while_label block blocks (drop ids.+1 stages) params k.
  Proof.
    move => block blocks stages idb ids params k l_while
                default Hidb Hnth Hwhile Hk.
    have Hids0: ids = (ids - 0)%coq_nat by ssrlia.

    rewrite Hids0; eapply (find_while_label_block_eq_gen _ _ Hidb);
      try ssrlia.

    rewrite -Hids0. apply Hnth.

    apply Hwhile.
  Qed.

  Lemma find_while_label_gen:
  forall blocks idb ids d params k,
    let l_while := get_while_label params in
    let l_wend := get_end_while_label params in
    let block :=
      List.nth (idb - d) blocks (FP_E_WF.get_default_block fpe) in
    let default_block := FP_E_WF.get_default_block fpe in
    let stages := FP_E_WF.get_block_stages block in
    let default :=  (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat) in
      (idb - d < Datatypes.length blocks)%coq_nat
      -> (idb >= d)%coq_nat
      ->  List.nth ids stages default = FP_E.WHILE ids params
      -> (forall idb block,
              (idb < Datatypes.length blocks)%coq_nat
              -> List.nth idb blocks (FP_E_WF.get_default_block fpe)
                     = block
              -> FP_E_WF.get_block_id block = d + idb)
      ->  (forall idb ids ids' params,
            let block := List.nth idb blocks
                                (FP_E_WF.get_default_block fpe) in
            let stages := FP_E_WF.get_block_stages block in
            let default := 
                (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat) in
            List.nth ids stages default = FP_E.WHILE ids' params
            -> ids = FP_E_WF.get_while_id params
                /\ ids = ids'
                /\  FP_E_WF.get_block_id block
                       =  FP_E_WF.get_while_block_id params)
      -> is_call_cont k
      -> find_label_ls
            l_while (gen_fp_blocks blocks default_block) (Kswitch k)
          = res_while_label block
              (drop (idb.+1 - d) (blocks ++ [::default_block]))
                                       (drop ids.+1 stages) params k.
  Proof.
    induction blocks as [|block blocks IHb];
      move => idb ids d params k l_while l_wend B default_block
              stages default Hlen Hd Hget Hsize Hwhile Hk.
    rewrite //= in Hlen. ssrlia.

    rewrite //= ?no_label_parse_c_code_option
                    ?no_label_exceptions.

    have Hnth: List.nth (idb - d) (block :: blocks)
                                    (FP_E_WF.get_default_block fpe) = B.
    { by rewrite /B. }

    destruct (Nat.eq_dec (idb - d) 0) as [Heq | Hne].
    - destruct (Hwhile _ _ _ _ Hget) as [Hids [Heq' Hp]].
      rewrite Hnth in Hp.
      apply Hsize in Hnth; last first. rewrite Heq; ssrlia.
      rewrite Hnth in Hp. symmetry in Hp.
      replace (d + (idb - d)) with idb in Hp; last first.
      to_nat Heq; ssrlia.

      have Hwhile':
        forall ids ids' params,
          List.nth ids stages
            (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat)
              = FP_E.WHILE ids' params
          -> ids = FP_E_WF.get_while_id params
            /\ ids = ids' /\ idb = FP_E_WF.get_while_block_id params.
      { move => ids' ids'' params' Hnth'.
        destruct (Hwhile _ _ _ _ Hnth') as [Hp' [Hids' Hn]];
        repeat (split; try by []).
        rewrite Hnth in Hn. rewrite -Hn. ssrlia. }

      have Hdrop: drop (idb.+1 - d)
          (block :: blocks ++ [:: default_block])
            = blocks ++ [:: default_block].
      { replace (idb.+1 - d) with ((idb - d).+1).
         by rewrite Heq //= drop0. ssrlia. }

      have Hs := find_while_label_block_eq block
                        (blocks ++ [:: default_block]) Hp Hget Hwhile' Hk.
      rewrite /stages /B Heq /stage_cont_gen
              /b_cont_gen /blocks_cont_gen //= in Hs.

      by rewrite /l_while /stages /B Heq /default_block //=
        seq_lstmt_to_stmt Hs Hdrop.
    - destruct (Hwhile _ _ _ _ Hget) as [Hids [Heq Hp]].
      have Hd': idb > d. to_nat Hne. ssrlia.

      apply Hsize in Hnth; last first. to_nat Hne.
      replace (d + (idb - d)) with idb in Hnth; last first.
      to_nat Hd'. ssrlia.
      have Hne': idb <> d. to_nat Hd'. ssrlia.
      rewrite -Hnth /B Hp in Hne'.
      rewrite (find_while_label_block_ne _ Hne'); last first.
      { move => ids' ids'' params' stages' default' Hnth'.
        have Hb: List.nth 0 (block :: blocks)
                                    (FP_E_WF.get_default_block fpe)
                                = block. by [].
        rewrite /stages' -Hb in Hnth'.
        destruct (Hwhile _ _ _ _ Hnth') as [Hids' [Hid Hget']];
          subst ids''.
        have Hlen' : (0 < Datatypes.length (block::blocks))%coq_nat.
          ssrlia.
        have H:= Hsize _ _  Hlen' Hb. rewrite //= H in Hget'.
        split; try by []. rewrite -Hget'; ssrlia. }
      rewrite /stages /B reduce_nth /l_while ; try by []. 
      replace ((idb - d).-1) with (idb - d.+1); last first.
          to_nat Hd'; ssrlia.
      rewrite (IHb idb ids d.+1); try by [].
      * rewrite reduce_drop; try ssrlia.
          replace (((idb.+1 - d)%coq_nat - 1)%coq_nat)
            with ((idb.+1 - d.+1)%coq_nat); last first.
            to_nat Hd'; destruct d; ssrlia. by [].
      * rewrite //=  in Hlen.
        have H: (idb - d.+1 < (Datatypes.length blocks))%coq_nat.
        to_nat Hlen. to_nat Hd'. ssrlia. apply H.
      * to_nat Hd'; ssrlia.
      * rewrite /stages /default /stages /B reduce_nth  in Hget; try by [].
        replace ((idb - d).-1) with (idb - d.+1) in Hget; last first.
          to_nat Hd'; ssrlia. 
        apply Hget. 
      * move => idb' block' Hlen' Hnth'.
         replace (d.+1 + idb') with (d + idb'.+1); last first. ssrlia.
         apply Hsize. ssrlia. rewrite reduce_nth; try by [].
      * move => idb' ids' ids'' params' block' stages' default' Hnth'.
        have Hw := Hwhile idb'.+1 ids' ids'' params'.
        apply Hw. rewrite reduce_nth; try by [].
  Qed.

  Lemma find_while_label:
    forall idb ids ids' params k,
      let l_while := get_while_label params in
      let block :=
        List.nth idb (FP_E.get_fp_blocks fpe)
                      (FP_E_WF.get_default_block fpe) in
      let stages := FP_E_WF.get_block_stages block in
      FP_E_WF.wf_fp_e fpe
        -> verified_fp_e fpe
        -> (idb < FP_E.get_nb_blocks fpe - 1)%coq_nat
        -> FP_E.get_stage fpe idb ids = FP_E.WHILE ids' params
        -> is_call_cont k
        -> find_label l_while (fn_body f) k
              = Some (
                (setNavStage (FP_E.get_while_id params)),
              Kseq
                (Sifthenelse
                    (gen_neg_bool (parse_c_code_cond
                                  (FP_E.get_while_cond params)))
                    (Sgoto (get_end_while_label params))
                      nextStageAndBreak)
                (Kseq
                  (seq_of_labeled_statement
                      (gen_fp_stages (drop ids.+1 stages)))
                        (stage_cont idb k))).
  Proof.
    move => idb ids ids' params k l_while block stages
                Hwf Hsize Hlen Hget Hk.

    replace (find_label _ _ _)
      with (find_label_ls l_while
              (gen_fp_blocks (FP_E.get_fp_blocks fpe)
                             (FP_E_WF.get_default_block fpe))
              (Kswitch k)); last first.
    rewrite //= 
             no_label_exceptions (* Global exceptions *)
             ?no_label_parse_c_code_option (* Pre_call *)
             ?no_label_exceptions (* Local exceptions *)
             //=.

    have Hwhile := FP_E_WF.get_wf_while Hwf.
    rewrite /FP_E_WF.wf_while /FP_E_WF.get_stage
              /FP_E_WF.get_stages /FP_E_WF.get_block in Hwhile.

    have Hget' := Hget.
    apply Hwhile in Hget'; destruct Hget' 
      as [Hids_p [Hlt [Hend [Hids Hidb]]]]; subst ids'.

    rewrite /FP_E.get_stage /FP_E.get_stages
              /FP_E.default_stage /FP_E.default_stage_id in Hget.

    have H: (idb - 0 < Datatypes.length (FP_E.get_fp_blocks fpe))%coq_nat.
    rewrite /FP_E.get_nb_blocks in Hlen. to_nat Hlen. ssrlia.

    rewrite /stages /block. replace idb with (idb - 0); last first. ssrlia.

    rewrite /l_while /res_while_label.

    erewrite (find_while_label_gen H); try by [].
    - rewrite /stage_cont /block_cont /res_while_label.
      replace (idb - 0) with idb; last first. ssrlia. by [].
    - ssrlia.
    - replace (idb - 0) with idb; last first. ssrlia.
      apply Hget.
    - replace (0 + idb) with idb; last first. ssrlia.
      move => idb' block' Hlen' Hnth'. subst.
      rewrite (get_well_numbered_blocks Hsize); ssrlia.
      rewrite /FP_E.get_nb_blocks. ssrlia.

    move => idb' ids' ids'' params' block' stages' default Hget'.

    destruct (Hwhile _ _ _ _ Hget')
      as [Hids_p' [Hlt' [Hend' [Hids' Hidb']]]].

    split; try by [].
  Qed.

  (** ** Lemmas specific to end while label *)
  Lemma find_end_while_label_block_ne_generic:
    forall stages idb d params k default,
      let l_wend := get_end_while_label params in
      FP_E.get_while_block_id params <> idb
      -> (forall ids ids' params' body', 
              List.nth ids stages default
                  = FP_E.END_WHILE ids' params' body'
            -> FP_E.get_while_block_id params' = idb
                /\ FP_E.get_end_while_id params' = d + ids)
      -> find_label_ls l_wend (gen_fp_stages stages) k = None.
  Proof.
    induction stages as [|stage stages IHs]; try by [].
    move => idb d params k default l_wend Hparams Hget.

    have Hrec:
      forall ids ids' params' body',
          List.nth ids stages default = FP_E.END_WHILE ids' params' body'
          -> FP_E.get_while_block_id params' = idb
            /\ FP_E.get_end_while_id params' = d.+1 + ids.
    { move => ids ids' params' body' Hnth.
      have H := Hget (ids.+1) ids' params' body'.
      destruct H as [H H']; try by []; split; try by [].
        rewrite H'; ssrlia.  }

    destruct stage; rewrite //=.
    
    (* WHILE *)
    { destruct ident_eq as [Heq | Hne].
      - symmetry in Heq. by apply gen_while_ne_end in Heq. 
      - apply (IHs _ _ _ _ _ Hparams Hrec). }

    (* END WHILE *)
    { have H:= Hget 0 id params0 body.
      destruct H as [H H']; try by [].
      have Hneg := Hparams. rewrite -H in Hparams.   
      destruct ident_eq as [Heq | Hne]; subst l_wend.
      - by apply gen_end_while_label_ne_block in Hparams.
      - apply (IHs _ _ _ _ _ Hneg Hrec). }


    all: try apply (IHs _ _ _ _ _ Hparams Hrec).
    
    (* CALL *)
    rewrite no_label_call.
    apply (IHs _ _ _ _ _ Hparams Hrec).

    (* NAV INIT *)
    rewrite no_label_nav_init.
    apply (IHs _ _ _ _ _ Hparams Hrec).

    (* NAV *)
    rewrite no_label_nav_pre_call
              no_label_nav_cond
              no_label_nav_until
              no_label_nav_post_call.
    apply (IHs _ _ _ _ _ Hparams Hrec).
  Qed.

  Lemma find_end_while_label_block_ne:
    forall  block idb params k,
      let l_wend := get_end_while_label params in
      FP_E.get_while_block_id params <> idb
      -> (forall ids ids' params' body', 
              let stages := (FP_E.get_block_stages block) in
              let default :=  (FP_E.DEFAULT
                                (Datatypes.length stages - 1)%coq_nat) in
              List.nth ids stages default
                = FP_E.END_WHILE ids' params' body'
            -> FP_E.get_while_block_id params' = idb
                /\ FP_E.get_end_while_id params' = ids)
      -> find_label_ls l_wend
            (gen_fp_stages (FP_E.get_block_stages block)) k = None.
  Proof.
    move => block idb params k l_wend Hparams Hget.

    eapply find_end_while_label_block_ne_generic with (d := 0).
    apply Hparams.
    intros. rewrite plus0n. eapply Hget. apply H.
  Qed.

  Definition res_end_while_label block blocks stages params k:=
    Some ((setNavStage ((FP_E_WF.get_end_while_id params) + 1)),
      Kseq (seq_of_labeled_statement (gen_fp_stages stages))
        (stage_cont_gen block blocks k)).

  Lemma find_end_while_label_block_eq_gen:
    forall block blocks stages idb ids params d k idd body,
      let l_wend := get_end_while_label params in
      let default :=  (FP_E.DEFAULT idd) in
        FP_E.get_while_block_id params = idb
        -> ids >= d
        ->  List.nth (ids - d) stages default
               = FP_E.END_WHILE ids params body
        ->  (forall ids ids' params body,
              List.nth ids stages default = FP_E.END_WHILE ids' params body
              -> d + ids = FP_E_WF.get_end_while_id params
                  /\ d + ids = ids'
                  /\  idb =  FP_E_WF.get_while_block_id params)
        -> is_call_cont k
        -> find_label_ls l_wend (gen_fp_stages stages) 
                                    (stage_cont_gen block blocks k)
          = res_end_while_label block blocks (drop (ids - d).+1 stages)
                                      params k.
  Proof.
    move => block blocks; induction stages as [|stage stages IHs];
      move => idb ids params d k idd body l_wend default Hp
                 Hids Hget Hewhile Hk.
    destruct (ids - d); rewrite //= in Hget.

    destruct (Nat.eq_dec (ids - d) 0) as [Heq | Hne].
    - rewrite Heq //= in Hget.
      rewrite /res_end_while_label Hget /l_wend //= Heq drop0.
      destruct ident_eq; try by [].
    - have Hd: (ids - d).-1 = ids -d.+1.
      { to_nat Hne; ssrlia. }
      rewrite reduce_nth in Hget; try by [].
      rewrite Hd in Hget.

      have Hids': d.+1 <= ids. {to_nat Hne; ssrlia. }

      have Hewhile': forall ids' ids'' params' body',
                List.nth ids' stages (FP_E.DEFAULT idd)
                    = FP_E.END_WHILE ids'' params' body'
                -> d.+1 + ids' = FP_E_WF.get_end_while_id params'
                    /\ d.+1 + ids' = ids''
                    /\ idb = FP_E_WF.get_while_block_id params'.
      { move => ids' ids'' params' body' Hnth.
         have Hew := Hewhile ids'.+1 ids'' params'.
         rewrite //= in Hew. destruct (Hew _ Hnth) as [Hp' [Hi' Hidb']].
         rewrite -Hp' -Hi' Hidb'.
         split; try by []; ssrlia. }

      replace (ids - d) with ((ids - d.+1).+1); last first.
        to_nat Hids'. ssrlia.
     
      destruct stage; rewrite //=.

      all: try by apply (IHs _ _ _ _ _ _ _ Hp Hids' Hget Hewhile' Hk). 

      (* WHILE *)
      destruct ident_eq as [Heq | Hne']. symmetry in Heq.
      by apply gen_while_ne_end in Heq.
      
      all: try by apply (IHs _ _ _ _ _ _ _ Hp Hids' Hget Hewhile' Hk). 

      (* END WHILE *)
      assert(H: get_end_while_label params <> get_end_while_label params0).
      { apply gen_end_while_label_ne_stage.
        destruct (Hewhile 0 id params0 body0) 
          as [Hp0 [Hp0' Hp0'']]; try by [].
        destruct (Hewhile' _ _ _ _ Hget) as [Hp1 [Hp1' Hp1'']].
        rewrite -Hp0 -Hp1. ssrlia. }
      rewrite /l_wend. destruct (ident_eq) as [Heq' | Hne']; try by [].
      by apply (IHs _ _ _ _ _ _ _ Hp Hids' Hget Hewhile' Hk). 

      (* CALL *)
      rewrite no_label_call.
      by apply (IHs _ _ _ _ _ _ _ Hp Hids' Hget Hewhile' Hk). 

      (* NAV INIT *)
      rewrite no_label_nav_init.
      by apply (IHs _ _ _ _ _ _ _ Hp Hids' Hget Hewhile' Hk). 

      (* NAV *)
      rewrite no_label_nav_pre_call
                no_label_nav_cond
                no_label_nav_until
                no_label_nav_post_call.
      by apply (IHs _ _ _ _ _ _ _ Hp Hids' Hget Hewhile' Hk). 
  Qed.

  Lemma find_end_while_label_block_eq:
  forall block blocks stages idb ids params body k,
    let l_wend := get_end_while_label params in
    let default :=  (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat) in
      FP_E.get_while_block_id params = idb
      ->  List.nth ids stages default = FP_E.END_WHILE ids params body
      ->  (forall ids ids' params body,
            List.nth ids stages default = FP_E.END_WHILE ids' params body
            -> ids = FP_E_WF.get_end_while_id params
                /\ ids = ids'
                /\  idb
                       =  FP_E_WF.get_while_block_id params)
      -> is_call_cont k
      -> find_label_ls l_wend (gen_fp_stages stages)
                                   (stage_cont_gen block blocks k)
          = res_end_while_label block blocks (drop ids.+1 stages) params k.
  Proof.
    move => block blocks stages idb ids params body k l_wend
                default Hidb Hnth Hewhile Hk.
    have Hids0: ids = (ids - 0)%coq_nat. ssrlia.

    rewrite Hids0; eapply (find_end_while_label_block_eq_gen _ _ Hidb);
      try ssrlia.

    rewrite -Hids0. apply Hnth.

    apply Hewhile.
  Qed.

  Lemma find_end_while_label_gen:
  forall blocks idb ids d params body k,
    let l_wend := get_end_while_label params in
    let block :=
      List.nth (idb - d) blocks (FP_E_WF.get_default_block fpe) in
    let default_block := FP_E_WF.get_default_block fpe in
    let stages := FP_E_WF.get_block_stages block in
    let default :=  (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat) in
      (idb - d < Datatypes.length blocks)%coq_nat
      -> (idb >= d)%coq_nat
      ->  List.nth ids stages default = FP_E.END_WHILE ids params body
      -> (forall idb block,
              (idb < Datatypes.length blocks)%coq_nat
              -> List.nth idb blocks (FP_E_WF.get_default_block fpe)
                     = block
              -> FP_E_WF.get_block_id block = d + idb)
      ->  (forall idb ids ids' params body,
            let block := List.nth idb blocks
                                (FP_E_WF.get_default_block fpe) in
            let stages := FP_E_WF.get_block_stages block in
            let default := 
                (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat) in
            List.nth ids stages default = FP_E.END_WHILE ids' params body
            -> ids = FP_E_WF.get_end_while_id params
                /\ ids = ids'
                /\  FP_E_WF.get_block_id block
                       =  FP_E_WF.get_while_block_id params)
      -> is_call_cont k
      -> find_label_ls l_wend
            (gen_fp_blocks blocks default_block) (Kswitch k)
          = res_end_while_label block (drop (idb.+1 - d) 
                        (blocks ++ [:: default_block]))
                        (drop ids.+1 stages) params k.
  Proof.
    induction blocks as [|block blocks IHb];
      move => idb ids d params body k l_wend B default_block stages
                  default Hlen Hd Hget Hsize Hewhile Hk.
    rewrite //= in Hlen. ssrlia.

    rewrite //= ?no_label_parse_c_code_option
                    ?no_label_exceptions.

    have Hnth: List.nth (idb - d) (block ::blocks)
                                    (FP_E_WF.get_default_block fpe) = B.
    { by rewrite /B. }

    destruct (Nat.eq_dec (idb - d) 0) as [Heq | Hne].
    - destruct (Hewhile _ _ _ _ _ Hget) as [Hids [Heq' Hp]].
      rewrite Hnth in Hp.
      apply Hsize in Hnth; last first. rewrite Heq; ssrlia.
      rewrite Hnth in Hp. symmetry in Hp.
      replace (d + (idb - d)) with idb in Hp; last first.
      to_nat Heq; ssrlia.

      have Hewhile':
        forall ids ids' params body,
          List.nth ids stages
            (FP_E.DEFAULT (Datatypes.length stages - 1)%coq_nat)
              = FP_E.END_WHILE ids' params body
          -> ids = FP_E_WF.get_end_while_id params
            /\ ids = ids' /\ idb = FP_E_WF.get_while_block_id params.
      { move => ids' ids'' params' bpdy' Hnth'.
        destruct (Hewhile _ _ _ _ _ Hnth') as [Hp' [Hids' Hn]];
        repeat (split; try by []).
        rewrite Hnth in Hn. rewrite -Hn. ssrlia. }

      have Hdrop: drop (idb.+1 - d)
        (block :: blocks ++ [:: default_block])
          = blocks ++ [:: default_block].
      { replace (idb.+1 - d) with ((idb - d).+1).
         by rewrite Heq //= drop0. ssrlia. }

      have Hs := find_end_while_label_block_eq block
                (blocks ++ [:: default_block]) Hp Hget Hewhile' Hk.
      rewrite /stages /B Heq //= in Hs.

      by rewrite /l_wend /stages /B Heq //= seq_lstmt_to_stmt Hs
               /stage_cont_gen /b_cont_gen /blocks_cont_gen 
                Hdrop.
    - destruct (Hewhile _ _ _ _ _ Hget) as [Hids [Heq Hp]].
      have Hd': idb > d. to_nat Hne. ssrlia.

      apply Hsize in Hnth; last first. to_nat Hne.
      replace (d + (idb - d)) with idb in Hnth; last first.
      to_nat Hd'. ssrlia.
      have Hne': idb <> d. to_nat Hd'. ssrlia.
      rewrite -Hnth /B Hp in Hne'.
      rewrite (find_end_while_label_block_ne _ Hne'); last first.
      { move => ids' ids'' params' body' stages' default' Hnth'.
        have Hb: List.nth 0 (block :: blocks)
                                    (FP_E_WF.get_default_block fpe)
                                = block. by [].
        rewrite /stages' -Hb in Hnth'.
        destruct (Hewhile _ _ _ _ _ Hnth') as [Hids' [Hid Hget']];
          subst ids''.
        have Hlen' : (0 < Datatypes.length (block::blocks))%coq_nat.
          ssrlia.
        have H:= Hsize _ _  Hlen' Hb. rewrite //= H in Hget'.
        split; try by []. rewrite -Hget'; ssrlia. }
      rewrite /stages /B reduce_nth /l_wend; try by []. 
      replace ((idb - d).-1) with (idb - d.+1); last first.
          to_nat Hd'; ssrlia.
      erewrite (IHb idb ids d.+1); try by [].
      * rewrite reduce_drop; try ssrlia.
          replace (((idb.+1 - d)%coq_nat - 1)%coq_nat)
            with ((idb.+1 - d.+1)%coq_nat); last first.
            to_nat Hd'; destruct d; ssrlia. by [].
      * rewrite //=  in Hlen.
        have H: (idb - d.+1 < (Datatypes.length blocks))%coq_nat.
        to_nat Hlen. to_nat Hd'. ssrlia. apply H.
      * to_nat Hd'; ssrlia.
      * rewrite /stages /default /stages /B reduce_nth  in Hget; try by [].
        replace ((idb - d).-1) with (idb - d.+1) in Hget; last first.
          to_nat Hd'; ssrlia. 
        apply Hget. 
      * move => idb' block' Hlen' Hnth'.
         replace (d.+1 + idb') with (d + idb'.+1); last first. ssrlia.
         apply Hsize. ssrlia. rewrite reduce_nth; try by [].
      * move => idb' ids' ids'' params' body'
                    block' stages' default' Hnth'.
        have Hew := Hewhile idb'.+1 ids' ids'' params' body'.
        apply Hew. rewrite reduce_nth; try by [].
  Qed.

  Lemma find_end_while_label:
    forall idb ids ids' params body k,
      let l_wend := get_end_while_label params in
      let block :=
        List.nth idb (FP_E.get_fp_blocks fpe)
                      (FP_E_WF.get_default_block fpe) in
      let stages := FP_E_WF.get_block_stages block in
      FP_E_WF.wf_fp_e fpe
        -> verified_fp_e fpe
        -> (idb < FP_E.get_nb_blocks fpe - 1)%coq_nat
        -> FP_E.get_stage fpe idb ids = FP_E.END_WHILE ids' params body
        -> is_call_cont k
        -> find_label l_wend (fn_body f) k
              = Some (
                (setNavStage ((FP_E_WF.get_end_while_id params) + 1)),
                (Kseq
                  (seq_of_labeled_statement
                      (gen_fp_stages (drop ids.+1 stages)))
                        (stage_cont idb k))).
  Proof.
    move => idb ids ids' params body k l_wend block stages
                Hwf Hsize Hlen Hget Hk.

    replace (find_label _ _ _)
      with (find_label_ls l_wend
              (gen_fp_blocks (FP_E.get_fp_blocks fpe)
                             (FP_E_WF.get_default_block fpe))
              (Kswitch k));
      last first.
    rewrite //= 
             no_label_exceptions (* Global exceptions *)
             ?no_label_parse_c_code_option (* Pre_call *)
             ?no_label_exceptions (* Local exceptions *)
             //=.

    have Hewhile := FP_E_WF.get_wf_end_while Hwf.
    rewrite /FP_E_WF.wf_while /FP_E_WF.get_stage
              /FP_E_WF.get_stages /FP_E_WF.get_block in Hewhile.

    have Hget' := Hget.
    apply Hewhile in Hget'; destruct Hget' 
      as [Hids_p [Hend [Hids Hidb]]]; subst ids'.

    rewrite /FP_E.get_stage /FP_E.get_stages
              /FP_E.default_stage /FP_E.default_stage_id in Hget.

    have H: (idb - 0 < Datatypes.length (FP_E.get_fp_blocks fpe))%coq_nat.
    rewrite /FP_E.get_nb_blocks in Hlen. to_nat Hlen. ssrlia.

    rewrite /stages /block. replace idb with (idb - 0); last first. ssrlia.

    rewrite /l_wend /res_while_label.

    erewrite (find_end_while_label_gen H); try by [].
    - rewrite /stage_cont /block_cont /res_end_while_label.
      replace (idb - 0) with idb; last first. ssrlia. by [].
    - ssrlia.
    - replace (idb - 0) with idb; last first. ssrlia.
      apply Hget.
    - replace (0 + idb) with idb; last first. ssrlia.
      move => idb' block' Hlen' Hnth'. subst.
      rewrite (get_well_numbered_blocks Hsize); ssrlia.
      rewrite /FP_E.get_nb_blocks. ssrlia.

    move => idb' ids' ids'' params' body' block' stages' default Hget'.

    destruct (Hewhile _ _ _ _ _ Hget')
      as [Hids_p' [Hend' [Hids' Hidb']]].

    split; try by [].
  Qed.

  (** Similar of the CompCert.Cminor remarks *)
  Remark is_call_cont_call_cont:
    forall k, is_call_cont (call_cont k).
  Proof.
    induction k; simpl; auto.
  Qed.

  Remark call_cont_is_call_cont:
    forall k, is_call_cont k -> call_cont k = k.
  Proof.
    destruct k; simpl; intros; auto || contradiction.
  Qed.

  Lemma call_cont_id:
    forall k k',
      call_cont k = k'
      -> is_call_cont k'.
  Proof. 
    move => k k' Hk.
    rewrite -Hk. apply is_call_cont_call_cont.
  Qed. 

End FIND_LABELS.
