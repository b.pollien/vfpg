From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

From compcert Require Import Coqlib Integers Floats AST Ctypes
                        Cop Clight Clightdefs Maps
                        Events Memory Values.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas CommonLemmas ClightLemmas
                        CommonLemmasNat8
                        BasicTypes
                        FPNavigationMode FPNavigationModeSem
                        FlightPlanGeneric FlightPlanExtended
                        FlightPlanSized
                        FPNavigationModeGen
                        CommonFPDefinition ClightGeneration
                        AuxFunctions GvarsVerification
                        FPEnvironmentGeneric FPEnvironmentDef
                        FPEnvironmentClight
                        FPBigStepDef
                        FPBigStepClight Generator
                        GeneratorProperties
                        
                        ExtractTraceFPEnv.
From GEN Require Import CommonFP.

Import Clightdefs.ClightNotations.
Local Open Scope clight_scope.
Local Open Scope nat_scope.

Set Implicit Arguments.

(** Verification of semantics preservation for the common functions *)

Module COMMON_FP_VERIF (EVAL_Def: EVAL_ENV)
                                    (ENVS_Def: ENVS_DEF EVAL_Def)
                                    (BS_Def: FP_BS_DEF EVAL_Def ENVS_Def).

  Module EXTRACT_FPENV := EXTRACT_TRACE_FPENV EVAL_Def ENVS_Def BS_Def.
  Import C_ENV BS_Def FPS_BS FPS_ENV
            ENVS_Def FPE_ENV FPE_ENV.Def
            FPE_BS.Common_Sem Def MATCH_FPS_C
            EXTRACT_FPENV EXTRACT_FPENV.FP_EXTRACT.

Section FLIGHT_PLAN.
  (** Flight Plan Size *)
  Variable fps: flight_plan_sized.
  Definition fpe_wf := proj1_sig fps.
  Definition Hsize := proj2_sig fps.

  (** Correct list of global variables defined by the user *)
  Variable gvars: cgvars.

  Definition fpe  := proj1_sig fpe_wf.
  Definition Hwf := proj2_sig fpe_wf.

  Remark eq_fps_fp: (` (` fps)) = fpe. by []. Qed.

  (** Global program generated *)
  Definition prog := generate_complete_context fpe gvars.

  (** Definition of the global environment *)
  Definition ge := globalenv prog.

  (** Function to execute*)
  Definition f := gen_fp_auto_nav fpe.

  (** * Section for the execution of abstract C cond *)
  (** Axiom that represent the evaluation of abstract condition. The
      condition is converted into a trace and replaced by a boolean
      constant. This constant is the result of the evaluation of the
      condition using evalc function. *)
  Axiom eval_c_code_cond:
    forall code t b,
      EVAL_Def.eval t code = b
      -> forall ce,  fp_trace_to_trace t = ce.t
      -> forall f (f_stmt: expr -> statement) k e le k' e' le' m' stmt,
        let m'' := app_ctrace m' [:: cond_event code b] in
        (step2 ge
              (State f (f_stmt (gen_bool_const b)) k e le (ce.m))
          E0 (State f stmt k' e' le' (m''.m))
      -> step2 ge (State f (f_stmt (parse_c_code_cond code)) k e le (ce.m))
          [:: cond_event code b] (State f stmt k' e' le' (m''.m))).

  Lemma eval_c_code_cond_gen:
    forall f e1 e2 e1' b code
      (f_stmt: expr -> statement),
    e1 ~cenv~ (ge, e2)
    -> evalc (` e1) code = (b, e1')
    -> forall k e le stmt k' e' le' e2',
      let t := extract_trace (` e1) e1' in
      let e2'' := app_ctrace e2' [:: cond_event code b] in 
    (step2 ge
            (State f (f_stmt (gen_bool_const b)) k e le (e2.m))
        E0 (State f stmt k' e' le' (e2''.m))
    -> step2 ge
        (State f (f_stmt (parse_c_code_cond code)) k e le (e2.m))
        (fp_trace_to_trace t) (State f stmt k' e' le' (e2''.m))).
  Proof.
    move => f e1 e2 e1' b code f_stmt He Heval k e le
                stmt k' e' le' e2' t e2'' Hstep.
    rewrite /evalc in Heval.

    injection Heval as Heval He1'.

    rewrite /t -He1' extract_app_trace Heval.

    by apply (eval_c_code_cond Heval (trace_generated_eq He)).
  Qed.

  Lemma eval_c_code_ifthenelse:
    forall f e1 e2 e1' b code,
    e1 ~cenv~ (ge, e2)
    -> evalc (` e1) code = (b, e1')
    -> forall stmt1 stmt2 k e le stmt k' e' le' e2',
      let t := extract_trace (` e1) e1' in
      let e2'' := app_ctrace e2' [:: cond_event code b] in 
    (step2 ge
        (State f (Sifthenelse (gen_bool_const b) stmt1 stmt2) k e le (e2.m))
          E0 (State f stmt k' e' le' (e2''.m))
    -> step2 ge
        (State f 
            (Sifthenelse (parse_c_code_cond code) stmt1 stmt2) k e le (e2.m))
        (fp_trace_to_trace t) (State f stmt k' e' le' (e2''.m))).
  Proof.
    move => f e1 e2 e1' b code He Heval stmt1 stmt2 k e le stmt
                k' e' le' e2' t e2'' Hstep.
    remember (fun expr => Sifthenelse expr stmt1 stmt2) as f_stmt.
    replace (Sifthenelse _ _ _) with (f_stmt (parse_c_code_cond code)).
    replace (Sifthenelse _ _ _) with (f_stmt (gen_bool_const b)) in Hstep.
    apply (eval_c_code_cond_gen He Heval Hstep).

    all: by rewrite Heqf_stmt.
  Qed.

  Lemma eval_c_code_ifthenelse_neg:
    forall f e1 e2 e1' b code,
    e1 ~cenv~ (ge, e2)
    -> evalc (` e1) code = (b, e1')
    -> forall stmt1 stmt2 k e le stmt k' e' le' e2',
      let t := extract_trace (` e1) e1' in
      let e2'' := app_ctrace e2' [:: cond_event code b] in 
    (step2 ge
        (State f (Sifthenelse (gen_neg_bool (gen_bool_const b))
            stmt1 stmt2) k e le (e2.m))
        E0 (State f stmt k' e' le' (e2''.m))
    -> step2 ge
        (State f 
          (Sifthenelse (gen_neg_bool (parse_c_code_cond code)) stmt1 stmt2)
              k e le (e2.m))
        (fp_trace_to_trace t) (State f stmt k' e' le' (e2''.m))).
  Proof.
    move => f e1 e2 e1' b code He Heval stmt1 stmt2 k e le stmt
                k' e' le' e2' t e2'' Hstep.
    remember (fun expr => Sifthenelse (gen_neg_bool expr) stmt1 stmt2)
      as f_stmt.
    replace (Sifthenelse _ _ _) with (f_stmt (parse_c_code_cond code)).
    replace (Sifthenelse _ _ _) with (f_stmt (gen_bool_const b)) in Hstep.
    apply (eval_c_code_cond_gen He Heval Hstep).

    all: by rewrite Heqf_stmt.
  Qed.

  Lemma eq_exec_set_cond:
    forall e1 e2 e1' b cond,
      e1 ~cenv~ (ge, e2)
      -> evalc (` e1) cond = (b, e1')
      -> forall k e le stmt k' e' le' e2',
          let t := extract_trace (` e1) e1' in
          let e2'' := app_ctrace e2' [:: cond_event cond b] in 
            step2 ge
              (State f (Sset tmp_cond (Ecast (gen_bool_const b) tbool))
                k e le (e2.m))
          E0 (State f stmt k' e' le' (e2''.m))
        -> step2 ge
                    (State f
                        (Sset tmp_cond
                            (Ecast (parse_c_code_cond cond) tbool))
                            k e le (e2.m))
                    (fp_trace_to_trace t)
                    (State f stmt k' e' le' (e2''.m)).
  Proof.
    move => e1 e2 e1' b cond He Heval k e le stmt 
                k' e' le' e2' t e2'' Hstep.
    remember (fun expr => Sset tmp_cond 
                            (Ecast expr tbool)) as f_stmt.
    replace (Sset _ _) with (f_stmt (parse_c_code_cond cond)).
    replace (Sset _ _) with (f_stmt (gen_bool_const b)) in Hstep.
    apply (eval_c_code_cond_gen He Heval Hstep).

    all: by rewrite Heqf_stmt.
  Qed.

  (** Evaluation of a condition from a function call *)
  (** The axiom state that if there is a call of arbitrary C code, then it
      can be converted into a trace and replaced by a boolean constant.
      This constant is the result of the evaluation of the condition using
      evalc function.*)
  Axiom exec_arbitrary_fun_call_with_res:
    forall f func params t b,
    EVAL_Def.eval t (gen_fun_call_sem func params) = b
      -> forall id m' tres ltypes e_p k e le k' e' le' stmt,
          tres = tbool
      -> match_params ltypes e_p params
      -> forall ce,  fp_trace_to_trace t = ce.t
      -> let m'' := app_ctrace m'
          [:: cond_event (gen_fun_call_sem func params) b] in 
          step2 ge
                    (State f (Sset id (gen_bool_const b)) k e le (ce.m))
                E0
                    (State f stmt k' e' le' (m''.m))
      -> step2 ge
          (State f
            (Scall (Some id) (Evar ##func (Tfunction ltypes tres cc_default)) e_p)
              k e le (ce.m))
              [:: cond_event (gen_fun_call_sem func params) b]
          (State f stmt k' e' le' (m''.m)).

  Lemma eq_exec_nav_cond_stmt:
    forall f e1 e2 e1' b cond ps pm pc stmt_cond wp,
      e1 ~cenv~ (ge, e2)
      -> evalc (` e1) cond = (b, e1')
      -> nav_cond_sem (GO ps pm pc) = Some cond
      -> nav_cond_stmt (GO ps pm pc) = Some (stmt_cond, wp)
      -> forall k e le stmt k' e' le' e2',
          let t := extract_trace (` e1) e1' in
          let e2'' := app_ctrace e2' [::cond_event cond b] in 
            step2 ge
              (State f (Sset tmp_NavCond (gen_bool_const b)) k e le (e2.m))
          E0 (State f stmt k' e' le' (e2''.m))
        -> step2 ge
                    (State f stmt_cond k e le (e2.m))
                    (fp_trace_to_trace t)
                    (State f stmt k' e' le' (e2''.m)).
  Proof.
    move => f e1 e2 e1' b cond ps pm pc stem_cond wp
                He Heval Hsem Hstmt k e le stmt k' e' le' e2' t e2'' Hstep.
    rewrite /evalc in Heval.

    injection Heval as Heval He1'.

    destruct ps as [wp' [last|] h ap];
      destruct ap as [ a | et |];
      inversion Hsem as [[H]]; inversion Hstmt as [[H' H'']];
      subst cond wp'.

    all: rewrite /t -He1' extract_app_trace Heval.

    all: apply (exec_arbitrary_fun_call_with_res Heval);
      try by []; simpl_eqparam_ex wp.

    all: try by exists last.

    all: by apply (trace_generated_eq He).
  Qed.

  (** * Lemmas Usefull for the execution of CLight code *)

  Lemma seq_lb_stmt_block:
    forall fpe e,
      let id := (get_nav_block e) in
      let blocks := FP_E.get_fp_blocks fpe in
      let default_block := FP_E_WF.get_default_block fpe in
      let b := FP_E.get_block fpe id in
      verified_fp_e fpe
      -> fp_env_on_8 e
      -> id < FP_E.get_nb_blocks fpe - 1
      -> (seq_of_labeled_statement
          (select_switch
            (Int.unsigned 
              (ClightGeneration.int_of_nat 
                id))
            (gen_fp_blocks blocks default_block))
          ) = 
          ((gen_fp_block b)
          @+@ (seq_of_labeled_statement
                  (gen_fp_blocks (drop (id.+1) blocks) default_block))).
  Proof.
    move => fpe e id blocks default_block b Hsize' He Hlt.
    rewrite (select_switch_fp_block _ Hsize').
    - by [].
    - rewrite /FP_E.get_nb_blocks in Hlt. to_nat Hlt. ssrlia.
  Qed.

  Lemma seq_lb_stmt_default_block:
    forall fpe e,
      let id := (get_nav_block e) in
      let blocks := FP_E.get_fp_blocks fpe in
      let default_block := FP_E_WF.get_default_block fpe in
      let b := FP_E.get_block fpe id in
      verified_fp_e fpe
      -> fp_env_on_8 e
      -> FP_E.get_nb_blocks fpe - 1 <= id
      -> (seq_of_labeled_statement
          (select_switch
            (Int.unsigned 
              (ClightGeneration.int_of_nat 
                id))
            (gen_fp_blocks blocks default_block))
          ) = 
          ((gen_fp_block default_block) @+@ Sskip).
  Proof.
    move => fpe e id blocks default_block b Hsize' He Hlt.
    rewrite (select_switch_default_block (nav_block8 He) _ Hsize').
    - by [].
    - rewrite /FP_E.get_nb_blocks /id in Hlt. to_nat Hlt. ssrlia.
  Qed.

  Remark type_of_private_nav_block:
    typeof (Evar CommonFP._private_nav_block tuchar)
      = tuchar.
  Proof. by []. Qed.

  Lemma eval_expr_private_nav_block:
    forall e1 e2,
    e1 ~menv~ (ge, e2)
    -> forall le, eval_expr ge empty_env le e2
        (Evar CommonFP._private_nav_block tuchar)
        (create_val (get_nav_block e1)).
  Proof.
    move => e1 e2 He le.
    destruct (get_symb_private_nav_block fpe gvars) as [b Hfs].

    econstructor. apply eval_Evar_global; try by [].  apply Hfs.
    econstructor; try by [].

    apply (proj2 ((MATCH_ENV.correct_private_nav_block He) _ Hfs)).
  Qed.

  (** *  Definition of a correct local env for common definitions  *)
  Record correct_env (e:env) := {
    none_init_function: e!CommonFP._init_function = None;
    none_NextStage: e!CommonFP._NextStage = None;
    none_get_nav_block: e!CommonFP._get_nav_block = None;
    none_set_nav_block: e!CommonFP._set_nav_block = None;
    none_get_nav_stage: e!CommonFP._get_nav_stage = None;
    none_next_stage: e!CommonFP._NextStage = None;
    none_set_nav_stage: e!CommonFP._set_nav_stage = None;
    none_private_nav_block: e!CommonFP._private_nav_block = None;
    none_private_last_block: e!CommonFP._private_last_block = None;
    none_private_nav_stage: e!CommonFP._private_nav_stage = None;
    none_private_last_stage: e!CommonFP._private_last_stage = None;
    none_nav_block: e!CommonFP._nav_block = None;
    none_last_block: e!CommonFP._last_block = None;
    none_nav_stage: e!CommonFP._nav_stage = None;
    none_last_stage: e!CommonFP._last_stage = None;
    none_goto_block: e!CommonFP._nav_goto_block = None;
    none_next_block: e!CommonFP._NextBlock = None;
    none_nb_blocks: e!CommonFP._nb_blocks = None;
    none_return: e!CommonFP._Return = None;
    none_on_enter_block: e!_on_enter_block = None;
    none_on_exit_block: e!_on_exit_block = None;
    none_forbidden_deroute: e!_forbidden_deroute = None;
  }.

  Lemma correct_empty_env :
    correct_env empty_env.
  Proof. by []. Qed.

  Definition Hcempt := correct_empty_env.

  (** Get the nb_blocks value *)
  Lemma get_load_nb_blocks:
    forall e2 b,
      MATCH_ENV.correct_const_access ge e2 CommonFP._nb_blocks
      -> Globalenvs.Genv.find_symbol ge CommonFP._nb_blocks = Some b
      -> Mem.loadv Mint8unsigned e2 (Vptr b Ptrofs.zero)
          = Some (create_val (FP_E.get_nb_blocks fpe)).
  Proof.
    move => e2 b Hc Hfs.
    eapply (Hc _ _ _ _ Hfs).
    - have Hnb := get_nb_blocks fpe gvars.
      apply Globalenvs.Genv.find_def_symbol in Hnb.
      destruct Hnb as [b' [Hfs' Hfd']].
      rewrite Hfs' in Hfs; inversion Hfs; subst b'.
      apply Hfd'.
    - by [].
  Qed.

  (** Initiale state exists *)
  Definition prop_correct_init_env
    (val: ident * globdef (Ctypes.fundef function) type)
 :=
    forall (id : ident) (v : globvar type),
      val = (id, Gvar v)
      -> 
        Globalenvs.Genv.init_data_list_aligned 0 (gvar_init v)
        /\ (forall i o,
              In (Init_addrof i o) (gvar_init v)
              -> exists b,
                  Globalenvs.Genv.find_symbol
                    (Globalenvs.Genv.globalenv prog) i = Some b).

  Lemma correct_init_var:
    forall var, 
      correct_gvar var
      -> prop_correct_init_env var.
  Proof.
    move => var H.
    destruct (access_gvar H) as [id Heq]; subst var.
    move => id' v H'; inversion H'; subst id' v.
    split => //=.
    - split; try by []; apply Z.divide_0_r.
    - move => i o Hin; destruct Hin; by [].
  Qed.

  Lemma Forall_correct_init_gvars:
    forall (Pres: Prop),
    Pres
    -> fold_right (fun x  => and (prop_correct_init_env x)) Pres (` gvars).
  Proof.
    move => Pres Hp. destruct gvars as [vars Hvars].
    induction vars as [|var vars IH] => //=.
    split.
    - apply (correct_init_var (correct_gvars_hd Hvars)).
    - apply IH. by apply (correct_gvars_tail Hvars).
  Qed.

  Lemma Forall_correct_init_env:
    Forall prop_correct_init_env (AST.prog_defs prog).
  Proof.
    apply Forall_fold_right.

    repeat
      (split; [ move => id v; split; injection H as Hid Hv; subst id v;
                [ split; try by []; apply Z.divide_0_r
                  | move => i o Hin; destruct Hin; by [] ] |]).

    repeat (split; [ move => id v H; by inversion H | ]).

    rewrite /= fold_right_app. apply Forall_correct_init_gvars.

    repeat (split; [ move => id v H; by inversion H | ]).

    split; try by [].

    move => id v H; injection H as Hid Hv; subst id v; split.
    - split; try by []; apply Z.divide_0_r.
    - move => i o Hin; destruct Hin; by [].  
  Qed.

  Lemma init_mem_exists:
    exists m, Globalenvs.Genv.init_mem prog = Some m.
  Proof.
    apply Globalenvs.Genv.init_mem_exists.
    move => id v H.
    remember (id, Gvar v) as val.
    generalize dependent Heqval;
    generalize dependent v;
    generalize dependent id.
    by apply ((proj1 (Forall_forall _ _)) Forall_correct_init_env).
  Qed.

  Lemma semantics_init_exists:
    exists e_init, C_BIGSTEP.initial_state prog e_init.
  Proof.
    destruct init_mem_exists as [m Hm].
    by exists (create_fp_cenv m [::]).
  Qed.

  (** Evaluation of NB_BLOCKS - 1*)
  Definition nb_blocks_minus1 :=
    (Ebinop Osub (Evar CommonFP._nb_blocks tuchar)
                      (Econst_int (Int.repr 1) tint) tint).

  Lemma eval_expr_nb_blocks_minus1:
    forall e m,
      correct_env e
      -> MATCH_ENV.correct_const_access ge m CommonFP._nb_blocks
      -> forall le,
        eval_expr ge e le m nb_blocks_minus1
          (create_val ((FP_E.get_nb_blocks fpe) - 1)).
  Proof.
    move => e m Hce Hc le.

    destruct (get_symb_nb_blocks fpe gvars) as [b Hfs].

    econstructor.
    - econstructor. apply eval_Evar_global.
      apply (none_nb_blocks Hce). apply Hfs. 
      econstructor; try by []. 
      apply (get_load_nb_blocks Hc Hfs).
    - econstructor.

    rewrite //= /sem_sub /sem_binarith ?sem_cast8 //=.
    rewrite sem_cast_one /create_val //=. repeat f_equal.
    apply (decr_int_val (FP_E.nb_blocks_ne_0 fpe) (get_nb_blocks8 Hsize)).

    apply (get_nb_blocks8 Hsize).
  Qed.

  (** * Verification of Aux functions *)

  (** ** Verification of forbidden deroute function *)
  Lemma test_fordidden_deroute_ne_from:
    forall from to fbd e,
      test_from from fbd = false
      -> test_forbidden_deroute e from to fbd = (false, e).
  Proof.
    move => from to [from' to' cond] e Hne.
    rewrite /test_forbidden_deroute //=.
    apply test_from_false in Hne. rewrite //= in Hne.
    rewrite ((proj2 (Nat.eqb_neq _ _)) Hne) //=.
  Qed.

  Lemma test_forbidden_deroutes_filtered:
    forall from to fbds fbds' filtered e1 e1' b,
      test_forbidden_deroutes e1 from to fbds = (b, e1')
    -> partition (test_from from) fbds = (filtered, fbds')
    -> test_forbidden_deroutes e1 from to filtered = (b, e1').
  Proof.
    move => from to; induction fbds as [|fbd fbds IHfbd];
      move => fbds' filtered e1 e1' b Htest Hp.
    - by inversion Hp.
    - destruct (partition _ fbds) as [filtered' fbds''] eqn:Hp';
        destruct (test_from from fbd) eqn:Ht.
      + rewrite (partition_cons1 _ _ _ Hp' Ht)
          in Hp; inversion Hp; subst filtered fbds''; rewrite //=.
        destruct (test_forbidden_deroute e1 from to fbd)
          as [[] e1''] eqn:Htest'; rewrite //= Htest' in Htest.
        * by inversion Htest.
        * eapply (IHfbd _ _ _ _ _ Htest); by reflexivity. 
      + rewrite //= (test_fordidden_deroute_ne_from to e1 Ht)
                  //= in Htest.
        eapply IHfbd. apply Htest.
        rewrite //= Hp' Ht in Hp. inversion Hp; reflexivity.
  Qed.

  Definition fb_return_stmt (b: bool) :=
    if b then return_true
    else Sskip.

  Lemma step_test_frobidden_deroute:
    forall from to fbd e1 e1' e2 b ,
    e1 ~cenv~ (ge, e2)
    -> is_nat8 from
    -> is_nat8 to
    -> correct_fbd_deroute fpe fbd
    -> test_from from fbd 
    -> test_forbidden_deroute (` e1) from to fbd = (b, ` e1')
    -> exists e2', let t := extract_trace (` e1) (` e1') in
        (forall f k,
        let le := 
        (PTree.set _to (Vint (int_of_nat to))
          (PTree.set _from (Vint (int_of_nat from))
            (PTree.empty val))) in
        Smallstep.star step2 ge
        (State f (gen_fbd_test fbd) k empty_env le (e2.m))
        (fp_trace_to_trace t)
        (State f (fb_return_stmt b) k empty_env le (e2'.m)))
    /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => from to [from' to' cond] e1 e1' e2 b He Hfrom8 Hto8 Hc Hp Ht;
      rewrite /gen_fbd_test //=;
      rewrite / test_forbidden_deroute  //= in Ht.

    apply test_from_eq in Hp; rewrite //= in Hp; subst from'.
    rewrite Nat.eqb_refl in Ht.

    destruct (to =? to') eqn:Heq.

    (* fbd correspond to [from] and [to] *)
    - rewrite //= in Ht. destruct cond as [cond|].
      * { (* Evaluation of the condition *)
          destruct (evalc (` e1) cond) as [b' e1''] eqn:Heval;
          inversion Ht; subst b' e1''.

          destruct b.

          (* Is forbidden *)
          eexists; split. move => f k.

          star_step step_ifthenelse. 
          + econstructor. econstructor. apply PTree.gss.
            econstructor. econstructor.
          + apply Nat.eqb_eq in Heq; subst to'.
             by rewrite //= Int.eq_true.

          rewrite Int.eq_false //=; try by []. step_trans.
          star_step (@eval_c_code_ifthenelse f _ _ _ _ _ He Heval).
          econstructor. apply eval_expr_const_bool. by [].
          rewrite true_ne_0. step_refl. all: try by [].

          step_refl. simpl_trace. 

          by apply (evalc_preserve_match Heval He).

          (* Is not fobidden *)
          eexists; split. move => f k.

          star_step step_ifthenelse. 
          + econstructor. econstructor. apply PTree.gss.
            econstructor. econstructor.
          + apply Nat.eqb_eq in Heq; subst to'.
             by rewrite //= Int.eq_true.

          rewrite Int.eq_false //=; try by []. step_trans.
          star_step (@eval_c_code_ifthenelse f _ _ _ _ _ He Heval).
          econstructor. apply eval_expr_const_bool. by [].
          rewrite false_eq_0. step_refl. all: try by [].

          step_refl. simpl_trace.

          by apply (evalc_preserve_match Heval He).
        }

      * injection Ht as Hb He1'; apply env8_proj in He1'; subst b e1'.
        exists e2; split; try by []; try move => f k.

        star_step step_ifthenelse. 
        + econstructor. econstructor. apply PTree.gss.
          econstructor. econstructor.
        + apply Nat.eqb_eq in Heq; subst to'.
           by rewrite //= Int.eq_true.

        rewrite Int.eq_false; try by []. step_refl.
        simpl_trace. by rewrite extract_trace_refl.

    (* fbd does not correspond *)
    - injection Ht as Hb He1'; apply env8_proj in He1'; subst b e1'.
      exists e2; split; try by []; try move => f k.

      star_step step_ifthenelse. 
      * econstructor. econstructor. apply PTree.gss.
        econstructor. econstructor.
      * apply Nat.eqb_neq in Heq.
        assert (is_nat8 to').
        {
          have H1 := (get_correct_to Hc).
          simpl in H1.
          have H2 := get_nb_blocks8 Hsize.
          rewrite /is_user_id /nb_blocks_lt_256 /is_nat8 /FP_E_WF.get_nb_blocks in H1 H2.
          rewrite eq_fps_fp in H2.
          unfold is_nat8. to_nat H1. to_nat H2. ssrlia.
        }
        apply (int_of_nat_ne8 Hto8 H) in Heq.
        rewrite //= in Heq. rewrite //= (Int.eq_false _ _ Heq).
      * by [].

      rewrite Int.eq_true //=. step_refl.
      rewrite extract_trace_refl. simpl_trace.
  Qed.

  Definition fbs_return_state f (b: bool) k e le m :=
    if b then (Returnstate (Vint Int.one) (call_cont k) m)
    else (State f Sbreak k e le m).

  Lemma step_test_frobidden_deroutes:
    forall f from to fbds e1 e1' e2 b,
    fn_return f = tbool
    -> e1 ~cenv~ (ge, e2)
    -> is_nat8 from
    -> is_nat8 to
    -> Forall (correct_fbd_deroute fpe) fbds
    -> Forall (test_from from) fbds
    -> test_forbidden_deroutes (` e1) from to fbds = (b, ` e1')
    -> exists e2', let t := extract_trace (` e1) (` e1') in
        (forall k,
        let le := 
        (PTree.set _to (Vint (int_of_nat to))
          (PTree.set _from (Vint (int_of_nat from))
            (PTree.empty val))) in
        Smallstep.star step2 ge
        (State f
          (fold_right (fun x y : statement => x @+@ y) Sbreak
              [seq gen_fbd_test i | i <- fbds])
          k empty_env le (e2.m))
        (fp_trace_to_trace t)
        (fbs_return_state f b k empty_env le (e2'.m)))
    /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => f from to; induction fbds as [|fbd fbds IHfbd];
      move => e1 e1' e2 b Hfn He Hfrom8 Hto8 Hc Hfor Htest.
    - injection Htest as H H'; apply env8_proj in H'; subst b e1'.
      rewrite extract_trace_refl. exists e2; split; try by [].
      move => k le; step_refl.

    - (* Execution of fbd *)
      destruct (test_forbidden_deroute8 e1 from to fbd)
        as [b' [e1'' Hfbd]].
      destruct (step_test_frobidden_deroute He Hfrom8 Hto8
      (Forall_inv Hc) (Forall_inv Hfor) Hfbd)
        as [e2' [Hfbd' He2']].

      rewrite //= Hfbd in Htest; destruct b'.

      (* Deroute forbidden *)
      exists e2'; injection Htest as Hb He1'; apply env8_proj in He1';
        subst b e1''; split; [ rewrite //= => k | apply He2'].
      step_seq. apply Hfbd'. star_step step_return_1.
      apply eval_expr_const_bool. by rewrite Hfn. by []. step_refl.
      all: try by []. simpl_trace. 


      (* Deroute not forbidden, continue the execution *)
      destruct (IHfbd _ _ _ _ Hfn He2' Hfrom8 Hto8
        (Forall_inv_tail Hc) (Forall_inv_tail Hfor) Htest)
        as [e2'' [Hfbds' [He2'' Hex']]].

      exists e2''; split; try by []; try move => k le.
      step_seq. apply Hfbd'. step_skip_seq.
      rewrite /fold_right /map //= in Hfbds'. apply Hfbds'.
      all: try by []. simpl_trace.

      have H := is_extract_trace_conc
                (extract_test_forbidden_deroute Hfbd)
                (extract_test_forbidden_deroutes Htest). 
      by rewrite (get_extract_trace H).
  Qed.

  Lemma step_forbidden_deroute:
    forall e1 e1' e2 le b id,
      e1 ~cenv~ (ge, e2)
      -> is_nat8 id
      -> forbidden_deroute fpe (` e1) id
              = (b,` e1')
      -> le ! CommonFP._b = Some (create_val id)
      -> let t := extract_trace (` e1) (` e1') in
        let le' := set_opttemp (Some CommonFP._t'1) (create_bool_val b) le
        in exists e2', (forall k,
        Smallstep.star step2 ge
          (State CommonFP.f_nav_goto_block
            (Scall (Some CommonFP._t'1)
                    (Evar CommonFP._forbidden_deroute
                        (Tfunction (Tcons tuchar (Tcons tuchar Tnil))
                                tbool cc_default))
                    [:: Evar CommonFP._private_nav_block tuchar;
                          Etempvar CommonFP._b tuchar])
                      k empty_env le (e2.m)) 
            (fp_trace_to_trace  t)
          (State CommonFP.f_nav_goto_block Sskip k empty_env le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 le b id He Hid Hfb Hle t le'.
    rewrite /forbidden_deroute in Hfb.

    have Hfun := forbidden_deroute_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hfun.
    destruct Hfun as [block [Hfs Hfd]].

    have Hb8 := (is_nav_block8 Hsize (proj2_sig e1)).

    (* Get stage partitionned *)
    destruct (partition (test_from (get_nav_block (` e1)))
                           (FP_E.get_fp_forbidden_deroutes fpe))
      as [filtered fbds'] eqn:Hpartition.
    destruct (seq_of_labeled_statement_fbd Hpartition) as [s Hsl].

    have Hfn : fn_return (gen_fp_forbidden_deroute fpe) = tbool by [].

    destruct (step_test_frobidden_deroutes Hfn He Hb8 Hid
              (Forall_P_partition (get_correct_fbd Hsize) Hpartition)
              (partition_forall Hpartition)
              (test_forbidden_deroutes_filtered Hfb Hpartition))
      as [e2' [Htest He2']].

    destruct b; rewrite //=.

    (* Return true *)
    { eexists; split. move => k.

      (* Access to the body of the function *)
      econstructor. econstructor; try by []. econstructor.

      eapply eval_Evar_global; try by [].
      apply Hfs. 
      by eapply deref_loc_reference.

      econstructor.
      - apply (eval_expr_private_nav_block (proj1 He)). all: try by [].
      - econstructor. econstructor. apply Hle. all: try by [].
      - apply eval_Enil.

      apply find_def_to_funct. apply Hfd. by [].

      econstructor. econstructor. econstructor; try by [].
      - apply list_norepet_nil.
      - constructor; try by []. rewrite //= => [[Hto | Hf]]; try by [].
        by apply neg_ident in Hto. econstructor. by []. 
        apply list_norepet_nil.
      - econstructor; by []. step_seq.

      star_step step_switch. econstructor. rewrite PTree.gso.
      apply PTree.gss. by apply neg_ident. by [].

      rewrite //= (id_zero_ext8 Hb8) (id_zero_ext8 Hid) -ZeqUInt;
        try apply (max_unsigned_256 Hb8).
      rewrite Hsl. step_seq. apply Htest.

      (* Return true *)
      star_step step_returnstate. step_refl. all: try by []. step_refl.
      simpl_trace. by []. }

    (* Return false *)
    eexists; split. move => k.

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    apply Hfs. 
    by eapply deref_loc_reference.

    econstructor.
    - apply (eval_expr_private_nav_block (proj1 He)). all: try by [].
    - econstructor. econstructor. apply Hle. all: try by [].
    - apply eval_Enil.

    apply find_def_to_funct. apply Hfd. by [].

    econstructor. econstructor. econstructor; try by [].
    - apply list_norepet_nil.
    - constructor; try by []. rewrite //= => [[Hto | Hf]]; try by [].
      by apply neg_ident in Hto. econstructor. by []. 
      apply list_norepet_nil.
    - econstructor; by []. step_seq.

    star_step step_switch. econstructor. rewrite PTree.gso.
    apply PTree.gss. by apply neg_ident. by [].

    rewrite //= (id_zero_ext8 Hb8) (id_zero_ext8 Hid) -ZeqUInt;
      try apply (max_unsigned_256 Hb8).
    rewrite Hsl. step_seq. apply Htest.

    (* Return false *)
    star_step step_break_seq.
    star_step step_skip_break_switch; try by right. step_skip_seq.
    star_step step_return_1. apply eval_expr_const_bool. all: try by [].
    rewrite false_eq_0 //=. star_step step_returnstate. step_refl.
    all: try by []. step_refl.

    simpl_trace. by [].
  Qed.

  (** ** On exit and on enter functions *)

  (** Select switch for on_enter and on_exit *)
  Lemma select_switch_on_gen_code:
    forall get_code code e,
    let idb := (get_nav_block e) in
    let blocks := FP_E.get_fp_blocks fpe in
    let b := FP_E.get_block fpe idb in
      fp_env_on_8 e
      -> get_code b = Some code
      -> get_code (FP_E.get_default_block fpe) = None
      -> exists s, 
        (seq_of_labeled_statement
          (select_switch
            (Int.unsigned (int_of_nat idb))
            (fold_right (gen_case_on get_code)
                (LScons None Sbreak LSnil) blocks)))
        = seq_of_labeled_statement (gen_case_on get_code b s).
  Proof.
    move => get_code code e idb blocks b He8 Hcode Hdef.

    destruct (le_lt_dec (FP_E.get_nb_blocks fpe - 1) idb) as [Hge | Hlt].

    (** Default code -> not possible *)
    rewrite /b /FP_E.get_block nth_overflow /= ?Hdef in Hcode.
    inversion Hcode. rewrite /FP_E.get_nb_blocks in Hge. to_nat Hge.
    ssrlia.

    (** Code of the block *)
    destruct (select_switch_gen_code_code Hsize Hlt Hcode) as [s Hs].
    eexists. by rewrite Hs /gen_case_on Hcode.
  Qed.

  Lemma select_switch_on_gen_no_code:
    forall (get_code: FP_E_WF.Def.fp_block -> option c_code) e,
    let idb := (get_nav_block e) in
    let blocks := FP_E.get_fp_blocks fpe in
    let b := FP_E.get_block fpe idb in
      fp_env_on_8 e
      -> get_code b = None
      -> (seq_of_labeled_statement
          (select_switch
            (Int.unsigned (int_of_nat idb))
            (fold_right (gen_case_on get_code)
                (LScons None Sbreak LSnil) blocks)))
          = (Sbreak @+@ Sskip).
  Proof.
    move => get_code e idb blocks b He8 Hcode.
    by rewrite /select_switch
      (select_switch_gen_no_code_code Hsize (nav_block8 He8) Hcode)
      select_switch_gen_no_code_default.
  Qed.

  Remark eq_get_code_on_enter:
    forall b,
    FP_E_WF.Def.get_block_on_enter b = FP_E.get_block_on_enter b.
  Proof. by destruct b. Qed. 

  Remark eq_get_code_on_exit:
    forall b,
    FP_E_WF.Def.get_block_on_exit b = FP_E.get_block_on_exit b.
  Proof. by destruct b. Qed. 

  Lemma step_on_enter:
    forall f e1 e2 le k,
      e1 ~menv~ (ge, e2)
      -> fp_env_on_8 e1
      -> Smallstep.star step2 ge
          (State f
            (Scall None (Evar CommonFP._on_enter_block
                          (Tfunction (Tcons tuchar Tnil) tvoid cc_default))
                          [:: Evar CommonFP._private_nav_block tuchar])
                      k empty_env le e2)
            (fp_trace_to_trace 
              (FP_E_WF.on_enter fpe (get_nav_block e1)))
            (State f Sskip k empty_env le e2).
  Proof.
    move => f e1 e2 le k He He8.
    rewrite /FP_E_WF.on_enter.

    have Hid8 := is_nav_block8 Hsize He8.

    (* Get the function *)
    have Hon := on_enter_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hon.
    destruct Hon as [b [Hfs Hfd]].

    (** Get private_nav_block variable *)
    have Hpnb := get_private_nav_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnb.
    destruct Hpnb as [b_pnb [Hfspnb Hfdpnb]].

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    apply Hfs. by eapply deref_loc_reference.
    econstructor; try by [].

    (* Eval expr *)
    { econstructor. apply eval_Evar_global; try by [].
      apply Hfspnb. econstructor; try by [].
      apply (proj2 ((MATCH_ENV.correct_private_nav_block He) _ Hfspnb)).
    } all: try by econstructor.

    apply find_def_to_funct. apply Hfd. by [].

    star_step step_internal_function. econstructor.
    - apply list_norepet_nil.
    - constructor; try by []. apply list_norepet_nil.
    - all: try by [].  econstructor.

    (* Execute the function *)
    star_step step_switch. rewrite -(cast_int_to_int8 Hid8).
    apply eval_expr_tmp_uint8. by [].

    remember (FP_E_WF.get_block fpe (get_nav_block e1))
      as block.

    eapply Smallstep.star_trans
      with (t1 := (fp_trace_to_trace 
        (FP_E_WF.on_enter fpe (get_nav_block e1)))).

    { destruct (FP_E.get_block_on_enter block) as [code|] eqn:Hcode;
        subst block.

      (* There is code to execute *)
      destruct (select_switch_on_gen_code He8 Hcode)
        as [s Hsw] => //.
      rewrite Hsw //= /gen_case_on Hcode //=.

      step_seq. step_seq. apply exec_arbitrary_C_code.
      step_skip_seq. star_step step_break_seq.
      star_step step_skip_break_switch; try by right.
      step_refl. all: try by []. step_refl.
      simpl_trace. by rewrite /FP_E_WF.on_enter
                                    /FP_E_WF.get_code_on_enter
                                    eq_get_code_on_enter Hcode.

      (* There is no code to execute *)
      rewrite (select_switch_on_gen_no_code He8 Hcode).
      step_seq. star_step step_break_seq; try by [].
      star_step step_skip_break_switch; try by right. step_refl. by [].
      simpl_trace. step_refl. by [].

      by rewrite /FP_E_WF.on_enter
                                  /FP_E_WF.get_code_on_enter
                                  eq_get_code_on_enter Hcode.
    }

    star_step step_skip_call; try by [].
    star_step step_returnstate. step_refl. all: try by [].

    simpl_trace.
  Qed.

  Lemma step_on_exit:
    forall f e1 e2 le k,
      e1 ~menv~ (ge, e2)
      -> fp_env_on_8 e1
      -> Smallstep.star step2 ge
          (State f
            (Scall None (Evar CommonFP._on_exit_block
                          (Tfunction (Tcons tuchar Tnil) tvoid cc_default))
                          [:: Evar CommonFP._private_nav_block tuchar])
                      k empty_env le e2)
            (fp_trace_to_trace 
              (FP_E_WF.on_exit fpe (get_nav_block e1)))
            (State f Sskip k empty_env le e2).
  Proof.
    move => f e1 e2 le k He He8.
    rewrite /FP_E_WF.on_exit.

    have Hid8 := is_nav_block8 Hsize He8.

    (* Get the function *)
    have Hon := on_exit_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hon.
    destruct Hon as [b [Hfs Hfd]].

    (** Get private_nav_block variable *)
    have Hpnb := get_private_nav_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnb.
    destruct Hpnb as [b_pnb [Hfspnb Hfdpnb]].

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    apply Hfs. by eapply deref_loc_reference.
    econstructor; try by [].

    (* Eval expr *)
    { econstructor. apply eval_Evar_global; try by [].
      apply Hfspnb. econstructor; try by [].
      apply (proj2 ((MATCH_ENV.correct_private_nav_block He) _ Hfspnb)).
    } all: try by econstructor.

    apply find_def_to_funct. apply Hfd. by [].

    star_step step_internal_function. econstructor.
    - apply list_norepet_nil.
    - constructor; try by []. apply list_norepet_nil.
    - all: try by [].  econstructor.

    (* Execute the function *)
    star_step step_switch. rewrite -(cast_int_to_int8 Hid8).
    apply eval_expr_tmp_uint8. by [].

    remember (FP_E_WF.get_block fpe (get_nav_block e1))
      as block.

    eapply Smallstep.star_trans
      with (t1 := (fp_trace_to_trace 
        (FP_E_WF.on_exit fpe (get_nav_block e1)))).

    { destruct (FP_E.get_block_on_exit block) as [code|] eqn:Hcode;
        subst block.

      (* There is code to execute *)
      destruct (select_switch_on_gen_code He8 Hcode)
        as [s Hsw] => //.
      rewrite Hsw //= /gen_case_on Hcode //=.

      step_seq. step_seq. apply exec_arbitrary_C_code.
      step_skip_seq. star_step step_break_seq.
      star_step step_skip_break_switch; try by right.
      step_refl. all: try by []. step_refl.
      simpl_trace. by rewrite /FP_E_WF.on_enter
                                  /FP_E_WF.on_exit
                                  /FP_E_WF.get_code_on_exit
                                  eq_get_code_on_exit Hcode.

      (* There is no code to execute *)
      rewrite (select_switch_on_gen_no_code He8 Hcode).
      step_seq. star_step step_break_seq; try by [].
      star_step step_skip_break_switch; try by right. step_refl. by [].
      simpl_trace. step_refl. by [].

      by rewrite /FP_E_WF.on_exit /FP_E_WF.get_code_on_exit
                                  eq_get_code_on_exit Hcode.
    }

    star_step step_skip_call; try by [].
    star_step step_returnstate. step_refl. all: try by [].

    simpl_trace.
  Qed.

  (** * Execution of Common FP functions *)

  (** ** Lemmas to store variables *)

  Lemma step_update_nav_block:
    forall e1 e1' e2 e id id_expr,
      e1 ~menv~ (ge, e2)
      -> correct_env e
      -> is_nat8 id
      -> update_nav_block e1 id = e1'
      -> typeof id_expr = tuchar \/ typeof id_expr = tint
      -> (forall le,
            eval_expr ge e le e2 id_expr (create_val id))
      -> exists e2' e2'',
        (forall f le k,
        Smallstep.star step2 ge
          (State f
              (Sassign (Evar CommonFP._private_nav_block tuchar)
                          id_expr)
              k e le e2) 
            E0
            (State f Sskip k e le e2'))
      /\ (forall f le k,
            Smallstep.star step2 ge
              (State f
                  (Sassign (Evar CommonFP._nav_block tuchar)
                  (Evar CommonFP._private_nav_block tuchar))
                  k e le e2') 
                E0
                (State f Sskip k e le e2''))
        /\ e1' ~menv~ (ge, e2'').
  Proof.
    move => e1 e1' e2 e id id_expr He Hce Hid8 Hup Htype Hexpr.

    (** Get private_nav_block variable *)
    have Hpnb := get_private_nav_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnb.
    destruct Hpnb as [b_pnb [Hfspnb Hfdpnb]].

    (** Store private_nav_block *)
    edestruct (Mem.valid_access_store e2 Mint8unsigned
                    b_pnb (Ptrofs.unsigned Ptrofs.zero)) as [m_b Hmem_b].
    { apply (proj1 ((MATCH_ENV.correct_private_nav_block He) _ Hfspnb)). }

    (** Get nav_block variable *)
    have Hnb := get_nav_block_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hnb.
    destruct Hnb as [b_nb [Hfsnb Hfdnb]].

    (** Store nav_block *)
    edestruct (Mem.valid_access_store m_b Mint8unsigned
          b_nb (Ptrofs.unsigned Ptrofs.zero)) as [m_b' Hmem_b'].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_b _ _
              _ _ (proj1 ((MATCH_ENV.correct_nav_block He) _ Hfsnb))). } 

    exists m_b, m_b'; split; try move => f le k .

    (** Assign private_nab_block *)
    star_step step_assign.
    - apply eval_Evar_global; try by [].
    - apply (none_private_nav_block Hce). apply Hfspnb.
    - apply Hexpr. destruct Htype as [H|H]; by rewrite H. 

    econstructor; try by []. apply Hmem_b. step_refl. simpl_trace.

    (** Assign nav_block *)
    split; try move => le k s.
    star_step step_assign.
    - apply eval_Evar_global; try by [].
    - apply (none_nav_block Hce). apply Hfsnb.
    - econstructor. apply eval_Evar_global; try by [].
      apply (none_private_nav_block Hce). apply Hfspnb.
      econstructor; try by []. rewrite /Mem.loadv.
      eapply Mem.load_store_same. apply Hmem_b.
    all: try by [].

    econstructor; try by []. apply Hmem_b'. step_refl.

    subst e1'.
    rewrite -(cast_int_to_int8 Hid8) (id_zero_ext8 Hid8) in Hmem_b'.
    by apply (update_nav_block_match He Hid8 Hfspnb Hfsnb Hmem_b Hmem_b').
  Qed.

  Lemma step_update_nav_stage:
    forall e1 e1' e2 e id id_expr,
      e1 ~menv~ (ge, e2)
      -> correct_env e
      -> is_nat8 id
      -> update_nav_stage e1 id = e1'
      -> typeof id_expr = tuchar \/ typeof id_expr = tint
      -> (forall le,
            eval_expr ge e le e2 id_expr (create_val id))
      -> exists e2' e2'',
        (forall f le k,
        Smallstep.star step2 ge
          (State f
              (Sassign (Evar CommonFP._private_nav_stage tuchar)
                          id_expr)
              k e le e2) 
            E0
            (State f Sskip k e le e2'))
      /\ (forall f le k,
            Smallstep.star step2 ge
              (State f
                  (Sassign (Evar CommonFP._nav_stage tuchar)
                  (Evar CommonFP._private_nav_stage tuchar))
                  k e le e2') 
                E0
                (State f Sskip k e le e2''))
        /\ e1' ~menv~ (ge, e2'').
  Proof.
    move => e1 e1' e2 e id id_expr He Hce Hid8 Hup Htype Hexpr.

    (** Get private_nav_stage variable *)
    have Hpns := get_private_nav_stage fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpns.
    destruct Hpns as [b_pns [Hfspns Hfdpns]].

    (** Store private_nav_stage *)
    edestruct (Mem.valid_access_store e2 Mint8unsigned
                    b_pns (Ptrofs.unsigned Ptrofs.zero)) as [m_b Hmem_b].
    { apply (proj1 ((MATCH_ENV.correct_private_nav_stage He) _ Hfspns)). }

    (** Get nav_stage variable *)
    have Hns := get_nav_stage_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hns.
    destruct Hns as [b_ns [Hfsns Hfdns]].

    (** Store nav_block *)
    edestruct (Mem.valid_access_store m_b Mint8unsigned
          b_ns (Ptrofs.unsigned Ptrofs.zero)) as [m_b' Hmem_b'].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_b _ _
              _ _ (proj1 ((MATCH_ENV.correct_nav_stage He) _ Hfsns))). } 

    exists m_b, m_b'; split; try move => f le k .

    (** Assign private_nav_stage *)
    star_step step_assign.
    - apply eval_Evar_global; try by [].
    - apply (none_private_nav_stage Hce). apply Hfspns.
    - apply Hexpr. destruct Htype as [H|H]; by rewrite H.  

    econstructor; try by []. apply Hmem_b. step_refl. simpl_trace.

    (** Assign nav_stage *)
    split; try move => le k s.
    star_step step_assign.
    - apply eval_Evar_global; try by [].
    - apply (none_nav_stage Hce). apply Hfsns.
    - econstructor. apply eval_Evar_global; try by [].
      apply (none_private_nav_stage Hce). apply Hfspns.
      econstructor; try by []. rewrite /Mem.loadv.
      eapply Mem.load_store_same. apply Hmem_b.
    all: try by [].

    econstructor; try by []. apply Hmem_b'. step_refl.

    subst e1'.
    rewrite -(cast_int_to_int8 Hid8) (id_zero_ext8 Hid8) in Hmem_b'.
    by apply (update_nav_stage_match He Hid8 Hfspns Hfsns Hmem_b Hmem_b').
  Qed.


  Lemma step_update_last_block:
    forall e1 e1' e2 e id id_expr,
      e1 ~menv~ (ge, e2)
      -> correct_env e
      -> fp_env_on_8 e1
      -> is_nat8 id
      -> update_last_block e1 id = e1'
      -> typeof id_expr = tuchar
      -> (forall le,
            eval_expr ge e le e2 id_expr (create_val id))
      -> exists e2' e2'',
        (forall f le k,
        Smallstep.star step2 ge
          (State f
              (Sassign (Evar CommonFP._private_last_block tuchar)
                          id_expr)
               k e le e2) 
            E0
            (State f Sskip k e le e2'))
      /\ (forall f le k,
            Smallstep.star step2 ge
              (State f
                  (Sassign (Evar CommonFP._last_block tuchar)
                  (Evar CommonFP._private_last_block tuchar))
                  k e le e2') 
                E0
                (State f Sskip k e le e2''))
        /\ e1' ~menv~ (ge, e2'').
  Proof.
    move => e1 e1' e2 e id id_expr He Hce He8 Hid8 Hup Htype Hexpr.

    (** Get private_LastBlock variable *)
    have Hplb := get_private_last_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hplb.
    destruct Hplb as [b_plb [Hfsplb Hfdplb]].

    (** Store private_last_block *)
    edestruct (Mem.valid_access_store e2 Mint8unsigned
                    b_plb (Ptrofs.unsigned Ptrofs.zero)) as [m_b Hmem_b].
    { apply (proj1 ((MATCH_ENV.correct_private_last_block He) _ Hfsplb)). }

    (** Get last_block variable *)
    have Hlb := get_last_block_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hlb.
    destruct Hlb as [b_lb [Hfslb Hfdlb]].

    (** Store last_block *)
    edestruct (Mem.valid_access_store m_b Mint8unsigned
          b_lb (Ptrofs.unsigned Ptrofs.zero)) as [m_b' Hmem_b'].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_b _ _
              _ _ (proj1 ((MATCH_ENV.correct_last_block He) _ Hfslb))). } 

    exists m_b, m_b'; split; try move => f le k .

    (** Assign private_last_block *)
    star_step step_assign.
    - apply eval_Evar_global; try by [].
    - apply (none_private_last_block Hce). apply Hfsplb.
    - apply Hexpr. by rewrite Htype. 

    econstructor; try by []. apply Hmem_b. step_refl. simpl_trace.

    (** Assign private_block *)
    split; try move => le k s.
    star_step step_assign.
    - apply eval_Evar_global; try by [].
    - apply (none_last_block Hce). apply Hfslb.
    - econstructor. apply eval_Evar_global; try by [].
      apply (none_private_last_block Hce). apply Hfsplb.
      econstructor; try by []. rewrite /Mem.loadv.
      eapply Mem.load_store_same. apply Hmem_b.
    all: try by [].

    econstructor; try by []. apply Hmem_b'. step_refl.

     subst e1'.
    rewrite -(cast_int_to_int8 Hid8) (id_zero_ext8 Hid8) in Hmem_b'.
    by apply (update_last_block_match He Hid8 Hfsplb Hfslb Hmem_b Hmem_b').
  Qed.

  Lemma step_update_last_stage:
    forall e1 e1' e2,
      e1 ~menv~ (ge, e2)
      -> fp_env_on_8 e1
      -> update_last_stage e1
          (get_nav_stage e1) = e1'
      -> exists e2', (forall le k,
      Smallstep.star step2 ge 
      (State CommonFP.f_nav_goto_block  Sskip
      (Kseq (
        Sassign (Evar CommonFP._private_last_stage tuchar)
          (Evar CommonFP._private_nav_stage tuchar) @+@
        Sassign (Evar CommonFP._last_stage tuchar)
          (Evar CommonFP._private_last_stage tuchar)) k) empty_env le e2) 
          E0
          (State CommonFP.f_nav_goto_block Sskip k empty_env le e2'))
        /\ e1' ~menv~ (ge, e2')
        /\ fp_env_on_8 e1'.
  Proof.
    move => e1 e1' e2 He He8 Hup.

    (** Get private_nav_block variable *)
    have Hpns := get_private_nav_stage fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpns.
    destruct Hpns as [b_pns [Hfspns Hfdpns]].

    (** Get private_last_stage variable *)
    have Hpls := get_private_last_stage fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpls.
    destruct Hpls as [b_pls [Hfspls Hfdpls]].

    (** Store private_last_block *)
    edestruct (Mem.valid_access_store e2 Mint8unsigned
                    b_pls (Ptrofs.unsigned Ptrofs.zero)) as [m_s Hmem_s].
    { apply (proj1 ((MATCH_ENV.correct_private_last_stage He) _ Hfspls)). }

    (** Get last_block variable *)
    have Hls := get_last_stage_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hls.
    destruct Hls as [b_ls [Hfsls Hfdls]].

    (** Store last_block *)
    edestruct (Mem.valid_access_store m_s Mint8unsigned
          b_ls (Ptrofs.unsigned Ptrofs.zero)) as [m_s' Hmem_s'].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_s _ _
              _ _ (proj1 ((MATCH_ENV.correct_last_stage He) _ Hfsls))). } 

    exists m_s'; split. move => le k.

  (** Assign private_last_stage *)
    step_skip_seq. step_seq. star_step step_assign.
    - apply eval_Evar_global; try by []. apply Hfspls.
    - econstructor. apply eval_Evar_global; try by []. apply Hfspns.
      econstructor; try by []. rewrite /Mem.loadv.
      apply (proj2 ((MATCH_ENV.correct_private_nav_stage He) _ Hfspns)).
    all: try by [].

    econstructor; try by []. apply Hmem_s. step_skip_seq.

    (** Assign private_block *)
    star_step step_assign.
    - apply eval_Evar_global; try by []. apply Hfsls.
    - econstructor. apply eval_Evar_global; try by []. apply Hfspls.
      econstructor; try by []. rewrite /Mem.loadv.
      eapply Mem.load_store_same. apply Hmem_s.
    all: try by [].

    econstructor; try by []. apply Hmem_s'.
    step_refl. all: try by []. step_refl.

    split.

    have Hid8 := nav_stage8 He8; subst e1'.
    rewrite -(cast_int_to_int8 Hid8) (id_zero_ext8 Hid8) in Hmem_s'.
    by apply (update_last_stage_match He Hid8 Hfspls Hfsls Hmem_s Hmem_s').

    by apply (update_last_stage8 He8 Hup).
  Qed.

  Lemma step_get_NavBlock:
    forall e le e1 e2,
     correct_env e
      -> fp_env_on_8 e1
      -> e1 ~menv~ (ge, e2)
      -> forall k, Smallstep.star step2 ge
            (State f tmpNavBlock_stmt k e le e2)
            E0
            (State f Sskip k e (update_le_NavBlock e1 le) e2).
  Proof.
    move => e le e1 e2 Hce He8 He k.
    rewrite /tmpNavBlock_stmt.

    have Hnav := get_nav_block_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hnav.
    destruct Hnav as [b [Hfs Hfd]].

    destruct (get_symb_private_nav_block fpe gvars) as [b' Hfs'].

    have Hfs'' := Hfs'.
    apply (MATCH_ENV.correct_private_nav_block He) in Hfs''.
    destruct Hfs'' as [Hw Hl].

    econstructor. econstructor; try by []. econstructor.
  
    eapply eval_Evar_global; try by [].
    apply (none_get_nav_block Hce).
    apply Hfs. 
    by eapply deref_loc_reference.

    econstructor; try by [].
    apply find_def_to_funct. apply Hfd. by [].

    econstructor. econstructor. econstructor; try by [].
    all: try apply list_norepet_nil.
    apply alloc_variables_nil.
    econstructor. econstructor. econstructor.
    eapply eval_Evar_global.
    apply (none_private_nav_block Hcempt).

    apply Hfs'. apply deref_loc_value
      with (chunk := Mint8unsigned); try by [].
    apply Hl.

    apply cast_val_casted. constructor.
    rewrite //=.  apply id_zero_ext8.
    replace e1 with (` (exist _ e1 He8)); try by [].
    apply (is_nav_block8 Hsize). all: try by [].

    econstructor. econstructor. step_refl.

    all: by [].
  Qed.

  Lemma step_get_NavStage:
    forall e le e1 e2,
      correct_env e
      -> fp_env_on_8 e1
      -> e1 ~menv~ (ge, e2)
      -> forall k, Smallstep.star step2 ge
          (State f tmpNavStage_stmt k e le e2)
          E0
          (State f Sskip k e (update_le_NavStage e1 le) e2).
  Proof.
    move => e le e1 e2 Hce He8 He k.
    rewrite /tmpNavStage_stmt.

    have Hnav := get_nav_stage_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hnav.
    destruct Hnav as [b [Hfs Hfd]].

    destruct (get_symb_private_nav_stage fpe gvars) as [b' Hfs'].

    have Hfs'' := Hfs'.
    apply (MATCH_ENV.correct_private_nav_stage He) in Hfs''.
    destruct Hfs'' as [Hw Hl].

    econstructor. econstructor; try by []. econstructor.
  
    eapply eval_Evar_global; try by [].
    apply (none_get_nav_stage Hce).
    apply Hfs. 
    by eapply deref_loc_reference.

    econstructor; try by [].
    apply find_def_to_funct. apply Hfd. by [].

    econstructor. econstructor. econstructor; try by [].
    all: try apply list_norepet_nil.
    apply alloc_variables_nil.
    econstructor. econstructor. econstructor.
    eapply eval_Evar_global.
    apply (none_private_nav_stage Hcempt).

    apply Hfs'. apply deref_loc_value
      with (chunk := Mint8unsigned); try by [].
    apply Hl.

    apply cast_val_casted. constructor.
    rewrite //=.  apply id_zero_ext8.
    replace e1 with (` (exist _ e1 He8)); try by [].
    by apply is_nav_stage8. by [].

    econstructor. econstructor. step_refl.

    all: by [].
  Qed.

  Lemma get_nb_blocks:
    forall le m,
    MATCH_ENV.correct_const_access ge m CommonFP._nb_blocks
    -> eval_expr ge empty_env le m
        (Evar CommonFP._nb_blocks tuchar)
        (create_val (FP_E.get_nb_blocks fpe)).
  Proof.
    move => le m Hc.
    destruct (get_symb_nb_blocks fpe gvars) as [b Hfs].

    econstructor. apply eval_Evar_global; try by [].  apply Hfs.
    econstructor; try by [].

    apply (get_load_nb_blocks Hc Hfs).
  Qed.

  Lemma step_set_NavStage:
    forall e le e1 e2 id,
      correct_env e
      -> is_nat8 id
      -> e1 ~cenv~ (ge, e2)
      ->exists e1' e2', 
            ` e1' =  update_nav_stage (` e1) id
            /\    (forall k, Smallstep.star step2 ge
                        (State f (setNavStage id) k e le (e2.m))
                        E0
                        (State f Sskip k e le (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e le e1 e2 id Hce He8 He.
    rewrite /setNavStage.

    (** Get SetNavStage function *)
    have Hnav := set_nav_stage_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hnav.
    destruct Hnav as [b [Hfs Hfd]].

    (** Get private_NavStage variable *)
    have Hpnav := get_private_nav_stage fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnav.
    destruct Hpnav as [b_p [Hfsp Hfdp]].

    (** Store private_nav_stage *)
    edestruct (Mem.valid_access_store (e2.m) Mint8unsigned
                   b_p (Ptrofs.unsigned Ptrofs.zero)) as [e2' Hmem].
    { apply (proj1 ((MATCH_ENV.correct_private_nav_stage (proj1 He)) _ Hfsp)). }

    (** Get NavStage variable *)
    have Hvnav := get_nav_stage_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hvnav.
    destruct Hvnav as [b_v [Hfsv Hfdv]].

    (** Store private_nav_stage *)
    edestruct (Mem.valid_access_store e2' Mint8unsigned
          b_v (Ptrofs.unsigned Ptrofs.zero)) as [e2'' Hmem'].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem _ _
              _ _ (proj1 ((MATCH_ENV.correct_nav_stage (proj1 He)) _ Hfsv))). }

    replace le with (set_opttemp None Vundef le);
      last first. by [].

    (** Create e1' *)
    remember (update_nav_stage (` e1) id) as e1'_m eqn:Heq.
    symmetry in Heq.
    set e1' := exist _ _ (update_nav_stage8 (proj2_sig e1) He8 Heq).

    (** Verification of the code *)
    exists e1', (create_fp_cenv e2''
      (fp_trace_to_trace (get_trace (` e1')))); split => //;
      split; [move => k | ].
    do 2 (econstructor; try by []). econstructor; try by [].

    eapply eval_Evar_global.
    apply (none_set_nav_stage Hce).
    apply Hfs.
    by eapply deref_loc_reference.

    (* Eval expr params *)
    { eapply eval_Econs; try by [].
      econstructor.
      by []. constructor. }

    apply find_def_to_funct. apply Hfd.
    by [].

    do 2 econstructor; try by []; rewrite //=.
    - apply list_norepet_nil.
    - constructor; try by []. apply list_norepet_nil.
    - econstructor; by [].

   (* Execution of the code *)
    do 3 econstructor. eapply eval_Evar_global; try by [].
    apply Hfsp. econstructor; try by [].
    apply PTree.gss. apply cast_val_casted. econstructor.
    rewrite //=. apply Int.zero_ext_idem; try ssrlia.
    econstructor; try by []. rewrite /Mem.storev.
    apply Hmem.

    repeat econstructor. do 2 econstructor.
    eapply eval_Evar_global; try by [].
    apply Hfsv. econstructor; try by [].
    apply PTree.gss. apply cast_val_casted. econstructor.
    rewrite //=. apply Int.zero_ext_idem; try ssrlia.
    econstructor; try by []. rewrite /Mem.storev.
    apply Hmem'.

    econstructor; try by [].
    econstructor. econstructor. step_refl.

    all: try by [].

    (** Prove the equivalence *)
    split.
    - rewrite /e1' /= -Heq.
      by apply (update_nav_stage_match
          (proj1 He) He8 Hfsp Hfsv Hmem Hmem').
    - by apply trace_generated_match.
  Qed. 

  Lemma step_set_NavStage_unchanged:
    forall e le e1 e2,
      let id := get_nav_stage (` e1) in
      correct_env e
      -> e1 ~cenv~ (ge, e2)
      -> exists e2', (forall k,
        Smallstep.star step2 ge
          (State f (setNavStage id) k e le (e2.m))
          E0
          (State f Sskip k e le (e2'.m)))
        /\ e1 ~cenv~ (ge, e2').
  Proof.
    move =>  e le e1 e2 id Hce He.

    destruct (step_set_NavStage le Hce
          (nav_stage8 (proj2_sig e1)) He) as [e1' [e2' [Heq [Hs He']]]].

    exists e2'; split => //.
    destruct He' as [He' Ht]; rewrite Heq /= ?update_stage_id in He', Ht.
    by split.
  Qed.

  Lemma step_nextstage:
    forall e1 e1' e2 e le,
      let idb := get_nav_block (` e1) in
      e1 ~cenv~ (ge, e2)
      -> correct_env e
      -> next_stage (` e1) = (` e1')
      -> exists e2', (forall k,
        Smallstep.star step2 ge (State f nextStage k e le (e2.m)) 
                                      (fp_trace_to_trace  [:: init_stage])
                                      (State f Sskip k e le (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 e le idb He Hce Hnext.
    rewrite /nextStage. rewrite /next_stage in Hnext.

    have Hfun := next_stage_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hfun.
    destruct Hfun as [b [Hfs Hfd]].

    (* is_nat8 variable*)
    have H8:= nav_stage8 (proj2_sig e1).
    have H8':= nav_stage8 (proj2_sig e1').
      rewrite (update_stage_change_stage Hnext) in H8'.

    (** Store *)
    have Hpns := get_private_nav_stage fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpns.
    destruct Hpns as [b_pns [Hfs_pns Hfd_pns]].

    edestruct (Mem.valid_access_store (e2.m) Mint8unsigned
      b_pns (Ptrofs.unsigned Ptrofs.zero)) as [e2' Hpmem].
    { apply (proj1
      ((MATCH_ENV.correct_private_nav_stage (proj1 He)) _ Hfs_pns)). }

    have Hns := get_nav_stage_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hns.
    destruct Hns as [b_ns [Hfs_ns Hfd_ns]].

    edestruct (Mem.valid_access_store e2' Mint8unsigned
    b_ns (Ptrofs.unsigned Ptrofs.zero)) as [e2'' Hmem].
    { eapply Mem.store_valid_access_1.
      - apply Hpmem.
      - apply
        (proj1 ((MATCH_ENV.correct_nav_stage (proj1 He)) _ Hfs_ns)). }

    set e2'_f := create_fp_cenv e2''
      (fp_trace_to_trace (get_trace (` e1'))).
    exists e2'_f; split. move => k.

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    apply (none_next_stage Hce).
    apply Hfs. 
    by eapply deref_loc_reference.

    econstructor.  apply find_def_to_funct. apply Hfd. by [].

    econstructor. econstructor. econstructor; try by [].
    - apply list_norepet_nil.
    - apply list_norepet_nil.
    - econstructor; by [].

    (* Execute the function *)
    step_seq. star_step step_assign.
    - apply eval_Evar_global; try by []. apply Hfs_pns.
    -  econstructor; try by [].
      *  econstructor. apply eval_Evar_global; try by [].
      apply Hfs_pns. econstructor; try by [].
      apply (proj2
        ((MATCH_ENV.correct_private_nav_stage (proj1 He)) _ Hfs_pns)).
      * econstructor. econstructor. by [].
    - econstructor. by []. rewrite -cast_int_to_int32 //=
                                  (incr_int_val H8') (id_zero_ext8 H8'). 
       apply Hpmem.  step_skip_seq.

    step_seq. star_step step_assign.
    - apply eval_Evar_global; try by []. apply Hfs_ns.
    - econstructor. apply eval_Evar_global; try by [].
      apply Hfs_pns. econstructor; try by [].
      rewrite /Mem.loadv; apply (Mem.load_store_same _ _ _ _ _ _ Hpmem).
      by []. 
    - econstructor. by []. apply Hmem. step_skip_seq.

    apply exec_arbitrary_C_code. all: try by []. step_refl.
    econstructor. econstructor; try by []. econstructor. econstructor.
    step_refl.  all: try by [].

    (* Prove the equivalence *)
    split; try by apply trace_generated_match.
    rewrite -Hnext.
    eapply (update_nav_stage_match (proj1 He) H8' Hfs_pns Hfs_ns).
    - rewrite //= (id_zero_ext8 H8'). apply Hpmem.
    - rewrite (id_zero_ext8 H8') in Hmem. apply Hmem.
  Qed.

  Lemma step_change_block_middle:
    forall e1 e1' e2,
    e1 ~menv~ (ge, e2)
    -> fp_env_on_8 e1
    -> mk_fp_env
        (get_nav_block e1) (get_nav_stage e1)
        (get_nav_block e1) (get_nav_stage e1)
         (get_trace e1) = e1'
    -> exists e2', (forall le k,
  Smallstep.star step2 ge
    (State CommonFP.f_nav_goto_block
        (Sassign (Evar CommonFP._private_last_block tuchar)
        (Evar CommonFP._private_nav_block tuchar) @+@
      Sassign (Evar CommonFP._last_block tuchar)
        (Evar CommonFP._private_last_block tuchar) @+@
      Sassign (Evar CommonFP._private_last_stage tuchar)
        (Evar CommonFP._private_nav_stage tuchar) @+@
      Sassign (Evar CommonFP._last_stage tuchar)
        (Evar CommonFP._private_last_stage tuchar)) 
          k empty_env le e2) 
      E0
      (State CommonFP.f_nav_goto_block Sskip k empty_env le e2'))
    /\ e1' ~menv~ (ge, e2').
  Proof.
    move => e1 e1' e2 He He8 Heq.

    remember (update_last_block e1
          (get_nav_block e1)) as  e1b; symmetry in Heqe1b.
    destruct (step_update_last_block He Hcempt He8
                    (is_nav_block8 Hsize He8) Heqe1b type_of_private_nav_block
                    (eval_expr_private_nav_block He))
      as [e2'1 [e2' [Hpv [Hv He2']]]].
    have He8' := update_last_block8 He8 Heqe1b.

    remember (update_last_stage e1b
      (get_nav_stage e1b)) as  e1s; symmetry in Heqe1s.
    destruct (step_update_last_stage He2' He8' Heqe1s)
      as [e2'' [Hexec_s [He2'' He8'']]].

    have Heq': e1s = e1'.
    { rewrite -Heqe1s -Heqe1b -Heq //=. }
    rewrite -Heq'.

    exists e2''; split; try by []; move => le k.
    step_trans. step_seq. apply Hpv. step_skip_seq.
    step_seq. apply Hv. apply Hexec_s.

    all: simpl_trace. step_refl.
  Qed.

  Lemma step_nav_init_block:
    forall e1_init e1' e2 id,
      let e1 := update_nav_block e1_init id in
      e1 ~menv~ (ge, e2)
      -> fp_env_on_8 e1_init
      -> is_nat8 id
      -> update_nav_stage
            (update_nav_block e1 
                (FP_E_WF.Common.normalise_block_id fpe id)) 0 = e1'
      -> exists e2', (forall le k,
        let o :=
          ([:: reset_time; init_stage]
          ++ FP_E_WF.Common.on_enter fpe 
                (FP_E_WF.Common.normalise_block_id fpe id))%list in
        Smallstep.star step2 ge
        (State CommonFP.f_nav_goto_block
            (Scall None
            (Evar CommonFP._nav_init_block (Tfunction Tnil tvoid cc_default))
              [::]) k empty_env le e2) 
          (fp_trace_to_trace o)
          (State CommonFP.f_nav_goto_block Sskip k empty_env le e2'))
        /\ e1' ~menv~ (ge, e2').
  Proof.
    move => e1_init e1' e2 id e1 He He8 Hid8 He1'.
    rewrite /FP_E_WF.Common.normalise_block_id in He1'.

    have Heq :get_nav_block e1 = id.
    { rewrite /e1. eapply update_block_change_block.
      reflexivity. }

    (* Get the function *)
    have Hinit := nav_init_block_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hinit.
    destruct Hinit as [bi [Hfsi Hfdi]].

    (** Get private_nav_block variable *)
    have Hpnb := get_private_nav_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnb.
    destruct Hpnb as [b_pnb [Hfspnb Hfdpnb]].

    destruct (FP_E.get_nb_blocks fpe <= id) eqn:Hin.

    (* Execute the if *)
    { remember (update_nav_block e1
              (FP_E.get_nb_blocks fpe -1)) as  e1''1; symmetry in Heqe1''1.
      have Htype:  typeof nb_blocks_minus1 = tint by [].
      have Hexpr:= eval_expr_nb_blocks_minus1
        Hcempt (MATCH_ENV.correct_nb_blocks He).
      edestruct (@step_update_nav_block _ _ _ _ _ nb_blocks_minus1
                    He Hcempt (default_block8 fps) Heqe1''1)
        as [e2'1 [e2' [Hpv [Hv He2']]]]; try by right.
      apply Hexpr.

      remember (update_nav_stage e1''1
          0) as  e1''; symmetry in Heqe1''.
      remember (Econst_int (Int.repr 0) tint) as id_expr.
      have Hexpr': forall le,
                eval_expr ge empty_env le e2' id_expr (create_val 0).
      { rewrite Heqid_expr => le. econstructor. }
      destruct (@step_update_nav_stage _ _ _ _ _ id_expr He2' Hcempt
                      zero8 Heqe1'')
        as [e2''1 [e2'' [Hpv' [Hv' He2'']]]]; subst id_expr.

      by right. apply Hexpr'.

      eexists; split. move => k.

      (* Access to the body of the function *)
      econstructor. econstructor; try by []. econstructor.

      eapply eval_Evar_global; try by [].
      apply Hfsi. 
      by eapply deref_loc_reference.

      econstructor. apply find_def_to_funct. apply Hfdi. by [].

      econstructor. econstructor. econstructor; try by [].
      - apply list_norepet_nil.
      - apply list_norepet_nil.
      - econstructor; by [].

      (* Evalutation of the if *)
      step_seq. star_step step_ifthenelse.
      econstructor. econstructor.
      - apply eval_Evar_global; try by []. apply Hfspnb.
      econstructor; try by []. rewrite /Mem.loadv.
      apply (proj2 ((MATCH_ENV.correct_private_nav_block He) _ Hfspnb)).
      - apply (get_nb_blocks _ (MATCH_ENV.correct_nb_blocks He)).
      - econstructor. apply ge_true_impl_ge in Hin.
        rewrite //= (destruct_int_lt_ne (get_nb_blocks8 Hsize) Hid8 Hin)
                  //.

      rewrite Int.eq_false //=; try by [].

      (* Execution if *)
      step_seq. apply Hpv. step_skip_seq.
      apply Hv. all: try by []. step_skip_seq.

      (* Execution assigns *)
      step_seq. apply Hpv'. step_skip_seq.
      step_seq. apply Hv'. step_skip_seq.

      (* Assign time *)
      step_seq. apply eq_exec_assign_time. step_skip_seq.

      (* Init stage *)
      step_seq. apply exec_arbitrary_C_code. step_skip_seq.

      (* On enter *)
      step_trans. apply (step_on_enter _ _ _ He2'').
      { have H: fp_env_on_8 e1''1.
        { rewrite -Heqe1''1 /e1.
          by apply update_nav_block_twice8. }
        by apply (update_nav_stage8 H zero8). }

      all: try by [].

      star_step step_skip_call; try by [].
      star_step step_returnstate. step_refl. by [].

      simpl_trace.
      all: apply ge_true_impl_ge in Hin; apply leb_correct in Hin.

      by rewrite -Heq fold_nav_block -(update_stage_nc_block Heqe1'')
                      (update_block_change_block Heqe1''1)
                      /FP_E_WF.Common.normalise_block_id Hin.

      replace e1' with e1''; try by [].
      by rewrite -Heqe1'' -Heqe1''1 -He1' Hin.
    }

    (* Do not enter in the if *)
    remember (update_nav_stage e1
                      0) as  e1''; symmetry in Heqe1''.
    remember (Econst_int (Int.repr 0) tint) as id_expr.
    have Hexpr: forall le,
      eval_expr ge empty_env le e2 id_expr (create_val 0).
    { rewrite Heqid_expr => le. econstructor. }
    destruct (@step_update_nav_stage _ _ _ _ _ id_expr He Hcempt
                  zero8 Heqe1'')
    as [e2'1 [e2' [Hpv [Hv He2']]]]; subst id_expr; try by right.

    apply Hexpr.

    eexists; split. move => k.

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    apply Hfsi. 
    by eapply deref_loc_reference.

    econstructor. apply find_def_to_funct. apply Hfdi. by [].

    econstructor. econstructor. econstructor; try by [].
    - apply list_norepet_nil.
    - apply list_norepet_nil.
    - econstructor; by [].

    (* Evalutation of the if *)
    step_seq. star_step step_ifthenelse.
    econstructor. econstructor.
    - apply eval_Evar_global; try by []. apply Hfspnb.
      econstructor; try by []. rewrite /Mem.loadv.
      apply (proj2 ((MATCH_ENV.correct_private_nav_block He) _ Hfspnb)).
    - apply (get_nb_blocks _ (MATCH_ENV.correct_nb_blocks He)).
    - econstructor. apply ge_false_impl_le in Hin.
      rewrite //= (destruct_int_lt Hid8 (get_nb_blocks8 Hsize) Hin) //=.

    rewrite Int.eq_true //=. step_skip_seq.

    (* Execution assigns *)
    step_seq. apply Hpv. step_skip_seq.
    step_seq. apply Hv. step_skip_seq.

    (* Assign time *)
    step_seq. apply eq_exec_assign_time. step_skip_seq.

    (* Init stage *)
    step_seq. apply exec_arbitrary_C_code. step_skip_seq.

    (* On enter *)
    apply (step_on_enter _ _ _ He2').
    apply ge_false_impl_le in Hin. 
    have He8': fp_env_on_8 e1.
    { rewrite /e1. by apply (@update_nav_block8 _ _ id He8). }
    by apply (update_nav_stage8 He8' zero8).

    all: try by [].

    star_step step_skip_call; try by [].
    star_step step_returnstate. step_refl. by [].

    simpl_trace.
    all: apply ge_false_impl_le in Hin; apply leb_correct_conv in Hin.
    by rewrite -Heq fold_nav_block -(update_stage_nc_block Heqe1'')
                          /FP_E_WF.Common.normalise_block_id Hin.

    replace e1' with e1''; try by [].
    by rewrite -Heqe1'' -He1' Hin. 
  Qed.

  Lemma step_change_block_end:
    forall e1 e1' e2 id,
    e1 ~cenv~ (ge, e2)
    -> update_nav_stage 
        (update_nav_block (` e1)
         (FP_E_WF.Common.normalise_block_id fpe id)) 0 = (` e1')
    -> is_nat8 id
    -> exists e2', (forall le k,
        let o := c_change_block fpe
        (get_nav_block (` e1)) id in
        le ! CommonFP._b = Some (create_val id)
    -> Smallstep.star step2 ge
      (State CommonFP.f_nav_goto_block
          (Scall None
            (Evar CommonFP._on_exit_block
              (Tfunction (Tcons tuchar Tnil) tvoid cc_default))
            [:: Evar CommonFP._private_nav_block tuchar] @+@
          Sassign (Evar CommonFP._private_nav_block tuchar)
            (Etempvar CommonFP._b tuchar) @+@
          Sassign (Evar CommonFP._nav_block tuchar)
            (Evar CommonFP._private_nav_block tuchar) @+@
          Scall None
          (Evar CommonFP._nav_init_block (Tfunction Tnil tvoid cc_default))
            [::]) k empty_env le (e2.m)) 
        (fp_trace_to_trace o)
        (State CommonFP.f_nav_goto_block Sskip k empty_env le (e2'.m)))
      /\ e1' ~cenv~ (ge, e2').
  Proof. 
    move => e1 e1'_f e2 id [He Ht] Heqe1'_f Hid8.
    have He8 := proj2_sig e1.

    (** Get private_nav_block variable *)
    have Hpnb := get_private_nav_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnb.
    destruct Hpnb as [b_pnb [Hfspnb Hfdpnb]].

    (** Store private_nav_block *)
    edestruct (Mem.valid_access_store (e2.m) Mint8unsigned
                  b_pnb (Ptrofs.unsigned Ptrofs.zero)) 
                  with (v := create_val id) as [e2'1 Hmem].
    { apply (proj1 ((MATCH_ENV.correct_private_nav_block He) _ Hfspnb)). }

    (** Get nav_block variable *)
    have Hnb := get_nav_block_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hnb.
    destruct Hnb as [b_nb [Hfsnb Hfdnb]].

    (** Store nav_block *)
    edestruct (Mem.valid_access_store e2'1 Mint8unsigned
          b_nb (Ptrofs.unsigned Ptrofs.zero)) 
          with (v := create_val id) as [e2' Hmem_e2'].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem _ _
              _ _ (proj1 ((MATCH_ENV.correct_nav_block He) _ Hfsnb))). }

    rewrite /create_val (cast_int_to_int8 Hid8) in Hmem.
    rewrite /create_val (cast_int_to_int8 Hid8) in Hmem_e2'.
    have He2' :=
      (update_nav_block_match He Hid8 Hfspnb Hfsnb Hmem Hmem_e2').

    remember
      (update_nav_stage
        (update_nav_block (` e1) 
            (FP_E_WF.Common.normalise_block_id fpe id)) 0)
      as e1'; symmetry in Heqe1'.
    destruct (step_nav_init_block He2' He8 Hid8 Heqe1')
      as [m2'' [Hinit He2'']].
    set e2'' := create_fp_cenv m2'' (fp_trace_to_trace (get_trace e1')).

    eexists; split. move => le k o Hle.

    step_seq. apply (step_on_exit _ le _ He He8). step_skip_seq.

    (** Assign private_last_block *)
    step_seq. star_step step_assign.
    - apply eval_Evar_global; try by []. apply Hfspnb.
    - econstructor. apply Hle; try by []. econstructor.

    econstructor; try by []. apply Hmem. step_skip_seq.

    (** Assign private_block *)
    step_seq. star_step step_assign.
    - apply eval_Evar_global; try by []. apply Hfsnb.
    - econstructor. apply eval_Evar_global; try by []. apply Hfspnb.
      econstructor; try by []. rewrite /Mem.loadv.
      eapply Mem.load_store_same. apply Hmem.
    all: try by [].

    econstructor; try by [].
    rewrite -(cast_int_to_int8 Hid8) (id_zero_ext8 Hid8).
    apply Hmem_e2'. step_skip_seq. step_refl. all: try by [].

    replace m2'' with (e2'' .m); try by rewrite /e2''. step_refl.

    simpl_trace.

    split; rewrite -Heqe1'_f /e2'' //=.
    apply trace_generated_match.
  Qed.

  Lemma step_change_block:
    forall e1 e1' e2 id,
    e1 ~cenv~ (ge, e2)
    -> change_block fpe (` e1) id = (` e1')
    -> is_nat8 id
    -> exists e2' le', (forall k,
        let o := c_change_block fpe
              (get_nav_block (` e1)) id in
  Smallstep.star step2 ge
    (State CommonFP.f_nav_goto_block
      (Sifthenelse
          (Ebinop One (Etempvar CommonFP._b tuchar)
            (Evar CommonFP._private_nav_block tuchar) tint)
          (Sassign (Evar CommonFP._private_last_block tuchar)
            (Evar CommonFP._private_nav_block tuchar) @+@
          Sassign (Evar CommonFP._last_block tuchar)
            (Evar CommonFP._private_last_block tuchar) @+@
          Sassign (Evar CommonFP._private_last_stage tuchar)
            (Evar CommonFP._private_nav_stage tuchar) @+@
          Sassign (Evar CommonFP._last_stage tuchar)
            (Evar CommonFP._private_last_stage tuchar)) Sskip @+@
        Scall None
          (Evar CommonFP._on_exit_block
            (Tfunction (Tcons tuchar Tnil) tvoid cc_default))
          [:: Evar CommonFP._private_nav_block tuchar] @+@
        Sassign (Evar CommonFP._private_nav_block tuchar)
          (Etempvar CommonFP._b tuchar) @+@
        Sassign (Evar CommonFP._nav_block tuchar)
          (Evar CommonFP._private_nav_block tuchar) @+@
        Scall None
          (Evar CommonFP._nav_init_block (Tfunction Tnil tvoid cc_default))
          [::]) k empty_env
      (PTree.set CommonFP._t'1 (create_bool_val false)
          (PTree.set CommonFP._b (Vint (int_of_nat id))
            (PTree.set CommonFP._t'1 Vundef (PTree.empty val)))) (e2.m)) 
      (fp_trace_to_trace o)
      (State CommonFP.f_nav_goto_block Sskip k empty_env le' (e2'.m)))
    /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 id He Hc Hid8;
      rewrite /change_block //= in Hc.

    have He8 := proj2_sig e1.

    (** Get private nav block *)
    have Hpnb := get_private_nav_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnb.
    destruct Hpnb as [b_pnb [Hfs_pnb Hfd_pnb]].

    destruct (get_nav_block (` e1) =? id) eqn:Heq.

    (* Do not enter in the if *)
    { have Hc':
        update_nav_stage 
          (update_nav_block (` e1)
            (FP_E_WF.Common.normalise_block_id fpe id)) 0 = (` e1').
      { rewrite -Hc -((proj1 (Nat.eqb_eq _ _)) Heq)
                 Nat.eqb_refl /= /FP_E.normalise_block_id.
        by destruct (_ <=? _) eqn:Hle. }

      destruct (step_change_block_end He Hc' Hid8)
        as [e2' [Hend He2']].

      do 2 eexists; split. move => k o.

      step_seq. star_step step_ifthenelse.

      econstructor.
      - econstructor. rewrite PTree.gso; try by apply neg_ident.
        apply PTree.gss.
      - econstructor. apply eval_Evar_global; try by [].
        apply Hfs_pnb. econstructor; try by [].
        rewrite /Mem.loadv.
        apply (proj2 ((MATCH_ENV.correct_private_nav_block (proj1 He)) _ Hfs_pnb)).
      - econstructor. apply Nat.eqb_eq in Heq; subst id.
        rewrite //= Int.eq_true //=.

      rewrite Int.eq_true //=. step_skip_seq.

      apply Hend. rewrite PTree.gso; try by apply neg_ident.
      apply PTree.gss.
      all: try by []. step_refl. 
      simpl_trace. rewrite //= ?trace_app //=. }

    (* Enter in the if *)
    remember
    (mk_fp_env
      (get_nav_block (` e1)) (get_nav_stage (` e1))
      (get_nav_block (` e1)) (get_nav_stage (` e1))
      (get_trace (` e1))) as e1''; symmetry in Heqe1''.
    have He1''8: fp_env_on_8 e1''.
    { rewrite -Heqe1'' /=; destruct e1 as [e1 []]. by split. }
    destruct (step_change_block_middle (proj1 He) He8 Heqe1'')
      as [m2' [Hm Hm2']].

    set e2' := create_fp_cenv m2' (fp_trace_to_trace (get_trace e1'')).
    have He2': (exist _ e1'' He1''8) ~cenv~ (ge, e2').
    { split; rewrite //=. apply trace_generated_match. }

    have Heqe1':
      update_nav_stage 
        (update_nav_block e1''
          (FP_E_WF.Common.normalise_block_id fpe id)) 0 = ` e1'
          by rewrite -Heqe1'' -Hc Heq //=.

    have He8':= modify_last_on_8 He8 Heqe1''.

    destruct (step_change_block_end He2' Heqe1' Hid8)
        as [e2'' [Hend He2'']].

    do 2 eexists; split. move => k o.

    step_seq. star_step step_ifthenelse.

    econstructor.
    - econstructor. rewrite PTree.gso; try by apply neg_ident.
      apply PTree.gss.
    - econstructor. apply eval_Evar_global; try by [].
      apply Hfs_pnb. econstructor; try by [].
      rewrite /Mem.loadv.
      apply (proj2 ((MATCH_ENV.correct_private_nav_block (proj1 He)) _ Hfs_pnb)).
    - econstructor. rewrite //= Int.eq_false //=.
      apply (int_of_nat_ne8 Hid8 (is_nav_block8 Hsize He8)).
      apply Nat.eqb_neq. by rewrite Nat.eqb_sym.

    rewrite Int.eq_false //=; try by []. by []. step_skip_seq.
    apply Hend. rewrite PTree.gso; try by apply neg_ident.
    apply PTree.gss.
    all: try by []. simpl_trace. by rewrite //= ?trace_app //= -Heqe1''.
  Qed.


  Lemma step_goto_block_gen:
    forall e1 e1' e2 e le id id_expr,
      e1 ~cenv~ (ge, e2)
      -> correct_env e
      -> is_nat8 id
      -> goto_block fpe (` e1) id = ` e1'
      -> typeof id_expr = tuchar \/ typeof id_expr = tint
      -> eval_expr ge e le (e2.m) id_expr (create_val id)
      -> exists e2' le', 
          let t := extract_trace (` e1) (` e1') in
        (forall k f,
        Smallstep.star step2 ge
          (State f
            (Scall None (gen_void_fun_param8 CommonFP._nav_goto_block)
              [:: id_expr]) k e le (e2.m)) 
            (fp_trace_to_trace  t)
            (State f Sskip k e le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 e le id id_expr He Hce Hid8
                Hgoto Htype Hexpr.
    rewrite /goto_block in Hgoto.

    have Hfun := goto_block_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hfun.
    destruct Hfun as [block [Hfs Hfd]].

    (* Execution of the forbidden deroute *)
    destruct (forbidden_deroute8 fps e1 id) as [b [e1' Hfb]];
      rewrite Hfb in Hgoto.

    remember
      (PTree.set CommonFP._b
        (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
        (create_undef_temps (fn_temps CommonFP.f_nav_goto_block)))
    as le'.
    have Hle' : le' ! CommonFP._b = Some (create_val id).
    { subst le'. by rewrite PTree.gss -(cast_int_to_int8 Hid8).  }
    destruct (step_forbidden_deroute He Hid8 Hfb Hle')
      as [e2' [Hfb' He2']]; subst le'.

    destruct b.

    { apply env8_proj in Hgoto; subst e1'_f; do 2 eexists; split.
       move => k f.

      (* Access to the body of the function *)
      econstructor. econstructor; try by []. econstructor.

      eapply eval_Evar_global; try by [].
      apply (none_goto_block Hce). apply Hfs. 
      by eapply deref_loc_reference.

      econstructor. apply Hexpr. destruct Htype as [H|H]; by rewrite H.
      apply eval_Enil.

      apply find_def_to_funct. apply Hfd. by [].

      econstructor. econstructor. econstructor; try by [].
      - apply list_norepet_nil.
      - constructor; try by []. apply list_norepet_nil.
      - apply list_disjoint_cons_r; try by [].
        rewrite //= => [[Heq | Hf]]; try by []. by apply neg_ident in Heq.
      econstructor. step_seq. 

      step_seq. step_trans. apply Hfb'. step_skip_seq.

      star_step step_ifthenelse.
      econstructor. apply PTree.gss. by [].

      rewrite true_ne_0 //=.
      econstructor. all: try by []. star_step step_return_0.
      by []. star_step step_returnstate. step_refl.

      all: try by []. step_refl. simpl_trace. by []. }

    (* Deroute not fobidden *)
    { (* Changement of block *)
      destruct (change_block8 fps e1' id) as [e1'' Heqe1''].
        rewrite Heqe1'' in Hgoto.
      have He8' := exec_forbidden_deroute8 fps _ (proj2_sig e1) Hfb.
      destruct (step_change_block He2' Heqe1'' Hid8)
        as [e2'' [le' [Hchange He2'']]].
      remember (c_change_block fpe _ _) as c_code.
      set e2''_f := app_ctrace e2'' (fp_trace_to_trace c_code).
      exists e2''_f, le; split. move => k.

      (* Access to the body of the function *)
      econstructor. econstructor; try by []. econstructor.

      eapply eval_Evar_global; try by [].
      apply (none_goto_block Hce). apply Hfs. 
      by eapply deref_loc_reference.

      econstructor. apply Hexpr. destruct Htype as [H|H]; by rewrite H.
      apply eval_Enil.

      apply find_def_to_funct. apply Hfd. by [].

      econstructor. econstructor. econstructor; try by [].
      - apply list_norepet_nil.
      - constructor; try by []. apply list_norepet_nil.
      - apply list_disjoint_cons_r; try by [].
        rewrite //= => [[Heq | Hf]]; try by []. by apply neg_ident in Heq.
      econstructor. step_seq. 

      step_seq. step_trans. apply Hfb'. step_skip_seq.

      star_step step_ifthenelse.
      econstructor. apply PTree.gss. by [].

      rewrite false_eq_0 //=. step_skip_seq.
      rewrite (id_zero_ext8 Hid8). apply Hchange. all: try by [].

      replace (e2''.m) with (e2''_f.m); try by [].
      star_step step_skip_call; try by [].
      star_step step_returnstate. step_refl. by [].
      step_refl. simpl_trace. subst c_code.
      rewrite -Hgoto (forbidden_deroute_unchanged _ _ _ Hfb)
                /extract_trace (extract_forbidden_deroute Hfb)
                get_app_trace -(change_block_eq_trace Heqe1'')
                (extract_forbidden_deroute Hfb) drop_cat_le;
                try by [].
      rewrite size_cat -size_eq_length. apply leq_addr.

      destruct He2'' as [He2'' Ht].
      split; rewrite -Hgoto.
        by apply app_trace_preserve_mmatch.
        rewrite /e2''_f. by apply app_ctrace_match. }
  Qed.

  Lemma step_goto_block:
    forall e1 e1' e2 le id,
      e1 ~cenv~ (ge, e2)
      -> is_nat8 id
      -> goto_block fpe (` e1) id = ` e1'
      -> exists e2' le',
          let t := extract_trace (` e1) (` e1') in  
      (forall k f,
        Smallstep.star step2 ge
          (State f (gen_call_iparams_fun CommonFP._nav_goto_block id)
                      k empty_env le (e2.m)) 
            (fp_trace_to_trace  t)
            (State f Sskip k empty_env le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le id He Hid8 Hgoto.

    apply (step_goto_block_gen He Hcempt Hid8 Hgoto); try by left.

    apply eval_expr_const_uint8.
  Qed.

  Definition expr_incr_nav_block:=
    Ebinop Oadd
      (Evar CommonFP._private_nav_block tuchar)
      (Econst_int (Int.repr 1) tint) tint.

  Lemma type_expr_incr_nav_block:
    typeof expr_incr_nav_block = tint.
  Proof. by []. Qed.

  Lemma eval_expr_incr_nav_block: 
    forall e le e1 e2,
    e1 ~menv~ (ge, e2)
    -> correct_env e
    -> fp_env_on_8 e1
    -> (get_nav_block e1 < FP_E.get_nb_blocks fpe - 1)%coq_nat
    -> eval_expr ge e le e2 expr_incr_nav_block
          (create_val (get_nav_block e1 + 1)).
  Proof.
    move => e le e1 e2 He Hce He8 Hlt.
    destruct (get_symb_private_nav_block fpe gvars) as [b Hfs].
    assert (Hadd8 : is_nat8 (get_nav_block e1 + 1)).
    {
        to_nat Hlt.
        have Hblock8 := (get_nb_blocks8 Hsize). 
        rewrite /nb_blocks_lt_256 //= in Hblock8.
        unfold is_nat8 in *.
        rewrite eq_fps_fp in Hblock8.
        ssrlia.
    }

    econstructor. econstructor. apply eval_Evar_global; try by [].
    apply (none_private_nav_block Hce). apply Hfs.
    econstructor; try by [].

    apply (proj2 ((MATCH_ENV.correct_private_nav_block He) _ Hfs)).
    econstructor.

    rewrite //= /sem_add /sem_binarith //=
            ?(sem_cast8 _ (is_nav_block8 Hsize He8)) //= sem_cast_one
            /create_val.
    repeat f_equal. by apply incr_int_val.
  Qed.

  Lemma step_nextblock:
    forall e1 e1' e2 e le,
      e1 ~cenv~ (ge, e2)
      -> correct_env e
      -> FPE_BS.next_block fpe_wf (` e1) = `e1'
      -> exists e2',
        let t := extract_trace (` e1) (` e1') in
        (forall k,
        Smallstep.star step2 ge (State f nextBlock k e le (e2.m)) 
                                      (fp_trace_to_trace  t)
                                      (State f Sskip k e le (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 e le He Hce Hnext.
    rewrite /FPE_BS.next_block in Hnext.
    have He8 := proj2_sig e1.
    destruct (get_symb_nb_blocks fpe gvars) as [b Sol].

    destruct (le_lt_dec (FP_E_WF.get_nb_blocks (FPE_BS.fp fpe_wf) - 1) (get_nav_block (` e1))) as [Hle | Hlt].

    (** Nav block equal to 255 *)
    { remember (goto_block fpe (` e1) (get_nav_block (` e1))) as e_block eqn:Heb;
      symmetry in Heb.
      set e1' := exist _ e_block (exec_goto_block8 fps _ He8 Heb).
      have Hg: goto_block fpe (` e1) (get_nav_block (` e1)) = ` e1' by rewrite /e1'.

      destruct (@step_goto_block_gen _ _ _ _ (PTree.empty val) _
              (Evar _private_nav_block tuchar)
               He Hcempt (nav_block8 He8) Hg)
        as [e2' [le' [Hgoto He2']]]; try by left. 
      apply (eval_expr_private_nav_block (proj1 He)).
      
    all: try by ssrlia.

    have Hfun := next_block_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hfun.
    destruct Hfun as [block [Hfs Hfd]].


    exists e2'; split; try by []. move => k.

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    apply (none_next_block Hce). apply Hfs. 
    by eapply deref_loc_reference.
    apply eval_Enil.

    apply find_def_to_funct. apply Hfd. by [].

    econstructor. econstructor. econstructor; try by [].
    - apply list_norepet_nil.
    - apply list_norepet_nil.
    - econstructor.

    step_trans. star_step step_ifthenelse.

    (** Evaluation of the condition *)
    econstructor. apply (eval_expr_private_nav_block (proj1 He)).
    econstructor.
    eapply (get_nb_blocks).
    eapply (MATCH_ENV.correct_nb_blocks (proj1 He)).
    econstructor.
    constructor.
    constructor.
    simpl.
    rewrite decr_int_val.
    rewrite /= (destruct_int_lt_ne _ _); ssrlia.
    have Hblock8 := get_nb_blocks8 Hsize.
    rewrite /nb_blocks_lt_256 in Hblock8.
    rewrite eq_fps_fp in Hblock8. 
    unfold is_nat8 in *. ssrlia.
    apply (nav_block8 (proj2_sig e1)).
    apply FP_E.nb_blocks_ne_0.
    apply (get_nb_blocks8 Hsize).

    rewrite Int.eq_true //=. by [].

    (* Return *)
    star_step step_skip_call; try by [].
    star_step step_returnstate; try by []. rewrite //=. step_refl.

    all: try by []. simpl_trace.
    all: apply Nat.ltb_ge in Hle.
    all: rewrite // ?Hg Hle in Hnext; apply env8_proj in Hnext;
          subst e1'_f; try by []. }

    (* Execution of the next_block *)
    assert (Hadd8 : is_nat8 (get_nav_block (` e1) + 1)).
    {
        to_nat Hlt.
        have Hblock8 := (get_nb_blocks8 Hsize). 
        rewrite /FPE_BS.fp /fpe_wf eq_fps_fp in Hlt.
        rewrite /nb_blocks_lt_256 //= in Hblock8.
        unfold is_nat8 in *.
        rewrite eq_fps_fp in Hblock8.
        to_nat Hblock8.
        ssrlia.
    }
    remember (goto_block fpe (` e1)
                    (get_nav_block (` e1) + 1)) as e_block eqn:Heb;
      symmetry in Heb.
    set e1' := exist _ e_block (exec_goto_block8 fps _ He8 Heb).
    have Hg: goto_block fpe (` e1) (get_nav_block (` e1) + 1)
                  = ` e1' by rewrite /e1'.

    destruct (@step_goto_block_gen _ _ _ _ (PTree.empty val) _
                  expr_incr_nav_block He Hcempt Hadd8 Hg)
      as [e2' [le' [Hgoto He2']]]; try by right.

    apply (eval_expr_incr_nav_block _ (proj1 He) Hcempt He8 Hlt).

    have Hfun := next_block_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hfun.
    destruct Hfun as [block [Hfs Hfd]].

    exists e2'; split; try by []. move => k.

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    apply (none_next_block Hce). apply Hfs. 
    by eapply deref_loc_reference.
    apply eval_Enil.

    apply find_def_to_funct. apply Hfd. by [].

    econstructor. econstructor. econstructor; try by [].
    - apply list_norepet_nil.
    - apply list_norepet_nil.
    - econstructor.

    step_trans. star_step step_ifthenelse.

    (** Evaluation of the condition *)
    econstructor. apply (eval_expr_private_nav_block (proj1 He)).
    econstructor.
    apply get_nb_blocks.
    apply (MATCH_ENV.correct_nb_blocks (proj1 He)).
    constructor.
    constructor.
    constructor. simpl.
    rewrite decr_int_val.
    rewrite /= (destruct_int_lt _ _); ssrlia.
    apply (nav_block8 (proj2_sig e1)).
    have Hblock8 := get_nb_blocks8 Hsize.
    rewrite /nb_blocks_lt_256 in Hblock8.
    rewrite eq_fps_fp in Hblock8. 
    unfold is_nat8 in *. ssrlia.
    apply FP_E.nb_blocks_ne_0.
    apply (get_nb_blocks8 Hsize).

    rewrite Int.eq_false //=. by [].

    (* Return *)
    star_step step_skip_call; try by [].
    star_step step_returnstate; try by []. rewrite //=. step_refl.

    all: try by []. simpl_trace.
    all: apply Nat.ltb_lt in Hlt.
    all: rewrite Hg Hlt in Hnext; apply env8_proj in Hnext;
          subst e1'_f; try by [].
  Qed.

  (** Evaluation of last_block*)
  Definition expr_last_block := (Evar CommonFP._private_last_block tuchar).

  Lemma eval_expr_last_block:
    forall e e1 e2,
      e1 ~menv~ (ge, e2)
      -> correct_env e
      -> fp_env_on_8 e1
      -> forall le,
          eval_expr ge e le e2 expr_last_block
          (create_val (get_last_block e1)).
  Proof.
    move => e e1 e2 He Hce He8 le.
    destruct (get_symb_private_last_block fpe gvars) as [b Hfs].

    econstructor. apply eval_Evar_global; try by [].
    apply (none_private_last_block Hce). apply Hfs.
    econstructor; try by [].

    apply (proj2 ((MATCH_ENV.correct_private_last_block He) _ Hfs)).
  Qed.

  (** Evaluation of last_stage *)
  Definition expr_last_stage := (Evar CommonFP._private_last_stage tuchar).

  Lemma eval_expr_last_stage:
    forall e e1 e2,
      e1 ~menv~ (ge, e2)
      -> correct_env e
      -> fp_env_on_8 e1
      -> forall le,
          eval_expr ge e le e2 expr_last_stage
          (create_val (get_last_stage e1)).
  Proof.
    move => e e1 e2 He Hce He8 le.
    destruct (get_symb_private_last_stage fpe gvars) as [b Hfs].

    econstructor. apply eval_Evar_global; try by [].
    apply (none_private_last_stage Hce). apply Hfs.
    econstructor; try by [].

    apply (proj2 ((MATCH_ENV.correct_private_last_stage He) _ Hfs)).
  Qed.

  Definition return_reset_value (e:fp_env) (p: bool) :=
    if p then 0
    else get_last_stage e.

  Lemma return_reset_value8:
    forall e p,
      fp_env_on_8 e
      -> is_nat8 (return_reset_value e p).
  Proof.
    move => e [] He8; try by [].
    - rewrite //= /is_nat8. lia.
    - by destruct He8.
  Qed.

  Definition expr_return_reset_value (p: bool): expr :=
    if p then Econst_int (Int.repr 0) tint
    else expr_last_stage.

  Lemma step_return:
    forall e1 e1' e2 e le params,
      let idb := get_nav_block (` e1) in
      e1 ~cenv~ (ge, e2)
      -> correct_env e
      -> return_block fpe (` e1) params = (` e1')
      -> exists e2' le', (forall k,
        Smallstep.star step2 ge
          (State f 
             (gen_call_iparams_fun CommonFP._Return params)
                      k e le (e2.m)) 
            (fp_trace_to_trace ((FP_E_WF.on_exit (` (` fps)) (idb)) ++ [:: reset_time, init_stage & (FP_E_WF.on_enter (` (` fps)) (get_last_block (` e1)))]))
            (State f Sskip k e le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 e le params idb He Hce Hr.
    rewrite /return_block in Hr.
    have He8 := proj2_sig e1.

    (* Store new block *)
    remember (update_nav_block (` e1)
              (get_last_block (` e1))) as  e1'; symmetry in Heqe1'.
    destruct (@step_update_nav_block _ _ _ _ _ expr_last_block (proj1 He)
      Hcempt (is_last_block8 Hsize He8) Heqe1')
        as [e2'1 [e2' [Hpv [Hv He2']]]]; try by left.
    apply (eval_expr_last_block (proj1 He) Hcempt He8).

    have He8' := update_nav_block_with_last8 He8 Heqe1'.

    (* Store new block *)
    remember (update_nav_stage e1'
              (return_reset_value e1' params)) as  e1'';
              symmetry in Heqe1''.
    destruct (@step_update_nav_stage _ _ _ _ _
                      (expr_return_reset_value params) He2' Hcempt
                   (return_reset_value8 params He8') Heqe1'')
      as [e2''1 [e2'' [Hpv' [Hv' He2'']]]].
    destruct params; [ by right | by left ].

    destruct params.
    - intro; econstructor.
    - apply (eval_expr_last_stage He2' Hcempt He8').
   
    have Hfun := return_fun fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hfun.
    destruct Hfun as [block [Hfs Hfd]].

    set e2'_f := create_fp_cenv e2''
        (fp_trace_to_trace (get_trace (` e1'_f))).

    exists e2'_f; eexists; split;try by []. move => k.

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].
    - apply (none_return Hce). apply Hfs. 
      by eapply deref_loc_reference.
    - econstructor. apply eval_expr_const_uint8. by [].
      apply eval_Enil.

    apply find_def_to_funct. apply Hfd. by [].

    econstructor. econstructor. econstructor; try by [].
    - apply list_norepet_nil.
    - constructor; try by []. apply list_norepet_nil.
    - econstructor.

    (* call on_exit *)
    step_seq. apply step_on_exit.
    - apply He.
    - apply He8.
    step_skip_seq.

    (* Store nav_block *)
    step_seq. apply Hpv. step_skip_seq.
    step_seq. apply Hv. step_skip_seq.

    step_trans.

    (* Prove that the two branch provides the same result *)
    { destruct params eqn:Hb.

      (* Condition is true *)
      step_seq. star_step step_ifthenelse.

      (* Evaluation of the condition *)
      econstructor. econstructor. apply PTree.gss.
      econstructor. econstructor. rewrite //=.

      rewrite Int.eq_false; try by []. by [].
      rewrite Hb. step_refl. all: try by [].

      (* Condition is false *)
      step_seq. star_step step_ifthenelse.

      (* Evaluation of the condition *)
      econstructor. econstructor. apply PTree.gss.
      econstructor. econstructor. rewrite //=.

      rewrite Int.eq_true //=; try by []. by [].
      step_refl. all: try by []. }

    step_skip_seq. step_seq. apply Hv'. step_skip_seq.

    step_seq.

    step_trans. apply eq_exec_assign_time.
    step_skip_seq.
    step_seq. apply exec_arbitrary_C_code. step_skip_seq.

    apply step_on_enter.
    - apply He2''.
    - eapply update_nav_stage8.
      + apply He8'.
      + apply (return_reset_value8 params He8').
      + apply Heqe1''.
    all: try reflexivity.

    (* Return of the function *)
    star_step step_skip_call; try by [].
    star_step step_returnstate; try by []. rewrite //=. step_refl.

    all: try by [].

    rewrite eq_fps_fp /idb.
    assert (Hid : get_last_block (` e1) = get_nav_block e1'').
    {
      rewrite <- Heqe1''.
      by rewrite <- Heqe1'.
    }
    rewrite Hid.
    simpl.
    rewrite <- cat1s.
    rewrite trace_app.
    rewrite (trace_app [:: reset_time]).
    f_equal.
    simpl. f_equal.
    rewrite E0_right.
    f_equal.

    have Heq: ` e1'_f = e1''.
    { rewrite -Hr -Heqe1'' -Heqe1'. by destruct params. }

    split. by rewrite Heq. by apply trace_generated_match.
  Qed.

Section No_External_Call_FP.
  
  Lemma find_funct_in_add_global : 
    forall g v a res,
    Globalenvs.Genv.find_funct (@Globalenvs.Genv.add_global fundef type g a) v = res ->
      (exists v1 func, res = Some func /\ a = (v1, Gfun func))
      \/ Globalenvs.Genv.find_funct g v = res \/ res = None.
  Proof.
    intros. unfold Globalenvs.Genv.find_funct in H. destruct v; simpl;
    try (right; left; assumption).
    destruct (Ptrofs.eq_dec i Ptrofs.zero); try (right; assumption).
    unfold Globalenvs.Genv.find_funct_ptr in H.
    unfold Globalenvs.Genv.find_def in H. simpl in H.
    remember (Globalenvs.Genv.genv_next g) as b1.
    destruct (b =? b1)%positive eqn:Eqb. 
    - apply Pos.eqb_eq in Eqb; subst b.
      rewrite PTree.gss in H. destruct a. destruct g0.
      * symmetry in H. simpl in H. left. exists i0, f0. split.
        apply H. reflexivity.
      * simpl in H. symmetry in H. right. right. apply H. 
    - apply Pos.eqb_neq in Eqb.
      apply (PTree.gso) with (x := (snd a)) (m := (Globalenvs.Genv.genv_defs g)) in Eqb. 
      rewrite Eqb in H. right. left. apply H.
    - right. right. symmetry. assumption.
  Qed.

  Lemma find_funct_in_add_globals: 
    forall g seqdef v res,
    Globalenvs.Genv.find_funct (@Globalenvs.Genv.add_globals fundef type g seqdef) v = res ->
      (exists ident funct, res = Some funct /\ In (ident, Gfun funct) seqdef)
      \/ Globalenvs.Genv.find_funct g v = res \/ res = None.
  Proof.
    intros g seqdef. generalize dependent g. induction seqdef.
    - simpl. intros. right. left. apply H.
    - simpl. intros g v res H.
      destruct (IHseqdef (Globalenvs.Genv.add_global g a) _ _ H).
      + destruct H0 as [ident [funct [H1 H2]]].
        left. exists ident, funct. split.
        * apply H1.
        * right. apply H2.
      + destruct H0.
        * apply find_funct_in_add_global in H0.
          --destruct H0 as [[v1 [funct [H1 H2]]] | [H0 | H0]].
            ++left. exists v1, funct. split. apply H1. left. apply H2.
            ++right. left. apply H0.
            ++right. right. apply H0.
            ++right. right. apply H0.
  Qed.
  

  
  (** common lemma about NEC_statements *)

  Lemma parse_c_code_NEC: 
    forall a,
    No_external_call_statement (parse_c_code_option a).
  Proof.
    intros a. unfold parse_c_code_option. destruct a; constructor.
  Qed.

  Lemma gen_fun_call_stmt_NEC : 
    forall f_name f_type f_sig_type params pc,
    No_external_call_statement (gen_fun_call_stmt f_name f_type f_sig_type params pc).
  Proof.
    intros. unfold gen_fun_call_stmt. destruct (get_nav_nav_params pc);
    repeat (try constructor; try apply parse_c_code_NEC).
  Qed.

  Lemma  gen_fp_pitch_NEC : 
    forall nav pitch,
    No_external_call_statement (gen_fp_pitch nav pitch).
  Proof.
    intros.
    unfold gen_fp_pitch. destruct pitch;
    repeat (try constructor; try apply parse_c_code_NEC).
  Qed.
  
  Lemma gen_fp_vmode_NEC : 
    forall nav vmode,
    No_external_call_statement (gen_fp_vmode nav vmode).
  Proof.
    intros. unfold gen_fp_vmode. destruct vmode;
    repeat (try constructor; try apply parse_c_code_NEC).
    destruct val;
    repeat (try constructor; try apply parse_c_code_NEC).
  Qed.
  
  Lemma gen_fp_hmode_NEC : 
    forall h pc,
    No_external_call_statement (gen_fp_hmode h pc).
  Proof.
    destruct h; intros pc; apply gen_fun_call_stmt_NEC.
  Qed.
  
  Ltac autoProov_NEC_f := repeat (try constructor;
    try apply parse_c_code_NEC;
    try apply gen_fun_call_stmt_NEC;
    try apply gen_fp_pitch_NEC;
    try apply gen_fp_vmode_NEC;
    try apply gen_fp_hmode_NEC ).

  (** Not trivial stage statement with no external call *)
  
  Lemma gen_fp_stage_call_NEC : 
    forall id params,
    No_external_call_statement (gen_fp_stage (FP_E.CALL id params)).
  Proof.
    intros. repeat (constructor).
    unfold gen_fp_call. destruct (get_call_loop params).
    - constructor. (** if then else *)
      + (** then *)
        destruct (get_call_break params); 
        autoProov_NEC_f.
      + (** else *)
        destruct (get_call_until params);
        autoProov_NEC_f.
    - (** simpl call *)
      destruct (get_call_break params); 
      autoProov_NEC_f.
  Qed.
  
  Lemma gen_fp_stage_nav_init_NEC : 
    forall id nav_mode,
    No_external_call_statement (gen_fp_stage (FP_E.NAV_INIT id nav_mode)).
  Proof.
    intros. destruct (nav_mode); 
    autoProov_NEC_f.
  Qed.

  Lemma gen_fp_nav_post_call_NEC : 
    forall mode,
    No_external_call_statement (gen_fp_nav_post_call mode).
  Proof.
    unfold gen_fp_nav_post_call. intros mode.
    destruct (get_fp_pnav_call mode);
    autoProov_NEC_f.
  Qed.

  Lemma gen_fp_nav_until_NEC : forall nav_mode until,
    No_external_call_statement (gen_fp_nav_until nav_mode until).
  Proof.
    destruct until; autoProov_NEC_f.
    apply gen_fp_nav_post_call_NEC.
  Qed.
  
  Lemma gen_fp_stage_nav_NEC : 
    forall id nav_mode until,
    No_external_call_statement (gen_fp_stage (FP_E.NAV id nav_mode until)).
  Proof.
    autoProov_NEC_f.
    - (** nav_pre_call *)
      unfold gen_fp_nav_pre_call.
      destruct (get_fp_pnav_call nav_mode);
      autoProov_NEC_f.
    - (** nav_cond *)
      unfold gen_fp_nav_cond. unfold nav_cond_stmt.
      destruct nav_mode; autoProov_NEC_f.
      destruct (get_go_from params); autoProov_NEC_f.
    - (** nav_until *)
      apply gen_fp_nav_until_NEC.
    - (** nav_post_call *)
      apply gen_fp_nav_post_call_NEC.
  Qed.

  Lemma auto_nav_NEC: 
    forall fp, 
    No_external_call_statement (fn_body (gen_fp_auto_nav fp)).
  Proof.
    intros fp.
    simpl. constructor.
    { (** Exceptions *)
      induction (FP_E.get_fp_exceptions fp).
      constructor.
      simpl. constructor.
      + autoProov_NEC_f.
      + (** induction *)
        apply IHf0. }

    { (** gen_block_switch *)
      unfold gen_block_switch. repeat constructor.
      induction (FP_E.get_fp_blocks fp).
      - (** default block *)
        autoProov_NEC_f.
      - (** block a *)
        autoProov_NEC_f.
        + (** exeptions *)
          induction (FP_E.get_block_exceptions a); autoProov_NEC_f.
          apply IHf1.
        + (** stage *)
          induction (FP_E.get_block_stages a).
          * constructor.
          * simpl.
            destruct a0; constructor; 
            try apply IHl; try by autoProov_NEC_f.
            -- (** call *) apply gen_fp_stage_call_NEC.
            -- (** Nav init *) apply gen_fp_stage_nav_init_NEC.
            -- (** Nav *) apply gen_fp_stage_nav_NEC.
        + (** induction *) apply IHf0. }
  Qed.
  
  Lemma aux_forbidden_deroute_NEC : 
    forall n fpe,
    length fpe <= n ->
    No_external_call_statement_labeled (gen_forbidden_deroute_case fpe (LScons None Sbreak LSnil)).
  Proof.
    intros n. 
    induction n.
    - intros fpe H. destruct fpe; try discriminate. 
      rewrite gen_forbidden_deroute_case_equation.
      autoProov_NEC_f.
    - intros fpe H. destruct fpe; simpl in *.
      + apply IHn. simpl. apply leq0n.
      + rewrite gen_forbidden_deroute_case_equation.
        remember (partition (test_from (get_fbd_from f0)) (f0 :: fpe0)) as partition.
        destruct partition.
        autoProov_NEC_f. destruct Heqpartition.
        induction l. 
        * autoProov_NEC_f.
        * simpl. autoProov_NEC_f.
          -- destruct (get_fbd_only_when a); autoProov_NEC_f.
          -- apply IHl.
        apply IHn. symmetry in Heqpartition.
        simpl in Heqpartition. rewrite test_from_refl in Heqpartition.
        remember (partition (test_from (get_fbd_from f0)) fpe0) as temp.
        destruct temp; subst. inversion Heqpartition. subst.
        symmetry in Heqtemp. 
        apply partition_length in Heqtemp. rewrite Heqtemp in H.
        to_nat H. ssrlia.
  Qed.

  Lemma forbidden_deroute_NEC : 
    forall fpe,
    No_external_call_statement (fn_body (gen_fp_forbidden_deroute fpe)).
  Proof.
    simpl. autoProov_NEC_f. 
    apply aux_forbidden_deroute_NEC with ((Datatypes.length (FP_E.get_fp_forbidden_deroutes fpe0))).
    auto.
  Qed.
  
  Lemma No_function_in_gvars: forall ident0 f0,
    ~ (In (ident0, Gfun f0) (` gvars)%list).
  Proof.
    intros ident0 f0 H. induction gvars.
    simpl in H. induction x.
    - simpl in H. apply H.
    - inversion p. inversion all_correct_gvar; subst.
      inversion H2. destruct is_gvar as [id [v Eqa]]. subst. 
      clear H2. clear def_var. simpl in H. destruct H as [contra | H].
      + inversion contra.
      + generalize H. apply IHx. constructor.
        * inversion no_repet. auto.
        * apply H3.
  Qed. 

  (** Main lemma about the generator having no external function *)

  Lemma NECge : No_external_call_environement ge.
  Proof.
    intros id. remember (Globalenvs.Genv.find_funct ge id) as funct.
    unfold ge in Heqfunct. unfold globalenv in Heqfunct.
    simpl in Heqfunct. unfold prog in Heqfunct.
    unfold Globalenvs.Genv.globalenv in Heqfunct.
    symmetry in Heqfunct. apply find_funct_in_add_globals in Heqfunct.
    destruct Heqfunct as [[ident0 [funct0 [Heq HIn]]] | [Hfunct | HNone]].
    - subst funct. simpl in HIn.
      repeat (destruct HIn as [HIn | HIn]; (** go throught the list of defintitions *)
        try by inversion HIn); (**if it is a Gvar, contradiction in HIn *)
        (** if it is a Gfun, then it is an internal function*)
        try ( inversion HIn; destruct funct0; try(right; exists f0; 
        inversion H1; subst; split; try reflexivity; autoProov_NEC_f); inversion H1).

      unfold Generator.global_definitions in HIn. 
      apply in_app_or in HIn as [contra | HIn]. destruct (No_function_in_gvars contra).
      simpl in HIn.
      repeat (destruct HIn as [HIn | HIn]; try by inversion HIn);
      try by ( inversion HIn; destruct funct0; try(right; exists f0; 
      inversion H1; subst; split; try reflexivity; autoProov_NEC_f); inversion H1).
      -- (** on enter *)
        inversion HIn; destruct funct0; try discriminate; inversion H1; subst.
        right. exists (gen_fp_on_enter_block fpe). split. reflexivity. 
        autoProov_NEC_f.
        induction (FP_E.get_fp_blocks fpe).
        + simpl. autoProov_NEC_f.
        + simpl. unfold gen_case_on. destruct (FP_E.get_block_on_enter a).
          * constructor. autoProov_NEC_f. apply IHf0. apply IHf0.  
      -- (** on exit *)
        inversion HIn; destruct funct0; try discriminate; inversion H1; subst.
        right. exists (gen_fp_on_exit_block fpe). split. reflexivity. 
        autoProov_NEC_f.
        induction (FP_E.get_fp_blocks fpe). (** on exit *)
        + simpl. autoProov_NEC_f.
        + simpl. unfold gen_case_on. destruct (FP_E.get_block_on_exit a).
          * constructor. autoProov_NEC_f. apply IHf0. apply IHf0.
      -- (** fp_forbidden *)
        inversion HIn; destruct funct0; try discriminate; inversion H1; subst.
        right. exists (gen_fp_forbidden_deroute fpe). split. reflexivity.
        apply forbidden_deroute_NEC.
      -- (** execption *) 
        inversion HIn; destruct funct0; try discriminate; inversion H1; subst.
        right. exists (gen_fp_auto_nav fpe). split. reflexivity.
        apply auto_nav_NEC.
    - simpl in Hfunct. unfold Globalenvs.Genv.empty_genv in Hfunct.
      unfold Globalenvs.Genv.find_funct in Hfunct.
      destruct id; try by (left; auto).
      destruct (Ptrofs.eq_dec i Ptrofs.zero).
      unfold Globalenvs.Genv.find_funct_ptr in Hfunct. simpl in Hfunct.
      left; auto.
      left; auto.
      left; auto.
  Qed.

End No_External_Call_FP.

End FLIGHT_PLAN.



End COMMON_FP_VERIF.
