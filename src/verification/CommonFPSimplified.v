From GEN Require Import CommonFP.

From compcert Require Import Coqlib Integers Floats AST Ctypes Cop Clight Clightdefs.
Import Clightdefs.ClightNotations.
Local Open Scope clight_scope.

From VFP Require Import ClightGeneration.

(** Create a simplified version of the global definitions lists *)
(** The new list only contains the functions declared in
  common_flight_plan.c. The built-in functions added by clightgen are
  removed and also the functions that will be defined by this coq generator
  *)

(** A global definition the couple of a idents a its value *)
Definition gdef := (ident * globdef fundef type) % type.

Definition composites : list composite_definition :=
nil.

(** List of global_definition needed *)
Definition global_definitions : list (ident * globdef fundef type) :=
((_stage_time, Gvar v_stage_time) ::
 (_block_time, Gvar v_block_time) :: (_nav_stage, Gvar v_nav_stage) ::
 (_nav_block, Gvar v_nav_block) :: (_last_block, Gvar v_last_block) ::
 (_last_stage, Gvar v_last_stage) ::
 (_private_nav_stage, Gvar v_private_nav_stage) ::
 (_private_nav_block, Gvar v_private_nav_block) ::
 (_private_last_block, Gvar v_private_last_block) ::
 (_private_last_stage, Gvar v_private_last_stage) ::
 (_init_function, Gfun(Internal f_init_function)) ::
 (_get_nav_block, Gfun(Internal f_get_nav_block)) ::
 (_get_nav_stage, Gfun(Internal f_get_nav_stage)) ::
 (_get_last_block, Gfun(Internal f_get_last_block)) ::
 (_get_last_stage, Gfun(Internal f_get_last_stage)) ::
 (_set_nav_block, Gfun(Internal f_set_nav_block)) ::
 (_set_nav_stage, Gfun(Internal f_set_nav_stage)) ::
 (_NextBlock, Gfun(Internal f_NextBlock)) ::
 (_NextStage, Gfun(Internal f_NextStage)) ::
 (_Return, Gfun(Internal f_Return)) ::
 (_nav_init_block, Gfun(Internal f_nav_init_block)) ::
 (_nav_goto_block, Gfun(Internal f_nav_goto_block)) :: nil).

(** List of public idents *)
Definition public_idents : list ident :=
(_nav_goto_block :: _nav_init_block :: _Return :: _NextStage
  :: _NextBlock :: _set_nav_stage :: _set_nav_block :: _get_last_stage
  :: _get_last_block :: _get_nav_stage :: _get_nav_block :: _last_stage
  :: _last_block :: _nav_block :: _nav_stage :: _block_time
  :: _stage_time :: _init_function :: nil).

(** Definition of the idents for the the auto_nav function *)

Definition _auto_nav : ident := #"auto_nav".

(** List of reserved identifiant 
      (public idents ++ variables ++ generated functions) *)
Definition reserved_idents: list ident :=
 (_stage_time :: _block_time :: _nav_stage :: _nav_block :: _last_block
      :: _last_stage :: _private_nav_stage ::  _private_nav_block
      :: _private_last_block :: _private_last_stage :: _init_function
      :: _get_nav_block :: _get_nav_stage :: _get_last_block
      :: _get_last_stage :: _set_nav_block :: _set_nav_stage 
      :: _NextBlock :: _NextStage :: _Return :: _nav_init_block 
      :: _nav_goto_block 
      (** Generated functions *)
      :: _nb_blocks :: _on_enter_block :: _on_exit_block
      :: _forbidden_deroute :: _auto_nav :: nil).

(** No repetition in reserved_idents *)
Lemma norepet_reserved_idents:
    list_norepet reserved_idents.
Proof.
  repeat
    (* For all the elements of the list *)
    (apply list_norepet_cons; 

    (* Intro the list of comparaison of all elements *)
    simpl; try intro H;

    repeat (* For all disjonctions *)
      (destruct H as [H | H];
      (* Remove the create_ident function*)
      try apply create_ident_injective in H;
      try discriminate H);
      try apply H).
  apply list_norepet_nil.
Qed.
