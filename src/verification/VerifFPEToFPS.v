From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString Lia.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import BasicTypes 
                        FlightPlanExtended FlightPlanSized
                        FPSizeVerification 
                        FPEnvironmentGeneric FPEnvironmentDef
                        FPBigStepGeneric FPBigStepDef.

Import FP_E_WF.

Import Clightdefs.ClightNotations.
Local Open Scope clight_scope.
Local Open Scope nat_scope.

Set Implicit Arguments.


(** * Verification of the semantics preservation from FPE to FPS *)

Module FPE_TO_FPS_VERIF (EVAL_Def: EVAL_ENV)
                                    (ENVS_Def: ENVS_DEF EVAL_Def)
                                    (BS_Def: FP_BS_DEF EVAL_Def ENVS_Def).

  Import EVAL_Def ENVS_Def FP_E_WF BS_Def FPS_BS FPE_BS.

Section FLIGHT_PLAN.

  Variable fpe: flight_plan_wf.
  Variable fps: flight_plan_sized.

  Hypothesis VEFIF_SIZE: size_verification fpe = OK fps.

  Remark fpe_eq_fps:
    fpe = ` fps.
  Proof. by apply size_verification_unchange_fp. Qed.

  (** ** Proof of the semantics preservation for FPE to FPS *)
  Theorem semantics_preservation:
    forall e1 e1',
      FPE_BS.step fpe e1 = e1'
      -> forall e2, e1 ~env8~ e2
      -> exists e2', FPS_BS.step fps e2 = e2'
                      /\ e1' ~env8~ e2'.
  Proof.
    move => e1 e1' Hs [e2 He2] He.

    (* Remove fpe *)
    have Hfpe : fpe = FPS_BS.fpe_wf fps by rewrite fpe_eq_fps.
      subst fpe.

    inversion He; subst e1. rewrite /= in Hs.
    have He2' := FPS_BS.exec_step8 _ He2 Hs;
      subst e1'.

    eexists; split; try by [].
  Qed.

  (** ** Proof of the semantics preservation for FPS to FPE *)
  Theorem semantics_preservation_inv:
    forall e2 e2',
    FPS_BS.step fps e2 = e2'
    -> forall e1, e1 ~env8~ e2
    -> exists e1', FPE_BS.step fpe e1 = e1'
                /\ e1' ~env8~ e2'.
  Proof.
    move =>  e2 e2' Hs2 e1 He.

    remember (FPE_BS.step fpe e1) as e1' eqn:Hs1;
        symmetry in Hs1.

    have Hs2' := semantics_preservation Hs1 He.
    destruct Hs2' as [e2'' [Hs2' He']].
    rewrite Hs2 in Hs2'.

    exists e1'. by rewrite Hs2'.
  Qed.

  (** ** Proof of the semantics preservation for the inital case *)
  Theorem semantics_preservation_init:
      (FPE_ENV.Def.init_env (` fpe)) 
          ~env8~ (FPS_ENV.init_env fpe).
  Proof. by []. Qed.

  (** ** Bisimulation *)

  Theorem bisim_transf_extended_correct:
    bisimulation (FPE_BS.semantics_fpe fpe)
                  (FPS_BS.semantics_fps fps).
  Proof.
    apply Bisimulation with FPS_ENV.match_env; do 2 split.
    - intros; eexists; split; try by [].
    - by apply semantics_preservation.
    - intros; eexists; split => //. inversion H; by subst.
    - by apply semantics_preservation_inv.
  Qed.

End FLIGHT_PLAN.

End FPE_TO_FPS_VERIF.
