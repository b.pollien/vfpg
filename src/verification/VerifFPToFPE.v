From Coq Require Import Arith ZArith Psatz Bool
                        String List Program.Equality Program.Wf
                        Program.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas CommonLemmas BasicTypes
                        FPNavigationMode FPNavigationModeSem FlightPlan
                        FlightPlanGeneric FlightPlanExtended
                        FPEnvironment CommonFPDefinition
                        FPBigStep FPExtended FPBigStepExtended
                        FPBigStepGeneric FPEnvironmentGeneric
                        FPEnvironmentExtended
                        FPEnvironmentDef
                        FPBigStepDef.

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.

Set Implicit Arguments.

(** Verification of semantics preservation between FP and FP_E *)

(** * Common Lemmas for FP to FPE *)

Module FP_TO_FPE_COMMON.

  Lemma eq_get_fp_exceptions:
    forall fp fpe,
      fpe = extend_flight_plan fp
      -> FP.Common.get_fp_exceptions fp 
        = FP_E_WF.Common.get_fp_exceptions (` fpe).
  Proof.
    move => fp [[fb e bs] Hwf] Hfp //=;
    by injection Hfp as Hfb He Hb.
  Qed.

  Lemma eq_get_fp_forbidden_deroutes:
    forall fp fpe,
    fpe = extend_flight_plan fp
    -> FP_E_WF.Common.get_fp_forbidden_deroutes (` fpe)
       = FP.Common.get_fp_forbidden_deroutes fp.
  Proof.
    rewrite /extend_flight_plan => fp fpe_wf He.
    destruct fp as [fb1 e1 b1], fpe_wf as [[fb2 e2 b2] Hfp].
    by injection He.
  Qed.

End FP_TO_FPE_COMMON.

(** * Verification of the semantics preservation *)

Module FP_TO_FPE_VERIF (EVAL_Def: EVAL_ENV)
                                  (ENVS_Def: ENVS_DEF EVAL_Def)
                                  (BS_Def: FP_BS_DEF EVAL_Def ENVS_Def).
  Import ENVS_Def FP_TO_FPE_COMMON BS_Def MATCH_FP_FPE
            FP_BS FPE_BS.

  Section FLIGHT_PLAN.

  (** Flight plan *)
  Variable fp: FP.flight_plan.
  Definition fpe := extend_flight_plan fp.

  Remark Hfp: fpe = extend_flight_plan fp.
  Proof. by rewrite /fpe. Qed.

  Remark extend_fp_nwf_proj:
    extend_flight_plan_not_wf fp = ` fpe.
  Proof. by []. Qed.

  (** ** Generics lemmas *)

  (** *** Evalc Lemmas *)

  (** Lemma about the matching env to evaluate a condition *)
  Lemma match_evalc:
    forall e1 e2 e1' c b,
      e1 ~( fpe )~ e2
      -> FP_ENV.Def.evalc e1 c = (b, e1')
      -> exists e2', 
        FPE_ENV.Def.evalc e2 c = (b, e2')
        /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FP_ENV.Def.evalc
            /FPE_ENV.Def.evalc => e1 e2 e1' c b He Heval.
    exists (FPE_ENV.Def.app_trace e2
            [:: COND c (EVAL_Def.eval (FPE_ENV.Def.get_trace e2) c)]).
    injection Heval as Hb He'; subst e1' b; rewrite -(get_match_trace He).
    split; try by []. by apply match_app_trace.
  Qed.

  (** The evalutaion of a condition do not modify next_stage*)
  Lemma evalc_change_trace_next_stage:
    forall e c b e',
      FPE_ENV.Def.evalc e c = (b, e')
      -> FPE_ENV.Def.change_trace (FPE_ENV.next_stage e)
                                      (FPE_ENV.Def.get_trace e')
        = FPE_ENV.next_stage e'.
  Proof.
    rewrite /FPE_ENV.Def.evalc => e c b e' Hevalc.
    destruct e as [[idb ids lidb lids] t];
    destruct e' as [[idb' ids' lidb' lids'] t'].
    rewrite //= in Hevalc.
    injection Hevalc as Heval Hidb Hids Hlidb Hlids Ht.
    rewrite Hidb Hids Hlidb Hlids -Ht //=.
  Qed.

  (** ** Forbidden deroutes lemmas *)

  (** Lemma about the equivalence to evaluate forbidden deroute *)
  Lemma match_test_forbidden_deroute:
    forall e1 e2 from to fb e1' b,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.test_forbidden_deroute e1 from to fb = (b, e1')
      -> exists e2',
          FPE_BS.Common_Sem.test_forbidden_deroute e2 from to fb
            = (b, e2')
          /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FP_BS.Common_Sem.test_forbidden_deroute
            /FPE_BS.Common_Sem.test_forbidden_deroute
            //=
    => e1 e2 from to fb e1' b He.
    destruct ((from =? get_fbd_from fb) && (to =? get_fbd_to fb)) eqn:Hd.

    (* * From and To are forbidden by fb *)
    destruct (get_fbd_only_when fb) as [c| ] eqn:Hc.

    (*  + There is a condition to test *)
    destruct (FP_ENV.Def.evalc e1 c) as [b' e1''] eqn:Hevalc.
    have Heval := match_evalc He Hevalc;
      destruct Heval as [e2' [Heval He2']].
    move => He1''; injection He1'' as Hb' He1''.
    exists e2'; by rewrite Heval Hb' -He1''.

    (*  + There is a no condition to test *)
    move => He1'; injection He1' as Hb' He1'.
    exists e2; by rewrite Hb' -He1'.

    (* * From and To are not forbidden by fb *)
    move => He1'; injection He1' as Hb' He1'.
    exists e2; by rewrite Hb' -He1'.
  Qed.

  (** Matching state for the test of forbidden deroutes *)
  Lemma match_test_forbidden_deroutes:
    forall fbs from to e1 e2  e1' b,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.test_forbidden_deroutes e1 from to fbs
            = (b, e1')
      -> exists e2',
          FPE_BS.Common_Sem.test_forbidden_deroutes e2 from to fbs
            = (b, e2')
          /\ e1' ~( fpe )~ e2'.
  Proof.
    induction fbs as [| fb fbs IHfb];
    rewrite //= => from to e1 e2 e1' b He Hfb.
    - exists e2; injection Hfb as Hb He1.
      by rewrite Hb -He1.
    - destruct (FP_BS.Common_Sem.test_forbidden_deroute e1 from to fb)
        as [[] e1''] eqn:Hres;
        have Hfb':= match_test_forbidden_deroute He Hres;
        destruct Hfb' as [e2' [Hfb' He2']].

      (* Forbidden deroute raised *)
      * exists e2'; injection Hfb as Hb He1'.
        by rewrite Hfb' Hb -He1'.

      (* Forbidden deroute not raised *)
      * have IH := IHfb from to e1'' e2' e1' b He2' Hfb.
        destruct IH as [e2'' [Htfb He']]; exists e2''.
        by rewrite Hfb' Htfb.
  Qed.

  (** Preservation of the semantics for forbidden deroute *)
  Lemma match_forbidden_deroute:
    forall e1 e2 to e1' b,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.forbidden_deroute fp e1 to = (b, e1')
      -> exists e2',
          FPE_BS.Common_Sem.forbidden_deroute (` fpe) e2 to = (b, e2')
          /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FPE_BS.Common_Sem.forbidden_deroute
            /FP_BS.Common_Sem.forbidden_deroute
    => e1 e2 to e1'0 b0 He.
    rewrite (get_match_block He).
    by apply match_test_forbidden_deroutes with (e2 := e2).
  Qed.

  (** *** Change of block lemmas *)

  (** Lemmas about the change of block *)
  Lemma eq_normalise_block_id:
    forall id,
      FP.Common.normalise_block_id fp id
          = FP_E_WF.Common.normalise_block_id (` fpe) id.
  Proof.
    move => id.
    by rewrite /FP.Common.normalise_block_id
                  /FP_E_WF.Common.normalise_block_id
                  (extend_eq_nb_blocks Hfp)
                  /FP_E_WF.get_nb_blocks.
  Qed. 

  Lemma match_c_change_block:
    forall from to,
      FP_BS.Common_Sem.c_change_block fp from to =
          FPE_BS.Common_Sem.c_change_block (` fpe) from to.
  Proof.
    rewrite /FP_BS.Common_Sem.c_change_block
            /FPE_BS.Common_Sem.c_change_block
    => from to.
    by rewrite (eq_normalise_block_id to)
                  (eq_on_exit from Hfp) (eq_on_enter _ Hfp).
  Qed.

  Lemma match_change_block:
    forall e1 e2 e1' to,
      e1 ~( fpe )~ e2
      -> e1' = FP_ENV.Def.change_block fp e1 to
      -> exists e2', 
          FPE_ENV.Def.change_block (` fpe) e2 to = e2'
          /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FP_ENV.Def.change_block
            /FPE_ENV.Def.change_block
    => e1 e2 e1' to He Hc.
    rewrite (eq_normalise_block_id to) in Hc.
    remember (FPE_ENV.Def.change_block (` fpe) e2 to) as e2'.
    exists e2'. split; try by [].

    (* Destruct the envs *)
    destruct e1 as [[idb1 ids1 lb1 ls1] t1].
    destruct e1' as [[idb1' ids1' lb1' ls1'] t1'].
    destruct e2 as [[idb2 ids2 lb2 ls2] t2].
    destruct e2' as [[idb2' ids2' lb2' ls2'] t2'].

    (* Get info about the envs *)
    destruct He as [Hb Heq Hlb Hleq Ht];
      rewrite //= in Hb, Heq, Hlb, Hleq, Ht.

    destruct (idb1 =? to) eqn:Hl; rewrite //= Hl in Hc;
    rewrite /FPE_ENV.Def.change_block //= -Hb Hl in Heqe2';
    injection Hc as Hb1 Hs1 Hlb1 Hls1 Ht1; subst;
    injection Heqe2' as Hb2 Hs2 Hlb2 Hls2 Ht2; subst.

    all: split; rewrite //=; try lia; try by [].
    all: try apply (match_env_stages_init _ Hfp).
  Qed.

  Lemma match_goto_block:
    forall e1 e2 e1' to,
      e1 ~( fpe )~ e2
      -> e1' = FP_BS.Common_Sem.goto_block fp e1 to
      -> exists e2', 
        FPE_BS.Common_Sem.goto_block (` fpe) e2 to = e2'
          /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FP_BS.Common_Sem.goto_block
             /FPE_BS.Common_Sem.goto_block => e1 e2 e1' to He Hc.

    (** Test if there is a forbidden deroute *)
    destruct (FP_BS.Common_Sem.forbidden_deroute) as [res e1''] eqn:Hfbs.
    have Hfbs' := match_forbidden_deroute He Hfbs;
    destruct Hfbs' as [e2'' [Hfbse2'' He2'']].
    destruct res; rewrite Hfbse2''.

    (** Forbidden deroute raised => do not change block *)
    by exists e2''; rewrite Hc; split.

    (** No forbidden deroute *)
    remember (FP_ENV.Def.change_block _ _ _) as e1'''.
    have Hcb' := match_change_block He2'' Heqe1''';
    destruct Hcb' as [e2''' [Hcbe2' He2''']].

    remember (FPE_ENV.Def.get_nav_block e2) as idb2.
    rewrite Hcbe2' Hc (get_match_block He)
              -(match_c_change_block idb2 to) -Heqidb2 //=.

    eexists; split; try reflexivity.
    by apply match_app_trace.
  Qed.

  (** ** Exception lemmas *)

  (** Lemmas for the test of exceptions *)
  Lemma eq_test_exception:
    forall e1 e2 e1' b ex,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.test_exception fp e1 ex = (b, e1')
      -> (exists e2', 
          FPE_BS.Common_Sem.test_exception (` fpe) e2
                ex =(b, e2')
          /\ e1' ~( fpe )~ e2').
  Proof.
    move => e1 e2 e1'_f res ex He Hex.

    rewrite //= /FP_BS.Common_Sem.test_exception in Hex;
    destruct (get_expt_block_id ex =? FP_ENV.Def.get_nav_block e1)
      eqn:Hd; rewrite //= /FPE_BS.Common_Sem.test_exception
                                -(get_match_block He) Hd.

    (* * Already in the block -> next exception *)
    inversion Hex; subst e1'_f res. by exists e2.

    (* * Not in the block -> evalutation of the condition *)
    remember (get_expt_cond ex) as c;
    (* Destruct the eval and apply the result in the goal *)
    destruct (FP_ENV.Def.evalc e1 c) as [[] e'] eqn:Hevalc;
      have Heval := match_evalc He Hevalc;
      destruct Heval as [e2' [Heval He2']];
      rewrite Heval //=.

    (*   + Exception raised *)
    simpl in Hex.
    destruct (FP_BS.Common_Sem.goto_block _ _ _) as [e'' o''] eqn:Hg;
      symmetry in Hg.
    remember (opt_c_code_to_trace _) as t.
    have Hg' := match_goto_block (match_app_trace t He2') Hg;
      destruct Hg' as [e2'' [Hg' He2'']]; exists e2''.
      injection Hex as Hres He1'.
      by rewrite -He1' Hg' -Hres; split.

    (*   + Exception not raised -> next exception *)
    inversion Hex; subst res e1'_f. by exists e2'.
  Qed.

  Lemma eq_test_exceptions:
    forall excpt e1 e2 e1' b,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.test_exceptions fp e1 excpt = (b, e1')
      -> (exists e2', 
          FPE_BS.Common_Sem.test_exceptions (` fpe) e2
          excpt =(b, e2')
          /\ e1' ~( fpe )~ e2').
  Proof.
    induction excpt as [|ex excpt IHe]; move => e1 e2 e1' res He Hexs.

    (* Empty exception list *)
    exists e2. injection Hexs as Hres He1.
    rewrite -He1 -Hres. split; by [].

    (* Exception list not empty *)
    rewrite //= in Hexs;
    destruct (FP_BS.Common_Sem.test_exception fp e1 ex)
      as [res' e1''] eqn:Hex.

    destruct (eq_test_exception He Hex) as [e2' [Hex' He2']].

    destruct res'.

    (* Exception raised -> stop *)
    exists e2'; split; inversion Hexs; subst res e1''; try by [].
    by rewrite //= Hex'.

    (* Exception not raised -> continue *)
    rewrite //= in Hexs;
    destruct (FP_BS.Common_Sem.test_exceptions fp e1'' excpt)
      as [res'' e1'''] eqn:Hexs';
      inversion Hexs; subst res'' e1'''.

    destruct (IHe _ _ _ _ He2' Hexs') as [e2'' [Hexs'' He2'']].
    exists e2''; split; try by []. by rewrite //= Hex' Hexs''. 
  Qed.

  (** Preservation of the semantics for global exceptions *)
  Lemma eq_test_global_exceptions:
    forall e1 e2 e1' b,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.test_exceptions fp e1 
          (FP.Common.get_fp_exceptions fp) = (b, e1')
      -> (exists e2', 
          FPE_BS.Common_Sem.test_exceptions (` fpe) e2
          (FP_E_WF.Common.get_fp_exceptions (` fpe)) =(b, e2')
          /\ e1' ~( fpe )~ e2').
  Proof.
    move =>  e1 e2 e1' b He Hex.
    remember (FP.Common.get_fp_exceptions fp) as excpt.
    apply (eq_test_exceptions He) in Hex.
    destruct Hex as [e2' Hex]; exists e2'.
    by rewrite -(eq_get_fp_exceptions Hfp) -Heqexcpt.
  Qed.

  (** Preservation of the semantics for local exceptions *)
  Lemma eq_test_local_exceptions:
    forall e1 e2 e1' b,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.test_exceptions fp e1 
          (FP_ENV.Def.get_local_exceptions fp e1) = (b, e1')
      -> (exists e2', 
          FPE_BS.Common_Sem.test_exceptions (` fpe) e2
            (FPE_ENV.Def.get_local_exceptions (` fpe) e2 ) 
              = (b, e2')
          /\ e1' ~( fpe )~ e2').
  Proof.
    move => e1 e2 e1' b He Hex.
    remember (FP_ENV.Def.get_local_exceptions fp e1) as excpt.
    apply (eq_test_exceptions He) in Hex.
    destruct Hex as [e2' Hex]; exists e2'.
    have Hexcpt: excpt = 
              FPE_ENV.Def.get_local_exceptions (` fpe) e2.
    { rewrite /FPE_ENV.Def.get_local_exceptions
              /FPE_ENV.Def.get_current_block
              -(get_match_block He) Heqexcpt
              /FP_ENV.Def.get_local_exceptions
              /FP_ENV.Def.get_current_block. 
      by apply FPE_get_local_exceptions. }
    by rewrite -Hexcpt.
  Qed.

  (** Preservation of the semantics for exceptions *)
  Lemma match_sem_exceptions:
    forall e1 e1' e2 b,
      e1 ~( fpe )~ e2
      -> FP_BS.Common_Sem.exception fp e1 = (b, e1')
      -> exists e2',
            FPE_BS.Common_Sem.exception (` fpe) e2 = (b, e2')
            /\ e1' ~( fpe )~ e2'.
  Proof.
     rewrite /FP_BS.Common_Sem.exception /FPE_BS.Common_Sem.exception //=
      => e1 e1' e2 b Henv Hex.
    destruct FP_BS.Common_Sem.test_exceptions as [b' e1''] eqn:Hd.
    have Hexp := (eq_test_global_exceptions Henv Hd).
    destruct Hexp as [e2' [Hexp He2']]; destruct b'.

    (** Global exception raised *)
    exists e2'; injection Hex as Hb He.
    by rewrite Hexp -Hb -He.

    (** Global exceptions not raised *)
    (** Execute local *)
    remember (FP.Common.get_code_block_pre_call _) as t.
    destruct (FP_BS.Common_Sem.test_exceptions fp _
                (FP_ENV.Def.get_local_exceptions fp e1''))
                as [b'' e1'''] eqn:Hle.
    have He2'' := match_app_trace t He2'.
    have Hlexp := (eq_test_local_exceptions He2'' Hle).
    destruct Hlexp as [e2'' [Hlexp He2''']].

    (** Use the result of the execution *)
    exists e2''; injection Hex as Hb He; subst t.
    rewrite (eq_get_code_block_pre_call Hfp He2') in Hlexp.
    by rewrite Hexp  Hlexp Hb -He.
  Qed.

  (** ** Stages lemmas  *)

  (** Tactic to destruct match_env *)
  Ltac destruct_match_env He :=
    assert (He' := He);
      destruct He' as [Hidb Hs Hlidb Hls Ht];
      rewrite //= in Hidb, Hs, Hlidb, Hls, Ht.

  (** *** Common stages lemmas *)
  (** Lemmas about the preservation of the semantics when the list of
      stages to execute is empty. *)
  Lemma match_sem_run_nil:
    forall idb lidb lstages t e2 e1',
        {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages := [::];
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
        |} ~( fpe )~ e2
      -> FP_BS.run_step_rec fp idb [::] lidb lstages t = e1'
      -> exists e2',
          FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    move => idb lidb lstages t e2 e1' He;
      destruct_match_env He => Hr.

    (** Get the extended stage *)
    inversion Hs as [stages stages_e n Heq Hstages Hdrop].
    inversion Heq as [H Hstages_e |H|H|H|H].
    rewrite -Hstages_e //= in Hdrop. have Hdrop' := Hdrop.
    apply drop_get_default in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage -Hdrop.

    (** Rebuild e1 and He *)
    remember (FP_ENV.mk_fp_env idb [::] lidb lstages t) as e1;
    have He1 : e1 ~( fpe )~ e2 . {rewrite Heqe1 //=. }

    (** Next block *)
    inversion Hr as [Hnext]. rewrite //= /FP_BS.next_block //= in Hnext.
    rewrite /FPE_BS.default_sem /FPE_BS.next_block //= -Hidb.

    (** Test if it is the last possible block *)
    destruct (idb <? (FP_E_WF.get_nb_blocks (extend_flight_plan_not_wf fp)) - 1) eqn:Hlt.

    (** Jump into the next block *)
    remember (FP_BS.Common_Sem.goto_block fp _ _) as e1''.
    have He1' := reset_stage_preserve_match He1 Hdrop'.
    rewrite /= in Heqe1. rewrite -Heqe1 in Heqe1''.
    have Hg' := match_goto_block He1' Heqe1''.
    destruct Hg' as [e2'' [Hg' He2'']]; exists e2''.
    rewrite /FP_BS.next_block //= -Heqe1 -Heqe1'' ?extend_fp_nwf_proj.
    have eq_nb_blocks := extend_eq_nb_blocks.
    rewrite (eq_nb_blocks fp fpe). by rewrite Hlt. 
    by [].

    (** Jump to the 255 block *)
    remember (FP_BS.Common_Sem.goto_block fp _ idb) as e1''.
    have He1' := reset_stage_preserve_match He1 Hdrop'.
    rewrite /= in Heqe1. rewrite -Heqe1 in Heqe1''.
    have Hg' := match_goto_block He1' Heqe1''.
    destruct Hg' as [e2'' [Hg' He2'']]; exists e2''.
    rewrite /FP_BS.next_block //= -Heqe1 -Heqe1'' ?extend_fp_nwf_proj.
    have eq_nb_blocks := extend_eq_nb_blocks.
    rewrite (eq_nb_blocks fp fpe). by rewrite Hlt.
    by []. 
  Qed.

  (** Definition of the continue function, defined internaly in
      FP_BS.run_step_rec *)
      Definition continue (fp: FP.flight_plan) (idb: block_id) 
                          (stages: FP_ENV.exec_stages) (lidb: block_id)
                          (lstages: FP_ENV.exec_stages):
                            FP_BS.continue_fun :=
        fun (e : FP_ENV.fp_env) =>
          FP_BS.run_step_rec fp idb stages lidb lstages
                        (FP_ENV.Def.get_trace e).

  (** Continue function for exented stages *)
  Definition continue_extended (fpe: FP_E_WF.flight_plan_wf)
                              (res: (bool * FPE_ENV.fp_env))
                                    : FPE_ENV.fp_env :=
    let '(b, e) := res in
      if b then
        FPE_BS.run_step fpe e
      else e.

  (** Definition of the recursive properties for the matching relation
      between FP_BS.run_step_rec and FPE_BS.run_step. *)
  Definition IH_match_run_step (fp : FP.flight_plan)
                                (fpe : FP_E_WF.flight_plan_wf)
                                  (stages: seq FP.fp_stage) :=
    forall idb lidb lstages t e1' e2,
      {|
            FP_ENV.get_state := {|
              FP_ENV.nav_block := idb;
              FP_ENV.nav_stages := stages;
              FP_ENV.last_block := lidb;
              FP_ENV.last_stages := lstages;
            |};
            FP_ENV.get_trace_env := t
      |} ~( fpe )~ e2
      -> FP_BS.run_step_rec fp idb stages lidb lstages t = e1'
      -> exists e2',
        FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.

  (** Lemma about the preservation of the semantics when there is a next
      stage that continue *)
  Lemma match_next_stage_continue:
    forall idb s stages lidb lstages t e2 e1' e2' s_e stages_e n,
      {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages := (s :: stages)%SEQ;
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
      |} ~( fpe )~ e2
    -> IH_match_run_step fp fpe stages
    -> e2' = FPE_ENV.next_stage e2
    -> ((s_e :: stages_e) ++ [:: FP_E.DEFAULT n])%SEQ =
        FPE_ENV.get_current_stages (` fpe) e2
    -> MATCH_FP_FPE.match_stages stages stages_e
    -> FP_BS.run_step_rec fp idb stages lidb lstages t = e1'
    -> exists e2'',
          FPE_BS.run_step fpe e2' = e2'' /\ e1' ~( fpe )~ e2''.
  Proof.
    move =>  idb s stages lidb lstages t e2
             e1' e2' s_e stages_e n He IH He2' Hdrop Heq Hr;
      destruct_match_env He.

    apply (IH idb lidb lstages t e1' e2') in Hr; try by [].
    { destruct e2 as [[idb2 ids2 lidb2 lids2] t2] eqn:He2.
      rewrite He2' Hidb Hlidb.
      repeat (split; try by []).
      rewrite //= in Hdrop.
      rewrite /MATCH_FP_FPE.match_env_stages. apply drop_incr in Hdrop.
      rewrite -Hdrop. by apply MATCH_FP_FPE.match_default.
    }
  Qed.

  (** Lemma about the preservation of the semantics when there is a next
      stage that not break *)
  Lemma match_next_stage_break:
    forall idb s stages stages' lidb lstages
          t e1' e2 e2' s_e stages_e n,
          {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages := (s :: stages)%SEQ;
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
          |} ~( fpe )~ e2
        -> ([:: s_e & stages_e] ++ [:: FP_E.DEFAULT n])%SEQ
            = FPE_ENV.get_current_stages (` fpe) e2
        -> (FP_ENV.mk_fp_env idb stages' lidb lstages t) = e1'
        -> FPE_ENV.next_stage e2 = e2'
        -> MATCH_FP_FPE.match_stages stages' stages_e
      -> (continue_extended fpe (FPE_BS.break e2')) = e2'
            /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  idb s stages stages' lidb lstages
          t e1' e2 e2' s_e stages_e n He Hc He1' He2' Heq.
    split; try by [];

    destruct e2 as [[idb2 ids2 lidb2 lids2] t2] eqn:He2;
      destruct_match_env He.

    rewrite -He1' -He2' /FPE_ENV.next_stage //=.

    repeat (split; try by []).

    rewrite /FPE_ENV.get_current_stages //= in Hc.
      apply drop_incr in Hc.
    rewrite /MATCH_FP_FPE.match_env_stages -Hc.
    by apply /MATCH_FP_FPE.match_default.
  Qed.

  (** *** While semantics *)

  (** Lemmas about the jump at the end of the while *)
  Lemma jump_end_while:
    forall nb p_e ids stages_e n e,
      nb = (FP_E.get_end_while_id p_e - (FP_E.get_while_id p_e + 1))
      -> ((FP_E.WHILE ids p_e :: stages_e) ++ [:: FP_E.DEFAULT n])%SEQ
          = FPE_ENV.get_current_stages (` fpe) e
      -> drop (FP_E_WF.get_end_while_id p_e + 1)
          (FP_E.get_stages (MATCH_FP_FPE.fpe fpe)
          (FPE_ENV.Def.get_nav_block e))
            = (drop (1 + nb) stages_e) ++ [:: FP_E.DEFAULT n].
  Proof.
    move =>  nb p_e ids' stages_e n e Hnb Hc;
    destruct fpe as [fpe' Hwf] eqn:Hfpe;
    destruct e as [[idb ids lidb lids] t] eqn:He;
    rewrite -He -Hfpe //= in Hc; rewrite //=.

    have Hc' := Hc; symmetry in Hc';
      apply get_current_stages_to_stage in Hc'.
    rewrite /FPE_ENV.get_current_stage extend_fp_nwf_proj Hfpe He //=
      in Hc'; have Hgetw := Hc';
      apply (FP_E_WF.get_wf_while Hwf) in Hc';
    destruct Hc' as [Hlw [Hlt [Hgetew Hids]]].
    remember (subseq_stages fpe' idb (FP_E_WF.get_while_id p_e)
                                      (FP_E_WF.get_end_while_id p_e)) as b.

    rewrite /FPE_ENV.get_current_stages in Hc; symmetry in Hc;
      apply cons_drop_nth in Hc.
    rewrite /FP_E_WF.get_stage in Hgetew.
    remember (FP_E_WF.default_stage fpe' idb) as d.
    remember (FP_E_WF.get_end_while_id p_e) as lew.
    rewrite Hnb -Hlw.
    apply drop_drop_app with (d := d) (e :=  FP_E_WF.END_WHILE lew p_e b);
      try by [].
    * by rewrite Heqd.
    * ssrlia.
    * by rewrite He extend_fp_nwf_proj Hfpe //= -Hlw in Hc.
  Qed.

  (** Preservation of the semantics for while loop when the condition is
      true and the execution continue *)
  Lemma match_sem_while_next_stage:
    forall idb params block stages lidb lstages t e1 e2 e1' e2'
            ids p_e stages_e n,
    e1 ~( fpe )~ e2
    -> e1 = FP_ENV.mk_fp_env idb
      (FP.WHILE params block :: stages)%SEQ lidb lstages t
    -> MATCH_FP_FPE.match_stages (FP.WHILE params block :: stages)
      (FP_E.WHILE ids p_e :: stages_e)
    -> ((FP_E.WHILE ids p_e :: stages_e) ++ [:: FP_E.DEFAULT n])%SEQ
        = FPE_ENV.get_current_stages (` fpe) e2
    -> e1' = FP_ENV.update_nav_stage e1 block
    -> e2' = FPE_ENV.next_stage e2
    -> e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FP_ENV.update_nav_stage
            /FPE_ENV.next_stage
    =>  idb params block stages lidb lstages t
            [[idb1 stages1 lidb1 lstages1] t1] [[idb2 ids2 lidb2 lids2] t2]
            [[idb1' stages1' lidb1' lstages1'] t1']
            [[idb2' ids2' lidb2' lids2'] t2']
            //= ids p_e stages_e n Heq He1 Hmatch Hget He1' He2'.
    injection He1' as Hidb1 Hstages1 Hlidb1 Hlstages1 Ht1.
    injection He2' as Hidb2 Hids2 Hlidb2 Hlids2 Ht2.
    destruct_match_env Heq; subst.

    (** Rewrite hypothesis and destruct match_env *)
    repeat (split; try by []).

    (** Get stages1 *)
    injection He1 as Hb1 Hs1 Hlb1 Hls1 Ht1'.
    rewrite /FPE_ENV.get_current_stages //= in Hget;
      apply drop_incr in Hget.
    rewrite /MATCH_FP_FPE.match_env_stages -Hget.
    apply MATCH_FP_FPE.match_default.
    rewrite Hs1.

    (** Prove the equivalence *)
    inversion Hmatch. inversion H2.
    have Hne : (FP_E.DEFAULT n'') <> (FP_E.END_WHILE n' p_e' b').
      by [].
    have Hstages_e := list_eq_take_nth_drop Hne H9.
    rewrite Hstages_e. apply app_match_stages; try by [].
    replace (FP.WHILE params block :: stages)
      with ([:: FP.WHILE params block] ++ stages); try by [].
    apply app_match_stages; try by [].
    apply MATCH_FP_FPE.match_end_while; try by [].
    by apply MATCH_FP_FPE.match_nil.
  Qed.

  (** Preservation of the semantics when the while loop is exited *)
  Lemma match_sem_while_exit_loop:
    forall e1 e2 e2' idb params block stages lidb
          lstages t ids p_e stages_e n,
    e1 ~( fpe )~ e2
    -> MATCH_FP_FPE.match_stages (FP.WHILE params block :: stages)
                                 (FP_E.WHILE ids p_e :: stages_e)
    -> ((FP_E.WHILE ids p_e :: stages_e) ++ [:: FP_E.DEFAULT n])%SEQ
        = FPE_ENV.get_current_stages (` fpe) e2
    -> e1 = FP_ENV.mk_fp_env idb
            (FP.WHILE params block :: stages)%SEQ lidb lstages t
    -> e2' = FPE_ENV.update_nav_stage e2
              (FP_E_WF.get_end_while_id p_e + 1)
    ->  {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages := stages;
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
        |} ~( fpe )~ e2'.
  Proof. 
    move =>  e1 e2 e2' //= idb params
          block stages lidb lstages t ids p_e stages_e n Heq He Hc
          He1 He2'; rewrite He1 in Heq.
    destruct e2 as [[idb2 ids2 lidb2 lids2] t2] eqn:He2.
    destruct_match_env Heq.

    rewrite He2' //=.
    repeat (split; try by []).

    (** inversion He. *)
    have Hc' := Hc; rewrite -He2 in Hc'.
    rewrite /FPE_ENV.get_current_stages //= in Hc.
    rewrite /MATCH_FP_FPE.match_env_stages -Hc //= in Hs.
    inversion Hs as [stages' stages_e' n' Heq' Hstages_e Hdrop].

    have Hstages_e' : stages_e' = FP_E.WHILE ids p_e :: stages_e.
    { apply (app_inj_tail stages_e' (FP_E.WHILE ids p_e :: stages_e))
       in Hdrop. by destruct Hdrop. }
    rewrite Hstages_e' in Heq'. inversion Heq'. inversion H2.

    rewrite /MATCH_FP_FPE.match_env_stages.

    replace (drop _ _) with ((drop (1 + nb) stages_e) 
                              ++ [:: FP_E.DEFAULT n]); try by []. 
    symmetry.
    replace idb2 with  (FPE_ENV.Def.get_nav_block e2);
      last first. by rewrite He2. rewrite //=.
    apply jump_end_while with (nb := nb) (ids := ids)
                              (stages_e := stages_e) (n := n); try by [].
  Qed.

  (** Preservation of the semantics for the while_sem function *)
  Lemma match_sem_while_global:
    forall idb block params stages lidb lstages t e2 e1' 
            ids p_e stages_e n,
      {|
        FP_ENV.get_state := {|
          FP_ENV.nav_block := idb;
          FP_ENV.nav_stages :=
            (FP.WHILE params block :: stages)%SEQ;
          FP_ENV.last_block := lidb;
          FP_ENV.last_stages := lstages;
        |};
        FP_ENV.get_trace_env := t
      |} ~( fpe )~ e2
      -> IH_match_run_step fp fpe stages
      -> MATCH_FP_FPE.match_stages (FP.WHILE params block :: stages)
                                   (FP_E.WHILE ids p_e :: stages_e)
      -> ((FP_E.WHILE ids p_e :: stages_e) ++ [:: FP_E.DEFAULT n])%SEQ
            = FPE_ENV.get_current_stages (` fpe) e2
      -> FP_E_WF.get_while_cond p_e = params
      -> FP_BS.while_sem (FP_ENV.mk_fp_env idb
                          (FP.WHILE params block :: stages) lidb lstages t)
                          (continue fp idb stages lidb lstages) params
                          block = e1'
      -> exists e2',
          (continue_extended fpe (FPE_BS.while_sem e2 p_e)) = e2'
            /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /continue_extended
            /FPE_BS.while_sem
    =>  idb block params stages lidb lstages
         t e2 e1'' ids p_e stages_e n He IH Heqw Hget Hp Hr;
      destruct_match_env He.

    (** Rebuild e1 *)
    remember (FP_ENV.mk_fp_env idb 
                ((FP.WHILE params block :: stages)) lidb lstages t) as e1;
    have He1 : e1 ~( fpe )~ e2 by rewrite Heqe1.

    rewrite /FP_BS.while_sem in Hr.

    (** Evaluate the condition *)
    destruct (FP_ENV.Def.evalc e1 params) as [b' e1'] eqn:Hevalc.
    have Heval := match_evalc He1 Hevalc;
      destruct Heval as [e2' [Heval He2']]; rewrite Hp Heval.
    rewrite (evalc_get_current_stages (` fpe) Heval)
      in Hget.

    (** Build e1' *)
    destruct e1' as [[idb1' stages1' lidb1' lstages1'] t1'] eqn:He1';
      rewrite -He1' in He2'.
    have Heqe1': e1' = FP_ENV.mk_fp_env idb
              (FP.WHILE params block :: stages)%SEQ lidb lstages t1'.
    { rewrite /FP_ENV.Def.evalc Heqe1 //= in Hevalc.
      remember (EVAL_Def.eval t params) as b.
      injection Hevalc as Hb Hidb1' Hstages1' Hlidb1' Hlstages1' Ht'.
      by rewrite He1'  Hidb1' Hstages1' Hlidb1' Hlstages1'. }

    (*** Test the condition *)
    destruct b'; rewrite /FPE_BS.break /FPE_BS.continue.

    (*** Continue the execution of the loop *)
    eexists; split; try by []. rewrite -He1' in Hr. rewrite -Hr.
    apply match_app_trace.
    eapply (match_sem_while_next_stage He2' Heqe1' Heqw Hget);
      try by [].

    (*** Exit the loop *)
    rewrite /continue //= in Hr.
    remember (FP_BS.run_step_rec fp _ stages _ _ _) as e1'0;
      symmetry in Heqe1'0.
    remember (FPE_ENV.update_nav_stage e2'
                (FP_E_WF.get_end_while_id p_e + 1)) as e2''.
    have He2'' := Heqe2''.
    apply (match_sem_while_exit_loop He2' Heqw Hget Heqe1') in Heqe2''.
    apply (IH idb lidb lstages t1' e1'0 e2'' Heqe2'') in Heqe1'0.
    destruct Heqe1'0 as [e2'0 [Hre2' Heqe'0]]; exists e2'0.
    by rewrite  Hre2' -Hr.
  Qed.

  (** Lemma about the change of stage from end while to the while *)
  Lemma get_stages_end_while_to_while:
    forall (fpe: FP_E_WF.flight_plan_wf) e e' p_e ids b stages n,
      (FP_E.END_WHILE ids p_e b :: stages) ++ [:: FP_E.DEFAULT n]
          =  FPE_ENV.get_current_stages (` fpe) e
      -> e' = FPE_ENV.update_nav_stage e (FP_E_WF.get_while_id p_e)
      -> (FP_E.WHILE (FP_E_WF.get_while_id p_e) p_e :: b)
          ++ (FP_E.END_WHILE ids p_e b :: stages) 
          ++ [:: FP_E.DEFAULT n]
          = FPE_ENV.get_current_stages (` fpe) e'.
  Proof.
    rewrite /FPE_ENV.update_nav_stage
            /FPE_ENV.get_current_stages
    => fpe e e' p_e ids' b stages n Hgete He'.
    destruct fpe as [fpe' Hwf] eqn:Hfpe;
    destruct e as [[idb ids lidb lids] t] eqn:He;
    rewrite //= in He'; rewrite //= in Hgete; rewrite He' //=.

    assert(Hnth: FPE_ENV.get_current_stage (` fpe) e 
                  = FP_E.END_WHILE ids' p_e b).
    { apply get_current_stages_to_stage
        with (stages := stages ++  [:: FP_E.DEFAULT n]).
      by rewrite /FPE_ENV.get_current_stages He Hfpe //=. }

    rewrite /FPE_ENV.get_current_stage Hfpe He //= in Hnth.
    have Hnth' := Hnth;
      apply (FP_E_WF.get_wf_end_while Hwf) in Hnth';
      destruct Hnth' as [Hgetids [Hgetw [Hids Hidb]]].

    have Hgetw' := Hgetw;
    apply (FP_E_WF.get_wf_while Hwf) in Hgetw';
    destruct Hgetw' as [Hpe [Hle [Hgetew [Hgetw' Hidb']]]].

    rewrite Hgetids Hnth Hids in Hgetew. injection Hgetew as Hb.

    rewrite /FP_E_WF.get_stage in Hgetw.
    remember (FP_E_WF.default_stage fpe' idb) as d.
    symmetry; apply drop_cons_nth with (d := d); try by [].
    by rewrite Heqd.

    rewrite -Hids -Hgetids /subseq_stages /subseq in Hb.

    apply drop_extract with (j := FP_E_WF.get_end_while_id p_e); try by [].
    ssrlia.

    by rewrite Hgetids -Hgete.
  Qed.

  (** Lemma to prove the match_stages between FP.WHILE and FPE.WHILE after
      that the loop is restarted. *)
  Lemma match_while_stages:
    forall fpe idb ids p_e block_e 
            stages_while block stages stages_e params n,
      FP_E.get_stage (MATCH_FP_FPE.fpe fpe) idb ids
        = FP_E.END_WHILE ids p_e block_e
      -> stages_while ++ [:: FP_E.DEFAULT n]
            = drop (FP_E_WF.get_while_id p_e).+1
                    (FP_E.get_stages (MATCH_FP_FPE.fpe fpe) idb)
      -> stages_while 
          = block_e ++ (FP_E.END_WHILE ids p_e block_e :: stages_e)
      -> MATCH_FP_FPE.match_stages block block_e
      -> MATCH_FP_FPE.match_stages stages stages_e
      -> params = FP_E.get_while_cond p_e
      -> MATCH_FP_FPE.match_stages
          (FP.WHILE params block :: stages)
          (FP_E.WHILE (FP_E_WF.get_while_id p_e) p_e :: stages_while).
  Proof.
    move => fpe idb ids p_e block_e stages_while block
            stages stages_e params n Hget Hdrop Hstagew Hb Hs Hparams.
    destruct fpe as [fpe' Hwf] eqn: Hfpe.
    have Hget' := Hget; apply (FP_E_WF.get_wf_end_while Hwf) in Hget';
      destruct Hget' as [Hlew [Hgetw Hids]].

    have Hgetw' := Hgetw; apply (FP_E_WF.get_wf_while Hwf) in Hgetw';
      destruct Hgetw' as [Hlw [Hlt [Hgetew Hids']]].

    remember (FP_E_WF.get_while_id p_e) as lw.

    (* subseq_stages *)
    rewrite //= -Hlew in Hget.
    rewrite /subseq_stages /subseq Hget in Hgetew.
    injection Hgetew as Hblock.

    rewrite //= Hstagew in Hdrop.

    have Hsize : size block_e = (ids - (lw + 1)).
    { rewrite -Hdrop -cat_app -cat_app -catA Hlew in Hblock.
      by apply take_app_impl_size in Hblock. }

    apply MATCH_FP_FPE.match_while
      with (p_e' := p_e) (b' := block_e)
            (nb := ids - (lw + 1))
            (n' := ids) (n'' := n); try by [].

    - rewrite -Hlew; ssrlia.
    - have Htake: take (ids - (lw + 1)) stages_while = block_e.
      { rewrite Hblock Hlew Hstagew -Hdrop -cat_app -cat_app -catA.
        repeat rewrite take_size_cat; try by []. }
      by rewrite Htake.
    - rewrite Hstagew nth_of_app Hsize; try by [].
      have Hi: ids - (lw + 1) - (ids - (lw + 1)) = 0. ssrlia.
      by rewrite Hi.
    - have Hd: drop (1 + (ids - (lw + 1))) stages_while = stages_e.
      { by rewrite Hstagew drop_cat Hsize add1n
                  succ_not_lt subSnn //= drop0. }
      by rewrite Hd.
  Qed.

  (** Main lemma about the preservation of the semantics for the while
      loop.*)
  Lemma match_sem_while:
    forall idb block params stages lidb lstages t e2 e1',
      {|
        FP_ENV.get_state := {|
          FP_ENV.nav_block := idb;
          FP_ENV.nav_stages :=
            (FP.WHILE params block :: stages)%SEQ;
          FP_ENV.last_block := lidb;
          FP_ENV.last_stages := lstages;
        |};
        FP_ENV.get_trace_env := t
      |} ~( fpe )~ e2
      -> IH_match_run_step fp fpe stages
      -> FP_BS.while_sem (FP_ENV.mk_fp_env idb
                          (FP.WHILE params block :: stages) lidb lstages t)
            (continue fp idb stages lidb lstages) params block = e1'
      -> exists e2',
          FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  idb block params stages
              lidb lstages t e2 e1' He IH Hr;
      destruct_match_env He.

    (*** Get the extended stage*)
    inversion Hs as [stages' stages_e n Heq Hstages Hdrop].
    inversion Heq.

    (** While cond *)
    inversion H0.

    (** End while case *)
    inversion H1. rewrite -H3 in Hdrop.
    have Hdrop' := Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation
              /FPE_BS.run_stage Hdrop
            /FPE_BS.end_while_sem.
    remember (FP_E_WF.get_while_id p_e) as ids_w.
    remember (FPE_ENV.update_nav_stage e2 ids_w) as e2'.
    have Hdrope2':
            ((FP_E.WHILE ids_w p_e :: b')
            ++ stages_e
            ++ [:: FP_E.DEFAULT n])%SEQ
              = drop (FPE_ENV.get_nav_stage e2')
                (FP_E.get_stages (MATCH_FP_FPE.fpe fpe)
                                 (FPE_ENV.Def.get_nav_block e2')).
    { rewrite -H3 Heqids_w. rewrite Heqids_w in Heqe2'.
      apply get_stages_end_while_to_while 
        with (p_e := p_e) (e := e2) (b := b') (stages := stages'0);
          try by []. 
    }
    remember (b' ++ stages_e) as stages_while.
    have He2': MATCH_FP_FPE.match_stages
                  (FP.WHILE params block :: stages)
                  (FP_E.WHILE ids_w p_e :: stages_while).
    { destruct e2 as [[idb2 ids2 lidb2 lids2] t2] eqn:He2;
      rewrite /FPE_ENV.get_current_stage //= in Hdrop;
        rewrite //= in Hdrop'. rewrite Heqids_w.

      have Hdrop_b := Hdrop.
      destruct fpe as [fpe' Hwf] eqn:Hfpe.
        rewrite extend_fp_nwf_proj Hfpe in Hdrop_b.
        apply (FP_E_WF.get_wf_end_while Hwf) in Hdrop_b;
        destruct Hdrop_b as [Hlew [Hgetew [Hids Hidbp]]].
      rewrite -Hids extend_fp_nwf_proj in Hdrop.
      rewrite -H3 -Hids in Heqstages_while.

      rewrite Heqe2' -Hfpe Heqids_w //= in Hdrope2'.
        apply drop_incr in Hdrope2'.
      rewrite -cat_app in Heqstages_while.
      rewrite catA -H3 -Hids cat_app -Heqstages_while addn1 in Hdrope2'.

      apply match_while_stages
        with (fpe := fpe) (idb := idb2) (ids := ids2) (n:= n)
             (block_e := b') (stages_e := stages'0); try by [].
    }
    apply match_sem_while_global
      with (e2 := e2') (p_e := p_e) 
            (ids:= ids_w) (stages_e := stages_while) (n := n) in Hr;
      try rewrite /FPE_ENV.get_current_stages; try by [].
    - rewrite Heqe2'.
      repeat (split; try by []). rewrite /MATCH_FP_FPE.match_env_stages.
      destruct e2 as [[idb2 ids2 lids2 lidb2] t2] eqn:He2.
      rewrite Heqe2' //= in Hdrope2'. 
      rewrite //= -Hdrope2'. rewrite Heqstages_while //= in He2'.
      have Hmatch := MATCH_FP_FPE.match_default n He2'.
      rewrite -cat_app //= in Hmatch.
      by rewrite -list_cons_app.
    - rewrite Heqstages_while -Hdrope2' -cat_app app_comm_cons -cat_app.
      symmetry. apply app_assoc.

    (** While case *)
    rewrite -H7 in Hdrop; rewrite -H7 in Heq.
    have Hdrop' := Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage Hdrop
            /FPE_BS.end_while_sem.
    apply match_sem_while_global
      with (e2 := e2) (p_e := p_e)
           (ids:= n0) (stages_e := stages_e0) (n := n)in Hr;
      try by [].
  Qed.

  (** *** Set semantics *)

  Lemma match_sem_set:
    forall idb params stages lidb lstages t e2 e1',
      {|
        FP_ENV.get_state := {|
          FP_ENV.nav_block := idb;
          FP_ENV.nav_stages := (FP.SET params :: stages)%SEQ;
          FP_ENV.last_block := lidb;
          FP_ENV.last_stages := lstages;
        |};
        FP_ENV.get_trace_env := t
      |} ~( fpe )~ e2
      -> IH_match_run_step fp fpe stages
      -> FP_BS.set_sem (FP_ENV.mk_fp_env idb stages lidb lstages t)
            (continue fp idb stages lidb lstages) params = e1'
      -> exists e2',
          FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  idb params stages lidb lstages t e2 e1'' He IH Hr;
      destruct_match_env He.

    (** Get the extended stage*)
    inversion Hs as [stages' stages_e n Heq Hstages Hdrop].
    inversion Heq as [H|s s_e stages'' stages_e' Heqs Heq' Hs' Hse|H|H|H].
    inversion Heqs. rewrite -Hse -H0 in Hdrop.
    have Hdrop' := Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage Hdrop
            /FPE_BS.set_sem /FPE_BS.continue.

    (** Unfold the stage *)
    rewrite /FP_BS.set_sem /continue in Hr.

    (** Use the recursive hypothesis *)
    rewrite /IH_match_run_step in IH.
    remember (FP_BS.run_step_rec fp idb stages lidb lstages _) as e1'.
    have Hrec' := Heqe1'; symmetry in Heqe1'.
    remember [:: FPE_ENV.get_c_set params; init_stage] as t'.
    have He' := match_app_trace t' He.
    remember (FPE_ENV.next_stage _) as e2'.

    have Htest := match_next_stage_continue He' IH Heqe2' Hdrop' Heq'.
    rewrite //= in Htest. rewrite //= in Heqe1'. subst t'.
    apply Htest in Heqe1'. 

    destruct Heqe1' as [e2'' [Hr2' Heq2']].
    eexists; split; try by []. by rewrite Hr2' -Hr.
  Qed.

  (** *** Call semantics *)

  (** Preservation of the matching relation for a next stage after a Call
      stages that can continue or break.*)
  Lemma match_next_stage_call_break_or_continue:
    forall idb params stages lidb
            lstages t stages_e e2 e1' ids n b t',
        {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages := (FP.CALL params :: stages)%SEQ;
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
        |} ~( fpe )~ e2
      -> IH_match_run_step fp fpe stages
      -> ((FP_E.CALL ids params :: stages_e) ++ [:: FP_E.DEFAULT n])%SEQ 
          = FPE_ENV.get_current_stages (` fpe) e2
      -> MATCH_FP_FPE.match_stages stages stages_e
      ->(if b then
            FP_ENV.Def.app_trace
            (FP_ENV.mk_fp_env idb stages lidb lstages t) t'
         else
          continue fp idb stages lidb lstages
          (FP_ENV.Def.app_trace
              (FP_ENV.mk_fp_env idb stages lidb lstages t) t')) = e1'
      -> exists e2',
        continue_extended fpe
            (if b then
                FPE_BS.break
                (FPE_ENV.Def.app_trace (FPE_ENV.next_stage e2) t')
              else
                FPE_BS.continue
                (FPE_ENV.Def.app_trace (FPE_ENV.next_stage e2) t')
            ) = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  idb params stages lidb
      lstages t stages_e e2 e1' ids n b t' He IH Hc Heq Hr; 
    destruct_match_env He.

    (*** Rebuild e1 *)
    remember (FP_ENV.mk_fp_env idb 
              (FP.CALL params :: stages)
                  lidb lstages t) as e1;
    have He1 : e1 ~( fpe )~ e2.  { rewrite Heqe1 //=. }

    destruct b.

    (*** Break *)
    rewrite /continue_extended.
    remember (FPE_ENV.next_stage
            (FPE_ENV.Def.app_trace e2 t')) as e2'; exists e2';
      symmetry in Heqe2'. rewrite /FP_ENV.Def.app_trace //= in Hr.
      rewrite (app_trace_get_current_stages _ _ t') in Hc.

    destruct
      (match_next_stage_break (match_app_trace t' He) Hc Hr Heqe2' Heq)
      as [Hres Heq']. by split.

    (*** Continue *)

    (*** Use the recursive hypothesis *)
    rewrite /IH_match_run_step in IH.
    remember (FP_BS.run_step_rec fp idb stages lidb lstages (t ++ t'))
      as e1'' eqn:Hrec; symmetry in Hrec.
    have Hrec' := Hrec.
    remember (FPE_ENV.next_stage (FPE_ENV.Def.app_trace e2 t'))
      as e2'.

    have He' := match_app_trace t' He;
      rewrite /FP_ENV.Def.app_trace //= in He'.

    apply (match_next_stage_continue He' IH Heqe2' Hc Heq) in Hrec.

    destruct Hrec as [e2'' [Hr2' Heq2']].
    eexists; split; try by [].
    by rewrite /continue_extended /FPE_BS.continue -Hr /continue //=
              -FPE_ENV.next_stage_app_trace Hrec' -Heqe2' Hr2'. 
  Qed.

  (** Main lemma for the verification of the preservation of the semantics
      for the call stage*)
  Lemma match_sem_call:
    forall idb params stages lidb lstages t e2 e1',
      {|
        FP_ENV.get_state := {|
          FP_ENV.nav_block := idb;
          FP_ENV.nav_stages := (FP.CALL params :: stages)%SEQ;
          FP_ENV.last_block := lidb;
          FP_ENV.last_stages := lstages;
        |};
        FP_ENV.get_trace_env := t
      |} ~( fpe )~ e2
      -> IH_match_run_step fp fpe stages
      -> FP_BS.call_sem 
            (FP_ENV.mk_fp_env idb (FP.CALL params :: stages) lidb lstages t)
            (FP_ENV.mk_fp_env idb stages lidb lstages t)
            (continue fp idb stages lidb lstages) params
                      = e1'
      -> exists e2',
          FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  idb params stages lidb lstages t e2 e1' He IH Hr;
    destruct_match_env He.

    (*** Rebuild e1 *)
    remember (FP_ENV.mk_fp_env idb 
              (FP.CALL params :: stages)
                  lidb lstages t) as e1;
    have He1 : e1 ~( fpe )~ e2. 
    { rewrite Heqe1 //=. }

    (*** Get the extended stage*)
    inversion Hs as [stages' stages_e n Heq Hstages Hdrop];
    inversion Heq as [H|s s_e stages'' stages_e' Heqs Heq' Hs' Hse|H|H|H];
    inversion Heqs.

    rewrite -Hse -H0 in Hdrop. have Hdrop' := Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage 
              Hdrop /FPE_BS.call_sem.
    rewrite /FP_BS.call_sem in Hr.

    destruct (get_call_loop params) eqn:Hloop; last first.

    (**  Loop not enable *)
    apply (match_next_stage_call_break_or_continue He IH Hdrop' Heq' Hr).

    (** Loop is enable *)

    (** Eval the condition *)
    destruct (FP_ENV.Def.evalc e1 (get_call_fun params))
      as [b e1'']eqn:Hevalc. rewrite Heqe1 in Hevalc.
    have Heval := match_evalc He Hevalc;
      destruct Heval as [e2'' [Heval He2'']].

    (** Modify Hdrop *)
    have Hevale2':= evalc_get_current_stages (` fpe) Heval.
    rewrite /FPE_ENV.get_current_stages in Hevale2';
      rewrite /MATCH_FP_FPE.fpe Hevale2' in Hdrop'.
    (** Modify Hr *)
    rewrite /FP_ENV.Def.evalc in Hevalc;
      injection Hevalc as Hb'' He1''.
    rewrite /FP_ENV.Def.change_trace //= in He1''.
    rewrite -He1'' /FP_ENV.Def.change_trace in Hr.

    rewrite Heval; destruct b eqn:Hb;
      rewrite (evalc_change_trace_next_stage Heval) //=; try by [];
      last first.

    (** The function hase returned true *)

    (** Modify He2'' *)
    rewrite -He1'' in He2''.
    apply (match_next_stage_call_break_or_continue 
                He2'' IH Hdrop' Heq' Hr).

    (** The function has returned false *)

    destruct (get_call_until params) as [cond'|]eqn:HuHrntil; last first.

    (** There is no until condition to evaluate *)
    exists e2''.  by rewrite -He1'' Hr in He2''.

    (** There is a until condition to evaluate *)

    (** Eval the condition *)
    destruct (FP_ENV.Def.evalc e1'' cond') as [b' e1'''] eqn:Hevalc.
    rewrite -He1'' in He2''; rewrite -He1'' in Hevalc.
    have Heval' := match_evalc He2'' Hevalc;
      destruct Heval' as [e2''' [Heval' He2''']].
    rewrite Hevalc //= in Hr.

    have Heval'' := Heval'. injection Heval'' as Hb' Heqe2'''.
    rewrite Hb'; clear Hb' ; destruct b' eqn:Hb'; last first.

    (** Until condition has returned false *)
    eexists. rewrite //= in Hr. subst e1'.
    injection Heval' as Hb''' H'.
    split; try by []. by rewrite //= -Hb''' Heqe2'''.

    (** Until condition has returned true *)
    have Hget:  FPE_ENV.Def.get_trace e2'' ++ [:: COND cond' true]
              = FPE_ENV.Def.get_trace e2'''.
    { injection Heval' as Htrue H'. by rewrite -H' Htrue. }
    rewrite Hget (evalc_change_trace_next_stage Heval').
    remember (FPE_ENV.next_stage e2''') as e2'4; 
      symmetry in Heqe2'4; eexists.

    (** Modify He2''' *)
    apply (match_app_trace [:: init_stage]) in He2'''.

    (** Modify Hdrop' *)
    have Hdrop'' := evalc_get_current_stages (` fpe) Heval';
      rewrite /FPE_ENV.get_current_stages -Hdrop' in Hdrop''.

    injection Hevalc as Htrue He1'''; subst e1'''.

    apply (match_next_stage_break He2''' Hdrop'' Hr).
    - by rewrite FPE_ENV.next_stage_app_trace Heqe2'4.
    - by [].
  Qed.

  (** *** Deroute semantics *)

  Lemma match_sem_deroute:
    forall idb params stages lidb lstages t e2 e1',
      {|
        FP_ENV.get_state := {|
          FP_ENV.nav_block := idb;
          FP_ENV.nav_stages :=
            (FP.DEROUTE params :: stages)%SEQ;
          FP_ENV.last_block := lidb;
          FP_ENV.last_stages := lstages;
        |};
        FP_ENV.get_trace_env := t
      |} ~( fpe )~ e2
      -> FP_BS.deroute_sem fp
            (FP_ENV.mk_fp_env idb stages lidb lstages t)
            (get_deroute_block_id params) = e1'
      -> exists e2',
          FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  idb params stages lidb lstages t e2 e1' He Hr;
    destruct_match_env He.

    (** Get the extended stage*)
    destruct params as [bname id] eqn:Hparams.
    inversion Hs as [stages' stages_e n Heq Hstages Hdrop].
    inversion Heq as [H |s s_e stages'' stages_e' Heqs Heq' Hs' Hse|H|H|H].
    inversion Heqs. rewrite -Hse -H0 in Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage
              Hdrop /FPE_BS.deroute_sem
             /FPE_BS.break.

    (** Rebuild e1 and He *)
    remember (FP_ENV.mk_fp_env idb 
                ((FP.DEROUTE params :: stages)) lidb lstages t) as e1;
    have He1 : e1 ~( fpe )~ e2. 
    { rewrite Heqe1 Hparams //=. }

    (** Destruct e2 *)
    destruct e2 as [[idb2 ids2 lidb2 lids2] t2] eqn:He2.
    rewrite /FPE_ENV.next_stage /FPE_ENV.update_nav_stage //=.
    remember (FPE_ENV.mk_fp_env idb2 (ids2 + 1) lidb2 lids2 t2) as e2''.
    remember (FP_ENV.mk_fp_env idb stages lidb lstages t) as e1''.
    have He1'' : e1'' ~( fpe )~ e2'' . 
    { rewrite Heqe2'' Heqe1'' //=.
        repeat split; try by []. rewrite //= in Hs.
        by apply (match_env_stages_next_stages_deroute Hs). }


    (** Unfold the stage *)
    rewrite /FP_BS.deroute_sem //= in Hr.
    remember (FP_BS.Common_Sem.goto_block _ _ _) as e1''' eqn:Hg.
    have Hg' := match_goto_block (match_app_trace _ He1'') Hg.
    destruct Hg' as [e2''' [Hg' He2''']]; exists e2'''.
    subst e1' e2''.
    by rewrite /FPE_BS.fp Hg'.
  Qed.

  (** *** Return semantics *)

  Lemma match_sem_return:
    forall idb reset stages lidb lstages t e2 e1',
        {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages :=
              (FP.RETURN reset :: stages)%SEQ;
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
        |} ~( fpe )~ e2
      -> FP_BS.return_sem fp
            (FP_ENV.mk_fp_env idb stages lidb lstages t)
            reset = e1'
      -> exists e2',
          FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  idb reset stages lidb lstages t e2 e1' He Hr;
      destruct_match_env He.

    (** Get the extended stage*)
    inversion Hs as [stages' stages_e n Heq Hstages Hdrop].
    inversion Heq as [H |s s_e stages'' stages_e' Heqs Heq' Hs' Hse|H|H|H].
    inversion Heqs; subst s_e p. rewrite -Hse in Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage Hdrop
            /FPE_BS.return_sem /FPE_ENV.return_block.

    (** Destruct e2 *)
    destruct e2 as [[idb2 ids2 lidb2 lids2] t2] eqn:He2.

    (** Unfold the stage *)
    rewrite /FP_BS.return_sem in Hr; destruct reset; rewrite //=;
    rewrite //= in Hr.

    (** Reset set to true *)
    remember (FPE_ENV.mk_fp_env lidb2 0 lidb2 lids2 t2) as e2'.
    eexists; split; try by [].
    subst e1' e2'.
    simpl in *. rewrite Hidb Hlidb extend_fp_nwf_proj.
    rewrite (eq_on_enter _ Hfp) (eq_on_exit _ Hfp).

    apply match_app_trace.
    rewrite /FP_ENV.return_block.
    split; try by [].
    apply (match_env_stages_init lidb2 Hfp).

    (** Reset set to false *)
    remember (FPE_ENV.mk_fp_env lidb2 lids2 lidb2 lids2 t2) as e2'.
    eexists; split; try by [].
    subst e2'. rewrite -Hr Hlidb Ht //=.
    rewrite Hidb extend_fp_nwf_proj.
    rewrite (eq_on_enter _ Hfp) (eq_on_exit _ Hfp).
    by split.
  Qed.

  (** *** Nav semantics *)

  Lemma match_nav_code_sem:
    forall idb nav_mode until stages lidb lstages t e2
           e1' ids stages_e n,
        {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages :=
              (FP.NAV nav_mode until false :: stages)%SEQ;
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
        |} ~( fpe )~ e2
      -> FP_BS.nav_code_sem
           (FP_ENV.mk_fp_env idb 
              (FP.NAV nav_mode until false :: stages)%SEQ lidb lstages t)
            (FP_ENV.mk_fp_env idb stages lidb lstages t)
            nav_mode until = e1'
      -> ((FP_E.NAV ids nav_mode until :: stages_e) 
            ++ [:: FP_E.DEFAULT n])%SEQ 
              = FPE_ENV.get_current_stages (` fpe) e2
      -> MATCH_FP_FPE.match_stages stages stages_e
      -> exists e2',
          (continue_extended fpe 
            (FPE_BS.nav_code_sem e2 nav_mode until))
            = e2' /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /continue_extended
            /FP_BS.nav_code_sem
            /FPE_BS.nav_code_sem
    =>  idb nav_mode until stages lidb lstages t e2
       e1' ids stages_e n He Hr Hc Heq.

    (** Rebuild e1 *)
    remember (FP_ENV.mk_fp_env idb 
              (FP.NAV nav_mode until false :: stages)
                  lidb lstages t) as e1;
    have He1 : e1 ~( fpe )~ e2. 
    { rewrite Heqe1 //=. }

    destruct until as [cond|] eqn:Huntil.

    (** There is an until condition*)

    (** Evalutation of the condition *)
    destruct (FP_ENV.Def.evalc _ cond) as [b' e1''] eqn:Hevalc.
    rewrite Heqe1 in Hevalc.
    have Heval := match_evalc (match_app_trace _ He) Hevalc;
      destruct Heval as [e2'' [Heval He2'']].

    (** get e1'' *)
    rewrite /FP_ENV.Def.evalc in Hevalc.
    remember (EVAL_Def.eval _ cond) as b'';
    injection Hevalc as Hb'' He1''.
      rewrite /FP_ENV.Def.change_trace //= in He1''.

    remember [:: post_call_nav_sem nav_mode; init_stage] as post.
    remember (gen_fp_nav_code_sem nav_mode) as code.

    rewrite Heval; destruct b' eqn:Hb';
      rewrite //= in Hr.

    (** Condition has returned true *)
    rewrite -He1'' //= in He2''.
    remember (FPE_ENV.next_stage e2'') as e2'; eexists;
      symmetry in Heqe2'. apply (match_app_trace post) in He2''.


    (** Update get_current_stages *)
    rewrite (app_trace_get_current_stages _ _ code)
      (evalc_get_current_stages (` fpe) Heval)
      (app_trace_get_current_stages _ _ post) in Hc.

    (** Build e1' *)
    rewrite -He1'' /FP_ENV.Def.app_trace //= -Heqpost in Hr.

    rewrite //= -Heqpost.

    apply (match_next_stage_break He2'' Hc Hr).
    - by rewrite FPE_ENV.next_stage_app_trace.
    - by [].

    (** Condition has return false *)
    eexists. split; try by [].
    
    rewrite -Hr //=. by apply match_app_trace.

    (** There is no until condition *)
    eexists. split; try by [].
    
    rewrite //= -Hr. by repeat apply match_app_trace.
  Qed.

  Lemma match_sem_nav:
    forall idb nav_mode until init stages lidb lstages t e2 e1',
        {|
          FP_ENV.get_state := {|
            FP_ENV.nav_block := idb;
            FP_ENV.nav_stages :=
              (FP.NAV nav_mode until init :: stages)%SEQ;
            FP_ENV.last_block := lidb;
            FP_ENV.last_stages := lstages;
          |};
          FP_ENV.get_trace_env := t
        |} ~( fpe )~ e2
      -> IH_match_run_step fp fpe stages
      -> FP_BS.nav_sem
          (FP_ENV.mk_fp_env idb
            (FP.NAV nav_mode until init :: stages)%SEQ lidb lstages t)
          (FP_ENV.mk_fp_env idb stages lidb lstages t)
            nav_mode until init
        = e1'
      -> exists e2',
          FPE_BS.run_step fpe e2 = e2' /\ e1' ~( fpe )~ e2'.
  Proof. 
    move =>  idb nav_mode until init stages lidb lstages
            t e2 e1' He IH Hr;
    destruct_match_env He.

    (** Get the extended stage*)
    inversion Hs as [stages' stages_e n Heq Hstages Hdrop];
    inversion Heq as [H|s s_e stages'' stages_e' Heqs Heq' Hs' Hse|H|H|H];
    last first.

    (** Nav Init *)
    rewrite -H5 in Hdrop. have Hdrop' := Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage 
              Hdrop /FPE_BS.nav_init_sem.
    rewrite /FP_BS.nav_sem -H3 /FP_BS.nav_init_sem 
            /FP_ENV.update_nav_stage //= in Hr.

    remember [:: init_code_nav_sem nav_mode; init_stage] as code.

    remember (FPE_ENV.next_stage e2) as e2'; symmetry in Heqe2'.
    remember (FP_ENV.mk_fp_env idb
                    (FP.NAV nav_mode until false :: stages)%SEQ
                      lidb lstages t) as e1''.
    symmetry in Heqe1''.
    destruct (match_next_stage_break He Hdrop' Heqe1'' Heqe2')
      as [Hc He'].
    apply MATCH_FP_FPE.match_cons;
      try by []; apply MATCH_FP_FPE.match_nav.

    exists (FPE_ENV.Def.app_trace e2' code).
    have He1': e1' = FP_ENV.Def.app_trace e1'' code
      by rewrite -Hr -Heqe1''.
    rewrite //= He1'; split; try by [].
    by apply match_app_trace.

    (** Nav function *)
    inversion Heqs; subst p u init s_e.
    rewrite -Hse in Hdrop. have Hdrop' := Hdrop.
    apply FPE_ENV.drop_get_current_stage in Hdrop.
    rewrite FPE_BS.run_step_equation /FPE_BS.run_stage 
              Hdrop /FPE_BS.nav_sem.
    destruct (nav_cond_sem nav_mode) as [cond|] eqn:Hcond;
    rewrite /FP_BS.nav_sem Hcond in Hr.

    (** There is a condition to evaluate *)

    (** Rebuild e1 *)
    remember (FP_ENV.mk_fp_env idb 
              (FP.NAV nav_mode until false :: stages)
                  lidb lstages t) as e1;
    have He1 : e1 ~( fpe )~ e2. 
    { rewrite Heqe1 //=. }
    remember (pre_call_nav_sem _) as pre.

    (** Eval the condition *)
    destruct (FP_ENV.Def.evalc _ cond) as [b' e1''] eqn:Hevalc.
    rewrite Heqe1 in Hevalc.
    have Heval := match_evalc (match_app_trace _ He) Hevalc;
      destruct Heval as [e2'' [Heval He2'']].

    (** Modify Hdrop *)
    have Hevale2':= evalc_get_current_stages (` fpe) Heval.
    rewrite /FPE_ENV.get_current_stages in Hevale2';
      rewrite /MATCH_FP_FPE.fpe Hevale2' in Hdrop'.
    (** Modify Hr *)
    rewrite /FP_ENV.Def.evalc in Hevalc.
      remember (EVAL_Def.eval _ cond) as b'';
      injection Hevalc as Hb'' He1''.
    rewrite /FP_ENV.Def.change_trace //= in He1''.
    rewrite -He1'' /FP_ENV.Def.change_trace //= in Hr.

    rewrite Heval; destruct b' eqn:Hb'; try by [].

    (** Condition evaluate to true *)

    (** Modify He2''*)
    rewrite -He1'' /FP_ENV.Def.app_trace //= in He2''.

    remember (FPE_ENV.next_stage e2'') as e2';
    remember [:: post_call_nav_sem nav_mode;
                       FP_ENV.last_wp_exec nav_mode;
                       init_stage] as post.
    remember [:: post_call_nav_sem nav_mode;
                    FPE_ENV.last_wp_exec nav_mode;
                    init_stage] as post'.
    symmetry in Heqe2'.
    remember (FP_ENV.mk_fp_env idb
        stages lidb lstages ((t ++ [:: pre]) ++ [:: COND cond b'']))
        as e1'''; symmetry in Heqe1'''.
    destruct (match_next_stage_break He2''
                  Hdrop' Heqe1''' Heqe2' Heq') as [Hc He'].

    rewrite //= FPE_ENV.next_stage_app_trace Heqe2'.
    eexists; split; try by [].
    subst e1' e1''' post post'. by apply match_app_trace.

    (** Condition evaluate to false *)

    (** Modify He2'' *)
    rewrite -He1'' in He2''.
    remember (FP_BS.nav_code_sem _ _ _ _) as e' eqn:Hr';
      symmetry in Hr'.
    destruct (match_nav_code_sem He2'' Hr' Hdrop')
      as [e2''' [Hcode He2''']]; try by [].
    destruct (FPE_BS.nav_code_sem e2'' _ _) as [b e_tmp];
      rewrite //= in Hcode.
    destruct (FPE_BS.run_step fpe e_tmp) as [b_tmp' e_tmp'];
      exists e2'''; destruct b; subst e2''' e' e1';
      split; try by [].

    (** No condition to evalute *)
    remember (FP_BS.nav_code_sem _ _ _ _) as e' eqn:Hr'; symmetry in Hr'.
    destruct (match_nav_code_sem (match_app_trace _ He) Hr' Hdrop')
      as [e2' [Hcode He2']]; try by [].
    destruct (FPE_BS.nav_code_sem _ _ _) as [b e_tmp];
      rewrite //= in Hcode.
    destruct (FPE_BS.run_step fpe e_tmp) as [o_tmp' e_tmp'];
      exists e2'; destruct b; inversion Hcode; inversion Hr;
      subst e' e1' e2'; split; try by [].
  Qed.

  (** * Main Lemmas/Theorems for the semantics preservation *)

  (** Matching relation for the run_step_rec function *)
  Lemma match_sem_run_step_rec:
    forall stages idb lidb lstages t e1' e2,
    {|
      FP_ENV.get_state := {|
        FP_ENV.nav_block := idb;
        FP_ENV.nav_stages := stages;
        FP_ENV.last_block := lidb;
        FP_ENV.last_stages := lstages;
      |};
      FP_ENV.get_trace_env := t
    |} ~( fpe )~ e2
    -> FP_BS.run_step_rec fp idb stages lidb lstages t = e1'
    -> exists e2',
        FPE_BS.run_step fpe e2 = e2'
         /\ e1' ~( fpe )~ e2'.
  Proof.
    induction stages as [|s stages IHs];
      move => idb lidb lstages t e1' e2 He Hr.

    (* Empty list *)
    apply (match_sem_run_nil He Hr).

    (* Execute a stage *)
    destruct s eqn:Hs; rewrite //= in Hr.

    (* While *)
    apply (match_sem_while He IHs Hr).

    (* Set *)
    apply (match_sem_set He IHs Hr).

    (* Call *)
    apply (match_sem_call He IHs Hr).

    (* Deroute *)
    destruct params as [bname id] eqn:Hparams. rewrite //= in Hr.
    replace id with (get_deroute_block_id params) in Hr; last first.
    by rewrite Hparams.
    rewrite -Hparams in He. apply (match_sem_deroute He Hr).

    (* Return *)
    apply (match_sem_return He Hr).

    (* Nav mode *)
    apply (match_sem_nav He IHs Hr).
  Qed.

  (** Matching relation for the run_step function *)
  Lemma match_sem_run_step:
    forall e1 e1' e2,
    e1 ~( fpe )~ e2
    -> FP_BS.run_step fp e1 = e1'
    -> exists e2',
          FPE_BS.run_step fpe e2 = e2'
          /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FP_BS.run_step.
    move  => [[idb stages lidb lstages] t] //= e1' e2.
    by apply match_sem_run_step_rec.
  Qed.

  (** ** Proof of the semantics preservation for FP to FPE *)
  Theorem semantics_preservation:
    forall e1 e1',
    FP_BS.step fp e1 = e1'
    -> forall e2, e1 ~( fpe )~ e2
    -> exists e2', FPE_BS.step fpe e2 = e2'
                  /\ e1' ~( fpe )~ e2'.
  Proof.
    rewrite /FP_BS.step /FPE_BS.step =>  e1 e1' Hs e2 He.

    (** Manage exception *)
    destruct (FP_BS.Common_Sem.exception fp e1) as [b e'] eqn:Hde.
    have Hexpt := match_sem_exceptions He Hde.
    destruct Hexpt as [e2' [Hexpt' He']]; destruct b eqn:Hb.

    (** Exception raised *)
    exists e2'; split.
      by rewrite Hexpt'.
      by rewrite -Hs.

    (** Exception not raised *)
    (** Manage run_step *)
    destruct (FP_BS.run_step fp e') as [o'' e''] eqn:Hdr.
    have Hrun := match_sem_run_step He' Hdr.
    destruct Hrun as [e2'' [Hrun' He'']].

    eexists; rewrite Hexpt' Hrun'; split. by [].
    rewrite -Hs (eq_get_code_block_post_call Hfp He') //=.
    by apply match_app_trace.
  Qed.

  (** ** Proof of the semantics preservation for FPE to FP *)
  Theorem semantics_preservation_inv:
    forall e2 e2',
    FPE_BS.step fpe e2 = e2'
    -> forall e1, e1 ~( fpe )~ e2
    -> exists e1', FP_BS.step fp e1 = e1'
                /\ e1' ~( fpe )~ e2'.
  Proof.
    move =>  e2 e2' Hs2 e1 He.

    remember (FP_BS.step fp e1) as e1' eqn:Hs1;
      symmetry in Hs1.

    have Hs2' := semantics_preservation Hs1 He.
    destruct Hs2' as [e2'' [Hs2' He']].
    rewrite Hs2 in Hs2'.

    exists e1'. by rewrite Hs2'.
  Qed.

  (** ** Proof of the semantics preservation for the inital case *)
  Theorem semantics_preservation_init:
      (FP_ENV.Def.init_env fp) 
            ~( fpe )~ (FPE_ENV.Def.init_env (` fpe)).
  Proof.
    rewrite /FP_ENV.Def.init_env
            /FPE_ENV.Def.init_env
    => //=.
    repeat (split; try by []; try by apply match_env_stages_init).
  Qed.

  (** ** Bisimulation *)

  Theorem bisim_transf_extended_correct:
    bisimulation (FP_BS.semantics_fp fp) (FPE_BS.semantics_fpe fpe).
  Proof.
    apply Bisimulation with (match_env fpe); do 2 split.
    - rewrite /initial_env /= => e1 H.
      exists (FPE_ENV.Def.init_env (` fpe)); split; try by [].
      rewrite -H. by apply semantics_preservation_init.
    - by apply semantics_preservation.
    - rewrite /initial_env /= => e1 H.
      exists (FP_ENV.Def.init_env fp); split; try by [].
      rewrite -H. by apply semantics_preservation_init.
    - by apply semantics_preservation_inv.
  Qed.

  End FLIGHT_PLAN.

End FP_TO_FPE_VERIF.
