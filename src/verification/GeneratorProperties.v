From VFP Require Import BasicTypes
                        FlightPlanGeneric FlightPlan FlightPlanExtended
                        ClightGeneration CommonFPDefinition
                        AuxFunctions TmpVariables FBDerouteAnalysis
                        FPExtended CommonLemmas FPSizeVerification
                        CommonSSRLemmas CommonLemmasNat8
                        CommonFPSimplified Generator
                        CommonStringLemmas FlightPlanSized
                        GvarsVerification.
From GEN Require Import CommonFP.

From compcert Require Import Coqlib Integers Floats AST Ctypes
                             Cop Clight Clightdefs Maps.
Import Clightdefs.ClightNotations.

From Coq Require Import  FunInd Program Program.Equality
                                 Program.Wf.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope nat_scope.
Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope clight_scope.

Import FP FP_E Generator.

(** * Functions and lemmas required for the verification of the generator*)

(** Definition to build the whole program:                            *)
(**   CommonFPSimplified ++ Gvars ++ Generated functions              *)
(** Gvars corresponds to the list of variables that can be defined in *)
(** the flight plan                                                   *)
Definition generate_complete_context (fp_e: FP_E_WF.flight_plan)
                                              (gvars: cgvars):
                                                    Clight.program :=
  let composites := CommonFPSimplified.composites ++ composites in
  let gdefs := CommonFPSimplified.global_definitions
                  ++ (` gvars)
                  ++ (global_definitions fp_e) in
  let pid := CommonFPSimplified.public_idents ++ public_idents in
  mkprogram composites gdefs pid _auto_nav Logic.I.

(** ** Lemmas to access variables and functions from CommonFP *)
(** No repetition of program in the ge. *)

Lemma prog_generated_norepet:
  forall fpe gvars,
    let prog := generate_complete_context fpe gvars in
    list_norepet (prog_defs_names prog).
Proof.
  move => fpe [gvars Hg] prog.
  rewrite /prog_defs_names /CommonFP.prog
            list_append_map.

  rewrite list_append_map. 
  apply list_norepet_append_triple.

  by apply (no_repet Hg).
Qed.

(** Tactic to simplify In predicate over list *)
Ltac find_In := simpl; repeat (try (left; by auto); right).

(** All lemmas to access elements in the environment *)
Lemma get_private_nav_block:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._private_nav_block
      = Some (Gvar CommonFP.v_private_nav_block).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_nav_block_var:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._nav_block
      = Some (Gvar CommonFP.v_nav_block).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_private_nav_stage:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._private_nav_stage
      = Some (Gvar CommonFP.v_private_nav_stage).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_nav_stage_var:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._nav_stage
      = Some (Gvar CommonFP.v_nav_stage).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_private_last_block:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._private_last_block
      = Some (Gvar CommonFP.v_private_last_block).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_last_block_var:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._last_block
      = Some (Gvar CommonFP.v_last_block).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_private_last_stage:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._private_last_stage
      = Some (Gvar CommonFP.v_private_last_stage).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_last_stage_var:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._last_stage
      = Some (Gvar CommonFP.v_last_stage).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_nav_block_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._get_nav_block
      = Some (Gfun(Internal CommonFP.f_get_nav_block)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma get_nav_stage_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._get_nav_stage
      = Some (Gfun(Internal CommonFP.f_get_nav_stage)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma set_nav_block_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._set_nav_block
      = Some (Gfun(Internal CommonFP.f_set_nav_block)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma next_stage_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._NextStage
      = Some (Gfun(Internal CommonFP.f_NextStage)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma set_nav_stage_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._set_nav_stage
      = Some (Gfun(Internal CommonFP.f_set_nav_stage)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma goto_block_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._nav_goto_block
      = Some (Gfun(Internal CommonFP.f_nav_goto_block)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma next_block_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._NextBlock
      = Some (Gfun(Internal CommonFP.f_NextBlock)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma return_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._Return
      = Some (Gfun(Internal CommonFP.f_Return)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma on_enter_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!_on_enter_block
      = Some (Gfun(Internal (gen_fp_on_enter_block fpe))).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - find_In. apply no_on_enter_in_cgvars. by find_In.
Qed.

Lemma on_exit_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!_on_exit_block
      = Some (Gfun(Internal (gen_fp_on_exit_block fpe))).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - find_In. apply no_on_exit_in_cgvars. by find_In.
Qed.

Lemma nav_init_block_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._nav_init_block
      = Some (Gfun(Internal CommonFP.f_nav_init_block)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.

Lemma forbidden_deroute_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!_forbidden_deroute
      = Some (Gfun(Internal (gen_fp_forbidden_deroute fpe))).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - find_In. apply no_forbidden_deroute_in_cgvars. by find_In.
Qed.

Lemma get_nb_blocks:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!CommonFP._nb_blocks
      = Some (Gvar (gvar_nb_blocks fpe)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - find_In. apply no_nb_blocks_in_cgvars. by find_In.
Qed.

Lemma init_Function_fun:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!_init_function
      = Some (Gfun(Internal CommonFP.f_init_function)).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - by find_In.
Qed.  

(** Find symbol lemmas *)

Lemma get_symb_private_nav_block:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    exists b,
      Globalenvs.Genv.find_symbol
        (globalenv prog) CommonFP._private_nav_block
          = Some b.
Proof.
  intros. have H := get_private_nav_block fpe gvars.
  apply Globalenvs.Genv.find_def_symbol in H.
  destruct H as [b [H H']]. by exists b.
Qed.

Lemma get_symb_private_nav_stage:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    exists b,
      Globalenvs.Genv.find_symbol
        (globalenv prog) CommonFP._private_nav_stage
          = Some b.
Proof.
  intros. have H := get_private_nav_stage fpe gvars.
  apply Globalenvs.Genv.find_def_symbol in H.
  destruct H as [b [H H']]. by exists b.
Qed.


Lemma get_symb_private_last_block:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    exists b,
      Globalenvs.Genv.find_symbol
        (globalenv prog) CommonFP._private_last_block
          = Some b.
Proof.
  intros. have H := get_private_last_block fpe gvars.
  apply Globalenvs.Genv.find_def_symbol in H.
  destruct H as [b [H H']]. by exists b.
Qed.

Lemma get_symb_private_last_stage:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    exists b,
      Globalenvs.Genv.find_symbol
        (globalenv prog) CommonFP._private_last_stage
          = Some b.
Proof.
  intros. have H := get_private_last_stage fpe gvars.
  apply Globalenvs.Genv.find_def_symbol in H.
  destruct H as [b [H H']]. by exists b.
Qed.

Lemma get_symb_nb_blocks:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    exists b,
      Globalenvs.Genv.find_symbol
        (globalenv prog) CommonFP._nb_blocks
          = Some b.
Proof.
  intros. have H := get_nb_blocks fpe gvars.
  apply Globalenvs.Genv.find_def_symbol in H.
  destruct H as [b [H H']]. by exists b.
Qed.

Ltac find_Symb := try apply get_symb_private_nav_block;
                  try apply get_symb_private_nav_stage;
                  try apply get_symb_private_last_block;
                  try apply get_symb_private_last_stage.

(** Find auto_nav function *)

Lemma get_auto_nav:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    (prog_defmap prog)!_auto_nav
      = Some (Gfun(Internal (gen_fp_auto_nav fpe))).
Proof.
  move => fpe gvars prog; apply prog_defmap_norepet.
  - by apply prog_generated_norepet.
  - find_In. apply no_auto_nav_in_cgvars. by find_In.
Qed.

Lemma get_symb_auto_nav:
  forall fpe gvars, let prog := generate_complete_context fpe gvars in
    exists b,
      Globalenvs.Genv.find_symbol
        (globalenv prog) _auto_nav = Some b
      /\ Globalenvs.Genv.find_def (globalenv prog) b 
          = Some (Gfun (Internal (gen_fp_auto_nav fpe))).
Proof.
  intros. have H := get_auto_nav fpe gvars.
  apply Globalenvs.Genv.find_def_symbol in H.
  destruct H as [b [H H']]. by exists b.
Qed.

(** Property about select_swicth *)
Lemma select_switch_ne:
  forall n n' s sl,
  n <> n'
  -> select_switch (Z.of_nat n) 
                      (LScons (Some (Z.of_nat n')) s sl)
      = select_switch (Z.of_nat n) sl.
Proof.
  move => n n' s sl Hne.
  rewrite /select_switch /select_switch_case zeq_false //=.
  lia.
  Qed.

(** Properties about test_from *)
Lemma test_from_eq:
  forall from fbd,
    test_from from fbd = true <-> (get_fbd_from fbd) = from.
Proof.
  move => from [from' to cond];rewrite /test_from //=.
  apply Nat.eqb_eq.
Qed.

Lemma test_from_refl:
  forall fbd,
    test_from (get_fbd_from fbd) fbd = true.
Proof. destruct fbd. by rewrite /test_from //= Nat.eqb_refl. Qed.

Lemma test_from_false:
  forall id fbd, id <> (get_fbd_from fbd) <-> test_from id fbd = false.
Proof.
  move => id fbd. rewrite /test_from //=; split; move => Hne.
  - apply Nat.eqb_neq. move => Heq. by rewrite Heq in Hne.
  - apply Nat.eqb_neq. by rewrite Nat.eqb_sym in Hne.
Qed.

(** Properites about forbidden deroutes *)
Lemma seq_of_labeled_statement_fbd:
  forall from fbds filtered fbds',
  partition (test_from from) fbds = (filtered, fbds')
  -> exists s, 
    (seq_of_labeled_statement
      (select_switch (Z.of_nat from)
        (gen_forbidden_deroute_case fbds (LScons None Sbreak LSnil))))
      = ((fold_right (fun x y => x @+@ y) Sbreak
          (map gen_fbd_test filtered)) @+@ s).
Proof.
  move => from. apply partition_ind.

  - rewrite gen_forbidden_deroute_case_equation //=.
    by exists Sskip.

  - move => e l l1 l2 Ht Hp.
    rewrite gen_forbidden_deroute_case_equation //=.
    have Heq:= (proj1 (test_from_eq _ _)) Ht; subst from.
    destruct (partition _ l) as [l1' l2'] eqn:Hp'.
    rewrite (partition_cons1 _ _ _ Hp' (test_from_refl e))
      in Hp; inversion Hp; subst l2 l1.
    rewrite Ht /select_switch //= zeq_true //=.
    eexists. reflexivity.
  - move => e Ht. have Hne:= (proj2 (test_from_false _ _)) Ht.
    exists (test_from (get_fbd_from e)); split.
    * split; move => e' H; apply test_from_eq in H.
      + subst from.
         apply test_from_false. move => H. by rewrite H in Hne.
      + apply test_from_false; move => H'; subst from.
         by rewrite H in Hne.
    * split. apply test_from_refl.
    * move => l l1 l2 l1' l2' Hp Hp' Hs.
      rewrite gen_forbidden_deroute_case_equation //=.
      destruct (partition (test_from (get_fbd_from e))  l)
        as [l1'' l2''] eqn:Hp''.
      rewrite (partition_cons1 _ _ _ Hp'' (test_from_refl e))
        in Hp'; inversion Hp'; subst l2' l1'.
      destruct Hs as [s Hs].
      rewrite test_from_refl select_switch_ne.
      exists s. apply Hs. by apply test_from_false.
Qed.

(** Lemmas to acces a block generated from a switch statement *)
Lemma reduce_blocks_wf:
  forall d blocks block default, 
  (forall id : nat,
    id < Datatypes.length (block :: blocks) ->
    FP_E.get_block_id (List.nth id (block :: blocks) default) = d + id)
  -> (forall id : nat,
        id < Datatypes.length blocks ->
          FP_E.get_block_id (List.nth id blocks default) = d.+1 + id).
Proof.
  move => d blocks block default H id Hlt.
  have Hlt' : id.+1 < Datatypes.length (block :: blocks).
  { to_nat Hlt. }
  apply H in Hlt'. rewrite //= in Hlt'.
  rewrite Hlt'. ssrlia.
Qed.

Lemma select_switch_block_drop_lt_len:
  forall id' id d default blocks,
    (forall id, id < length blocks
      -> FP_E.get_block_id (List.nth id blocks default) = d + id)
    -> id' <= id
    -> (Z.of_nat (d + id) <= Int.max_unsigned)%Z
    -> id < length blocks
    ->  (select_switch (Int.unsigned (int_of_nat (d + id)))
                        (gen_fp_blocks blocks default))
      = (select_switch (Int.unsigned (int_of_nat (d + id)))
                    (gen_fp_blocks (drop id' blocks) default)).
Proof.
  induction id' as [|id' IH] 
    => [|id d default blocks Hlt Hle Hmax Hlen].
  intros; by rewrite drop0.

  have id_gt_0: 0 < id. { by apply lt_impl_lt_0 with (m := id'). }
  to_nat id_gt_0.

  destruct blocks as [|block blocks]; try by [].
  replace (drop id'.+1 (block :: blocks)) with (drop id' blocks);
    try by [].

  replace (select_switch (Int.unsigned (int_of_nat (d + id)))
                (gen_fp_blocks (block :: blocks) default))
    with (select_switch (Int.unsigned (int_of_nat (d + id)))
                  (gen_fp_blocks blocks default)).

  have Hdid: d + id = d.+1 + id.-1 by ssrlia.
  rewrite Hdid. 

  apply IH with (id := id.-1) (d := d.+1) (default := default).
  apply reduce_blocks_wf with (block := block).

  to_nat Hle; apply ltnSE; ssrlia.
  to_nat Hle. ssrlia.
  by rewrite -Hdid.
  rewrite //= in Hlen; to_nat Hlen; ssrlia.
  ssrlia.

  rewrite //= /select_switch /select_switch_case zeq_false.
  by []. 

  have Hle': 0 < Datatypes.length (block :: blocks). by [].
  apply Hlt in Hle'. rewrite //= in Hle'.
  rewrite Hle' /int_of_nat.
  rewrite Int.unsigned_repr; try ssrlia. 
  split; ssrlia.
Qed.

Lemma get_correct_block_id:
  forall fpe,
  verified_fp_e fpe
  -> forall id,
    id < FP_E.get_nb_blocks fpe
    ->  get_block_id
          (List.nth id (get_fp_blocks fpe) (get_default_block fpe)) = id.
Proof.
  move => fpe Hsize id Hlt.
  rewrite (get_well_numbered_blocks Hsize); ssrlia.
    rewrite /FP_E_WF.get_nb_blocks /=. to_nat Hlt.
    by apply Hlt.
Qed.

Lemma select_switch_fp_block:
  forall fpe id,
    let blocks := FP_E.get_fp_blocks fpe in
    let default := FP_E.get_default_block fpe in
    let b := FP_E.get_block fpe id in
    id < length blocks
    -> verified_fp_e fpe
    -> (select_switch (Int.unsigned (int_of_nat id))
                          (gen_fp_blocks blocks default))
        = LScons (Some (Z.of_nat  id))
                      (gen_fp_block b)
                      (gen_fp_blocks (drop (id.+1) blocks) default).
Proof.
  move => fpe id blocks default b Hlt Hsize.
  rewrite -(plus0n id). rewrite /blocks in Hlt.

  have Hmax: (Z.of_nat id <= Int.max_unsigned)%Z.
  { have H := get_nb_blocks8 Hsize.
    rewrite /nb_blocks_lt_256 /is_nat8 //= in H.
    to_nat Hlt. rewrite /Int.max_unsigned //=.
    rewrite /FP_E_WF.get_nb_blocks in H. ssrlia. }

  erewrite select_switch_block_drop_lt_len
    with (id' := id) (default := FP_E.get_default_block fpe); try by [].

  (* Manage drops *)
  - rewrite (plus0n id) 
      (len_drop (FP_E.get_default_block fpe) Hlt).
    replace (List.nth id blocks (FP_E.get_default_block fpe)) with b;
      last first. by rewrite /b .
    rewrite //= /select_switch /select_switch_case /b.
    to_nat Hlt. have H := (get_well_numbered_blocks Hsize).
    rewrite /well_numbered_blocks /FP_E_WF.get_fp_blocks 
            /FP_E_WF.get_nb_blocks //= in H.
    rewrite H. rewrite ZeqUInt; try by []. by rewrite zeq_true.
    ssrlia.

  (* Well-indexed *)
  - rewrite /blocks => id' Hlt'.
    apply (get_correct_block_id Hsize). rewrite /FP_E.get_nb_blocks.
    remember (Datatypes.length _) as nb. to_nat Hlt'. ssrlia.
Qed.

(** Lemmas to acces the default generated from a switch statement *)

Lemma select_switch_case_default_block_ge_len:
  forall blocks id d default,
    (forall id, id < length blocks
      -> FP_E.get_block_id (List.nth id blocks default) = d + id)
    -> (Z.of_nat (d + id) <= Int.max_unsigned)%Z
    -> length blocks <= id
    -> (select_switch_case (Int.unsigned (int_of_nat (d + id)))
                            (gen_fp_blocks blocks default))
      = None.
Proof.
  induction blocks as [| block blocks IHb]
    => [//|id d default Hnum Hlt Hge].

  have Hid: get_block_id block = d.
  { replace d with (d + 0) by ssrlia.
    rewrite -Hnum; by ssrlia. }

  have Hdid: d + id = d.+1 + id.-1.
  { to_nat Hge. ssrlia. }

  rewrite /= Hid zeq_false Hdid.

  apply IHb with (id := id.-1) (d := d.+1) (default := default).
  - apply reduce_blocks_wf with (block := block). apply Hnum.
  - by rewrite -Hdid.
  - to_nat Hge. ssrlia.
  - rewrite /int_of_nat -Hdid Int.unsigned_repr.
    * to_nat Hge. ssrlia.
    * split; try by ssrlia. 
Qed.

Lemma select_switch_default_default_block_ge_len:
  forall blocks default,
    (select_switch_default (gen_fp_blocks blocks default))
      = LScons None (gen_fp_block default) LSnil.
Proof.
  induction blocks as [|block blocks IHb]; move => default //=.
Qed.

Lemma select_switch_default_block:
  forall fpe id,
    let blocks := FP_E.get_fp_blocks fpe in
    let default := FP_E.get_default_block fpe in
    let b := FP_E.get_block fpe id in
    is_nat8 id
    -> length blocks <= id
    -> verified_fp_e fpe
    -> (select_switch (Int.unsigned (int_of_nat id))
                          (gen_fp_blocks blocks default))
        = LScons None (gen_fp_block default) LSnil.
Proof.
  move => fpe id blocks default b Hid8 Hlt Hsize.
  rewrite -(plus0n id) /select_switch. rewrite /blocks in Hlt.

  have Hmax: (Z.of_nat (0 + id) <= Int.max_unsigned)%Z.
  { apply max_unsigned_256 in Hid8. ssrlia. }

  have Hnum: forall id, id < Datatypes.length blocks
    ->  get_block_id (List.nth id blocks (get_default_block fpe)) = 0 + id.
  { rewrite /blocks => id' Hlt'.
    apply (get_correct_block_id Hsize).
    rewrite /FP_E.get_nb_blocks. to_nat Hlt'. ssrlia. }

  rewrite select_switch_case_default_block_ge_len; try by [].

  rewrite select_switch_default_default_block_ge_len; try by [].
Qed.

(** Lemmas to acces a stage generated from a switch statement *)
Lemma reduce_stages_wf:
  forall d stages stage default, 
  (forall id : nat,
    id < Datatypes.length (stage :: stages) ->
    FP_E.get_stage_id (List.nth id (stage :: stages) default) = d + id)
  -> (forall id : nat,
        id < Datatypes.length stages ->
          FP_E.get_stage_id (List.nth id stages default) = d.+1 + id).
Proof.
  move => d stages stage default H id Hlt.
  have Hlt' : id.+1 < Datatypes.length (stage :: stages).
  { to_nat Hlt. }
  apply H in Hlt'. rewrite //= in Hlt'.
  rewrite Hlt'. ssrlia.  
Qed.

Lemma select_switch_case_gen_cases:
  forall stage stages ids,
    let id := FP_E.get_stage_id stage in
    (forall id, stage <> DEFAULT id)
    -> exists stmt,
    select_switch_case ids (gen_fp_cases stage (gen_fp_stages stages))
     = (if (zeq (Z.of_nat id) ids)
       then Some stmt
       else select_switch_case ids (gen_fp_stages stages)).
Proof.
  move => stage stages ids id H.
  destruct stage; try (eexists; rewrite //=; by destruct (zeq _ _)).
  have H' := H id0. by [].
Qed.

Lemma select_switch_default_gen_cases:
  forall stage stages,
    (forall id, stage <> DEFAULT id)
    -> select_switch_default (gen_fp_cases stage (gen_fp_stages stages))
        = select_switch_default (gen_fp_stages stages).
Proof.
  move => stage stages Hd.
  destruct stage eqn:H; rewrite //= /gen_fp_cases.
  by have H' := Hd id.
Qed.

Lemma select_switch_default_ge_len:
  forall stage stages d,
    let id' := (d + (length stages)) in
    let default := DEFAULT id' in
       (forall id', stage <> DEFAULT id')
      -> select_switch_default
        (gen_fp_cases stage (gen_fp_stages (stages ++ [:: default])))
      = select_switch_default (gen_fp_stages (stages ++ [:: default])).
Proof.
  move => stage stages d id' default Hnd.
  destruct stage eqn:Hs; rewrite //=.
  by have H := Hnd id.
Qed.

Lemma select_switch_stage_drop_ge_len:
  forall stages id d,
    let id' := (d + (length stages)) in
    let default := DEFAULT id' in
    (forall id, id < length stages
      -> FP_E.get_stage_id (List.nth id stages default) = d + id)
    -> (forall id, id < length stages
      -> forall id', List.nth id stages default <> DEFAULT id')
    -> (Z.of_nat (d + id) <= Int.max_unsigned)%Z
    -> id >= length stages
    -> d + id >= (length stages)
    ->  (select_switch (Int.unsigned (int_of_nat (d + id)))
                        (gen_fp_stages (stages ++ [:: default])))
          = LScons None (gen_fp_stage default) LSnil.
Proof.
  induction stages as [|stage stages IHs];
    rewrite /select_switch //= => id d Hnum Hnd Hmax Hge0 Hlen.

  remember (DEFAULT _) as default.
  have Hd : forall id, stage <> DEFAULT id.
  { move => id'. have H := Hnd 0.
     rewrite //= in H. apply H. ssrlia. }
  destruct (select_switch_case_gen_cases (stages ++ [:: default]) 
                (Int.unsigned (int_of_nat (d + id))) Hd)  as [s'' Hs].
  rewrite Hs /proj_sumbool zeq_false //=; subst default; clear Hd.
  
  have Hdid: d + id = d.+1 + id.-1.
    {to_nat Hge0; to_nat Hlen; ssrlia. }
  rewrite /select_switch in IHs.
  have Hd: (d + (Datatypes.length stages).+1)
                =  (d.+1 + (Datatypes.length stages)). ssrlia.
  rewrite Hdid Hd select_switch_default_ge_len. apply IHs.
  * move => id0 Hlen'. 
    have H' := Hnum id0.+1. rewrite //= in H'.
    rewrite -Hd H'; try ssrlia.
  * move => id0 Hnd'. 
    have H' := Hnd id0.+1. rewrite Hd //= in H'.
    apply H'. to_nat Hnd'; ssrlia.
  * by rewrite -Hdid.
  * to_nat Hge0; ssrlia.
  * to_nat Hlen; ssrlia.
  * have H' := Hnd 0; rewrite //= in H'.
    apply H'. by []. 
  * have H' := Hnum 0; rewrite //= in H'.
    rewrite H'; try ssrlia. rewrite Int.unsigned_repr.
    to_nat Hge0; ssrlia. to_nat Hmax; ssrlia.
Qed.

Lemma select_switch_stage_drop_lt_len:
  forall id' id d default stages,
    (forall id, id < length stages
      -> FP_E.get_stage_id (List.nth id stages default) = d + id)
    -> (forall id, id < (length stages).-1
      -> forall id', List.nth id stages default <> DEFAULT id')
    -> id' <= id
    -> (Z.of_nat (d + id) <= Int.max_unsigned)%Z
    -> id < length stages
    ->  (select_switch (Int.unsigned (int_of_nat (d + id)))
                        (gen_fp_stages stages))
      = (select_switch (Int.unsigned (int_of_nat (d + id)))
                    (gen_fp_stages (drop id' stages))).
Proof.
  induction id' as [|id' IH] 
    => [|id d default stages Hlt Hd Hle Hmax Hlen].
  intros; by rewrite drop0.

  have id_gt_0: 0 < id. { by apply lt_impl_lt_0 with (m := id'). }
  to_nat id_gt_0.

  destruct stages as [|stage stages]; try by [].
  replace (drop id'.+1 (stage :: stages)) with (drop id' stages);
    try by [].

  replace (select_switch (Int.unsigned (int_of_nat (d + id)))
                (gen_fp_stages (stage :: stages)))
    with (select_switch (Int.unsigned (int_of_nat (d + id)))
                  (gen_fp_stages stages)).

  have Hdid: d + id = d.+1 + id.-1 by ssrlia.
  rewrite Hdid. 

  apply IH with (id := id.-1) (d := d.+1) (default := default).
  apply reduce_stages_wf with (stage := stage).

  to_nat Hle; apply ltnSE; ssrlia.
  { move => id0 Hid id0'.
    have Hid': id0.+1 < (Datatypes.length (stage :: stages)).-1.
    to_nat Hid; ssrlia.
    apply Hd with (id' := id0') in Hid'.
    rewrite //= in Hid'. }
  to_nat Hle. ssrlia.
  by rewrite -Hdid.
  rewrite //= in Hlen; to_nat Hlen; ssrlia.
  ssrlia.

  have Hnd' : forall id, stage <> DEFAULT id.
  { move => idd'. have H := Hd 0.
     rewrite //= in H. apply H. to_nat Hlen. ssrlia. }

  destruct (select_switch_case_gen_cases stages
                  (Int.unsigned (int_of_nat (d + id)%coq_nat)) Hnd')
    as [stmt Hswitch].
  rewrite  /select_switch Hswitch //= /proj_sumbool zeq_false //=.
  rewrite select_switch_default_gen_cases;[ reflexivity |].

  have Hle': 0 < (Datatypes.length (stage :: stages)).-1.
  { rewrite //= in Hlen; to_nat Hlen. ssrlia. }
  move => id0; apply Hd with (id' := id0) in Hle'; by rewrite //Hle'.
 
  have Hle': 0 < Datatypes.length (stage :: stages). by [].
  apply Hlt in Hle'. rewrite //= in Hle'.
  rewrite Hle' /int_of_nat.
  rewrite Int.unsigned_repr; try ssrlia. 
  split; ssrlia.
Qed.

Lemma seq_of_labeled_statement_fp_stage:
  forall fpe idb ids,
    let blocks := FP_E.get_fp_blocks fpe in
    let stages := FP_E_WF.get_block_stages (FP_E.get_block fpe idb) in
    let stages' := take ((length stages).-1) stages in
    let default := FP_E.default_stage fpe idb in
    let s := FP_E.get_stage fpe idb ids in
    verified_fp_e fpe
    -> FP_E_WF.wf_fp_e fpe
    -> (Z.of_nat ids <= Int.max_unsigned)%Z
    -> (seq_of_labeled_statement
        (select_switch (Int.unsigned (int_of_nat ids))
                          (gen_fp_stages stages)))
        = (seq_of_labeled_statement
              (gen_fp_stages ((drop ids stages') ++ [:: default]))).
Proof.
  move => fpe idb ids blocks stages stages' default s Hsize Hwf Hids.

  destruct (le_lt_dec ((length stages).-1) ids) as [Hge | Hlt];
    last first.

  (* First case *)
  { rewrite -(plus0n ids).
    erewrite select_switch_stage_drop_lt_len
      with (id' := ids) (default := FP_E.default_stage fpe idb); try by [].

    (* Manage drops *)
    { (* Get the whole list *)
      rewrite <- (drop_cat_le [::default]).
      rewrite /stages' -Nat.sub_1_r /default /stages.
      rewrite  <- (FP_E_WF.wf_stages_plus_default _ Hwf).

      rewrite (plus0n ids)
        (len_drop (FP_E.default_stage fpe idb)); try ssrlia.

      (** Get the ids stage *)
      replace (List.nth ids _ (FP_E.default_stage fpe idb)) with s;
        last first. by rewrite /s.
      have Hids' : FP_E.get_stage_id s = ids.
      { apply FP_E_WF.stage_wf_lt_len_numeroted; try by [].
        rewrite /default_stage_id /get_stages. rewrite /stages in Hlt.
        to_nat Hlt. ssrlia. }

      destruct s eqn:Hs; rewrite //= in Hids'; subst id;
        rewrite //= /gen_case /select_switch;
        try rewrite ZeqUInt //=; try by [].

      all: try rewrite zeq_true; try by [].

      have Hids': (ids < default_stage_id fpe idb)%coq_nat.
      { rewrite /default_stage_id /get_stages. rewrite /stages in Hlt.
         to_nat Hlt. ssrlia. }
      have Hd := ((FP_E_WF.get_wf_no_default Hwf) idb ids) Hids' ids.
      by rewrite -Hs /s in Hd.

      all: rewrite /stages in Hlt; try rewrite /stages'; ssrlia.

      rewrite size_takel /stages; ssrlia.
      rewrite size_eq_length; ssrlia.
    }

  all: try (move => id' Hlt'; rewrite /stages in Hlt'; to_nat Hlt').
  (* Well-indexed *)
  apply FP_E_WF.stage_wf_lt_len_numeroted; try by [].
  rewrite /default_stage_id /get_stages; ssrlia. 

  (* No default *)
    apply (FP_E_WF.get_wf_no_default Hwf).
    rewrite /default_stage_id /get_stages; ssrlia.

  (* Length *)
  ssrlia. }

  rewrite drop_oversize /stages.
  rewrite  (FP_E_WF.wf_stages_plus_default _ Hwf) -(plus0n ids). 
  replace (take _ _) with stages'; last first.
    by rewrite /stages' /stages Nat.sub_1_r. 
  rewrite /FP_E_WF.default_stage /FP_E_WF.default_stage_id.
  have Hl: (Datatypes.length (FP_E_WF.get_stages fpe idb) - 1)%coq_nat
                  = 0 + Datatypes.length stages'.
  { rewrite /stages' -(size_eq_length (take _ _)) size_takel
              /stages /FP_E_WF.get_stages; ssrlia.
     rewrite size_eq_length; ssrlia. }

  rewrite Hl select_switch_stage_drop_ge_len.
  by rewrite plus0n //= -(plus0n (Datatypes.length stages'))
                -Hl Nat.sub_1_r /default_stage_id
                /get_stages /FP_E_WF.get_stages Nat.sub_1_r.

  (* Get id *)
  { rewrite /stages' => id Hlt; repeat rewrite plus0n.
    have H' : (id <= default_stage_id fpe idb)%coq_nat.
    rewrite -size_eq_length size_takel /stages in Hlt;
      try rewrite size_eq_length; ssrlia; to_nat Hlt.
    rewrite /default_stage_id Nat.sub_1_r /get_stages; ssrlia.

    apply (FP_E_WF.stage_wf_lt_len_numeroted Hwf) in H'.
    rewrite list_nth_take. rewrite -size_eq_length size_takel /stages
            -Nat.sub_1_r.
    rewrite /get_stage /get_stages /default_stage /default_stage_id in H'.
    apply H'.

    rewrite size_eq_length; ssrlia.

    rewrite  -size_eq_length size_takel in Hlt.
    to_nat Hlt; ssrlia. rewrite size_eq_length; ssrlia. }

  (* No default *)
  { rewrite /stages' plus0n => id Hlt id'.
    have H : (id < default_stage_id fpe idb)%coq_nat.
      rewrite -size_eq_length size_takel /stages in Hlt;
        try rewrite size_eq_length; ssrlia; to_nat Hlt.
    rewrite /default_stage_id Nat.sub_1_r /get_stages; ssrlia.

    apply (FP_E_WF.get_wf_no_default Hwf) in H.
    have H' := H id'.
    rewrite list_nth_take. rewrite -size_eq_length size_takel /stages
            -Nat.sub_1_r.
    rewrite /get_stage /get_stages /default_stage /default_stage_id in H'.
    apply H'.

    rewrite size_eq_length; ssrlia.

    rewrite  -size_eq_length size_takel in Hlt.
    to_nat Hlt; ssrlia. rewrite size_eq_length; ssrlia. }

  ssrlia.

  all: rewrite /stages' -size_eq_length size_takel;
        try rewrite size_eq_length; ssrlia.

Qed.

(** ** Properties about select swicth for the on_exit/on_enter functions *)

Lemma select_switch_gen_code_generic:
  forall get_code code default id d  blocks,
    let b := List.nth id blocks default in 
  (forall id, id < length blocks
    -> FP_E.get_block_id (List.nth id blocks default) = d + id)
  -> (Z.of_nat (d + id) <= Int.max_unsigned)%Z
  -> id < length blocks
  -> get_code b = Some code
  -> exists s, (select_switch (Z.of_nat (d + id))
                            (fold_right (gen_case_on get_code)
                            (LScons None Sbreak LSnil) blocks))
    = LScons (Some (Z.of_nat (d + id))) 
                (gen_case_code_on code)
                s.
Proof.
  move => get_code code default; induction id as [|id IHid];
    move => d blocks b Hnum Hle Hlt Hcode;
    destruct blocks as [|block blocks]; try by inversion Hlt.

  (* id = 0 *)
  have Hb: b = block by rewrite /b. rewrite -Hb.
  apply Hnum in Hlt. rewrite //= -Hb in Hlt.

  eexists. by rewrite /gen_case_on //= Hcode //= Hlt
                    /select_switch /select_switch_case //=
                    zeq_true.

  (* Recursive case*)
  replace (select_switch (Z.of_nat (d + id.+1))
              (fold_right (gen_case_on get_code)
                            (LScons None Sbreak LSnil)
                            (block :: blocks)))
    with  (select_switch (Z.of_nat (d + id.+1))
              (fold_right (gen_case_on get_code)
                            (LScons None Sbreak LSnil)
                            blocks)).
  have Heq: d + id.+1 = d.+1 + id by ssrlia. rewrite Heq.

  apply IHid; try rewrite -Heq; try by ssrlia.
  - move => id' Hid. have Hnum' := Hnum id'.+1; rewrite //= in Hnum'.
    to_nat Hid. rewrite Hnum'; ssrlia.

  rewrite //= /gen_case_on; destruct (get_code block) as [code'|];
    rewrite /select_switch /select_switch_case //= zeq_false //=.

  have Hlt0: 0 < Datatypes.length (block :: blocks) by ssrlia.
  have Hblock := Hnum 0 Hlt0; rewrite //= in Hblock.
  rewrite Hblock. ssrlia.
Qed.

Lemma select_switch_gen_code_code:
  forall fpe get_code id code,
    let blocks := FP_E.get_fp_blocks fpe in
    let b := FP_E.get_block fpe id in
    verified_fp_e fpe
    -> (id < FP_E.get_nb_blocks fpe - 1)%coq_nat
    -> get_code b = Some code
    -> exists s,
        (select_switch  (Int.unsigned (int_of_nat id))
                            (fold_right (gen_case_on get_code)
                                (LScons None Sbreak LSnil) blocks))
        = LScons (Some (Z.of_nat id)) 
                    (gen_case_code_on code)
                    s.
Proof.
  move => fpe get_code id code blocks b Hsize' Hlt Hcode.

  rewrite /blocks //= in Hlt. to_nat Hlt.
  apply FP_E_WF.nb_blocks_to_length in Hlt.

  have Hmax: (Z.of_nat id <= Int.max_unsigned)%Z.
  { have H := get_nb_blocks8 Hsize'.
    rewrite /nb_blocks_lt_256 /is_nat8 //= in H.
    to_nat Hlt. rewrite /Int.max_unsigned //=.
    rewrite /FP_E_WF.get_nb_blocks in H. ssrlia. }
  rewrite -(ZeqUInt Hmax).

  edestruct (select_switch_gen_code_generic)
    with(default := FP_E.get_default_block fpe)
          (id := id) (d := 0) (blocks := get_fp_blocks fpe)
          as [s Hs]; try by []; rewrite ?plus0n; ssrlia.
  - rewrite /blocks => id' Hlt'.
    apply (get_correct_block_id Hsize').
    rewrite /FP_E.get_nb_blocks. to_nat Hlt'.
    remember (Datatypes.length _) as nb. ssrlia.
  - by rewrite FP_E_WF.get_fp_blocks_eq.
  - apply Hcode.

  eexists. by rewrite Hs plus0n.
Qed.

Lemma select_switch_gen_no_code_generic:
  forall (get_code: FP_E_WF.Def.fp_block -> option c_code)
            default id blocks,
  (forall id', id' < length blocks + 1
    -> (let b := List.nth id' blocks default in
           FP_E.get_block_id b = id
           -> get_code b = None))
  ->  select_switch_case (Z.of_nat id)
          (fold_right (gen_case_on get_code)
                        (LScons None Sbreak LSnil) blocks)
        = None.
Proof.
  move => get_code default id; induction blocks as [|block blocks IHb];
  move => Hcode; try by [].

  replace (select_switch_case (Z.of_nat id)
              (fold_right (gen_case_on get_code)
                            (LScons None Sbreak LSnil)
                            (block :: blocks)))
      with (select_switch_case (Z.of_nat id)
              (fold_right (gen_case_on get_code)
                            (LScons None Sbreak LSnil)
                            blocks)).

  apply IHb.

  move => id' Hlt b; subst b.
  have Hcode' := Hcode id'.+1; rewrite //= in Hcode'.
  apply Hcode'. to_nat Hlt. ssrlia.

  (* Prove the replace *)
  remember (get_block_id block) as idb.

  destruct (Nat.eq_dec id idb) as [Heq | Hne].

  (* id = idb *)
  have Hcode0 := Hcode 0; rewrite //= in Hcode0.
  rewrite /gen_case_on Hcode0 //=.
  by rewrite Heq Heqidb.

  (* id <> idb *)
  rewrite /gen_case_on //=.
  destruct (get_code block) as [code|]; try by [].
  rewrite /select_switch_case zeq_false //=.
  rewrite -Heqidb. ssrlia.
Qed.

Lemma select_switch_gen_no_code_code:
  forall fpe (get_code: FP_E_WF.Def.fp_block -> option c_code) id,
    let blocks := FP_E.get_fp_blocks fpe in
    let b := FP_E.get_block fpe id in
    verified_fp_e fpe
    -> is_nat8 id
    -> get_code b = None
    -> (select_switch_case (Int.unsigned (int_of_nat id))
                            (fold_right (gen_case_on get_code)
                                (LScons None Sbreak LSnil) blocks))
        = None.
Proof.
  move => fpe get_code id blocks b Hsize' H8 Hcode.

  have Hmax: (Z.of_nat id <= Int.max_unsigned)%Z.
  { have H := get_nb_blocks8 Hsize'.
    rewrite /nb_blocks_lt_256 /is_nat8 //= in H.
    rewrite /is_nat8 in H8. to_nat H8.
    rewrite /Int.max_unsigned //=. ssrlia. }

  rewrite -(ZeqUInt Hmax).

  erewrite select_switch_gen_no_code_generic
    with (default := FP_E.get_default_block fpe); try by [].

  move => id' Hlt' b' Hget.
  rewrite /b' (get_correct_block_id Hsize') in Hget. subst.
  rewrite /b /get_block in Hcode.
  by rewrite /b' Hcode. ssrlia.
Qed.

Lemma select_switch_gen_no_code_default:
  forall (get_code: FP_E_WF.Def.fp_block -> option c_code) blocks,
  (select_switch_default
  (fold_right (gen_case_on get_code) (LScons None Sbreak LSnil) blocks))
    = LScons None Sbreak LSnil.
Proof.
  move => get_code. induction blocks as [|block blocks IHb]; try by [].
  rewrite //= /gen_case_on.
  destruct (get_code block) as [code|]; try by [].
Qed.

(** Properties about gen_exceptions *)
Lemma gen_exceptions_cons:
  forall ex exs,
    gen_exceptions (ex :: exs) 
      = ((gen_exception ex) @+@ (gen_exceptions exs)).
Proof. by []. Qed.

(** Properties about label *)
Lemma create_ident_ne: 
  forall s1 s2, create_ident s1 <> create_ident s2 <-> s1 <> s2.
Proof.
  move => s1 s2; split; move => H H'.
  - by rewrite H' in H.
  - apply create_ident_injective in H'. by rewrite H' in H.
Qed.

Lemma gen_label_ne_block:
  forall l idb idb' ids ids',
    idb <> idb'
    -> (l ++ "_" ++ (string_of_nat idb) ++ "_" ++ (string_of_nat ids))%string
        <> (l ++ "_" ++ (string_of_nat idb') ++ "_" ++ (string_of_nat ids'))%string.
Proof.
  move => l idb idb' ids ids' H.
  do 2 apply ne_app_fst_string.
  destruct (Nat.eq_dec ids ids') as [Heq | Hne].
  - subst ids'. apply ne_app_lst_string. by apply ne_string_of_nat.
  - destruct (string_dec (string_of_nat idb ++ "_")%string
                                (string_of_nat idb' ++ "_")%string)
                                as [Heq' | Hne'].
    * rewrite ?string_app_assoc Heq'. apply ne_app_fst_string.
       by apply ne_string_of_nat.
    * by apply ne_app_string_of_nat.
Qed.

Lemma gen_label_ne_stage:
  forall l idb idb' ids ids',
    ids <> ids'
    -> (l ++ "_" ++ (string_of_nat idb) ++ "_" ++ (string_of_nat ids))%string
        <> (l ++ "_" ++ (string_of_nat idb') ++ "_" ++ (string_of_nat ids'))%string.
Proof.
  move => l idb idb' ids ids' H.
  do 2 apply ne_app_fst_string.
  destruct (Nat.eq_dec idb idb') as [Heq | Hne].
  - subst idb'. do 2 apply ne_app_fst_string. by apply ne_string_of_nat.
  - destruct (string_dec (string_of_nat idb ++ "_")%string
                                (string_of_nat idb' ++ "_")%string)
                                as [Heq' | Hne'].
    * rewrite ?string_app_assoc Heq'. apply ne_app_fst_string.
      by apply ne_string_of_nat.
    * by apply ne_app_string_of_nat.
Qed.

Lemma gen_while_label_ne_block:
  forall p p',
    FP_E.get_while_block_id p <> FP_E.get_while_block_id p'
    -> get_while_label p <> get_while_label p'.
Proof.
  rewrite /get_while_label /gen_label => p p' H.
  apply neg_ident. apply (gen_label_ne_block H).
Qed.

Lemma gen_end_while_label_ne_block:
  forall p p',
    FP_E.get_while_block_id p <> FP_E.get_while_block_id p'
    -> get_end_while_label p <> get_end_while_label p'.
Proof.
  rewrite /get_while_label /gen_label => p p' H.
  apply neg_ident. apply (gen_label_ne_block H).
Qed.

Lemma gen_while_label_ne_stage:
  forall p p',
    FP_E.get_while_id p <> FP_E.get_while_id p'
    -> get_while_label p <> get_while_label p'.
Proof.
  rewrite /get_while_label /gen_label => p p' H.
  apply neg_ident. apply (gen_label_ne_stage H).
Qed.

Lemma gen_end_while_label_ne_stage:
  forall p p',
    FP_E.get_end_while_id p <> FP_E.get_end_while_id p'
    -> get_end_while_label p <> get_end_while_label p'.
Proof.
  rewrite /get_while_label /gen_label => p p' H.
  apply neg_ident. apply (gen_label_ne_stage H).
Qed.

Lemma gen_while_ne_end:
  forall p p',
    get_while_label p <> get_end_while_label p'.
Proof.
  rewrite /get_while_label /get_end_while_label /gen_label
    => [[idb idw ide cond] [idb' idw' ide' cond'] //=].
  apply neg_ident. by [].
Qed.
