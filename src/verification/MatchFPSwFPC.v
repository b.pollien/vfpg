From Coq Require Import  FunInd Program Program.Equality
                        Program.Wf.
From VFP Require Import BasicTypes 
                        FlightPlanGeneric FlightPlanExtended
                        ClightGeneration 
                        CommonFPDefinition 
                        CommonLemmasNat8 
                        FPEnvironmentGeneric
                        FPEnvironmentDef
                        FPEnvironmentClight.
From compcert Require Import Integers AST Ctypes
                             Cop Clight Clightdefs Maps
                             Events Memory Values .
From Coq.PArith Require Import BinPos.

Set Warnings "-parsing".
From mathcomp Require Import all_ssreflect.
Set Warnings "parsing".
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Clightdefs.ClightNotations.
Local Open Scope nat_scope.
Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope clight_scope.

(** Definition of the a match env relation between FPS env and Clight env *)

Module MATCH_FPS_C (EVAL_Def: EVAL_ENV)
                            (ENVS_Def: ENVS_DEF EVAL_Def).

  Import ENVS_Def FP_ENV FPS_ENV FPE_ENV C_ENV
           OUT_TO_TRACE Def FP_E_WF.

  (** * Matching relation between fp_trace and trace *)
  Module MATCH_TRACE.

    (** Matching relation between fp_event and Cligth evevent *)
    Inductive match_event: fp_event -> event -> Prop :=
    | condition_event: forall cond r r',
      r = r'
      -> match_event (COND cond r)
                         (cond_event  cond r')
    | call_event: forall code, match_event (C_CODE code) (code_event code).

    (** Matching relation between fp_trace and trace *)
    Inductive match_trace: fp_trace -> trace -> Prop :=
    | empty_trace: match_trace nil nil
    | skip_trace: forall o t,
        match_trace o t
        -> match_trace (SKIP :: o) t
    | event_trace: forall c o e t,
      match_event c e
      -> match_trace o t
      -> match_trace (c :: o) (e ::t).

  End MATCH_TRACE.
  Import MATCH_TRACE.

  Notation "o ~t~ t" := (match_trace o t) (at level 10).

  (** ** Propeties about traces match *)
  Lemma match_trace_app:
    forall t1 t2 t1' t2',
      t1 ~t~ t2
      -> t1' ~t~ t2'
      -> (t1 ++ t1') ~t~ (t2 ++ t2').
  Proof.
    induction t1 as [| t t1 IHt]; move => t2 t1' t2' Ht Ht'.
    - inversion Ht. by [].
    - destruct t2 as [|t' t2]; inversion Ht.
      * apply skip_trace. by apply IHt.
      * apply skip_trace. by apply IHt.
      * apply event_trace; try by [].
        by apply IHt.
  Qed.

  Lemma match_trace_cond:
    forall cond res,
      [:: COND cond res] ~t~ [:: cond_event cond res].
  Proof.
    move => cond res. econstructor.
    - by econstructor.
    - by apply empty_trace.
  Qed.

  (** * Matching relation between FPE_ENV and C_ENV *)
  Module MATCH_ENV.
    Import FlightPlanExtended.FP_E.

    (** [writable8_mem m b] is defined is the block [b] in the       *)
    (** memory [m] has the Writable permission for a Mint8unsigned.  *)
    Definition writable8_mem (m: Mem.mem) (b: block): Prop :=
      Mem.valid_access m Mint8unsigned b 
            (Ptrofs.unsigned Ptrofs.zero) Writable.

    (** [correct_mem_access ge m id v] is defined if the symbol [id] *)
    (** exist in [ge] then the block referenced is Writable in the   *)
    (** memory and in this block we can find the value [v].          *)
    Definition correct_mem_access (ge: genv) (m: Mem.mem) 
                       (id: ident) (v: Values.val): Prop :=
      forall b,
        Globalenvs.Genv.find_symbol ge id = Some b
        -> writable8_mem m b
          /\ Mem.loadv Mint8unsigned m (Vptr b Ptrofs.zero) = Some v.

    (** [correct_const_access ge m id v] is defined if the symbol [id] *)
    (** exist in [ge] then we can find the value [v] in the block.       *)
    Definition correct_const_access (ge: genv) (m: Mem.mem) 
                       (id: ident): Prop :=
      forall b d nb_blocks l,
        Globalenvs.Genv.find_symbol ge id = Some b
        -> Globalenvs.Genv.find_def ge b = Some (Gvar d)
        -> gvar_init d = (AST.Init_int8 nb_blocks) :: l
        -> Mem.loadv Mint8unsigned m (Vptr b Ptrofs.zero)
              = Some (Vint nb_blocks).

    (** Match properties about fp_env and a memory state. Every
        flight plan information of the fp_env (for example nav_stage or
        nav_block) must be stored somewhere in the memory. *)
    Record match_env (ge: genv) (e: fp_env) (e': m_env)
                            := create_match_env {
      correct_nav_block: correct_mem_access ge e'
                                  CommonFP._nav_block
                                (create_val (get_nav_block e));
      correct_private_nav_block: correct_mem_access ge e'
                                CommonFP._private_nav_block
                              (create_val (get_nav_block e));
      correct_nav_stage: correct_mem_access ge e'
                                  CommonFP._nav_stage
                                 (create_val (get_nav_stage e));
      correct_private_nav_stage: correct_mem_access ge e'
                                  CommonFP._private_nav_stage
                                 (create_val (get_nav_stage e));
      correct_last_block: correct_mem_access ge e'
                                  CommonFP._last_block
                                 (create_val (get_last_block e));
      correct_private_last_block: correct_mem_access ge e'
                                  CommonFP._private_last_block
                                 (create_val (get_last_block e));
      correct_last_stage: correct_mem_access ge e'
                                    CommonFP._last_stage
                                    (create_val (get_last_stage e));
      correct_private_last_stage: correct_mem_access ge e'
                                    CommonFP._private_last_stage
                                    (create_val (get_last_stage e));
      correct_nb_blocks: correct_const_access ge e'
                                    CommonFP._nb_blocks;
    }.

  (** An fp_env and a c_env are matching iff the memory state is
      matching to the fp_env and they have matching memory states. *)
  Definition match_fp_cenv (ge: genv) (e: fp_env8) (e': fp_cenv): Prop :=
    match_env ge (` e) (get_m_env e')
    /\ (get_trace (` e)) ~t~ (C_ENV.get_trace e').

  End MATCH_ENV.
  Import MATCH_ENV.

  Notation "e1 '~menv~' '(' ge ',' e2 ')'"
    := (match_env ge e1 e2) (at level 10).

  Notation "e1 '~cenv~' '(' p ',' e2 ')'"
    := (match_fp_cenv p e1 e2) (at level 10).

  (** ** Proporties about the matching relation *)
  Lemma app_trace_preserve_mmatch:
    forall e1 t (ge: genv) e2,
      e1 ~menv~ (ge, e2)
      -> (app_trace e1 t) ~menv~ (ge, e2).
  Proof.
    rewrite /app_trace => e1 t ge e2 He.
    destruct He; by split.
  Qed.

  Lemma app_trace_preserve_mmatch_gen:
    forall ge e1 e2 post,
      e1 ~menv~ (ge, e2.m)
      -> (app_trace e1 post) ~menv~
          (ge, (app_ctrace e2 (fp_trace_to_trace post)) .m).
  Proof.
    move => ge e1 [m2 t2] post H.
    destruct e1. by destruct H.
  Qed.

  Lemma evalc_preserve_mmatch:
    forall e1 e1' b cond (ge: genv) e2,
      evalc e1 cond = (b, e1')
      -> e1 ~menv~ (ge, e2)
      -> e1' ~menv~ (ge, e2).
  Proof.
    rewrite /evalc => e1 e1' b cond ge e2 Heval He.
    injection Heval as Heval He1'. rewrite -He1'.
    by apply app_trace_preserve_mmatch.
  Qed.

  Lemma evalc_preserve_match:
    forall e1 e1' b cond (ge: genv) e2,
      evalc (` e1) cond = (b,` e1')
      -> e1 ~cenv~ (ge, e2)
      -> e1' ~cenv~ (ge, app_ctrace e2 [:: cond_event cond b]).
  Proof.
    rewrite /evalc.
    move  => [e1 He1] [e1' He1'] b cond ge e2 Heval [He Ht].
    split.
    - by apply (evalc_preserve_mmatch Heval).
    - injection Heval as Hbn He'; subst b e1'.
      apply match_trace_app; try by [].
      by apply match_trace_cond.
  Qed.

  (** ** Conversion function *)
  (** [update_val ge m id v] generates a new states memory from [m]   *)
  (** where the variable [id] is set to the value [v]. If [id] does   *)
  (** not exist in [ge] or the memory cannnot be changed, the         *)
  (** previous memory [m] is returned.                                *)
  Definition update_val (ge: genv) (m: Mem.mem)
                        (id: ident) (v: Values.val): Mem.mem :=
    match Globalenvs.Genv.find_symbol ge id with
      | Some b => 
        match Mem.storev Mint8unsigned m (Vptr b Ptrofs.zero) v with
        | Some m' => m'
        | None => m
        end
      | None => m
    end.

  Lemma get_update_val_same_id:
    forall (ge: genv) id m v v',
      correct_mem_access ge m id (create_val v')
      -> is_nat8 v
      -> correct_mem_access ge (update_val ge m id (create_val v))
                            id (create_val v).
  Proof.
    move => ge id m v v' Ha Hnat8 b Hid.

    have H := Hid; apply Ha in H; destruct H as [Hw Hload].

    rewrite /update_val Hid /Mem.storev /Mem.loadv.

    have Hw' := Hw.
    apply (Mem.valid_access_store m _ _ _ (create_val v)) in Hw'.
    destruct Hw' as [m' Hstore]. rewrite Hstore.

    split.
    (* Permission not modified *)
    by apply (Mem.store_valid_access_1 Mint8unsigned m b
                                      (Ptrofs.unsigned Ptrofs.zero)
                                      (create_val v)).

    rewrite id_load_nat8; try by [].
    by apply Mem.load_store_same with (m1 := m).
  Qed.

  Lemma get_update_val_diff_id:
    forall (ge: genv) id1 id2 m v v',
      id1 <> id2 
      -> (exists b, Globalenvs.Genv.find_symbol ge id1 = Some b)
      -> correct_mem_access ge m id2 v
      -> correct_mem_access ge (update_val ge m id1 v') id2 v.
  Proof.
    move => ge id1 id2 m v v' Hne Hid1 H b' Hid2.

    destruct Hid1 as [b Hid1].
    have Hid2' := Hid2; apply H in Hid2'; destruct Hid2' as [Hw Hload].
    remember (update_val ge m id1 v') as m'.

    rewrite /update_val Hid1 in Heqm'.
    destruct (Mem.storev Mint8unsigned m _ v') as [m''|] eqn:Hstore.
    rewrite -Heqm' /Mem.storev in Hstore.

    (** Write succeed *)
    split.
    - by apply (Mem.store_valid_access_1 Mint8unsigned m b
                                        (Ptrofs.unsigned Ptrofs.zero) v').
    - rewrite -Hload /Mem.loadv. 
      apply (Mem.load_store_other Mint8unsigned m b
                                  (Ptrofs.unsigned Ptrofs.zero) v');
                                  try by [].
      left. apply (Globalenvs.Genv.global_addresses_distinct ge) 
              with (id1 := id2) (id2 := id1); try by []. auto.

    (** Write failed *)
    rewrite Heqm'. by apply H.
  Qed.

  (** Update local environment *)
  Definition update_le_NavBlock (e: fp_env) (le: temp_env) :=
    set_opttemp (Some tmp_nav_block) (create_val (get_nav_block e)) le.

  Definition update_le_NavStage (e: fp_env) (le: temp_env) :=
    set_opttemp (Some tmp_nav_stage) (create_val (get_nav_stage e)) le.

  (** Get the element of an updated environment *)
  Lemma get_update_le_NavBlock:
    forall ge e env le m,
      eval_expr ge e (update_le_NavBlock env le) m 
                    tmpNavBlock (create_val (get_nav_block env)).
  Proof.
    move => ge e env le m. rewrite /tmpNavBlock. apply eval_Etempvar.
    rewrite /update_le_NavBlock /set_opttemp.
    apply PTree.gss.
  Qed.

  Lemma get_update_le_NavStage:
    forall ge e env le m,
      eval_expr ge e (update_le_NavStage env le) m 
                    tmpNavStage (create_val (get_nav_stage env)).
  Proof.
    move => ge e env le m. rewrite /tmpNavStage. apply eval_Etempvar.
    rewrite /update_le_NavStage /set_opttemp.
    apply PTree.gss.
  Qed.

  (** Properties on the conversion from a fp_trace into a trace *)
  Lemma trace_generated_match:
    forall o, o ~t~ (fp_trace_to_trace o).
  Proof.
    induction o as [|[cond | code| ] o']; rewrite //=.
    - apply empty_trace.
    - apply event_trace; try by [].
      by apply condition_event.
    - apply event_trace; try by [].
      by apply call_event.
    - by apply skip_trace.
  Qed.

  Lemma trace_generated_eq':
    forall t o, 
      t ~t~ o
      -> fp_trace_to_trace t = o.
  Proof.
    induction t as [|e t IHt]; move => o Ho.
    - by inversion Ho.
    - inversion Ho as [| t' o' Ht | e' t' e_o o' Heq Ht].
      * subst t' o'. by apply IHt.
      * subst e' t'.
         inversion Heq as [ cond b b' Hb Hcond He| c code Hcode].
         + subst b' e e_o. rewrite /=. f_equal.
            by apply IHt.
         + subst e e_o o. rewrite /=. f_equal.
            by apply IHt.
  Qed.

  Lemma trace_generated_eq:
    forall ge e1 e2,
      e1 ~cenv~ (ge, e2)
      -> fp_trace_to_trace (get_trace (` e1)) = e2.t.
  Proof.
    move => ge e1 e2 [_ Ht].
    by apply trace_generated_eq'.
  Qed.

  Lemma app_ctrace_match:
    forall e1 e2 c_code,
      (get_trace e1) ~t~ (e2 .t)
      -> (get_trace (app_trace e1 c_code)) ~t~
            ((app_ctrace e2 (fp_trace_to_trace c_code)) .t).
  Proof.
    move => [[idb ids lidb lids] t] [m t'] c_code H;
      rewrite /=; rewrite /= in H.
    apply match_trace_app => [ // |].
    apply trace_generated_match.
  Qed.

  Lemma app_ctrace_mem:
    forall e c,
      e.m = (app_ctrace e c).m.
  Proof. by destruct e. Qed.

  Lemma set_fp_trace_to_trace:
    forall params, fp_trace_to_trace [:: get_c_set params]
            = [:: code_event_assign_set params].
  Proof. by destruct params. Qed.

  (** Tranformation of exec to trace*)
  Definition exec_to_trace (exec: option c_code): Events.trace :=
  match exec with
  | None => E0
  | Some e => (Event_annot e [::]) :: E0
  end.

  Lemma exec_to_trace_fp_trace:
  forall func,
    fp_trace_to_trace (opt_c_code_to_trace func) = exec_to_trace func.
  Proof. by destruct func. Qed.

  Lemma app_trace_preserve_match:
    forall ge e1 e2 e' code,
    e' = app_trace (` e1) code
    -> e1 ~cenv~ (ge, e2)
    -> exists e1',
        e' = ` e1'
        /\ e1' ~cenv~ (ge, app_ctrace e2 (fp_trace_to_trace code)).
  Proof.
    move => ge e1 e2 e' code Heq He.
    have H: fp_env_on_8 e'.
    { subst e'. by destruct e1 as [e1 []]. }
    exists (exist _ e' H); split => // ; split; rewrite /=Heq /= ;
      destruct He as [He Ht].
    - by apply app_trace_preserve_mmatch.
    - apply match_trace_app => //. apply trace_generated_match.
  Qed.

  Lemma match_app_trace:
    forall ge e1 e2 code,
    e1 ~cenv~ (ge, e2)
    -> exists e1',
        ` e1' = app_trace (` e1) code
        /\ e1' ~cenv~ (ge, app_ctrace e2 (fp_trace_to_trace code)).
  Proof.
    move => ge e1 e2 code He.
    remember (app_trace (`e1) code) as e1''.
    destruct (app_trace_preserve_match Heqe1'' He) as [e_f [Heq He']].
    exists e_f; split => //.
  Qed.

  Lemma app_trace_preserve_match2:
    forall ge e1 e2 code,
      e1 ~cenv~ (ge, e2)
      -> (app_trace (`e1) code)
            ~menv~ (ge, (app_ctrace e2 (fp_trace_to_trace code)) .m)
          /\ (get_trace (app_trace (` e1) code))
                ~t~ ((app_ctrace e2 (fp_trace_to_trace code)) .t).
  Proof.
    move => ge e1 e2 code [He Ht].
    split.
    - by apply app_trace_preserve_mmatch_gen.
    - by apply app_ctrace_match.
  Qed.

  (** ** Properties about m_env *)

  Lemma nb_blocks_not_modified:
    forall (ge: genv) e2 e2' var block t,
    correct_const_access ge e2 CommonFP._nb_blocks
    -> Globalenvs.Genv.find_symbol ge var = Some block
    -> Mem.store Mint8unsigned e2 block (Ptrofs.unsigned Ptrofs.zero) t
        = Some e2'
    -> CommonFP._nb_blocks <> var
    -> correct_const_access ge e2' CommonFP._nb_blocks.
  Proof.
    move => ge e2 e2' var block t Hconst Hfs Hstore Hne
                block' d nb_blocks l Hfs' Hfd' Hg.
    have H' :=  (Hconst _ d nb_blocks l Hfs' Hfd' Hg).
    rewrite /Mem.loadv.
    rewrite (Mem.load_store_other _ e2 _ _ _ _ Hstore); try left.
    * rewrite /Mem.loadv in H'. by rewrite H'.
    * apply (Globalenvs.Genv.global_addresses_distinct _ Hne Hfs' Hfs).
  Qed.

  Lemma update_nav_block_match:
    forall (ge: genv) e1 e2 e2' e2'' id b_p b_v,
    e1 ~menv~ (ge, e2)
    -> is_nat8 id
    -> Globalenvs.Genv.find_symbol ge CommonFP._private_nav_block
          = Some b_p
    -> Globalenvs.Genv.find_symbol ge CommonFP._nav_block
          = Some b_v
    -> Mem.store Mint8unsigned e2 b_p (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
          = Some e2'
    -> Mem.store Mint8unsigned e2' b_v (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
        = Some e2''
    -> let e1' :=  update_nav_block e1 id in
          e1'  ~menv~ (ge, e2'').
  Proof.
    move => ge e1 e2 e2' e2'' id b_p b_v He He8 Hfsp Hfsv Hsp Hsv e1'.

    have Heq1: update_nav_block e1 id = e1' by rewrite /e1'.

    have Hne:
      forall var v,
      var <> CommonFP._private_nav_block
      -> var <> CommonFP._nav_block
      -> correct_mem_access ge e2 var (create_val v)
      -> correct_mem_access ge e2'' var (create_val v).
    {move => var v Hne Hne' Hmem b Hfp. 
    have Hfp' := Hfp. apply Hmem in Hfp'. split.
    - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
      apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
      apply (proj1 Hfp').
    - rewrite /Mem.loadv (Mem.load_store_other _ _ _ _ _ _ Hsv);try left.
      rewrite (Mem.load_store_other _ _ _ _ _ _ Hsp); try left.
      * apply (proj2 Hfp').
      * apply (Globalenvs.Genv.global_addresses_distinct _ Hne Hfp Hfsp).
      * apply (Globalenvs.Genv.global_addresses_distinct _ Hne' Hfp Hfsv).
    }

    have Heq:
      forall var b,
      Globalenvs.Genv.find_symbol ge var = Some b
      -> writable8_mem e2 b
      -> Mem.load Mint8unsigned e2'' b (Ptrofs.unsigned Ptrofs.zero)
          = Some (Val.load_result Mint8unsigned
              (Vint (cast_int_int I8 Unsigned (int_of_nat id))))
      -> correct_mem_access ge e2'' var (create_val id).
    { move => var b Hfs Hw Hl b' Hfs'.
      rewrite Hfs in Hfs'; inversion Hfs'; subst b'; split.
      - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
        by apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
      - by rewrite /Mem.loadv (id_load_nat8 He8)
                (cast_int_int8_create_val He8).
    }

    split.

    apply (Heq _ _ Hfsv (proj1 ((correct_nav_block He) _ Hfsv)));
      apply (Mem.load_store_same _ _ _ _ _ _ Hsv).

    apply (Heq _ _ Hfsp (proj1 ((correct_private_nav_block He) _ Hfsp))).
    { rewrite (Mem.load_store_other _ _ _ _ _ _ Hsv); try left.
      * apply (Mem.load_store_same _ _ _ _ _ _ Hsp).
      * have Hne':
            CommonFP._private_nav_block <> CommonFP._nav_block
            by apply neg_ident.
        eapply (Globalenvs.Genv.global_addresses_distinct
                      _ Hne' Hfsp Hfsv).
    }

    apply Hne; try by apply neg_ident.
      rewrite -(update_block_nc_stage Heq1);
      apply (correct_nav_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_block_nc_stage Heq1);
      apply (correct_private_nav_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_block_nc_last_block Heq1);
      apply (correct_last_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_block_nc_last_block Heq1);
      apply (correct_private_last_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_block_nc_last_stage Heq1);
      apply (correct_last_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_block_nc_last_stage Heq1);
      apply (correct_private_last_stage He).

    eapply nb_blocks_not_modified.
      apply (nb_blocks_not_modified (correct_nb_blocks He) Hfsp Hsp);
        by apply neg_ident.
      apply Hfsv. apply Hsv. by apply neg_ident.
  Qed.

  Lemma update_nav_stage_match:
    forall (ge: genv) e1 e2 e2' e2'' id b_p b_v,
    e1 ~menv~ (ge, e2)
    -> is_nat8 id
    -> Globalenvs.Genv.find_symbol ge CommonFP._private_nav_stage
          = Some b_p
    -> Globalenvs.Genv.find_symbol ge CommonFP._nav_stage
          = Some b_v
    -> Mem.store Mint8unsigned e2 b_p (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
          = Some e2'
    -> Mem.store Mint8unsigned e2' b_v (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
        = Some e2''
    -> let e1' :=  update_nav_stage e1 id in
          e1'  ~menv~ (ge, e2'').
  Proof.
    move => ge e1 e2 e2' e2'' id b_p b_v He He8 Hfsp Hfsv Hsp Hsv e1'.

    have Heq1: update_nav_stage e1 id = e1' by rewrite /e1'.

    have Hne:
      forall var v,
      var <> CommonFP._private_nav_stage
      -> var <> CommonFP._nav_stage
      -> correct_mem_access ge e2 var (create_val v)
      -> correct_mem_access ge e2'' var (create_val v).
    {move => var v Hne Hne' Hmem b Hfp. 
     have Hfp' := Hfp. apply Hmem in Hfp'. split.
     - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
       apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
       apply (proj1 Hfp').
     - rewrite /Mem.loadv (Mem.load_store_other _ _ _ _ _ _ Hsv);try left.
       rewrite (Mem.load_store_other _ _ _ _ _ _ Hsp); try left.
       * apply (proj2 Hfp').
       * apply (Globalenvs.Genv.global_addresses_distinct _ Hne Hfp Hfsp).
       * apply (Globalenvs.Genv.global_addresses_distinct _ Hne' Hfp Hfsv).
    }

    have Heq:
      forall var b,
      Globalenvs.Genv.find_symbol ge var = Some b
      -> writable8_mem e2 b
      -> Mem.load Mint8unsigned e2'' b (Ptrofs.unsigned Ptrofs.zero)
          = Some (Val.load_result Mint8unsigned
              (Vint (cast_int_int I8 Unsigned (int_of_nat id))))
      -> correct_mem_access ge e2'' var (create_val id).
    { move => var b Hfs Hw Hl b' Hfs'.
      rewrite Hfs in Hfs'; inversion Hfs'; subst b'; split.
      - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
        by apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
      - by rewrite /Mem.loadv (id_load_nat8 He8)
                 (cast_int_int8_create_val He8).
    }

    split.

    apply Hne; try by apply neg_ident.
      rewrite -(update_stage_nc_block Heq1); apply (correct_nav_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_stage_nc_block Heq1);
      apply (correct_private_nav_block He).

    apply (Heq _ _ Hfsv (proj1 ((correct_nav_stage He) _ Hfsv)));
      apply (Mem.load_store_same _ _ _ _ _ _ Hsv).

    apply (Heq _ _ Hfsp (proj1 ((correct_private_nav_stage He) _ Hfsp))).
    { rewrite (Mem.load_store_other _ _ _ _ _ _ Hsv); try left.
       * apply (Mem.load_store_same _ _ _ _ _ _ Hsp).
       * have Hne':
            CommonFP._private_nav_stage <> CommonFP._nav_stage
            by apply neg_ident.
         eapply (Globalenvs.Genv.global_addresses_distinct
                      _ Hne' Hfsp Hfsv).
    }

    apply Hne; try by apply neg_ident.
      rewrite -(update_stage_nc_last_block Heq1);
      apply (correct_last_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_stage_nc_last_block Heq1);
      apply (correct_private_last_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_stage_nc_last_stage Heq1);
      apply (correct_last_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_stage_nc_last_stage Heq1);
      apply (correct_private_last_stage He).

    eapply nb_blocks_not_modified.
      apply (nb_blocks_not_modified (correct_nb_blocks He) Hfsp Hsp);
        by apply neg_ident.
      apply Hfsv. apply Hsv. by apply neg_ident.
  Qed.

  Lemma update_last_block_match:
    forall (ge: genv) e1 e2 e2' e2'' id b_p b_v,
    e1 ~menv~ (ge, e2)
    -> is_nat8 id
    -> Globalenvs.Genv.find_symbol ge CommonFP._private_last_block
          = Some b_p
    -> Globalenvs.Genv.find_symbol ge CommonFP._last_block
          = Some b_v
    -> Mem.store Mint8unsigned e2 b_p (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
          = Some e2'
    -> Mem.store Mint8unsigned e2' b_v (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
        = Some e2''
    -> let e1' :=  update_last_block e1 id in
          e1'  ~menv~ (ge, e2'').
  Proof.
    move => ge e1 e2 e2' e2'' id b_p b_v He He8 Hfsp Hfsv Hsp Hsv e1'.

    have Heq1: update_last_block e1 id = e1' by rewrite /e1'.

    have Hne:
      forall var v,
      var <> CommonFP._private_last_block
      -> var <> CommonFP._last_block
      -> correct_mem_access ge e2 var (create_val v)
      -> correct_mem_access ge e2'' var (create_val v).
    {move => var v Hne Hne' Hmem b Hfp. 
     have Hfp' := Hfp. apply Hmem in Hfp'. split.
     - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
       apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
       apply (proj1 Hfp').
     - rewrite /Mem.loadv (Mem.load_store_other _ _ _ _ _ _ Hsv);try left.
       rewrite (Mem.load_store_other _ _ _ _ _ _ Hsp); try left.
       * apply (proj2 Hfp').
       * apply (Globalenvs.Genv.global_addresses_distinct _ Hne Hfp Hfsp).
       * apply (Globalenvs.Genv.global_addresses_distinct _ Hne' Hfp Hfsv).
    }

    have Heq:
      forall var b,
      Globalenvs.Genv.find_symbol ge var = Some b
      -> writable8_mem e2 b
      -> Mem.load Mint8unsigned e2'' b (Ptrofs.unsigned Ptrofs.zero)
          = Some (Val.load_result Mint8unsigned
              (Vint (cast_int_int I8 Unsigned (int_of_nat id))))
      -> correct_mem_access ge e2'' var (create_val id).
    { move => var b Hfs Hw Hl b' Hfs'.
      rewrite Hfs in Hfs'; inversion Hfs'; subst b'; split.
      - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
        by apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
      - by rewrite /Mem.loadv (id_load_nat8 He8)
                 (cast_int_int8_create_val He8).
    }

    split.

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_block_nc_block Heq1);
      apply (correct_nav_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_block_nc_block Heq1);
      apply (correct_private_nav_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_block_nc_stage Heq1);
      apply (correct_nav_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_block_nc_stage Heq1);
      apply (correct_private_nav_stage He).

    apply (Heq _ _ Hfsv (proj1 ((correct_last_block He) _ Hfsv)));
      apply (Mem.load_store_same _ _ _ _ _ _ Hsv).

    apply (Heq _ _ Hfsp (proj1 ((correct_private_last_block He) _ Hfsp))).
    { rewrite (Mem.load_store_other _ _ _ _ _ _ Hsv); try left.
       * apply (Mem.load_store_same _ _ _ _ _ _ Hsp).
       * have Hne':
            CommonFP._private_last_block <> CommonFP._last_block
            by apply neg_ident.
         eapply (Globalenvs.Genv.global_addresses_distinct
                      _ Hne' Hfsp Hfsv).
    }

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_block_nc_last_stage Heq1);
      apply (correct_last_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_block_nc_last_stage Heq1);
      apply (correct_private_last_stage He).

    eapply nb_blocks_not_modified.
      apply (nb_blocks_not_modified (correct_nb_blocks He) Hfsp Hsp);
        by apply neg_ident.
      apply Hfsv. apply Hsv. by apply neg_ident.
  Qed.

  Lemma update_last_stage_match:
    forall (ge: genv) e1 e2 e2' e2'' id b_p b_v,
    e1 ~menv~ (ge, e2)
    -> is_nat8 id
    -> Globalenvs.Genv.find_symbol ge CommonFP._private_last_stage
          = Some b_p
    -> Globalenvs.Genv.find_symbol ge CommonFP._last_stage
          = Some b_v
    -> Mem.store Mint8unsigned e2 b_p (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
          = Some e2'
    -> Mem.store Mint8unsigned e2' b_v (Ptrofs.unsigned Ptrofs.zero)
                (Vint (cast_int_int I8 Unsigned (int_of_nat id)))
        = Some e2''
    -> let e1' :=  update_last_stage e1 id in
          e1'  ~menv~ (ge, e2'').
  Proof.
    move => ge e1 e2 e2' e2'' id b_p b_v He He8 Hfsp Hfsv Hsp Hsv e1'.

    have Heq1: update_last_stage e1 id = e1' by rewrite /e1'.

    have Hne:
      forall var v,
      var <> CommonFP._private_last_stage
      -> var <> CommonFP._last_stage
      -> correct_mem_access ge e2 var (create_val v)
      -> correct_mem_access ge e2'' var (create_val v).
    {move => var v Hne Hne' Hmem b Hfp. 
     have Hfp' := Hfp. apply Hmem in Hfp'. split.
     - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
       apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
       apply (proj1 Hfp').
     - rewrite /Mem.loadv (Mem.load_store_other _ _ _ _ _ _ Hsv);try left.
       rewrite (Mem.load_store_other _ _ _ _ _ _ Hsp); try left.
       * apply (proj2 Hfp').
       * apply (Globalenvs.Genv.global_addresses_distinct _ Hne Hfp Hfsp).
       * apply (Globalenvs.Genv.global_addresses_distinct _ Hne' Hfp Hfsv).
    }

    have Heq:
      forall var b,
      Globalenvs.Genv.find_symbol ge var = Some b
      -> writable8_mem e2 b
      -> Mem.load Mint8unsigned e2'' b (Ptrofs.unsigned Ptrofs.zero)
          = Some (Val.load_result Mint8unsigned
              (Vint (cast_int_int I8 Unsigned (int_of_nat id))))
      -> correct_mem_access ge e2'' var (create_val id).
    { move => var b Hfs Hw Hl b' Hfs'.
      rewrite Hfs in Hfs'; inversion Hfs'; subst b'; split.
      - apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsv).
        by apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hsp).
      - by rewrite /Mem.loadv (id_load_nat8 He8)
                 (cast_int_int8_create_val He8).
    }

    split.

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_stage_nc_block Heq1);
      apply (correct_nav_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_stage_nc_block Heq1);
      apply (correct_private_nav_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_stage_nc_stage Heq1);
      apply (correct_nav_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_stage_nc_stage Heq1);
      apply (correct_private_nav_stage He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_stage_nc_last_block Heq1);
      apply (correct_last_block He).

    apply Hne; try by apply neg_ident.
      rewrite -(update_last_stage_nc_last_block Heq1);
      apply (correct_private_last_block He).

    apply (Heq _ _ Hfsv (proj1 ((correct_last_stage He) _ Hfsv)));
      apply (Mem.load_store_same _ _ _ _ _ _ Hsv).

    apply (Heq _ _ Hfsp (proj1 ((correct_private_last_stage He) _ Hfsp))).
    { rewrite (Mem.load_store_other _ _ _ _ _ _ Hsv); try left.
       * apply (Mem.load_store_same _ _ _ _ _ _ Hsp).
       * have Hne':
            CommonFP._private_last_stage <> CommonFP._last_stage
            by apply neg_ident.
         eapply (Globalenvs.Genv.global_addresses_distinct
                      _ Hne' Hfsp Hfsv).
    }

    eapply nb_blocks_not_modified.
      apply (nb_blocks_not_modified (correct_nb_blocks He) Hfsp Hsp);
        by apply neg_ident.
      apply Hfsv. apply Hsv. by apply neg_ident.
  Qed.

End MATCH_FPS_C.
