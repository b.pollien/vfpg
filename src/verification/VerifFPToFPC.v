From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.


Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import BasicTypes
                        FlightPlan
                        FlightPlanSized
                        Generator
                        FPExtended
                        FPSizeVerification
                        FBDerouteAnalysis
                        GeneratorProperties
                        FPEnvironmentGeneric 
                        FPEnvironmentDef
                        FPBigStepGeneric
                        FPBigStepDef
                        VerifFPToFPE
                        VerifFPEToFPS
                        VerifFPSToFPC
                        GvarsVerification.

Local Open Scope nat_scope.

Set Implicit Arguments.

(** * Verification FP to Clight *)

(** Verification of the semantics preservation between FP and the Clight
code *)

Import FP.

(** * Definition of Global generation function *)
(** This function is similar to the function extracted and used by the
  generated (generate_flight_plan defined in Generator.v). The difference
  from the original function is that the program contains the functions
  generated but also a simplified environment from CommonFP (see
  CommonFPSimplified for more information on why we use a simplified
  version). This environment contains for example the definition of the
  variables private_nav_block or the function NextStage. *)

Definition generator (fp: flight_plan) (gvars: list string): res_gen :=
  let fpe := extend_flight_plan fp in
  let deroute_analysis := fb_deroute_analysis fp in
  match gvars_definition gvars with
  | GVARS gvars =>
    match size_verification fpe with
    | OK fps =>
      CODE (generate_complete_context (get_fp fps) gvars, deroute_analysis)
    | ERR errors => ERROR (errors ++ deroute_analysis)
    end
  | ERR_GVARS errors => ERROR (deroute_analysis ++ errors)
  end.

Module FP_TO_C_VERIF (EVAL_Def: EVAL_ENV)
                                 (ENVS_Def: ENVS_DEF EVAL_Def)
                                 (BS_Def: FP_BS_DEF EVAL_Def ENVS_Def).
  Import BS_Def FP_BS C_BS.

  (** Verification of FP to FPE *)
  Module FP_TO_FPE := FP_TO_FPE_VERIF EVAL_Def ENVS_Def BS_Def.
  Import FP_TO_FPE.

  (** Verification of FPE to FPS *)
  Module FPE_TO_FPS := FPE_TO_FPS_VERIF EVAL_Def ENVS_Def BS_Def.
  Import FPE_TO_FPS.

  (** Verification of FPS to Clight *)
  Module FPS_TO_C := FPS_TO_C_VERIF EVAL_Def ENVS_Def BS_Def.
  Import FPS_TO_C.

Section FLIGHT_PLAN.

  (** Initial Flight Plan *)
  Variable fp: flight_plan.

  (** Global variables defined by the user *)
  Variable gvars: list string.

  (** Resulting program *)
  Variable prog: Clight.program.

  (** Resulting warnigs *)
  Variable warnings: list err_msg.

  (** The generator does not produce error *)
  Hypothesis Hcorrect: generator fp gvars = CODE (prog, warnings).

  (** Size verification produces a result *)
  Lemma size_verification_result:
    exists cgvars fps,
                  gvars_definition gvars = GVARS cgvars
                  /\ size_verification (FP_TO_FPE.fpe fp) = OK fps
                  /\ prog = generate_complete_context (get_fp fps) cgvars.
  Proof.
    have H := Hcorrect.
    rewrite /generator in H;
      destruct gvars_definition as [cgvars | e];
      destruct (size_verification _) as [fps | e'].

    injection H as Hprog Hw. by exists cgvars, fps. 

    all: by [].
  Qed.

  (** ** Bisimulation between FP to Clight*)
  Theorem bisim_transf_correct:
    bisimulation (FP_BS.semantics_fp fp)
                            (C_BS.semantics_fpc prog).
  Proof.
    (** FP to FPE *)
    apply (compose_bisimulations
      (FP_TO_FPE.bisim_transf_extended_correct fp)).

    (** FPE to FPS *)
    destruct size_verification_result
      as [cgvars [fps [Hgvars [Hfps Hprog]]]].
    apply (compose_bisimulations
      (FPE_TO_FPS.bisim_transf_extended_correct Hfps)).

    (** FPS to Clight *)
    rewrite Hprog /=.
    apply FPS_TO_C.bisim_transf_extended_correct.
  Qed.

End FLIGHT_PLAN.

End FP_TO_C_VERIF.
