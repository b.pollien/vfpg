From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

From compcert Require Import Clight.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas CommonLemmas 
                        BasicTypes FlightPlanGeneric
                        FlightPlanExtended
                        FPNavigationModeSem
                        CommonFPDefinition
                        GvarsVerification
                        Generator GeneratorProperties
                        ExtractTraceGeneric
                        FPEnvironmentGeneric FPEnvironmentDef
                        FPEnvironmentClight
                        FPBigStepDef. 

Import Clightdefs.ClightNotations.
Local Open Scope clight_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.
Import ListNotations.

Set Implicit Arguments.

(** File to extract the trace generated for each call in the FPE semantics
  *)

(** * Extract trace module for fp_env *)

Module EXTRACT_TRACE_FPENV (EVAL_Def: EVAL_ENV)
                                  (ENVS_Def: ENVS_DEF EVAL_Def)
                                  (BS_Def: FP_BS_DEF EVAL_Def ENVS_Def).
  Import BS_Def FPS_BS FPE_BS FPE_BS.Common_Sem
           FPS_ENV FPE_ENV FPE_ENV.Def MATCH_FPS_C
           MATCH_ENV.

  (** Definition of ENV_TRACE for fp_env *)
  Module Def <: ENV_TRACE.
    Definition event:= fp_event.
    Definition trace := list event.

    (** Environments that contain trace *)
    Definition env := fp_env.
    Definition get_trace := get_trace.
  End Def.
  Import Def.
  Module Import FP_EXTRACT := EXTRACT_TRACE_GEN Def.
  Export FP_EXTRACT.

    Section FLIGHT_PLAN.
    (** Flight Plan Extended *)
    Variable fp: FP_E_WF.flight_plan_wf.
    Definition fpe: FP_E_WF.flight_plan := proj1_sig fp.
    Definition Hwf := proj2_sig fp.

    (** Correct list of global variables defined by the user *)
    Variable gvars: cgvars.

    (** Global program generated *)
    Definition prog := generate_complete_context fpe gvars.

    (** Definition of the global environment *)
    Definition ge := globalenv prog.

    (** Function to execute*)
    Definition f := gen_fp_auto_nav fpe.

    (** * Commons Lemmas specific to fp_env *)

    Lemma extract_app_trace:
      forall e t,
        extract_trace e (app_trace e t) = t.
    Proof.
      move => [[idb ids lidb lids] t] t'.
      by rewrite /extract_trace /app_trace //= drop_size_cat.
    Qed.

    (** Property about extract trace *)
  
    Lemma is_extract_app_trace:
      forall e t,
        is_extract_trace e (app_trace e t) t.
    Proof.
      move => e t. by destruct e.
    Qed.

    Lemma change_block_eq_trace:
      forall e id e',
        change_block fpe e id = e'
        -> get_trace e = get_trace e'.
    Proof. 
      rewrite /change_block => e id e' H.
      destruct ((_ =? _)%N); by rewrite -H.
    Qed.

    (** Property that a trace has been appended *)

    Lemma get_app_trace:
      forall e t,
        get_trace (app_trace e t) = ((get_trace e) ++ t)%list.
    Proof. by destruct e. Qed.

    Lemma trace_appended_app_trace:
      forall e t, trace_appended e (app_trace e t).
    Proof.
      rewrite /trace_appended => e t.
      rewrite extract_app_trace. apply is_extract_app_trace.
    Qed.

    Lemma trace_appended_app_trace_rec:
      forall e e' t, 
        trace_appended e e'
        -> trace_appended e (app_trace e' t).
    Proof.
      move => e e' t H.
      apply (trace_appended_trans H).
       apply trace_appended_app_trace.
    Qed.

    Lemma app_extract_app_trace:
      forall e e' t,
        trace_appended (app_trace e t) e'
        -> t ++ (extract_trace (app_trace e t) e') = extract_trace e e'.
    Proof.
      move => e e' t H. 
      rewrite -(extract_app_trace e t). symmetry.
      replace (app_trace e (extract_trace _ _)) with (app_trace e t);
        try by rewrite extract_app_trace.
      by apply (trace_appended_app (trace_appended_app_trace e t) H).
    Qed.

    Lemma extract_app_trace_gen:
      forall e e' t,
        trace_appended e e'
        -> extract_trace e (app_trace e' t) = (extract_trace e e') ++  t.
    Proof.
      move => [[idb ids lidb lids] t] [[idb' ids' lidb' lids'] t'] t''.
      rewrite /trace_appended /is_extract_trace /extract_trace
                /app_trace //= => H.
      rewrite H drop_cat_le. f_equal.
      rewrite size_cat size_eq_length. ssrlia.
    Qed.

  (** * Verification of the semantics function *)

  (** ** Verification of forbidden deroute *)

  Lemma update_stage_nc_trace:
    forall e e' ids,
    update_nav_stage e ids = e' 
    -> get_trace e = get_trace e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hb.
  Qed.

  Lemma extract_update_stage:
    forall e e' e'' id,
      update_nav_stage e id = e'
      -> extract_trace e e'' = extract_trace e' e''.
  Proof.
    rewrite /extract_trace => [[s t] [s' t'] [s'' t'']] id H.
    destruct s, s', s''. injection H as Hb Hs Hlb Hls Ht.
    by rewrite //= Ht.
  Qed.

  Lemma extract_test_forbidden_deroute:
    forall e e' from to b fbd,
      test_forbidden_deroute e from to fbd = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /test_forbidden_deroute /trace_appended
      => e e' from to b [from' to' cond].
    destruct (_ && _) => //= H.
    - destruct cond; injection H as Hb He; subst e'.
      * rewrite extract_app_trace. 
        apply is_extract_app_trace.
      * apply trace_appended_refl.
    - injection H as Hb He; subst e'.
      apply trace_appended_refl.
  Qed.

  Lemma extract_test_forbidden_deroutes:
    forall fbds e e' from to b,
      test_forbidden_deroutes e from to fbds = (b, e')
      -> trace_appended e e'.
  Proof.
    induction fbds as [| fbd fbds IH];
      move => e e' from to b Htest.
    - inversion Htest; subst e'.
      apply trace_appended_refl.
    - inversion Htest as [[Hres]].
      destruct (test_forbidden_deroute _ _ _ _) as [[] e''] eqn:Hfb.
      * injection Hres as Hb He; subst e''.
        apply (extract_test_forbidden_deroute Hfb).
      * have H := IH _ _ _ _ _ Hres.
        have H' := extract_test_forbidden_deroute Hfb.
        by apply trace_appended_trans with (e' := e'').
  Qed.

  Lemma extract_forbidden_deroute:
    forall e id b e',
      forbidden_deroute fpe e id = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /forbidden_deroute => e id b e'.
    by apply extract_test_forbidden_deroutes.
  Qed.

    (** ** Common lemmas for the verification *)

    Lemma extract_evalc:
      forall e e' cond b,
        evalc e cond = (b, e')
        -> trace_appended e e'.
    Proof.
      rewrite /trace_appended => e e' cond b Heval.
      injection Heval as Heval He; subst e'.
      apply trace_appended_app_trace.
    Qed.

  Lemma extract_goto_block:
    forall e e' id,
      goto_block fpe e id = e'
      -> trace_appended e e'.
  Proof.
    rewrite /goto_block => e e' id H.
    destruct (forbidden_deroute _ _ _) as [[] e''] eqn:H'.
    - subst e''. apply (extract_forbidden_deroute H').
    - have He := (extract_forbidden_deroute H').
      apply (trace_appended_trans He). rewrite -H.
      rewrite /trace_appended.
      apply trace_appended_app_trace_rec.
      remember (change_block _ _ _) as e''' eqn:Hc; symmetry in Hc.
      by rewrite /trace_appended /is_extract_trace /extract_trace
                -(change_block_eq_trace Hc) drop_size // -app_nil_end.
  Qed.

  Lemma extract_goto_block':
    forall e id,
      trace_appended e (goto_block fpe e id).
  Proof.
    move => e id.
    remember (goto_block _ _ _) as e' eqn:Hgoto; symmetry in Hgoto.
    apply (extract_goto_block Hgoto).
  Qed.

  (** *** Next Stage *)

  Lemma extract_trace_next_stage:
    forall e e',
      extract_trace e e' = extract_trace e (next_stage e').
  Proof.
    rewrite /extract_trace /next_stage => e e'.
    by destruct e, e'.
  Qed.

  Lemma extract_next_stage:
    forall e,
      trace_appended e (next_stage e).
  Proof.
    rewrite /trace_appended /is_extract_trace
              /next_stage /extract_trace => e.
    destruct e. by rewrite //= drop_size -app_nil_end.
  Qed.

  Lemma extract_next_stage_rec:
    forall e e',
      trace_appended e e'
      -> trace_appended e (next_stage e').
  Proof.
    rewrite /trace_appended /is_extract_trace
              /next_stage /extract_trace => e e'.
    by destruct e, e'.
  Qed.

  (** *** Return block *)

  Lemma extract_trace_return_block:
    forall fp p e e',
      extract_trace e e' = extract_trace e (return_block fp e' p).
  Proof.
    rewrite /extract_trace /next_stage => e e'.
    by destruct e, e'.
  Qed.

  Lemma extract_return_block:
    forall fp e p,
      trace_appended e (return_block fp e p).
  Proof.
    rewrite /trace_appended /is_extract_trace
              /extract_trace /return_block => fp' e p.
    destruct p, e; by rewrite //= drop_size -app_nil_end.
  Qed.

  Lemma extract_update_nav_stage:
    forall e id,
      trace_appended e (update_nav_stage e id).
  Proof.
    rewrite /update_nav_stage => e id. 
    by rewrite /trace_appended /is_extract_trace
                 /extract_trace //= drop_size app_nil_r.
  Qed.

  Lemma extract_reset_stage:
    forall e,
      trace_appended e (reset_stage fpe e).
  Proof.
    rewrite /reset_stage => e.
    by apply extract_update_nav_stage.
  Qed.

  Lemma extract_next_block:
    forall e,
      trace_appended e (FPE_BS.next_block fp e).
  Proof.
    rewrite /FPE_BS.next_block => e.
    destruct (_ <? _).
    all: apply extract_goto_block'.
  Qed.

  (** ** Verification of exceptions *)

  Lemma extract_test_exception:
    forall e ex b e',
      test_exception fpe e ex = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /test_exception => e [c id code] b e' /=.
    destruct (_ =? _).

    (* Alredy in the block*)
    move => H; injection H as Hb He; subst e'.
    apply trace_appended_refl.

    destruct (EVAL_Def.eval _ _);
      move => H; injection H as Hb He; subst e'.

    (* Exception raised *)
    repeat apply extract_goto_block_app.
    apply (trace_appended_trans
      (trace_appended_app_trace e [COND c true])).
    apply (trace_appended_trans
      (trace_appended_app_trace _ (opt_c_code_to_trace code))).
    apply (trace_appended_trans (extract_goto_block' _ id)).
    apply trace_appended_refl.

    (* Exception not raised *)
    apply trace_appended_app_trace.
  Qed.

  Lemma extract_test_exceptions:
    forall exs e b e',
      test_exceptions fpe e exs = (b, e')
      -> trace_appended e e'.
  Proof.
    induction exs as [| ex exs IH];
      move => e b e' H.
    - injection H as Hb He; subst e'.
      apply trace_appended_refl.
    - rewrite //= in H.
      destruct (test_exception _ _ _) as [[] e''] eqn: Hex.
      * injection H as Hb He; subst e''.
        apply (extract_test_exception Hex).
      * apply (trace_appended_trans (extract_test_exception Hex)
                                              (IH _ _ _ H)).
  Qed.

  (** ** Verification of the semantics *)

  (** *** Nav functions *)

  Lemma extract_while_sem:
    forall e e' b params,
      FPE_BS.while_sem e params = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.while_sem => e e' b params H.

    destruct (evalc _ _) as [[] e''] eqn:Hevalc;
      apply (trace_appended_trans (extract_evalc Hevalc));
      injection H as Hb He; subst e'.

    - apply (trace_appended_trans (extract_next_stage e'')
                                          (trace_appended_app_trace _ _)).

    - by apply extract_update_nav_stage. 
  Qed.

  Lemma extract_end_while_sem:
    forall e e' b params,
      FPE_BS.end_while_sem e params = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.end_while_sem => e e' b params H.
    apply (trace_appended_trans (extract_update_nav_stage e _)
                                          (extract_while_sem H)).
  Qed.

  Lemma extract_set_sem:
    forall e e' b params,
      FPE_BS.set_sem e params = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.set_sem => e e' b params H.
    injection H as Hb He; subst e'.
    apply (trace_appended_trans (trace_appended_app_trace _ _)
                                          (extract_next_stage _)).
  Qed.

  Lemma extract_call_sem:
    forall e e' b params,
      FPE_BS.call_sem e params = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.call_sem => e e' b params H.
    destruct (get_call_loop _).
    - destruct (evalc _ _) as [[] e'' ] eqn:Hevalc;
        apply (trace_appended_trans (extract_evalc Hevalc)).
      * destruct (get_call_until params).
        + destruct (evalc e'' _) as [[] e'''] eqn:Hevalc';
            apply (trace_appended_trans (extract_evalc Hevalc'));
            injection H as Hb He; subst e'.
           ++ rewrite change_trace_trans_next_stage 
                        (evalc_change_trace_refl Hevalc)
                        change_trace_trans_next_stage
                        (evalc_change_trace_refl Hevalc').
              apply (trace_appended_trans (extract_next_stage e''')
                        (trace_appended_app_trace _ _)).
           ++ apply trace_appended_refl. 
        + injection H as Hb He; subst e'.
          apply trace_appended_refl.
      * rewrite change_trace_trans_next_stage 
                  (evalc_change_trace_refl Hevalc) in H.
        destruct (get_call_break _);
        injection H as Hb He; subst e';
        apply (trace_appended_trans (extract_next_stage e'')
                      (trace_appended_app_trace _ _)).
    - destruct (get_call_break _);
        injection H as Hb He; subst e';
        apply (trace_appended_trans (extract_next_stage e)
                      (trace_appended_app_trace _ _)).
  Qed.

  Lemma extract_deroute_sem:
    forall e e' b id,
      FPE_BS.deroute_sem fp e id = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.deroute_sem => e e' b id H.
    injection H as Hb He; subst e'.
    apply (trace_appended_trans (extract_next_stage e)).
    apply (trace_appended_trans
      (trace_appended_app_trace _  [init_stage])).
    apply (extract_goto_block' _ id).
  Qed.

  Lemma extract_return_sem:
    forall e e' b params,
      FPE_BS.return_sem fp e params = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.return_sem => e e' b params H.
    injection H as Hb He; subst e'.
    by apply (trace_appended_trans (extract_return_block _ e params)
                                          (trace_appended_app_trace _ _)).
  Qed.

  Lemma extract_nav_init_sem:
    forall e e' b nav_mode,
      FPE_BS.nav_init_sem e nav_mode = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.nav_init_sem => e e' b nav_mode H.
    injection H as Hb He; subst e'.
    by apply (trace_appended_trans (extract_next_stage e)
                                          (trace_appended_app_trace _ _)).
  Qed.

  Lemma extract_nav_code_sem:
    forall e e' b nav_mode until,
    FPE_BS.nav_code_sem e nav_mode until = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.nav_code_sem => e e' b nav_mode until H.

    remember (gen_fp_nav_code_sem _) as code.
    apply (trace_appended_trans (trace_appended_app_trace e code)).

    destruct until as [cond |].
    - destruct (evalc _ _) as [[] e'' ] eqn:Hevalc;
        apply (trace_appended_trans (extract_evalc Hevalc));
        injection H as Hb He; subst e'.
      * apply (trace_appended_trans
          (trace_appended_app_trace e''
            [post_call_nav_sem nav_mode; init_stage])).
        apply extract_next_stage.
      * apply trace_appended_app_trace.
    - injection H as Hb He; subst e'.
      apply trace_appended_app_trace.
  Qed.

  Lemma extract_nav_sem:
    forall e e' b nav_mode until,
      FPE_BS.nav_sem e nav_mode until = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.nav_sem => e e' b nav_mode until H.

    remember (pre_call_nav_sem _) as pre.
    apply (trace_appended_trans (trace_appended_app_trace e [::pre])).

    remember (post_call_nav_sem _) as post.

    destruct (nav_cond_sem _) as [cond |].
    - destruct (evalc _ _) as [[] e'' ] eqn:Hevalc;
        apply (trace_appended_trans (extract_evalc Hevalc)).
      * injection H as Hb He; subst e'.
        apply (trace_appended_trans
          (trace_appended_app_trace e''
            [post; last_wp_exec nav_mode; init_stage])
          (extract_next_stage _)).
      * apply (extract_nav_code_sem H).
    - apply (extract_nav_code_sem H).
  Qed.

  Lemma extract_default_sem:
    forall e e' b,
      FPE_BS.default_sem fp e = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.default_sem => e e' b H.
    injection H as Hb He; subst e'.
    apply (trace_appended_trans (extract_reset_stage e)
                                          (extract_next_block _)).
  Qed.

  Lemma extract_run_stage:
    forall e e' b, 
      FPE_BS.run_stage fp e = (b, e')
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.run_stage => e e' b H.
    destruct (get_current_stage _ e).
    - by apply (extract_while_sem H).
    - by apply (extract_end_while_sem H).
    - by apply (extract_set_sem H).
    - by apply (extract_call_sem H).
    - destruct params.
      by apply (extract_deroute_sem H).
    - by apply (extract_return_sem H).
    - by apply (extract_nav_init_sem H).
    - by apply (extract_nav_sem H).
    - by apply (extract_default_sem H).
  Qed.

  Lemma extract_run_step:
    forall e e', 
      FPE_BS.run_step fp e = e'
      -> trace_appended e e'.
  Proof.
    have H:=  (FPE_BS.run_step_ind fp). move => e. apply H; clear H.
    - move => e' e'' Hr Hind e''' Hs.
      apply (trace_appended_trans (extract_run_stage Hr)).
      apply (Hind _ Hs).
    - move => e' e'' Hs e''' He; subst e'''.
      apply (extract_run_stage Hs).
  Qed.

  Theorem extract_step:
    forall e e',
      FPE_BS.step fp e = e'
      -> trace_appended e e'.
  Proof.
    rewrite /FPE_BS.step / FPE_BS.Common_Sem.exception => e e' Hr.

    destruct (test_exceptions _ _ (FP_E_WF.Common.get_fp_exceptions _))
      as [b e1] eqn:Hgex.
    have Hg := extract_test_exceptions Hgex.
    destruct b.

    (* Global exception raised *)
    inversion Hr; by subst e1.

    apply (trace_appended_trans Hg).

    remember (FP_E_WF.Common.get_code_block_pre_call _ ) as t.
    destruct (test_exceptions _ (app_trace e1 t) _)
      as [b e1'] eqn:Hlex.
    have Hl := extract_test_exceptions Hlex.
    apply (trace_appended_trans (trace_appended_app_trace _ t)).
    destruct b.

    (* Local exception raised *)
    inversion Hr; by subst e1'.

    remember (FPE_BS.run_step fp e1') as e1'' eqn:Hrun; symmetry in Hrun.
    rewrite -Hr.
    apply trace_appended_app_trace_rec.
    apply (trace_appended_trans Hl (extract_run_step Hrun)).
  Qed.


  (** * Properties about match trace *)
  Lemma match_event_eq:
    forall e1 e2 e2',
    MATCH_TRACE.match_event e1 e2
    -> MATCH_TRACE.match_event e1 e2'
    -> e2 = e2'.
  Proof.
    move => e1 e2 e2' Heq Heq'.
    inversion Heq as [cond r r' Hc He| code Hc He].
    - subst r' e1 e2.
      inversion Heq'. by subst r.
    - subst e1 e2. by inversion Heq'.
  Qed. 

  Lemma trace_appended_clight_gen:
    forall t1 t1' t2 t2',
      t1 ~t~ t2
      -> t1' ~t~ t2'
      -> t1' = t1 ++ drop (Datatypes.length t1) t1'
      -> t2' = t2 ++ drop (Datatypes.length t2) t2'.
  Proof.
    induction t1 as [|e1 t1 IH];
      move => t1' t2 t2' Ht Ht' H.
    - inversion Ht. by rewrite drop_0.
    - inversion Ht as [|t1_t t2_t Ht1 |e' t1_t e2 t2_t Heq Ht1].
      * subst e1 t1_t t2_t. destruct t1' as [|e1' t1'].
        + by inversion H.
        + inversion H as [[He Hd]]. subst e1'.
           inversion Ht' as [|t1'_t t2'_t H'| e1' t1'_t e2' t2'_t H'].
           ** apply (IH _ _ _ Ht1 H' Hd).
           ** by inversion H'.
      * subst t2 e' t1_t. destruct t1' as [|e' t1'].
        + by inversion H.
        + inversion H as [[He Hd]]. subst e'.
          destruct t2' as [|e' t2'].
          ** inversion Ht' as [|t t' Ht'' | ]. subst t t' e1.
              by inversion Heq.
          ** inversion Ht' as [| |e1' t1'' e2' t2'' Heq' H'];
                try subst e1; try by inversion Heq.
              subst  t1'' e2' t2''.
              have He := match_event_eq Heq Heq'.
              subst e'. rewrite /=. f_equal.
              by apply (IH t1' _ _ Ht1).
  Qed.

  Lemma trace_appended_clight:
    forall ge e1 e1' e2 e2',
      e1 ~cenv~ (ge, e2)
      -> e1' ~cenv~ (ge, e2')
      -> trace_appended (` e1) (` e1')
      -> C_EXTRACT.trace_appended e2 e2'.
  Proof.
    rewrite /trace_appended /is_extract_trace /extract_trace 
             /C_EXTRACT.trace_appended /C_EXTRACT.is_extract_trace
             /C_EXTRACT.extract_trace
      => ge [e1 He1] [e1' He1'] [m2 t2] [m2' t2'] [He Ht] [He' Ht'] H.
    by apply (trace_appended_clight_gen Ht Ht').
  Qed.

  Remark fp_trace_to_trace_skip:
    forall e1 t1,
      fp_trace_to_trace (e1 :: t1) = []
      -> e1 = SKIP.
  Proof.
    move => e1 t1 H. by destruct e1.
  Qed.

  Remark event_skip:
    forall e, {e = SKIP} + {e <> SKIP}.
  Proof.
   destruct e; try by right. by left.
  Qed.

  Remark fp_trace_to_trace_nskip:
    forall e1 t1 e2 t2,
      e1 <> SKIP
      -> fp_trace_to_trace (e1 :: t1) = e2 :: t2
      -> fp_trace_to_trace t1 = t2.
  Proof.
    move => e1 t1 e2 t2 Hne H. destruct e1; try by contradiction.
    all: by inversion H.
  Qed.

  Lemma extract_trace_clight_gen:
    forall t1 t1' t2 t2',
      t1' = t1 ++ drop (Datatypes.length t1) t1'
      -> t2' = t2 ++ drop (Datatypes.length t2) t2'
      -> fp_trace_to_trace t1 = t2
      -> fp_trace_to_trace t1' = t2'
      -> drop (Datatypes.length t2) t2'
          = fp_trace_to_trace (drop (Datatypes.length t1) t1').
  Proof.
    induction t1 as [|e1 t1 IH];
      move => t1' t2 t2' H1 H2 H H'.
    - by rewrite -H ?drop_0 -H'.
    - destruct t1' as [|e1' t1'].
      * by rewrite -H'.
      * inversion H1 as [[He H1']]. subst e1'.
        destruct (event_skip e1) as [Hs | Hnes];
          try subst e1; rewrite /= drop_size_cat ?size_cat //;
          try by apply IH.
        destruct t2 as [|e2 t2].
        rewrite /= in H.
        + have H_tmp := fp_trace_to_trace_skip H. subst e1.
          rewrite  drop_0 -H' H1' /= drop_size_cat ?size_cat //
                   trace_app H //.
        + destruct t2' as [|e2' t2' ]; try by inversion H2.
          inversion H2 as [[He' H2']]. subst e2'.
          rewrite /= drop_size_cat ?size_cat //.
          apply IH => //.
          all: by apply (@fp_trace_to_trace_nskip e1 _ e2).
  Qed.

  Lemma extract_trace_clight:
    forall ge e1 e1' e2 e2',
    e1 ~cenv~ (ge, e2)
    -> e1' ~cenv~ (ge, e2')
    -> trace_appended (` e1) (` e1')
    ->   C_EXTRACT.extract_trace e2 e2' =
          fp_trace_to_trace (extract_trace (` e1) (` e1')).
  Proof.
    rewrite /C_EXTRACT.extract_trace /extract_trace
      => ge [e1 He1] [e1' He1'] [m2 t2] [m2' t2'] He He' Ht.
    have Ht' := trace_appended_clight He He' Ht.
    have H := trace_generated_eq He.
    have H' := trace_generated_eq He'.
    by apply extract_trace_clight_gen.
  Qed.

  End FLIGHT_PLAN.

End EXTRACT_TRACE_FPENV.
