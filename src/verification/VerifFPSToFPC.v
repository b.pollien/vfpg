From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

From compcert Require Import Integers AST Ctypes
                        Cop Clight Clightdefs Maps
                        Events Memory Values.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas ClightLemmas
                        CommonLemmasNat8 CommonFPVerification
                        BasicTypes
                        FPNavigationMode FPNavigationModeSem
                        FPNavigationModeGen FPNavigationModeVerification
                        FlightPlanGeneric FlightPlanExtended
                        FlightPlanSized
                        CommonFPDefinition ClightGeneration
                        GvarsVerification
                        FPEnvironmentGeneric FPEnvironmentDef
                        FPEnvironmentClight
                        FPBigStepDef FPBigStepClight 
                        Generator GeneratorProperties 
                        FPSizeVerification.

Local Open Scope nat_scope.

Set Implicit Arguments.

(** Verification of semantics preservation between FP_E and Clight *)

(** * Verification of the semantics preservation *)

Module FPS_TO_C_VERIF (EVAL_Def: EVAL_ENV)
                                 (ENVS_Def: ENVS_DEF EVAL_Def)
                                 (BS_Def: FP_BS_DEF EVAL_Def ENVS_Def).

  Module COMMON := COMMON_FP_VERIF EVAL_Def ENVS_Def BS_Def.
  Import COMMON C_ENV BS_Def FPS_BS FPS_ENV FPE_BS
          FPE_ENV FPE_ENV.Def Common_Sem
          COMMON.EXTRACT_FPENV MATCH_FPS_C.

  (** CLight Step *)
  Module C_BS := C_BIGSTEP.

Section FLIGHT_PLAN.

  (** Definition of the flight plan being executed *)
  Variable fps: flight_plan_sized.
  Definition fp := ` fps.
  Definition Hsize := proj2_sig fps.

  Definition fpe   := ` fp.
  Definition Hwf := proj2_sig fp.

  Remark eq_fps_fp: (` (` fps)) = fpe. by []. Qed.

  (** Correct list of global variables defined by the user *)
  Variable gvars: cgvars.

  (** Global program generated *)
  Definition prog := generate_complete_context fpe gvars.

  (** Definition of the global environment *)
  Definition ge := globalenv prog.

  (** Function to execute *)
  Definition f := gen_fp_auto_nav fpe.

  (** Local environment during the execution that is correct*)
  Definition ev := empty_env.
  Definition Hcev := correct_empty_env.

  (** ** Management of exception *)
  Lemma no_error_same_block:
    forall exs fpe e e',
    test_exceptions fpe e exs = (false, e')
    -> get_nav_block e = get_nav_block e'.
  Proof.
    induction exs as [|ex exs IHexs];
   move  => fpe e e' H.
    - injection H as He. by rewrite He.
    - rewrite /test_exceptions
                /test_exception in H.
      destruct (get_expt_block_id ex =? _).
      * rewrite //= in H. apply IHexs with (fpe := fpe).
        destruct (test_exceptions fpe e exs)
          as [b' e1'] eqn: H'.
        rewrite /test_exceptions /test_exception in H'. 
        rewrite H' in H. by inversion H.
      * destruct (evalc e _) as [[] e1] eqn:Heval;rewrite //= in H.
        + destruct (test_exceptions fpe e1 exs)
              as [b e''] eqn: Hex.
            have Hex' := Hex.
            rewrite /test_exceptions in Hex'.
            rewrite Hex' in H. clear Hex'. injection H as Hb He.
            subst e' b. apply IHexs in Hex.
            apply evalc_unchanged_position in Heval.
            rewrite -Hex. destruct e, e1. 
            by rewrite (unchanged_nav_block Heval).
  Qed.

  Definition bool_return_state (b: bool) (k: cont) (e: env)(le: temp_env)
                                    (m: m_env): state :=
    if b then (Returnstate Vundef (call_cont k) m)
    else (State f Sskip k e le m).

  (** Blocks continuation *)
  Definition blocks_cont_e (e: fp_env) (k: cont): cont :=
    blocks_cont fp (get_nav_block e) k.

  Definition block_cont_e (e: fp_env) (k: cont): cont :=
    block_cont fp (get_nav_block e) k.

  Definition stage_cont_e (e: fp_env) (k: cont): cont :=
    stage_cont fp (get_nav_block e) k.

  Remark blocks_cont_e_eq:
    forall e k,
    blocks_cont fp (get_nav_block e) k = blocks_cont_e e k.
  Proof. by []. Qed.

  Remark block_cont_eq:
    forall e e' k, 
    get_nav_block e = get_nav_block e'
    -> block_cont_e e k = block_cont_e e' k.
  Proof.
    rewrite /block_cont /block_cont_e => e e' k Hc. by rewrite Hc.
  Qed.

  Remark stage_cont_eq:
    forall e e' k, 
     get_nav_block e = get_nav_block e'
     -> stage_cont_e e k = stage_cont_e e' k.
  Proof.
    rewrite /stage_cont /stage_cont_e => e e' k Hc. by rewrite Hc.
  Qed.

  Remark stage_cont_eq':
    forall e e' k cond b, 
    evalc e cond = (b, e')
    -> stage_cont_e e k = stage_cont_e e' k.
  Proof.
    move => e e' k cond b H.
    apply (stage_cont_eq _
             (unchanged_nav_block
            (evalc_unchanged_position H))).
  Qed.

  Remark stage_cont_app_trace:
    forall e t k,
      stage_cont_e e k = stage_cont_e (app_trace e t) k.
  Proof.
    move => e t k.
    apply (stage_cont_eq _
            (unchanged_nav_block
            (app_trace_unchanged_position _ _))).
  Qed.

  (** Get the list of remaining stage to execute *)
  Definition remaining_stages (e: fp_env) :=
    let stages :=
      FP_E_WF.get_block_stages
        (FP_E.get_block fpe (get_nav_block e)) in
    let default :=
      FP_E.default_stage fpe (get_nav_block e) in
    let stages' :=
      drop (get_nav_stage e)
        (take (Datatypes.length stages).-1 stages) in
    stages' ++ [:: default].

  Lemma remaining_stages_nextstage:
    forall e stage stages default,
      remaining_stages e = (stage:: stages) ++ [:: default]
      -> (remaining_stages (next_stage e)) = stages ++ [:: default].
  Proof.
    rewrite /remaining_stages
      => [[[idb ids lidb lids] t] stage stages default]; rewrite //= => H.
    rewrite -cat_cons in H. apply app_split_eq in H. destruct H as [Hs Hd].
    apply app_split_eq; split; try by [].
    rewrite addn1. apply (cons_drop_nth Hs).
  Qed.

  Lemma evalc_remaining_stages:
    forall e cond b e',
      evalc e cond = (b, e')
      -> remaining_stages e = remaining_stages e'.
  Proof.
    rewrite /evalc /remaining_stages
      => [[[idb ids lidb lids] t] cond b [[idb' ids' lidb' lids'] t']];
          rewrite //= => H. injection H; intros; by subst.
  Qed.

  Lemma change_trace_remaining_stages:
    forall e t,
      remaining_stages (change_trace e t) = remaining_stages e.
  Proof.
    rewrite /change_trace /remaining_stages
      => [[[idb ids lidb lids] t']  t];
          rewrite //=.
  Qed.

  Lemma app_trace_remaining_stages:
    forall e t,
      remaining_stages (app_trace e t) = remaining_stages e.
  Proof.
    rewrite /app_trace /remaining_stages
      => [[[idb ids lidb lids] t']  t];
          rewrite //=.
  Qed.

  Definition bool_continue (e: fp_env)  (b: bool): statement :=
    if b then
      seq_of_labeled_statement
          (gen_fp_stages (remaining_stages e))
    else Sbreak.

  Lemma eval_expr_eq_block_ex:
    forall e le e1 b e2 ex,
    fp_env_on_8 e1
    -> correct_excpt fpe ex
    -> (get_expt_block_id ex =? get_nav_block e1) = b
    -> eval_expr ge e (update_le_NavBlock e1 le) e2
                           (gen_neq_expr tmpNavBlock
                                 (gen_uint8_const (get_expt_block_id ex))) 
                           (create_bool_val (~~b)).
  Proof.
    move => e le e1 b e2 ex He8 Hex8 Heq.

    have Hb8: is_nat8 (get_nav_block e1)
      by apply (is_nav_block8 Hsize He8).
    rewrite /correct_excpt in Hex8.
    apply user_id_is_nat_8 in Hex8.

    econstructor. apply get_update_le_NavBlock.
    econstructor.

    rewrite //= /sem_cmp /sem_binarith ?sem_cast8 /create_val //=.
    destruct b.
    - apply Nat.eqb_eq in Heq; rewrite Heq. by rewrite Int.eq_true.
    - apply Nat.eqb_neq in Heq.
      have H := (Int.eq_spec (int_of_nat (get_nav_block e1))
                                    (int_of_nat (get_expt_block_id ex))).
      destruct (Int.eq _ _) eqn:Heq'.
      * apply int_of_nat_eq8 in H; try by []. by rewrite H in Heq.
      * by []. 
  Qed.

  Lemma eq_exec_exception:
    forall ex e1 e1' e2 b le ,
    e1 ~cenv~ (ge, e2)
    -> correct_excpt fpe ex
    -> test_exception fpe (` e1) ex = (b, ` e1')
    -> let t := fp_trace_to_trace (extract_trace (` e1) (` e1')) in
        exists e2' le', (forall k,
          Smallstep.star step2 ge
            (State f (gen_exception ex) k ev le (e2.m))
          t (bool_return_state b k ev le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => ex e1 e1'_f e2 b le He Hex8 Hr t.
    have He8 := proj2_sig e1.

    rewrite //= /test_exception in Hr;
    destruct (get_expt_block_id ex =? get_nav_block (` e1))
      eqn: Heq.

    (* Already in the block -> stop *)
    { injection Hr as Hb He1'_f. apply env8_proj in He1'_f.
      subst b e1'_f.

      do 2 eexists; split; try intro.
      step_seq. apply (step_get_NavBlock le Hcev He8 (proj1 He)).
      step_skip_seq.

      step_seq. star_step step_ifthenelse.
      apply (eval_expr_eq_block_ex ev le (e2.m) He8 Hex8 Heq). by [].

      rewrite false_eq_0 //=.
      star_step step_set. econstructor. econstructor. by [].
      step_skip_seq. star_step step_ifthenelse.
      econstructor. apply PTree.gss. by []. rewrite false_eq_0 //=.
      step_refl. all: try by []. rewrite extract_trace_refl.
      step_refl. by []. } 

    (* Not in the block -> continue *)
    { destruct (evalc8 e1 (get_expt_cond ex))
        as [[] [e1' Heval]];
      rewrite Heval /= in Hr.
      have He' := evalc_preserve_match Heval He.
      have He8' := evalc_cond8 He8 Heval.
      remember (app_trace _ _) as e1_app eqn:Happ.
      destruct (app_trace_preserve_match Happ He')
        as [e1_app' [Heq_app He'']]; subst e1_app.
      have Hex: trace_appended (` e1') (` e1_app') by
        rewrite -Heq_app; apply trace_appended_app_trace.

      (* Exception raised -> change block *)
      { destruct (goto_block8 fps e1_app' (get_expt_block_id ex))
          as [e1'' Hgoto]; rewrite Heq_app Hgoto in Hr;
        injection Hr as Hb He1'_f; apply env8_proj in He1'_f;
        subst b e1'_f.
        apply user_id_is_nat_8 in Hex8 as Hex8'.
        remember (PTree.set tmp_cond (Vint Int.one)
                           (update_le_NavBlock (`e1) le)) as le'.
        destruct (step_goto_block le' He'' Hex8' Hgoto)
          as [e2' [le'' [Hgoto' He2']]]; subst le'.

        exists e2', le''; split; try intro.
        step_seq. apply (step_get_NavBlock le Hcev He8 (proj1 He)).
        step_skip_seq. 

        step_seq. star_step step_ifthenelse.

        apply (eval_expr_eq_block_ex ev le (e2.m) He8 Hex8 Heq). by [].

        rewrite true_ne_0 //=. star_step (eq_exec_set_cond He Heval).
        apply step_set. econstructor. econstructor. econstructor.
        all: try by [].
        step_skip_seq. step_refl. all: try by [].
        star_step step_ifthenelse.
        econstructor. apply PTree.gss. by []. rewrite true_ne_0 //=.
        step_seq. eapply Smallstep.star_trans
          with (t1 := exec_to_trace (get_expt_exec ex)).

        { destruct (get_expt_exec ex) as [exec|].
          - apply exec_arbitrary_C_code.
          - constructor.
        }

        step_skip_seq. step_seq. apply Hgoto'. step_skip_seq.
        step_refl. all: try by [].
        star_step step_return_0. by [].
        step_refl. all: try by [].
        simpl_trace; rewrite /t -?trace_app
              (trace_appended_app (extract_evalc Heval)).
        rewrite (trace_appended_app Hex).
        by rewrite -Heq_app extract_app_trace ?trace_app
                  exec_to_trace_fp_trace.

        all: try apply (trace_appended_trans Hex).
        all: apply (trace_appended_trans (extract_goto_block Hgoto)
                                      (trace_appended_refl (` e1''))). }

      (* Exception not raised -> return false *)
      { injection Hr as Hb He1'_f; apply env8_proj in He1'_f;
         subst b e1'_f.

        have He' := (evalc_preserve_match Heval He).
        remember (app_ctrace _ _) as e2'.

        exists e2'; eexists; split; try intro.
        step_seq. apply (step_get_NavBlock le Hcev He8 (proj1 He)).
        step_skip_seq.

        step_seq. star_step step_ifthenelse.
        apply (eval_expr_eq_block_ex ev le (e2.m) He8 Hex8 Heq). by [].

        rewrite true_ne_0 //=. star_step (eq_exec_set_cond He Heval).
        apply step_set. econstructor. econstructor. econstructor.
        all: try by [].
        step_skip_seq. star_step step_ifthenelse.
        econstructor. apply PTree.gss. by []. rewrite false_eq_0 //=.
        step_refl. all: try by [].
        rewrite Heqe2' /=. step_refl. simpl_trace. } }
  Qed.

  Lemma eq_exec_exceptions:
    forall exs e1 e1' e2 b le ,
    e1 ~cenv~ (ge, e2)
    -> Forall (correct_excpt fpe) exs
    -> test_exceptions fpe (` e1) exs = (b, ` e1')
    -> let t := fp_trace_to_trace (extract_trace (` e1) (` e1')) in
        exists e2' le', (forall k,
          Smallstep.star step2 ge
                    (State f (gen_exceptions exs) k ev le (e2.m))
                  t (bool_return_state b k ev le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    induction exs as [|ex exs' IHex];
      move => e1 e1' e2 b le He Hexs8 Hex t.

    (** There is no exception *)
    exists e2, le. injection Hex as Hb He'.
    apply env8_proj in He'. subst b e1'.
    split; try by []. intro. rewrite /t extract_trace_refl. step_refl.

    (** There is exceptions *)
    destruct (test_exception8 fps e1 ex) as [b' [e1'' Hex']];
    rewrite /= Hex' /= in Hex.

    destruct (eq_exec_exception le He (Forall_inv Hexs8) Hex')
      as [e2' [le' [Hex_c He2']]].
    destruct b'.
 
    (** Exception raised -> stop *)
    exists e2', le'; split; injection Hex as Hb He1'';
      apply env8_proj in He1''; subst b e1'';
      try by []; try intro.
    step_seq. apply Hex_c. step_refl. all: try by []. by simpl_trace. 

    (** Exception not raised -> continue *)
    destruct (test_exceptions8 fps e1'' exs') as [b'' [e1''' Hex'']]; 
    rewrite /= Hex'' in Hex; injection Hex as Hb He1''';
    apply env8_proj in He1'''; subst b'' e1'''.

    destruct (IHex _ _ _ _ le' He2' (Forall_inv_tail Hexs8) Hex'')
      as [e2'' [le'' [Hex_c' He2'']]].

    exists e2'', le''; split; try by []; intro.
    step_seq. apply Hex_c. step_skip_seq.
    apply Hex_c'. all: try by [].
    simpl_trace; rewrite /t -trace_appended_app //.
    - apply (extract_test_exception Hex').
    - apply (extract_test_exceptions Hex'').
  Qed.

  Lemma eq_exec_blocks_lexcpt_raised:
    forall e1 e1' e2 le ,
      e1 ~cenv~ (ge, e2)
      -> test_exceptions fpe (` e1)
              (get_local_exceptions fpe (` e1)) = (true,` e1')
      -> let pre := (FP_E_WF.get_code_block_pre_call
                         (get_current_block fpe (` e1))) in
          let t := fp_trace_to_trace (pre
            ++ (extract_trace (` e1) (` e1'))) in
          exists e2', (forall k,
            Smallstep.star step2 ge
            (State f (gen_fp_block (FP_E.get_block fpe
                                (get_nav_block (` e1))))
                                k ev le (e2.m))
          t (Returnstate Vundef (call_cont k) (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 le He Ht pre t.
    have He8 := proj2_sig e1.

    have Hid1 := nav_block8 He8. to_nat Hid1.
    have Hexs8 := get_correct_lexcpts
                (get_well_sized_blocks Hsize (get_nav_block (`e1))).

    (* Get the execution of local exceptions*)
    destruct (eq_exec_exceptions le He Hexs8 Ht)
      as [e2'' [le' [Hex He2'']]].

    do 2 eexists; try intro.

    step_seq. remember (FP_E.get_block_pre_call _) as pre'.
    eapply Smallstep.star_trans
      with (t1 := exec_to_trace pre').

    { destruct pre' as [c_pre'|].
      - apply exec_arbitrary_C_code.
      - constructor.
    } step_skip_seq.

    step_seq. apply Hex. step_refl.
    all: try by []. by subst pre'.
    step_refl.

    have H: forall b,
      FP_E_WF.Def.get_block_pre_call b = FP_E.get_block_pre_call b.
    { by destruct b. }

    by simpl_trace; rewrite /t /pre ?trace_app
      /FP_E_WF.get_code_block_pre_call
      /get_current_block
      H exec_to_trace_fp_trace.
    by [].
  Qed.

  (** Correct block when WHILE stage *)
  Lemma correct_block_id_while:
    forall e1 id params,
      get_current_stage fpe e1 = FP_E.WHILE id params
      -> (get_nav_block e1 < FP_E.get_nb_blocks fpe - 1)%coq_nat.
  Proof.
    move => e1 id params H.
    destruct (le_lt_dec (FP_E.get_nb_blocks fpe - 1) (get_nav_block e1))
      as [Hge | Hlt] => //.
    rewrite /get_current_stage /FP_E.get_stage
            /FP_E.get_stages /FP_E.get_block
            (nth_overflow (FP_E.get_fp_blocks fpe)) /= in H.
    destruct (nav_stage (get_state e1)) as [|[|[|n]]];
      inversion H.

    rewrite /FP_E.get_nb_blocks in Hge. to_nat Hge. ssrlia.
  Qed.

  (** Execution of Generic while *)
  Lemma eq_exec_while_gen:
    forall e1 e1' e2 le b id params,
      let idb := get_nav_block (` e1) in
      e1 ~cenv~ (ge, e2)
      -> while_sem (` e1) params = (b, ` e1')
      -> get_current_stage fpe (` e1) = FP_E.WHILE id params
      -> exists e2' le', (forall stages k, is_call_cont k
          -> Smallstep.star step2 ge
            (State f Sskip
            (Kseq
                (Sifthenelse
                    (gen_neg_bool (parse_c_code_cond
                                  (FP_E.get_while_cond params)))
                    (Sgoto (get_end_while_label params))
                      nextStageAndBreak)
                (Kseq
                  (seq_of_labeled_statement
                      (gen_fp_stages stages))
                        (stage_cont fp idb k))) ev le (e2.m))
            (fp_trace_to_trace (extract_trace (` e1) (` e1')))
            (State f (bool_continue (` e1') b) (stage_cont_e (` e1) k)
                ev le' (e2'.m)))
          /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id params idb He Hr Hc.
    have He8 := proj2_sig e1.

    (* Evaluation of the condition *)
    destruct (evalc8 e1 (FP_E.get_while_cond params)) 
      as [b'  [e1' Heval]].
    rewrite /while_sem Heval in Hr.
    have He' := evalc_preserve_match Heval He.

    destruct b'.

    (** Condition true -> enter in the loop *)
    { (** Execution of the NextStage *)
      destruct (next_stage8 fps e1') as [e1'' Hn].
        by rewrite -(evalc_get_current_stage fpe Heval) Hc. 
      destruct (step_nextstage le He' Hcev Hn)
        as [e2' [Hnext He2']].

      (** Execute code *)
      remember [:: _] as init.
      destruct (match_app_trace init He2')
        as [e1''' [Happ He'']].

      rewrite Hn -Happ in Hr; injection Hr as Hb Heq;
        apply env8_proj in Heq; subst b e1'_f.

      do 2 eexists; split. move => stages k Hk.

      step_skip_seq.

      star_step (@eval_c_code_ifthenelse_neg _ gvars f _ _ _ _ _ He Heval).
      eapply step_ifthenelse.
      apply eval_expr_neg_const_bool. by [].

      rewrite false_eq_0 //= /nextStageAndBreak.
      step_seq. step_trans. apply Hnext.
      step_skip_seq. step_refl.

      all: try by [].

      econstructor. econstructor. 
      rewrite (app_ctrace_mem e2' (fp_trace_to_trace init)). step_refl.

      all: try by [].

      simpl_trace. rewrite
         (trace_appended_app (extract_evalc Heval)
                             (trace_appended_refl (` e1')))
         Happ -Hn extract_app_trace_gen
        ?(trace_appended_app (extract_evalc Heval)
                             (extract_next_stage (` e1')))
        -?extract_trace_next_stage
        ?extract_trace_refl //.
      eapply (trace_appended_trans
        (extract_evalc Heval) (extract_next_stage _)). }

    (** Condition false -> exit the loop *)
    destruct ((FP_E_WF.get_wf_while Hwf) _ _ _ _ Hc)
      as [Hp [Hlt [Hew [Hid Hb']]]].

    have Hew8 := FPS_BS.get_end_while_params_nat8 fps He8 Hc.
    destruct (step_set_NavStage le Hcev Hew8 He')
      as [e1'' [e2'' [Heqe1'' [Hset He'']]]].

    rewrite -Heqe1'' in Hr; injection Hr as Hb Heq; apply env8_proj in Heq;
      subst b e1'_f.

    exists e2'', le; split => //. move => stages k Hk.

    step_skip_seq.

    star_step (@eval_c_code_ifthenelse_neg _ gvars f _ _ _ _ _ He Heval).
    eapply step_ifthenelse.
    apply eval_expr_neg_const_bool. by [].

    rewrite true_ne_0 //=.
    step_trans. eapply Smallstep.star_one.
    eapply step_goto.

    apply correct_block_id_while in Hc.

    apply (find_end_while_label Hwf Hsize Hc Hew).
    apply is_call_cont_call_cont.
    step_trans. apply Hset.
    step_skip_seq. rewrite /remaining_stages.

    symmetry in Heqe1''.
    rewrite (update_stage_change_stage Heqe1'')
              ?addn1 /stage_cont_e.

    remember (drop _ _) as stages'.
    remember (drop _ (take _ _)) as stages''.
    have Htp: stages' = stages''
                   ++ [:: FP_E.default_stage fpe
                           (get_nav_block (` e1''))].
    { apply evalc_unchanged_position in Heval.
      rewrite Heqstages''
                -(update_stage_nc_block Heqe1'')
                -(unchanged_nav_block Heval) -Nat.sub_1_r.
      remember (get_nav_block (` e1)) as idb'.
      have H := FP_E_WF.wf_stages_plus_default idb' Hwf.
      rewrite /FP_E.get_block in H. 
      rewrite  H drop_cat_le in Heqstages'.
      by rewrite -Heqidb' Heqstages'. rewrite size_takel.
      replace ((Datatypes.length _) - 1)
        with (FP_E.default_stage_id fpe idb'); try by [].
      ssrlia. apply (FP_E_WF.stage_wf_lt_default Hwf).
      - by rewrite Hew.
      - rewrite size_eq_length; ssrlia. }

    rewrite -Htp //= (call_cont_is_call_cont Hk) //=.

    step_refl.

    all: try by [].

    simpl_trace. by rewrite Heqe1''.
   Qed.

  (* Execution of WHILE*)
  Lemma eq_exec_while:
    forall e1 e1' e2 le b id params stages default,
      e1 ~cenv~ (ge, e2)
      -> while_sem (` e1) params = (b,` e1')
      -> get_current_stage fpe (` e1) = FP_E.WHILE id params
      -> remaining_stages (` e1) 
          = ((FP_E.WHILE id params) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
            (State f (gen_fp_stage (FP_E.WHILE id params))
            (Kseq
              (seq_of_labeled_statement
                (gen_fp_stages (stages ++ [:: default]))) 
                (stage_cont_e (` e1) k)) ev le (e2.m))
            (fp_trace_to_trace (extract_trace (` e1) (`e1')))
            (State f (bool_continue (` e1') b)
              (stage_cont_e (` e1) k) ev le' (e2'.m)))
          /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 le b id params stages default 
                He Hr Hc Hremain.
    have He8 := proj2_sig e1.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He)
      as [e2' [Hset He2']].

    rewrite //= /gen_fp_while //=.

    destruct ((FP_E_WF.get_wf_while Hwf) _ _ _ _ Hc)
        as [Hp [Hlt [Hew [Hid Hb]]]]; subst id.

    destruct (eq_exec_while_gen le He2' Hr Hc)
      as [e2'' [le' [Hwhile He2'']]].

    exists e2'', le'; split.

    move => k HK.
    step_seq. star_step step_label.
    apply Hset. by [].

    apply Hwhile.

    all: try  by [].
  Qed.

  (** Correct block when END_WHILE stage *)
  Lemma correct_block_id_ewhile:
    forall e1 id params body,
      get_current_stage fpe e1 = FP_E.END_WHILE id params body
      -> (get_nav_block e1 < FP_E.get_nb_blocks fpe - 1)%coq_nat.
  Proof.
    move => e1 id params body H.
    destruct (le_lt_dec (FP_E.get_nb_blocks fpe - 1) (get_nav_block e1))
      as [Hge | Hlt] => //.
    rewrite /get_current_stage /FP_E.get_stage
            /FP_E.get_stages /FP_E.get_block
            (nth_overflow (FP_E.get_fp_blocks fpe)) /= in H.
    destruct (nav_stage (get_state e1)) as [|[|[|n]]];
      inversion H.

    rewrite /FP_E.get_nb_blocks in Hge. to_nat Hge. ssrlia.
  Qed.

  (* Execution of END_WHILE*)
  Lemma eq_exec_end_while:
    forall e1 e1' e2 le b id params body stages default,
      e1 ~cenv~ (ge, e2)
      -> end_while_sem (` e1) params = (b, ` e1')
      -> get_current_stage fpe (` e1)
              = FP_E.END_WHILE id params body
      -> remaining_stages (` e1)
          = ((FP_E.END_WHILE id params body) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
            (State f (gen_fp_stage (FP_E.END_WHILE id params body))
            (Kseq
              (seq_of_labeled_statement
                  (gen_fp_stages (stages ++ [:: default])))
              (stage_cont_e (` e1) k)) ev le (e2.m))
              (fp_trace_to_trace (extract_trace (` e1) (`e1')))
            (State f (bool_continue (`e1') b)
                      (stage_cont_e (`e1) k) ev le' (e2'.m)))
          /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id params body stages default
               He Hr Hc Hremain.
    rewrite /end_while_sem //= in Hr.
    have He8 := proj2_sig e1.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He)
      as [e2' [Hset He2']].

    have Hw8 := FPS_BS.get_while_params_nat8 fps He8 Hc.
    destruct (step_set_NavStage le Hcev Hw8 He2')
      as [e1'' [e2'' [Heqe1'' [Hset' He2'']]]].
      symmetry in Heqe1''. rewrite Heqe1'' in Hr.

    destruct ((FP_E_WF.get_wf_end_while Hwf) _ _ _ _ _ Hc)
      as [Hew [Hw [Hid Hb]]]; subst id.

    rewrite (update_stage_nc_block Heqe1'')
              -(update_stage_change_stage Heqe1'') in Hw.

    destruct (eq_exec_while_gen le He2'' Hr Hw)
      as [e2''' [le' [Hwhile He2''']]].

    exists e2''', le'; split => //.

    move => k Hk.
    step_seq. apply Hset. step_skip_seq. step_seq.
    eapply Smallstep.star_one. eapply step_goto; rewrite //=.

    apply correct_block_id_ewhile in Hc.

    rewrite -(update_stage_nc_block Heqe1'') in Hw.
    apply (find_while_label Hwf Hsize Hc Hw).
    apply is_call_cont_call_cont.

    step_trans. apply Hset'.

    rewrite (call_cont_is_call_cont Hk)
      (stage_cont_eq _ (update_stage_nc_block Heqe1''))
      (update_stage_nc_block Heqe1'').
    apply Hwhile.

    all: try by []. 

    rewrite (extract_update_stage _ Heqe1''). by simpl_trace.
  Qed.

  (* Execution of SET *)
  Lemma eq_exec_set:
  forall e1 e1' e2 le b id params stages default,
    e1 ~cenv~ (ge, e2)
    -> set_sem (` e1) params = (b,` e1')
    -> get_current_stage fpe (` e1)
            = FP_E.SET id params
    -> remaining_stages (` e1)
        = ((FP_E.SET id params) :: stages) ++ [:: default]
    -> exists e2' le',
        (forall k, is_call_cont k
        -> Smallstep.star step2 ge
            (State f (gen_fp_stage (FP_E.SET id params))
            (Kseq
              (seq_of_labeled_statement
                (gen_fp_stages (stages ++ [:: default])))
              (stage_cont_e (` e1) k)) ev le (e2.m))
              (fp_trace_to_trace (extract_trace (` e1) (` e1')))
            (State f (bool_continue (` e1') b)
                      (stage_cont_e (` e1) k) ev le' (e2'.m)))
          /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id params stages default
                He Hr Hc Hremain.
    rewrite /set_sem in Hr.

    (** Execute code *)
    remember [:: _; _] as code.
    destruct (match_app_trace code He)
      as [e1' [Heqe1' He']]. rewrite -Heqe1' in Hr.

    destruct params as [var value] eqn:Hparams.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He')
      as [e2' [Hset He2']].
    have Hid: id = (get_nav_stage (` e1')).
    { rewrite /get_current_stage /fpe -Hparams in Hc.
      replace id with (FP_E.get_stage_id (FP_E.SET id params)).
      rewrite Heqe1' -Hc. apply (FP_E_WF.get_wf_numbering Hwf).
      - by rewrite Hc.
      - by []. }

    (** Execution of the NextStage *)
    destruct (next_stage8 fps e1') as [e1'' Hn].
      by rewrite Heqe1' -app_trace_get_current_stage Hc.
    destruct (step_nextstage le He2' Hcev Hn)
      as [e2'' [Hnext He2'']].

    rewrite Hn in Hr; injection Hr as Hb Heq; apply env8_proj in Heq;
      subst b e1'_f.

    exists e2'', le; split.

    move => k Hk. step_seq. rewrite Hid. apply Hset. step_skip_seq.

    step_seq. rewrite -Hparams /gen_fp_set //=.
    apply exec_arbitrary_assign_from_parse. step_skip_seq.

    step_trans. apply Hnext. step_skip_seq.

    rewrite -Hn //. remember [:: _; _] as t.
    rewrite -(app_trace_remaining_stages _ t) in Hremain.
    rewrite -(remaining_stages_nextstage Hremain) Heqe1'.
    step_refl.

    all: try by [].

    simpl_trace.
    by rewrite -Hn -extract_trace_next_stage Heqe1'
                  extract_app_trace Hparams Heqcode.
  Qed.

  (* Execution of CALL *)
  Lemma eq_exec_call:
    forall e1 e1' e2 le b id params stages default,
      e1 ~cenv~ (ge, e2)
      -> call_sem (` e1) params = (b,` e1')
      -> get_current_stage fpe (` e1)
              = FP_E.CALL id params
      -> remaining_stages (` e1)
          = ((FP_E.CALL id params) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
              (State f (gen_fp_stage (FP_E.CALL id params))
              (Kseq
                (seq_of_labeled_statement
                  (gen_fp_stages (stages ++ [:: default])))
                (stage_cont_e (` e1) k)) ev le (e2.m))
                (fp_trace_to_trace (extract_trace (` e1) (` e1')))
              (State f (bool_continue (` e1') b)
                        (stage_cont_e (` e1) k) ev le' (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    rewrite /gen_fp_stage /gen_fp_call /call_sem
      => e1 e1'_f e2 le b id params stages default
            He Hr Hc Hremain.
    destruct params as [func until loop break] eqn:Hparams.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He)
      as [e2' [Hset He2']].
    have Hid: id = (get_nav_stage (` e1)).
    { rewrite /get_current_stage /fpe -Hparams in Hc.
      replace id with (FP_E.get_stage_id (FP_E.CALL id params)).
      rewrite -Hc. apply (FP_E_WF.get_wf_numbering Hwf).
      - by rewrite Hc.
      - by []. }

    destruct loop.

    (* Loop enabled*)
    { (* Evaluation of the function *)
      destruct (evalc8 e1 func) as [[] [e1' Heval]];
        have Hevalc := Heval; injection Heval as Heval He1';
        have He' := evalc_preserve_match Hevalc He2'.

     (* Condition evaluate to true.*)
      { rewrite //= Heval in Hr; rewrite //=.
        destruct until as [cond' |].

        (* There is a until condition *)
        { destruct (evalc8 e1' cond') as [[] [e1'' Heval']];
            have Hevalc' := Heval'; injection Heval' as Heval' He1'';
            have He'' := evalc_preserve_match Hevalc' He'.

           (* Until condition evaluted to true *)
           { rewrite change_trace_trans_next_stage -get_app_trace
                        -Heval He1' Heval' /=
                        (evalc_change_trace_refl Hevalc)
                        change_trace_trans_next_stage 
                        -get_app_trace -Heval' He1''
                        (evalc_change_trace_refl Hevalc') in Hr. 
              injection Hr as Hb He1'_f. subst b.

              have Hne: forall id, get_current_stage (get_fp fps) (` e1'')
                  <> FP_E.DEFAULT id
                by rewrite -(evalc_get_current_stage fpe Hevalc') 
                  -(evalc_get_current_stage fpe Hevalc) Hc.

              destruct (next_stage8 fps e1'' Hne) as [e1''' Heqe1'''].
              destruct (step_nextstage le He'' Hcev Heqe1''')
                as [e2''' [Hnext He''']].
              exists (app_ctrace e2'''
                (fp_trace_to_trace [:: init_stage])); eexists; split.

              move => k Hk.
              step_seq. rewrite Hid. eapply Hset. step_skip_seq.

              star_step
                (@eval_c_code_ifthenelse_neg _ gvars
                    f _ _ _ _ _ He2' Hevalc).
              econstructor. apply eval_expr_neg_const_bool. by [].

              rewrite false_eq_0 //=.

              step_seq. star_step
              (@eval_c_code_ifthenelse _ gvars f _ _ _ _ _ He' Hevalc').
              econstructor. apply eval_expr_const_bool. by [].

              rewrite true_ne_0 //=.
              step_seq. apply Hnext. 
              step_skip_seq. star_step step_break_seq.
              star_step step_break_seq. step_refl.

              all: try by [].  step_refl.

              simpl_trace.

              have Hex: trace_appended (` e1) (next_stage (` e1'')).
              { apply extract_next_stage_rec.
                apply (trace_appended_trans 
                          (extract_evalc Hevalc)
                          (extract_evalc Hevalc')). }

              by rewrite -He1'_f (extract_app_trace_gen _ Hex)
                          -?extract_trace_next_stage
                          ?(trace_appended_app
                              (extract_evalc Hevalc)
                              (extract_evalc Hevalc')) app_assoc.

              rewrite /MATCH_ENV.match_fp_cenv -He1'_f Heqe1'''.
              by apply app_trace_preserve_match2. }

           (* Until condition evaluated to false *)
           { rewrite -get_app_trace -Heval He1' Heval' /= in Hr.
              injection Hr as Hb He1'_f; subst b.

              exists (create_fp_cenv (e2'.m)
                (fp_trace_to_trace (get_trace (`e1'_f))));  eexists; split.

              move => k Hk.
              step_seq. rewrite Hid. eapply Hset. step_skip_seq.

              star_step
                (@eval_c_code_ifthenelse_neg _ gvars
                    f _ _ _ _ _ He2' Hevalc).
              econstructor. apply eval_expr_neg_const_bool. by [].

              rewrite false_eq_0 //=.

              step_seq. star_step
                (@eval_c_code_ifthenelse _ gvars f _ _ _ _ _ He' Hevalc').
              econstructor. apply eval_expr_const_bool. by [].

              rewrite false_eq_0 //=.
              step_skip_seq. star_step step_break_seq. step_refl.
              all: try by []. step_refl.

              simpl_trace.
              by rewrite - He1'_f-Heval' He1''
                (trace_appended_app
                  (extract_evalc Hevalc) (extract_evalc Hevalc')).

              split; try by apply trace_generated_match.
              rewrite -He1'_f /=. destruct He' as [He' Ht].
              rewrite /= in He'. by apply app_trace_preserve_mmatch. }
        }

        (* There is no until condition *)
        { (* Execution of the NextStage *)
          have Hne: forall id, get_current_stage (get_fp fps) (` e1')
            <> FP_E.DEFAULT id
          by rewrite -(evalc_get_current_stage fpe Hevalc) Hc.

          destruct (next_stage8 fps e1' Hne) as [e1'' Heqe1''].
          destruct (step_nextstage le He' Hcev Heqe1'' )
              as [e2''' [Hnext He'']].
          do 2 eexists; split.

          move => k Hk. step_seq. rewrite Hid. eapply Hset. step_skip_seq.

          star_step
            (@eval_c_code_ifthenelse_neg _ gvars f _ _ _ _ _ He2' Hevalc).
          econstructor. apply eval_expr_neg_const_bool. by [].

          rewrite false_eq_0 //=. inversion Hr; try subst b e1'_f.

          step_seq. step_skip_seq. star_step step_break_seq.
          rewrite (app_ctrace_mem e2' [:: cond_event func true]).
          step_refl.

          all: try by []. step_refl. simpl_trace.

          all: injection Hr as Hb Heq; subst b.

          by rewrite -Heq -Heval He1'.

          rewrite Heval //= Heq in He1'.
          apply env8_proj in He1'; by subst e1'_f. }
      }

      (* Condition evaluate to false *)
      { (* Execution of the NextStage *)
        rewrite //= Heval change_trace_trans_next_stage
                  -get_app_trace -Heval He1' 
                  (evalc_change_trace_refl Hevalc) in Hr; rewrite //=.

        have Hne: forall id, get_current_stage (get_fp fps) (` e1')
          <> FP_E.DEFAULT id
        by rewrite -(evalc_get_current_stage fpe Hevalc) Hc.

        destruct (next_stage8 fps e1' Hne) as [e1'' Heqe1''].
        destruct (step_nextstage le He' Hcev Heqe1'')
            as [e2''' [Hnext He'']].
        do 2 eexists; split.

        move => k Hk. step_seq. rewrite Hid. eapply Hset. step_skip_seq.

        star_step
          (@eval_c_code_ifthenelse_neg _ gvars f _ _ _ _ _ He2' Hevalc).
        econstructor. apply eval_expr_neg_const_bool. by [].

        rewrite true_ne_0 //=.

        all: destruct break; injection Hr as Hb Heq; try subst b.

        - step_seq. apply Hnext. step_skip_seq.
          star_step step_break_seq.
          rewrite (app_ctrace_mem e2'''
            (fp_trace_to_trace [:: init_stage])). step_refl. 
          all: try by [].

        - step_trans. eapply Hnext. step_skip_seq.
          rewrite (evalc_remaining_stages Hevalc) in Hremain.
          rewrite -Heq //= change_trace_remaining_stages
                         (remaining_stages_nextstage Hremain).
                         step_refl. all: try by []. all: try simpl_trace.

        all: try rewrite -Heq
                (trace_appended_app (extract_evalc Hevalc)).
        all: try rewrite
                (extract_app_trace_gen _ (extract_next_stage (` e1')))
                -?extract_trace_next_stage
                ?extract_trace_refl //.
        all: try apply
            (trace_appended_app_trace_rec _ (extract_next_stage (` e1'))).

        all: rewrite Heqe1'' in Heq.
        all: rewrite /MATCH_ENV.match_fp_cenv -Heq.
        all: by apply app_trace_preserve_match2. }
     }

    (* Loop not enabled *)
    { rewrite //=; rewrite //= in Hr. 
      (* Execution of the NextStage *)
      have Hne: forall id, get_current_stage (get_fp fps) (` e1)
        <> FP_E.DEFAULT id  by rewrite Hc.

      destruct (next_stage8 fps e1 Hne) as [e1' Heqe1'].
      destruct (step_nextstage le He2' Hcev Heqe1')
        as [e2'' [Hnext He2'']].

      do 2 eexists; split.

      move => k Hk. step_seq. rewrite Hid. eapply Hset. step_skip_seq.

      step_seq. eapply exec_arbitrary_C_code. step_skip_seq.

      all: destruct break; injection Hr as Hb Heq; subst b.

      - step_seq. apply Hnext. step_skip_seq.
        star_step step_break_seq.
        rewrite (app_ctrace_mem e2''
            (fp_trace_to_trace [:: C_CODE func; init_stage])). step_refl. 
        all: try by [].

      - step_trans. eapply Hnext. step_skip_seq.
        rewrite -Heq //= app_trace_remaining_stages
                  (remaining_stages_nextstage Hremain).
        step_refl. all: try by [].

      all: simpl_trace.

      all: try rewrite -Heq
                (trace_appended_app
                    (trace_appended_app_trace _ _) (extract_next_stage _))
                -extract_trace_next_stage
                extract_trace_refl extract_app_trace //.

      all: rewrite /MATCH_ENV.match_fp_cenv -Heq Heqe1'.

      all: by apply app_trace_preserve_match2.

      all: try by apply app_trace_preserve_match.
      }
  Qed.

  (* Execution of DEROUTE *)
  Lemma eq_exec_deroute:
    forall e1 e1' e2 le b id params stages default,
      e1 ~cenv~ (ge, e2)
      -> deroute_sem fp (`e1) (get_deroute_block_id params) = (b,` e1')
      -> get_current_stage fpe (` e1)
              = FP_E.DEROUTE id params
      -> remaining_stages (` e1)
          = ((FP_E.DEROUTE id params) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
              (State f (gen_fp_stage (FP_E.DEROUTE id params))
              (Kseq
                (seq_of_labeled_statement
                  (gen_fp_stages (stages ++ [:: default])))
                (stage_cont_e (` e1) k)) ev le (e2.m))
                (fp_trace_to_trace (extract_trace (` e1) (` e1')))
              (State f (bool_continue (` e1') b)
                        (stage_cont_e(` e1)k) ev le' (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id params stages default
      He Hr Hc Hremain.
    rewrite /deroute_sem in Hr.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He)
      as [e2' [Hset He2']].
    have Hid: id = (get_nav_stage (` e1)).
    { rewrite /get_current_stage /fpe in Hc.
      replace id with (FP_E.get_stage_id (FP_E.DEROUTE id params)).
      rewrite -Hc. apply (FP_E_WF.get_wf_numbering Hwf).
      - by rewrite Hc.
      - by []. }

    (** Execution of the NextStage *)
    destruct (next_stage8 fps e1) as [e1' Hn].
      by rewrite Hc. rewrite Hn in Hr.
    destruct (step_nextstage le He2' Hcev Hn)
      as [e2'' [Hnext He2'']].

    (** Execute code *)
    destruct (match_app_trace [:: init_stage] He2'')
      as [e1'' [Heqe1'' He'']]. rewrite -Heqe1'' in Hr.

    (** Execution of the goto *)
    destruct (goto_block8 fps e1'' (get_deroute_block_id params))
      as [e1''' Hd]; rewrite Hd in Hr; injection Hr as Hb Heq;
      apply env8_proj in Heq; subst b e1'_f.

    have Hid8 := user_id_is_nat_8 ((correct_deroutes_gen Hsize) _ _ _ Hc).

    destruct (step_goto_block le He'' Hid8 Hd)
      as [e2''' [le'' [Hgoto He2''']]].

    exists e2''', le''; split; try by [].

    move => k Hk. step_seq. rewrite Hid. apply Hset. step_skip_seq.

    step_seq. apply Hnext. step_skip_seq.

    step_seq. apply Hgoto. step_skip_seq.
    econstructor. econstructor. step_refl.

    all: try by [].

    have H: trace_appended (` e1') (` e1'').
    { rewrite Heqe1''. apply trace_appended_app_trace. }

    have H': trace_appended (` e1'') (` e1''').
    {rewrite -Hd. apply extract_goto_block'. }

    rewrite ?E0_left ?E0_right -trace_app
              -(extract_app_trace (`e1')[:: init_stage])
              -Heqe1'' -trace_appended_app //.
    rewrite (trace_appended_app (extract_next_stage (` e1)))
              -?extract_trace_next_stage ?extract_trace_refl Hn //.
    apply (trace_appended_trans H H').

  Qed.

  (* Execution of RETURN *)
  Lemma eq_exec_return:
    forall e1 e1' e2 le b id params stages default,
      e1 ~cenv~ (ge, e2)
      -> return_sem fp (` e1) params = (b,` e1')
      -> get_current_stage fpe (` e1)
              = FP_E.RETURN id params
      -> remaining_stages (` e1)
          = ((FP_E.RETURN id params) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
              (State f (gen_fp_stage (FP_E.RETURN id params))
              (Kseq
                (seq_of_labeled_statement
                  (gen_fp_stages (stages ++ [:: default])))
                (stage_cont_e (` e1) k)) ev le (e2.m))
                (fp_trace_to_trace (extract_trace (` e1) (` e1')))
              (State f (bool_continue (` e1') b)
                        (stage_cont_e (` e1) k) ev le' (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id params stages default
                He Hr Hc Hremain.
     rewrite /return_sem in Hr.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He)
      as [e2' [Hset He2']].
    have Hid: id = (get_nav_stage (` e1)).
    { rewrite /get_current_stage /fpe in Hc.
      replace id with (FP_E.get_stage_id (FP_E.RETURN id params)).
      rewrite -Hc. apply (FP_E_WF.get_wf_numbering Hwf).
      - by rewrite Hc.
      - by []. }

    (** Execution of the return *)
    destruct (return_block8 fps e1 params) as [e1' Heqe1'];
      rewrite Heqe1' in Hr.
    destruct (step_return le He2' Hcev Heqe1')
      as [e2'' [le' [Hreturn He2'']]].

    (** Execute the code *)
    destruct (match_app_trace ((FP_E_WF.on_exit (` fp) (get_nav_block (` e1))) ++ [::reset_time, init_stage & (FP_E_WF.on_enter (` fp) (get_last_block (` e1)))]) He2'')
      as [e1'' [Heqe1'' He'']].
    rewrite -Heqe1'' in Hr; injection Hr as Hb Heq;
      apply env8_proj in Heq; subst e1'_f b.

    exists (app_ctrace e2'' (fp_trace_to_trace ((FP_E_WF.on_exit (` fp) (get_nav_block (` e1))) ++ [:: reset_time, init_stage & (FP_E_WF.on_enter (` fp) (get_last_block (` e1)))]))), le'.
  
    split; try by [].
    move => k Hk. step_seq. rewrite Hid. apply Hset. step_skip_seq.

    step_seq. apply Hreturn. step_skip_seq.
    econstructor. econstructor. step_refl.

    all: try by [].

    rewrite ?E0_right ?E0_left Heqe1'' extract_app_trace_gen
              -?Heqe1' -?extract_trace_return_block
              ?extract_trace_refl //.
    by apply extract_return_block.

  Qed.

  (* Execution of NAV_INIT *)
  Lemma eq_exec_nav_init:
    forall e1 e1' e2 le b id nav_mode stages default,
      e1 ~cenv~ (ge, e2)
      -> nav_init_sem (` e1) nav_mode = (b,` e1')
      -> get_current_stage fpe (` e1)
              = FP_E.NAV_INIT id nav_mode
      -> remaining_stages (` e1)
          = ((FP_E.NAV_INIT id nav_mode) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
              (State f (gen_fp_stage (FP_E.NAV_INIT id nav_mode))
              (Kseq
                (seq_of_labeled_statement
                  (gen_fp_stages (stages ++ [:: default])))
                (stage_cont_e (` e1) k)) ev le (e2.m))
              (fp_trace_to_trace (extract_trace (` e1) (` e1')))
              (State f (bool_continue (` e1') b)
                        (stage_cont_e (` e1) k) ev le' (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id nav_mode stages default
                He Hr Hc Hremain;
    rewrite /nav_init_sem in Hr. remember [:: _; _] as code.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He)
    as [e2' [Hset He2']].
    have Hid: id = (get_nav_stage (` e1)).
    { rewrite /get_current_stage /fpe in Hc.
      replace id with (FP_E.get_stage_id (FP_E.NAV_INIT id nav_mode)).
      rewrite -Hc. apply (FP_E_WF.get_wf_numbering Hwf).
      - by rewrite Hc.
      - by []. }

    (** Execution of the NextStage *)
    have Hne: forall id, get_current_stage (FPS_BS.fp fps) (` e1)
        <> FP_E.DEFAULT id by rewrite Hc.
    destruct (next_stage8 fps e1 Hne) as [e1' Hn];
      rewrite Hn in Hr.
    destruct (step_nextstage le He2' Hcev Hn) as [e2'' [Hnext He2'']].

    (** Execute the code *)
    destruct (match_app_trace code He2'') as [e1'' [Heqe1'' He'']].
      rewrite -Heqe1'' in Hr; injection Hr as Hb Heq;
      apply env8_proj in Heq; subst e1'_f b.

    exists (app_ctrace e2'' (fp_trace_to_trace code)), le; split.

    move => k Hk. step_seq. rewrite Hid. apply Hset. step_skip_seq.

    step_seq. apply eq_exec_nav_init_code. step_skip_seq.

    step_seq. apply Hnext. step_skip_seq.
    econstructor. econstructor.
    step_refl.

    all: try by [].

    rewrite ?E0_right ?E0_left Heqe1'' -Hn extract_app_trace_gen
              -?extract_trace_next_stage ?extract_trace_refl ?Heqcode
              -?trace_app //.
    apply (extract_next_stage_rec (trace_appended_refl _)). 

  Qed.

  (* Execution of NAV *)

  Lemma nav_code_sem_break:
    forall e1 e1' nav_mode until b,
      nav_code_sem e1 nav_mode until = (b, e1')
      -> b = false.
  Proof.
    move =>  e1 e1' nav_mode until b.
    rewrite /nav_code_sem.
    destruct until as [u|];
      [destruct (evalc _ u) as [[] e]|];
      move => H; by inversion H.
  Qed.

  Lemma eq_exec_nav_code:
    forall e1 e1' e2 le b id nav_mode until stages default,
      e1 ~cenv~ (ge, e2)
      -> nav_code_sem (` e1) nav_mode until = (b,` e1')
      -> get_current_stage fpe (` e1)
              = FP_E.NAV id nav_mode until
      -> remaining_stages (` e1)
          = ((FP_E.NAV id nav_mode until) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
              (State f (gen_fp_nav_code nav_mode)
              (Kseq
                (gen_fp_nav_until nav_mode until @+@
                  gen_fp_nav_post_call nav_mode @+@ Sbreak)
              (Kseq
                (seq_of_labeled_statement
                  (gen_fp_stages (stages ++ [:: default])))
                (stage_cont_e (` e1) k))) ev le (e2.m))
                (fp_trace_to_trace (extract_trace (` e1) (` e1')))
              (State f Sbreak (stage_cont_e (` e1) k) ev le' (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id nav_mode until stages default
                He Hr Hc Hremain.
    rewrite /nav_code_sem in Hr.
    remember (gen_fp_nav_code_sem _) as code.

    destruct (match_app_trace code He) as [e1' [Heqe1' He']];
      rewrite -Heqe1' in Hr.

    destruct until as [cond|] eqn:Huntil.

    (* There is a condition to evaluate *)
    destruct (evalc8 e1' cond) as [[] [e1'' Heval]];
      have He'' := (evalc_preserve_match Heval He');
      rewrite Heval /= in Hr.

    (* Condition evaluate to true*)
    { remember [:: post_call_nav_sem nav_mode; init_stage] as post.
      destruct (match_app_trace post He'') as [e1''' [Heqe1''' He''']];
        rewrite -Heqe1''' in Hr.

      have Hne: forall id, get_current_stage fpe (` e1''')
            <> FP_E.DEFAULT id by
        rewrite Heqe1''' -app_trace_get_current_stage
                              -(evalc_get_current_stage fpe Heval) Heqe1'
                              -app_trace_get_current_stage Hc.

      destruct (next_stage8 fps e1''' Hne) as [e1'4 Hn];
        rewrite Hn in Hr; injection Hr as Hb Heq;
        apply env8_proj in Heq; subst b e1'_f.
      destruct (step_nextstage le He''' Hcev Hn) as [e2'' [Hnext He'4]].

      do 2 eexists; split. move => k Hk.
      step_trans. eapply eq_exec_gen_fp_nav_code. step_skip_seq.

      step_seq. star_step
        (@eval_c_code_ifthenelse _ gvars f _ _ _ _ _ He' Heval).
      econstructor. apply eval_expr_const_bool. by [].
      rewrite true_ne_0 //=.

      step_seq. eapply eq_exec_post_nav_call. step_skip_seq.

      step_seq. eapply Hnext. step_skip_seq.
      star_step step_break_seq. star_step step_break_seq. step_refl.

      all: try by []. step_refl. simpl_trace.

      have Hex : trace_appended (` e1'') (` e1'''). 
      { rewrite Heqe1'''. 
        apply (trace_appended_app_trace_rec _ (trace_appended_refl _)). } 
      rewrite  -Heqpost -Heqcode //=
                -(extract_app_trace (` e1'') post) -Hn
                -(extract_trace_next_stage (` e1))
                -Heqe1'''.
      rewrite -(trace_appended_app (extract_evalc Heval) Hex) Heqe1'
                app_extract_app_trace // -?Heqe1'.
      apply (trace_appended_trans (extract_evalc Heval) Hex).

      by []. }

      (* Condition evaluate to true*)
      { remember [:: post_call_nav_sem nav_mode] as post.
        destruct (match_app_trace post He'') as [e1''' [Heqe1''' He''']];
          rewrite -Heqe1''' in Hr; injection Hr as Hb Heq;
          apply env8_proj in Heq; subst b e1'''.

        do 2 eexists; split. move => k Hk.
        step_trans. eapply eq_exec_gen_fp_nav_code. step_skip_seq.

        rewrite (app_ctrace_mem e2  [:: cond_event cond false]).

        step_seq. star_step
          (@eval_c_code_ifthenelse _ gvars f _ _ _ _ _ He' Heval).
        econstructor. apply eval_expr_const_bool. by [].
        rewrite false_eq_0 //=. step_skip_seq.

        step_seq. eapply eq_exec_post_nav_call. step_skip_seq.

        star_step step_break_seq.
        rewrite (app_ctrace_mem e2 (fp_trace_to_trace code)).
        rewrite (app_ctrace_mem _ [:: cond_event cond false]).
        rewrite (app_ctrace_mem _ (fp_trace_to_trace post)).
        step_refl.

        all: try by []. step_refl. simpl_trace.

        have Hex : trace_appended (` e1'') (` e1'_f).
        { rewrite Heqe1'''.
          apply (trace_appended_app_trace_rec _
            (trace_appended_refl _)). }
        rewrite  -Heqpost -Heqcode //= -(extract_app_trace (` e1'') post)
                  -Heqe1'''.
        rewrite -(trace_appended_app (extract_evalc Heval) Hex)
                  Heqe1' app_extract_app_trace // -?Heqe1'.
        apply (trace_appended_trans (extract_evalc Heval) Hex).

        by []. }

    (* There is no condition to evaluate*)
    remember [:: post_call_nav_sem nav_mode] as post.
    destruct (match_app_trace post He') as [e1'' [Heqe1'' He'']];
      rewrite -Heqe1'' in Hr; injection Hr as Hb Heq;
      apply env8_proj in Heq; subst b e1''.

    do 2 eexists; split. move => k Hk.

    step_trans. eapply eq_exec_gen_fp_nav_code. step_skip_seq.

    step_seq. rewrite //=. step_skip_seq.

    step_seq. eapply eq_exec_post_nav_call. step_skip_seq.

    star_step step_break_seq.
    rewrite (app_ctrace_mem e2 (fp_trace_to_trace code)).
    rewrite (app_ctrace_mem _ (fp_trace_to_trace post)).
    step_refl.
    all: try by []. step_refl. simpl_trace.

    rewrite -Heqpost -Heqcode
                -(extract_app_trace (`e1') post) -Heqe1''
                -(extract_app_trace (`e1) code) -Heqe1'
                -trace_appended_app // ?Heqe1'' ?Heqe1'.
    all: try by apply trace_appended_app_trace.

    by [].
  Qed.

  Lemma eq_exec_nav:
    forall e1 e1' e2 le b id nav_mode until stages default,
      e1 ~cenv~ (ge, e2)
      -> nav_sem (` e1) nav_mode until = (b, ` e1')
      -> get_current_stage fpe (` e1)
              = FP_E.NAV id nav_mode until
      -> remaining_stages (` e1)
          = ((FP_E.NAV id nav_mode until) :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
              (State f (gen_fp_stage (FP_E.NAV id nav_mode until))
              (Kseq
                (seq_of_labeled_statement
                  (gen_fp_stages (stages ++ [:: default])))
                (stage_cont_e (` e1) k)) ev le (e2.m))
              (fp_trace_to_trace (extract_trace (` e1) (` e1')))
              (State f (bool_continue (` e1') b)
                        (stage_cont_e (` e1) k) ev le' (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id nav_mode until stages default
                He Hr Hc Hremain.
    rewrite /nav_sem in Hr.

    (** Execution of the setNav *)
    destruct (step_set_NavStage_unchanged le Hcev He)
    as [e2' [Hset He2']].
    have Hid: id = (get_nav_stage (` e1)).
    { rewrite /get_current_stage /fpe in Hc.
      replace id with (FP_E.get_stage_id (FP_E.NAV id nav_mode until)).
      rewrite -Hc. apply (FP_E_WF.get_wf_numbering Hwf).
      - by rewrite Hc.
      - by []. }

    (** Test if there is a nav condition *)
    destruct (nav_cond_sem nav_mode) as [cond |] eqn:Hcond.

    (** There is a condition to evaluate *)
    destruct (nav_cond_impl_go Hcond) as [ps [pm [pc Hgo]]].
      subst nav_mode.
    destruct (nav_cond_sem_to_stmt Hcond) as [stmt_cond [wp Hcstmt]].

    remember [:: pre_call_nav_sem _] as pre.
    remember (post_call_nav_sem _) as post.

    destruct (match_app_trace pre He2') as [e1' [Heqe1' He']];
      rewrite -Heqe1' in Hr.

    destruct (evalc8 e1' cond) as [[] [e1'' Heval]];
    have He'' := (evalc_preserve_match Heval He');
    rewrite Heval in Hr.

    (** Condition evaluate to true -> break *)
    { injection Hr as Hb Hn.
      remember [:: post; C_CODE _; _ ] as post'.
      destruct (match_app_trace post' He'') as [e1''' [Heqe1''' He''']].
      rewrite -Heqe1''' in Hn; subst b.
      remember (PTree.set tmp_NavCond (create_bool_val true) le) as le'.

      destruct (step_nextstage le' He''' Hcev Hn ) as [e2'' [Hnext He'_f]].

      exists e2'', le'; split; subst le'.
      move => k Hk. step_seq. rewrite Hid; apply Hset. step_skip_seq.

      step_seq. apply eq_exec_pre_nav_call. step_skip_seq.

      rewrite /gen_fp_nav_cond Hcstmt. step_seq. star_step step_seq.
      rewrite (app_ctrace_mem e2' (fp_trace_to_trace pre)).
      
      star_step (@eq_exec_nav_cond_stmt fps
                      gvars  f _ _ _ _ _ _ _ _ _ _ He' Heval Hcond Hcstmt).
      apply step_set. apply eval_expr_const_bool. step_skip_seq.

      star_step step_ifthenelse;
        [apply eval_expr_tmp_bool |by [] |rewrite true_ne_0 //= | ].

      step_seq. apply eq_exec_post_nav_call. step_skip_seq.
      rewrite /nextStageAndBreakFrom /nextStageAndBreak //=.
      step_seq. apply (eq_exec_last_wp _ _ _ _ _ _ Hcstmt). step_skip_seq.

      step_seq. apply Hnext. step_skip_seq.

      star_step step_break_seq. star_step step_break_seq. step_refl.

      all: try by []. step_refl.

      subst post'.  remember (C_CODE _) as last.
      rewrite ?E0_left ?E0_right -?trace_app -Heqpre -Heqpost //=
                -(extract_app_trace (`e1) pre)
                -(extract_app_trace (` e1'') [:: post; last; init_stage])
                -Heqe1''' -Hn -extract_trace_next_stage
                -Heqe1'.

      have H : trace_appended (`e1) (`e1')
                by rewrite Heqe1'; apply trace_appended_app_trace.
      have H' : trace_appended (`e1'') (`e1''')
                by rewrite Heqe1'''; apply trace_appended_app_trace.
      by rewrite -(trace_appended_app (extract_evalc Heval) H')
                -(trace_appended_app H
                    (trace_appended_trans (extract_evalc Heval) H')). }

    (** Condition evaluate to false -> execute the code *)
    { rewrite (app_trace_get_current_stage _ (` e1) pre) -Heqe1'
                (evalc_get_current_stage fpe Heval) in Hc.
      rewrite -(app_trace_remaining_stages (` e1) pre) -Heqe1'
                          (evalc_remaining_stages Heval) in Hremain.
      destruct (nav_code_sem8 fps e1'' Hc) as [b' [e1''' Hcode]].
      rewrite Hcode in Hr; injection Hr as Hb Heq;
        apply env8_proj in Heq; subst b' e1'_f.

      remember (PTree.set tmp_NavCond (create_bool_val false) le) as le'.
      edestruct (eq_exec_nav_code le' He'' Hcode Hc Hremain)
        as [e2'' [le''  [Hcode' He2'']]].

      exists e2'', le''; split.

      move => k Hk.

      step_seq. rewrite Hid; apply Hset. step_skip_seq.

      step_seq. apply eq_exec_pre_nav_call. step_skip_seq.

      rewrite /gen_fp_nav_cond Hcstmt. step_seq. star_step step_seq.
      star_step (@eq_exec_nav_cond_stmt fps gvars f
                        _ _ _ _ _ _ _ _ _ _ He' Heval Hcond Hcstmt).
      apply step_set. apply eval_expr_const_bool. step_skip_seq.

      star_step step_ifthenelse;
        [apply eval_expr_tmp_bool |by [] |rewrite false_eq_0 //= | ].

      rewrite /gen_fp_nav_code Heqle' in Hcode'.
      all: try rewrite (stage_cont_app_trace (` e1) pre) -Heqe1'
                  (stage_cont_eq' _ Heval) (nav_code_sem_break Hcode).
      apply Hcode'.

      all: try by []. step_refl.

      rewrite ?E0_left ?E0_right -?trace_app -Heqpre //=
            -(trace_appended_app (extract_evalc Heval)
                            (extract_nav_code_sem Hcode))
            Heqe1' app_extract_app_trace // -?Heqe1'.

      by apply (trace_appended_trans
                          (extract_evalc Heval)
                          (extract_nav_code_sem Hcode)). }

    (** There is no condition to evaluate *)
    rewrite //= /gen_fp_nav_stmt (no_nav_cond_impl_code Hcond).
    remember [:: pre_call_nav_sem _] as pre.

    destruct (match_app_trace pre He2') as [e1' [Heqe1' He']];
      rewrite -Heqe1' in Hr. 

    rewrite (app_trace_get_current_stage _ (` e1) pre) -Heqe1' in Hc.
    rewrite -(app_trace_remaining_stages (` e1) pre) -Heqe1' in Hremain.

    destruct (nav_code_sem8 fps e1' Hc) as [b' [e1'' Hcode]];
      rewrite Hcode in Hr; injection Hr as Hb Heq;
      apply env8_proj in Heq; subst b' e1'_f.

    destruct (eq_exec_nav_code le He' Hcode Hc Hremain)
      as [e2'' [le' [Hcode' He2'']]].

    exists e2'', le'; split. move => k Hk.
    step_seq. rewrite Hid; apply Hset. step_skip_seq.

    step_seq. apply eq_exec_pre_nav_call. step_skip_seq.

    rewrite (nav_code_sem_break Hcode). step_seq.
    all: try rewrite (stage_cont_app_trace (` e1) pre) -Heqe1'.
    by apply Hcode'. step_refl.

    all: try by [].

    rewrite ?E0_left ?E0_right -?trace_app -Heqpre //= Heqe1'
              app_extract_app_trace // -?Heqe1'.
    by apply (extract_nav_code_sem Hcode). 
  Qed.

  (** Default stage*)
  Lemma eq_exec_default:
    forall e1 e1' e2 le b id,
      e1 ~cenv~ (ge, e2)
      -> default_sem fp (` e1) = (b,` e1')
      -> get_current_stage fpe (` e1) = FP_E.DEFAULT id
      -> exists e2' le', (forall k,
        Smallstep.star step2 ge
          (State f
            (gen_fp_stage
              (FP_E.DEFAULT id))
            k ev le (e2.m))
          (fp_trace_to_trace (extract_trace (` e1) (` e1')))
          (State f Sbreak k ev le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 le b id He Hr Hc.
    rewrite /default_sem in Hr.
    rewrite //= /gen_fp_default.
    have He8 := proj2_sig e1.

    rewrite /get_current_stage in Hc.
    have Hid := FP_E_WF.get_id_default Hwf Hc; subst id.

    have Hd8 := default_stage_id8 fps He8.

    (* Exec Set NavStage*)
    destruct (step_set_NavStage le Hcev Hd8 He)
      as [e1' [e2' [Heqe1' [Hset He']]]].
    rewrite /reset_stage in Hr.

    (* Exec NextBlock *)
    destruct (next_block8 fps e1') as [e1'' Hnext].
    destruct (step_nextblock le He' Hcev Hnext)
      as [e2'' [Hnext' He'']].

    rewrite -Heqe1' Hnext in Hr. injection Hr as Hb Heq.
    apply env8_proj in Heq ; subst b e1'_f.

    do 2 eexists; split.

    move => k. step_seq. apply Hset. step_skip_seq.

    step_seq. apply Hnext'. step_skip_seq. step_refl.

    all: try by [].

    simpl_trace. symmetry in Heqe1'.
    by rewrite /extract_trace (update_stage_nc_trace Heqe1').
  Qed.

  (** Execution of one stage *)
  Lemma eq_exec_stage:
    forall e1 e1' e2 le b stage stages default,
      e1 ~cenv~ (ge, e2)
      -> run_stage fp (` e1) = (b,` e1')
      -> get_current_stage fpe (` e1) = stage
      -> remaining_stages (` e1) = (stage :: stages) ++ [:: default]
      -> exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
            (State f (gen_fp_stage stage)
            (Kseq
              (seq_of_labeled_statement
                (gen_fp_stages (stages ++ [:: default]))) 
              (stage_cont_e (` e1) k)) ev le (e2.m))
            (fp_trace_to_trace (extract_trace (` e1) (` e1')))
            (State f (bool_continue (` e1') b)
                       (stage_cont_e (` e1) k) ev le' (e2'.m)))
          /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 le b stage stages default
                He Hr Hstage Hremain.
    rewrite /run_stage Hstage in Hr.
    destruct stage.

    (* While *)
    apply (eq_exec_while le He Hr Hstage Hremain).

    (* End While *)
    apply (eq_exec_end_while le He Hr Hstage Hremain).

    (* Set *)
    apply (eq_exec_set le He Hr Hstage Hremain).

    (* Call *)
    apply (eq_exec_call le He Hr Hstage Hremain).

    (* Deroute *)
    have Hr': deroute_sem fp (` e1) (get_deroute_block_id params)
                      = (b,` e1') by rewrite -Hr; by destruct params.
    apply (eq_exec_deroute le He Hr' Hstage Hremain).

    (* Return *)
    apply (eq_exec_return le He Hr Hstage Hremain).

    (* Nav Init *)
    apply (eq_exec_nav_init le He Hr Hstage Hremain).

    (* Nav *)
    apply (eq_exec_nav le He Hr Hstage Hremain).

    (* Default *)
    destruct (eq_exec_default le He Hr Hstage)
      as [e2' [le' [Hstages' He']]].
    exists e2', le'; split; try by [].
    move => k Hk.  step_trans. apply Hstages'.

    rewrite /default_sem in Hr.
    destruct (next_block fp _). inversion Hr; rewrite //=.
    apply Smallstep.star_one. apply step_break_seq.
    by simpl_trace.
  Qed.

  (** Execution of the stages *)
  Definition P_eq_exec_stages (e e': fp_env):=
    forall (le : temp_env) (e2 : fp_cenv),
    e ~menv~ (ge, e2 .m) /\ (get_trace e) ~t~ (e2 .t)
    ->  exists (e2' : fp_cenv) (le' : temp_env),
        (forall k : cont,
        is_call_cont k ->
        Smallstep.star step2 ge
          (State f
              (seq_of_labeled_statement (gen_fp_stages (remaining_stages e)))
              (stage_cont_e e k) ev le (e2 .m))
          (fp_trace_to_trace (extract_trace e e'))
          (State f Sskip (block_cont_e e k) ev le' (e2' .m)))
      /\  e' ~menv~ (ge, e2' .m) /\ (get_trace e') ~t~ (e2' .t).

  Lemma eq_exec_stages:
    forall e1 e1' e2 le,
      e1 ~cenv~ (ge, e2)
      -> run_step fp (` e1) = (` e1')
      -> exists e2' le',
          (forall k, is_call_cont k
          -> (Smallstep.star step2 ge
              (State f
              (seq_of_labeled_statement
                      (gen_fp_stages (remaining_stages (` e1))))
                (stage_cont_e (` e1) k) ev le (e2.m))
              (fp_trace_to_trace (extract_trace (` e1) (` e1')))
              (State f Sskip  (block_cont_e (` e1) k) ev le' (e2'.m))))
                /\ e1' ~cenv~ (ge, e2').
  Proof.
    rewrite /MATCH_ENV.match_fp_cenv.
    move => E1 E1' e2 le H H'.
    generalize dependent e2. generalize dependent le.
    have H := (run_step_ind8 fps P_eq_exec_stages _ _ E1 E1' H').
    apply H.
    { move => e1 e1' e1'' Hstage Hstep IH le e2 He.
      rewrite /remaining_stages //=.

      remember (drop _ _) as stages.
      remember (FP_E.default_stage _ _) as default.

      destruct stages as [| stage stages'] eqn:Hstages.
      - (* Not possible as its continue *)
        rewrite /run_stage 
                  /get_current_stage
                  /FP_E.get_stage 
                  (get_default_stage_drop _ _ Hwf Heqstages)
                  //= in Hstage.
      - replace (seq_of_labeled_statement _)
          with ((gen_fp_stage stage)
                    @+@ (seq_of_labeled_statement
                            (gen_fp_stages (stages' ++ [:: default]))));
          last first. by destruct stage.
        have Hget: get_current_stage fpe (` e1) = stage.
        { rewrite /get_current_stage
                    /FP_E.get_stage.
           apply (get_stage_drop _ _ Hwf Heqstages). }

        have Hremain : remaining_stages (` e1) =
                    (stage :: stages') ++ [:: default]
        by rewrite /remaining_stages Heqstages Heqdefault.

        edestruct (eq_exec_stage le He Hstage Hget Hremain)
          as [e2' [le' [Hs He']]].

        rewrite /P_eq_exec_stages in IH.

        edestruct (IH le' _ He') as [e2'' [le'' [Hs' He'']]].

        exists e2'', le''; split.
        move => k Hk. step_seq. step_trans. apply Hs. rewrite //=.
        rewrite (stage_cont_eq _
                 (run_continue_stage_unchange_block _ _ Hstage)).
        by apply Hs'.

        all: try by []. 
        rewrite (block_cont_eq _
                  (run_continue_stage_unchange_block _ _ Hstage)).
        step_refl. simpl_trace.
        by rewrite -(trace_appended_app (extract_run_stage Hstage)
                                              (extract_run_step Hstep)).
    }

    move => e1 e1' Hr le e2 He; rewrite //= /remaining_stages.

    remember (drop _ _) as stages.
    remember (FP_E.default_stage _ _) as default.

    destruct stages as [| stage stages'] eqn:Hstages.
    - replace (seq_of_labeled_statement _)
          with (gen_fp_stage default @+@ Sskip);
          last first. by subst default.
      have Hget: get_current_stage fpe (` e1) = default.
      { rewrite /get_current_stage /FP_E.get_stage Heqdefault.
         by apply (get_default_stage_drop _ _ Hwf Heqstages). }
      subst default.

      rewrite /run_stage Hget in Hr.
      edestruct (eq_exec_default le He Hr Hget)
        as [e2' [le' [Hs He']]]; exists e2', le'; split.

      move => k Hk. step_seq. apply Hs.
      eapply Smallstep.star_two. apply step_break_seq.
      apply step_skip_break_switch. by right.

      all: try by []. by simpl_trace.
    - replace (seq_of_labeled_statement _)
          with ((gen_fp_stage stage)
                    @+@ (seq_of_labeled_statement
                            (gen_fp_stages (stages' ++ [:: default]))));
          last first. by destruct stage.
      have Hget: get_current_stage fpe (` e1) = stage.
      { rewrite /get_current_stage
                  /FP_E.get_stage.
        by apply (get_stage_drop _ _ Hwf Heqstages).  }

      have Hremain : remaining_stages (` e1) =
        (stage :: stages') ++ [:: default]
      by rewrite /remaining_stages Heqstages Heqdefault.

      edestruct (eq_exec_stage le He Hr Hget Hremain)
        as [e2' [le' [Hs He']]].

      exists e2', le'; split. move => k Hk. step_seq. by apply Hs.
      eapply Smallstep.star_one.
      apply step_skip_break_switch. by right.

      all: try by []. by simpl_trace.
  Qed.

  (** Execution of the stages *)
  Lemma eq_exec_run_step:
    forall e1 e1' e2 le,
      e1 ~cenv~ (ge, e2)
      -> run_step fp (` e1) = ` e1'
      -> let stages :=
          FP_E_WF.get_block_stages
            (FP_E.get_block fpe (get_nav_block (` e1))) in
      exists e2' le',
        (forall k, is_call_cont k
        -> Smallstep.star step2 ge
          (State f (Sswitch tmpNavStage (gen_fp_stages stages))
            (block_cont_e (` e1) k) ev (update_le_NavStage (` e1) le) (e2.m))
          (fp_trace_to_trace (extract_trace (` e1) (` e1')))
          (State f Sskip (block_cont_e (` e1) k) ev le' (e2'.m)))
        /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 le He Hr stages.
    have He8 := proj2_sig e1.

    remember (drop (get_nav_stage (` e1))
                    (take ((Datatypes.length stages).-1) stages))
        as stages'.

    edestruct (eq_exec_stages (update_le_NavStage (` e1) le) He Hr)
      as [e2' [le'  [Hs' He']]].

    exists e2', le'; split; try by [].

    move => k Hk. star_step step_switch.
    apply get_update_le_NavStage. by rewrite //=.

    remember (get_nav_stage (` e1)) as ids.

    rewrite (seq_of_labeled_statement_fp_stage _ Hsize Hwf).
    rewrite fold_nav_stage -Heqids -Heqstages'.

    step_trans. rewrite /remaining_stages in Hs'.
    rewrite Heqstages' /stages Heqids. by apply Hs'. step_refl.

    by [].

    rewrite fold_nav_stage. apply (max_unsigned_256 (nav_stage8 He8)).

    by simpl_trace.
  Qed.

  (** Execution of the block *)
  Lemma eq_exec_blocks_lexcpt_not_raised:
      forall e1 (e1' e1'': fp_env8) e2 le,
        e1 ~cenv~ (ge, e2)
        -> test_exceptions fpe (` e1)
                (get_local_exceptions fpe (` e1))
                  = (false,` e1')
        -> run_step fp (` e1') = ` e1''
        -> let pre := (FP_E_WF.get_code_block_pre_call
                            (get_current_block fpe (` e1))) in
            let post := (FP_E_WF.get_code_block_post_call
                            (get_current_block fpe (` e1'))) in
            let t := fp_trace_to_trace
                      (pre ++ (extract_trace (` e1) (` e1'))
                            ++ (extract_trace (` e1') (` e1''))
                            ++  post) in
            exists e2'' le',
            (forall k, is_call_cont k
            -> Smallstep.star step2 ge
                (State (gen_fp_auto_nav fpe)
                  (gen_fp_block (FP_E.get_block fpe
                          (get_nav_block (` e1))))
                  (blocks_cont_e (` e1) k) empty_env le (e2.m)) t
                (State (gen_fp_auto_nav fpe) Sbreak
                    (blocks_cont_e (` e1) k) empty_env le' (e2''.m)))
              /\ e1'' ~cenv~ (ge, e2'').
  Proof.
    move => e1 e1' e1'' e2 le He Hle Hr pre post t.

    destruct (FP_E.get_block _ _)
      as [name id stages excs  pre' post' on_enter on_exit] eqn:Hblock.

    rewrite /gen_fp_block //=.
    rewrite /t. repeat rewrite trace_app.

    (** Execute the C light code for the local exceptions *)
    rewrite /get_local_exceptions
              /get_current_block Hblock //= in Hle.
    have Hexs8:  Forall (correct_excpt fpe) excs.
    { have Hid1 := nav_block8 (proj2_sig e1). to_nat Hid1.
      have H:= (get_correct_lexcpts
          (get_well_sized_blocks Hsize (get_nav_block (` e1)))).
      by rewrite Hblock in H. }
    destruct (eq_exec_exceptions le He Hexs8 Hle)
      as [e2'' [le' [Hex He2'']]].
    have Heq := no_error_same_block Hle. 

    (** Execute the access to the NavStage variable *)
    have Hstage := step_get_NavStage le' Hcev
      (proj2_sig e1') (proj1 He2'').

    (** Execution of the C code for the stages *)
    edestruct (eq_exec_run_step le' He2'' Hr)
      as [e2''' [le'' [Hrc He2''']]].

    exists e2''', le''; split. move => k Hk.

    (** Execution of the pre option code *)
    star_step step_seq.
    eapply Smallstep.star_trans with (t1 := exec_to_trace pre').

    { destruct pre' as [c_pre'|].
      - apply exec_arbitrary_C_code.
      - constructor.
    }

    step_skip_seq.

    (** Execution of the local exceptions *)
    step_seq. apply Hex. step_skip_seq.

    (** Get the value of the tmpNavStage variables *)
    step_seq. apply Hstage. step_skip_seq.

    (** Execution of the nav stage *)
    apply Hrc in Hk.
    step_seq. rewrite /block_cont_e /block_cont /b_cont
                          -Heq Hblock blocks_cont_e_eq //= in Hk.
    apply Hk. step_skip_seq.

    (** Execution of the post option code *)
    star_step step_seq.
    eapply Smallstep.star_trans with (t1 := exec_to_trace post').

    { destruct post' as [c_post'|].
      - apply exec_arbitrary_C_code.
      - constructor.
    }

    step_skip_seq. constructor.

    (** Prove the matching relation between the envrionment *)
    all: try by [].

    by rewrite ?E0_right /pre /post /get_current_block
                 -Heq ?Hblock /FP_E_WF.get_code_block_post_call
                 /FP_E_WF.get_code_block_pre_call ?exec_to_trace_fp_trace.
  Qed.

  (** Execution of the default block *)

  Lemma eq_exec_default_block:
    forall e1 e1' e2 le,
      e1 ~cenv~ (ge, e2)
      -> run_step fp (` e1) = ` e1'
      -> (FP_E.get_nb_blocks fpe - 1 <= get_nav_block (` e1))%coq_nat
      -> let t := fp_trace_to_trace (extract_trace (` e1) (` e1')) in
          exists e2' le',
          (forall k, is_call_cont k
          -> Smallstep.star step2 ge
              (State (gen_fp_auto_nav fpe)
                (gen_fp_block (FP_E.get_default_block fpe))
                (Kseq Sskip (Kswitch k)) empty_env le
                (e2.m))
              t
              (State (gen_fp_auto_nav fpe) Sbreak
                     (Kseq Sskip (Kswitch k)) empty_env le' (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 le He Hr Hlt t.
    rewrite /FP_E.get_nb_blocks in Hlt. to_nat Hlt.

    (** Execute the access to the NavStage variable *)
    have Hstage := step_get_NavStage le Hcev (proj2_sig e1) (proj1 He).

    (** Execution of the C code for the stages *)
    edestruct (eq_exec_run_step le He Hr)
      as [e2' [le' [Hrc He2']]].

    rewrite /FP_E.get_block nth_overflow /= in Hrc; try ssrlia.

    exists e2', le'. split. move => k Hk.

    have Hc: block_cont_e (` e1) k
      = Kseq (Sskip @+@ Sbreak)
              (Kseq Sskip (Kswitch k)).
    { rewrite /block_cont_e /block_cont /blocks_cont
              /blocks_cont_gen /b_cont /b_cont_gen
              /FP_E.get_block nth_overflow /=
              ?drop_oversize //.
      rewrite size_eq_length last_length.
      all: rewrite /ClightLemmas.fpe; rewrite /fpe in Hlt.
      all: ssrlia. }
    have Hrc' := Hrc k Hk. rewrite Hc in Hrc'.

    (** Execution of the pre call *)
    step_seq. step_skip_seq.

    (** Execution of the exceptions *)
    step_seq. step_skip_seq.

    (** Get the nav stage*)
    step_seq. apply Hstage. step_skip_seq.

    (** Execute the stages *)
    step_seq. apply Hrc'. step_skip_seq.

    (** Execution of the post call *)
    step_seq. step_skip_seq.

    all: try step_refl.
    all: try by [].

    simpl_trace.
  Qed.

  Lemma eq_step_auto_nav:
      forall e1 e1' e2 k,
        e1 ~cenv~ (ge, e2)
        -> step fp (` e1) = ` e1'
        -> let t := fp_trace_to_trace (extract_trace (` e1) (` e1')) in
            let le :=
              (create_undef_temps (fn_temps (gen_fp_auto_nav fpe))) in
            exists e2',
              (forall f_call e le_ext,
                let K := Kcall None f_call e le_ext k in
              Smallstep.star step2 ge
                (State (gen_fp_auto_nav fpe)
                        (fn_body (gen_fp_auto_nav fpe))
                        K empty_env le (e2.m))
                t
                (State f_call Sskip k e le_ext (e2'.m)) )
              /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1'_f e2 k  He Hs t le.
    have He8 := proj2_sig e1.

    rewrite /step /exception in Hs.

    (** Test of the global exceptions *)
    remember (FP_E_WF.Common.get_fp_exceptions _) as exs.
    destruct (test_exceptions8 fps e1 exs) as [b [e1' Hexg]].
    subst exs. rewrite Hexg in Hs.

    destruct (eq_exec_exceptions le He
      (get_correct_gexcpts Hsize) Hexg) as [e2' [le' [Hexs He']]].
    destruct b.

    (** Global exception raised *)
    { apply env8_proj in Hs. subst e1'_f. exists e2'; split; try by [].

      move => f_call e le_ext K. step_seq; [apply Hexs | | |].
      econstructor. constructor; try by [].

      rewrite //=. step_refl.

      all: by rewrite ?E0_right.
    }

    (** Global exceptions not raised *)
    (** Test Local exceptions *)
    remember (FP_E_WF.Common.get_code_block_pre_call _) as pre.
    remember (app_trace (` e1') pre) as e_pre eqn:Hpre.
    destruct (app_trace_preserve_match Hpre He')
      as [e1'_pre [Heq He'']].
    destruct (test_exceptions8 fps e1'_pre
       (get_local_exceptions (FPE_BS.fp fp) (` e1')))
        as [b [e1'' Hexl]].
    rewrite Heq Hexl in Hs. subst e_pre.

    have Heq' :
      (get_local_exceptions (FPE_BS.fp fp) (` e1')
            = get_local_exceptions (FPE_BS.fp fp) (` e1'_pre)).
    { rewrite -Heq /get_local_exceptions. by destruct e1'. }
    rewrite Heq' in Hexl; clear Heq'.
    have Heq' :get_nav_block (` e1'_pre) = get_nav_block (` e1')
      by rewrite -Heq; destruct e1'.

    (** Get the nav block *)
    have Hnav := step_get_NavBlock le' Hcev (proj2_sig e1') (proj1 He').

    (** Execution of the local execution and the block of the C code *)
    destruct b.

    (*** Local exception raised *)
    { apply env8_proj in Hs. subst e1''.

      (** Get the execution of the block *)
      have Hexec := eq_exec_blocks_lexcpt_raised
                  (update_le_NavBlock (` e1') le') He'' Hexl.
      destruct Hexec as [e2'_f [Hexec He'_f]].
      rewrite Heq' in Hexec.

      eexists. split. move => e le_ext K.

      (** Split the execution of the global exceptions and the switch *)
      step_seq; [apply Hexs | | |].

      step_skip_seq.

      (** Get the nav block *)
      step_seq. apply Hnav. step_skip_seq.

      (** Execution of the switch *)
      star_step step_switch.
      apply get_update_le_NavBlock. by rewrite //=.

      rewrite (seq_lb_stmt_block Hsize (proj2_sig e1')).

      step_seq. apply Hexec.
      step_trans. rewrite //=.

      eapply Smallstep.star_left; [apply step_returnstate | |].
      step_refl.

      all: try by []. step_refl.

      rewrite -Heq'.
      apply (local_exceptions_raised_in_block (` fps) _ Hexl).

      simpl_trace.
      rewrite /t (trace_appended_app (extract_test_exceptions Hexg)).
      rewrite (trace_appended_app (trace_appended_app_trace (` e1') pre)).
      by rewrite extract_app_trace Heq /get_current_block Heq' -Heqpre.

      all: try apply (trace_appended_trans
          (trace_appended_app_trace (` e1') pre)).
      all: try rewrite Heq; try apply (extract_test_exceptions Hexl).

      by [].
    }

    (*** Local exception not raised *)
    (*** Test if the default block is executed *)
    destruct (le_lt_dec (FP_E.get_nb_blocks (` (` fps)) - 1)
                        (get_nav_block (` e1')))
                        as [Hge | Hlt].

    (*** -> Execution of the default block *)
    { rewrite -Heq' in Hge.
      have Heq'': e1'_pre = e1''.
      { apply env8_proj.
        apply no_local_exception_default_block with (fp_wf := (` fps)).
        - ssrlia. apply Hge.
        - by []. } subst e1'_pre.

      have Heq'': e1' = e1''.
      { apply env8_proj.
        rewrite -Heq Heqpre get_current_default_block //.
        - apply app_trace_nil.
        - rewrite -Heq'. to_nat Hge. ssrlia. apply Hge. } subst.

      (** Running the flight plan *)
      destruct (run_step8 fps e1'') as [e1''' Hr].

      have Heq'': e1''' = e1'_f.
      { apply env8_proj.
        rewrite -Hs Hr get_current_default_block //=.
        - apply app_trace_nil.
        - to_nat Hge. ssrlia. apply Hge. } subst.

      (** Get the execution of the default block *)
      edestruct (eq_exec_default_block
                    (update_le_NavBlock (` e1'') le')
                    He'' Hr Hge)
        as [e2'_f [le'' [Hexec He1'']]].

      eexists. split. move => f_call e le_ext K.

      (** Split the execution of the global exceptions and the switch *)
      step_seq; [apply Hexs| rewrite //= | |].

      step_skip_seq.

      (** Get the nav block *)
      step_seq. apply Hnav. step_skip_seq.

      (** Execution of the switch *)
      star_step step_switch.
      apply get_update_le_NavBlock. by rewrite //=.

      rewrite (seq_lb_stmt_default_block Hsize (proj2_sig e1'')).

      step_seq. rewrite -app_ctrace_mem /= in Hexec. apply Hexec.
      by [].

      star_step step_break_seq. star_step step_skip_break_switch.
      by right.

      star_step step_skip_call; try by [].
      star_step step_returnstate. step_refl.

      all: try by []. to_nat Hge. ssrlia.

      simpl_trace. rewrite /t.

      rewrite -trace_appended_app //.
      - apply (extract_test_exceptions Hexg).
      - apply (extract_run_step Hr). }

    (*** -> Running the flight plan *)
    remember (FP_E_WF.get_code_block_post_call _) as post.
    destruct (run_step8 fps e1'') as [e1''' Hr].

    (** Get the execution of the block *)
    edestruct (eq_exec_blocks_lexcpt_not_raised
                  (update_le_NavBlock (` e1') le') 
                  He'' Hexl Hr)
      as [e2'_f [le'' [Hexec He1'']]].
      rewrite -app_ctrace_mem in Hexec.

    (** execute the switch *)

    (** Improve the notations in the proof *)
    eexists. split. move => f_call e le_ext K.

    (** Split the execution of the global exceptions and the switch *)
    step_seq; [apply Hexs| rewrite //= | |].

    step_skip_seq.

    (** Get the nav block *)
    step_seq. apply Hnav. step_skip_seq.

    (** Execution of the switch *)
    star_step step_switch.
    apply get_update_le_NavBlock. by rewrite //=.

    rewrite (seq_lb_stmt_block Hsize (proj2_sig e1')).

    step_seq. rewrite -Heq' seq_lstmt_to_stmt.
    rewrite cat_app -drop_cat_le. by apply Hexec.

    have Hlen: get_nav_block (` e1')
        < Datatypes.length (FP_E.get_fp_blocks (` (` fps))).
    { rewrite /FP_E.get_nb_blocks in Hlt.
      remember (Datatypes.length (FP_E.get_fp_blocks (` (` fps)))) as len. to_nat Hlt. ssrlia. }

    rewrite Heq' (size_eq_length) //.

    replace (COMMON.f fps) with (gen_fp_auto_nav (` (` fps))); try by [].
    rewrite /get_nav_block /get_nav_block_env.

    step_trans. eapply Smallstep.star_two.
    constructor. constructor. by right.
    all: try by [].

    rewrite /K //=.
    eapply Smallstep.star_two.
    apply step_skip_call; try by [].
    rewrite (app_ctrace_mem e2'_f (fp_trace_to_trace post)).
    apply step_returnstate.

    all: try by [].

    to_nat Hlt. ssrlia.

    simpl_trace. rewrite /t -?app_assoc -Heqpost
      /get_current_block Heq' -Heqpre ?cat_app
      -(extract_app_trace (` e1''') post)
      -(trace_appended_app (extract_run_step Hr)
                              (trace_appended_app_trace (` e1''') post)).
    have Hx := trace_appended_trans (extract_run_step Hr)
                            (trace_appended_app_trace (` e1''') post).

    rewrite -(trace_appended_app (extract_test_exceptions  Hexl) Hx).
    have Hx' := trace_appended_trans
      (extract_test_exceptions Hexl) Hx. rewrite -Heq in Hx'.

    remember (app_trace (` e1') pre) as e_pre.
    rewrite -(extract_app_trace (` e1') pre); subst e_pre.
    rewrite -Heq -(trace_appended_app
                (trace_appended_app_trace (`e1') pre) Hx').
    have Hx'' := trace_appended_trans
      (trace_appended_app_trace (` e1') pre) Hx'.

    by rewrite -Hs Hr
      -(trace_appended_app (extract_test_exceptions Hexg) Hx'').

    destruct He1'' as [He''' Ht]; split; rewrite -Hs ?Hr.
    - by apply app_trace_preserve_mmatch_gen.
    - by apply app_ctrace_match.
  Qed.

  Lemma eq_eval_funcall_auto_nav:
    forall e1 e1' e2,
      e1 ~cenv~ (ge, e2)
      -> step fp (` e1) = ` e1'
      -> let t := fp_trace_to_trace (extract_trace (` e1) (` e1')) in
          exists e2',
          (forall f_call e le,
        Smallstep.star step2 ge
              (Callstate (Internal (gen_fp_auto_nav fpe)) [::]
                          (Kcall None f_call e le Kstop) (e2.m))
              t
              (State f_call Sskip Kstop e le  (e2'.m)))
            /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e1 e1' e2 He Hs t.

    (* Execution of the body stmt *)
    destruct (eq_step_auto_nav Kstop He Hs)
      as [e2' [Hex [Hout He']]].

    exists e2'. split; try by []. 
    move => f_call e le. step_trans.

    remember (Kcall None f_call e le Kstop) as k.

    have Hcont: is_call_cont k by rewrite Heqk.

    eapply Smallstep.star_left
      with (t1 := E0)
            (s2 := State (gen_fp_auto_nav fpe)
                             (fn_body (gen_fp_auto_nav fpe))
                              k empty_env
                              (create_undef_temps
                                (fn_temps (gen_fp_auto_nav fpe)))
                              (e2.m)).
    eapply step_internal_function.
    apply function_entry2_intro; try by econstructor; try by [].
    by []. 

    subst k. apply Hex.

    by simpl_trace. constructor. by simpl_trace.
    Qed. 

  (** ** Proof of the semantics preservation for FPE to Clight *)

  Theorem semantics_preservation:
    forall e1 e1',
    FPS_BS.step fps e1 = e1'
    -> forall e2, e1 ~cenv~ (ge, e2)
    -> exists e2', C_BS.step prog e2 e2'
                  /\ e1' ~cenv~  (ge, e2').
  Proof.
    move => e1 e1' Hs e2 He.

    (* Destruct the dependant type *)
    rewrite /FPS_BS.step //= in Hs.
    have Hs' : step fp (` e1) = ` e1'.
    { by inversion Hs as [[Hs']]. }

    (* Get the localisation of the auto_nav function*)
    destruct (get_symb_auto_nav fpe gvars) as [b [Hfs Hfd]].

    (* Execute the aut_nav function *)
    destruct (eq_eval_funcall_auto_nav He Hs')
      as [ce2' [Hstep He2']].

    (* Get trace appended *)
    have Hex := (trace_appended_clight He He2' (extract_step Hs')).

    exists ce2'; split => //.

    (* Enter in the step *)
    apply FP_step with (cge := ge) => //.
    move => f_call [env Henv] le. econstructor.

    (* Execute the function auto_nav *)
    econstructor; try by [].

    (* Evalutation of the Call expresion *)
    { apply (eval_Elvalue _ _ _ _ _ b Ptrofs.zero Full).
      - apply eval_Evar_global; try by [].
      - apply deref_loc_reference; constructor.
    }

    econstructor.

    (* Find the function in the global environment *)
    apply (find_def_to_funct Hfd).

    (* Get the type of the function *)
    by [].

    (* Execute the function *)
    by apply Hstep.

    (* Verify that the fp_trace are correct *)
    simpl_trace. apply (extract_trace_clight He He2' (extract_step Hs')).
  Qed.

  (** * Proof of the init state *)
  Lemma correct_mem_access_init:
    forall e_init p_var,
    C_BIGSTEP.initial_state prog e_init
      -> (let gvar := {|
              gvar_info := tuchar;
              gvar_init := (Init_space 1 :: nil);
              gvar_readonly := false;
              gvar_volatile := false
            |} in
        (prog_defmap prog) ! p_var
              = Some (Gvar gvar))
      -> MATCH_ENV.correct_mem_access ge (get_m_env e_init)
                  p_var (create_val 0).
  Proof.
    move => e_init p_var [Hinit _] Hget b Hfs;
      apply Globalenvs.Genv.init_mem_characterization_gen in Hinit.

    apply Globalenvs.Genv.find_def_symbol in Hget.
    destruct Hget as [b' [Hfs' Hfd]].

    have H := find_symbol_genv Hfs Hfs'; subst b'.

    destruct (Hinit _ _ Hfd) as [Hr [Hp' [Hl Hl']]].

    split.
    - split; [ apply Hr | apply Z.divide_0_r].
    - destruct Hl as [Hl H]; try by []; clear H.
      rewrite /Mem.loadv Hl; try by []. apply Z.divide_0_r.
  Qed.

  Lemma correct_const_access_init:
    forall e_init,
    C_BIGSTEP.initial_state prog e_init
      -> (prog_defmap prog) ! CommonFP._nb_blocks
              = Some (Gvar (gvar_nb_blocks fpe))
      -> MATCH_ENV.correct_const_access ge (get_m_env e_init)
                  CommonFP._nb_blocks.
  Proof.
    move => e_init [Hinit _] Hget b type nb_blocks l Hfs Hfd Hg.
      apply Globalenvs.Genv.init_mem_characterization_gen in Hinit.

    apply Globalenvs.Genv.find_def_symbol in Hget.
    destruct Hget as [b' [Hfs' Hfd']].

    have H := find_symbol_genv Hfs Hfs'; subst b'.

    rewrite Hfd in Hfd'. inversion Hfd'. subst type.


    destruct (Hinit _ _ Hfd) as [_ [_ [Hl _]]].
     simpl in *.
    destruct Hl as [H1 _]; try reflexivity.

    rewrite H1.
    f_equal.
    inversion Hg; subst. 
    rewrite id_zero_ext8.
    - by reflexivity.
    - apply get_nb_blocks8.
      apply Hsize.
  Qed.

  Theorem semantics_preservation_init:
    forall e_init,
      C_BIGSTEP.initial_state prog e_init
      -> (init_env8 fps) ~cenv~ (ge, e_init).
  Proof.
    move => e_init Hinit. split; try by [].
      split; rewrite /MATCH_ENV.correct_const_access //=.
    - apply (correct_mem_access_init Hinit (get_nav_block_var fpe gvars)).
    - apply (correct_mem_access_init Hinit
        (get_private_nav_block fpe gvars)).
    - apply (correct_mem_access_init Hinit (get_nav_stage_var fpe gvars)).
    - apply (correct_mem_access_init Hinit
        (get_private_nav_stage fpe gvars)).
    - apply (correct_mem_access_init Hinit (get_last_block_var fpe gvars)).
    - apply (correct_mem_access_init Hinit
        (get_private_last_block fpe gvars)).
    - apply (correct_mem_access_init Hinit (get_last_stage_var fpe gvars)).
    - apply (correct_mem_access_init Hinit
        (get_private_last_stage fpe gvars)).

    - { move => b d nb l Hfs Hfd Hg.

        have Hnb := GeneratorProperties.get_nb_blocks fpe gvars.
        apply Globalenvs.Genv.find_def_symbol in Hnb.

        destruct Hnb as [b' [Hfs' Hfd']];
          rewrite Hfs in Hfs'; inversion Hfs'; subst b';
          rewrite Hfd in Hfd'; inversion Hfd'; subst d.

        destruct Hinit as [Hinit _].

        apply Globalenvs.Genv.init_mem_characterization_gen in Hinit.
        destruct (Hinit _ _ Hfd) as [Hr [Hp [Hl Hl']]].
        destruct Hl as [Hl H]; try by []; clear H.

        rewrite Hl; try by [].

        inversion Hg; subst nb. rewrite //=; repeat f_equal.
        remember (FP_E.get_nb_blocks _) as nb.

        have H: (nb < 256)%nat.
        { have H:= get_nb_blocks8 Hsize.
          rewrite /nb_blocks_lt_256 /is_nat8 in H.
          to_nat H. rewrite Heqnb /fpe /fp. ssrlia. } to_nat H.

        rewrite /Int.zero_ext. f_equal.
        rewrite Zbits.Zzero_ext_mod ?Zmod_small; try lia.
        - rewrite Int.unsigned_repr //; split; try ssrlia. 
          apply (Z.le_trans) with (m := (256)%Z); ssrlia.
        - rewrite Int.unsigned_repr; split;
            rewrite /Int.max_unsigned; ssrlia.
            rewrite two_power_pos_correct. ssrlia. }

    - destruct Hinit as [_ Hinit]. rewrite Hinit /init_env8 //=.
      by apply MATCH_TRACE.empty_trace.
  Qed.

  Theorem semantics_preservation_inv:
    forall e2 e2',
    C_BS.step prog e2 e2'
    -> forall e1, e1 ~cenv~ (ge, e2)
    -> exists e1', FPS_BS.step fps e1 = e1'
                  /\ e1' ~cenv~ (ge, e2').
  Proof.
    move => e2 e2' Hstep e1 Heq.
    remember (FPS_BS.step fps e1) as e1' eqn:Hstep'.
    symmetry in Hstep'.
    destruct (semantics_preservation Hstep' Heq) as [e2'' [Hstep2 Heq2]].

    have Hp: ge = globalenv prog by rewrite /ge.

    have H := step_deterministic_gen _ _ _ Hstep Hstep2.
    rewrite (H _ Hp).
    - by exists e1'.
    - apply NECge.
    - destruct (get_symb_auto_nav fpe gvars) as [b [Hfs Hfd]].
      exists b; eexists; split => //. 
      split. by apply Hfd.
      apply auto_nav_NEC.
  Qed.

  Theorem initial_state_preservation:
    forall e le e_init,
    initial_env fpe (` e)
    -> C_BIGSTEP.initial_state prog e_init
    -> exists ec mev, 
      (forall f,
      Smallstep.star step2 ge (State f initFunct Kstop ev le (e_init.m))
                (ec.t)
                (State f Sskip Kstop mev le (ec.m)))
      /\ e ~cenv~(ge, ec).
  Proof.
    move => e le e_init Hi He. simpl in *.

    assert (Hmev : (` e) ~menv~ (ge, (e_init .m))).
    {
      inversion Hi;
      split;
      try eapply (correct_mem_access_init He).
      - apply get_nav_block_var.
      - apply get_private_nav_block.
      - apply get_nav_stage_var.
      - apply get_private_nav_stage.
      - apply get_last_block_var.
      - apply get_private_last_block.
      - apply get_last_stage_var.
      - apply get_private_last_stage.
  
      - eapply (correct_const_access_init He).
        apply (GeneratorProperties.get_nb_blocks).
    }

    have Hfun := init_Function_fun fpe gvars.
    eapply Globalenvs.Genv.find_def_symbol in Hfun.

    (** Get private_nav_stage variable *)
    have Hpns := get_private_nav_stage fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpns.
    destruct Hpns as [b_pns [Hfspns Hfdpns]].

    (** Store private_nav_stage *)
    edestruct (Mem.valid_access_store (e_init .m) Mint8unsigned
                    b_pns (Ptrofs.unsigned Ptrofs.zero)) as [m_b_pns Hmem_b_pns].
    { apply (proj1 ((MATCH_ENV.correct_private_nav_stage Hmev) _ Hfspns)). }

    (** Get nav_stage variable *)
    have Hns := get_nav_stage_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hns.
    destruct Hns as [b_ns [Hfsns Hfdns]].

    (** Store nav_stage *)
    edestruct (Mem.valid_access_store m_b_pns Mint8unsigned
          b_ns (Ptrofs.unsigned Ptrofs.zero)) as [m_b_ns Hmem_b_ns].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_b_pns _ _
              _ _ (proj1 ((MATCH_ENV.correct_nav_stage Hmev) _ Hfsns))). } 

    (* spread ~menv~ *)
    assert (Hmev1 : (` e) ~menv~ (ge, (m_b_ns))).
    {
      assert (update_nav_stage (` e) 0 = (` e)); try by inversion Hi.
      rewrite -H.
      eapply update_nav_stage_match.
      - apply Hmev.
      - apply zero8.
      - apply Hfspns.
      - apply Hfsns.
      - apply Hmem_b_pns.
      - apply Hmem_b_ns.
    }

    clear Hmev; rename Hmev1 into Hmev.

    (** Get private_nav_block variable *)
    have Hpnb := get_private_nav_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpnb.
    destruct Hpnb as [b_pnb [Hfspnb Hfdpnb]].

    (** Store private_nav_block *)
    edestruct (Mem.valid_access_store (m_b_ns) Mint8unsigned
                    b_pnb (Ptrofs.unsigned Ptrofs.zero)) as [m_b_pnb Hmem_b_pnb].
    { apply (proj1 ((MATCH_ENV.correct_private_nav_block Hmev) _ Hfspnb)). }

    (** Get nav_block variable *)
    have Hnb := get_nav_block_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hnb.
    destruct Hnb as [b_nb [Hfsnb Hfdnb]].

    (** Store nav_block *)
    edestruct (Mem.valid_access_store m_b_pnb Mint8unsigned
          b_nb (Ptrofs.unsigned Ptrofs.zero)) as [m_b_nb Hmem_b_nb].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_b_pnb _ _
              _ _ (proj1 ((MATCH_ENV.correct_nav_block Hmev) _ Hfsnb))). } 
    
    assert (Hmev1 : (` e) ~menv~ (ge, (m_b_nb))).
    {
      assert (update_nav_block (` e) 0 = (` e)); try by inversion Hi.
      rewrite -H.
      eapply update_nav_block_match.
      - apply Hmev.
      - apply zero8.
      - apply Hfspnb.
      - apply Hfsnb.
      - apply Hmem_b_pnb.
      - apply Hmem_b_nb.
    }

    clear Hmev; rename Hmev1 into Hmev.

    (** Get private_last_stage variable *)
    have Hpls := get_private_last_stage fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hpls.
    destruct Hpls as [b_pls [Hfspls Hfdpls]].

    (** Store private_nav_block *)
    edestruct (Mem.valid_access_store (m_b_nb) Mint8unsigned
                    b_pls (Ptrofs.unsigned Ptrofs.zero)) as [m_b_pls Hmem_b_pls].
    { apply (proj1 ((MATCH_ENV.correct_private_last_stage Hmev) _ Hfspls)). }

    (** Get last_stage variable *)
    have Hls := get_last_stage_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hls.
    destruct Hls as [b_ls [Hfsls Hfdls]].

    (** Store nav_block *)
    edestruct (Mem.valid_access_store m_b_pls Mint8unsigned
          b_ls (Ptrofs.unsigned Ptrofs.zero)) as [m_b_ls Hmem_b_ls].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_b_pls _ _
              _ _ (proj1 ((MATCH_ENV.correct_last_stage Hmev) _ Hfsls))). } 

    assert (Hmev1 : (` e) ~menv~ (ge, (m_b_ls))).
    {
      assert (update_last_stage (` e) 0 = (` e)); try by inversion Hi.
      rewrite -H.
      eapply update_last_stage_match.
      - apply Hmev.
      - apply zero8.
      - apply Hfspls.
      - apply Hfsls.
      - apply Hmem_b_pls.
      - apply Hmem_b_ls.
    }

    clear Hmev; rename Hmev1 into Hmev.

    (** Get private_last_block variable *)
    have Hplb := get_private_last_block fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hplb.
    destruct Hplb as [b_plb [Hfsplb Hfdplb]].

    (** Store private_last_block *)
    edestruct (Mem.valid_access_store (m_b_ls) Mint8unsigned
                    b_plb (Ptrofs.unsigned Ptrofs.zero)) as [m_b_plb Hmem_b_plb].
    { apply (proj1 ((MATCH_ENV.correct_private_last_block Hmev) _ Hfsplb)). }

    (** Get last_block variable *)
    have Hlb := get_last_block_var fpe gvars.
    apply Globalenvs.Genv.find_def_symbol in Hlb.
    destruct Hlb as [b_lb [Hfslb Hfdlb]].

    (** Store last_block *)
    edestruct (Mem.valid_access_store m_b_plb Mint8unsigned
          b_lb (Ptrofs.unsigned Ptrofs.zero)) as [m_b_lb Hmem_b_lb].
    { apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hmem_b_plb _ _
              _ _ (proj1 ((MATCH_ENV.correct_last_block Hmev) _ Hfslb))). } 

    assert (Hmev1 : (` e) ~menv~ (ge, (m_b_lb))).
    {
      assert (update_last_block (` e) 0 = (` e)); try by inversion Hi.
      rewrite -H.
      eapply update_last_block_match.
      - apply Hmev.
      - apply zero8.
      - apply Hfsplb.
      - apply Hfslb.
      - apply Hmem_b_plb.
      - apply Hmem_b_lb.
    }

    clear Hmev; rename Hmev1 into Hmev.
    assert (m_b_lb = ({| get_m_env := m_b_lb; C_ENV.get_trace := [:: ]|} .m)) by reflexivity.

    rewrite H in Hmem_b_lb.

    destruct Hfun as [block [Hfs Hfd]].
    eexists; eexists; split.
    intros f0.

    (* Access to the body of the function *)
    econstructor. econstructor; try by []. econstructor.

    eapply eval_Evar_global; try by [].

    apply Hfs.
    by eapply deref_loc_reference.
    
    apply eval_Enil.

    apply find_def_to_funct. apply Hfd. by [].

    clear Hfs; clear Hfd; clear block.

    econstructor; try by [].
    econstructor.
    econstructor; try by [].
    - apply Coqlib.list_norepet_nil.
    - apply Coqlib.list_norepet_nil.
    - econstructor. rewrite //=.

    (** assign privat_nav_stage *)
    star_step step_seq; try by [].
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfspns.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_pns.

    star_step step_skip_seq; try by [].

    (** assign nav_stage *)
    star_step step_seq; try by [].
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfsns.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_ns.

    star_step step_skip_seq; try by [].

    clear Hmem_b_ns; clear Hfdns; clear Hfsns; clear b_ns.
    clear Hmem_b_pns; clear m_b_pns; clear Hfspns; clear Hfdpns;
      clear b_pns.

    (** assign private_nav_block *)
    star_step step_seq; try by [].
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfspnb.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_pnb.

    star_step step_skip_seq; try by [].

    (** assign nav_block *)
    star_step step_seq; try by [].
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfsnb.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_nb.

    star_step step_skip_seq; try by [].

    clear Hmem_b_nb; clear Hfdnb; clear Hfsnb; clear b_nb.
    clear Hmem_b_pnb; clear m_b_pnb; clear Hfspnb; clear Hfdpnb;
      clear b_pnb.

    (** assign private_last_stage *)
    star_step step_seq; try by [].
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfspls.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_pls.

    star_step step_skip_seq; try by [].

    (** assign last_stage *)
    star_step step_seq; try by [].
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfsls.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_ls.

    star_step step_skip_seq; try by [].

    clear Hmem_b_ls; clear Hfdls; clear Hfsls; clear b_ls.
    clear Hmem_b_pls; clear m_b_pls; clear Hfspls; clear Hfdpls;
      clear b_pls.

    (** assign private_last_block *)
    star_step step_seq; try by [].
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfsplb.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_plb.

    star_step step_skip_seq; try by [].

    (** assign last_block *)
    eapply Smallstep.star_left; try by [].
    econstructor; try by [].
    apply eval_Evar_global; try by [].
    apply Hfslb.
    constructor.
    constructor.

    econstructor; try by []. eapply Hmem_b_lb.

    clear Hmem_b_lb; clear Hfdlb; clear Hfslb; clear b_lb.
    clear Hmem_b_plb; clear m_b_plb; clear Hfsplb; clear Hfdplb;
      clear b_plb.
    
    star_step step_skip_call; try by [].
    star_step step_returnstate; try by [].
    step_refl.
    reflexivity.
    split.
    apply Hmev.
    inversion Hi. apply MATCH_TRACE.empty_trace.
  Qed.

  (** ** Bisimulation *)
  Theorem bisim_transf_extended_correct:
    FPBigStepGeneric.bisimulation
                            (FPS_BS.semantics_fps fps)
                            (C_BS.semantics_fpc prog).
  Proof.
    apply FPBigStepGeneric.Bisimulation
      with (@MATCH_ENV.match_fp_cenv ge); do 2 split.
    - move => e1 H.
      destruct (semantics_init_exists fps gvars) as [e_init Hinit].
      exists e_init; split; try by [].
      rewrite -H. by apply semantics_preservation_init.
    - by apply semantics_preservation.
    - move => e1 H. exists (init_env8 fps); split => //.
      split => //. by apply semantics_preservation_init.
      inversion H as [H' H'']. rewrite H'' /=. 
      by apply MATCH_TRACE.empty_trace.
    - by apply semantics_preservation_inv.
  Qed.

End FLIGHT_PLAN.

End FPS_TO_C_VERIF.
