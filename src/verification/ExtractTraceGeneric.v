From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.


Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas BasicTypes
                                FPEnvironmentGeneric.
Local Open Scope nat_scope.
Local Open Scope list_scope.
Import ListNotations.

Set Implicit Arguments.

(** * Generic Definition for the trace extraction of a environment *)

(** * Module type of env containing trace *)
Module Type ENV_TRACE.
  (** A trace is a list of generic event *)
  Parameter event: Type.
  Definition trace := list event.

  (** Environments that contain trace *)
  Parameter env: Type.
  Parameter get_trace: env -> trace.

End ENV_TRACE.

(** * Definition of the properties about env trace*)

Module EXTRACT_TRACE_GEN (ENV: ENV_TRACE).
  Import ENV.

  (** Extract the trace generated between [e] and [e']. *)
  Definition extract_trace (e: env) (e': env): trace :=
    drop (length (get_trace e)) (get_trace e').

  (** Test if the trace of [e'] corresponds to the trace of [e] with [t]
  appended. *)
  Definition is_extract_trace (e e': env) (t: trace): Prop :=
    get_trace e' = ((get_trace e) ++ t)%list.

  (** Test if [e'] has the same trace with new elements than [e]. *)
  Definition trace_appended (e e': env): Prop :=
    is_extract_trace e e' (extract_trace e e').

  (** * Common lemmas *)

  (** ** Lemas about extract_trace *)

  Lemma extract_trace_refl:
    forall e1,
      extract_trace e1 e1 = [].
  Proof.
    move => e1.
    by apply drop_size.
  Qed.

  (** ** Lemmas about is_extract_trace *)

  Lemma get_extract_trace:
    forall e e' t,
      is_extract_trace e e' t
      -> extract_trace e e' = t.
  Proof.
    rewrite /extract_trace => e e' t H.
    by rewrite H drop_size_cat.
  Qed.

  Lemma is_extract_trace_refl:
    forall e,
      is_extract_trace e e [].
  Proof.
    rewrite /is_extract_trace => e.
    by apply app_nil_end.
  Qed.

  Lemma is_extract_trace_conc:
    forall e e' e'' t t',
      is_extract_trace e e' t
      -> is_extract_trace e' e'' t'
      -> is_extract_trace e e'' (t ++ t')%list.
  Proof.
    rewrite /is_extract_trace => e e' e'' t t' H H'.
    by rewrite H' H app_assoc.
  Qed.

  (** ** Lemmas about trace_appended *)

  Lemma trace_appended_refl:
    forall e, trace_appended e e.
  Proof.
    move => e. rewrite /trace_appended extract_trace_refl.
    apply is_extract_trace_refl.
  Qed.

  Lemma trace_appended_trans:
    forall e e' e'',
      trace_appended e e'
      -> trace_appended e' e''
      -> trace_appended e e''.
  Proof.
    rewrite /trace_appended => e e' e'' H H'.
    by rewrite /is_extract_trace H' H
                  (get_extract_trace (is_extract_trace_conc H H'))
                  app_assoc.
  Qed.

  Lemma trace_appended_app:
    forall e e' e'',
      trace_appended e e'
      -> trace_appended e' e''
      -> extract_trace e e''
            = (extract_trace e e') ++ (extract_trace e' e'').
  Proof.
    rewrite /trace_appended
              /is_extract_trace /extract_trace => e e' e'' H H'.
    rewrite H' drop_cat_le. f_equal.
    by rewrite drop_cat_le -size_eq_length // drop_size.
    rewrite H size_cat size_eq_length. ssrlia.
  Qed.

  Lemma eq_extract_trace:
    forall e e1 e2,
      trace_appended e e1
      -> trace_appended e e2
      -> extract_trace e e1 = extract_trace e e2
      -> get_trace e1 = get_trace e2.
  Proof.
    rewrite /trace_appended
            /is_extract_trace /extract_trace => e e1 e2 H H' H''.
    rewrite H H' ?drop_size_cat ?size_eq_length // in H''.
    by rewrite H H' H''.
  Qed.

End EXTRACT_TRACE_GEN.
