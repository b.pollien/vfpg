From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".


From VFP Require Import CommonSSRLemmas
                        BasicTypes
                        FPNavigationMode
                        FlightPlanGeneric
                        FlightPlan
                        FlightPlanExtended
                        FPExtended
                        FPEnvironmentGeneric
                        FPEnvironmentDef.

Local Open Scope list_scope.
Local Open Scope string_scope.
Import ListNotations.
Require Import FunInd.

Set Implicit Arguments.

(** * Definition of the relation between FP env and FPE env *)

Module MATCH_FP_FPE (EVAL_Def: EVAL_ENV)
                              (ENVS_Def: ENVS_DEF EVAL_Def).

Import EVAL_Def ENVS_Def FP_ENV FPS_ENV FPE_ENV.

  Section FLIGHT_PLAN.

    Variable fp: FP.flight_plan.
    Variable fp_e_wf: FP_E_WF.flight_plan_wf.

    Definition fpe := ` fp_e_wf.

    (** Property about the Mathing relation between a FP.fp_stage and  
        a FP_E.fp_stage *)
    Variant match_stage: FP.fp_stage -> FP_E.fp_stage -> Prop :=
    | match_set: forall ids p,
      match_stage (FP.SET p) (FP_E.SET ids p)
    | match_call: forall ids p,
      match_stage (FP.CALL p) (FP_E.CALL ids p)
    | match_deroute: forall ids p,
      match_stage (FP.DEROUTE p) (FP_E.DEROUTE ids p)
    | match_return: forall ids p,
      match_stage (FP.RETURN p) (FP_E.RETURN ids p)
    | match_nav: forall ids p u,
      match_stage (FP.NAV p u false) (FP_E.NAV ids p u).

      (** Property about the matching between a list of FP.stage and 
          a list of FP_E.fp_stage *)
      Inductive match_stages:
        list FP.fp_stage -> list FP_E.fp_stage -> Prop :=
      | match_nil: match_stages [] []
      | match_cons: forall s s_e stages stages_e,
        match_stage s s_e
        -> match_stages stages stages_e
        -> match_stages (s :: stages) (s_e :: stages_e)
      | match_nav_init: forall stages stages_e p u ids ids',
        match_stages stages stages_e 
        -> match_stages (FP.NAV p u true :: stages)
                        (FP_E.NAV_INIT ids p 
                         :: FP_E.NAV ids' p u ::stages_e)
      | match_end_while: forall p p_e b b' ids stages stages',
        p = FP_E.get_while_cond p_e
        -> match_stages b b'
        -> match_stages stages stages'
        -> match_stages (FP.WHILE p b :: stages)
                        (FP_E.END_WHILE ids p_e b' :: stages')
      | match_while: forall p p_e p_e' b b' stages stages_e n nb n' n'',
        nb = (FP_E.get_end_while_id p_e) - ((FP_E.get_while_id p_e) + 1)
        -> p = FP_E.get_while_cond p_e
        -> p = FP_E.get_while_cond p_e'
        -> match_stages b (take nb stages_e)
        -> List.nth nb stages_e (FP_E.DEFAULT n'')
            = FP_E.END_WHILE n' p_e' b'
        -> match_stages b b'
        -> match_stages stages (drop (1 + nb) stages_e)
        -> match_stages (FP.WHILE p b :: stages)
                        (FP_E.WHILE n p_e :: stages_e).

      (** Property that the extended stages must have a default stage
          at the end *)
      Inductive match_stages_default:
        list FP.fp_stage -> list FP_E.fp_stage -> Prop :=
        | match_default:  forall stages stages_e n,
          match_stages stages stages_e
          -> match_stages_default stages (stages_e ++ [FP_E.DEFAULT n]).

      (** Match of the FP.fp_stages and the stages in the flight plan
          exended of the block [idb] from the id [ids] to the end. *)
      Definition match_env_stages (stages: exec_stages) (idb: block_id)
                          (ids: stage_id) : Prop :=
        match_stages_default stages (drop ids (FP_E.get_stages fpe idb)).

    Record match_env (e: FP_ENV.fp_env) (e': FPE_ENV.fp_env) :=
      create_match_env {
        get_match_block:
          FP_ENV.Def.get_nav_block e = FPE_ENV.Def.get_nav_block e';
        get_match_stages: match_env_stages (FP_ENV.get_nav_stages e)
                                  (FPE_ENV.Def.get_nav_block e')
                                  (FPE_ENV.get_nav_stage e');
        get_match_last_block:
          FP_ENV.get_last_block e = FPE_ENV.get_last_block e';
        get_match_last_stages: match_env_stages (FP_ENV.get_last_stages e)
                                      (FPE_ENV.get_last_block e')
                                      (FPE_ENV.get_last_stage e');
        get_match_trace: FP_ENV.Def.get_trace e = FPE_ENV.Def.get_trace e'
      }.

  End FLIGHT_PLAN.

  Notation "e1 '~(' fp_e_wf ')~' e2" := (match_env fp_e_wf e1 e2)
                                              (at level 10).

  (** * Usefull lemmas about the matching property *)
  Lemma match_change_trace:
    forall fpe e e' t,
      e ~( fpe )~ e'
      -> (FP_ENV.Def.change_trace e t) 
            ~( fpe )~ (FPE_ENV.Def.change_trace e' t).
  Proof.
    move => fpe e e' t H. destruct e, e', H. by split.
  Qed.

  Lemma match_app_trace:
    forall fpe e e' t,
      e ~( fpe )~ e'
      -> (FP_ENV.Def.app_trace e t) 
            ~( fpe )~ (FPE_ENV.Def.app_trace e' t).
  Proof.
    move => fpe e e' t H. destruct e, e'.
    destruct H as [Hb Hs Hlb Hls Ht]. 
    split; try by []. rewrite //= in Ht. by rewrite Ht.
  Qed.

  Lemma match_app_trace':
    forall fpe e e' t,
      (FP_ENV.Def.app_trace e t) 
            ~( fpe )~ (FPE_ENV.Def.app_trace e' t)
      -> e ~( fpe )~ e'.
  Proof.
    move => fpe e e' t H. destruct e, e'.
    destruct H as [Hb Hs Hlb Hls Ht]. 
    split; try by []. rewrite //= in Ht. 
    apply app_inv_tail_iff in Ht. by rewrite Ht.
  Qed.

  Lemma eq_get_code_block_pre_call:
    forall fp fpe e e',
      fpe = extend_flight_plan fp
      -> e ~( fpe )~ e'
      -> FP.get_code_block_pre_call (FP_ENV.Def.get_current_block fp e)
        = FP_E_WF.get_code_block_pre_call
            (FPE_ENV.Def.get_current_block (` fpe) e').
  Proof.
    rewrite /FP_ENV.Def.get_current_block /FPE_ENV.Def.get_current_block
    => fp fpe e e' Hfp He.
    rewrite (get_match_block He) /FP.get_code_block_pre_call
                                         /FP_E_WF.get_code_block_pre_call.
    by rewrite (FPE_unchanged_pre_call (Def.get_nav_block e') Hfp).
  Qed.

  Lemma eq_get_code_block_post_call:
    forall fp fpe e e',
      fpe = extend_flight_plan fp
      -> e ~( fpe )~ e'
      -> FP.get_code_block_post_call (FP_ENV.Def.get_current_block fp e)
        = FP_E_WF.get_code_block_post_call
            (FPE_ENV.Def.get_current_block (` fpe) e').
  Proof.
    rewrite /FP_ENV.Def.get_current_block /FPE_ENV.Def.get_current_block
    => fp fpe e e' Hfp He.
    rewrite (get_match_block He) /FP.get_code_block_post_call
                                         /FP_E_WF.get_code_block_post_call.
    by rewrite (FPE_unchanged_post_call (Def.get_nav_block e') Hfp).
  Qed.

  Ltac simpl_match :=
    try rewrite //=;
    try apply match_nav_init;
    try apply match_cons;
    try repeat apply match_default;
    try apply match_set;
    try apply match_call;
    try apply match_deroute;
    try apply match_return;
    try apply match_nav_init;
    try apply match_nav;
    try apply match_nil.

  (** ** Lemmas about match_stages *)

  Lemma match_cons_stages:
    forall s s_e stages stages_e,
      match_stages [s] s_e
      -> match_stages stages stages_e
      -> match_stages (s :: stages) (s_e ++ stages_e).
  Proof.
    move => s s_e.
    induction s_e; move => stages stages_e Hs Hstages; inversion Hs.
    - inversion H4. simpl_match; try by [].
    - simpl_match. by inversion H0.
    - apply match_end_while; try by []. by inversion H5.
    - have Hsize : ssrnat.leq nb (size s_e).
      { inversion H9; try by [].
        apply nth_drop_impl_size in H7; try by [].
        rewrite H7. by repeat apply ssrnat.leqW. }
      apply match_while with (nb := nb) (n' := n') (b' := b')
                              (n'' := n'') (p_e' := p_e'); try by [].
      * rewrite takel_cat; try by [].
      * apply nth_find; by [].
      * inversion H9.
        apply drop_oversize_app with (l' := stages_e) in H7; try by [].
        by rewrite H7.
  Qed.

  Lemma match_fp_fpe_stage:
    forall s1 s2 s2',
      equiv_fpe_stage s2 s2'
      -> match_stage s1 s2
      -> match_stage s1 s2'.
  Proof.
    move => s1 s2 s2' H H'. 
    inversion H; try (rewrite -H0 in H'; inversion H'; simpl_match).
    all: try (rewrite -H2 in H'; inversion H'; simpl_match).
  Qed.

  (** * Lemmas about the fpe_stages that use seq function *)

  Lemma equiv_fpe_stages_take:
    forall n s s',
      equiv_fpe_stages s s'
      -> equiv_fpe_stages (take n s) (take n s').
  Proof.
    induction n as [| n IHn]; move => s s' Hs.
    - repeat rewrite take0; simpl_equiv_fpe.
    - destruct s as [|x s]; destruct s' as [|x' s'];
        try inversion Hs; try by []; rewrite //=.
      simpl_equiv_fpe; try by []; by apply IHn. 
  Qed.

  Lemma equiv_fpe_stages_nth:
    forall n s s' d e d',
      equiv_fpe_stages s s'
      -> List.nth n s d = e
      -> d <> e
      -> equiv_fpe_stage e (List.nth n s' d').
  Proof.
    induction n as [|n IHn]; move => [|x s] [|x' s'] d e d' Hs Hn Hde;
        try by []; inversion Hs.
    - rewrite //= in Hn. rewrite -Hn //=.
    - rewrite //=. by apply IHn with (s := s) (d := d).
  Qed.

  Lemma equiv_fpe_stages_drop:
    forall n s s',
      equiv_fpe_stages s s'
      -> equiv_fpe_stages (drop n s) (drop n s').
  Proof.
    induction n as [| n IHn]; move => s s' Hs.
    - repeat rewrite drop0; simpl_equiv_fpe; by [].
    - destruct s as [|x s]; destruct s' as [|x' s'];
        try inversion Hs; try by []; rewrite //=.
      simpl_equiv_fpe; try by []; by apply IHn. 
  Qed.

  (*****************)

  Lemma equiv_fpe_stages_nav_init:
    forall ids1 ids1' p u stages s s' stages',
      equiv_fpe_stages
        [:: FP_E.NAV_INIT ids1 p, FP_E.NAV ids1' p u & stages]
        [:: s, s' & stages']
      -> exists ids2 ids2', s = FP_E.NAV_INIT ids2 p
                      /\ s' = FP_E.NAV ids2' p u.
  Proof.
    move => ids1 ids2' p u stages s s' stages' H. inversion H.
    inversion H3. inversion H5. inversion H12.
    by exists ids', ids'0.
  Qed.
  
  Lemma match_fp_fpe_stages:
    forall s1 s2,
      match_stages s1 s2
      -> (forall s2', equiv_fpe_stages s2 s2'
                      -> match_stages s1 s2').
  Proof.
    move => s1 s2 H. induction H; move => s2 H'.
    - inversion H'; simpl_match.
    - destruct s2 as [| s' s2'] eqn:Hs2; inversion H'.
      simpl_match; try by apply (IHmatch_stages).
      by apply match_fp_fpe_stage with (s2 := s_e).
    - inversion H'. destruct stages' as [| s'' stages''] eqn:Hs'.
      * inversion H4.
      * rewrite -H3 in H'. apply equiv_fpe_stages_nav_init in H'.
        destruct H' as [ids_eq [ids_eq' [Hs_eq' Hs_eq'']]];
          rewrite Hs_eq' Hs_eq''.
        apply match_nav_init. apply IHmatch_stages. by inversion H4.
      * inversion H'. inversion H4. apply match_end_while.
        + by rewrite -H11.
        + by apply IHmatch_stages1.
        + by apply IHmatch_stages2.
    - inversion H'. inversion H8.

      (* End while*)
      have Hne: FP_E.DEFAULT n'' <> FP_E.END_WHILE n' p_e' b';
        try by [].
      have Hnth:= equiv_fpe_stages_nth (FP_E.DEFAULT n') H10 H3 Hne.
      rewrite -H0 in H13.
      inversion Hnth.

      apply match_while with (nb := nb) (n' := ids'0) (b' := b'0)
                            (n'' := n') (p_e' := p'0); try lia; try by [].
      * by rewrite H.
      * by rewrite H1.
      * apply IHmatch_stages1; by apply equiv_fpe_stages_take.
      * by apply IHmatch_stages2.
      * apply IHmatch_stages3. by apply equiv_fpe_stages_drop.
Qed.


  (** ** Lemmas about relation between match_stages and extended stages *)

  Lemma match_extended_nil:
    forall idb, match_stages [] (extend_stages idb []).
  Proof. rewrite /extend_stages //= => idb. by apply match_nil. Qed.

  Lemma numbered_unchange_match:
  forall (e:extended_stages) ids ids' stages,
      match_stages stages (fst (`e ids))
      -> match_stages stages (fst (`e ids')).
  Proof.
    move => [e [Hex Hn]] ids ids' stages.
    destruct (e ids) as [stages_e ids_e] eqn:He;
    destruct (e ids') as [stages_e' ids_e'] eqn:He'.
    have Hex':= Hex ids ids'. rewrite He He' //= in Hex'.
    rewrite //= He He' //= => Hstages.
    apply match_fp_fpe_stages with (s2 := stages_e); try by [].
  Qed.

  Lemma match_cons_stages_extended:
    forall idb s stages, 
      match_stages [s] (extend_stages idb [s])
      -> match_stages stages (extend_stages idb stages)
      -> match_stages (s :: stages) (extend_stages idb (s :: stages)).
  Proof.
    move => idb s stages.
    rewrite /extend_stages /extract_stages //=.
    destruct ((` (extend_stage _ _)) 0) as [s_e ids'] eqn:Hds.
    destruct ((` (extend_list_stages _ _ _)) 0) 
      as [stages_e ids0] eqn:Hdstages;
    destruct ((` (extend_list_stages _ _ _)) ids') 
      as [stages_e' ids''] eqn:Hdstages'; rewrite //= => Hs Hstages.
    apply match_cons_stages.
    by rewrite app_nil_r in Hs.
    have H': stages_e' 
                 = fst ((` (extend_list_stages (extend_stage idb)
                                         extend_nil stages)) ids').
    by rewrite Hdstages'. rewrite H'.
    apply numbered_unchange_match with (ids := 0); by rewrite Hdstages.
  Qed.

  Lemma forall_match_concat:
    forall idb e block block',
      Forall
        (fun s : FP.fp_stage => match_stages [s] (extend_stages idb [s]))
        block
      -> e = extend_list_stages (extend_stage idb) extend_nil block
      -> forall ids ids', (` e) ids = (block', ids')
      -> match_stages block block'.
  Proof.
    move => idb e block block' Hforall He ids ids' Hids.
    replace block' with (fst ((` e) ids)); last first. by rewrite Hids.
    apply numbered_unchange_match with (ids := 0).
    replace (fst ((` e) 0)) with (extend_stages idb block); last first.
      by rewrite /extend_stages He. clear He Hids.
    induction block as [|s block IHb].
    - apply match_extended_nil.
    - apply match_cons_stages_extended.
      * by apply Forall_inv with (l := block) (a := s).
      * apply IHb. by apply Forall_inv_tail with (a := s).
  Qed.

  Lemma match_extended_stage:
    forall idb s, 
      match_stages [s] (extend_stages idb [s]).
  Proof.
    move => idb. induction s using FP.fp_stage_ind';
      try destruct init;
      try (rewrite /extend_stages /extract_stages //=; simpl_match).

    remember (extend_list_stages _ _ block) as eb.
    destruct ((` eb) 1) as [block' ids'] eqn:Heb.
    remember (FP_E_WF.mk_end_while idb 0 ids' p) as params.
    rewrite app_comm_cons -app_assoc.
    apply match_while with (nb := ids' - 1) (n' := ids') (b' := block')
                           (p_e' := params) (n'' := ids').
    - rewrite Heqparams //=.
    - by rewrite Heqparams.
    - by rewrite Heqparams.
    - rewrite take_size_cat.
      * apply (forall_match_concat H Heqeb Heb).
      * apply (extended_stages_length_ids Heb).
    - rewrite app_nil_r.
      have H' := extended_stages_length_ids Heb.
      ssrlia; by rewrite -H' nth_middle.
    - apply (forall_match_concat H Heqeb Heb).
    - rewrite drop_oversize; try simpl_match.
      rewrite size_cat. have H' :=extended_stages_length_ids Heb.
      rewrite //=. have H'': size block' = ids' - 1.
      { ssrlia; by rewrite - H'. } rewrite H''.
      rewrite ssrnat.addnC -?plusE //=.
  Qed.

  Lemma app_match_stages:
    forall s1 s2 s1' s2',
      MATCH_FP_FPE.match_stages s1 s2
      -> MATCH_FP_FPE.match_stages s1' s2'
      -> MATCH_FP_FPE.match_stages (s1 ++ s1') (s2 ++ s2').
  Proof.
    induction s1; move => s2 s1' s2' Hs1 Hs2.
    - by inversion Hs1.
    - inversion Hs1.
      + apply MATCH_FP_FPE.match_cons; try by []. apply IHs1; try by [].
      + apply MATCH_FP_FPE.match_nav_init; apply IHs1; try by [].
      + apply MATCH_FP_FPE.match_end_while; try by [].
        apply IHs1; try by [].
      + have Hsize : nb < size stages_e.
        { apply  nth_impl_le_size
            with (e := FP_E.END_WHILE n' p_e' b') (d:= FP_E.DEFAULT n'');
            try by []. }
        apply MATCH_FP_FPE.match_while
              with (p_e' := p_e') (nb := nb) (n' := n')
                    (n'' := n'') (b' :=b');
              try by [].
        * rewrite takel_cat; try by []. by apply ltnW.
        * by apply nth_find.
        * rewrite drop_cat_le. apply IHs1; try by [].
          ssrlia.
  Qed.

  Lemma match_stages_extended:
    forall idb stages,
    match_stages stages (extend_stages idb stages).
  Proof.
    move => idb stages; induction stages as [|s stages IHs].
    - rewrite /extend_stages //=. simpl_match.
    - apply match_cons_stages_extended; try apply IHs.
      apply match_extended_stage.
  Qed.

  Lemma match_env_stages_init:
    forall fp fpe id,
      fpe = extend_flight_plan fp
      -> MATCH_FP_FPE.match_env_stages fpe (FP.get_stages fp id) id 0.
  Proof.
    rewrite /MATCH_FP_FPE.match_env_stages => fp fpe id Hfp.
    remember (FP.get_block fp id) as b;
    remember (FP.get_block_id b) as idb;
    remember (FP.get_block_stages b) as stages.
    rewrite drop0
            /FP.get_stages
            (extend_stages_applied Hfp Heqb)
            -Heqb -Heqidb -Heqstages.
    rewrite (extend_stages_app_default idb stages).
    apply match_default. apply match_stages_extended.
  Qed.

  Lemma match_env_stages_next_stages_deroute:
    forall fpe params stages idb2 ids2,
      match_env_stages fpe (FP.DEROUTE params:: stages)%SEQ idb2 ids2
        -> MATCH_FP_FPE.match_env_stages fpe stages idb2 (ids2 + 1).
  Proof. 
    rewrite /match_env_stages => fpe params stages idb2 ids2 Hd.
    inversion Hd. inversion H1. 
    rewrite -H5 //= in H. 
    by rewrite -(drop_incr H).
  Qed.

  Lemma evalc_get_current_stages:
    forall fpe e e' c b,
      FPE_ENV.Def.evalc e c = (b, e')
      -> get_current_stages fpe e = get_current_stages fpe e'.
  Proof.
    rewrite /FPE_ENV.Def.evalc
            /get_current_stages
    => fpe [[idb ids lidb lids] t] [[idb' ids' lidb' lids'] t'] c b //=.
    remember (EVAL_Def.eval t c) as b' => //= H.
    injection H as Hb Hidb Hids Hlidb Hlids Ht.
    by rewrite Hidb Hids.
  Qed.

  Lemma app_trace_get_current_stages:
    forall fpe e t,
      get_current_stages fpe e
        = get_current_stages fpe (FPE_ENV.Def.app_trace e t).
  Proof.
    rewrite /FPE_ENV.Def.app_trace
            /get_current_stages
    => fpe [[idb ids lidb lids] t] t'//.
  Qed.

  Lemma drop_get_default:
    forall fpe e n,
      [:: FP_E.DEFAULT n] = get_current_stages fpe e
      -> FP_E.DEFAULT n = FPE_ENV.get_current_stage fpe e.
  Proof.
    rewrite /FPE_ENV.get_current_stage
            /FP_E.get_stage
    => fpe [[idb ids lidb lids] t] //= n.
    apply drop_impl_nth.
  Qed.

  Lemma get_current_stages_to_stage:
    forall fpe e s stages,
      get_current_stages fpe e = s :: stages
      -> FPE_ENV.get_current_stage fpe e = s.
  Proof.
    rewrite /get_current_stages
            /FPE_ENV.get_current_stage
            /FP_E.get_stage
    => fpe [[idb ids lidb lids] t] //= s stages H.
    remember (FP_E.default_stage fpe _) as d.
    symmetry in H.
    by apply drop_impl_nth with (d := d) in H.
  Qed.

  (** Lemma that reset preserve the matching *)
  Lemma reset_stage_preserve_match:
    forall fpe n e1 e2, 
    e1 ~( fpe )~ e2
    -> [:: FP_E.DEFAULT n]
        = drop (FPE_ENV.get_nav_stage e2)
                 (FP_E.get_stages (`fpe)
                                        (FPE_ENV.Def.get_nav_block e2))
    -> e1 ~( fpe )~ (FPE_ENV.reset_stage (`fpe) e2).
  Proof.
    move => fpe n e1 e2 Heq Hc.

    destruct (Heq) as [Hb Hs Hlb Hls Ht];
      repeat split; try by [].

    destruct e2 as [[idb ids lidb lids] t].
    rewrite /match_env_stages //= /FP_E.default_stage_id //=.
    rewrite /match_env_stages //= in Hs.
    rewrite /FPE_ENV.get_current_stage //= in Hc.

    symmetry in Hc. apply drop_single_element in Hc.
    by rewrite Nat.sub_1_r -Hc.
  Qed.

End MATCH_FP_FPE.
