From Coq Require Import Arith ZArith Psatz Bool String List Program.Equality Program.Wf Program FunInd.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas
                        BasicTypes FPNavigationMode FPNavigationModeSem
                        FlightPlanGeneric 
                        FlightPlanExtended FlightPlanSized 
                        CommonFPDefinition
                        FPEnvironmentGeneric FPEnvironmentDef
                        FPBigStepGeneric FPBigStepSized.

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.

Set Implicit Arguments.

Module FPS_PROP (EVAL_Def: EVAL_ENV)
                            (ENVS_Def: ENVS_DEF EVAL_Def).

  (** properties based on FPS big step *)
  Module Import FPS_BS := FPS_BIGSTEP EVAL_Def ENVS_Def.
  Export FPS_BS.

  Import FP_E_WF ENVS_Def FPS_ENV FPE_ENV Def.

  Section FLIGHT_PLAN.

    Variable fps: flight_plan_sized.
    Definition fpe_wf := proj1_sig fps.
    Definition Hsize := proj2_sig fps.

    Definition fp   := proj1_sig fpe_wf.
    Definition Hwf := proj2_sig fpe_wf.

    Remark eq_fps_fp: (` (` fps)) = fp. by []. Qed.
    Remark eq_fpe_fp: (` fpe_wf) = fp. by []. Qed.

    Section stableFPS.

    (** * Properties about flight plan having a stable behaviour : *)
    (** in case we find ourselves in an incorrect environemnt 
        (e.i. the block id or the last block id point to an incorrect block)
        these properties guaranty us that the drone will keep a
        controled behaviour, it will follow the same evolution as one with 
        fixed blocks id. *)

    Lemma change_block_is_semi_correct : 
      forall e new_id,
      semi_correct_env fps (change_block (` (` fps)) e new_id).
    Proof.
      intros e new_id. 
      rewrite /semi_correct_env /change_block. 
      destruct (get_nav_block e =? new_id)%N; 
      apply normalise_block_id_is_correct.
    Qed.

    Lemma semi_correct_change_block_is_correct : 
      forall e new_id,
      semi_correct_env fps e
      -> get_nav_block e <> new_id
      -> correct_env fps (change_block (` (` fps)) e new_id).
    Proof.
      intros e new_id Hsc Hid. unfold change_block.
      apply Nat.eqb_neq in Hid. rewrite Hid. simpl.
      split.
      - apply normalise_block_id_is_correct.
      - apply Hsc.
    Qed.

    (** ** first, we will show that a correct environment will stay correct *)

    (** correct_through_X means that 
      if e is a correct environment
      then X(e) is one too *)

    Lemma correct_through_change_block : 
      forall e new_id,
      correct_env fps e
      -> correct_env fps (change_block (` (` fps)) e new_id).
    Proof.
      intros e new_id [Hid Hlid].
      unfold change_block.
      destruct (get_nav_block e =? new_id)%N;
      split; try apply normalise_block_id_is_correct; auto.
    Qed.
  
    Ltac auto_correct_env := repeat (
      try apply correct_through_change_block;
      try apply correct_through_normalise;
      auto
    );
    try by (split; auto).

    Lemma correct_through_test_forbidden_deroutes:
      forall e id1 id2 lfb res e1,
      correct_env fps e
      -> Common_Sem.test_forbidden_deroutes e id1 id2 lfb = (res, e1)
      -> correct_env fps e1.
    Proof.
      intros [st t] id1 id2 lfb. generalize dependent t.
      rewrite /Common_Sem.test_forbidden_deroutes //=.
      induction lfb.
      
      - move => t res e1 Hce H. by inversion H.

      - move => t res e1 Hce.
        rewrite /Common_Sem.test_forbidden_deroute /evalc.
        destruct (_ && _).
        + destruct (get_fbd_only_when _).
          * destruct (EVAL_Def.eval _ _).
            -- intros H; inversion H; by subst.
            -- by apply IHlfb.
          * intros H; inversion H; by subst.
        + by apply IHlfb.
    Qed.

    Lemma correct_through_goto : 
      forall e new_id,
      correct_env fps e
      -> correct_env fps (Common_Sem.goto_block (` (` fps)) e new_id).
    Proof.
      intros e new_id Hce.
      rewrite /Common_Sem.goto_block /Common_Sem.forbidden_deroute.
      destruct (Common_Sem.test_forbidden_deroutes e _ _ _) 
        as [res e'] eqn:Efbd.
      have Hce' := 
          (correct_through_test_forbidden_deroutes _ _ _ Hce Efbd).
      destruct res.
      - by [].
      - by apply correct_through_change_block.
    Qed.

    Lemma correct_through_test_exceptions : 
      forall e b e' lexp,
      correct_env fps e 
      -> Common_Sem.test_exceptions (` (` fps)) e lexp = (b, e')
      -> correct_env fps e'.
    Proof.
      intros [st t] b e1 lexp. generalize dependent t.
      induction lexp.
      - (* trivail case : no exception *)
        intros t Hce H. inversion H; by subst.
      - (* inductive case *)
        simpl. intros t Hce H. 
        destruct (Common_Sem.test_exception _ _ _)
          as [b' e'] eqn:Etest.
        unfold Common_Sem.test_exception in Etest. 
        destruct (_ =? _).
        + (* exception to same block, skiped *) 
          inversion Etest; subst; clear Etest. apply (IHlexp t Hce H).
        + (* exception to a different block *)
          unfold evalc in Etest.
          destruct (~~ EVAL_Def.eval _ _);
          inversion Etest; subst; clear Etest.
          * generalize H. by apply IHlexp.
          * inversion H; subst; clear H. by apply correct_through_goto.
    Qed.

    Lemma correct_through_run_stage : 
      forall e e' b,
      correct_env fps e 
      -> run_stage (` fps) e = (b, e')
      -> correct_env fps e'.
    Proof.
      intros e e' b [H1 H2] Hr.
      unfold run_stage in Hr.
      destruct (get_current_stage _ _); try by (inversion Hr; subst).

      { (* while *)
        rewrite /while_sem /evalc in Hr.
        destruct (EVAL_Def.eval _ _);
        inversion Hr; subst; clear Hr; auto_correct_env.
      }

      { (* end_while *)
        rewrite /end_while_sem /while_sem /evalc in Hr.
        destruct (EVAL_Def.eval _ _);
        inversion Hr; subst; clear Hr; auto_correct_env.
      }

      { (* call *)
        unfold call_sem in Hr.
        destruct (get_call_loop params).
        - unfold evalc in Hr. 
          destruct (EVAL_Def.eval _ _).
          + destruct (get_call_until params).
            * destruct (EVAL_Def.eval _ _);
              inversion Hr; subst; clear Hr; auto_correct_env.
            * inversion Hr; subst; clear Hr; auto_correct_env.
          + destruct (get_call_break params);
            inversion Hr; subst; clear Hr; auto_correct_env.
        - destruct (get_call_break params);
          inversion Hr; subst; clear Hr; auto_correct_env.
      }

      { (* detoute *)
        destruct params as [name new_id].
        unfold deroute_sem in Hr. inversion Hr; subst; clear Hr.
        by apply correct_through_goto.
      }

      { (* nav *)
        unfold nav_sem in Hr. 
        destruct (nav_cond_sem nav_mode).
        - unfold evalc in Hr.
          destruct (EVAL_Def.eval _ _);
          try by (inversion Hr; subst; clear Hr; auto_correct_env).
          unfold nav_code_sem in Hr.
          destruct until.
          + unfold evalc in Hr. 
            destruct (EVAL_Def.eval _ _);
            try by (inversion Hr; subst; clear Hr; auto_correct_env).
          + inversion Hr; subst; clear Hr; auto_correct_env.
        - unfold nav_code_sem in Hr.
          destruct until.
          + unfold evalc in Hr. 
            destruct (EVAL_Def.eval _ _);
            try by (inversion Hr; subst; clear Hr; auto_correct_env).
          + inversion Hr; subst; clear Hr; auto_correct_env.
      }

      { (* default *)
        unfold default_sem in Hr.
        inversion Hr; subst; clear Hr.
        unfold next_block.
        destruct (_ <? _);
        apply correct_through_goto; auto_correct_env.
      }

    Qed.

    Lemma correct_through_run_step : 
      forall e,
      correct_env fps e 
      -> correct_env fps (run_step (` fps) e).
    Proof.
      intros e. apply run_step_ind; clear e.
      - intros e e' He Hi Hc.
        apply Hi. apply (correct_through_run_stage Hc He).
      - intros e e' He Hc. apply (correct_through_run_stage Hc He). 
    Qed.

    (** Main property about a correct environement 
      staying correct through a step *)

    Lemma correct_through_step : 
      forall e,
      correct_env fps e
      -> correct_env fps (FPE_BS.step (` fps) e).
    Proof.
      intros e Hc. rewrite /FPE_BS.step /Common_Sem.exception.
      destruct (Common_Sem.test_exceptions _ _ _)
        as [b e1] eqn:Exception1.
      have Hc1 := (correct_through_test_exceptions _ Hc Exception1).
      destruct b.
      - apply Hc1.
      - remember  (app_trace e1 _) as e1'.
        have Hc1' : (correct_env fps e1') by rewrite Heqe1'.
        destruct (Common_Sem.test_exceptions _ e1' _)
          as [b e2] eqn:Exception2.
        have Hc2 := (correct_through_test_exceptions _ Hc1'
                                                        Exception2).
        destruct b.
        + apply Hc2.
        + by apply (correct_through_run_step).
    Qed.

    (* some extra lemma about incorrect environment becoming semi-correct *)
    Lemma goto_block_is_semi_correct : 
      forall e new_id,
      semi_correct_env fps (FPE_BS.Common_Sem.goto_block fp e new_id).
    Proof.
      intros [st t] new_id.
      rewrite /FPE_BS.Common_Sem.goto_block /semi_correct_env //=.
      have Hcfb := (get_correct_fbd Hsize).
      generalize dependent t.
      unfold Common_Sem.forbidden_deroute.
      induction (Common.get_fp_forbidden_deroutes _).
      - simpl. intros t.
        by apply change_block_is_semi_correct.
      - simpl. intros t.
        inversion Hcfb as [|x l [Hafb _] IHcfb H1]; subst.
        destruct (Common_Sem.test_forbidden_deroute _ _ _ _)
         as [b e'] eqn:FB.
        destruct b.
        + unfold Common_Sem.test_forbidden_deroute in FB.
          destruct (_ =? _) eqn:Hidb.
          * apply Nat.eqb_eq in Hidb.
            rewrite <- Hidb in Hafb.
            simpl in FB. destruct (_ =? _).
            -- destruct (get_fbd_only_when a).
             ++ unfold evalc in FB. inversion FB. simpl.
                unfold correct_block_id. 
                unfold is_user_id in Hafb.
                unfold get_nb_blocks in *. 
                ssrlia.
              ++ inversion FB. simpl.
                unfold correct_block_id. 
                unfold is_user_id in Hafb.
                unfold get_nb_blocks in *.
                ssrlia.
            -- inversion FB.
          * simpl in FB. inversion FB.
        + unfold Common_Sem.test_forbidden_deroute in FB.
          destruct (_ && _).
          * destruct (get_fbd_only_when a).
            -- inversion FB. apply IHf.
              by inversion Hcfb.
            -- inversion FB.
          * inversion FB. apply IHf. by inversion Hcfb.
    Qed.

    Lemma Incorrect_idb_excecption_is_semi_correct : 
      forall e e1,
      ~semi_correct_env fps e
      -> Common_Sem.exception (` (` fps)) e = (true, e1)
      -> semi_correct_env fps e1.
    Proof.
      unfold Common_Sem.exception.
      have Hexsg := (get_correct_gexcpts Hsize). 
      induction (Common.get_fp_exceptions _).
      - (*local exception *)
        rewrite //= /get_local_exceptions /get_current_block.
        intros e e1 Nsc. 
        assert (FP_E.get_block (` (` fps)) (get_nav_block e) = get_default_block (` (` fps))).
        apply get_block_default_block.
        rewrite /FPE_BS.fp.
        rewrite /semi_correct_env /correct_block_id in Nsc.
        to_nat Nsc. ssrlia.
        by rewrite H.
      - (* global exception *)
        rewrite //= /Common_Sem.test_exception. intros e e1 Nsc.
        inversion Hexsg as [| x l Hexec IHexsg H1]. subst.
        assert (get_expt_block_id a =? get_nav_block e = false).
        { 
          apply Nat.eqb_neq. 
          rewrite /semi_correct_env /correct_block_id in Nsc.
          rewrite /correct_excpt /is_user_id in Hexec.
          intros contra.
          apply Nsc. ssrlia. 
        }
        rewrite H /evalc.
        destruct (~~ EVAL_Def.eval _ _).
        + (* induction *)
          apply IHf. apply IHexsg. apply Nsc.
        + (* case *)
          intros E. inversion E; subst; clear E.
          apply goto_block_is_semi_correct.
    Qed.

    (** ** theses lemmas proves that given any fp_env, 
        executing a step from them give the same result as
        normalising the environemnt and the doing the step
        (so an invalid environment will not diverge and crash) *)

    (* some lemmas about checking an id or its normalise value against
      exception, deroute of forbidden deroute *)

    Lemma normalise_get_from : 
      forall id1 fb,
      correct_fbd_deroute fp fb
      -> id1 =? get_fbd_from fb = 
              (normalise_block_id fp id1 =? get_fbd_from fb).
    Proof.
      intros id1 fb Hfb.
      unfold normalise_block_id. 
      destruct (_ <=? _) eqn:E.
      - destruct Hfb as [Hfb _].
        apply leb_complete in E. 
        rewrite /is_user_id in Hfb.
        assert (id1 =? get_fbd_from fb = false)
          by (apply Nat.eqb_neq; ssrlia).
        rewrite H.
        symmetry.
        apply Nat.eqb_neq. ssrlia.
      - reflexivity.
    Qed.

    Lemma normalise_get_to : 
      forall id1 fb,
      correct_fbd_deroute fp fb
      -> id1 =? get_fbd_to fb =
          (normalise_block_id fp id1 =? get_fbd_to fb).
    Proof.
      intros id1 fb Hfb.
      unfold normalise_block_id. 
      destruct (get_nb_blocks _ <=? id1) eqn:E.
      - destruct Hfb as [_ Hfb].
        apply leb_complete in E.
        rewrite /is_user_id in Hfb.
        assert (id1 =? get_fbd_to fb = false)
          by (apply Nat.eqb_neq; ssrlia).
        rewrite H.
        symmetry.
        apply Nat.eqb_neq. ssrlia.
      - reflexivity.
    Qed.

    Lemma get_block_get_normalise :
      forall id,
      get_block fp id = get_block fp (normalise_block_id fp id).
    Proof.
      unfold normalise_block_id. intros id.
      destruct (get_nb_blocks _ <=? id) eqn:Hidb.
      - rewrite /get_block.
        repeat rewrite nth_overflow.
        + reflexivity.
        + rewrite /get_nb_blocks. ssrlia.
        + rewrite /get_nb_blocks in Hidb.
          apply leb_complete in Hidb. ssrlia.
      - reflexivity.
    Qed.

    Lemma normalise_get_exception_block_id : 
      forall id1 ex,
      correct_excpt (` (` fps)) ex
      -> get_expt_block_id ex =? id1 = 
         (get_expt_block_id ex =? normalise_block_id (` (` fps)) id1).
    Proof.
      intros id1 ex H.
      unfold normalise_block_id.
      destruct (get_nb_blocks (` (` fps)) <=? id1) eqn:E.
      - rewrite /correct_excpt /is_user_id in H.
        apply leb_complete in E.
        transitivity false.
        apply Nat.eqb_neq; ssrlia.
        symmetry.
        apply Nat.eqb_neq; ssrlia.
      - reflexivity.
    Qed.

    Lemma normalise_block_id_idempotent : 
      forall id,
      normalise_block_id fp (normalise_block_id fp id) =
      normalise_block_id fp id.
    Proof.
      intros id.
      unfold normalise_block_id.
      destruct (get_nb_blocks _ <=? id) eqn:E1.
      destruct (get_nb_blocks _ <=? (get_nb_blocks _ - 1)%coq_nat);
      reflexivity.
      by rewrite E1.
    Qed.

    (* Lemmas about full_normalise and other function permutating*)

    Lemma normalise_through_reset_stage :
      forall e,
      reset_stage fp (full_normalise fp e) =
      full_normalise fp (reset_stage fp e).
    Proof.
      intros e.
      rewrite /reset_stage /full_normalise /update_nav_stage //=.
      remember (FP_E.normalise_block_id _ _) as Idb.
      remember (FP_E.normalise_block_id _ (last_block _)) as LIdb.
      remember (last_stage _) as LIds. 
      remember (get_trace _) as t.
      rewrite /default_stage_id /get_stages HeqIdb. 
      by rewrite <- get_block_get_normalise.
    Qed.

    Lemma normalise_through_test_forbidden_deroute: 
      forall e id1 id2 id3 fb e1 b1 ne nid1 ne1,
      correct_fbd_deroute fp fb
      -> full_normalise fp e = ne
      -> normalise_block_id fp id1 = nid1
      -> full_normalise fp e1 = ne1
      -> (normalise_block_id fp id2) = (normalise_block_id fp id3)
      -> Common_Sem.test_forbidden_deroute e id1 id2 fb = (b1, e1)
      -> Common_Sem.test_forbidden_deroute ne nid1 id3 fb = (b1, ne1).
    Proof.
      unfold Common_Sem.test_forbidden_deroute. 
      intros e id1 id2 id3 fb e1 b1 ne nid1 ne1.
      intros Hfb Hne Hnid1 Hne1 Hid.
      subst.
      rewrite <- (normalise_get_from id1 Hfb).
      rewrite (normalise_get_to id3 Hfb). rewrite <- Hid.
      rewrite <- (normalise_get_to id2 Hfb).
      destruct (_ && _).
      - destruct (get_fbd_only_when fb).
        + unfold evalc. intros H. by inversion H.
        + intros H. by inversion H. 
      - intros H. by inversion H.
    Qed.

    Lemma normalise_through_test_forbidden_deroutes : 
      forall e id2 id3 lfb e1 b1 ne ne1,
      Forall (correct_fbd_deroute fp) lfb
      -> full_normalise fp e = ne
      -> full_normalise fp e1 = ne1
      -> (normalise_block_id fp id2) = (normalise_block_id fp id3) 
      -> Common_Sem.test_forbidden_deroutes e (get_nav_block e) id2
                                          lfb = (b1, e1)
      -> Common_Sem.test_forbidden_deroutes ne 
              (normalise_block_id fp (get_nav_block e)) id3 lfb
                                              = (b1, ne1).
    Proof.
      intros e id2 id3 lfb. generalize dependent e. induction lfb.
      - simpl. intros e e1 b1 ne ne1 _ Hne Hne1 _ H1. subst.
        by inversion H1.
      - simpl. intros e e1 b1 ne ne1 Hlex Hne Hne1 Hid.
        destruct (Common_Sem.test_forbidden_deroute _ _ _ a)
          as [b e'] eqn:Testfb.
        inversion Hlex as [|x l Hafb IHlex H1]; subst x l.

        remember (full_normalise fp e') as ne'; symmetry in Heqne'.
        remember (normalise_block_id _ (nav_block _)) as nb;
        symmetry in Heqnb.
        have Testfb2 := (normalise_through_test_forbidden_deroute 
                            _ _ _ Hafb Hne Heqnb Heqne' Hid Testfb).
        rewrite Testfb2. subst.
        destruct b. 
        + intros H. by inversion H.
        + have H1 := (test_forbidden_deroute_unchanged _ _ _ Testfb).
          simpl in H1. rewrite H1.
          by apply IHlfb.
    Qed.

    Lemma normalise_through_forbidden_deroute : 
      forall e new_id new_id2 e1 b1 ne ne1,
      full_normalise fp e = ne
      -> full_normalise fp e1 = ne1
      -> (normalise_block_id fp new_id) = (normalise_block_id fp new_id2)
      -> Common_Sem.forbidden_deroute fp e new_id = (b1, e1)
      -> Common_Sem.forbidden_deroute fp ne new_id2 = (b1, ne1).
    Proof.
      unfold Common_Sem.forbidden_deroute. 
      intros e new_id new_id2 e1 b1 ne ne1 Hne. 
      assert (get_nav_block ne = 
                    normalise_block_id fp (get_nav_block e)).
        by rewrite <- Hne.
      rewrite H. 
      apply normalise_through_test_forbidden_deroutes.
      by apply (get_correct_fbd Hsize).
      by apply Hne.
    Qed.

    Lemma normalise_through_goto_valid : 
      forall e new_id,
      is_user_id fp new_id
      -> Common_Sem.goto_block fp (full_normalise fp e) new_id 
          = full_normalise fp (Common_Sem.goto_block fp e new_id).
    Proof.
      unfold Common_Sem.goto_block. intros e new_id Hid.
      remember (full_normalise _ _) as ne eqn:Hne; symmetry in Hne.
      
      destruct (Common_Sem.forbidden_deroute _ e new_id)
          as [res1 e1'] eqn:fb1.
      remember (full_normalise fp e1') as ne1' eqn:Hne1'; 
      symmetry in Hne1'.

      have fb2 := (normalise_through_forbidden_deroute 
                      _ new_id Hne Hne1' _ fb1).
      rewrite fb2; try by reflexivity. 
      destruct res1; try by [].
      - rewrite /Common_Sem.c_change_block /Common.on_exit -Hne //=.
        rewrite <- get_block_get_normalise.
        unfold change_block.
        assert ((get_nav_block e1' =? new_id) =
                  (get_nav_block (full_normalise fp e1') =? new_id)).
        {
          rewrite //= /normalise_block_id.
          rewrite /is_user_id in Hid.
          destruct (_ =? _) eqn:Test; symmetry.
          - apply Nat.eqb_eq in Test. subst.
            assert (get_nb_blocks fp <=? nav_block (get_state e1')
                         = false).
              apply leb_correct_conv. ssrlia.
            rewrite H. by rewrite Nat.eqb_refl.
          - destruct (_ <=? _).
            + apply Nat.eqb_neq. ssrlia.
            + apply Test.
        } 
        rewrite H -Hne1' //=. 
        destruct (FP_E.normalise_block_id _ _ =? _);
        rewrite /app_trace /full_normalise /mk_fp_env //=;         rewrite normalise_block_id_idempotent; reflexivity.
    Qed.

    Lemma normalise_through_same_goto : 
      forall e e1 ne,
      full_normalise fp e = ne
      -> (Common_Sem.goto_block fp e (get_nav_block e)) = e1
      -> Common_Sem.goto_block fp ne (get_nav_block ne) 
        = (full_normalise fp e1).
    Proof.
      intros e e1 ne Hne.
      rewrite /Common_Sem.goto_block.
      destruct (Common_Sem.forbidden_deroute _ e _)
        as [res e'] eqn:fb1.
      remember (full_normalise fp e') as ne' eqn:Hne';
      symmetry in Hne'.

      have fb2 := (normalise_through_forbidden_deroute 
                      _ _ Hne Hne' _ fb1).
      rewrite fb2.
      - destruct res; try by (intros H; subst).
        have Hid := (forbidden_deroute_unchanged _ _ _ fb1).
        rewrite /Common_Sem.c_change_block /Common.on_exit -Hne //=.
        rewrite <- get_block_get_normalise.
        rewrite /change_block -Hne' //=.
        simpl in *.
        rewrite Hid Nat.eqb_refl Nat.eqb_refl.
        rewrite /full_normalise /app_trace /mk_fp_env //=.
        intros H; subst; simpl.
        by rewrite normalise_block_id_idempotent.

      - rewrite -Hne. by rewrite normalise_block_id_idempotent.
    Qed.

    Lemma normalise_through_test_exception : 
      forall e a e' b ne ne',
      correct_excpt fp a
      -> full_normalise fp e = ne
      -> full_normalise fp e' = ne'
      -> Common_Sem.test_exception fp e a = (b, e')
      -> Common_Sem.test_exception fp ne a = (b, ne').
    Proof.
      unfold Common_Sem.test_exception.
      intros e a e' b ne ne' Hex Hne Hne'.
      rewrite (normalise_get_exception_block_id (get_nav_block e) Hex) -Hne //=.
      destruct (get_expt_block_id _ =? normalise_block_id _ _).
      - intros H. rewrite -Hne'. by inversion H.
      - destruct (~~ EVAL_Def.eval _ _).
        + intros H. rewrite -Hne'. by inversion H.
        + intros Ex1. inversion Ex1. subst. clear Ex1.
          by rewrite <- (normalise_through_goto_valid _ Hex).
    Qed.

    Lemma normalise_through_test_exceptions : 
      forall e lex e' b ne ne',
      Forall (correct_excpt fp) lex
      -> full_normalise fp e = ne
      -> full_normalise fp e' = ne'
      -> Common_Sem.test_exceptions fp e lex = (b, e')
      -> Common_Sem.test_exceptions fp ne lex = (b, ne').
    Proof.
      intros e lex. generalize dependent e. induction lex; simpl.
      - intros e e' b ne ne' _ Hne Hne' H.
        rewrite -Hne -Hne'.
        by inversion H.
      - intros e.
        destruct (Common_Sem.test_exception _ e a)
          as [b e'] eqn:Test1.
        remember (full_normalise fp e') as ne' eqn:Hne';
        symmetry in Hne'.

        intros e1 b1 ne ne1 Hlex Hne Hne1.
        inversion Hlex as [|x l Ha IH H1]; subst x l.

        have Test2 := (normalise_through_test_exception 
                            Ha Hne Hne' Test1).
        rewrite Test2 -Hne' -Hne1.
        destruct b.
        + intros H. by inversion H.
        + by apply IHlex.
    Qed.

    Lemma normalise_through_exception :
      forall e e' b ne ne',
      full_normalise fp e = ne
      -> full_normalise fp e' = ne'
      -> Common_Sem.exception fp e = (b, e')
      -> Common_Sem.exception fp ne = (b, ne').
    Proof.
      intros e e' b ne ne' Hne Hne'.
      unfold Common_Sem.exception.
      destruct (Common_Sem.test_exceptions _ e _)
      as [b1 e1] eqn:Test1.
      remember (full_normalise fp e1) as ne1 eqn:Hne1;
      symmetry in Hne1.

      have Hexsg := (get_correct_gexcpts Hsize).
      have Test2 := (normalise_through_test_exceptions Hexsg Hne Hne1 Test1).
      rewrite Test2.
      destruct b1.
      - intros H. rewrite -Hne1 -Hne'. by inversion H.

      - intros H.
        unfold get_local_exceptions.
        unfold get_current_block in *.
        rewrite -Hne1 //= -get_block_get_normalise.
        have Hexsl := (get_correct_lexcpts (get_well_sized_blocks Hsize (get_nav_block e1))).
        remember (app_trace e1 _) as e1' eqn:He1'.
        eapply normalise_through_test_exceptions with (e := e1').
        + apply Hexsl.
        + by rewrite He1'.
        + apply Hne'.  
        + apply H.
    Qed.

    Lemma normalise_through_next_block :
      forall e,
      next_block fpe_wf (reset_stage fp (full_normalise fp e)) 
      = (full_normalise fp (next_block fpe_wf (reset_stage fp e))).
    Proof.
      intros e.
      rewrite /next_block /FPE_BS.fp normalise_through_reset_stage; 
      rewrite //= eq_fpe_fp.
      destruct (nav_block _ <? get_nb_blocks _ - 1) eqn:Estage.
      - apply Nat.ltb_lt in Estage as Estage1.
        assert (normalise_block_id fp (nav_block (get_state e)) 
                                    = nav_block (get_state e)).
          rewrite /normalise_block_id.
          assert (Estage2 : 
                (nav_block (get_state e) < get_nb_blocks fp)%coq_nat) 
            by (to_nat Estage1; ssrlia).
          apply leb_correct_conv in Estage2.
          by rewrite Estage2.
        rewrite H Estage /Common_Sem.goto_block.
        destruct (Common_Sem.forbidden_deroute _ (reset_stage _ e) _)
          as [res e1'] eqn:fb1.
        have fb2 := (normalise_through_forbidden_deroute _ _ _ _ _ fb1).
        erewrite fb2; try by [].
        destruct res; try by [].
        rewrite /Common_Sem.c_change_block /Common.on_exit //=.
        rewrite <- get_block_get_normalise.
        unfold change_block.
        have Hid := (forbidden_deroute_unchanged _ _ _ fb1).
        simpl in *. rewrite <- Hid.
        rewrite H.
        assert (nav_block (get_state e) =? nav_block (get_state e) + 1 = false).
          apply Nat.eqb_neq. ssrlia.
        by rewrite H0 /full_normalise /app_trace //= //H normalise_block_id_idempotent.
      - assert (normalise_block_id fp (nav_block (get_state e)) = get_nb_blocks fp - 1).
          rewrite /normalise_block_id. apply Nat.ltb_ge in Estage as Estage1.
          inversion Estage1.
          + by destruct (_ <=? _).
          + to_nat H0.
            assert (get_nb_blocks fp <= m.+1) by ssrlia.
            to_nat H1.
            apply leb_correct in H1.
            by rewrite H1.
        rewrite H Nat.ltb_irrefl.
        rewrite <- H.
        by eapply normalise_through_same_goto.
    Qed.

    Lemma normalise_through_run_stage :
      forall e e1 b1,
      run_stage (` fps) e = (b1, e1) ->
      run_stage (` fps) (full_normalise (` (` fps)) e) = (b1, (full_normalise (` (` fps)) e1)).
    Proof.
      intros e.
      rewrite /run_stage /FPE_BS.fp /get_current_stage /get_stage;
      rewrite /default_stage /default_stage_id /get_stages //=. 
      rewrite <- get_block_get_normalise.
      destruct (List.nth _ _ _) eqn:Estage;
      try by (intros e1 b1 H; inversion H).

      {
        (* while_sem *)
        rewrite /while_sem /evalc //=.
        destruct (EVAL_Def.eval _ _);
        by (intros e1 b1 H; inversion H).
      }

      {
        (* end_while_sem *)
        rewrite /end_while_sem /while_sem /evalc //=.
        destruct (EVAL_Def.eval _ _);
        by (intros e1 b1 H; inversion H).
      }

      {
        (* call_sem *)
        intros e1 b1.
        unfold call_sem.
        destruct (get_call_loop params).
        - unfold evalc.
          simpl.
          destruct (EVAL_Def.eval _ _).
          + destruct (get_call_until params).
            * destruct (EVAL_Def.eval _ _);
                intros H; by inversion H. 
            * intros H; by inversion H.
          + destruct (get_call_break params);
              intros H; by inversion H.
        - destruct (get_call_break params);
            intros H; by inversion H.
      }

      {
        (* deroute_sem *)
        intros e1 b1.
        have Hderoute := (get_correct_deroute 
                            (get_well_sized_blocks Hsize (nav_block (get_state e)))
                            Estage).
        destruct params as [name new_id] eqn:Eparam.
        rewrite /is_user_id //= in Hderoute.
        rewrite /deroute_sem  /app_trace /next_stage //=.
        intros H. inversion H. rewrite H2. clear H.
        subst.
        by rewrite <- (normalise_through_goto_valid _ Hderoute).
      }

      { (* return *)
        intros e1 b1.
        rewrite /return_sem.
        intros H. inversion H. clear H. clear H1. clear H2.
        rewrite /break /app_trace /full_normalise /mk_fp_env //=.
        rewrite /FPE_BS.fp /Common.on_enter /Common.on_exit.
        by repeat rewrite -get_block_get_normalise.
      }

      {
        (* nav_sem *)
        intros e1 b1.
        unfold nav_sem. simpl.
        destruct (nav_cond_sem nav_mode).
        - destruct (EVAL_Def.eval _ _).
          + intros H; by inversion H.
          + unfold nav_code_sem; simpl.
            destruct (until).
            * destruct (EVAL_Def.eval _ _);
                intros H; by inversion H.
            * intros H; by inversion H.
        - unfold nav_code_sem; simpl.
            destruct (until).
            * destruct (EVAL_Def.eval _ _);
                intros H; by inversion H.
            * intros H; by inversion H.
      }

      {
        (* default_sem *)
        intros e1 b1.
        unfold default_sem.
        intros H; inversion H.
        by rewrite (normalise_through_next_block _).
      }
    Qed.

    Lemma normalise_through_run_step_ind :
      forall e e1 e2,
      e1 = full_normalise (` (` fps)) e -> 
      run_step (` fps) e1 = e2 ->
      e2 = full_normalise (` (` fps)) (run_step (` fps) e).
    Proof.
      intros e. apply run_step_ind.
      {
        intros e0 e' H Hi e1 e2 IH IH2. subst.
        have H1 := (normalise_through_run_stage _ H).
        eapply Hi. reflexivity.
        have H2 := (run_continue_stage_unchange_block _ _ H).
        symmetry.
        rewrite run_step_equation.
        by rewrite H1.
      }


      {
        intros e0 e' H Hid e2 e3 HI. subst.
        have Sol := (normalise_through_run_stage _ H).
        rewrite (run_step_equation). by rewrite Sol.
      }
    Qed.

    Lemma normalise_through_run_step :
      forall e,
      run_step (` fps) (full_normalise (` (` fps)) e) = full_normalise (` (` fps)) (run_step (` fps)  e).
    Proof.
      intros e.
      remember ((full_normalise (` (` fps)) e)) as e1.
      remember (run_step (` fps) e1) as e2.
      by apply normalise_through_run_step_ind with e1.
    Qed.

    (** main lemma about environment having the same behaviour if they are
        normalised  *)
    Lemma normalise_through_step : 
      forall e,
      FPE_BS.step (` fps) (full_normalise (` (` fps)) e)=
      full_normalise (` (` fps)) (FPE_BS.step (` fps) e).
    Proof.
      intros e.
      unfold FPE_BS.step.
      destruct (Common_Sem.exception _ e)
        as [b e'] eqn:Exception1.
      have Exception2 := (normalise_through_exception _ _ Exception1).
      erewrite Exception2; try by rewrite eq_fps_fp.

      destruct b; try by []. 

      - rewrite (normalise_through_run_step _). 
        + unfold get_code_block_post_call.
          unfold get_current_block.
          simpl.
          by rewrite <- get_block_get_normalise.
      
      by rewrite eq_fps_fp.
    Qed.
  
    End stableFPS.

    Section On_Enter_Exit_Verif.

    (** * Proof that on_enter and on_exit are when changing block : *)
    (** one of the new functionality of the VFPG is on_enter and on_exit
      code taht are run when a block is enter or exit. we will prove that
      theses codes are indeed executed if needed*)

    (** ** some usefull lemmas about function adding trace at the end*)
      
    Lemma test_forbidden_deroute_add_trace :
      forall e id1 id2 fb res e2,
      Common_Sem.test_forbidden_deroute e id1 id2 fb = (res, e2) ->
      exists (t' : fp_trace),
      get_trace e2 = get_trace e ++ t'.
    Proof.
      intros e id1 id2 fb res e2.
      rewrite /Common_Sem.test_forbidden_deroute /evalc.
      destruct (_ && _).
      - destruct (get_fbd_only_when _).
        + intros H; eexists ; by inversion H.
        + intros H; exists (nil)%list; inversion H. apply app_nil_end.
      - intros H; exists (nil)%list; inversion H. apply app_nil_end.
    Qed.
      
    Lemma test_forbidden_deroutes_add_trace :
      forall e id1 id2 fbs res e2,
      Common_Sem.test_forbidden_deroutes e id1 id2 fbs = (res, e2) ->
      exists (t' : fp_trace),
      get_trace e2 = get_trace e ++ t'.
    Proof.
      intros e id1 id2 fbs. generalize dependent e.
      induction fbs.
      - simpl.
        intros e res e2 H; inversion H.
        exists nil. apply app_nil_end.
      - simpl.
        intros e res e2.
        destruct (Common_Sem.test_forbidden_deroute _ _ _ _)
          as [b e1] eqn:Testfb.
        destruct (test_forbidden_deroute_add_trace _ _ _ _ Testfb)
          as [t' Ht].
        destruct b. 
        + intros H; inversion H; subst.
          by exists t'.

        + intros H.
          destruct (IHfbs _ _ _ H) as [t'' Ht'].
          exists (t' ++ t''). rewrite Ht' Ht.
          by rewrite app_assoc.
    Qed.
      
    Lemma forbidden_deroute_add_trace :
      forall e id res e2,
      Common_Sem.forbidden_deroute (` (` fps)) e id = (res, e2) ->
      exists (t' : fp_trace),
      get_trace e2 = get_trace e ++ t'.
    Proof.
      intros e id res e2.
      rewrite /Common_Sem.forbidden_deroute.
      apply test_forbidden_deroutes_add_trace.
    Qed.

    Lemma goto_block_add_trace :
      forall e id e2,
      Common_Sem.goto_block (` (` fps)) e id =  e2 ->
      exists (t' : fp_trace),
      get_trace e2 = get_trace e ++ t'.
    Proof.
      intros e id e2.
      rewrite /Common_Sem.goto_block.
      destruct (Common_Sem.forbidden_deroute _ _ _)
        as [res e1] eqn:fb.
      destruct (forbidden_deroute_add_trace _ _ fb) as [t' Ht].
      destruct res.
      - intros H; subst; by exists t'.
      - intros H; subst.
        rewrite /change_block.
        destruct (_ =? _); simpl;
        eexists; rewrite Ht; by rewrite <- app_assoc.
    Qed.

    Lemma test_exception_add_trace :
      forall e ex res e2,
      Common_Sem.test_exception (` (` fps)) e ex = (res, e2) ->
      exists (t' : fp_trace),
      get_trace e2 = get_trace e ++ t'.
    Proof.
      intros e ex res e2.
      rewrite /Common_Sem.test_exception /evalc.
      destruct (_ =? _).
      - intros H; eexists; inversion H; by apply app_nil_end.
      - destruct (~~ EVAL_Def.eval _ _).
        + intros H; eexists; by inversion H.
        + intros H; inversion H; rewrite H2.
          destruct (goto_block_add_trace _ _ H2) as [t' Ht].
          eexists. simpl in Ht.
          repeat rewrite <- app_assoc in Ht.
          apply Ht.
    Qed.
    
    Lemma test_exceptions_add_trace :
      forall e lex res e2,
      Common_Sem.test_exceptions (` (` fps)) e lex = (res, e2) ->
      exists (t' : fp_trace),
      get_trace e2 = get_trace e ++ t'.
    Proof.
      intros e lex. generalize dependent e.
      induction lex; intros e.
      - simpl. intros res e2 H; exists nil.
        inversion H. apply app_nil_end.
      - simpl.
        destruct (Common_Sem.test_exception _ _ _) as [b e'] eqn:Test.
        destruct (test_exception_add_trace _ _ Test) as [t' Ht].
        destruct b. 
        + intros res e2 H; eexists; inversion H; subst. apply Ht.
        + intros res e2 H.
          destruct (IHlex _ _ _ H) as [t'' Ht'].
          exists (t' ++ t''). rewrite Ht' Ht .
          by rewrite app_assoc.
    Qed.

    Lemma exception_add_trace :
      forall e1 e2 res,
      Common_Sem.exception (` (` fps)) e1 = (res, e2)
      -> exists (t' : fp_trace),
        get_trace e2 = get_trace e1 ++ t'.
    Proof.
      rewrite /Common_Sem.exception.
      intros e1 e2 res Hexc.
      destruct (Common_Sem.test_exceptions _ _ _) as [b e1'] eqn:Hexc1.
      destruct b. 
      - inversion Hexc; subst.
        apply (test_exceptions_add_trace _ _ Hexc1).
      - destruct (test_exceptions_add_trace _ _ Hexc1)
          as [t Ht].
        destruct (test_exceptions_add_trace _ _ Hexc)
          as [t' Ht'].
        eexists.
        rewrite Ht' //= Ht.
        by repeat rewrite app_assoc_reverse.
    Qed.

    Lemma run_stage_add_trace : 
      forall e1 res e2,
      run_stage (` fps) e1 = (res, e2)
      -> exists (t' : fp_trace),
        get_trace e2 = get_trace e1 ++ t'.
    Proof.
      intros e1 re e2.
      rewrite /run_stage.
      destruct (get_current_stage _);
      try by (intros H; inversion H; by eexists).

      { (* while *)
        rewrite /while_sem /evalc /break /continue 
                /app_trace /next_stage //=.
        destruct (EVAL_Def.eval _ _).
        - intros H; inversion H.
          eexists. by rewrite app_assoc_reverse.
        - intros H; inversion H.
          by eexists.
      }

      { (* end while *)
        rewrite /end_while_sem /while_sem /evalc /break /continue 
                /app_trace /next_stage //=.
        destruct (EVAL_Def.eval _ _).
        - intros H; inversion H.
          eexists. by rewrite app_assoc_reverse.
        - intros H; inversion H.
          by eexists.
      }

      { (* call *)
        rewrite /call_sem /evalc.
        destruct (get_call_loop _).
        - destruct (EVAL_Def.eval _ _).
          + destruct (get_call_until _).
            * destruct (EVAL_Def.eval _ _).
              -- intros H; inversion H.
                eexists. simpl.
                by repeat rewrite app_assoc_reverse.
              -- intros H; inversion H.
                eexists. simpl.
                by repeat rewrite app_assoc_reverse.
            * intros H; inversion H.
              eexists. simpl.
              by repeat rewrite app_assoc_reverse.
          + destruct (get_call_break _).
            * intros H; inversion H.
              eexists. simpl.
              by repeat rewrite app_assoc_reverse.
            * intros H; inversion H.
              eexists. simpl.
              by repeat rewrite app_assoc_reverse.
        - destruct (get_call_break _).
          + intros H; inversion H.
            eexists. simpl.
            by repeat rewrite app_assoc_reverse.
          + intros H; inversion H.
            eexists. simpl.
            by repeat rewrite app_assoc_reverse.
      }

      { (* deroute *)
        destruct params.
        intros H; inversion H.
        destruct (goto_block_add_trace _ _ H2) as [t' Ht].
        rewrite H2 Ht //=. eexists.
        by apply app_assoc_reverse.
      }

      { (* nav mode *)
        rewrite /nav_sem /evalc /break /continue.
        destruct (nav_cond_sem _).
        - destruct (EVAL_Def.eval _ _).
          + intros H; inversion H.
            eexists. simpl.
            by repeat rewrite app_assoc_reverse.
          + rewrite /nav_code_sem /evalc /break /continue.
            destruct until.
            * destruct (EVAL_Def.eval _ _).
              -- intros H; inversion H.
                eexists; simpl.
                by repeat rewrite app_assoc_reverse.
              -- intros H; inversion H.
                eexists; simpl.
                by repeat rewrite app_assoc_reverse.
            * intros H; inversion H.
              eexists; simpl.
              by repeat rewrite app_assoc_reverse.
        - rewrite /nav_code_sem /evalc /break /continue.
          destruct until.
          * destruct (EVAL_Def.eval _ _).
            -- intros H; inversion H.
              eexists; simpl.
              by repeat rewrite app_assoc_reverse.
            -- intros H; inversion H.
              eexists; simpl.
              by repeat rewrite app_assoc_reverse.
          * intros H; inversion H.
            eexists; simpl.
            by repeat rewrite app_assoc_reverse.
      }

      { (* default *)
        rewrite /default_sem /break /next_block.
        intros H; inversion H. rewrite H2.
        destruct (_ <? _);
        apply (goto_block_add_trace _ _ H2). 
      }
    Qed.
    
    Lemma run_step_add_trace :
      forall e e',
      run_step (` fps) e = e'
      -> exists (t' : fp_trace), 
        get_trace e' = get_trace e ++ t'.
    Proof.
      intros e e' H; subst.
      apply run_step_ind; clear e; intros e e' H.
      - intros [t' Ht].
        destruct (run_stage_add_trace _ H) as [t'' Ht'].
        rewrite Ht Ht'.
        eexists.
        by rewrite app_assoc_reverse.
      - destruct (run_stage_add_trace _ H) as [t' Ht].
        exists t'. apply Ht.
    Qed.

    (** ** Definition of having run on_enter and on_exit *)

    Definition has_run_on_enter (e1 e2 : fp_env) (id : block_id) :=
      exists (t1 t2 : fp_trace),
      get_trace e2 = get_trace e1 ++ t1 ++ (Common.on_enter (` (` fps)) id) ++ t2.

    Definition has_run_on_exit (e1 e2 : fp_env) (id : block_id) :=
      exists (t1 t2 : fp_trace),
      get_trace e2 = get_trace e1 ++ t1 ++ (Common.on_exit (` (` fps)) id) ++ t2.

    Definition has_run_on_enter_and_exit (e1 e2 : fp_env) (id1 id2 : block_id) := 
      has_run_on_enter e1 e2 id2 /\ has_run_on_exit e1 e2 id1.

    Lemma goto_block_run_on_enter_and_exit :
      forall e1 e1' e2 id2,
      Common_Sem.forbidden_deroute (` (` fps)) e1 id2 = (false, e1')
      -> Common_Sem.goto_block (` (` fps)) e1 id2 = e2
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) id2.
    Proof.
      intros e1 e1' e2 id2 Hfb Hgoto.
      rewrite /Common_Sem.goto_block Hfb in Hgoto.
      subst.
      destruct (forbidden_deroute_add_trace _ _ Hfb)
        as [t' Ht].
      split.
      + (* on_enter id2 *)
        rewrite /has_run_on_enter.
        rewrite /change_block /Common_Sem.c_change_block //=.
        destruct (_ =? _); simpl.
        * exists (t' ++ (Common.on_exit (` (` fps)) (nav_block (get_state e1))) ++ [::reset_time] ++ [::init_stage]); exists nil.
          rewrite Ht //= /Common.on_enter. 
          repeat rewrite <- app_assoc.
          rewrite <- get_block_get_normalise.
          by rewrite app_nil_r.
        * exists (t' ++ (Common.on_exit (` (` fps)) (nav_block (get_state e1))) ++ [::reset_time] ++ [::init_stage]); exists nil.
          rewrite Ht //= /Common.on_enter. 
          repeat rewrite <- app_assoc.
          rewrite <- get_block_get_normalise.
          by rewrite app_nil_r.
      + (* on exit id1 *)
        rewrite /has_run_on_exit.
        rewrite /change_block /Common_Sem.c_change_block //=.
        destruct (_ =? _); simpl.
        * exists t'; eexists.
          rewrite Ht. 
          by rewrite <- app_assoc.
        * exists t'; eexists.
          rewrite Ht. 
          by rewrite <- app_assoc.
    Qed.

    Lemma test_exception_change_run_on_enter_and_exit :
      forall e1 e2 ex,
      Common_Sem.test_exception (` (` fps)) e1 ex = (true , e2)
      -> get_nav_block e1 <> get_nav_block e2
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1 e2 ex.
      rewrite /Common_Sem.test_exception /evalc.
      destruct (_ =? _) eqn:Eex; try by [].
      destruct (~~ EVAL_Def.eval _ _); try by [].
      intros H Hid. inversion H; subst; clear H.
      remember (app_trace (app_trace _ _) _) as e1'.
      remember (Common_Sem.goto_block _ e1' _) as e2; symmetry in Heqe2.
      apply Nat.eqb_neq in Eex. apply not_eq_sym in Eex.
      destruct (Common_Sem.forbidden_deroute (` (` fps)) e1' (get_expt_block_id ex))
      as [b e1''] eqn:fb.
      destruct b.
      - rewrite /Common_Sem.goto_block fb in Heqe2.
        subst.
        have contra := (forbidden_deroute_unchanged _ _ _ fb).
        simpl in contra.
        destruct (Hid contra).

      - destruct (goto_block_run_on_enter_and_exit e1' _ fb Heqe2)
        as [[t1 [t1' H1]] [t2 [t2' H2]]].
        split.
        + rewrite /has_run_on_enter H1 Heqe1' //=.
          clear H2; clear t2; clear t2'.
          have Hid2 := (goto_block_forbidden_derout_nraised _ _ _ fb Heqe2).
          simpl in Hid2. rewrite Hid2 /Common.on_enter.
          rewrite <- get_block_get_normalise.
          eexists; eexists.
          by rewrite (app_assoc _ t1 _) 
                  (app_assoc_reverse _ (opt_c_code_to_trace _) _)
                  (app_assoc_reverse _ [:: COND _ _] _)
                  app_assoc_reverse.
        + rewrite /has_run_on_exit H2 Heqe1' //=.
          eexists; eexists.
          by rewrite (app_assoc _ t2 _) 
                  (app_assoc_reverse _ (opt_c_code_to_trace _) _)
                  (app_assoc_reverse _ [:: COND _ _] _)
                  app_assoc_reverse.
    Qed.
    
    Lemma test_exceptions_change_run_on_enter_and_exit:
      forall e1 lex e2,
      Common_Sem.test_exceptions (` (` fps)) e1 lex = (true, e2) ->
      get_nav_block e1 <> get_nav_block e2 ->
      has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1 lex; generalize dependent e1.
      induction lex.
      - simpl. intros e1 e2 contra. by inversion contra.
      - simpl. intros e1.
        destruct (Common_Sem.test_exception _ _ _) as [b e1'] eqn:test_ex.
        destruct b. 
        + intros e2 H; inversion H; subst; clear H.
          by apply test_exception_change_run_on_enter_and_exit with a.
        + rewrite /Common_Sem.test_exception /evalc in test_ex.
          destruct (_ =? _).
          * inversion test_ex. 
            by apply IHlex.
          * destruct (~~ EVAL_Def.eval _ _); try by [].
            inversion test_ex; rewrite H0.
            intros e2 Hlex Hid.
            clear test_ex.
            assert (nav_block (get_state e1) = nav_block (get_state e1'));
            try by rewrite <- H0.
            simpl in IHlex. rewrite H in Hid.
            destruct (IHlex _ _ Hlex Hid) as [[t1 [t1' H1]] [t2 [t2' H2]]].
            split.
            -- rewrite /has_run_on_enter H1.
               rewrite <- H0.
               rewrite //=
                       (app_assoc _ t1 _)
                       (app_assoc_reverse _ _ t1)
                       (app_assoc_reverse).
               eexists; by eexists.
            -- rewrite /has_run_on_exit H2.
               rewrite <- H0.
               rewrite //=
                       (app_assoc _ t2 _)
                       (app_assoc_reverse _ _ t2)
                       (app_assoc_reverse).
               eexists; by eexists.
    Qed.
    
    Lemma exception_change_run_on_enter_and_exit:
      forall e1 e2,
      Common_Sem.exception (` (` fps)) e1 = (true, e2) ->
      get_nav_block e1 <> get_nav_block e2 ->
      has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1.
      rewrite /Common_Sem.exception.
      destruct (Common_Sem.test_exceptions _ _ _)
        as [b e1'] eqn:Test1.
      destruct b. 
      - intros e2 H Hid.
        inversion H; subst; clear H.
        by apply (test_exceptions_change_run_on_enter_and_exit _ _ Test1 Hid).
      - have Hid := (test_exceptions_nraised_unchanged _ _ _ Test1).
        rewrite Hid.
        intros e2 Test2 Hid2.
        remember (app_trace e1' _ ) as e1''.
        destruct (test_exceptions_add_trace _ _ Test1) as [t Ht].
        assert (get_nav_block e1' = get_nav_block e1''); try by rewrite Heqe1''.
        rewrite H in Hid2.
        destruct (test_exceptions_change_run_on_enter_and_exit _ _ Test2 Hid2)
        as [[t1 [t1' H1]] [t2 [t2' H2]]].
        split.
        + rewrite /has_run_on_enter H1 Heqe1'' //= Ht.
          rewrite (app_assoc _ t1 _)
                  (app_assoc_reverse _ _ t1)
                  (app_assoc_reverse _ t _)
                  (app_assoc_reverse).
          eexists; by eexists.
        + rewrite /has_run_on_exit H2 Heqe1'' //= Ht.
          rewrite (app_assoc _ t2 _)
                  (app_assoc_reverse _ _ t2)
                  (app_assoc_reverse _ t _)
                  (app_assoc_reverse).
          eexists; by eexists.
    Qed.

    Lemma deroute_change_run_on_enter_and_exit : 
      forall e1 e2 id params res,
      get_stage (` (` fps)) (get_nav_block e1) (get_nav_stage e1) = FP_E.DEROUTE id params
      -> get_nav_block e1 <> get_nav_block e2
      -> run_stage (` fps) e1 = (res, e2)
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1 e2 id [block id0] res Hstage Hid.
      rewrite /run_stage /get_current_stage Hstage /deroute_sem /break /FPE_BS.fp.
      remember (Common_Sem.goto_block _ _ _) as e1'' eqn:Hgoto.
      symmetry in Hgoto.
      intros H; inversion H; subst e1''; clear H.
      destruct (Common_Sem.forbidden_deroute (` (` fps)) (app_trace (next_stage e1) [:: init_stage]) id0)
        as [b e1'] eqn:fb.
      destruct b.
      - rewrite /Common_Sem.goto_block fb in H2.
        subst.
        have contra := (forbidden_deroute_unchanged _ _ _ fb).
        simpl in contra. destruct (Hid contra).

      - destruct (goto_block_run_on_enter_and_exit _ _ fb H2)
        as [[t1 [t1' Ht1]] [t2 [t2' Ht2]]].
        split.
        + rewrite /has_run_on_enter Ht1.
          have Hid2 := (goto_block_forbidden_derout_nraised _ _ _ fb H2).
          simpl in Hid2. rewrite /Common.on_enter //= Hid2.
          rewrite <- get_block_get_normalise.
          rewrite (app_assoc _ t1 _)
                  (app_assoc_reverse _ _ t1)
                  (app_assoc_reverse).
          eexists; by eexists.
        + rewrite /has_run_on_exit Ht2 //=.
          rewrite (app_assoc _ t2 _)
                  (app_assoc_reverse _ _ t2)
                  (app_assoc_reverse).
          eexists; by eexists.
    Qed.

    Lemma default_run_on_enter_and_exit : 
      forall e1 e2 id res,
      get_stage (` (` fps)) (get_nav_block e1) (get_nav_stage e1) = FP_E.DEFAULT id
      -> get_nav_block e1 <> get_nav_block e2
      -> run_stage (` fps) e1 = (res, e2)
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1 e2 id res Hstage Hid.
      rewrite /run_stage /get_current_stage Hstage //= 
              /default_sem /break /FPE_BS.fp.
      intros H; inversion H; clear H. rewrite H2.
      rewrite /next_block /FPE_BS.fp in H2.
      destruct (_ <? _) eqn:Eid.
      - destruct (Common_Sem.forbidden_deroute (` (` fps)) (reset_stage (` (` fps)) e1) (get_nav_block (reset_stage (` (` fps)) e1) + 1))
        as [b e1'] eqn:fb.
        destruct b.
        * rewrite /Common_Sem.goto_block fb in H2.
          subst.
          have contra := (forbidden_deroute_unchanged _ _ _ fb).
          simpl in contra. destruct (Hid contra).
        * destruct (goto_block_run_on_enter_and_exit _ _ fb H2)
          as [[t1 [t1' Ht1]] [t2 [t2' Ht2]]].
          split.
          + rewrite /has_run_on_enter Ht1 //=.
            have Hid2 := (goto_block_forbidden_derout_nraised _ _ _ fb H2).
            simpl in Hid2. rewrite /Common.on_enter //= Hid2.
            rewrite <- get_block_get_normalise.
            eexists; by eexists.
          + rewrite /has_run_on_exit Ht2 //=.
            eexists; by eexists.
      - destruct (Common_Sem.forbidden_deroute (` (` fps)) (reset_stage (` (` fps)) e1) (get_nav_block (reset_stage (` (` fps)) e1)))
        as [b e1'] eqn:fb.
        destruct b.
        * rewrite /Common_Sem.goto_block fb in H2.
          subst.
          have contra := (forbidden_deroute_unchanged _ _ _ fb).
          simpl in contra. destruct (Hid contra).
        * destruct (goto_block_run_on_enter_and_exit _ _ fb H2)
          as [[t1 [t1' Ht1]] [t2 [t2' Ht2]]].
          split.
          + rewrite /has_run_on_enter Ht1 //=.
            have Hid2 := (goto_block_forbidden_derout_nraised _ _ _ fb H2).
            simpl in Hid2. rewrite /Common.on_enter //= Hid2.
            rewrite <- get_block_get_normalise.
            eexists; by eexists.
          + rewrite /has_run_on_exit Ht2 //=.
            eexists; by eexists.
    Qed.

    Lemma return_run_on_enter_and_exit : 
      forall e1 e2 id params res,
      get_stage (` (` fps)) (get_nav_block e1) (get_nav_stage e1) = FP_E.RETURN id params
      -> get_nav_block e1 <> get_nav_block e2
      -> run_stage (` fps) e1 = (res, e2)
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1 e2 id params res Hstage Hid.
      rewrite /run_stage /get_current_stage Hstage /return_sem.
      intros H; inversion H. clear H. clear H1. clear H2.
      rewrite /return_block /app_trace.

      simpl. split.
      - (* on_enter *)
        rewrite /has_run_on_enter //=.
        eexists; exists nil.
        rewrite -cat1s cat_app
                -(cat1s init_stage) cat_app
                app_assoc
                (app_assoc)
                app_assoc -(app_assoc _ [:: reset_time]) //=
                (app_assoc_reverse (get_trace _))
                app_assoc_reverse //=.
        repeat f_equal.
        by apply app_nil_end.

      - (* on_exit *)
        rewrite /has_run_on_exit //=.
        exists nil; eexists.
        by f_equal.
    Qed.

    Lemma run_stage_change_run_on_enter_and_exit :
      forall e1 e2 res,
      run_stage (` fps) e1 = (res, e2)
      -> get_nav_block e1 <> get_nav_block e2
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1 e2 res.
      rewrite /run_stage.
      destruct (get_current_stage _ _) eqn:Hstage;
      try by ( intros H; inversion H; 
          intros contra; exfalso; auto).
      { (* while *)
        rewrite /while_sem /evalc.
        destruct (EVAL_Def.eval).
        - intros H; inversion H. simpl. 
          intros contra. exfalso. auto.
        - intros H; inversion H. simpl. 
          intros contra. exfalso. auto.
      }

      { (* end while *)
        rewrite /end_while_sem /while_sem /evalc.
        destruct (EVAL_Def.eval).
        - intros H; inversion H. simpl. 
          intros contra. exfalso. auto.
        - intros H; inversion H. simpl. 
          intros contra. exfalso. auto.
      }

      { (* call *)
        rewrite /call_sem /evalc.
        destruct (get_call_loop _).
        - destruct (EVAL_Def.eval _ _).
          + destruct (get_call_until _).
            * destruct (EVAL_Def.eval _ _).
              --intros H; inversion H; intros contra;
                exfalso; auto.
              --intros H; inversion H; intros contra;
                exfalso; auto.
            * intros H; inversion H; intros contra;
              exfalso; auto.
          + destruct (get_call_break _).
            * intros H; inversion H; intros contra;
              exfalso; auto.
            * intros H; inversion H; intros contra;
              exfalso; auto.
        - destruct (get_call_break _).
          + intros H; inversion H; intros contra;
            exfalso; auto.
          + intros H; inversion H; intros contra;
            exfalso; auto.
      }

      { (* deroute *)
        intros Hrun Hid.
        eapply deroute_change_run_on_enter_and_exit.
        - apply Hstage.
        - apply Hid.
        - by rewrite /run_stage Hstage Hrun.
      }

      { (* return *)
        intros Hrun Hid.
        eapply return_run_on_enter_and_exit.
        - apply Hstage.
        - apply Hid.
        - by rewrite /run_stage Hstage Hrun.
      }

      { (* nav *)
        rewrite /nav_sem /evalc /break.
        destruct (nav_cond_sem _).
        - destruct (EVAL_Def.eval _ _).
          + intros H; inversion H. by [].
          + rewrite /nav_code_sem /evalc /break /app_trace //=.
            destruct until.
            * destruct (EVAL_Def.eval _ _).
              -- intros H; inversion H; by [].
              -- intros H; inversion H; by [].
            * intros H; inversion H; by [].
        - rewrite /nav_code_sem /evalc /break /app_trace //=.
          destruct until.
          + destruct (EVAL_Def.eval _ _).
            * intros H; inversion H; by [].
            * intros H; inversion H; by [].
          + intros H; inversion H; by [].
      }

      { (*default *)
        intros Hrun Hid.
        eapply default_run_on_enter_and_exit.
        - apply Hstage.
        - apply Hid.
        - by rewrite /run_stage Hstage Hrun.
      }
    Qed.

    Lemma run_step_change_run_on_enter_and_exit :
      forall e1 e2,
      run_step (` fps) e1 = e2
      -> get_nav_block e1 <> get_nav_block e2
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      intros e1 e2 Hr.
      rewrite <- Hr.
      apply run_step_ind; clear Hr; clear e1; clear e2.
      - intros e1 e2 Hr IH Hid.
        destruct (get_nav_block e1 =? get_nav_block e2) eqn:Hid'.
        + apply Nat.eqb_eq in Hid'. rewrite Hid' in Hid.
          destruct (run_stage_add_trace _ Hr) as [t Ht].
          destruct (IH Hid) as [[t1 [t1' Ht1]] [t2 [t2' Ht2]]].
          split.
          * rewrite /has_run_on_enter Ht1 Ht.
            eexists; eexists.
            by rewrite (app_assoc)
                    (app_assoc_reverse _ _ t1)
                    (app_assoc_reverse).
          * rewrite /has_run_on_exit Ht2 Ht Hid'.
            eexists; eexists.
            by rewrite (app_assoc)
                    (app_assoc_reverse _ _ t2)
                    (app_assoc_reverse).
        + apply Nat.eqb_neq in Hid'.
          destruct (get_nav_block e2 =? get_nav_block (run_step (` fps) e2)) eqn:Hid''.
          * apply Nat.eqb_eq in Hid''. rewrite -Hid''.
            remember (run_step _ e2) as e2'; symmetry in Heqe2'.
            destruct (run_step_add_trace _ Heqe2') as [t' Ht'].
            destruct (run_stage_change_run_on_enter_and_exit _ Hr Hid')
              as [[t1 [t1' Ht1]] [t2 [t2' Ht2]]].
            split.
            -- rewrite /has_run_on_enter Ht' Ht1.
              eexists; eexists.
              by repeat rewrite app_assoc_reverse.
            -- rewrite /has_run_on_exit Ht' Ht2.
              eexists; eexists.
              by repeat rewrite app_assoc_reverse.
          * apply Nat.eqb_neq in Hid''.
            destruct (run_stage_change_run_on_enter_and_exit _ Hr Hid')
              as [[t1 [t1' Ht1]] [t2 [t2' Ht2]]].
            destruct (IH Hid'') as [[t3 [t3' Ht3]] [t4 [t4' Ht4]]].
            split.
            -- rewrite /has_run_on_enter Ht3 Ht1.
               rewrite (app_assoc _ t3 _)
                       (app_assoc_reverse _ _ t3)
                       (app_assoc_reverse (get_trace e1) _ _).
               eexists; by eexists.
            -- rewrite /has_run_on_exit Ht3 Ht2.
               eexists; eexists.
               by repeat rewrite app_assoc_reverse.
      - intros e e' Hr Hid.
        by apply (run_stage_change_run_on_enter_and_exit _ Hr Hid).
    Qed.

    (** main lemma of the verification :
        if in a step, we go from nav_block id1 to nav_block id 2,
        than we can have run the on_exit code of nav_block id 1
        and the on_enter code of nav_block id 2*)

    Lemma step_change_run_on_enter_and_exit :
      forall e1 e2,
      FPE_BS.step (` fps) e1 = e2
      -> get_nav_block e1 <> get_nav_block e2
      -> has_run_on_enter_and_exit e1 e2 (get_nav_block e1) (get_nav_block e2).
    Proof.
      rewrite /FPE_BS.step.
      intros e1 e2 Hstep Hid.
      destruct (Common_Sem.exception _ _) as [b e1'] eqn:Exception.
      destruct b. 
      - subst.
        by apply (exception_change_run_on_enter_and_exit _ Exception Hid).
      - remember (run_step _ e1') as e1''; symmetry in Heqe1''.
        rewrite -Hstep //= in Hid.
        rewrite /FPE_BS.fp in Exception.
        have Hid' := (exception_nraised_unchanged _ _ Exception).
        simpl in Hid'. rewrite Hid' in Hid.
        destruct (run_step_change_run_on_enter_and_exit _ Heqe1'' Hid)
          as [[t1 [t1' Ht1]] [t2 [t2' Ht2]]].
        destruct (exception_add_trace _ Exception) as [t Ht].
        split.
        + rewrite /has_run_on_enter -Hstep //= Ht1 Ht.
          eexists; eexists.
          by rewrite (app_assoc _ t1 _)
                    (app_assoc_reverse _ t t1)
                    (app_assoc_reverse (get_trace e1))
                    (app_assoc_reverse _ _ (get_code_block_post_call _))
                    (app_assoc_reverse)
                    (app_assoc_reverse _ t1').
        + rewrite /has_run_on_exit -Hstep //= Hid' Ht2 Ht.
          eexists; eexists. 
          by rewrite (app_assoc _ t2 _)
                    (app_assoc_reverse _ t t2)
                    (app_assoc_reverse (get_trace e1))
                    (app_assoc_reverse _ _ (get_code_block_post_call _))
                    (app_assoc_reverse)
                    (app_assoc_reverse _ t2').
    Qed.

    End On_Enter_Exit_Verif.

  End FLIGHT_PLAN.

End FPS_PROP.