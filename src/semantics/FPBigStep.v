From Coq Require Import Arith ZArith Psatz Bool String List Program.Equality Program.Wf.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import BasicTypes FPNavigationMode FPNavigationModeSem
                        FlightPlanGeneric FlightPlan
                        CommonFPDefinition
                        FPEnvironmentGeneric FPBigStepGeneric
                        FPEnvironmentDef.

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.

Set Implicit Arguments.

(** * Definition of the big step semantics for the flight plan. *)

Module FP_BIGSTEP (EVAL_Def: EVAL_ENV)
                  (ENVS_Def: ENVS_DEF EVAL_Def).
  Import EVAL_Def FP ENVS_Def.FP_ENV ENVS_Def.FP_ENV.Def.
  Module Import Common_Sem :=
    COMMON_SEM FP.Def EVAL_Def FP.Common ENVS_Def.FP_ENV.Def.

  Section FlightPlan.

  (** Definition of the flight plan being executed *)
  Variable fp: flight_plan.

  (** Get the exec_stages corresponding to the block id [id] *)
  Definition get_exec_stages (id: block_id): exec_stages :=
    FP.get_stages fp id.

  (** Last block *)
  Definition default_block := get_default_block_id fp.

  (** ** Run function *)

  (** *** Go to the next block function*)
  Definition next_block (e: fp_env): fp_env :=
    let nav_block := get_nav_block e in
    (** If it is not the last possible block *)
    if (nav_block <? get_nb_blocks fp - 1) then
      (** Go to the following block *)
      goto_block fp e  (nav_block + 1)
    else
      (** Go to the last possible block *)
      goto_block fp e nav_block.

  (** ** Stages Semantics *)

  (** All the semantics function takes a continue function. This is a *)
  (** closure function that will continue the execution of            *)
  (** the flight plan until a break is reached.                       *)
  (** The function is called with the new env computed                *)
  (** The type of this function is:                                   *)
  Definition continue_fun := fp_env -> fp_env.

  (** We note e the environment of the current stage being executed   *)
  (** and e' the environment without the stage being executed.        *)

  (** *** While *)
  Definition while_sem (e:fp_env)
                        (continue: continue_fun) (cond: c_cond)
                         (stages: exec_stages): fp_env :=
    (** Evalutation of the while condition *)
    let '(b, e') := evalc e cond in
    (if b
    then (** Restart while loop *)
      (** Add the stages of the while loop and the while stage 
          (futur restart of the loop) *)
      let e'' := update_nav_stage e' stages in
      app_trace e'' (init_stage :: nil)
    else (** While loop end *)
      (** Continue the execution of the block *)
      continue e').

  (** *** Set *)
  Definition set_sem (e: fp_env) (continue: continue_fun)
                        (params: fp_params_set): fp_env :=
    (** Execute the set stage and continue the execution *)
    continue (app_trace e ((get_c_set params) :: init_stage :: nil)).

  (** *** Call *)
  Definition call_sem (e e':fp_env) (continue: continue_fun)
                      (params: fp_params_call): fp_env :=
    let f := get_call_fun params in
    if get_call_loop params then
      (** Loop is enable *)
      (** Execution of the function *)
      let '(b, e) := evalc e f in
      let e' := change_trace e' (get_trace e) in
      if b then
        (** The function returns true, the stage continue *)
        (** Verify if the option until is enable *)
        match get_call_until params with
        (** Option until not enabled, the stage break*)
        | None => e
        | Some cond =>
          (** Option until enabled *)
          let '(b', e) := evalc e cond in
          let e' := change_trace e' (get_trace e) in
          (** Evaluate the condition of the until.
              Jump to the next stage if the conditions is true. *)
          if b' then
            app_trace e' (init_stage :: nil)
          else
            e
        end
      else
        (** The function returns false, the stage end *)
        (** Break if the option is enabled *)
        let e'' := app_trace e' (init_stage :: nil) in
        if get_call_break params then
          e''
        else continue e''
    else
      (** Loop not enable *)
      (** Execute the function and break if it is enabled *)
      let e'' := app_trace e' ((C_CODE f) :: init_stage :: nil) in
      if get_call_break params then
        e''
      else
        continue e''.

  (** *** Deroute *)
  Definition deroute_sem (e':fp_env) (id: block_id): fp_env :=
    (** Update the environment to jump to the derouted block *)
    let e'' := app_trace e' (init_stage :: nil) in 
    goto_block fp e'' id.

  (** *** Return *)
  Definition return_sem (e:fp_env) (reset: bool): fp_env :=
    (** Return to the last block and reset the stages if the
        option is enabled *)
    let e' := return_block fp e reset in
    app_trace e' ((Common.on_exit fp (get_nav_block e)) ++ [:: reset_time, init_stage & (Common.on_enter fp (get_last_block e))]).

  (** *** Generic Nav *)

  (** Semantics for the init nav stage *)
  Definition nav_init_sem (e': fp_env) (nav: fp_navigation_mode)
                            (until: option c_cond):
                            fp_env :=
    let e' := app_trace e'
              ((init_code_nav_sem nav) :: init_stage :: nil )in
    let new_stages := (NAV nav until false) :: nil in
    update_nav_stage e' new_stages.

  (** Semantics the nav code  *)
  Definition nav_code_sem (e e': fp_env)
                          (nav: fp_navigation_mode)
                          (until: option c_cond): fp_env :=
    let code := gen_fp_nav_code_sem nav in
    let e := app_trace e code in
    let e' := app_trace e' code in
    let post_out := (post_call_nav_sem nav) :: nil in
    (** The stage has a until condition?*)
    match until with
    | Some u =>
        (** Evaluate the condition and jump to the next stage
            if the condition is true *)
        let '(b, e) := evalc e u in
        let e' := change_trace e' (get_trace e) in
        if b then
          app_trace e' (post_out ++ (init_stage :: nil))
        else
          app_trace e post_out
    | None =>
        app_trace e post_out
    end.

  Definition nav_sem (e e':fp_env)
                      (nav: fp_navigation_mode)
                        (until: option c_cond)
                         (init: bool): fp_env :=
    (** If the navigation function requires a init stage, execute it 
        and set the init value to false *)
    if init then
      nav_init_sem e' nav until
    else
      (** Pre call code for the nav functions *)
      let pre := (pre_call_nav_sem nav :: nil) in
      let e := app_trace e pre in
      let e' := app_trace e' pre in
      (** The navigation function has a condition to stop the execution *)
      match nav_cond_sem nav with
      | Some cond =>
        (** Compute the condition *)
        let '(b, e) := evalc e cond in
        let e' := change_trace e' (get_trace e) in
        if b then
          (** The condition return true,
                break and go to the next stage *)
          app_trace e' ((post_call_nav_sem nav)
                            :: (last_wp_exec nav) :: init_stage :: nil)
        else
          (** Condition is false, the navigation code is executed *)
          nav_code_sem e e' nav until
      | None =>
        (** No condition. Execute the navigation code and
            verify if there are an until option. *)
        nav_code_sem e e' nav until
      end.


  (** ** Main run function *)

  Fixpoint run_step_rec (nav_block: block_id)
                         (nav_stages: exec_stages)
                          (last_block: block_id)
                          (last_stages: exec_stages)
                          (t: fp_trace): fp_env :=
    (** Reconstruct the current environment *)
    let e := mk_fp_env nav_block nav_stages last_block last_stages t in
    match nav_stages with
    | nil =>
      (** End of the block -> Go to the next block *)
      next_block e
    | s :: ls =>
      (** Execution of a stage *)
      (** Function to execute recursively the run_step_rec
          and build the fp_trace and the new environment *)
      let continue e: fp_env := 
        run_step_rec nav_block ls last_block last_stages (get_trace e)
      in
      (** Building an env without the current stage being executed. *)
      let e' := mk_fp_env nav_block ls last_block last_stages t in
      match s with
      | WHILE cond stages =>
        while_sem e continue cond stages
      | SET params =>
        set_sem e continue params
      | CALL params =>
        call_sem e e' continue params
      | DEROUTE {|get_deroute_block_id := id|} =>
        deroute_sem e' id
      | RETURN reset =>
        return_sem e reset
      | NAV nav until init =>
        nav_sem e e' nav until init
      end
    end.

  Definition run_step (e: fp_env): fp_env :=
    run_step_rec (get_nav_block e)
                 (get_nav_stages e)
                 (get_last_block e)
                 (get_last_stages e)
                 (get_trace e).

  (** ** step function *)

  Definition step (e: fp_env): fp_env := 
    match exception fp e with
    | (true, e') => e'
    | (false, e') =>
      let e'' := run_step e' in
      let post := get_code_block_post_call (get_current_block fp e')
      in
        app_trace e'' post
    end.

  End FlightPlan.

  Definition semantics_fp (fp: flight_plan): fp_semantics := 
    FP_Semantics_gen ((step_prop step) fp) (initial_env fp).

End FP_BIGSTEP.
