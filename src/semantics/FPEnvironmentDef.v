From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".


From VFP Require Import FPEnvironmentGeneric
                        FPEnvironment
                        FPEnvironmentExtended
                        FPEnvironmentSized
                        FPEnvironmentClight.


(** Module type containing the definitions of all the environments needed *)

Module Type ENVS_DEF (EVAL_Def: EVAL_ENV).

  Module FP_ENV := FP_ENV EVAL_Def.

  Module FPS_ENV := FPS_ENV EVAL_Def.

  Export FP_ENV FPS_ENV FPE_ENV C_ENV.

End ENVS_DEF.
