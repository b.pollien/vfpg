From Coq Require Import Arith ZArith Psatz Bool String List Program.

From VFP Require Import BasicTypes TmpVariables
                        ClightGeneration CommonFPDefinition
                        FPNavigationMode CommonStringLemmas.

From compcert Require Import Coqlib Integers Floats AST Ctypes
                             Cop Clight Clightdefs.
Import Clightdefs.ClightNotations.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope clight_scope.
Local Open Scope dec_uint_scope.
Local Open Scope list_scope.
Local Open Scope string_scope.

(** Definition of the semantics functions for the FP modes of Paparazzi *)

(** Function that generates the call to a function from a [name] and a *)
(** list of [parameters]                                               *)
Definition gen_fun_call_sem (name: string) (parameters: list string):
                                                          c_code :=
  name ++ "(" ++
  (match parameters with 
  | nil => ""
  | p :: nil => p
  | p :: l =>
    p ++ (List.fold_right (fun a b => "," ++ a ++ b) "" l)
  end) ++ ")".

Definition gen_fun_call_set_sem (var: string) (name: string)
                                        (parameters: list string):
  c_code :=
gen_fun_call_sem (var ++ " = " ++ name) parameters.

(** Function that generates the call to a function from a [name] and a *)
(** list of [parameters] and add the nav parameter                *)
Definition gen_fun_call_nav (name: string) (parameters: list string)
                                    (pc: fp_params_nav_call): c_code :=
  match get_nav_nav_params pc with
  | Some p =>
    gen_fun_call_sem name (parameters ++  (p :: nil))
  | None =>
    gen_fun_call_sem name parameters
   end.

(** * Init Nav Semantics*)
Definition init_code_nav_sem (m: fp_navigation_mode): fp_event :=
  match m with
  | EIGHT _ _ _ => C_CODE (gen_fun_call_sem eight_init_code nil)
  | OVAL _ _ _ => C_CODE (gen_fun_call_sem oval_init_code nil)
  | SURVEY_RECTANGLE ps pc =>
    let wp1 := string_of_nat (get_srect_wp1 ps) in
    let wp2 := string_of_nat (get_srect_wp2 ps) in
    let grid := get_srect_grid ps in
    let o := convertOrientation_str (get_srect_orientation ps) in
    C_CODE (gen_fun_call_nav (rectangle_init_code pc) 
                        (wp1 :: wp2 :: grid :: o :: nil) pc)
  | _ => SKIP
  end.

(** * Nav cond functions for the semantics *)

(* Nav cond is of the form:
if (nav_cond) {
  NextStageAndBreakFrom(wp);
}
else {
  NAV_CODE(function)
}
*)

(** Function that return the c_code of the nav_cond if it is possible *)

Definition approaching_time_sem (p: fp_params_go): c_cond :=
  match get_go_approaching_time p with
  | AT a => a
  | ET e => "-" ++ e
  | CARROT => CARROT_str
  end.

Definition nav_cond_sem (m: fp_navigation_mode): option c_code :=
  match m with
  | GO p _ p_call =>
    let wp := string_of_nat (get_go_wp p) in
    let a_t := approaching_time_sem p in
    let nav := get_nav_type p_call in
    match get_go_from p with
    | Some last =>
      let l := string_of_nat last in
      Some 
      (gen_fun_call_sem (nav ++ "ApproachingFrom")
                          (wp :: l :: a_t :: nil))
    | None =>
      Some
      (gen_fun_call_sem (nav ++ "Approaching") (wp :: a_t ::nil))
    end
  | _ => None
  end.

(** * Pre/Post nav_call semantics *)

Definition pre_call_nav_sem (m: fp_navigation_mode): fp_event :=
  match get_fp_pnav_call m with
  | Some pc => 
    match get_nav_pre_call pc with
    | Some c => C_CODE c
    | None => SKIP
    end
  | None => SKIP
  end.

Definition post_call_nav_sem (m: fp_navigation_mode): fp_event :=
  match get_fp_pnav_call m with
  | Some pc =>
    match get_nav_post_call pc with
    | Some c => C_CODE c
    | None => SKIP
    end
  | None => SKIP
  end.

(** RadOfDeg trace *)
Definition radofdeg_sem (param: string): fp_event :=
  C_CODE (gen_fun_call_set_sem tmp_RadOfDeg_str
                                            RadOfDeg_str (param :: nil)).

(** * Vmode semantics *)

(** Pitch *)
Definition gen_fp_pitch_sem (nav: nav_type) (pitch: pitch): fp_trace :=
  match pitch with
  | AUTO t =>
    (** verticalAutoPitchMode *)
    C_CODE (gen_fun_call_sem (verticalAutoPitchMode_str nav)
                ((t ++ "*" ++ (string_of_nat (Nat.of_num_uint 9600))) :: nil)) :: nil
  | SOME p =>
    (** verticalAutoThrottleMode *)
    (radofdeg_sem p)
    :: (C_CODE
        (gen_fun_call_sem (verticalAutoThrottleMode_str nav)
                                  (tmp_RadOfDeg_str :: nil))) :: nil
  | NO_PITCH => SKIP :: nil
  end.

(** VerticalClimbMode *)
Definition verticalClimbMode_sem (nav: nav_type) (c: climb): fp_event := 
  C_CODE (gen_fun_call_sem 
            (verticalClimbMode_str nav)
            (c :: nil)).

Definition gen_fp_vmode_sem (nav: nav_type) (vmode: vmode): fp_trace :=
  match vmode with
    | CLIMB c =>
    (* verticalClimbMode *)
      C_CODE
        (gen_fun_call_sem (verticalClimbMode_str nav) (c :: nil)) :: nil
    | ALT a =>
      (* verticalAltitudeMode *)
      let (param, init_call) :=
          match a with
          | ABS a => (a, SKIP)
          | REL h => 
            (tmp_AltitudeMode_str,
            C_CODE (gen_fun_call_set_sem tmp_AltitudeMode_str
                                            Height_str (h :: nil)))
          | WP wp => 
            (tmp_AltitudeMode_str,
            C_CODE (gen_fun_call_set_sem tmp_AltitudeMode_str
                                          WaypointAlt_str
                                          ((string_of_nat wp) :: nil)))
          end
      in
        init_call
        :: C_CODE
          (gen_fun_call_sem (verticalAltitudeMode_str nav)
                  (param :: (string_of_nat 0) :: nil)) :: nil
    | XYZ_VM => SKIP :: nil
    | GLIDE wp l_wp =>
      (* glide *)
      C_CODE
          (gen_fun_call_sem (glide_str nav) 
          (l_wp :: (string_of_nat wp) :: nil)) :: nil
    | THROTTLE t =>
      (* verticalThrottleMode *)
      C_CODE
          (gen_fun_call_sem (verticalThrottleMode_str nav) 
          ((t ++ "*" ++ (string_of_nat (Nat.of_num_uint 9600))) :: nil))
          :: nil
    | NO_VMODE => SKIP :: nil
  end.

Definition gen_fp_nav_mode_params_sem (nav_call: fp_params_nav_call)
                                (nav_mode: fp_params_nav_mode): fp_trace :=
  let nav := get_nav_type nav_call in
    ((gen_fp_pitch_sem nav (get_nav_pitch nav_mode))
    ++ (gen_fp_vmode_sem nav (get_nav_vmode nav_mode)))%list.

(** * Hmode semantics *)
Definition gen_fp_hmode_sem (h: hmode) (pc: fp_params_nav_call): fp_event :=
  match h with
  | ROUTE wp_nat l_wp =>
    let wp := string_of_nat wp_nat in
    C_CODE (gen_fun_call_nav (segment_str pc) (l_wp :: wp :: nil) pc)
  | DIRECT wp_nat =>
    let wp := string_of_nat wp_nat in
    C_CODE (gen_fun_call_nav (gotoWaypoint_str pc) (wp :: nil) pc)
  end.

(** * Nav mode semantics *)
Definition gen_fp_heading_sem (ps: fp_params_heading)
                                (pm: fp_params_nav_mode)
                                  (pc: fp_params_nav_call): fp_trace :=
  radofdeg_sem (get_heading_course ps)
  :: C_CODE (gen_fun_call_nav (heading_code pc)
                                          (tmp_RadOfDeg_str :: nil) pc)
  :: (gen_fp_nav_mode_params_sem pc pm).

Definition gen_fp_attitude_sem (ps: fp_params_attitude)
                                (pm: fp_params_nav_mode)
                                  (pc: fp_params_nav_call): fp_trace :=
  radofdeg_sem(get_attitude_roll ps)
  :: C_CODE (gen_fun_call_nav (attitude_code pc)
                                          (tmp_RadOfDeg_str :: nil) pc)
  :: (gen_fp_nav_mode_params_sem pc pm).

Definition gen_fp_manual_sem (ps: fp_params_manual)
                              (pm: fp_params_nav_mode)
                                (pc: fp_params_nav_call): fp_trace :=
  let r := get_manual_roll ps in
  let p := get_manual_pitch ps in
  let y := get_manual_yaw ps in
  C_CODE (gen_fun_call_nav (manual_code pc) (r :: p :: y :: nil) pc)
  :: (gen_fp_nav_mode_params_sem pc pm).

Definition gen_fp_go_sem (ps: fp_params_go) (pm: fp_params_nav_mode)
                                    (pc: fp_params_nav_call): fp_trace :=
  (gen_fp_hmode_sem (get_go_hmode ps) pc)
  :: (gen_fp_nav_mode_params_sem pc pm).

Definition gen_fp_xyz_sem (ps: fp_params_xyz)
                            (pm: fp_params_nav_mode)
                              (pc: fp_params_nav_call): fp_trace :=
  C_CODE (gen_fun_call_sem xyz_code (ps :: nil))
  :: (gen_fp_nav_mode_params_sem pc pm).

Definition gen_fp_circle_sem (ps: fp_params_circle)
                              (pm: fp_params_nav_mode)
                               (pc: fp_params_nav_call): fp_trace :=
  let wp := string_of_nat (get_circle_wp ps) in
  let r := get_circle_radius ps in
  app (gen_fp_nav_mode_params_sem pc pm)
      (C_CODE (gen_fun_call_nav (circle_code pc) (wp :: r :: nil) pc)
        :: nil).

Definition gen_fp_stay_sem (ps: fp_params_stay) (pm: fp_params_nav_mode)
                                      (pc: fp_params_nav_call): fp_trace :=
  (gen_fp_hmode_sem (get_stay_hmode ps) pc)
  :: (gen_fp_nav_mode_params_sem pc pm).

Definition gen_fp_follow_sem (ps: fp_params_follow)
                              (pc: fp_params_nav_call): fp_trace :=
  let id := get_follow_ac_id ps in
  let d := get_follow_distance ps in
  let h := get_follow_height ps in
  (C_CODE (gen_fun_call_nav (follow_code pc) (id :: d :: h :: nil) pc))
  :: nil.

Definition gen_fp_survey_rectangle_sem (ps: fp_params_survey_rectangle)
                                    (pc: fp_params_nav_call): fp_trace :=
  let wp1 := string_of_nat (get_srect_wp1 ps) in
  let wp2 := string_of_nat (get_srect_wp2 ps) in
  C_CODE (gen_fun_call_sem (survey_rectangle_code pc) (wp1 :: wp2 :: nil))
  :: nil.

Definition gen_fp_eight_sem (ps: fp_params_eight) (pm: fp_params_nav_mode)
                                      (pc: fp_params_nav_call): fp_trace :=
  let c := string_of_nat (get_eight_center ps) in
  let t := string_of_nat (get_eight_turn_around ps) in
  let r := get_eight_radius ps in
  app (gen_fp_nav_mode_params_sem pc pm)
      ((C_CODE (gen_fun_call_sem eight_code (c :: t :: r :: nil))) :: nil).

Definition gen_fp_oval_sem (ps: fp_params_oval) (pm: fp_params_nav_mode)
                                    (pc: fp_params_nav_call): fp_trace :=
  let p1 := string_of_nat (get_oval_p1 ps) in
  let p2 := string_of_nat (get_oval_p2 ps) in
  let r  := get_oval_radius ps in
  app (gen_fp_nav_mode_params_sem pc pm)
      ((C_CODE (gen_fun_call_sem oval_code (p1 :: p2 :: r :: nil))) 
        :: nil).

Definition gen_fp_guided_sem (ps: fp_params_guided)
                                      (pc: fp_params_nav_call): fp_trace :=
  let cmds := get_guided_commands ps in
  let flags := get_guided_flags ps in
    (C_CODE (gen_fun_call_nav (guided_code pc) (flags :: cmds :: nil) pc)
      :: nil).

Definition gen_fp_home_sem :=
  C_CODE (gen_fun_call_sem home_code nil) :: nil.

(** Generates semantics for the nav_mode *)
Definition gen_fp_nav_code_sem (nav_mode: fp_navigation_mode): fp_trace :=
  match nav_mode with
  | HEADING ps pm pc => gen_fp_heading_sem ps pm pc
  | ATTITUDE ps pm pc => gen_fp_attitude_sem ps pm pc
  | MANUAL ps pm pc => gen_fp_manual_sem ps pm pc
  | GO ps pm pc => gen_fp_go_sem ps pm pc
  | XYZ ps pm pc => gen_fp_xyz_sem ps pm pc
  | CIRCLE ps pm pc => gen_fp_circle_sem ps pm pc
  | STAY ps pm pc => gen_fp_stay_sem ps pm pc
  | FOLLOW ps pc => gen_fp_follow_sem ps pc
  | SURVEY_RECTANGLE ps pc => gen_fp_survey_rectangle_sem ps pc
  | EIGHT ps pm pc => gen_fp_eight_sem ps pm pc
  | OVAL ps pm pc => gen_fp_oval_sem ps pm pc
  | GUIDED ps pc => gen_fp_guided_sem ps pc
  | HOME => gen_fp_home_sem
  end.
