From Coq Require Import Arith ZArith Psatz Bool
                        String List Program.Equality Program.Wf.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import BasicTypes
                        FlightPlanGeneric
                        FPEnvironmentGeneric
                        FPEnvironmentDef
                        CommonFPDefinition.

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.

Set Implicit Arguments.

(** Generic definition of the semanitics and the forward simulation based
  on CompCert (adapted from CompCert/common/Smallstep.v) *)

(** Definition of a semantics *)

Record fp_semantics: Type := FP_Semantics_gen {
  (** Environment for the semantics *)
  env: Type;
  (** Properties stating a step for the semantics *)
  step: env -> env -> Prop;
  (**  Properties stating if an env is an initial environment *)
  initial_env: env -> Prop;
}.

(** Function that convert a step function into a proposition *)
Definition step_prop {flight_plan: Set} {fp_env: Set}
                          (step: flight_plan -> fp_env -> fp_env ):
                          flight_plan -> fp_env -> fp_env -> Prop :=
  fun fp e e' => step fp e = e'.

(** ** Definition of the bisimulation *)

(** *** Definition of lock-step simulation properties *)

Record fp_fsim_properties (FP1 FP2: fp_semantics)
                       (match_envs: env FP1 -> env FP2 -> Prop) : Prop := {
  (** Initial envs must match *)
  fsim_match_initial_envs:
    forall e1, initial_env e1 ->
    exists e2, initial_env e2 /\ match_envs e1 e2;
  (** Two matching envs must produce two matching envs after a step *)
  fsim_simulation:
    forall e1 e1', step e1 e1' ->
    forall e2, match_envs e1 e2 ->
    exists e2', step e2 e2' /\ match_envs e1' e2';
}.

(** ** Definition of backward simulation *)

Record fp_bsim_properties (FP1 FP2: fp_semantics)
                       (match_envs: env FP1 -> env FP2 -> Prop) : Prop := {
  (** Initial envs must match *)
  bsim_match_initial_envs:
    forall e2, initial_env e2 ->
    exists e1, initial_env e1 /\ match_envs e1 e2;
  (** Two matching envs must produce two matching envs after a step *)
  bsim_simulation:
    forall e2 e2', step e2 e2' ->
    forall e1, match_envs e1 e2 ->
    exists e1', step e1 e1' /\ match_envs e1' e2';
}.

(** *** Definition of the bisimulation property *)

Record fp_bisim_properties (FP1 FP2: fp_semantics)
                       (match_envs: env FP1 -> env FP2 -> Prop) : Prop := {
  (** Forward simulation *)
  fsim: @fp_fsim_properties FP1 FP2 match_envs;

  (** Backward simulation *)
  bsim: @fp_bsim_properties FP1 FP2 match_envs;
}.

Arguments fp_bisim_properties: clear implicits.

Inductive bisimulation (L1 L2: fp_semantics) : Prop :=
  Bisimulation
          (match_envs: env L1 -> env L2 -> Prop)
          (props: fp_bisim_properties L1 L2 match_envs).

Lemma compose_bisimulations:
  forall FP1 FP2 FP3,
    bisimulation FP1 FP2 
    -> bisimulation FP2 FP3
    -> bisimulation FP1 FP3.
Proof.
  move => FP1 FP2 FP3
            [match_envs [[Hfinit Hfstep] [Hbinit Hbstep]]]
            [match_envs' [[Hfinit' Hfstep'] [Hbinit' Hbstep']]].

  set (ff_match_states := fun (e1: env FP1) (e3: env FP3)
    => exists e2, match_envs e1 e2
                      /\ match_envs' e2 e3).
  apply Bisimulation with ff_match_states; do 2 constructor.

  (** Init case for the forward simulation *)
  - move => e1 Hinit1.
    destruct (Hfinit _ Hinit1) as [e2 [Hinit2 Hmatch2]].
    destruct (Hfinit' _ Hinit2) as [e3 [Hinit3 Hmatch3]].
    exists e3; split; try by []. exists e2; by split.

  (** Step case for the forward simulation *)
  - move => e1 e1' Hstep1 e3 Hmatch13.
    destruct Hmatch13 as [e2 [He12 He23]].
    destruct (Hfstep _ _ Hstep1 _ He12) as [e2' [Hstep2 Hmatch12]].
    destruct (Hfstep' _ _ Hstep2 _ He23) as [e3' [Hstep3 Hmatch23]].
    exists e3'; split; try by []. exists e2'; by split.

  (** Init case for the backward simulation *)
  - move => e3 Hinit3.
    destruct (Hbinit' _ Hinit3) as [e2 [Hinit2 Hmatch2]].
    destruct (Hbinit _ Hinit2) as [e1 [Hinit1 Hmatch1]].
    exists e1; split; try by []. exists e2; by split.

  (** Step case for the backward simulation *)
  - move => e3 e3' Hstep3 e1 Hmatch13.
    destruct Hmatch13 as [e2 [He12 He23]].
    destruct (Hbstep' _ _ Hstep3 _ He23) as [e2' [Hstep2 Hmatch12]].
    destruct (Hbstep _ _ Hstep2 _ He12) as [e1' [Hstep1 Hmatch13]].
    exists e1'; split; try by []. exists e2'; by split.
Qed.

(** * Common function for the semantics *)

Module COMMON_SEM (G_FP: GENERIC_FP)
                  (EVAL_Def: EVAL_ENV)
                  (FP_SIG: COMMON_FP_FUN_SIG G_FP)
                  (FP_ENV: FP_ENV_DEF G_FP FP_SIG EVAL_Def).
  Import FP_SIG EVAL_Def FP_ENV.

  (** C code for the change of block *)
  Definition c_change_block (fp: flight_plan)
                              (from: block_id) (to: block_id): fp_trace :=
     (on_exit fp from)
      ++ [:: reset_time; init_stage]
      ++ (on_enter fp (normalise_block_id fp to)).

  (** ** Forbidden deroute functions *)

  (** Functions for forbidden deroute 
    Test if the [fp] is a forbidden deroute (The deroutes correspond and
    the condition is satisfied). Return the res *)
  Definition test_forbidden_deroute (e: fp_env)
                                      (from: block_id) (to: block_id)
                                        (fb: fp_forbidden_deroute):
                                          (bool * fp_env) :=
    if (from =? (get_fbd_from fb)) && (to =? (get_fbd_to fb)) then
      match get_fbd_only_when fb with 
      | None => (true, e)
      | Some cond => FP_ENV.evalc e cond
      end
    else
      (false, e).

  (** Verify recursively if the deroute is forbidden and return the trace
      generated and the new environment where the execution ended. *)
  Fixpoint test_forbidden_deroutes (e: fp_env)
                                    (from: block_id) (to: block_id) 
                                      (l_excp: fp_forbidden_deroutes):
                                        (bool * fp_env) :=
    match l_excp with
    | nil => (false, e)
    | fb :: fbs => 
      match test_forbidden_deroute e from to fb with
      | (true, e') => (true, e')
      | (false, e') => test_forbidden_deroutes e' from to fbs
      end
    end.

  Section FlightPlan.

  (** Definition of the flight plan being executed *)
  Variable fp: flight_plan.

  (** Verify if the deroute is forbidden and return the trace generated 
      and the new environment where the execution ended. *)
  Definition forbidden_deroute (e: fp_env) (to: block_id):
                                    (bool * fp_env) :=
    test_forbidden_deroutes e (get_nav_block e) to
                              (get_fp_forbidden_deroutes fp).

  (** Go to the block if the deroute is not forbidden *)
  Definition goto_block (e: fp_env) (to: block_id): fp_env :=
    (** Deroute forbidden? *)
    let nav_block := (get_nav_block e) in
    let '(res, e') := forbidden_deroute e to in
    if res then 
      e'
    else
      (** Changement of block*)
      let e'' := change_block fp e' to in
      app_trace e'' (c_change_block fp nav_block to).

  (** ** Exception function *)

  (* * Test if there is an exception raised and if the deroute is possible.
  *)
  (** Return the a boolean which will be true
      if an exception has been raised and a new env if there has been a
      deroute. *)
  Definition test_exception (e: fp_env) (ex: fp_exception):
                                 (bool * fp_env) :=
    let nav_block := (get_nav_block e) in
    (** Already in the deroute block? *)
    let id := get_expt_block_id ex in
    if  id =? nav_block then
      (false, e)
    else
      (** Exception raised? *)
      let cond := get_expt_cond ex in
      let '(res, e') := evalc e cond in
      if negb (res) then
        (false, e')
      else
        (** Deroute forbidden? *)
        let exec := get_expt_exec ex in
        let e'' := app_trace e' (opt_c_code_to_trace exec) in
        (true, goto_block e'' id).
  
  Fixpoint test_exceptions (e: fp_env) (exs: fp_exceptions):
                              (bool * fp_env) :=
  match exs with 
  | nil => (false, e)
  | ex :: exs =>
    (* Test exception *)
    match test_exception e ex with
    | (true, e') => (true, e')
    | (false, e') => 
      (* Exception not raised -> continue *)
      test_exceptions e' exs
    end
  end.

  Definition exception (e: fp_env): (bool * fp_env) := 
    match test_exceptions e (get_fp_exceptions fp) with
    | (true, e') => (true, e')
    | (false, e') =>
      let loc_excp := get_local_exceptions fp e' in
      let pre := get_code_block_pre_call (get_current_block fp e') in
      let e'' := app_trace e' pre in 
      test_exceptions e'' loc_excp
    end.

  End FlightPlan.

End COMMON_SEM.
