From Coq Require Import  FunInd Program Program.Equality
                                 Program.Wf.
From VFP Require Import BasicTypes ExtractTraceGeneric
                        ClightGeneration
                        FlightPlanGeneric CommonFPSimplified
                        FPNavigationModeSem.

From compcert Require Import Coqlib Clight Maps
                             Events Memory.
From Coq.PArith Require Import BinPos.

Set Warnings "-parsing".
From mathcomp Require Import all_ssreflect.
Set Warnings "parsing".
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Intv.
Import Clightdefs.ClightNotations.
Local Open Scope nat_scope.
Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope clight_scope.


(** * Envrionment for the Clight Code *)

(** **  Definition of a correct local env for the auto_nav *)
Definition correct_auto_nav_env (e:env) := 
  e!_auto_nav = None.

(** ** Environment *)

Module C_ENV.
  (** Global env *)
  Definition c_genv := genv.

  (** Local environment that does not contain the auto_nav function *)
  Definition auto_nav_env := {e: Clight.env | correct_auto_nav_env e}.

  (** Memory environment *)
  Definition m_env := Mem.mem.

  (** FP C env contains a space memory and a history of the trace *)
  Record fp_cenv := create_fp_cenv {
    get_m_env: m_env;
    get_trace: trace;
  }.

  (** Notation to access to the memory or the trace *)
  Notation "e '.m'" :=  (get_m_env e) (at level 10).
  Notation "e '.t'" :=  (get_trace e) (at level 10).

  (** Equality about c_env *)
  Lemma eq_cenv:
    forall e1 e2,
      e1.m = e2.m
      -> e1.t = e2.t
      -> e1 = e2.
  Proof.
    move => e1 e2 Hm Ht.
    destruct e1, e2. f_equal; by rewrite ?Hm ?Ht.
  Qed.

  (** Get properties about extract trace *)
  Module Def_Trace <: ENV_TRACE.
    Definition event:= event.
    Definition trace := list event.

    (** Environments that contain trace *)
    Definition env := fp_cenv.
    Definition get_trace := get_trace.
  End Def_Trace.
  Import Def_Trace.
  Module Import C_EXTRACT := EXTRACT_TRACE_GEN Def_Trace.
  Export C_EXTRACT.

  Definition app_ctrace (e: fp_cenv) (t: trace): fp_cenv :=
    create_fp_cenv (e.m) (e.t ** t).

  (** Definition of empty auto_nav env *)
  Lemma empty_correct_auto_nav_env:
    correct_auto_nav_env empty_env.
  Proof. by []. Qed.

  Definition empty_auto_nav: auto_nav_env :=
    exist _ empty_env empty_correct_auto_nav_env.

End C_ENV.

(** * Matching relation between FPS env and Clight env*)


(** ** Conversion between fp_trace and trace *)
Module OUT_TO_TRACE.
  (** Definition of functions generating event similar to FPE *)
  Definition cond_event (cond: c_cond) (res: bool): event :=
    (Event_syscall cond nil (EVint (int_of_bool res))).

  Definition code_event (code: c_code): event :=
    (Event_annot code nil).

  Definition code_event_assign (var: c_code) (value: c_code): event :=
    (Event_annot (var ++ " = " ++ value) nil).

  Definition code_event_assign_set (params: fp_params_set): event :=
    let (var, value) := params in
    code_event_assign var value.

  Definition call_optparams (func: c_code)
                                  (optparams: option (list c_code))
                                  : c_code :=
    match optparams with
    | Some params => gen_fun_call_sem func params
    | None => func
    end.

  Definition code_event_optassign (optvar: option c_code)
                                          (func: c_code)
                                          (optparams: option (list c_code)): event :=
    let call := (call_optparams func optparams) in
    match optvar with
    | Some var => code_event_assign var call
    | None => code_event call
    end.

  (** Converts a fp_trace into a trace *)
  Fixpoint fp_trace_to_trace (o: fp_trace): trace :=
    match o with
    | nil => E0
    | SKIP :: o' => (fp_trace_to_trace o')
    | COND cond res:: o' 
      => (cond_event cond res) :: (fp_trace_to_trace o')
    | C_CODE code :: o'
      => (code_event code) :: (fp_trace_to_trace o')
    end.

  Lemma trace_app:
    forall o o',
      fp_trace_to_trace ((o ++ o')%list)
        = (fp_trace_to_trace o) ** (fp_trace_to_trace o').
  Proof.
    induction o as [| e o IHo]; try by [].
    rewrite //= => o'.
    destruct e; try by []; by rewrite IHo.
  Qed.

End OUT_TO_TRACE.
Export OUT_TO_TRACE.
