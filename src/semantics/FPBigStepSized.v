From Coq Require Import Arith ZArith Psatz Bool String List Program.Equality Program.Wf Program FunInd.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonLemmas CommonSSRLemmas
                        CommonLemmasNat8
                        BasicTypes FPNavigationMode FPNavigationModeSem
                        FlightPlanGeneric FlightPlanExtended
                        FlightPlanSized
                        FPBigStepGeneric FPBigStepExtended
                        FPEnvironmentGeneric
                        FPEnvironmentDef.

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.

Set Implicit Arguments.

(** * Definition of the big step semantics for the extended flight plan
    with a verification of the size. *)

Module FPS_BIGSTEP (EVAL_Def: EVAL_ENV)
                            (ENVS_Def: ENVS_DEF EVAL_Def).

  (** Specialisation of the FPE big step *)
  Module Import FPE_BS := FPE_BIGSTEP EVAL_Def ENVS_Def.
  Export FPE_BS.

  Import FP_E_WF ENVS_Def FPS_ENV FPE_ENV Def.

  Section FLIGHT_PLAN.

    (** Definition of the flight plan being executed *)
    Variable fps: flight_plan_sized.
    Definition fpe_wf := proj1_sig fps.
    Definition Hsize := proj2_sig fps.

    Definition fp   := proj1_sig fpe_wf.
    Definition Hwf := proj2_sig fpe_wf.

    Remark eq_fps_fp: (` (` fps)) = fp. by []. Qed.

    (** Incr ids is nat 8*)
    Lemma incr_ids8:
      forall idb ids,
        (forall ids', get_stage fp idb ids <> DEFAULT ids')
        -> is_nat8 (ids + 1).
    Proof.
      move => idb ids Hd.

      have He8 := get_nb_stage8 ((get_well_sized_blocks Hsize) idb).
      rewrite /is_nat8 eq_fps_fp in He8. to_nat He8.

      have Hids: ids < FP_E.default_stage_id fp idb.
      { ssrlia. apply (stage_wf_lt_default Hwf). move => H. 
        rewrite H /FP_E.default_stage in Hd.
        have H' := Hd (FP_E.default_stage_id fp idb). by []. }

      rewrite /FP_E.default_stage_id /FP_E.get_stages in Hids.
      to_nat Hids.

      remember (get_block_stages (get_block fp idb)) as stages.
      rewrite /is_nat8. ssrlia.
    Qed.

    (** Params of end_while are nat 8 *)
    Lemma get_end_while_params_nat8:
      forall e id params,
      fp_env_on_8 e
       -> get_current_stage fp e = FP_E.WHILE id params
        -> is_nat8 ((get_end_while_id params) + 1).
    Proof.
      move => [[idb ids lidb lids] t] id params He Hw.

      rewrite /get_current_stage //= in Hw.
      destruct ((get_wf_while Hwf) _ _ _ _ Hw)
        as [Hids [Hge [Hew [Hid Hgid]]]]; subst.

      apply (incr_ids8 idb).

      move => ids'. by rewrite Hew.
    Qed.

    Lemma get_while_params_nat8:
      forall e id params body,
      fp_env_on_8 e
      -> get_current_stage fp e = FP_E.END_WHILE id params body
      -> is_nat8 (get_while_id params).
    Proof.
      move => [[idb ids lidb lids] t] id params body He Hew.

      rewrite /get_current_stage //= in Hew.
      destruct ((get_wf_end_while Hwf) _ _ _ _ _ Hew)
        as [Hids [Hw [Hid Hgid]]].

      have Hb8 := nav_block8 He. to_nat Hb8.
      apply decr8. apply (incr_ids8 idb).

      move => ids'. by rewrite Hw.
    Qed.

    Lemma id_lt_nb_blocks8:
      forall fpe id,
        verified_fp_e fpe
        -> id < get_nb_blocks fpe
        -> is_nat8 id.
    Proof.
      move => fpe id Hsize Hlt.
      have Hlt' := get_nb_blocks8 Hsize.
      rewrite /nb_blocks_lt_256 /is_nat8 in Hlt'.
      rewrite /is_nat8. to_nat Hlt. ssrlia.
    Qed.

    Lemma is_nav_block8:
    forall fpe e,
      verified_fp_e fpe
      -> fp_env_on_8 e
      ->  is_nat8 (get_nav_block e).
    Proof.
      move => fpe e Hv He8.
      apply get_nb_blocks8 in Hv;
        rewrite /nb_blocks_lt_256 /is_nat8 in Hv. to_nat Hv.
      apply nav_block8 in He8;
        rewrite /FP_E.get_nb_blocks in He8; to_nat He8.
    Qed.

    Lemma is_last_block8:
      forall fpe e,
        verified_fp_e fpe
        -> fp_env_on_8 e
        ->  is_nat8 (get_last_block e).
    Proof.
      move => fpe e Hv He8.
      apply get_nb_blocks8 in Hv;
        rewrite /nb_blocks_lt_256 /is_nat8 in Hv. to_nat Hv.
      apply last_block8 in He8;
        rewrite /FP_E.get_nb_blocks in He8; to_nat He8.
    Qed.

    Lemma default_block8:
      is_nat8 (FP_E.get_nb_blocks fp - 1).
    Proof.
      have He8 := (get_nb_blocks8 Hsize).
      rewrite /nb_blocks_lt_256 /is_nat8 eq_fps_fp in He8. to_nat He8.
      rewrite /is_nat8. ssrlia.
    Qed.

    Lemma default_stage_id8:
      forall e1,
        fp_env_on_8 e1
        -> is_nat8 (default_stage_id fp (Def.get_nav_block e1)).
    Proof.
      move => [[idb ids lidb lids] t] He.
      have Hb8 := nav_block8 He. to_nat Hb8.
      have He8 := get_nb_stage8 ((get_well_sized_blocks Hsize) idb).
      rewrite /is_nat8 eq_fps_fp //= in He8. to_nat He8.
      rewrite /default_stage_id /is_nat8 /get_stages //=. ssrlia.
    Qed.

    Lemma change_trace8:
      forall e1 t,
        fp_env_on_8 e1
        -> fp_env_on_8 (Def.change_trace e1 t).
    Proof.
      move => e1 t  He8. destruct e1, He8.
      repeat split; try by [].
    Qed.

  (** ** Property about correct size for updated env *)

  Lemma update_nav_block8:
    forall e1 e1' id,
      fp_env_on_8 e1
      -> (id < 256)%coq_nat
      -> update_nav_block e1 id = e1'
      -> fp_env_on_8 e1'.
  Proof.
    move => e1 e1' id He8 Hlt Heq; subst e1'; split; rewrite //=;
      try apply (nav_stage8 He8);
      try apply (last_stage8 He8);
      try apply (last_block8 He8).
  Qed.

  Lemma update_nav_block_twice8:
    forall id e1,
      fp_env_on_8 e1
      -> fp_env_on_8
          (update_nav_block
            (update_nav_block e1 id)
              (FP_E.get_nb_blocks fp - 1)).
  Proof.
    move => id [[idb ids lidb lids] t] H; split; rewrite //=.
    - rewrite /FP_E.get_nb_blocks.
      have H' := (get_nb_blocks8 Hsize).
      rewrite /nb_blocks_lt_256 /is_nat8 /get_nb_blocks in H'.
      rewrite /is_nat8 /fp /fpe_wf. ssrlia.
    - apply (nav_stage8 H).
    - apply (last_block8 H).
    - apply (last_stage8 H).
  Qed.

  Lemma update_nav_block_with_last8:
    forall e1 e1',
    fp_env_on_8 e1
    -> update_nav_block e1 (get_last_block e1) = e1'
    -> fp_env_on_8 e1'.
  Proof.
  move => e1 e1' He8 Heq; subst e1'; split; rewrite //=;
    try apply (last_block8 He8);
    try apply (nav_stage8 He8);
    try apply (last_stage8 He8).
  Qed.

  Lemma update_nav_stage8:
    forall e1 e1' id,
      fp_env_on_8 e1
      -> is_nat8 id
      -> update_nav_stage e1 id = e1'
      -> fp_env_on_8 e1'.
  Proof.
    move => e1 e1' id He8 Hid8 Heq; subst e1'; split; rewrite //=;
      try apply (nav_block8 He8);
      try apply (last_stage8 He8);
      try apply (last_block8 He8).
  Qed.

  Lemma update_last_block8:
    forall e1 e1',
      fp_env_on_8 e1
      -> update_last_block e1 (get_nav_block e1) = e1'
      -> fp_env_on_8 e1'.
  Proof.
    move => e1 e1' He8 Heq; subst e1'; split; rewrite //=;
      try apply (nav_block8 He8);
      try apply (nav_stage8 He8);
      try apply (last_stage8 He8).
  Qed.

  Lemma update_last_stage8:
      forall e1 e1',
        fp_env_on_8 e1
        -> update_last_stage e1 (get_nav_stage e1) = e1'
        -> fp_env_on_8 e1'.
    Proof.
      move => e1 e1' He8 Heq; subst e1'; split; rewrite //=;
        try apply (nav_block8 He8);
        try apply (nav_stage8 He8);
        try apply (last_block8 He8).
    Qed.

    Lemma modify_last_on_8:
    forall e1 e1',
      fp_env_on_8 e1
      -> mk_fp_env (get_nav_block e1)
                                  (get_nav_stage e1)
                                  (get_nav_block e1)
                                  (get_nav_stage e1)
                                  (Def.get_trace e1) = e1'
      -> fp_env_on_8 e1'.
  Proof.
    move => e1 e1' He8 Heq; subst e1'; split; rewrite //=;
      try apply (nav_block8 He8);
      try apply (nav_stage8 He8).
  Qed. 

  Lemma update_with_default8:
      forall e1' e1,
        fp_env_on_8 e1
        -> e1' = update_nav_stage e1 (FP_E.default_stage_id fp
                                          (Def.get_nav_block e1))
        -> fp_env_on_8 e1'.
    Proof.
      move => e1' e1 He8 He1'; subst e1'.
      destruct He8. repeat split; try by [].
      by apply default_stage_id8.
    Qed.

    Lemma evalc_cond8:
      forall e1 b e1' cond,
        fp_env_on_8 e1
        -> Def.evalc e1 cond = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      move => [[idb ids lidb lids] t] b
                  [[idb' ids' lidb' lids'] t'] cond He8 Heval.
      rewrite /Def.evalc //= in Heval.
      remember (EVAL_Def.eval t cond) as b';
        destruct He8 as [Hbc Hs Hlbc Hls]; inversion Heval; subst.
      repeat split; try by [].
    Qed.

    Lemma evalc8:
      forall (e: fp_env8) cond,
        exists b (e': fp_env8),
          evalc (` e) cond = (b, ` e').
    Proof.
      move => [e He] cond.
      destruct (evalc e cond) as [b e'] eqn:H.
      by exists b, (exist _ e' (evalc_cond8 He H)).
    Qed.

    Lemma exec_next_stage8:
      forall e1,
        fp_env_on_8 e1
        -> (forall id, get_current_stage fp e1 <> FP_E.DEFAULT id)
        -> fp_env_on_8 (next_stage e1).
    Proof.
      move => [[idb ids lidb lids] t] He8 Hd.

      rewrite /get_current_stage //= in Hd.

      destruct He8 as [Hidb Hids Hlidb Hlids].

      repeat split; try by [].

      by apply (incr_ids8 idb).
    Qed.

    Lemma next_stage8:
      forall (e: fp_env8),
        (forall id, get_current_stage fp (` e) <> FP_E.DEFAULT id)
        -> exists (e': fp_env8),
          next_stage (` e) = ` e'.
    Proof.
      move => [e He] Hne.
      by exists (exist _ (next_stage e) (exec_next_stage8 He Hne)).
    Qed.

    Lemma current_stage8:
      forall e1 stage,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = stage
        -> is_nat8 (FP_E.get_stage_id stage).
    Proof.
      move => [[idb ids lidb lids] t] stage He8 Hc.
      destruct (le_lt_dec (FP_E.default_stage_id fp idb) ids) as [Hle|Hlt].
      - have H := default_stage_id8 He8.
        rewrite //= /default_stage_id in H.
        by rewrite -Hc /get_current_stage //= 
                  (ids_ge_default Hwf Hle).
      - have Hs8 := get_nb_stage8 ((get_well_sized_blocks Hsize) idb).
        rewrite /is_nat8 in Hs8. rewrite /default_stage_id
                                                /get_stages in Hlt.
        have H := (get_wf_numbering Hwf) 
                      _ _ ((get_wf_no_default Hwf) _ _ Hlt).
        rewrite -Hc /get_current_stage //= H /is_nat8.
        rewrite -eq_fps_fp in Hlt. ssrlia.
    Qed.

    (** ** Lemma to verify the semantics *)

    Lemma reset_stage8:
      forall e1,
        fp_env_on_8 e1
        -> fp_env_on_8 (reset_stage fp e1).
    Proof.
      move => e1 He8.
      repeat split; try by destruct He8.
      apply (default_stage_id8 He8).
    Qed.

    Lemma exec_return_block8:
      forall e1 params,
        fp_env_on_8 e1
        -> fp_env_on_8 (return_block fp e1 params).
    Proof.
      move => e1 params He8.
      destruct e1, He8, params;
        repeat split; try by []; try apply zero8.
    Qed.

    Lemma return_block8:
      forall (e: fp_env8) params,
        exists (e': fp_env8),
          return_block fp (` e) params = ` e'.
    Proof.
      move => [e He] params.
      by exists (exist _ _ (exec_return_block8 params He)).
    Qed.

    (** App_trace preserve fp_env_on_8 *)
    Lemma app_trace8:
      forall e t,
        fp_env_on_8 e
        -> fp_env_on_8 (Def.app_trace e t).
    Proof.
      move => e t H. destruct e, H. repeat split; try by [].
    Qed.

    (** Propeties about deroute block id are nat8 *)
    Remark get_correct_from8:
      forall fbd,
      correct_fbd_deroute fp fbd
      -> is_nat8 (get_fbd_from fbd).
    Proof.
      move => fbd Hc.
      have H := get_correct_from Hc.
        rewrite /is_user_id /fp /fpe_wf in H. to_nat H.
      have H' := get_nb_blocks8 Hsize.
        rewrite /nb_blocks_lt_256 /is_nat8 in H'. to_nat H'.
      rewrite /is_nat8. ssrlia.
    Qed.

    Remark get_correct_to8:
      forall fbd,
      correct_fbd_deroute fp fbd
      -> is_nat8 (get_fbd_to fbd).
    Proof.
      move => fbd Hc.
      have H := get_correct_to Hc.
        rewrite /is_user_id /fp /fpe_wf in H. to_nat H.
      have H' := get_nb_blocks8 Hsize.
        rewrite /nb_blocks_lt_256 /is_nat8 in H'. to_nat H'.
      rewrite /is_nat8. ssrlia.
    Qed.

    (** Nav block unchanged after forbidden deroute *)
    Lemma test_forbidden_deroute_unchanged:
      forall ex e1 e1' id res,
        FPE_BS.Common_Sem.test_forbidden_deroute e1
        (Def.get_nav_block e1) id ex = (res, e1')
        -> Def.get_nav_block e1 = Def.get_nav_block e1'.
    Proof.
      move => [from to [cond |]] e1 e1' id res Ht;
        rewrite /FPE_BS.Common_Sem.test_forbidden_deroute //= in Ht;
        destruct ((_ =? from) && (id =? to)); try by inversion Ht.
    Qed.

    Lemma test_forbidden_deroutes_unchanged:
      forall exs e1 e1' id res,
        FPE_BS.Common_Sem.test_forbidden_deroutes e1
         (Def.get_nav_block e1) id exs = (res, e1')
        -> Def.get_nav_block e1 = Def.get_nav_block e1'.
    Proof.
      induction exs as [|ex exs IHexs]; move => e1 e1' id res Hfb.
      - by inversion Hfb.
      - rewrite //= in Hfb.
        destruct (FPE_BS.Common_Sem.test_forbidden_deroute _ _ _)
          as [[] e1''] eqn:Hexec;
        have H:= (test_forbidden_deroute_unchanged _ _ _ Hexec).
        + inversion Hfb. by subst.
        + rewrite H. eapply IHexs. rewrite -H. apply Hfb.
    Qed.

    Lemma forbidden_deroute_unchanged:
      forall e1 e1' id res,
        FPE_BS.Common_Sem.forbidden_deroute fp e1 id
        = (res, e1')
        -> Def.get_nav_block e1 = Def.get_nav_block e1'.
    Proof.
      move => e1 e1' id res Hfb.
      rewrite /FPE_BS.Common_Sem.forbidden_deroute in Hfb.
      apply (test_forbidden_deroutes_unchanged _ _ _ Hfb).
    Qed.

    (** Test of test forbidden deroute *)
    Lemma test_test_forbidden_deroute8:
      forall fb e1 b e1' from to,
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.test_forbidden_deroute e1
            from to fb = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      move => [from to optcond] e1 b e1' from' to' He8 Hfb.
      rewrite /FPE_BS.Common_Sem.test_forbidden_deroute in Hfb.

      destruct (_ && _).
      - destruct optcond as [cond|]; rewrite //= in Hfb.
        * apply (evalc_cond8 He8 Hfb).
        * inversion Hfb; by subst e1'.
      - inversion Hfb; by subst e1'.
    Qed.

    Lemma test_forbidden_deroute8:
      forall (e: fp_env8) from to fb,
        exists b (e': fp_env8),
          FPE_BS.Common_Sem.test_forbidden_deroute (` e) from to fb
            = (b, ` e').
    Proof.
      move => [e He] from to fb.
      destruct (FPE_BS.Common_Sem.test_forbidden_deroute e from to fb)
        as [b e'] eqn:H.
      by exists b, (exist _ e' (test_test_forbidden_deroute8 _ _ _ He H)).
    Qed.

    (** Test of test forbidden deroutes *)
    Lemma test_test_forbidden_deroutes8:
      forall fbd e1 b e1' id,
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.test_forbidden_deroutes e1
            (Def.get_nav_block e1) id fbd = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      induction fbd as [|fb fbd IHfbd];
        rewrite /FPE_BS.Common_Sem.test_forbidden_deroutes
           => e1 b e1' id He8 Hfbd.
      - inversion Hfbd; by subst e1'.
      - destruct (FPE_BS.Common_Sem.test_forbidden_deroute e1 _ _)
          as [b' e1''] eqn:Hfb.
        have Hfb8 := test_test_forbidden_deroute8 _ _ _ He8 Hfb.
        destruct b'.
        * inversion Hfbd; by subst e1''.
        * apply (IHfbd _ b _ id Hfb8).
          rewrite /FPE_BS.Common_Sem.test_forbidden_deroutes.
          by rewrite -(test_forbidden_deroute_unchanged _ _ _ Hfb).
    Qed.

    (** Test of forbidden deroute *)
    Lemma exec_forbidden_deroute8:
      forall e1 b e1' id,
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.forbidden_deroute fp e1 id
                = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.Common_Sem.forbidden_deroute
        => e1 b e1' id He8 Hfbd.

      apply (test_test_forbidden_deroutes8 _ id He8 Hfbd).
    Qed.

    Lemma forbidden_deroute8:
      forall (e: fp_env8) id,
        exists b (e': fp_env8),
          FPE_BS.Common_Sem.forbidden_deroute fp (` e) id
            = (b, ` e').
    Proof.
      move => [e He] id.
      destruct (FPE_BS.Common_Sem.forbidden_deroute fp e id)
        as [b e'] eqn:H.
      by exists b, (exist _ e' (exec_forbidden_deroute8 id He H)).
    Qed.

    Lemma normalise_block_id8:
      forall idb,
        is_nat8 (FP_E.normalise_block_id fp idb).
    Proof.
      rewrite /is_nat8 /FP_E.normalise_block_id => idb.

      have H := get_nb_blocks8 Hsize.
      rewrite /nb_blocks_lt_256 /is_nat8 in H. to_nat H.
      rewrite /fp /fpe_wf.

      destruct (_ <=? _) eqn:Hle.
      - ssrlia.
      - apply leb_complete_conv in Hle. ssrlia.
    Qed.

    Lemma exec_change_block8:
      forall e1 e1' id,
        fp_env_on_8 e1
        -> Def.change_block fp e1 id = e1'
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /Def.change_block
        => e1 e1' id He8 Hc.
      destruct (_ =? _) eqn:Heq;
        destruct He8 as [Hidb Hids Hlidb Hlids];
        destruct e1 as [[idb ids lidb lids] t];
        rewrite -Hc //=; repeat split; try by []; try apply zero8.
      - apply Nat.eqb_eq in Heq; subst id. by apply normalise_block_id8.
      - rewrite /FP_E.normalise_block_id.
        have H' := get_nb_blocks8 Hsize.
        rewrite /nb_blocks_lt_256 /is_nat8 in H'.
        destruct (FP_E.get_nb_blocks _ <=? id) eqn:Hlt'.
        * rewrite /=. have H := nb_blocks_ne_0 fp.
          rewrite /is_nat8 /fp /fpe_wf /=. ssrlia.
        * rewrite /=. apply leb_complete_conv in Hlt'.
          rewrite /fp /fpe_wf in Hlt'.
          rewrite /is_nat8 /=. ssrlia.
    Qed.

    Lemma change_block8:
      forall (e1: fp_env8) id,
        exists (e1': fp_env8),
          Def.change_block fp (` e1) id = (` e1').
    Proof.
      move => [e1 He1] id.
      remember (Def.change_block fp e1 id) as e1' eqn:H; symmetry in H.
      by exists (exist _ e1' (exec_change_block8 id He1 H)).
    Qed.

    Lemma goto_block_possible_result :
      forall e1 e1' id,
      FPE_BS.Common_Sem.goto_block fp e1 id = e1'
      -> get_nav_block e1' = (FP_E.normalise_block_id fp id)
      \/ get_nav_block e1' = get_nav_block e1.
    Proof.
      rewrite /Common_Sem.goto_block.
      intros e1 e2 id H.
      destruct (Common_Sem.forbidden_deroute _ _ _) as [res e1'] eqn:fb.
      have Hid := (forbidden_deroute_unchanged _ _ fb).
      destruct res.
      - right. by rewrite Hid H.
      - left. rewrite <- H.
        rewrite //= /change_block.
        destruct (_ =? _); by [].
    Qed.

    Lemma goto_block_forbidden_derout_nraised :
      forall e1 e1' e2 id,
      Common_Sem.forbidden_deroute fp e1 id = (false, e1')
      -> Common_Sem.goto_block fp e1 id = e2
      -> get_nav_block e2 = (FP_E.normalise_block_id fp id).
    Proof.
      intros e1 e1' e2 id fb. 
      have Hid := (forbidden_deroute_unchanged _ _ fb).
      rewrite /Common_Sem.goto_block fb.
      intros H; subst. rewrite //= /change_block.
      destruct (_ =? _); by [].
    Qed.

    Lemma exec_goto_block8:
      forall e1 e1' id,
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.goto_block fp e1 id = e1'
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.Common_Sem.goto_block => e1 e1' id He8 Hgoto.
      destruct (FPE_BS.Common_Sem.forbidden_deroute _ _ _)
        as [b e1''] eqn:Hfbd.
      have Hfb8 := exec_forbidden_deroute8 _ He8 Hfbd.
      destruct b.
      - inversion Hgoto; by subst e1'.
      - inversion Hgoto as[[H H']]; rewrite H. subst e1'.
        apply app_trace8.
        remember (Def.change_block _ _ _) as e1'''.
        symmetry in Heqe1'''. apply (exec_change_block8 _ Hfb8 Heqe1''').
    Qed.

    Lemma goto_block8:
      forall (e: fp_env8) id,
        exists (e': fp_env8),
        FPE_BS.Common_Sem.goto_block fp (` e) id = ` e'.
    Proof.
      move => [e He] id.
      remember (FPE_BS.Common_Sem.goto_block fp e id) as e' eqn:H;  
        symmetry in H.
      by exists (exist _ e' (exec_goto_block8 id He H)).
    Qed.

    (** Execution of exceptions*)

    (** Nav block unchanged if test_exception not raised *)
    Lemma test_exception_nraised_unchanged:
      forall ex e1 e1',
        FPE_BS.Common_Sem.test_exception fp e1 ex = (false, e1')
        -> Def.get_nav_block e1 = Def.get_nav_block e1'.
    Proof.
      rewrite /Common_Sem.test_exception /evalc.
      intros ex e1 e1'.
      destruct (_ =? _); try by (intros H; inversion H).
      destruct (~~ EVAL_Def.eval _ _);
      by (intros H; inversion H).
    Qed.

    Lemma exec_test_exception8:
      forall e1 ex b e1',
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.test_exception fp e1 ex
                = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      move => e1 ex b e1' He8 Hex.
      rewrite /FPE_BS.Common_Sem.test_exception in Hex.

      destruct (_ =? _).
      - inversion Hex; by subst e1'.
      - destruct (Def.evalc e1 (get_expt_cond ex))
          as [b' e1''] eqn:Heval.
        have He8' := evalc_cond8 He8 Heval.
        destruct b'; rewrite //= in Hex.
        * remember (FPE_BS.Common_Sem.goto_block _ _ _)
            as e1''' eqn:Hgoto.
          inversion Hex; subst e1'.
          rewrite Hgoto. eapply exec_goto_block8.
          eapply app_trace8. apply He8'. by [].
        * inversion Hex; by subst e1'.
    Qed.

    Lemma test_exception8:
      forall (e: fp_env8) ex,
        exists b (e': fp_env8),
        FPE_BS.Common_Sem.test_exception fp (` e) ex = (b, ` e').
    Proof.
      move => [e He] ex.
      destruct (FPE_BS.Common_Sem.test_exception fp e ex) as [b e']
        eqn:H.
      by exists b, (exist _ e' (exec_test_exception8 ex He H)).
    Qed.

    (** Execution of exceptions *)

    (** Nav block unchanged if test_exceptions not raised *)
    Lemma test_exceptions_nraised_unchanged:
      forall lex e1 e1',
        FPE_BS.Common_Sem.test_exceptions fp e1 lex = (false, e1')
        -> Def.get_nav_block e1 = Def.get_nav_block e1'.
    Proof.
      induction lex.
      - simpl. intros e1 e1' H.
        by inversion H.
      - simpl.
        intros e.
        destruct (Common_Sem.test_exception _ _ _)
        as [b e'] eqn:Test.
        destruct b. 
        + intros e1' contra.
          by inversion contra.
        + have Sol := (test_exception_nraised_unchanged _ _ Test).
          simpl in Sol. rewrite Sol.
          by apply IHlex.
    Qed.

    Lemma exception_nraised_unchanged:
      forall e1 e1',
        FPE_BS.Common_Sem.exception fp e1 = (false, e1')
        -> Def.get_nav_block e1 = Def.get_nav_block e1'.
    Proof.
      intros e1 e2 Hex.
      rewrite /Common_Sem.exception in Hex.
      destruct (Common_Sem.test_exceptions _ _ _) as [b e1'] eqn:Exc1.
      destruct b; try by [].
      rewrite (test_exceptions_nraised_unchanged _ _ Exc1).
      by rewrite -(test_exceptions_nraised_unchanged _ _ Hex).
    Qed.

    Lemma exec_test_exceptions8:
      forall exs e1 b e1',
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.test_exceptions fp e1
              exs = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      induction exs as [| ex exs IHexs];
      rewrite /FPE_BS.Common_Sem.test_exceptions
        => e1 b e1' He8 Hexs.
      - inversion Hexs; by subst e1'.
      - destruct (FPE_BS.Common_Sem.test_exception _ _ _)
          as [b' e1''] eqn:Hex.
        have Hex8 :=  exec_test_exception8 _ He8 Hex.
        destruct b'.
        * inversion Hexs; by subst e1'.
        * destruct (FPE_BS.Common_Sem.test_exceptions fp e1'' exs)
            as [b' e1'''] eqn:Hexs'.
          have H := IHexs _ _ _ Hex8 Hexs'.
          rewrite /FPE_BS.Common_Sem.test_exceptions in Hexs'.
          rewrite Hexs' in Hexs. 
          inversion Hexs; by subst e1'.
    Qed.

    Lemma test_exceptions8:
      forall (e: fp_env8) exs,
        exists b (e': fp_env8),
        FPE_BS.Common_Sem.test_exceptions fp (` e) exs = (b, ` e').
    Proof.
      move => [e He] exs.
      destruct (FPE_BS.Common_Sem.test_exceptions fp e exs) as [b e']
        eqn:H.
      by exists b, (exist _ e' (exec_test_exceptions8 exs He H)).
    Qed.

    (** Execution of global exceptions *)
    Lemma test_global_exceptions8:
      forall e1 b e1',
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.test_exceptions fp e1
               (FP_E_WF.Common.get_fp_exceptions fp)
                = (b, e1')
        -> fp_env_on_8 e1'.
    Proof. apply exec_test_exceptions8. Qed.

    (** Execution of local exceptions *)
    Lemma test_local_exceptions8:
      forall e1 b e1',
        fp_env_on_8 e1
        -> FPE_BS.Common_Sem.test_exceptions fp e1
            (Def.get_local_exceptions fp e1) = 
              (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      move => e1 b e1' He8 Hexs.
      apply (exec_test_exceptions8 _ He8 Hexs).
    Qed.

    Lemma exec_while_sem8:
      forall e1 b e1' id params,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.WHILE id params
        ->  FPE_BS.while_sem e1 params = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.while_sem => e1 b e1' id params He8 Hc Hs.
      destruct (Def.evalc _ _)
        as [b' e1''] eqn:Heval.
      have He8':= evalc_cond8 He8 Heval.
      destruct b'; inversion Hs.
      - eapply app_trace8; try by []. apply exec_next_stage8; try by [].
        by rewrite -(evalc_get_current_stage fp Heval) Hc.
      - repeat split; try by destruct He8'.
        rewrite (evalc_get_current_stage fp Heval) in Hc.
        apply (get_end_while_params_nat8 He8' Hc).
    Qed.

    Lemma exec_end_while_sem8:
      forall e1 b e1' id params body,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.END_WHILE id params body
        ->  FPE_BS.end_while_sem e1 params = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.end_while_sem => e1 b e1' id params body He8 Hc Hs.
      
      remember (update_nav_stage _ _) as e1''.
      have He8': fp_env_on_8 e1''.
      { rewrite Heqe1''. repeat split; try by destruct He8.
         apply (get_while_params_nat8 He8 Hc). }

      have Hc': get_current_stage fp e1''
                     = FP_E.WHILE (get_while_id params) params.
      { rewrite /get_current_stage //= in Hc.
        destruct ((get_wf_end_while Hwf) _ _ _ _ _ Hc)
          as [Hids [Hw [Hid Hgid]]].
        rewrite /get_current_stage -Hw
                  /get_stage Heqe1''. by destruct e1.  }

      apply (exec_while_sem8 He8' Hc' Hs).
    Qed.

    Lemma exec_set_sem8:
      forall e1 b e1' id params,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.SET id params
        ->  FPE_BS.set_sem e1 params = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.set_sem => e1 b e1' id params He8 Hc Hs.
      injection Hs as Hb He; subst.
      apply exec_next_stage8; try by [].
      eapply app_trace8; try by [].
      by rewrite -app_trace_get_current_stage Hc.
    Qed.

    Lemma exec_call_sem8:
      forall e1 b e1' id params,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.CALL id params
        ->  FPE_BS.call_sem e1 params = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.call_sem
        => e1 b e1' id [func until [] break] He8 Hc Hs;
        rewrite //= in Hs.

      destruct (EVAL_Def.eval _ func).

      destruct until as [cond|].
      - destruct (EVAL_Def.eval _ cond); injection Hs as Hb He; subst.
        + eapply app_trace8; try by [].
          repeat (apply change_trace8).
          apply exec_next_stage8; try by []; by rewrite Hc.
        + repeat (apply app_trace8; try by []).
      - inversion Hs; try by subst e1'. by apply app_trace8.
  
      all: destruct break; inversion Hs; subst e1'; try apply app_trace8;
            try apply change_trace8; apply exec_next_stage8; try by [];
              try by rewrite Hc.
    Qed.

    Lemma exec_deroute_sem8:
      forall e1 b e1' id params,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.DEROUTE id params
        ->  FPE_BS.deroute_sem fpe_wf e1 (get_deroute_block_id params)
               = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.deroute_sem => e1 b e1' id params He8 Hc Hs.

      destruct (FPE_BS.Common_Sem.goto_block _ _ _) as [e1'' o'] eqn:Hgoto.

      inversion Hs; subst e1'.
      eapply exec_goto_block8.
      - eapply app_trace8. apply (exec_next_stage8 He8); by rewrite Hc.
      - apply Hgoto.
    Qed.

    Lemma exec_return_sem8:
      forall e1 b e1' id params,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.RETURN id params
        ->  FPE_BS.return_sem fpe_wf e1 params = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.return_sem => e1 b e1' id params He8 Hc Hs.
      inversion Hs. apply app_trace8. apply (exec_return_block8 _ He8).
    Qed.

    Lemma exec_nav_init_sem8:
      forall e1 b e1' id params,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.NAV_INIT id params
        ->  FPE_BS.nav_init_sem e1 params = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.nav_init_sem => e1 b e1' id params He8 Hc Hs.
      inversion Hs. apply app_trace8.
      apply exec_next_stage8; try by []; by rewrite Hc.
    Qed.

    Lemma exec_nav_code_sem:
      forall e1 b e1' id params until,
      fp_env_on_8 e1
      -> get_current_stage fp e1 = FP_E.NAV id params until
      ->  FPE_BS.nav_code_sem e1 params until = (b , e1')
      -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.nav_code_sem => e1 b e1' id params until He8 Hc Hs.
      destruct until as [cond|].
      - destruct (Def.evalc _ cond)
          as [b' e1''] eqn:Heval.
        destruct b'; inversion Hs; subst e1'; try by [].

        apply exec_next_stage8; try by []. apply app_trace8.
        apply (evalc_cond8 (app_trace8 _ He8) Heval).

        + by rewrite -app_trace_get_current_stage
                  -(evalc_get_current_stage fp Heval)
                  -app_trace_get_current_stage Hc.
        + apply app_trace8.
          apply (evalc_cond8 (app_trace8 _ He8) Heval).
      - injection Hs as Hb He; subst e1'.
        by repeat apply app_trace8. 
    Qed.

    Lemma nav_code_sem8:
      forall (e: fp_env8) id params until,
        get_current_stage fp (` e) = FP_E.NAV id params until
        -> exists b (e': fp_env8),
            FPE_BS.nav_code_sem (` e) params until = (b, ` e').
    Proof.
      move => [e He] id params until H.
      destruct (FPE_BS.nav_code_sem e params until) as [b e'] eqn:H'.
      by exists b, (exist _ e' (exec_nav_code_sem He H H')).
    Qed.

    Lemma exec_nav_sem8:
      forall e1 b e1' id params until,
        fp_env_on_8 e1
        -> get_current_stage fp e1 = FP_E.NAV id params until
        ->  FPE_BS.nav_sem e1 params until = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.nav_sem => e1 b e1' id params until He8 Hc Hs.

      destruct (nav_cond_sem params) as [c|].
      - destruct (Def.evalc _ c)
          as [b' e1''] eqn:Heval.
        have He8' := evalc_cond8 (app_trace8 _ He8) Heval.
        destruct b'.
        * injection Hs as Hb He; subst e1'.
          apply exec_next_stage8; try by [].
          apply app_trace8; try by [].
          by rewrite -app_trace_get_current_stage
                    -(evalc_get_current_stage fp Heval)
                    -app_trace_get_current_stage Hc.
        * eapply (exec_nav_code_sem He8').
           + injection Heval as Hb' He'; subst e1''.
             rewrite -?app_trace_get_current_stage. apply Hc.
           + apply Hs. 
      - eapply (exec_nav_code_sem).
        * eapply app_trace8. apply He8.
        * rewrite -app_trace_get_current_stage. apply Hc.
        * apply Hs.
    Qed.

    Lemma exec_default_sem8:
      forall e1 b e1',
        fp_env_on_8 e1
        ->  FPE_BS.default_sem fpe_wf e1 = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.default_sem
                /FPE_BS.next_block => e1 b e1' He8 Hs.
      destruct (get_nav_block _ <? _) eqn:Hlt.
      all: destruct (FPE_BS.Common_Sem.goto_block _)
              as [e1'' o'] eqn:Hgoto;
           inversion Hs; subst e1';
           apply (exec_goto_block8 _ (reset_stage8 He8) Hgoto).
    Qed.

    Lemma exec_next_block8:
      forall e1 e1',
        fp_env_on_8 e1
        ->  FPE_BS.next_block fpe_wf e1 = e1'
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.next_block 
        => e1 e1' He8 Hnext.
      destruct (get_nav_block _ <? _) eqn:Hlt.
      all: destruct (FPE_BS.Common_Sem.goto_block _)
              as [e1'' o'] eqn:Hgoto;
           inversion Hnext; subst e1';
           apply (exec_goto_block8 _ He8 Hgoto).
    Qed.

    Lemma next_block8:
      forall (e: fp_env8),
        exists (e': fp_env8),
        FPE_BS.next_block fpe_wf (` e) = ` e'.
    Proof.
      move => [e He].
      remember (FPE_BS.next_block fpe_wf e) as e' eqn:H.
      symmetry in H.
      by exists (exist _ e' (exec_next_block8 He H)).
    Qed.

    Lemma exec_run_stage8:
      forall e1 b e1',
        fp_env_on_8 e1
        -> FPE_BS.run_stage fpe_wf e1 = (b, e1')
        -> fp_env_on_8 e1'.
    Proof.
      rewrite /FPE_BS.run_stage => e1 b e1' He8 Hr.
      destruct (get_current_stage (FPE_BS.fp fpe_wf) e1)
        eqn:Hc.
      - apply (exec_while_sem8 He8 Hc Hr).
      - apply (exec_end_while_sem8 He8 Hc Hr).
      - apply (exec_set_sem8 He8 Hc Hr).
      - apply (exec_call_sem8 He8 Hc Hr).
      - destruct params. apply (exec_deroute_sem8 He8 Hc Hr).
      - apply (exec_return_sem8 He8 Hc Hr).
      - apply (exec_nav_init_sem8 He8 Hc Hr).
      - apply (exec_nav_sem8 He8 Hc Hr).
      - apply (exec_default_sem8 He8 Hr).
    Qed.

    Lemma run_stage8:
      forall (e: fp_env8),
        exists b (e': fp_env8),
        FPE_BS.run_stage fpe_wf (` e) = (b, ` e').
    Proof.
      move => [e He].
      destruct (FPE_BS.run_stage fpe_wf e) as [b e'] eqn:H.
      by exists b, (exist _ e' (exec_run_stage8 He H)).
    Qed.

    Lemma exec_run_step8:
      forall e e',
        fp_env_on_8 e
        -> FPE_BS.run_step fpe_wf e = e'
        -> fp_env_on_8 e'.
    Proof.
      have H:=  (FPE_BS.run_step_ind fpe_wf). move => e. apply H; clear H.
      - move => e' e'' Hr Hind e''' He8 Hs.
        have He8' := (exec_run_stage8 He8 Hr).
        apply (Hind _ He8' Hs).
      - move => e' e'' Hs e''' He8 H. subst  e'''. 
        apply (exec_run_stage8 He8 Hs).
    Qed.

    Lemma run_step8:
      forall (e: fp_env8),
        exists (e': fp_env8),
        FPE_BS.run_step fpe_wf (` e) = ` e'.
    Proof.
      move => [e He].
      remember (FPE_BS.run_step fpe_wf e) as e' eqn:H;
        symmetry in H.
      by exists (exist _ e' (exec_run_step8 He H)).
    Qed.

    Lemma exec_step8:
      forall e e',
        fp_env_on_8 e
        -> FPE_BS.step fpe_wf e = e'
        -> fp_env_on_8 e'.
    Proof.
      rewrite /FPE_BS.step / FPE_BS.Common_Sem.exception
        => e e' He8 Hstep.

      destruct (FPE_BS.Common_Sem.test_exceptions (FPE_BS.fp fpe_wf) e _)
        as [b e1] eqn:Hgex.
      have He18 := test_global_exceptions8 He8 Hgex.
      destruct b.

      (* Global exception raised *)
      inversion Hstep; by subst e1.

      remember (Common.get_code_block_pre_call _ ) as t.
      destruct (FPE_BS.Common_Sem.test_exceptions (FPE_BS.fp fpe_wf)
        (app_trace e1 t) _) as [b e1'] eqn:Hlex.
      have Hexct8 := app_trace8 t He18.
      have He1'8 := test_local_exceptions8 Hexct8 Hlex.
      destruct b.

      (* Local exception raised *)
      inversion Hstep; by subst e1'.

      destruct (FPE_BS.run_step fpe_wf e1') eqn:Hrun.
      inversion Hstep; subst.
      apply app_trace8. by apply (exec_run_step8 He1'8 Hrun).
    Qed.

    Lemma step8:
      forall (e: fp_env8),
        exists (e': fp_env8),
        FPE_BS.step fpe_wf (` e) = ` e'.
    Proof.
      move => [e He].
      remember (FPE_BS.step fpe_wf e) as e' eqn:H;
        symmetry in H.
      by exists (exist _ e' (exec_step8 He H)).
    Qed.

    (** The flight plan must be well-sized *)
    Program Definition step (e: fp_env8): fp_env8 :=
      (FPE_BS.step fpe_wf (` e)).
    Next Obligation.
      destruct e as [e He8].
      destruct (FPE_BS.step fpe_wf e) as [o e'] eqn:Hstep.
      rewrite Hstep. by apply (exec_step8 He8 Hstep).
    Qed.

    Lemma is_init_env8:
      fp_env_on_8 (init_env fp).
    Proof.
      rewrite /init_env. split; try by []; rewrite //= /is_nat8; ssrlia.
    Qed.

    Definition init_env8: fp_env8 :=
      exist (fp_env_on_8) (init_env fp) is_init_env8.

    Definition init_env8_prop (e: fp_env8) :=
      init_env8 = e.

  (** properties about environment correctness *)

  (** a correct_env is an environement that have a correct block id
    and a correct id for the last block *)
    Definition correct_env (e : fp_env) : Prop :=
      correct_block_id (` (` fps)) (get_nav_block e) /\ correct_block_id (` (` fps)) (get_last_block e).
  
    (** if only the current block id is correct, the environment is
      semi-correct *)
    Definition semi_correct_env (e : fp_env) : Prop :=
      correct_block_id (` (` fps)) (get_nav_block e).
  
    (** a correct environment is a semi-correct environment *)
    Lemma correct_env_is_semi_correct : 
      forall e,
      correct_env e -> semi_correct_env e.
    Proof.
      intros e [Sol _]. apply Sol.
    Qed.
  
    (** the initial environment is a correct environment *)
    Lemma init_env_is_correct :
      correct_env (init_env (` (` fps))).
    Proof.
      unfold init_env. split;
      apply nb_blocks_ne_0.
    Qed.
    
    Lemma normalise_is_semi_correct : 
      forall e,
      semi_correct_env (normalise (` (` fps)) e).
    Proof.
      intros e.
      unfold semi_correct_env. unfold normalise. simpl.
      apply normalise_block_id_is_correct.
    Qed.

    Lemma full_normalise_is_correct :
      forall e,
        correct_env (full_normalise (` (` fps)) e).
    Proof.
      intros e. 
      unfold full_normalise. split; simpl;
      apply normalise_block_id_is_correct.
    Qed.
  
    Lemma correct_through_normalise : 
      forall e,
      correct_env e -> correct_env (normalise (` (` fps)) e).
    Proof.
      intros e [_ H]. unfold normalise. split; simpl.
      - apply normalise_block_id_is_correct.
      - apply H.
    Qed.
  
  End FLIGHT_PLAN.

  Definition step_size (fps: flight_plan_sized) (e e': fp_env8) :=
    step fps e = e'.
 
  Definition semantics_fps (fp: flight_plan_sized): fp_semantics := 
    FP_Semantics_gen (step_size fp) (init_env8_prop fp).

  (** Definition of an induction principle *)
  Lemma run_step_ind8:
  forall (fps : flight_plan_sized)
          (P : FPE_ENV.fp_env -> FPE_ENV.fp_env -> Prop),
    (forall e e' e'': fp_env8,
        run_stage (` fps) (` e) = (true, ` e')
        -> run_step (` fps) (` e') = ` e''
        -> P (` e') (` e'') 
        -> P (` e) (` e''))
    -> (forall e e' : fp_env8,
          run_stage (` fps) (`e) = (false, ` e')
          -> P (` e) (` e'))
    -> (forall e e' : fp_env8,
          run_step (` fps) (` e) = (` e')
          -> P (` e) (` e')).
Proof.
  move => fps P IH1 IH2 e e' H.
  have He' : fp_env_on_8 (` e') by destruct e'.
    generalize He'.
  have He : fp_env_on_8 (` e) by destruct e.
    generalize He.
  rewrite -H.
  apply run_step_ind.
  - move => e1 e1' Hr Hp He1 He1'.

    destruct (run_stage8 fps (exist _ _ He1)) as [b [e1'' He1'']].
    rewrite /= Hr in He1''. injection He1'' as Hb He1''; subst e1' b.

    destruct (run_step8 fps (exist _ _ He1')) as [e1_f He1_f].
    rewrite /= in He1_f.

    replace e1 with (` (exist _ _ He1)); try by [].
    replace (run_step _ _) with (` (exist _ _ He1')); try by [].

    apply IH1 with (e' := e1'') => //.
    apply Hp => //. by destruct e1''.

  - move => e1 e1' Hr He1 He1'.
    destruct (run_stage8 fps (exist _ _ He1)) as [b [e1'' He1'']].
    rewrite /= Hr in He1''. injection He1'' as Hb Heq; subst e1' b.

    replace e1 with (` (exist _ _ He1)); try by [].

    by apply IH2.
Qed.

End FPS_BIGSTEP.
