From Coq Require Import Program.
From VFP Require Import ClightGeneration 
                        FPBigStepGeneric
                        FPEnvironmentClight
                        CommonFPSimplified.
From compcert Require Import Coqlib Integers AST Ctypes
                             Cop Clight 
                             Values Smallstep.
From Coq.PArith Require Import BinPos.

Set Warnings "-parsing".
From mathcomp Require Import all_ssreflect.
Set Warnings "parsing".
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Clightdefs.ClightNotations.
Local Open Scope nat_scope.
Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope clight_scope.

Set Implicit Arguments.

(** * Encapsulate the execution of Clight code of the autonav function *)

Module C_BIGSTEP.
  Import C_ENV.

  (** Statement to call the auto_nav function *)
  Definition autoNav := gen_call_void_fun _auto_nav.

  Variant step: program -> fp_cenv -> fp_cenv -> Prop :=
    | FP_step:
      forall p cge e e',
        cge = Clight.globalenv p
      -> (forall f (env: auto_nav_env) le, 
          star step2 cge
                (State f autoNav Kstop (` env) le (get_m_env e))
                (extract_trace e e')
                (State f Sskip Kstop (` env) le (get_m_env e')))
      -> trace_appended e e'
      -> step p e e'.

  (** Definition of the init Clight state*)
  Record initial_state (prog: program) (e_init: fp_cenv):=
                                      create_initial_state {
    get_init_m_env:
      Globalenvs.Genv.init_mem prog = Some (get_m_env e_init);
    get_init_trace: (C_ENV.get_trace e_init) = nil;
  }.

  Definition semantics_fpc (prog: program): fp_semantics :=
    FP_Semantics_gen (step prog) (initial_state prog).

(** * Proof that step is deterministic *)

(** convert a find_def intro a find_funct *)
Lemma find_def_to_funct:
    forall (ge: genv) b f,
      Globalenvs.Genv.find_def ge b = Some (Gfun (Internal f))
      -> Globalenvs.Genv.find_funct ge (Vptr b Ptrofs.zero)
            = Some (Internal f).
Proof.
  move => ge b f Hfd.

  rewrite Globalenvs.Genv.find_funct_find_funct_ptr.
  apply Globalenvs.Genv.find_funct_ptr_iff; apply Hfd.
Qed.

(** Tactics to subst to subst all occurence of c 
    if (H := a = b) and (H' := a = c)  *)
Ltac retreive H H' :=
  rewrite H in H'; inversion H'; subst.

(** Load bitfield is deterministic *)
Lemma load_bitfield_deterministic:
  forall t sz sg pos width m loc ofs v v',
  load_bitfield t sz sg pos width m (Vptr loc ofs) v
  -> load_bitfield t sz sg pos width m (Vptr loc ofs) v'
  -> v = v'.
Proof.
  move => t sz sg pos width m loc ofs v v' Hl Hl'.
  inversion Hl; subst.
  inversion Hl'; subst.
  by retreive H3 H17.
Qed.

(** Store bitfield is determinante *)
Lemma store_bitfield_deterministic:
  forall t sz sg pos width m loc ofs m1 m2 v v1 v2,
  store_bitfield t sz sg pos width m (Vptr loc ofs) v m1 v1
  -> store_bitfield t sz sg pos width m (Vptr loc ofs) v m2 v2
  -> m1 = m2 /\ v1 = v2.
Proof.
  move => t sz sg pos width m loc ofs m1 m2 v v1 v2 Hs Hs'.
  inversion Hs; subst; inversion Hs'; subst.
  retreive H3 H20. by retreive H4 H21.
Qed.

(** Deref loc is deterministic *)
Lemma deref_loc_deterministic:
  forall t m loc ofs bf v v',
  deref_loc t m loc ofs bf v
  -> deref_loc t m loc ofs bf v'
  -> v = v'.
Proof.
  move => t m loc ofs bf v v' Hd Hd'.
  inversion Hd; subst.

  (* Value *)
  inversion Hd'; subst; retreive H H1; by retreive H0 H2.

  (* Reference *)
  inversion Hd'; subst; by retreive H H0.

  (* Copy *)
  inversion Hd'; subst; by retreive H H0.

  (* Loc bitfield *)
  inversion Hd'; subst; by apply (load_bitfield_deterministic H H5).
Qed.

(** Definition of a proposition to use the induction principle for 
    the proof that eval expr is deterministic *)
Definition Prop_eval_lvalue ge e le m a loc ofs bf:=
  forall loc' ofs' bf', eval_lvalue ge e le m a loc' ofs' bf'
  -> loc = loc' /\ ofs = ofs' /\ bf = bf'.

Definition Prop_eval_expr ge e le m a v:=
  forall v', eval_expr ge e le m a v'
  -> v = v'.

(** Lemma defined to apply easily the eval_expr_ind principle *)
Lemma eval_expr_deterministic_ind:
  forall ge e le m a v,
  eval_expr ge e le m a v
  -> Prop_eval_expr ge e le m a v.
Proof.
  move => ge e le m a v He.
  have H := (eval_expr_ind2 ge e le m
              (Prop_eval_expr ge e le m)
              (Prop_eval_lvalue ge e le m)).
  apply H => // ; rewrite /Prop_eval_expr /Prop_eval_lvalue;
    intros; clear H.

  (* Const int *)
  inversion H0 => //; subst; try inversion H.

  (* Const float *)
  inversion H0 => //; subst; try inversion H.

  (* Vsingle *)
  inversion H0 => //; subst; try inversion H.

  (* Vlong *)
  inversion H0 => //; subst; try inversion H.

  (* Etmpvar *)
  { inversion H1.
    - by retreive H0 H4.
    - inversion H. }

  (* Eaddrof *)
  { inversion H2; subst.
    - destruct (H1 _ _ _ H5) as [Hl [Ho Hbf]]; by subst.
    - inversion H. }

  (* Eunop *)
  { inversion H3; subst.
    - apply H1 in H7. subst. by retreive H2 H8.
    - inversion H. }

  (* Ebinop *)
  { inversion H5; subst.
    - apply H1 in H10. apply H3 in H11. subst.
      by retreive H4 H12.
    - inversion H. }

  (* Ecast *)
  { inversion H3; subst.
    - apply H1 in H5; subst. by retreive H2 H7.
    - inversion H. }

  (* Esizeof *)
  inversion H0 => //; subst; try inversion H.

  (* Ealignof *)
  inversion H0 => //; subst; try inversion H.

  (* lvalue *)
  { inversion H3; subst; try by inversion H0.
    destruct (H1 _ _ _ H) as [Hl [Ho Hb]]; subst.
    have Heq := (deref_loc_deterministic H2 H4); by subst. }

  (* lvalue local *)
  { inversion H1; subst.
    - by retreive H0 H6.
    - rewrite H0 in H3; inversion H3. }

  (* lvalue global *)
  { inversion H2; subst.
    - rewrite H0 in H7; inversion H7.
    - by retreive H1 H8. }

  (* lvalue deref *)
  { inversion H2; subst.
    apply H1 in H7. inversion H7. by subst. }

  (* lvalue struct *)
  { inversion H5; subst; retreive H2 H9.
    retreive H3 H13. retreive H4 H14.
    apply H1 in H8; inversion H8. by subst. }

  (* lvalue union *)
  { inversion H5; subst; apply H1 in H8; inversion H8; subst.
    - by retreive H2 H9.
    - retreive H2 H9. retreive H3 H13.
      by retreive H4 H14. }
Qed.

(** Eval_expr is deterministic *)
Lemma eval_expr_deterministic:
  forall ge e le m a v v',
  eval_expr ge e le m a v
  -> eval_expr ge e le m a v'
  -> v = v'.
Proof.
  move => ge e le m a v v' He He'.
  by apply (eval_expr_deterministic_ind He He').
Qed.

(** Eval_lvalue is deterministic *)
Lemma eval_lvalue_deterministic:
  forall ge e le m a loc loc' ofs ofs' bf bf',
  eval_lvalue ge e le m a loc ofs bf
  -> eval_lvalue ge e le m a loc' ofs' bf'
  -> loc = loc' /\ ofs = ofs' /\ bf = bf'.
Proof.
  move => ge e le m a loc loc' ofs ofs' bf bf' He He'.
  inversion He; subst.

  (* Local *)
  { inversion He'; subst.
    - by retreive H H5.
    - rewrite H in H2; inversion H2. }

  (* Global *)
  { inversion He'; subst.
    - rewrite H in H6; inversion H6.
    - by retreive H0 H7. }

  (* Deref *)
  { inversion He'; subst.
    have Heq := eval_expr_deterministic H H5.
    inversion Heq; by subst. }

  (* Field struct *)
  { inversion He'; subst; retreive H0 H7.
    have Heq := eval_expr_deterministic H H6.
    inversion Heq; subst.
    retreive H1 H11. by retreive H2 H12. }

  (* Field union *)
  { inversion He'; subst; have Heq := eval_expr_deterministic H H6;
    inversion Heq; subst; retreive H0 H7.
    retreive H1 H11. by retreive H2 H12. }

Qed.

(** Eval_exprlist is deterministic *)
Lemma eval_exprlist_deterministic:
  forall al ge e le m tyargs v v',
  eval_exprlist ge e le m al tyargs v
  -> eval_exprlist ge e le m al tyargs v'
  -> v = v'.
Proof.
  induction al as [|a al IH];
    move => ge e le m tyargs v v' He He';
    inversion He; subst; inversion He'; subst => //.
  have Heq := (eval_expr_deterministic H1 H6). subst.
  retreive H2 H8. f_equal.
  by apply (IH _ _ _ _ _ _ _ H5 H9).
Qed.

(** Assign_loc is deterministic *)
Lemma assign_loc_deterministic:
  forall ge t m loc ofs bf v m1 m2,
  assign_loc ge t m loc ofs bf v m1
  -> assign_loc ge t m loc ofs bf v m2
  -> m1 = m2.
Proof.
  move => ge t m loc ofs bf v m1 m2 Ha Ha'.
  inversion Ha; subst; inversion Ha'; subst; try retreive H H4.
  - retreive H H1. by retreive H0 H2.
  - rewrite H in H1; by inversion H1.
  - rewrite H in H5; by inversion H5.
  - retreive H3 H11. by retreive H4 H12.
  - destruct (store_bitfield_deterministic H H6). by subst.
Qed.

(** Alloc_variables is deterministic *)
Lemma alloc_variables_deterministic:
  forall f e ge m e1 e2 m1 m2,
  alloc_variables ge e m (fn_vars f) e1 m1
  -> alloc_variables ge e m (fn_vars f) e2 m2
  -> e1 = e2 /\ m1 = m2.
Proof.
  move => f;
    induction (fn_vars f) as [|[id v] vars IH];
    move => ge e m e1 e2 m1 m2 Ha Ha'.
  - inversion Ha; subst ; by inversion Ha'.
  - inversion Ha; inversion Ha'; subst.
    retreive H6 H15.
    by apply (IH _ _ _ _ _ _ _ H7 H16).
Qed.

(** Function Entry2 is deterministic *)
Lemma function_entry2_deterministic:
  forall ge f vargs m e1 e2 le1 le2 m1 m2,
  function_entry2 ge f vargs m e1 le1 m1
  -> function_entry2 ge f vargs m e2 le2 m2
  -> e1 = e2 /\ le1 = le2 /\ m1 = m2.
Proof.
  move => ge f vargs m e1 e2 le1 le2 m1 m2 Hf Hf'.
  inversion Hf. inversion Hf'.
  retreive H3 H8.
  destruct (alloc_variables_deterministic H2 H7) as [He Hm].
  by subst.
Qed.

(** The No External Call property (or NEC) defined a statement, a state or a global environement
    with no call to external function. it is used to prove that Clight is determinist *)


(** No_external_call_statement st state that st doesn't have any direct external call
    e.i. there is no Sbuiltin.
    However, Scall are alowed. this will be handle with the global environement *)
Inductive No_external_call_statement : statement -> Prop :=
  | NEC_Sskip : 
      No_external_call_statement Sskip
  | NEC_Sassign : 
      forall x v,
      No_external_call_statement (Sassign x v)
  | NEC_Sset : 
      forall x v,
      No_external_call_statement (Sset x v)
  | NEC_Scall : 
      forall i e le,
      No_external_call_statement (Scall i e le)
  (** No Sbuiltin *)
  | NEC_Ssequence st1 st2 :
      No_external_call_statement st1
      -> No_external_call_statement st2
      -> No_external_call_statement (Ssequence st1 st2)
  | NEC_Sifthenelse e st1 st2 :
      No_external_call_statement st1
      -> No_external_call_statement st2
      -> No_external_call_statement (Sifthenelse e st1 st2)
  | NEC_Sloop st1 st2 :
      No_external_call_statement st1
      -> No_external_call_statement st2
      -> No_external_call_statement (Sloop st1 st2)
  | NEC_Sbreak : 
      No_external_call_statement Sbreak
  | NEC_Scontinue : 
      No_external_call_statement Scontinue
  | NEC_Sreturn : 
      forall e, 
      No_external_call_statement (Sreturn e)
  | NEC_Sswitch : 
      forall e lst, 
      No_external_call_statement_labeled lst
      -> No_external_call_statement (Sswitch e lst)
  | NEC_Slabel : 
      forall l st,
      No_external_call_statement st
      -> No_external_call_statement (Slabel l st)
  | NEC_Sgoto : 
      forall l, 
      No_external_call_statement (Sgoto l)

with No_external_call_statement_labeled : labeled_statements -> Prop :=
  | NECL_LSnil : 
      No_external_call_statement_labeled LSnil
  | NECL_LScons : forall l st lst, 
      No_external_call_statement st
      -> No_external_call_statement_labeled lst
      -> No_external_call_statement_labeled (LScons l st lst).

Lemma NECSL_NECSseq : 
  forall sl,
  No_external_call_statement_labeled sl <-> No_external_call_statement (seq_of_labeled_statement sl).
Proof.
  intros sl. split; intros H; induction sl; try by constructor.
  - simpl. constructor; inversion H; subst. apply H2. apply (IHsl H4).
  - simpl in H. inversion H; subst. constructor; auto.
Qed.

Inductive Forall_lb (P : Clight.statement -> Prop) : labeled_statements -> Prop :=
  | Falb_nil : 
      Forall_lb P LSnil
  | Falb_cons : 
      forall o s lbs',
      (P s) 
      -> (Forall_lb P lbs') 
      -> Forall_lb P (LScons o s lbs').


(** advanced induction principle for Clight.statement with NEC.
  used in No_external_after_goto demonstration *)

Section NEC_statement_induction.
  Variable P : Clight.statement -> Prop.

  Hypothesis Sskip_case:
    P (Sskip).

  Hypothesis Sassign_cas:
    forall e1 e2, P (Sassign e1 e2).
  
  Hypothesis Sset_case:
    forall ident e, P (Sset ident e).
  
  Hypothesis Scall_case:
    forall opid e le, P (Scall opid e le).

  Hypothesis Sbuiltin_case:
    forall opid ef t le, P (Sbuiltin opid ef t le).
  
  Hypothesis Sseqence_case:
    forall s1 s2, 
    P s1
    -> P s2
    -> P (Ssequence s1 s2).

  Hypothesis Sifthenelse_case:
    forall e s1 s2, 
    P s1
    -> P s2
    -> P (Sifthenelse e s1 s2).

  Hypothesis Sloop_case:
    forall s1 s2,
    P s1
    -> P s2
    -> P (Sloop s1 s2).

  Hypothesis Sbreak_case:
    P (Sbreak).

  Hypothesis Scontinue_case:
    P (Scontinue).
  
  Hypothesis Sreturn_case:
    forall ope, P (Sreturn ope).

  Hypothesis Sswitch_case:
    forall e ls,
    Forall_lb P ls
    -> P (Sswitch e ls).

  Hypothesis Slabel_case:
    forall lbl s, P s
    -> P (Slabel lbl s).

  Hypothesis Sgoto_case:
    forall lbl, P (Sgoto lbl).

  Fixpoint statement_ind_nec (s : Clight.statement) : P s :=
      match s with
        | Sskip => Sskip_case
        | Sassign e1 e2 => Sassign_cas e1 e2
        | Sset ident e => Sset_case ident e
        | Scall opid e le => Scall_case opid e le
        | Sbuiltin opid ef tl le => Sbuiltin_case opid ef tl le
        | Ssequence s1 s2 => Sseqence_case (statement_ind_nec s1)
                                           (statement_ind_nec s2)
        | Sifthenelse e s1 s2 => Sifthenelse_case e
                                                (statement_ind_nec s1)
                                                (statement_ind_nec s2)
        | Sloop s1 s2 => Sloop_case (statement_ind_nec s1)
                                    (statement_ind_nec s2)
        | Sbreak => Sbreak_case
        | Scontinue => Scontinue_case
        | Sreturn ope => Sreturn_case ope
        | Sswitch e ls => Sswitch_case e (statement_ind_nec_lb ls)
        | Slabel lbl s => Slabel_case lbl (statement_ind_nec s) 
        | Sgoto lbl => Sgoto_case lbl
      end

      with statement_ind_nec_lb (ls : labeled_statements) : Forall_lb P ls :=
        match ls with
          | LSnil => Falb_nil P
          | LScons o s lb' => Falb_cons o
                                        (statement_ind_nec s)
                                        (statement_ind_nec_lb lb')
        end.
  
End NEC_statement_induction.

(** Selecting a branch of a switch with no external call give us 
  a statement with no external call*)
Lemma No_external_after_switch : 
  forall a sl n,
  No_external_call_statement (Sswitch a sl)
  -> No_external_call_statement (seq_of_labeled_statement
                                          (select_switch n sl)).
Proof. 
  intros a sl. induction sl; try by constructor.
  intros n NECswitch.
  destruct o; rewrite /select_switch.
  - rewrite /select_switch_case. destruct (zeq z n).
    + inversion NECswitch. by apply NECSL_NECSseq.
    + apply IHsl. inversion NECswitch. constructor. by inversion H0.
  - simpl. destruct (select_switch_case n sl) eqn:SelectRes.
    + unfold select_switch in IHsl. have IHsln := (IHsl n). 
        rewrite SelectRes in IHsln. apply IHsln.
        inversion NECswitch. constructor. by inversion H0.
    + inversion NECswitch. by apply NECSL_NECSseq.  
Qed.

(** similar to the statament, No_external_call_cont state that a Clight
  cont have no Sbuiltin statement *)
Inductive No_external_call_cont : cont -> Prop :=
  | NEC_Kstop : 
      No_external_call_cont Kstop
  | NEC_Kseq : 
      forall s c1,
      No_external_call_statement s
      -> No_external_call_cont c1
      -> No_external_call_cont (Kseq s c1)
  | NEC_Kloop1 : 
      forall s1 s2 c1,
      No_external_call_statement s1
      -> No_external_call_statement s2
      -> No_external_call_cont c1
      -> No_external_call_cont (Kloop1 s1 s2 c1)
  | NEC_Kloop2 : 
      forall s1 s2 c1,
      No_external_call_statement s1
      -> No_external_call_statement s2
      -> No_external_call_cont c1
      -> No_external_call_cont (Kloop2 s1 s2 c1)
  | NEC_Kswitch : 
      forall c1,
      No_external_call_cont c1
      -> No_external_call_cont (Kswitch c1)
  | NEC_Kcall : 
      forall opident f e te c1,
      No_external_call_statement (fn_body f)
      -> No_external_call_cont c1
      -> No_external_call_cont (Kcall opident f e te c1).

Lemma No_external_after_goto : 
  forall fb lbl k s' k',
  No_external_call_statement fb
  -> No_external_call_cont k
  -> find_label lbl fb k = Some (s', k')
  -> No_external_call_statement s' /\ No_external_call_cont k'.
Proof.
  intros fb lbl. induction fb using statement_ind_nec; 
  try discriminate.

  { (** Sseq *)
    simpl. intros k s' k' NECs NECk. inversion NECs; subst. 
    destruct (find_label lbl fb1 (Kseq fb2 k)) eqn:FL1. 
    - intros Hp. rewrite Hp in FL1.
      apply (IHfb1 (Kseq fb2 k) _ _); try assumption; constructor;
      assumption.
    - by apply IHfb2. }

  { (** Sifthenelse *)
    simpl. intros k s' k' NECs NECk. inversion NECs; subst. 
    destruct (find_label lbl fb1 k) eqn:FL1. 
    - intros Hp. rewrite Hp in FL1.
      by apply (IHfb1 k _ _).
    - by apply IHfb2.  }

  { (** Sloop *)
    simpl. intros k s' k' NECs NECk. inversion NECs; subst. 
    destruct (find_label lbl fb1 (Kloop1 fb1 fb2 k)) eqn:FL1. 
    - intros Hp. rewrite Hp in FL1.
      apply (IHfb1 (Kloop1 fb1 fb2 k) _ _); try assumption.
      constructor; assumption. 
    - apply (IHfb2 (Kloop2 fb1 fb2 k)).
      apply H2. constructor; assumption. }
    
  { (** Sswitch *)
    simpl. induction ls. 
    - simpl. discriminate.
    - simpl. intros k s' k' NECs NECk. 
      destruct (find_label lbl _ _) eqn:FLres.
      * intros Eqp. rewrite Eqp in FLres. clear Eqp.
        inversion H; subst.
        inversion NECs; subst.
        inversion H1; subst.
        eapply H2.
        + apply H5.
        + apply NEC_Kseq.
          -- apply NECSL_NECSseq. apply H7.
          -- apply NEC_Kswitch. apply NECk.
        + apply FLres.
      * inversion H; subst. apply (IHls H4).
        + constructor. 
          inversion NECs; subst. 
          by inversion H1.
        + apply NECk. }

  { (** Slabel *)
    simpl. destruct (ident_eq lbl lbl0); subst.
    - intros k s' k' NECs NECk Eq. 
      inversion Eq; subst. by inversion NECs.
    - intros k s' k' NECs.
      inversion NECs. by apply IHfb. }
Qed.

(** a No_external_call_state (or NEC_state) is a state that is
  - not currently calling an external function, 
  - is executing a statement with no external call
  and have no external call in it's continuation
*)
Inductive No_external_call_state : Clight.state -> Prop :=
  | NEC_SState : 
      forall f s k e le m,
      No_external_call_statement (fn_body f)
      -> No_external_call_statement s
      -> No_external_call_cont k
      -> No_external_call_state (State f s k e le m)
  | NEC_SCallstate : 
      forall fI args k m,
      No_external_call_statement (fn_body fI)
      -> No_external_call_cont k 
        (** only calling internal function *)
      -> No_external_call_state (Callstate (Internal fI) args k m)
  | NEC_SReturnstate : 
      forall res k m,
      No_external_call_cont k
      -> No_external_call_state (Returnstate res k m).
      
(** a No external_call_environement is a global environemnt
 with only internal function definition that have no Sbuiltin 
 e.i. finding a function in that environement either :
  - doesn't retrun a function (function not found)
  - give you an internal function with no Sbuiltin *)
Definition No_external_call_environement (ge : genv ) := 
  forall idf,
    (Globalenvs.Genv.find_funct ge idf = None \/
    exists idfI, 
      (Globalenvs.Genv.find_funct ge idf = Some (Internal idfI)) /\
      No_external_call_statement (fn_body idfI)).

(** a NEC state in a NEC environement give a NEC state*)
Theorem NEC_state_to_NEC_state : 
  forall ge s1 t s2,
  No_external_call_environement ge
  -> No_external_call_state s1
  -> step2 ge s1 t s2
  -> No_external_call_state s2.
Proof.
  intros ge s1 t s2 NECge NECs1 H.
  induction H; inversion NECs1; subst;
  try by repeat(constructor).
  { (** Scall *)
    destruct (NECge vf) as [H4 | [fI [H4 NECfI]]]; 
    rewrite H4 in H2;
    inversion H2; subst.
    - constructor. apply NECfI.
      constructor; assumption. }
  
  { (** Sseq *)
    inversion H6; subst. constructor.
    apply H2. apply H1.
    constructor; assumption. }

  { (** Sseq_Skip *)
    inversion H7; subst.
    constructor; assumption. }
  
  { (** Sseq_continue *)
    inversion H7; subst; constructor; assumption. }

  { (** SbreakSeq*)
    inversion H7; subst; constructor; assumption. }

  { (** Sifthenelse *)
    destruct b; inversion H8; subst; constructor; assumption. }

  { (** Sloop1 *)
    inversion H6; subst; constructor; try assumption.
    constructor; assumption. }
  
  { (** Sloop1 Skip or continue *)
    inversion H8; subst; constructor; try assumption; constructor; assumption. }
  
  { (** Sloop1 break *)
    inversion H7; subst; constructor; try assumption; constructor. }

  { (** Sloop2 skip *)
    inversion H7; subst; constructor; try assumption.
    constructor; assumption. }

  { (** Sloop2 break *)
    inversion H7; subst; constructor; try assumption; constructor. }

  { (** Sreturn None *)
    constructor. clear NECs1. induction k; simpl; 
    inversion H8; subst; auto. }

  { (** Sreturn Some a *)
    constructor. clear NECs1. induction k; simpl;
    inversion H10; subst; auto. }

  { (** Sswitch *)
    constructor; try assumption.
    clear NECs1. apply No_external_after_switch with a. apply H8.
    constructor. apply H9. }

  { (** Sswitch skip or break *)
    inversion H8; subst. constructor; try assumption; constructor. }

  { (** Sswitch continue *)
    inversion H7; subst. constructor; try assumption; constructor. }

  { (** Slabel *)
    inversion H6; subst. constructor; assumption. }

  { (** Sgoto *)
    apply No_external_after_goto in H as [NECs' NECk'].
    constructor; auto.
    apply H3. clear H. clear NECs1. induction k; simpl; 
    inversion H8; subst; auto. 
  }

  { (** Return Kcall *)
    inversion H0. subst. constructor; auto. constructor. }
Qed.

(** Clight Step is deterministic if there is no external call*)
Lemma clight_step_deterministic:
  forall ge s t t' s1 s2,
  No_external_call_state s
  -> step2 ge s t s1
  -> step2 ge s t' s2
  -> t = t' /\ s1 = s2.
Proof.
  intros ge s t t' s1 s2 NECs.
  induction 1; move => Hstep;
    try by (inversion Hstep; subst).

  (* Assign *)
  { inversion Hstep. subst.
    destruct (eval_lvalue_deterministic H H12) as [Hloc [Hofs Hbf]].
      subst.
    have Hv := (eval_expr_deterministic H0 H13). subst.
    retreive H1 H14.
    have Hm := (assign_loc_deterministic H2 H15). by subst.

    all: destruct H11 as [H' | H']; by inversion H'. }

  (* Set *)
  { inversion Hstep; subst.
      have Hv := (eval_expr_deterministic H H9). by subst.

    all: destruct H8 as [H'|H']; by inversion H'. }

  (* Scall *)
  { inversion Hstep; subst.
      retreive H H14.
      have Hv := (eval_expr_deterministic H0 H15). subst.
      retreive H2 H17. retreive H3 H18.
      have Hv' := (eval_exprlist_deterministic H1 H16). by subst.

    all: destruct H12 as [H'|H']; by inversion H'. }

  (* Sbuiltin *)
  { inversion NECs; subst. inversion H8. }

  (* Sequence *)
  { inversion Hstep; subst => //.
    all: destruct H7 as [H' | H']; by inversion H'. }

  (* Sifthenelse *)
  { inversion Hstep; subst; split => //.
      have Hv := (eval_expr_deterministic H H11). subst.
      by retreive H0 H12.

    all: destruct H9 as [H'|H']; by inversion H'. }

  (* Sloop *)
  { inversion Hstep; subst => //.
    all: destruct H7 as [H' | H']; by inversion H'. }

  (* Kloop *)
  { inversion Hstep; subst => //.
    all: try destruct H as [H' | H']; by inversion H'. }

  (* Kloop Sbreak *)
  { inversion Hstep; subst => //.
    destruct H9 as [H' | H']; by inversion H'. }

  (* Sreturn None *)
  { inversion Hstep; subst.
    all: try by (destruct H8 as [H' | H']; by inversion H').
    rewrite H in H7. inversion H7. by subst. }

  (* Sreturn Some val *)
  { inversion Hstep; subst.
    all: try by (destruct H10 as [H' | H']; by inversion H').
    retreive H1 H12.
    have Hv := (eval_expr_deterministic H H10). subst.
    by retreive H0 H11. }

  (* Sskip Return state *)
  { inversion Hstep; subst.
    all: try by rewrite /is_call_cont in H.
    by retreive H0 H9. }

  (* Sswitch *)
  { inversion Hstep; subst.
    all: try by (destruct H9 as [H' | H']; by inversion H').
    have Hv := (eval_expr_deterministic H H10). subst.
    by retreive H0 H11. }

  (* Skip Sbreak S breeak *)
  { inversion Hstep; subst.
    all: by (destruct H as [H' | H']; by inversion H'). }

  (* Scontinue Kswitch *)
  { inversion Hstep; subst => //.
    by (destruct H7 as [H' | H']; by inversion H'). }

  (* Slabel *)
  { inversion Hstep; subst => //.
    all: by (destruct H7 as [H' | H']; by inversion H'). }

  (* Sgoto *)
  { inversion Hstep; subst.
    all: try by (destruct H8 as [H' | H']; by inversion H').
    rewrite H in H8. inversion H8. by subst. }

  (* Callstate *)
  { inversion Hstep; subst.
    destruct (function_entry2_deterministic H H6) as [He [Hle Hm]].
    by subst. }

  (* External call *)
  { inversion NECs. }

Qed.

(** Proof that it is not possible a plus execution from similar state with
    a Sskip *)
Lemma star_skip_not_possible:
  forall ge f e le m m' t t' s,
    step2 ge (State f Sskip Kstop e le m) t s
    -> star step2 ge s t' (State f Sskip Kstop e le m')
    -> False.
Proof.
  move => ge f e le m m' t t' s Hstep Hstar.
  inversion Hstep; subst.
  inversion Hstar; subst.
  inversion H; subst.
Qed.

(** Induction lemma to prove that the execution of a Clight code is
    deterministic *)
Lemma function_exec_deterministic_ind:
  forall ge s t s',
    No_external_call_environement ge ->
    No_external_call_state s ->
    star step2 ge s t s'
    -> (forall f e le t' m1 m2,
        s' = State f Sskip Kstop e le m1
    -> star step2 ge
        s t' (State f Sskip Kstop e le m2)
    -> t = t' /\ m1 = m2).
Proof.
  intros ge s t s' NECge NECs.
  induction 1; move => f e le t' m1 m2 Hs Hstep.
  - subst s. inversion Hstep; subst => //.
    by apply (star_skip_not_possible H) in H0.
  - inversion Hstep.
    * subst. by apply (star_skip_not_possible H) in H0.
    * destruct (clight_step_deterministic NECs H H2) as [Ht Hs']. 
      subst t' s0 s5 t4 t s3 t1 s2.
      apply (NEC_state_to_NEC_state NECge NECs) in H.
      have Heq: t2 = t3 /\ m1 = m2 by eapply (IHstar H).
      destruct Heq. by subst t2 m1.
Qed.

(** Execution of Clight code is deterministic *)
Lemma function_exec_deterministic: 
  forall ge s f e le t t' m1 m2,
    No_external_call_environement ge ->
    No_external_call_state s ->
    star step2 ge
        s t (State f Sskip Kstop e le m1)
    -> star step2 ge
        s t' (State f Sskip Kstop e le m2)
    -> t = t' /\ m1 = m2.
Proof.
  move => ge s f e le t t' m1 m2 NECge NECs Hstep Hstep'.
  by eapply (function_exec_deterministic_ind NECge NECs Hstep).
Qed.

(** Execution of the auto_nav code is deterministic *)
Lemma step_auto_nav_deterministic:
  forall ge f e le m t t' s s',
    step2 ge
      (State f autoNav Kstop e le m)
       t s
    -> step2 ge
      (State f autoNav Kstop e le m)
       t' s'
    -> t = t' /\ s = s'.
Proof.
  move => ge f env le m t t' s s' Hs Hs'.
  inversion Hs; subst. inversion Hs'; subst.
  retreive H9 H14. have Heq := eval_expr_deterministic H10 H15; subst.
  have Heq' := eval_exprlist_deterministic H11 H16; subst.
  by retreive H12 H17.
Qed.

(** Call state to internal is deterministic *)
Lemma step_auto_nav_callstate_deterministic:
  forall ge fI vargs f e le m t t' s s',
  step2 ge
    (Callstate (Internal fI) vargs
        (Kcall None f e le Kstop) m) t s
  -> step2 ge
    (Callstate (Internal fI) vargs
         (Kcall None f e le Kstop) m) t' s'
  -> t = t' /\ s = s'.
Proof.
  move => ge fd vargs f env le m t t' s s' Hs Hs'.
  inversion Hs; subst; inversion Hs'; subst.
  - destruct (function_entry2_deterministic H5 H6) as [He [Hle Hm]].
    by subst.
Qed.

(** Step function is deterministic under the condition that the function
  auto_nav is in the global_env and it is not a External function*)
(** And the global_env have no External call *)
Lemma step_deterministic_gen:
  forall p cge e e1 e2,
    cge = Clight.globalenv p
    -> No_external_call_environement cge ->
    (exists b f,
      Globalenvs.Genv.find_symbol cge _auto_nav = Some b
      /\ Globalenvs.Genv.find_def cge b = Some (Gfun (Internal f))
      /\ No_external_call_statement (fn_body f))
    -> step p e e1
    -> step p e e2
    -> e1 = e2.
Proof.
  move => p cge e e1 e2 Hcge NECge [b [f [Hfs [Hfd NECf]]]] H H'.
  inversion H as [p' cge0 e0 e' Hp Hs Ht]; subst p' cge0 e0 e'.
  inversion H' as [p' cge0 e0 e' Hp Hs' Ht']; subst p' cge0 e0 e' cge.
  set env := empty_auto_nav.
  set le := create_undef_temps [::].
  have Hstep := Hs f env le; clear Hs.
  have Hstep' := Hs' f env le; clear Hs'.

  (* Step call *)
  inversion Hstep. subst s1 s3 t.
  inversion Hstep'. subst s1 s3 t.

  destruct (step_auto_nav_deterministic H0 H3) as [Ht1 Hs'].
  subst t0 s0. clear H3.

  inversion H0. subst f0 optid a al k e0 le0 m t1 s2.

  (* Callstate *)
  inversion H1. subst t2 t s1 s3.
  inversion H4. subst t3 t s1 s3.

  destruct NECge with vf as [fI | [idfI [Eqfd NECfI]]].
  - retreive fI H18.
  - retreive Eqfd H18.
  destruct (step_auto_nav_callstate_deterministic H3 H7)
    as [Ht1 Hs']. subst t2 s0. clear H7.

  inversion H3. subst idfI vargs0 k m t1 s2.

  remember (State f0 (fn_body f0) (Kcall None f empty_env le Kstop) e0 le0 m1) as s0.

  assert (NECs0 : No_external_call_state s0).
  {
    subst s0. constructor; try assumption.
    constructor. 
    apply NECf. constructor.
  }

  (* Internal call *)
  destruct (function_exec_deterministic NECge NECs0 H6 H8) as [Ht1 Hs'].
    subst t0. rewrite -H5 in H2.

  apply (eq_cenv Hs' (eq_extract_trace Ht Ht' H2)).
Qed.

End C_BIGSTEP.
