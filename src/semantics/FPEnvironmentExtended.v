From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".


From VFP Require Import CommonSSRLemmas
                        CommonStringLemmas
                        BasicTypes
                        FPNavigationMode
                        FlightPlanGeneric
                        FlightPlanExtended
                        FPEnvironmentGeneric.

Local Open Scope list_scope.
Local Open Scope string_scope.
Import ListNotations.
Require Import FunInd.

Set Implicit Arguments.

(** * Environement for the Fligth Plan Extended *)

Module FPE_ENV (EVAL_Def: EVAL_ENV).
  Import FP_E.
  (** Environment for the semantics of the extended flight plan *)

  (** State *)
  Record fp_state := mk_fp_state {
    nav_block: block_id;
    nav_stage: stage_id;
    last_block: block_id;
    last_stage: stage_id;
  }.

  (** ** Environment *)

  Record fp_env := create_fp_env {
    get_state: fp_state;
    get_trace_env: fp_trace;
  }.
  Definition mk_fp_env idb ids lidb lids t :=
    create_fp_env (mk_fp_state idb ids lidb lids) t.

  (** Getter functions of fp_env *)
  Definition get_nav_block_env e := nav_block (get_state e).
  Arguments get_nav_block_env _ /.
  Definition get_nav_stage e := nav_stage (get_state e).
  Arguments get_nav_stage _ /.
  Definition get_last_block e := last_block (get_state e).
  Arguments get_last_block _ /.
  Definition get_last_stage e := last_stage (get_state e).
  Arguments get_last_stage _ /.

  Module Def <: FP_ENV_DEF FP_E.Def FP_E.Common EVAL_Def.
    Definition fp_env := fp_env.
    Definition get_trace := get_trace_env.
    Definition get_nav_block := get_nav_block_env.
    Arguments get_nav_block _ /.

    Definition change_trace (e: fp_env) (t: fp_trace): fp_env :=
      create_fp_env (get_state e) t.

    Definition app_trace (e: fp_env) (t: fp_trace): fp_env :=
      create_fp_env (get_state e) ((get_trace e) ++ t)%list.

    Definition evalc (e: fp_env) (c: c_cond): (bool * fp_env) :=
      let b := EVAL_Def.eval (get_trace e) c in
      (b, app_trace e ((COND c b) :: nil)).

    (** Changement of block. This function updates the id of the current
    block and the stage to execute. If the nav_block is the same, then
    the last stage and block are not updated. *)
    Definition change_block (fp: flight_plan) (e: fp_env)
                              (new_id: block_id): fp_env :=
      let nav_block := get_nav_block e in
      let nav_stage := get_nav_stage e in
      let last_block := get_last_block e in
      let last_stage := get_last_stage e in
      let t := get_trace e in
      if (nav_block =? new_id)%nat then
        mk_fp_env (normalise_block_id fp new_id) 0 last_block last_stage t
      else
        mk_fp_env (normalise_block_id fp new_id) 0 nav_block nav_stage t.

    Definition normalise (fp: flight_plan) (e: fp_env): fp_env :=
      let nav_block := get_nav_block e in
      let nav_stage := get_nav_stage e in
      let last_block := get_last_block e in
      let last_stage := get_last_stage e in
      let t := get_trace e in
      mk_fp_env (normalise_block_id fp nav_block) nav_stage
                last_block last_stage t.

    Definition get_current_block (fp: flight_plan) (e:fp_env): fp_block := 
      (get_block fp (get_nav_block e)).

    Definition get_local_exceptions (fp: flight_plan)
                            (e: fp_env): fp_exceptions :=
      get_block_exceptions (get_current_block fp e).

    Definition init_env (fp: flight_plan): fp_env :=
      mk_fp_env 0 0 0 0 nil.

    Definition initial_env (fp: flight_plan) (e: fp_env): Prop :=
      init_env fp = e.

    Definition full_normalise (fp: flight_plan) (e: fp_env): fp_env :=
      let nav_block := get_nav_block e in
      let nav_stage := get_nav_stage e in
      let last_block := get_last_block e in
      let last_stage := get_last_stage e in
      let t := get_trace e in
      mk_fp_env (normalise_block_id fp nav_block) nav_stage
                (normalise_block_id fp last_block) last_stage t.

  End Def.
  Import Def.

  Definition get_current_stage (fp:flight_plan) (e: fp_env): fp_stage :=
    get_stage fp (get_nav_block e) (get_nav_stage e).

  Definition get_current_stages fp e :=
    drop (get_nav_stage e) (FP_E.get_stages fp (get_nav_block e)).

  (** ** Getter for set *)

  Definition get_c_set (params: fp_params_set): fp_event := 
    let (name, value) := params in
      C_CODE (name ++ " = " ++ value).

  Definition last_wp_exec (m: fp_navigation_mode): fp_event :=
    match m with
    | GO p _ _ => C_CODE ("last_wp = " ++ (string_of_nat (get_go_wp p)))
    | _ => SKIP
    end.

  (** ** Update function for fp_env *)

  (** Add [new_stages] as the current stage to execute. *)
  Definition update_nav_block (e: fp_env) (new_block: block_id): fp_env :=
    mk_fp_env new_block
              (get_nav_stage e)
              (get_last_block e)
              (get_last_stage e)
              (get_trace e).

  (** Add [new_stages] as the current stage to execute. *)
  Definition update_last_block (e: fp_env) (new_block: block_id): fp_env :=
    mk_fp_env (get_nav_block e)
              (get_nav_stage e)
              new_block
              (get_last_stage e)
              (get_trace e).

  (** Add [new_stages] as the current stage to execute. *)
  Definition update_nav_stage (e: fp_env) (new_stage: stage_id): fp_env :=
    mk_fp_env (get_nav_block e)
              new_stage
              (get_last_block e)
              (get_last_stage e)
              (get_trace e).

  (** Add [new_stages] as the last stage to execute. *)
  Definition update_last_stage (e: fp_env) (new_stage: stage_id): fp_env :=
    mk_fp_env (get_nav_block e)
              (get_nav_stage e)
              (get_last_block e)
              new_stage
              (get_trace e).

  (** Compute the next stage *)
  Definition next_stage (e: fp_env) :=
    update_nav_stage e ((get_nav_stage e) + 1).

  (** Return. This function updates the environment with the last block
      saved. If reset is set, then the stage are reset in the other case,
      last_stage is returned. *)
  Definition return_block (fp: flight_plan) (e: fp_env) (reset: bool): fp_env :=
    let last_block := get_last_block e in
    let last_stage := get_last_stage e in
    let t := get_trace e in
    let new_stages :=
      if reset then 0
              else last_stage in
    mk_fp_env last_block new_stages last_block last_stage t.

  (* Set the nav_stage to the default stage id *)
  Definition reset_stage (fp: flight_plan) (e: fp_env): fp_env :=
    update_nav_stage e (default_stage_id fp (get_nav_block e)).

  Lemma next_stage_unchange_block:
    forall e e',
     next_stage e = e' -> Def.get_nav_block e = Def.get_nav_block e'.
  Proof.
    move => e e' H; by rewrite -H.
  Qed.

  Lemma next_stage_incr_stage:
    forall e e',
      next_stage e = e' -> get_nav_stage e + 1 = get_nav_stage e'.
  Proof.
    move => e e' H; by rewrite -H.
  Qed.

  Lemma stage_lt_default:
    forall fp e,
      FP_E_WF.wf_fp_e fp
      -> get_current_stage fp e <> FP_E.default_stage fp (get_nav_block e)
      -> get_nav_stage e < default_stage_id fp (get_nav_block e).
  Proof.
    move => fp [[idb ids lb ls] t];
    rewrite /get_current_stage /get_nav_stage /get_nav_block //=.
    ssrlia. apply FP_E_WF.stage_wf_lt_default.
  Qed.

  (** ** Properties about the changement of block *)

  Lemma update_block_nc:
    forall e,
      update_nav_block e (get_nav_block e) = e.
  Proof. by destruct e as [[] t]. Qed.

  Lemma update_block_nc_stage:
    forall e e' ids,
      update_nav_block e ids = e' 
      -> get_nav_stage e = get_nav_stage e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hs.
  Qed.

  Lemma update_block_change_block:
    forall e e' idb,
    update_nav_block e idb = e' 
    -> get_nav_block e' = idb.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma get_nav_block_update:
    forall e idb,
    get_nav_block (update_nav_block e idb) = idb.
  Proof.
    move => [[idb ids lb ls] t] id //.
  Qed.

  Lemma fold_nav_block:
    forall e,
      nav_block (get_state e) = get_nav_block e.
  Proof. by []. Qed.

  Lemma fold_nav_stage:
    forall e,
      nav_stage (get_state e) = get_nav_stage e.
  Proof. by []. Qed.

  Lemma update_block_nc_last_block:
    forall e e' ids,
      update_nav_block e ids = e' 
      -> get_last_block e = get_last_block e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hls.
  Qed.

  Lemma update_block_nc_last_stage:
    forall e e' ids,
      update_nav_block e ids = e' 
      -> get_last_stage e = get_last_stage e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma update_block_nc_trace:
    forall e e' ids,
      update_nav_block e ids = e' 
      -> get_trace e = get_trace e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  (** ** Properties about the changement of stage *)

  Lemma update_stage_nc_block:
    forall e e' ids,
    update_nav_stage e ids = e' 
    -> get_nav_block e = get_nav_block e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hb.
  Qed.

  Remark update_stage_id:
    forall e ,
    update_nav_stage e (get_nav_stage e) = e.
  Proof. by destruct e as [[] t]. Qed.

  Lemma update_stage_nc_last_block:
    forall e e' ids,
    update_nav_stage e ids = e' 
    -> get_last_block e = get_last_block e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma update_stage_nc_last_stage:
    forall e e' ids,
    update_nav_stage e ids = e' 
    -> get_last_stage e = get_last_stage e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma update_stage_change_stage:
    forall e e' ids,
    update_nav_stage e ids = e' 
    -> get_nav_stage e' = ids.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hs.
  Qed.

  (** ** Properties about the changement of last_block *)

  Lemma update_last_block_nc_block:
    forall e e' ids,
    update_last_block e ids = e' 
    -> get_nav_block e = get_nav_block e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hb.
  Qed.

  Lemma update_last_block_change_last_block:
    forall e e' ids,
    update_last_block e ids = e' 
    -> get_last_block e' = ids.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma update_last_block_nc_last_stage:
    forall e e' ids,
    update_last_block e ids = e' 
    -> get_last_stage e = get_last_stage e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma update_last_block_nc_stage:
    forall e e' ids,
    update_last_block e ids = e' 
    -> get_nav_stage e = get_nav_stage e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hs.
  Qed.

  Lemma update_last_block_nc_trace:
    forall e e' ids,
    update_last_block e ids = e' 
    -> get_trace e = get_trace e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

    (** ** Properties about the changement of last stage *)

    Lemma update_last_stage_nc_block:
    forall e e' ids,
    update_last_stage e ids = e' 
    -> get_nav_block e = get_nav_block e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hb.
  Qed.

  Lemma update_last_stage_nc_last_block:
    forall e e' ids,
    update_last_stage e ids = e' 
    -> get_last_block e = get_last_block e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma update_last_stage_nc_stage:
    forall e e' ids,
    update_last_stage e ids = e' 
    -> get_nav_stage e = get_nav_stage e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by rewrite Hs.
  Qed.

  Lemma update_last_stage_change_last_stage:
    forall e e' ids,
    update_last_stage e ids = e' 
    -> get_last_stage e' = ids.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  Lemma update_last_stage_nc_trace:
    forall e e' ids,
    update_last_stage e ids = e' 
    -> get_trace e = get_trace e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] id H.
    injection H as Hb Hs Hls Hlb. by subst.
  Qed.

  (** Properties with the evolution of the trace *)
  Record unchanged_position (e e': fp_env) := mk_unchanged_position {
    unchanged_nav_block: get_nav_block e = get_nav_block e';
    unchanged_nav_stage: get_nav_stage e = get_nav_stage e';
    unchanged_last_block: get_last_block e = get_last_block e';
    unchanged_last_stage: get_last_stage e = get_last_stage e';
    }.

  Lemma change_trace_unchanged_position:
    forall e e' t,
    change_trace e t = e'
    -> unchanged_position e e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] T.
    rewrite /change_trace //= => H.
    by injection H.
  Qed.

  Lemma app_trace_unchanged_position:
    forall e t,
      unchanged_position e (app_trace e t ).
  Proof.
    move => [[idb ids lb ls] t] T.
    rewrite /app_trace //= => H.
  Qed.

  Lemma app_trace_nil:
    forall e,
      e = app_trace e [::].
  Proof.
    move => [s t]. rewrite /app_trace /=. f_equal.
    apply app_nil_end.
  Qed.
  
  Lemma evalc_unchanged_position:
    forall e e' c b,
    evalc e c = (b, e') -> unchanged_position e e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] c b.
    rewrite /evalc //= => He.
    destruct (EVAL_Def.eval t c); by injection He.
  Qed.

  Lemma update_stage_unchanged_block:
    forall e e' c,
     unchanged_position (update_nav_stage e c) e' 
     -> get_nav_block e = get_nav_block e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] c.
    rewrite /update_nav_stage //= => H.
    apply (unchanged_nav_block H).
  Qed.

  Lemma drop_get_current_stage:
    forall fp s stages_e n e,
      ((s :: stages_e) ++ [:: FP_E.DEFAULT n])%SEQ =
            drop (get_nav_stage e) 
                  (FP_E.get_stages fp (get_nav_block e))
      -> get_current_stage fp e = s.
  Proof.
    rewrite /get_current_stage 
            /get_stage
    => fp s stages n [[idb ids lidb lids] t] //= H.
    remember (default_stage fp _) as d.
    by apply drop_impl_nth with (d := d) in H.
  Qed.

  Lemma change_trace_trans_next_stage:
    forall e t,
      change_trace (next_stage e) t = next_stage (change_trace e t).
  Proof.
    move => e t; destruct e; by [].
  Qed.

  Lemma evalc_change_trace_refl:
    forall e e' c b,
    evalc e c = (b, e') -> change_trace e (get_trace e') = e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] c b.
    rewrite /evalc /change_trace //= => H.
    remember (EVAL_Def.eval t c) as b';
      injection H; intros; by subst idb' ids' lb' ls' t'.
  Qed.

  Lemma app_trace_get_current_stage:
    forall fpe e t,
      get_current_stage fpe e
        = get_current_stage fpe (Def.app_trace e t).
  Proof.
    rewrite /Def.app_trace
            /get_current_stage
    => fpe [[idb ids lidb lids] t] t'//.
  Qed.

  Lemma next_stage_app_trace:
    forall e t,
      next_stage (app_trace e t) = app_trace (next_stage e) t.
  Proof.
    move => e t. by destruct e.
  Qed.

  Lemma mk_env_app_trace:
    forall idb stages lidb lstages t t' t'',
      app_trace (mk_fp_env idb stages lidb lstages (t ++ t')%list) t''
        = app_trace (mk_fp_env idb stages lidb lstages t) (t' ++  t'')%list.
  Proof.
    intros. by rewrite /app_trace //= app_assoc.
  Qed.
  
End FPE_ENV.
