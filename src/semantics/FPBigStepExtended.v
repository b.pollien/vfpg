From Coq Require Import Arith ZArith Psatz Bool String List Program.Equality Program.Wf Program FunInd Program.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import CommonSSRLemmas
                        BasicTypes FPNavigationMode FPNavigationModeSem
                        FlightPlanGeneric FlightPlanExtended
                        CommonFPDefinition
                        FPBigStepGeneric
                        FPEnvironmentGeneric FPEnvironmentDef.

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.

Set Implicit Arguments.

(** * Definition of the big step semantics for the extended flight plan. *)

Module FPE_BIGSTEP (EVAL_Def: EVAL_ENV)
                   (ENVS_Def: ENVS_DEF EVAL_Def).
  Import EVAL_Def FP_E_WF ENVS_Def.FPS_ENV
           FPE_ENV FPE_ENV.Def FPE_ENV.
  Module Import Common_Sem :=
    COMMON_SEM FP_E_WF.Def EVAL_Def FP_E_WF.Common FPE_ENV.Def.

  Section FlightPlan.

  (** Definition of the flight plan being executed *)
  Variable fp_wf: flight_plan_wf.

  Definition fp := proj1_sig fp_wf.
  Definition Hfp := proj2_sig fp_wf.

  (** Last block *)
  Definition default_block := get_default_block_id fp.

  (** ** Run function *)

  (** *** Go to the next block function*)
  Definition next_block (e: fp_env) : fp_env :=
    let nav_block := get_nav_block e in
    (** If it is not the last possible block *)
    if (nav_block <? get_nb_blocks fp - 1) then
      (** Go to the following block *)
      goto_block fp e  (nav_block + 1)
    else
      (** Go to the last possible block *)
      goto_block fp e nav_block.

  (** ** Stages Semantics *)

  (** All the semantics function takes return the new environment *)
  (** and a boolean if the execution must continue.                 *)
  Definition fp_trace_sem: Set := (bool * fp_env).
  Definition continue(e: fp_env): fp_trace_sem :=
    (true, e).
  Definition break (e: fp_env): fp_trace_sem :=
    (false, e).

  (** We note e the environment of the current stage being executed.  *)

  (** *** While *)
  Definition while_sem (e:fp_env) (params: params_while):
                                                        fp_trace_sem :=
    let cond := get_while_cond params in
    (** Evalutation of the while condition *)
    let '(b, e) := evalc e cond in
    (if b
    then (** Restart while loop *)
      (** Go to the next stage and break *)
      let e' := next_stage e in
      break (app_trace e' (init_stage :: nil))
    else (** While loop end *)
      (** Go to the stage after the end *)
      let e' := update_nav_stage e ((get_end_while_id params) + 1) in
      continue e').

  Definition end_while_sem (e: fp_env)
                            (params: params_while): fp_trace_sem :=
    let e' := update_nav_stage e (get_while_id params) in
    while_sem e' params.

  (** *** Set *)
  Definition set_sem (e: fp_env) (params: fp_params_set): fp_trace_sem :=
    (** Execute the set stage and continue the execution *)
    let e' := app_trace e ((get_c_set params) :: init_stage :: nil) in
    continue (next_stage e').

  (** *** Call *)
  Definition call_sem (e:fp_env) (params: fp_params_call):
                                                          fp_trace_sem :=
    let function := get_call_fun params in
    let e' := next_stage e in
    if get_call_loop params then
      (** Loop is enable *)
      (** Execution of the function *)
      let '(b, e) := evalc e function in
      let e' := change_trace e' (get_trace e) in
      if b then
        (** The function returns true, the stage continue *)
        (** Verify if the option until is enable *)
        match get_call_until params with
        (** Option until not enabled, the stage break*)
        | None =>
          break e
        | Some cond =>
          (** Option until enabled *)
          (** Evaluate the condition of the until.
              Jump to the next stage if the conditions is true. *)
          let '(b', e) := evalc e cond in
          let e' := change_trace e' (get_trace e) in
          if b' then
            break (app_trace e' (init_stage :: nil))
          else
            break e
        end
      else
        (** The function returns false, the stage end *)
        (** Break if the option is enabled *)
        let e'' := app_trace e' (init_stage :: nil) in
        if get_call_break params then
          break e''
        else continue e''
    else
      (** Loop not enable *)
      (** Execute the function and break if it is enabled *)
      let e'' := app_trace e' ((C_CODE function) :: init_stage :: nil) in
      if get_call_break params then
        break e''
      else
        continue e''.

  (** *** Deroute *)
  Definition deroute_sem (e:fp_env) (id: block_id): fp_trace_sem :=
    (** Go to the next block *)
    let e' := next_stage e in
    let e'' := app_trace e' (init_stage :: nil) in
    (** Update the environment to jump to the derouted block *)
    break (goto_block fp e'' id).

  (** *** Return *)
  Definition return_sem (e:fp_env) (reset: bool): fp_trace_sem :=
    (** Return to the last block and reset the stages if the
        option is enabled *)
    let e' := return_block fp e reset in
    break (app_trace e' ((Common.on_exit fp (get_nav_block e)) ++ [:: reset_time, init_stage & (Common.on_enter fp (get_last_block e))])).

  (** *** Generic Nav *)

  (** Semantics for the init nav stage *)
  Definition nav_init_sem (e: fp_env) (nav: fp_navigation_mode):
                                            fp_trace_sem :=
    let e' := next_stage e in
    let e'' := app_trace e' ((init_code_nav_sem nav) :: init_stage :: nil)
    in break e''.

  (** Semantics the nav code  *)
  Definition nav_code_sem (e: fp_env)
                          (nav: fp_navigation_mode)
                          (until: option c_cond): fp_trace_sem :=
    let code := gen_fp_nav_code_sem nav in
    let e := app_trace e code in
    let post_out := (post_call_nav_sem nav) :: nil in
    (** The stage has a until condition?*)
    match until with
    | Some u =>
        (** Evaluate the condition and jump to the next stage
            if the condition is true *)
        let '(b, e) := evalc e u in
        if b then
          let e' := app_trace e (post_out ++ (init_stage :: nil)) in
          break (next_stage e')
        else 
          let e' := app_trace e post_out in
          break e'
    | None =>
        let e' := app_trace e post_out in
        break e'
    end.

  Definition nav_sem (e:fp_env) (nav: fp_navigation_mode)
                        (until: option c_cond): fp_trace_sem :=
    (** Pre call code for the nav functions *)
    let pre := pre_call_nav_sem nav in
    let e := app_trace e (pre :: nil) in
    (** The navigation function has a condition to stop the execution *)
    match nav_cond_sem nav with
    | Some cond =>
      (** Compute the condition *)
      let '(b, e) := evalc e cond in
      if b then
        (** The condition return true,
              break and go to the next stage *)
        let e' := app_trace e  ((post_call_nav_sem nav) 
                            :: (last_wp_exec nav) :: init_stage :: nil)
        in break (next_stage e')
      else
      (** Condition is false, the navigation code is executed *)
      nav_code_sem e nav until
    | None =>
      (** No condition. Execute the navigation code and
          verify if there are an until option. *)
      nav_code_sem e nav until
    end.

  Definition default_sem (e:fp_env): fp_trace_sem :=
    (** End of the block -> Go to the next block *)
    let e := reset_stage fp e in
    break (next_block e).

  (** ** Main run function *)
  Definition measure (e: fp_env) :=
    (default_stage_id fp (get_nav_block e)) - (get_nav_stage e).

  Lemma next_stage_incr_measure:
    forall e e',
      get_current_stage fp e <> FP_E.default_stage fp (get_nav_block e)
      -> next_stage e = e' 
      -> (measure e' < measure e)%coq_nat.
  Proof.
    rewrite /measure => e e' Hs He.
    rewrite -(next_stage_incr_stage He) -(next_stage_unchange_block He).
    have  H: get_nav_stage e < default_stage_id fp (get_nav_block e).
    { apply (stage_lt_default Hfp Hs). }
    to_nat H; ssrlia.
  Qed.

  Lemma up_trace_next_stage_incr_measure:
    forall e e' t,
      get_current_stage fp e <> FP_E.default_stage fp (get_nav_block e)
      -> change_trace (next_stage e) t = e'
      -> (measure e' < measure e)%coq_nat.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] T Hd.
    rewrite /measure /change_trace /next_stage //= => He.
    injection He as Hb Hs Hlb Hls Ht.
    rewrite -Hs -Hb.
    rewrite //= in Hd.
    have H : ids < default_stage_id fp idb.
    { ssrlia; apply stage_wf_lt_default. apply Hfp. apply Hd. }
    to_nat H; ssrlia.
  Qed.

  Lemma app_trace_eq_measure:
    forall e e' t,
      app_trace e t = e'
      -> measure e = measure e'.
  Proof.
    move => [[idb ids lb ls] t] [[idb' ids' lb' ls'] t'] t''.
    rewrite /measure /change_trace /next_stage //= => He.
    injection He as Hb Hs Hlb Hls Ht. by subst.
  Qed.

  Lemma nav_stage_lt_measure:
  forall e e',
    get_current_stage fp e <> FP_E.default_stage fp (get_nav_block e)
    -> get_nav_block e = get_nav_block e'
    -> get_nav_stage e < get_nav_stage e'
    -> (measure e' < measure e)%coq_nat.
  Proof.
    rewrite /measure => e e' Hs Heq Hlt.
    have  H: get_nav_stage e < default_stage_id fp (get_nav_block e).
    { apply (stage_lt_default Hfp Hs). }
    rewrite -Heq. to_nat H; to_nat Hlt; ssrlia.
  Qed.

  Definition run_stage (e: fp_env): fp_trace_sem :=
    match get_current_stage fp e with
    | WHILE _ params => while_sem e params
    | END_WHILE _ params _ => end_while_sem e params
    | SET _ params => set_sem e params
    | CALL _ params => call_sem e params
    | DEROUTE _ {|get_deroute_block_id := id|} =>
      deroute_sem e id
    | RETURN _ reset => return_sem e reset
    | NAV_INIT _ params => nav_init_sem e params
    | NAV _ params until => nav_sem e params until
    | DEFAULT _ => default_sem e
    end.


  Function run_step (e: fp_env) {measure measure e}: fp_env :=
    match run_stage e with
    | (true, e') => run_step e'
    | (false, e') => e'
    end.
  Proof.
  rewrite /run_stage => e b e' Hb; subst b.
  destruct (get_current_stage fp e) 
    as[id p | id p | id p| id p | ids p| ids p| ids p| id p|] eqn:s.
  (* While *)
  - rewrite /while_sem => Hs.
    destruct (evalc e (get_while_cond p)) as [b1 e1] eqn:Hd.
    apply evalc_unchanged_position in Hd; case b1 in Hs; try by [].
    injection Hs as He. apply nav_stage_lt_measure.
    * intro H; by rewrite H in s.
    * rewrite (unchanged_nav_block Hd);
        apply (update_stage_nc_block He).
    * destruct e as [[idb ids' lb ls] t]. rewrite //=.
      rewrite /get_current_stage //= in s.
      apply (get_wf_while Hfp) in s; destruct s as [Hw Hew].
      rewrite -He /=; simpl in Hew; ssrlia.

  (* End while *)
  - unfold end_while_sem, while_sem.
    move => Hs.
    destruct
      (evalc (update_nav_stage e (get_while_id p)) (get_while_cond p)) as
      [b1 e1] eqn:Hd.
    apply evalc_unchanged_position in Hd.
    case b1 in Hs; injection Hs as He; try by [].
    apply nav_stage_lt_measure.
    * intro H; by rewrite H in s. 
    * rewrite <- (update_stage_nc_block He).
      apply (update_stage_unchanged_block Hd).
    * destruct e as [[idb ids lb ls] t]; rewrite //= in s.
      apply Hfp in s. destruct s as [s Hids];
      rewrite -He s /update_nav_stage //=; ssrlia.

  (* Set *)
  - rewrite /set_sem => Hs. injection Hs as He.
    have H: get_current_stage fp
          (app_trace e [:: get_c_set p; init_stage]) <> 
               default_stage fp (get_nav_block e).
    { rewrite -app_trace_get_current_stage => H. by rewrite H in s. }
    apply (next_stage_incr_measure H); try by [].

  (* Call *)
  - move => Hs; rewrite /call_sem in Hs.
    have H: get_current_stage fp e <> 
                default_stage fp (get_nav_block e).
    { move => H; by rewrite H in s. }
    case (get_call_loop p) in Hs.
    - destruct (evalc e (get_call_fun p)) as [[] e1]; try by [].
      * case (get_call_until p) in Hs; rewrite //= in Hs; try by [];
        case (eval (get_trace e1) c) as [] in Hs; try by [].
      * case (get_call_until p) in Hs; rewrite //= in Hs; try by [];
          case (get_call_break p) in Hs; try by [];
          injection Hs as He;
          apply (up_trace_next_stage_incr_measure H He).
    - case (get_call_break p) in Hs; try by [];
      injection Hs as He.
      apply (up_trace_next_stage_incr_measure H He).

  (* Deroute *)
  - rewrite /deroute_sem => Hid. destruct p as [block id].
    destruct (goto_block fp _ id) in Hid; by [].

  (* Return *)
  - by [].

  (* Nav Init *)
  - by [].

  (* Nav *)
  - move => Hs; rewrite /nav_sem in Hs.
    case (nav_cond_sem p) in Hs.
    * destruct (evalc _ c) as [[] e1]; try by [].
      rewrite /nav_code_sem in Hs. case until in Hs; try by [].
      destruct (evalc _ c0) as [[] e2]; try by [].
    * rewrite /nav_code_sem in Hs. case until in Hs; try by [].
      destruct (evalc _ c) as [[] e1]; try by [].

  (* Default *)
  - rewrite /default_sem. destruct (next_block _) as [o' e'']. by [].
  Qed.

  (** ** step function *)

  Definition step (e: fp_env): fp_env := 
    match exception fp e with
    | (true, e') => e'
    | (false, e') =>
      let e'' := run_step e' in
      let post := get_code_block_post_call (get_current_block fp e')
      in
        app_trace e'' post
    end.

  (** Lemma About Big Step *)

  Theorem exists_result:
    forall e,
      exists e', step e = e'.
  Proof.
    rewrite /step => e.
    destruct (exception fp e) as [[] e'].

    (* Exception raised *)
    by exists e'.

    (* Exception not raised *)
    remember (get_code_block_post_call _) as post.
    by exists (app_trace (run_step e') post).
  Qed.

  (** Properties about local exception *)
  Lemma get_block_default_block:
    forall e1,
      FP_E.get_nb_blocks fp - 1 <= get_nav_block e1
      -> FP_E.get_block fp (get_nav_block e1) = get_default_block fp.
  Proof.
    rewrite /FP_E.get_block => e1 Hlt.
    rewrite nth_overflow //.
    rewrite /FP_E.get_nb_blocks in Hlt. to_nat Hlt. ssrlia.
  Qed.

  Lemma get_current_default_block:
    forall e1,
      FP_E.get_nb_blocks fp - 1 <= get_nav_block e1
      -> get_current_block fp e1 = get_default_block fp.
  Proof.
    rewrite /get_current_block. apply get_block_default_block.
  Qed.

  Lemma local_exceptions_raised_in_block:
    forall e1 e1',
      test_exceptions fp e1 (get_local_exceptions fp e1)
        = (true, e1')
      -> get_nav_block e1 < FP_E.get_nb_blocks fp - 1.
  Proof.
    move => e1 e1' Hexl.
    destruct (le_lt_dec (FP_E.get_nb_blocks fp - 1) (get_nav_block e1))
      as [Hge | Hlt]; try (to_nat Hlt; ssrlia).

    have H : get_local_exceptions fp e1 = nil.
    { rewrite /get_local_exceptions get_current_default_block //=.
      to_nat Hge. ssrlia. }

    rewrite H in Hexl. inversion Hexl.
  Qed.

  Lemma no_local_exception_default_block:
    forall e1 e1',
      (FP_E.get_nb_blocks fp) - 1 <= get_nav_block e1
      -> test_exceptions fp e1 (get_local_exceptions fp e1)
                = (false, e1')
      -> e1 = e1'.
  Proof.
    rewrite /get_local_exceptions => e1 e1' Hlt Hex.
    rewrite get_current_default_block //= in Hex => //.
    by inversion Hex.
  Qed.

  (** Default sem break *)
  Lemma default_sem_no_fp_trace:
    forall e e' b ,
     default_sem e = (b, e')
     -> b = false.
  Proof.
    move => e e' b Hd.
    rewrite /default_sem in Hd.
    destruct (next_block (reset_stage fp e)). by inversion Hd.
  Qed. 

  (** Run continue stage does not modify block *)
  Lemma run_continue_stage_unchange_block:
    forall e1 e1',
      run_stage e1 = (true, e1')
      -> get_nav_block e1 = get_nav_block e1'.
  Proof.
    rewrite /run_stage => e1 e1'; destruct e1, e1'.
    destruct (get_current_stage _ _); rewrite //=.

    (** While sem*)
    * rewrite /while_sem.
      destruct (evalc _ _) as [[] e ] eqn:He; rewrite //=.
      rewrite /continue => H. inversion H.
      apply (unchanged_nav_block (evalc_unchanged_position He)).

    (** End while sem*)
    * rewrite /end_while_sem /while_sem.
      destruct (evalc _) as [[] e ] eqn:He; rewrite //=.
      rewrite /continue => H. inversion H.
      apply (unchanged_nav_block (evalc_unchanged_position He)).

    (** Set sem*)
    * rewrite /set_sem //=.
       rewrite /continue => H. by inversion H.

    (** Call sem *)
    * rewrite /call_sem. destruct (get_call_loop params).
      - destruct (evalc _) as [[] e ] eqn:He; rewrite //=.
        + destruct (get_call_until params) as [c |].
          ** destruct (eval _ _) as [] eqn:He'; rewrite //=.
          ** rewrite /break => H. inversion H.
        + destruct (get_call_break params).
           ** rewrite /break => H; inversion H.
           ** rewrite /change_trace /next_stage //= => H. by inversion H.
      - destruct (get_call_break params).
        + rewrite /break => H; inversion H.
        + rewrite /change_trace /next_stage //= => H. by inversion H.

    (** Deroute sem *)
    * destruct (params); rewrite /deroute_sem /goto_block /next_stage.
       destruct (forbidden_deroute _ _) as [[ ] e];
       rewrite /break => H; inversion H.

    (** Nav sem *)
    * rewrite /nav_sem. destruct (nav_cond_sem nav_mode).
      - destruct (evalc _) as [[] e ] eqn:He; rewrite //=.
        rewrite /nav_code_sem /change_trace /next_stage //=;
          destruct until as [c' |].
        + destruct (eval _ _) as [] eqn:He'; rewrite //=.
        + rewrite /break => H; inversion H.
      - rewrite /nav_code_sem. destruct until as [c' |].
        + destruct (evalc _ c') as [[] e' ] eqn:He'; rewrite //=.
        + rewrite /break => H; inversion H.
  Qed.

  Lemma get_default_stage_drop:
    forall fpe idb ids,
      let stages := FP_E_WF.get_stages fpe idb in
      let default := FP_E_WF.default_stage fpe idb in
      FP_E_WF.wf_fp_e fpe
      -> nil = drop ids (take (Datatypes.length stages).-1 stages)
      -> List.nth ids stages default = default.
  Proof.
    move => fpe idb ids stages default Hwf Hd.
    symmetry in Hd. apply drop_oversize' in Hd.
    rewrite -size_eq_length seq.size_takel /stages in Hd.

    have H := ids_ge_default.
    rewrite /FP_E.get_stage in H.
    apply (H fpe idb ids Hwf); to_nat Hd; ssrlia.

    rewrite size_eq_length; ssrlia.
  Qed. 

  Lemma get_stage_drop:
    forall fpe idb ids stage stages',
      let stages := FP_E_WF.get_stages fpe idb in
      let default := FP_E_WF.default_stage fpe idb in
      FP_E_WF.wf_fp_e fpe
      -> stage :: stages' 
          = drop ids (take (Datatypes.length stages).-1 stages)
      -> List.nth ids stages default = stage.
  Proof.
    move => fpe idb ids stage stages' stages default Hwf Hd.
    have H := drop_take_not_empty Hd.
    apply drop_impl_nth with (d := default) in Hd.
    rewrite list_nth_take in Hd. by symmetry.
    ssrlia.
  Qed.

  End FlightPlan.

  Definition initial_env_wf (fp: flight_plan_wf) (e: fp_env): Prop :=
    initial_env (` fp) e.

  Definition semantics_fpe (fp: flight_plan_wf) : fp_semantics := 
    FP_Semantics_gen ((step_prop step) fp) (initial_env_wf fp).

End FPE_BIGSTEP.
