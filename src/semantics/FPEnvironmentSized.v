From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.
Require Import ProofIrrelevance.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".


From VFP Require Import CommonLemmasNat8
                        BasicTypes
                        FlightPlanGeneric
                        FlightPlanExtended
                        FPEnvironmentGeneric
                        FPEnvironmentExtended.

Local Open Scope list_scope.
Local Open Scope string_scope.
Import ListNotations.
Require Import FunInd.

Set Implicit Arguments.

(** * Envrionment for the Flight Plan Sized *)

Module FPS_ENV (EVAL_Def: EVAL_ENV).

  (** Specialization of FPE envs *)
  Module Import FPE_ENV := FPE_ENV EVAL_Def.
  Export FPE_ENV.

  Import FP_E FP_E_WF FPE_ENV.Def.
  (** Environment for the semantics of the sized extended flight plan *)

  (** Definition of the properties that the environment is representable
  on 8bits *)

  Record fp_env_on_8 (e: fp_env):= create_fp_env_on_8 {
    nav_block8: is_nat8 (Def.get_nav_block e);
    nav_stage8: is_nat8 (get_nav_stage e);
    last_block8: is_nat8 (get_last_block e);
    last_stage8: is_nat8 (get_last_stage e);
  }.

  Definition fp_env8 := {e: Def.fp_env | fp_env_on_8 e}.

  Lemma is_nav_stage8:
    forall (e: fp_env8), is_nat8 (get_nav_stage (` e)).
  Proof.
    move => [e He].
    apply (nav_stage8 He).
  Qed.

  Lemma is_last_stage8:
    forall (e: fp_env8 ), is_nat8 (get_last_stage (` e)).
  Proof.
    move => [e He].
    apply (last_stage8 He).
  Qed.

  Lemma evalc_get_current_stage:
    forall fpe e e' c b,
      Def.evalc e c = (b, e')
      -> get_current_stage fpe e = get_current_stage fpe e'.
  Proof.
    rewrite /Def.evalc
            /get_current_stage
    => fpe [[idb ids lidb lids] t] [[idb' ids' lidb' lids'] t'] c b //=.
    remember (EVAL_Def.eval t c) as b' => //= H.
    injection H as Hb Hidb Hids Hlidb Hlids Ht.
    by rewrite Hidb Hids.
  Qed.

  Lemma app_trace_get_current_stages:
    forall fpe e t,
      get_current_stages fpe e
        = get_current_stages fpe (Def.app_trace e t).
  Proof.
    rewrite /Def.app_trace
            /get_current_stages
    => fpe [[idb ids lidb lids] t] t'//.
  Qed.

  Lemma app_trace_get_current_stage:
    forall fpe e t,
      get_current_stage fpe e
        = get_current_stage fpe (Def.app_trace e t).
  Proof.
    rewrite /Def.app_trace
            /get_current_stage
    => fpe [[idb ids lidb lids] t] t'//.
  Qed.

  Lemma app_trace_get_current_block:
    forall fpe e t,
      get_current_block fpe e 
        = get_current_block fpe (app_trace e t).
  Proof.
    rewrite /get_current_block => fpe [[idb ids lidb lids] t] t' //.
  Qed.

  Lemma init_env_is_8:
    forall fpe, fp_env_on_8 (Def.init_env fpe).
  Proof.
    move => fpe.
    split; rewrite /init_env
                   /is_nat8 /get_nav_stage /get_last_stage //=; try lia.
  Qed.

  Definition init_env (fpe: flight_plan_wf) : fp_env8 :=  
      exist _ (Def.init_env (` fpe)) (init_env_is_8 (` fpe)).

  (** * Matching relation between FPE env and FPS env *)
  Definition match_env (e: fp_env) (e': fp_env8): Prop :=
    e = ` e'.

    Notation "e1 '~env8~' e2 "
    := (match_env e1 e2) (at level 10).

  (** Lemma about equality *)
  Lemma env8_proj:
    forall (e e': fp_env8),
      ` e = ` e'
      -> e = e'.
  Proof.
    move => [e He] [e' He'] H.
    by apply subset_eq_compat.
  Qed.

End FPS_ENV.
