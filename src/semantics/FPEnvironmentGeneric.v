From VFP Require Import BasicTypes
                        FlightPlanGeneric.

Set Implicit Arguments.

(** Generic definition for the FP Environments *)

(** Definition of the function that will evaluate condition *)

Module Type EVAL_ENV.
  Parameter eval: fp_trace -> c_cond -> bool.
End EVAL_ENV.

(** Definition of the environments that will be used by the semantics *)
(** The environment is associated with a type of flight plan*)

Module Type FP_ENV_DEF (G_FP: GENERIC_FP)
                       (FP_SIG: COMMON_FP_FUN_SIG G_FP)
                       (E: EVAL_ENV).
  Import G_FP FP_SIG.
  Parameter fp_env: Set.

  (** Evaluation of a condition in the current env:              *)
  (** Update the the trace of the fp_env and return the result   *)
  Parameter evalc: fp_env -> c_cond -> (bool * fp_env).

  (** Getter for the trace *)
  Parameter get_trace: fp_env -> fp_trace.

  (** Change the trace of the environment *)
  Parameter change_trace: fp_env -> fp_trace -> fp_env.

  (** Add the trace at the end of the current trace of the env *)
  Parameter app_trace: fp_env -> fp_trace -> fp_env.

  (** Getter for the current block_id *)
  Parameter get_nav_block: fp_env -> block_id.

  (** Change the block being executed *)
  Parameter change_block: flight_plan -> fp_env -> block_id -> fp_env.

  (** Normalisation of the block id *)
  Parameter normalise: flight_plan -> fp_env -> fp_env.

  (** Getter for the current blocks *)
  Parameter get_current_block: flight_plan -> fp_env -> fp_block.

  (** Get the local exceptions of the current block *)
  Parameter get_local_exceptions: flight_plan -> fp_env -> fp_exceptions.

  (** Initial environment *)
  Parameter init_env: flight_plan -> fp_env.
  Parameter initial_env: flight_plan -> fp_env -> Prop.
End FP_ENV_DEF.
