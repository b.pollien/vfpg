From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List FunInd Program Program.Equality
                        Program.Wf BinNums BinaryString.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".


From VFP Require Import CommonStringLemmas
                        BasicTypes
                        FPNavigationMode
                        FlightPlanGeneric
                        FlightPlan
                        FPEnvironmentGeneric.

Local Open Scope list_scope.
Local Open Scope string_scope.
Import ListNotations.
Require Import FunInd.

Set Implicit Arguments.

(** * Envrionment for the Flight Plan *)

Module FP_ENV (EVAL_Def: EVAL_ENV).
  Import FP.

  Definition exec_stages := list fp_stage.

  (** Environment for the semantics of the non extended flight plan *)

  (** An environment is composed of a state that contains the information
      about the execution and a trace that is a history of all the
      interactions with the outside world. *)

  (** State *)
  Record fp_state := mk_fp_state {
    nav_block: block_id;
    nav_stages: exec_stages;
    last_block: block_id;
    last_stages: exec_stages;
  }.

  (** ** Environment *)

  Record fp_env := create_fp_env {
    get_state: fp_state;
    get_trace_env: fp_trace;
  }.
  Definition mk_fp_env idb stages lidb lstages t: fp_env :=
    create_fp_env (mk_fp_state idb stages lidb lstages) t.

  (** Getter functions of fp_env *)
  Definition get_nav_block_env e := nav_block (get_state e).
  Arguments get_nav_block_env _ /.
  Definition get_nav_stages e := nav_stages (get_state e).
  Arguments get_nav_stages _ /.
  Definition get_last_block e := last_block (get_state e).
  Arguments get_last_block _ /.
  Definition get_last_stages e := last_stages (get_state e).
  Arguments get_last_stages _ /.

  Module Def <: FP_ENV_DEF FP.Def FP.Common EVAL_Def.
    Definition fp_env := fp_env.
    Definition get_trace := get_trace_env.
    Definition get_nav_block := get_nav_block_env.
    Arguments get_nav_block _ /.

    Definition change_trace (e: fp_env) (t: fp_trace): fp_env :=
      create_fp_env (get_state e) t.

    Definition app_trace (e: fp_env) (t: fp_trace): fp_env :=
      create_fp_env (get_state e) ((get_trace e) ++ t)%list.

    Definition evalc (e: fp_env) (c: c_cond): (bool * fp_env) :=
      let b := EVAL_Def.eval (get_trace e) c in
      (b, app_trace e (COND c b :: nil)).

    (** Changement of block. This function updates the id of the current
        block and the stage to execute. If the nav_block is the same, then
        the last stage and block are not updated. *)
    Definition change_block (fp: flight_plan) (e: fp_env)
                              (new_id: block_id) : fp_env :=
      let nav_block := get_nav_block e in
      let nav_stages := get_nav_stages e in
      let last_block := get_last_block e in
      let last_stages := get_last_stages e in
      let t := get_trace e in
      if (nav_block =? new_id)%nat then
        let new_id := normalise_block_id fp new_id in
        let new_stages := get_stages fp new_id in
        mk_fp_env new_id new_stages last_block last_stages t
      else
        let new_id := normalise_block_id fp new_id in
        let new_stages := get_stages fp new_id in
        mk_fp_env new_id new_stages nav_block nav_stages t.

    Definition normalise (fp: flight_plan) (e: fp_env): fp_env :=
      let nav_block := get_nav_block e in
      let nav_stages := get_nav_stages e in
      let last_block := get_last_block e in
      let last_stages := get_last_stages e in
      let t := get_trace e in
      mk_fp_env (normalise_block_id fp nav_block) nav_stages
                last_block last_stages t.

    Definition get_current_block (fp: flight_plan) (e:fp_env): fp_block := 
      (get_block fp (get_nav_block e)).

    Definition get_local_exceptions (fp: flight_plan)
                                    (e: fp_env): fp_exceptions := 
      get_block_exceptions (get_current_block fp e).

    Definition init_state (fp: flight_plan): fp_state :=
      mk_fp_state 0 (get_stages fp 0) 0 (get_stages fp 0).

    Definition init_env (fp: flight_plan): fp_env :=
      create_fp_env (init_state fp) nil.

    Definition initial_env (fp: flight_plan) (e: fp_env): Prop :=
      init_env fp = e.
  End Def.
  Import Def.


  (** ** Getter for set *)

  Definition get_c_set (params: fp_params_set): fp_event := 
    let (name, value) := params in
      C_CODE (name ++ " = " ++ value).

  Definition last_wp_exec (m: fp_navigation_mode): fp_event :=
    match m with
    | GO p _ _ => C_CODE ("last_wp = " ++ (string_of_nat (get_go_wp p)))
    | _ => SKIP
    end.

  (** ** Update function for fp_env *)

  (** Add [new_stages] to the list of stage to execute. *)
  Definition update_nav_stage (e: fp_env) (new_stages: exec_stages): fp_env :=
    mk_fp_env (get_nav_block e)
              (new_stages ++ (get_nav_stages e))%list
              (get_last_block e)
              (get_last_stages e)
              (get_trace e).

  (** Return. This function updates the environment with the last block
      saved. If reset is set, then the stage are reset in the other case,
      last_stage is returned. *)
  Definition return_block (fp: flight_plan) (e: fp_env) (reset: bool): fp_env :=
    let last_block := get_last_block e in
    let last_stages := get_last_stages e in
    let t := get_trace e in
    let new_stages :=
      if reset then (get_stages fp last_block) 
              else last_stages in
    mk_fp_env last_block new_stages last_block last_stages t.

End FP_ENV.
