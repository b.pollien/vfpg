From Coq Require Import  FunInd Program Program.Equality
                                 Program.Wf.
From VFP Require Import BasicTypes MatchFPwFPE MatchFPSwFPC
                                  FPEnvironmentGeneric FPEnvironmentDef
                                  FPBigStep FPBigStepExtended
                                  FPBigStepSized
                                  FPBigStepClight.
From Coq.PArith Require Import BinPos.


(** ** Modules containing the definition of the FP and FPE big step
  semantics *)

Module Type FP_BS_DEF (EVAL_Def: EVAL_ENV)
                                 (ENVS_Def: ENVS_DEF EVAL_Def).
  Export EVAL_Def ENVS_Def FP_ENV FPS_ENV FPE_ENV.

  (** Match FP to FPE env *)
  Module MATCH_FP_FPE := MATCH_FP_FPE EVAL_Def ENVS_Def.
  Export MATCH_FP_FPE.

  (** Match FP to FPE env *)
  Module MATCH_FPS_C := MATCH_FPS_C EVAL_Def ENVS_Def.

  (** FP Big Step semantics *)
  Module FP_BS := FP_BIGSTEP EVAL_Def ENVS_Def.
  Export FP_BS.

  (** FPS Big Step semantics *)
  Module FPS_BS := FPS_BIGSTEP EVAL_Def ENVS_Def.
  Export FPS_BS FPS_BS.FPE_BS.

  (** Cligth semantics *)
  Module C_BS := C_BIGSTEP.
  Export C_BS.

End FP_BS_DEF.
