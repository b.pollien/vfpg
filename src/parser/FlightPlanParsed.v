From Coq Require Import Arith Psatz Bool String List.
From VFP Require Import BasicTypes.

Local Open Scope string_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.

Set Implicit Arguments.

(** * Definition of an parsed Flight Plan *)
(** an parsed Flight Plan is a flight plan
    just after the parsing. it countains all the
    information but not in the correct format *)

(* header *)
Definition header := string.

(* waypoints *)

Record waypoint := create_waypoint {
    get_waypoint_name : option name;
    get_waypoint_x : option string;
    get_waypoint_y : option string;
    get_waypoint_lat : option string;
    get_waypoint_lon : option string;
    get_waypoint_alt : option string;
    get_waypoint_height : option string;
}.

Definition mk_waypoint := create_waypoint.

Record waypoints := create_waypoints {
    get_waypoints_utm_x0 : option string;
    get_waypoints_utm_y0 : option string;
    get_waypoint_list : list waypoint 
}.

Definition mk_waypoints := create_waypoints.

(* sectors *)

Definition corner := name.

Record sector := create_sector {
    get_sector_name : option name;
    get_sector_color : option string;
    get_sector_type : option string;

    get_sector_corners : list corner
}.

Definition mk_sector := create_sector.

Definition kml := string.

Inductive sector_or_corner :=
| Sector (s : sector)
| Kml (k : kml).

Definition sectors := list sector_or_corner.

(* variables *)

Record abi_binding := create_abi_binding {
    get_abi_binding_name : option name;
    get_abi_binding_vars : option string;
    get_abi_binding_id : option string;
    get_abi_binding_handler : option string;
}.

Definition mk_abi_binding := create_abi_binding.

Record variable := create_variable {
    get_variable_var : option string;
    get_variable_type : option string;
    get_variable_init : option string;
    get_variable_shortname : option string;
    get_variable_min : option string;
    get_variable_max : option string;
    get_variable_step : option string;
    get_variable_unit : option string;
    get_variable_alt_unit : option string;
    get_variable_alt_unit_coef : option string;
    get_variable_values : option string;
}.

Definition mk_variable := create_variable.

Inductive abi_binding_or_variable :=
| Abi_binding (a : abi_binding)
| Var (v : variable).

Definition variables := list abi_binding_or_variable.

(* modules *)

Record define := create_define {
    get_define_name : string;
    get_define_value : option string
}.

Definition mk_define := create_define.

Record configure := create_configure {
    get_configure_name : string;
    get_configure_value : string
}.

Definition mk_configure := create_configure.

Record module := create_module {
    get_module_name : string;
    get_module_type : option string;

    get_module_defines : list define;
    get_module_configures : list configure
}.

Definition mk_module := create_module.

Definition modules := list module.

(* includes *)

Record arg := create_arg {
    get_arg_name : string;
    get_arg_value : string;
}.

Definition mk_arg := create_arg.

Record fpp_with := create_fpp_with {
    get_fpp_with_from : string;
    get_fpp_with_to : string;
}.

Definition mk_fpp_with := create_fpp_with.

Record include := create_include {
    get_include_name : string;
    get_include_procedure : string;
    
    get_include_args : list arg;
    get_include_fpp_withs : list fpp_with
}.

Definition mk_include := create_include.

Definition includes := list include.

(* exception *)

Record exception := create_exception {
    get_exception_cond : option c_cond;
    get_exception_deroute : option block_name;
    get_exception_exec : option c_code
}.

Definition mk_exception := create_exception.

Definition exceptions := list exception.

(* forbidden_deroute *)

Record forbidden_deroute := create_forbidden_deroute {
    get_fbd_cond : option c_cond;
    get_fbd_to : block_name;
}.

Definition mk_fbd := create_forbidden_deroute.

Definition forbidden_deroutes := list forbidden_deroute.

(** ** Stages *)

Record nav_stage_param := create_nav_stage_param {
    get_stage_roll : option string;
    get_stage_pitch : option string;
    get_stage_yaw : option string;
    get_stage_climb : option string;

    get_stage_wp : option string;
    get_stage_wp2 : option string;
    get_stage_wpts : option string;
    get_stage_wp_qdr : option string;
    get_stage_wp_dist : option string;
    get_stage_from : option string;
    get_stage_from_qdr : option string;
    get_stage_from_dist : option string;

    get_stage_course : option course;

    get_stage_radius : option string;
    get_stage_center : option string;
    get_stage_turn_around : option string;

    get_stage_orientation : option string;
    get_stage_grid : option string;

    get_stage_commands : option string;
    get_stage_flags : option string;
    get_stage_ac_id : option string;
    get_stage_distance : option string;

    get_stage_vmode : option string;
    get_stage_hmode : option string;

    get_stage_alt : option string;
    get_stage_height : option string;

    get_stage_throttle : option string;

    get_stage_pre_call : option string;
    get_stage_post_call : option string;

    get_stage_nav_type : option string;

    get_stage_nav_params : option string;

    get_stage_until : option string;

    get_stage_approaching_time : option string;
    get_stage_exceeding_time : option string;
}.

Definition mk_stage_param := create_nav_stage_param.

(** set *)

Record set_stage := create_set_stage {
    get_set_var : string;
    get_set_value : string;
}.

Definition mk_set := create_set_stage.

(** call *)

Record call_stage := create_call_stage {
    get_call_fun : option string;
    get_call_until : option string;
    get_call_loop : option string;
    get_call_break : option string;
}.

Definition mk_call := create_call_stage.

(** call_once *)

Record call_once_stage := create_call_once_stage {
    get_call_once_fun : option string;
    get_call_once_break : option string;
}.

Definition mk_call_once := create_call_once_stage.

(** deroute *)

Definition deroute_stage := string.

Definition mk_deroute (s : string) : deroute_stage := s.

(** return *)

Definition return_stage := string.

Definition mk_return (s : string) : return_stage := s.

(** param *)

Record param_stage := create_param_stage {
    get_param_name : string;
    get_param_default_value : option string;
}.

Definition mk_param := create_param_stage.

Definition while_cond := option string.

Definition mk_while (s : option string) : while_cond := s.

Record for_param := create_for_param {
    get_for_var : option string;
    get_for_from : option string;
    get_for_to : option string;
}.

Definition mk_for := create_for_param.

(** fpp stage *)

Inductive fpp_stage :=
    | Home_stage
    | Heading_stage (param : nav_stage_param)
    | Attitude_stage (param : nav_stage_param)
    | Manual_stage (param : nav_stage_param)
    | Go_stage (param : nav_stage_param)
    | Xyz_stage (param : nav_stage_param)
    | Circle_stage (param : nav_stage_param)
    | Stay_stage (param : nav_stage_param)
    | Follow_stage (param : nav_stage_param)
    | Survey_rectangle_stage (param : nav_stage_param)
    | Eight_stage (param : nav_stage_param)
    | Oval_stage (param : nav_stage_param)
    | Path_stage (param : nav_stage_param)
    | Guided_stage (param : nav_stage_param)
    | Return_stage (rt : return_stage)
    | Deroute_stage (dr : deroute_stage)
    | Set_stage (se : set_stage)
    | Call_stage (ca : call_stage)
    | Call_once_stage (co : call_once_stage)
    | While_stage (whl_param : while_cond) (loop : list fpp_stage)
    | For_stage (fr_param : for_param) (loop : list fpp_stage).

Record block := create_block {
    get_block_name : option block_name;
    get_block_pre_call : option c_code;
    get_block_post_call : option c_code;
    get_block_on_enter : option c_code;
    get_block_on_exit : option c_code;
    get_block_strip_button : option string;
    get_block_strip_icon : option string;
    get_block_group : option string;
    get_block_key : option string;
    get_block_description : option string;

    get_block_exceptions : exceptions;
    get_block_fbd : forbidden_deroutes;
    get_block_stages : list fpp_stage;
}.

Definition mk_block := create_block.

Definition blocks := list block.

Record fpp_info := create_fpp_info {
    get_fpp_name : option name;
    get_fpp_lat0 : option string;
    get_fpp_lon0 : option string;
    get_fpp_max_dist_from_home : option string;
    get_fpp_ground_alt : option string;
    get_fpp_security_height : option string;
    get_fpp_alt : option string;
    get_fpp_wp_frame : option string;
    get_fpp_qfu : option string;
    get_fpp_home_mode_height : option string;
    get_fpp_geofence_max_alt : option string;
    get_fpp_geofence_max_height : option string;
    get_fpp_geofence_sector : option string;
}.

Definition mk_fpp_info := create_fpp_info.

Record fpp_elements := create_fpp_elements {
    get_header :  option header;
    get_waypoints : option waypoints;
    get_sectors : option sectors;
    get_variables : option variables;
    get_modules : option modules;
    get_includes : option includes;
    get_global_exceptions : option exceptions;
    get_blocks : option blocks
}.

Definition mk_fpp_elements := create_fpp_elements.

Record fpp := create_fpp {
    get_fpp_info : fpp_info;
    get_fpp_elements : fpp_elements;
}.

Definition mk_fpp := create_fpp.

(* procedures *)

(* param *)

Record proc_param := create_proc_param {
    get_proc_param_name : string;
    get_proc_param_default_value : option string;
}.

Definition mk_proc_param := create_proc_param.

Definition proc_params := list proc_param.

Record procedure := create_procedure {
    get_proc_params : proc_params ;
    get_proc_header :  option header;
    get_proc_variables : option variables;
    get_proc_waypoints : option waypoints;
    get_proc_sectors : option sectors;
    get_proc_global_exceptions : option exceptions;
    get_proc_blocks : option blocks
}.

Definition mk_procedure := create_procedure.

Inductive Parser_result :=
    | Fpp (Flight_plan : fpp)
    | Proc (prc : procedure)
    | Err (msg : string).