Require Import Ascii List String PeanoNat.
From VFP Require Import Parser CoqLexer FlightPlanParsed.
Import MenhirLibParser.Inter.
Open Scope string_scope.

(** Lexer + Parser function *)

Definition string2fp s :=
  match option_map (Parser.parse_fp 50) (lex_string s) with
  | Some (Parsed_pr f _) => Some f
  | _ => None
  end.