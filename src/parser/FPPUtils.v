From Coq Require Import Arith Psatz Bool String List.
From VFP Require Import BasicTypes 
                        FlightPlanParsed.
Local Open Scope string_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.

(** fpp_info *)

Definition empty_fpp_info := mk_fpp_info None (* name *)
                                         None (* lat0 *)
                                         None (* lon0 *)
                                         None (* max_dist_to_home *)
                                         None (* ground_alt *)
                                         None (* security_height *)
                                         None (* alt *)
                                         None (* wp_frame *)
                                         None (* qfu *)
                                         None (* hmode height *)
                                         None (* geofence max alt *)
                                         None (* geofence max height *)
                                         None (* geofence sector *).

Definition set_fpp_name (x : option name) 
                          (info : fpp_info) :=
  mk_fpp_info x (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info)
                  (get_fpp_geofence_max_height info) 
                  (get_fpp_geofence_sector info).

Definition set_fpp_lat0 (x : option string) 
                          (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  x 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info)
                  (get_fpp_geofence_max_height info) 
                  (get_fpp_geofence_sector info).

Definition set_fpp_lon0 (x : option string) 
                          (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  x
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info)
                  (get_fpp_geofence_max_height info) 
                  (get_fpp_geofence_sector info).

Definition set_fpp_max_dist_from_home (x : option string) 
                                        (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  x 
                  (get_fpp_ground_alt info) 
                  (get_fpp_security_height info)
                  (get_fpp_alt info) 
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info)
                  (get_fpp_geofence_max_height info)
                  (get_fpp_geofence_sector info).

Definition set_fpp_ground_alt (x : option string) 
                                (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  x 
                  (get_fpp_security_height info)
                  (get_fpp_alt info) 
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info)
                  (get_fpp_geofence_max_height info) 
                  (get_fpp_geofence_sector info).

Definition set_fpp_security_height (x : option string) 
                                      (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info) 
                  x
                  (get_fpp_alt info) 
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info)
                  (get_fpp_geofence_max_height info) 
                  (get_fpp_geofence_sector info).

Definition set_fpp_alt (x : option string) 
                          (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  x 
                  (get_fpp_wp_frame info)
                  (get_fpp_qfu info) 
                  (get_fpp_home_mode_height info)
                  (get_fpp_geofence_max_alt info) 
                  (get_fpp_geofence_max_height info)
                  (get_fpp_geofence_sector info).

Definition set_fpp_wp_frame (x : option string) 
                              (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info) 
                  x
                  (get_fpp_qfu info) 
                  (get_fpp_home_mode_height info)
                  (get_fpp_geofence_max_alt info) 
                  (get_fpp_geofence_max_height info)
                  (get_fpp_geofence_sector info).

Definition set_fpp_qfu (x : option string) 
                          (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  x
                  (get_fpp_home_mode_height info)
                  (get_fpp_geofence_max_alt info) 
                  (get_fpp_geofence_max_height info)
                  (get_fpp_geofence_sector info).

Definition set_fpp_home_mode_height (x : option string) 
                                      (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info) 
                  x
                  (get_fpp_geofence_max_alt info) 
                  (get_fpp_geofence_max_height info)
                  (get_fpp_geofence_sector info).

Definition set_fpp_geofence_max_alt (x : option string) 
                                      (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info)  
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  x 
                  (get_fpp_geofence_max_height info)
                  (get_fpp_geofence_sector info).

Definition set_fpp_geofence_max_height (x : option string) 
                                          (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info) 
                  x
                  (get_fpp_geofence_sector info).

Definition set_fpp_geofence_sector (x : option string) 
                                      (info : fpp_info) :=
  mk_fpp_info (get_fpp_name info) 
                  (get_fpp_lat0 info) 
                  (get_fpp_lon0 info)
                  (get_fpp_max_dist_from_home info) 
                  (get_fpp_ground_alt info)
                  (get_fpp_security_height info) 
                  (get_fpp_alt info)
                  (get_fpp_wp_frame info) 
                  (get_fpp_qfu info)
                  (get_fpp_home_mode_height info) 
                  (get_fpp_geofence_max_alt info)
                  (get_fpp_geofence_max_height info) 
                  x.

(** fpp_elements *)

Definition empty_fpp_elements :=
  mk_fpp_elements None (* header *)
                  None (* waypoints *)
                  None (* sectors *)
                  None (* variables *)
                  None (* modules *)
                  None (* includes *)
                  None (* global exceptions *)
                  None (* blocks *).

Definition set_fpp_header (x : option header) 
                            (elements : fpp_elements) :=
  create_fpp_elements
    x
    (get_waypoints elements)
    (get_sectors elements)
    (get_variables elements)
    (get_modules elements)
    (get_includes elements)
    (get_global_exceptions elements)
    (get_blocks elements).

Definition set_fpp_waypoints (x : option waypoints) 
                                (elements : fpp_elements) :=
  create_fpp_elements
    (get_header elements)
    x
    (get_sectors elements)
    (get_variables elements)
    (get_modules elements)
    (get_includes elements)
    (get_global_exceptions elements)
    (get_blocks elements).

Definition set_fpp_sectors (x : option sectors) (elements : fpp_elements) :=
  create_fpp_elements
    (get_header elements)
    (get_waypoints elements)
    x
    (get_variables elements)
    (get_modules elements)
    (get_includes elements)
    (get_global_exceptions elements)
    (get_blocks elements).

Definition set_fpp_variables (x : option variables) (elements : fpp_elements) :=
  create_fpp_elements
    (get_header elements)
    (get_waypoints elements)
    (get_sectors elements)
    x
    (get_modules elements)
    (get_includes elements)
    (get_global_exceptions elements)
    (get_blocks elements).

Definition set_fpp_modules (x : option modules) (elements : fpp_elements) :=
  create_fpp_elements
    (get_header elements)
    (get_waypoints elements)
    (get_sectors elements)
    (get_variables elements)
    x
    (get_includes elements)
    (get_global_exceptions elements)
    (get_blocks elements).

Definition set_fpp_includes (x : option includes) (elements : fpp_elements) :=
  create_fpp_elements
    (get_header elements)
    (get_waypoints elements)
    (get_sectors elements)
    (get_variables elements)
    (get_modules elements)
    x
    (get_global_exceptions elements)
    (get_blocks elements).

Definition set_fpp_global_exceptions (x : option exceptions) (elements : fpp_elements) :=
  create_fpp_elements
    (get_header elements)
    (get_waypoints elements)
    (get_sectors elements)
    (get_variables elements)
    (get_modules elements)
    (get_includes elements)
    x
    (get_blocks elements).

Definition set_fpp_blocks (x : option blocks) (elements : fpp_elements) :=
  create_fpp_elements
    (get_header elements)
    (get_waypoints elements)
    (get_sectors elements)
    (get_variables elements)
    (get_modules elements)
    (get_includes elements)
    (get_global_exceptions elements)
    x.

(** waypoint *)

Definition empty_waypoint :=
  mk_waypoint None (* name *)
              None (* x *)
              None (* y *)
              None (* lat *)
              None (* lon *)
              None (* alt *)
              None (* height *).


Definition set_waypoint_name (x : option name) (wp : waypoint) :=
  create_waypoint
    x
    (get_waypoint_x wp)
    (get_waypoint_y wp)
    (get_waypoint_lat wp)
    (get_waypoint_lon wp)
    (get_waypoint_alt wp)
    (get_waypoint_height wp).

Definition set_waypoint_x (x : option string) (wp : waypoint) :=
  create_waypoint
    (get_waypoint_name wp)
    x
    (get_waypoint_y wp)
    (get_waypoint_lat wp)
    (get_waypoint_lon wp)
    (get_waypoint_alt wp)
    (get_waypoint_height wp).

Definition set_waypoint_y (x : option string) (wp : waypoint) :=
  create_waypoint
    (get_waypoint_name wp)
    (get_waypoint_x wp)
    x
    (get_waypoint_lat wp)
    (get_waypoint_lon wp)
    (get_waypoint_alt wp)
    (get_waypoint_height wp).

Definition set_waypoint_lat (x : option string) (wp : waypoint) :=
  create_waypoint
    (get_waypoint_name wp)
    (get_waypoint_x wp)
    (get_waypoint_y wp)
    x
    (get_waypoint_lon wp)
    (get_waypoint_alt wp)
    (get_waypoint_height wp).

Definition set_waypoint_lon (x : option string) (wp : waypoint) :=
  create_waypoint
    (get_waypoint_name wp)
    (get_waypoint_x wp)
    (get_waypoint_y wp)
    (get_waypoint_lat wp)
    x
    (get_waypoint_alt wp)
    (get_waypoint_height wp).

Definition set_waypoint_alt (x : option string) (wp : waypoint) :=
  create_waypoint
    (get_waypoint_name wp)
    (get_waypoint_x wp)
    (get_waypoint_y wp)
    (get_waypoint_lat wp)
    (get_waypoint_lon wp)
    x
    (get_waypoint_height wp).

Definition set_waypoint_height (x : option string) (wp : waypoint) :=
  create_waypoint
    (get_waypoint_name wp)
    (get_waypoint_x wp)
    (get_waypoint_y wp)
    (get_waypoint_lat wp)
    (get_waypoint_lon wp)
    (get_waypoint_alt wp)
    x.

(** sector *)

Definition empty_sector :=
            mk_sector None (* name *)
                      None (* color *)
                      None (* type *)
                      nil (* corners *).


Definition set_sector_name (x : name) (sct : sector) :=
  mk_sector (Some x)
            (get_sector_color sct)
            (get_sector_type sct)
            (get_sector_corners sct).

Definition set_sector_color (x : string) (sct : sector) :=
  mk_sector (get_sector_name sct)
            (Some x)
            (get_sector_type sct)
            (get_sector_corners sct).

Definition set_sector_type (x : string) (sct : sector) :=
  mk_sector (get_sector_name sct)
            (get_sector_color sct)
            (Some x)
            (get_sector_corners sct).

Definition set_sector_corners (x : list corner) (sct : sector) :=
  mk_sector (get_sector_name sct)
            (get_sector_color sct)
            (get_sector_type sct)
            x.

(** variable *)

Definition empty_variable :=
  mk_variable None (* var *)
              None (* type *)
              None (* init *)
              None (* shortname *)
              None (* min *)
              None (* max *)
              None (* step *)
              None (* unit *)
              None (* alt_unit *)
              None (* alt_unit_coef *)
              None (* values *).

Definition set_variable_var (x : option string) (v : variable) :=
  mk_variable
    x
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_type (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    x
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_init (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    x
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_shortname (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    x
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_min (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    x
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_max (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    x
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_step (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    x
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_unit (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    x
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_alt_unit (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    x
    (get_variable_alt_unit_coef v)
    (get_variable_values v).

Definition set_variable_alt_unit_coef (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    x
    (get_variable_values v).

Definition set_variable_values (x : option string) (v : variable) :=
  mk_variable
    (get_variable_var v)
    (get_variable_type v)
    (get_variable_init v)
    (get_variable_shortname v)
    (get_variable_min v)
    (get_variable_max v)
    (get_variable_step v)
    (get_variable_unit v)
    (get_variable_alt_unit v)
    (get_variable_alt_unit_coef v)
    x.

(** abi_binding *)

Definition empty_abi_binding :=
  mk_abi_binding None (* name *)
                 None (* vars *)
                 None (* id *)
                 None (* handler *).

Definition set_abi_binding_name (x : option name) 
                                  (abi : abi_binding) :=
  mk_abi_binding
    x
    (get_abi_binding_vars abi)
    (get_abi_binding_id abi)
    (get_abi_binding_handler abi).

Definition set_abi_binding_vars (x : option string) 
                                  (abi : abi_binding) :=
  mk_abi_binding
    (get_abi_binding_name abi)
    x
    (get_abi_binding_id abi)
    (get_abi_binding_handler abi).

Definition set_abi_binding_id (x : option string) 
                                  (abi : abi_binding) :=
  mk_abi_binding
    (get_abi_binding_name abi)
    (get_abi_binding_vars abi)
    x
    (get_abi_binding_handler abi).

Definition set_abi_binding_handler (x : option string) 
                                      (abi : abi_binding) :=
  mk_abi_binding
    (get_abi_binding_name abi)
    (get_abi_binding_vars abi)
    (get_abi_binding_id abi)
    x.

Definition add_module_define (x : define)
                              (m : module) :=
  mk_module (get_module_name m)
            (get_module_type m)
            (x :: (get_module_defines m))
            (get_module_configures m).

Definition add_module_configure (x : configure)
                                  (m : module) :=
  mk_module (get_module_name m)
            (get_module_type m)
            (get_module_defines m)
            (x :: (get_module_configures m)).

Definition add_include_arg (x : arg)
                              (incl : include) :=
  mk_include (get_include_name incl)
             (get_include_procedure incl)
             (x :: (get_include_args incl))
             (get_include_fpp_withs incl).

Definition add_include_fpp_with (x : fpp_with)
                              (incl : include) :=
  mk_include (get_include_name incl)
             (get_include_procedure incl)
             (get_include_args incl)
             (x :: (get_include_fpp_withs incl)).

(** exception *)

Definition empty_exception :=
  mk_exception None (* cond *)
               None (* deroute *)
               None (* exec *).

Definition set_exception_cond (cond : option c_cond)
                          (exc : exception) :=
  mk_exception cond
               (get_exception_deroute exc)
               (get_exception_exec exc).

Definition set_exception_deroute (der : option block_name)
                          (exc : exception) :=
  mk_exception (get_exception_cond exc)
               der
               (get_exception_exec exc).

Definition set_exception_exec (exc_code : option c_code)
                          (exc : exception) :=
  mk_exception (get_exception_cond exc)
               (get_exception_deroute exc)
               exc_code.

(** block *)

Definition empty_block :=
  mk_block None (* name *)
           None (* pre_call *)
           None (* post_call *)
           None (* on_enter *)
           None (* on_exit *)
           None (* strip_button *)
           None (* strip_icon *)
           None (* group *)
           None (* key *)
           None (* description *)
           nil (* exceptions *)
           nil (* forbidden_deroute *)
           nil (* stages *).
  
Definition set_block_name (x : option block_name) (b : block) :=
  mk_block
    x
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_pre_call (x : option c_code) (b : block) :=
  mk_block
    (get_block_name b)
    x
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_post_call (x : option c_code) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    x
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_on_enter (x : option c_code) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    x
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_on_exit (x : option c_code) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    x
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_strip_button (x : option string) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    x
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_strip_icon (x : option string) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    x
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_group (x : option string) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    x
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_key (x : option string) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    x
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition set_block_description (x : option string) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    x
    (get_block_exceptions b)
    (get_block_fbd b)
    (get_block_stages b).

Definition app_block_exception (x : exception) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    ((get_block_exceptions b) ++ (x :: nil))
    (get_block_fbd b)
    (get_block_stages b).

Definition app_block_fbd (x : forbidden_deroute) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    ((get_block_fbd b) ++ (x :: nil) )
    (get_block_stages b).

Definition app_block_stage (x : fpp_stage) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    ((get_block_stages b) ++ (x :: nil)).
    
Definition set_block_stage (x : list fpp_stage) (b : block) :=
  mk_block
    (get_block_name b)
    (get_block_pre_call b)
    (get_block_post_call b)
    (get_block_on_enter b)
    (get_block_on_exit b)
    (get_block_strip_button b)
    (get_block_strip_icon b)
    (get_block_group b)
    (get_block_key b)
    (get_block_description b)
    (get_block_exceptions b)
    (get_block_fbd b)
    x.

(** stage parameters *)

Definition empty_stage :=
  mk_stage_param None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None
                None.

Definition set_stage_roll (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param x
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_pitch (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 x
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_yaw (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 x
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_climb (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 x
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_wp (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 x
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_wp2 (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 x
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_wpts (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 x
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_wp_qdr (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 x
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_wp_dist (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 x
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_from (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 x
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_from_qdr (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 x
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_from_dist (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 x
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_course (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 x
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_radius (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 x
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_center (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 x
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_turn_around (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 x
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_orientation (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 x
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_grid (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 x
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_commands (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 x
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_flags (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 x
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_ac_id (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 x
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_distance (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 x
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_vmode (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 x
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_hmode (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 x
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_alt (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 x
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_height (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 x
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_throttle (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 x
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_pre_call (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 x
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_post_call (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 x
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_nav_type (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 x
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_nav_params (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 x
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_until (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 x
                 (get_stage_approaching_time ob)
                 (get_stage_exceeding_time ob).

Definition set_stage_approaching_time (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 x
                 (get_stage_exceeding_time ob).

Definition set_stage_exceeding_time (x : option string)
                                        (ob : nav_stage_param) :=
  mk_stage_param (get_stage_roll ob)
                 (get_stage_pitch ob)
                 (get_stage_yaw ob)
                 (get_stage_climb ob)
                 (get_stage_wp ob)
                 (get_stage_wp2 ob)
                 (get_stage_wpts ob)
                 (get_stage_wp_qdr ob)
                 (get_stage_wp_dist ob)
                 (get_stage_from ob)
                 (get_stage_from_qdr ob)
                 (get_stage_from_dist ob)
                 (get_stage_course ob)
                 (get_stage_radius ob)
                 (get_stage_center ob)
                 (get_stage_turn_around ob)
                 (get_stage_orientation ob)
                 (get_stage_grid ob)
                 (get_stage_commands ob)
                 (get_stage_flags ob)
                 (get_stage_ac_id ob)
                 (get_stage_distance ob)
                 (get_stage_vmode ob)
                 (get_stage_hmode ob)
                 (get_stage_alt ob)
                 (get_stage_height ob)
                 (get_stage_throttle ob)
                 (get_stage_pre_call ob)
                 (get_stage_post_call ob)
                 (get_stage_nav_type ob)
                 (get_stage_nav_params ob)
                 (get_stage_until ob)
                 (get_stage_approaching_time ob)
                 x.

(** call *)

Definition empty_call :=
  mk_call None
          None
          None
          None.

Definition set_call_fun (x : option string)
                            (ob : call_stage) :=
  mk_call x
          (get_call_until ob)
          (get_call_loop ob)
          (get_call_break ob).

Definition set_call_until (x : option string)
                            (ob : call_stage) :=
  mk_call (get_call_fun ob)
          x
          (get_call_loop ob)
          (get_call_break ob).

Definition set_call_loop (x : option string)
                            (ob : call_stage) :=
  mk_call (get_call_fun ob)
          (get_call_until ob)
          x
          (get_call_break ob).

Definition set_call_break (x : option string)
                            (ob : call_stage) :=
  mk_call (get_call_fun ob)
          (get_call_until ob)
          (get_call_loop ob)
          x.
    
Definition call_once_from_call (ob : call_stage) :=
  mk_call_once (get_call_fun ob)
               (get_call_break ob).

(** for *)

Definition empty_for :=
  mk_for None
         None
         None.

Definition set_for_var (x : option string)
                          (ob : for_param) :=
  mk_for x
         (get_for_from ob)
         (get_for_to ob).

Definition set_for_from (x : option string)
                          (ob : for_param) :=
  mk_for (get_for_var ob)
         x
         (get_for_to ob).

Definition set_for_to (x : option string)
                          (ob : for_param) :=
  mk_for (get_for_var ob)
         (get_for_from ob)
         x.

Definition empty_procedure :=
  mk_procedure nil
               None
               None
               None
               None
               None
               None.


Definition add_proc_params (x : proc_param)
                                (ob : procedure) :=
  mk_procedure (x :: (get_proc_params ob))
               (get_proc_header ob)
               (get_proc_variables ob)
               (get_proc_waypoints ob)
               (get_proc_sectors ob)
               (get_proc_global_exceptions ob)
               (get_proc_blocks ob).

Definition set_proc_header (x : option header)
                                (ob : procedure) :=
  mk_procedure (get_proc_params ob)
               x
               (get_proc_variables ob)
               (get_proc_waypoints ob)
               (get_proc_sectors ob)
               (get_proc_global_exceptions ob)
               (get_proc_blocks ob).

Definition set_proc_variables (x : option variables)
                                (ob : procedure) := 
  mk_procedure (get_proc_params ob)
               (get_proc_header ob)
               x
               (get_proc_waypoints ob)
               (get_proc_sectors ob)
               (get_proc_global_exceptions ob)
               (get_proc_blocks ob).

Definition set_proc_waypoints (x : option waypoints)
                                (ob : procedure) :=
  mk_procedure (get_proc_params ob)
               (get_proc_header ob)
               (get_proc_variables ob)
               x
               (get_proc_sectors ob)
               (get_proc_global_exceptions ob)
               (get_proc_blocks ob).

Definition set_proc_sectors (x : option sectors)
                                (ob : procedure) :=
  mk_procedure (get_proc_params ob)
               (get_proc_header ob)
               (get_proc_variables ob)
               (get_proc_waypoints ob)
               x
               (get_proc_global_exceptions ob)
               (get_proc_blocks ob).

Definition set_proc_global_exceptions (x : option exceptions)
                                (ob : procedure) :=
  mk_procedure (get_proc_params ob)
               (get_proc_header ob)
               (get_proc_variables ob)
               (get_proc_waypoints ob)
               (get_proc_sectors ob)
               x
               (get_proc_blocks ob).

Definition set_proc_blocks (x : option blocks)
                                (ob : procedure) :=
  mk_procedure (get_proc_params ob)
               (get_proc_header ob)
               (get_proc_variables ob)
               (get_proc_waypoints ob)
               (get_proc_sectors ob)
               (get_proc_global_exceptions ob)
               x.
