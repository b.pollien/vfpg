%{
Require Import String.
From Coq Require Import List String Bool.
From VFP Require Import BasicTypes
                        FlightPlanParsed FPPUtils.
%}

%start <Parser_result> parse_fp

%token <string> Data
%token <string> Errormsg

%token BFLIGHT_PLAN BPROCEDURE
%type<fpp_info> Fp_data 
%type<fpp_elements> Fp_elem
%type<procedure> Procedure
%token NAME LAT0 LON0 MDFH GALT SHEIGHT ALT WPF QFU HMHEIGHT
%token GEOMAXH GEOMAXALT GEOSECTOR

%token BHEADER

%token BPARAM
%token DEFAULT_VALUE


%type<waypoints> Waypoints
%token BWAYPOINTS UTMX0 UTMY0

%type<list waypoint> Waypoints_aux
%type<waypoint> Waypoint
%token BWAYPOINT X Y LAT LON HEIGHT

%type<sectors> Sectors
%token BSECTORS BKML FILE

%type<sector> Sector_info
%token BSECTOR COLOR TYPE

%type<list corner> Corners
%token BCORNER

%type<variables> Variables
%token BVARIABLES

%type<variable> Var_info
%token BVARIABLE INIT SHORTNAME MIN MAX STEP UNIT ALT_UNIT
%token ALT_UNIT_COEF VALUES VAR

%type<abi_binding> Abi_info
%token BABI_BINDING VARS ID HANDLER

%type<modules> Modules
%token BMODULES

%type<module> Module Module_constr
%token BMODULE BDEFINE BCONFIGURE VALUE

%type<includes> Includes
%token BINCLUDES

%type<include> Include Include_constr
%token BINCLUDE BARG BWITH FROM BTO

%type<exceptions> Exceptions
%token BEXCEPTIONS

%type<exception> Exception
%token BEXCEPTION COND EXEC

%type<blocks> Blocks
%token BBLOCKS

%type<block> Block Block_constr
%token BBLOCK PRE_CALL POST_CALL ON_ENTER ON_EXIT STRIP_BUTTON STRIP_ICON
%token GROUP KEY DESCRIPTION

%type<forbidden_deroute> Forbidden_Deroute
%token BFORBIDDEN_DEROUTE


(* stages *)
%type<fpp_stage> Fp_stage
%type<list fpp_stage> Fp_stage_list
%token COURSE VMODE HMODE THROTTLE CLIMB PITCH YAW ROLL
%token UNTIL NAV_TYPE APPRTIME EXCEEDTIME WP WP2 WP_QDR
%token WP_DIST FROM_QDR FROM_DIST WPTS RADIUS CENTER ORIENTATION
%token TURN_AROUND GRID COMMANDS FLAGS AC_ID DISTANCE NAV_PARAMS

%token BHOME

%type<nav_stage_param> Nav_stage_param
%token BHEADING BATTITUDE BMANUAL BGO BXYZ BCIRCLE BEIGHT BOVAL
%token BSURVEY_RECT BSTAY BFOLLOW BPATH BGUIDED

%token BRETURN RESET_STAGE
%token BDEROUTE

%type<call_stage> Call
%token BCALL BCALL_ONCE
%token FUN LOOP BREAK

%token BSET

%type<option string> While_cond
%token BWHILE

%type<for_param> For_param
%token BFOR

%token EQ "="
%token SB "<" SCB "</" EB ">" EEB "/>"
%token EOF

%%

parse_fp:
  | "<" BFLIGHT_PLAN datas = Fp_data ">"
    elems = Fp_elem
    "</" BFLIGHT_PLAN ">" EOF
      { Fpp (mk_fpp datas elems) } 
  | "<" BPROCEDURE ">"
      proc = Procedure
      { Proc proc } 
  | err = Errormsg
    { Err err }

Fp_data :
    { empty_fpp_info }
  | NAME "=" name = Data info = Fp_data 
    { set_fpp_name (Some name) info }
  | LAT0 "=" data = Data info = Fp_data
    { set_fpp_lat0 (Some data) info }
  | LON0 "=" data = Data info = Fp_data
    { set_fpp_lon0 (Some data) info }
  | MDFH "=" data = Data info = Fp_data
    { set_fpp_max_dist_from_home (Some data) info }
  | GALT "=" data = Data info = Fp_data
    { set_fpp_ground_alt (Some data) info }
  | SHEIGHT "=" data = Data info = Fp_data
    { set_fpp_security_height (Some data) info }
  | ALT "=" data = Data info = Fp_data
    { set_fpp_alt (Some data) info }
  | WPF "=" data = Data info = Fp_data
    { set_fpp_wp_frame (Some data) info }
  | QFU "=" data = Data info = Fp_data
    { set_fpp_qfu (Some data) info }
  | HMHEIGHT "=" data = Data info = Fp_data
    { set_fpp_home_mode_height (Some data) info }
  | GEOMAXH "=" data = Data info = Fp_data
    { set_fpp_geofence_max_height (Some data) info }
  | GEOMAXALT "=" data = Data info = Fp_data
    { set_fpp_geofence_max_alt (Some data) info }
  | GEOSECTOR "=" data = Data info = Fp_data
    { set_fpp_geofence_sector (Some data) info }

Fp_elem :
    { empty_fpp_elements }
  | "<" BHEADER ">" data = Data "</" BHEADER ">"
      elems = Fp_elem
    {set_fpp_header (Some data) elems}
  | "<" BHEADER "/>"
      elems = Fp_elem
      {elems}
  | wps = Waypoints
      elems = Fp_elem
    {set_fpp_waypoints (Some wps) elems}
  | "<" BSECTORS ">"
      sects = Sectors
    "</" BSECTORS ">"
      elems = Fp_elem
    {set_fpp_sectors (Some sects) elems}
  | "<" BSECTORS "/>"
      elems = Fp_elem
    { elems }
  | "<" BVARIABLES ">"
      vars = Variables
    "</" BVARIABLES ">"
      elems = Fp_elem
    {set_fpp_variables (Some vars) elems}
  | "<" BVARIABLES "/>"
      elems = Fp_elem
    { elems }
  | "<" BMODULES ">"
      mods = Modules
    "</" BMODULES ">"
      elems = Fp_elem
    {set_fpp_modules (Some mods) elems}
  | "<" BMODULES "/>"
      elems = Fp_elem
    { elems }
  | "<" BINCLUDES ">"
      incls = Includes
    "</" BINCLUDES ">"
      elems = Fp_elem
    {set_fpp_includes (Some incls) elems}
  | "<" BINCLUDES "/>"
      elems = Fp_elem
    { elems }
  | "<" BEXCEPTIONS ">"
      excs = Exceptions
    "</" BEXCEPTIONS ">"
      elems = Fp_elem
    {set_fpp_global_exceptions (Some excs) elems}
  | "<" BEXCEPTIONS "/>"
      elems = Fp_elem
    { elems }
  | "<" BBLOCKS ">"
      bks = Blocks
    "</" BBLOCKS ">"
      elems = Fp_elem
    {set_fpp_blocks (Some bks) elems}
  | "<" BBLOCKS "/>"
      elems = Fp_elem
    { elems }

Procedure:
  | "</" BPROCEDURE ">" EOF { empty_procedure }
  | "<" BHEADER ">" data = Data "</" BHEADER ">"
      proc = Procedure
      {set_proc_header (Some data) proc}
  | "<" BHEADER "/>"
      proc = Procedure
    { proc }
  | "<" BVARIABLES ">"
      vars = Variables
    "</" BVARIABLES ">"
      proc = Procedure
    {set_proc_variables (Some vars) proc}
  | "<" BVARIABLES "/>"
      proc = Procedure
    { proc }
  | wps = Waypoints
      proc = Procedure
    {set_proc_waypoints (Some wps) proc}
  | "<" BSECTORS ">"
      sects = Sectors
    "</" BSECTORS ">"
      proc = Procedure
    {set_proc_sectors (Some sects) proc}
  | "<" BSECTOR "/>"
      proc = Procedure
    { proc }
  | "<" BEXCEPTIONS ">"
      excs = Exceptions
    "</" BEXCEPTIONS ">"
      proc = Procedure
    {set_proc_global_exceptions (Some excs) proc}
  | "<" BEXCEPTIONS "/>"
      proc = Procedure
    { proc }
  | "<" BBLOCKS ">"
      bks = Blocks
    "</" BBLOCKS ">"
        proc = Procedure
    {set_proc_blocks (Some bks) proc}
  | "<" BBLOCKS "/>"
      proc = Procedure
    { proc }
  (* params *)
  | "<" BPARAM NAME "=" name = Data "/>"
        proc = Procedure
    { add_proc_params (mk_proc_param name None) proc}
  | "<" BPARAM DEFAULT_VALUE "=" data = Data NAME "=" name = Data "/>"
        proc = Procedure
    { add_proc_params (mk_proc_param name (Some data)) proc}
  | "<" BPARAM NAME "=" name = Data DEFAULT_VALUE "=" data = Data "/>"
        proc = Procedure
    { add_proc_params (mk_proc_param name (Some data)) proc}


(** Waypoints *)

Waypoints :
  | "<" BWAYPOINTS 
          UTMX0 "=" utmx0 = Data
          UTMY0 "=" utmy0 = Data ">"
    wpts = Waypoints_aux
    "</" BWAYPOINTS ">"
      { mk_waypoints (Some utmx0) (Some utmy0) wpts }
  | "<" BWAYPOINTS 
          UTMY0 "=" utmy0 = Data
          UTMX0 "=" utmx0 = Data ">"
    wpts = Waypoints_aux
    "</" BWAYPOINTS ">"
      { mk_waypoints (Some utmx0) (Some utmy0) wpts }
  | "<" BWAYPOINTS ">"
    wpts = Waypoints_aux
    "</" BWAYPOINTS ">"
      { mk_waypoints None None wpts }

Waypoints_aux :
    "<" BWAYPOINT wp = Waypoint "/>" wps = Waypoints_aux { wp :: wps }
  | "<" BWAYPOINT wp = Waypoint "/>" { [wp] }

Waypoint :
    { empty_waypoint }
  | NAME "=" name = Data wp = Waypoint
    { set_waypoint_name (Some name) wp }
  | X "=" value = Data wp = Waypoint
    { set_waypoint_x (Some value) wp }
  | Y "=" value = Data wp = Waypoint
    { set_waypoint_y (Some value) wp }
  | LAT "=" value = Data wp = Waypoint
    { set_waypoint_lat (Some value) wp }
  | LON "=" value = Data wp = Waypoint
    { set_waypoint_lon (Some value) wp }
  | ALT "=" value = Data wp = Waypoint
    { set_waypoint_alt (Some value) wp }
  | HEIGHT "=" value = Data wp = Waypoint
    { set_waypoint_height (Some value) wp }

(* Sectors *)

Sectors :
  { [] }
  | "<" BSECTOR sector_info = Sector_info ">"
      corners = Corners 
    "</" BSECTOR ">"
    sects = Sectors 
    { (Sector (set_sector_corners corners sector_info)) :: sects }
  | "<" BKML FILE "=" info = Data "/>"
    sects = Sectors
    { (Kml info) :: sects }

    Sector_info :
    { empty_sector }
  | NAME "=" name = Data sct_info = Sector_info
    { set_sector_name name sct_info }
  | COLOR "=" color = Data sct_info = Sector_info
    { set_sector_color color sct_info }
  | TYPE "=" data = Data sct_info = Sector_info
    { set_sector_type data sct_info }
  
Corners :
    { [] }
  | "<" BCORNER NAME "=" name = Data "/>" cnrs = Corners
    { name :: cnrs }

(** variables *)

Variables :
    { [] }
  | "<" BVARIABLE info = Var_info "/>"
    vars = Variables
    { (Var info) :: vars }
  | "<" BABI_BINDING info = Abi_info "/>"
    vars = Variables 
    { (Abi_binding info) :: vars }

Var_info:
    { empty_variable }
  | VAR "=" data = Data info = Var_info
    { set_variable_var (Some data) info }
  | TYPE "=" data = Data info = Var_info
    { set_variable_type (Some data) info }
  | INIT "=" data = Data info = Var_info
    { set_variable_init (Some data) info }
  | SHORTNAME "=" data = Data info = Var_info
    { set_variable_shortname (Some data) info }
  | MIN "=" data = Data info = Var_info
    { set_variable_min (Some data) info }
  | MAX "=" data = Data info = Var_info
    { set_variable_max (Some data) info }
  | STEP "=" data = Data info = Var_info
    { set_variable_step (Some data) info }
  | UNIT "=" data = Data info = Var_info
    { set_variable_unit (Some data) info }
  | ALT_UNIT "=" data = Data info = Var_info
    { set_variable_alt_unit (Some data) info }
  | ALT_UNIT_COEF "=" data = Data info = Var_info
    { set_variable_alt_unit_coef (Some data) info }
  | VALUES "=" data = Data info = Var_info
    { set_variable_values (Some data) info }

Abi_info:
    { empty_abi_binding }
  | NAME "=" data = Data info = Abi_info
    { set_abi_binding_name (Some data) info }
  | VARS "=" data = Data info = Abi_info
    { set_abi_binding_vars (Some data) info }
  | ID "=" data = Data info = Abi_info
    { set_abi_binding_id (Some data) info }
  | HANDLER "=" data = Data info = Abi_info
    { set_abi_binding_handler (Some data) info }

(** modules *)
Modules:
    { [] }
  | m = Module "</" BMODULE ">" mods = Modules
    { m :: mods }
  | m = Module_constr "/>" mods = Modules
    { m :: mods }

Module_constr:
  | "<" BMODULE 
        NAME "=" name = Data
    { mk_module name None nil nil }
  | "<" BMODULE 
        NAME "=" name = Data 
        TYPE "=" data = Data
    { mk_module name (Some data) nil nil }
  | "<" BMODULE 
        TYPE "=" data = Data 
        NAME "=" name = Data
    { mk_module name (Some data) nil nil }

Module:
  (* module constructor *)
  | m = Module_constr ">"
    { m }
  (* add define *)
  | m = Module 
    "<" BDEFINE NAME "=" name = Data "/>"
    { add_module_define (mk_define name None) m }
  | m = Module 
    "<" BDEFINE NAME "=" name = Data 
                VALUE "=" data = Data "/>"
    { add_module_define (mk_define name (Some data)) m }
  | m = Module 
    "<" BDEFINE VALUE "=" data = Data
                NAME "=" name = Data "/>"
    { add_module_define (mk_define name (Some data)) m }
  (* add configure *)
  | m = Module 
    "<" BCONFIGURE NAME "=" name = Data 
                   VALUE "=" data = Data "/>"
    { add_module_configure (mk_configure name data) m }
  | m = Module 
    "<" BCONFIGURE VALUE "=" data = Data
                   NAME "=" name = Data "/>"
    { add_module_configure (mk_configure name data) m }

(* Includes *)
Includes:
    { [] }
  | incl = Include "</" BINCLUDE ">"
    incls = Includes
    { incl :: incls }
  | incl = Include_constr "/>"
    incls = Includes
    { incl :: incls }

Include_constr:
  | "<" BINCLUDE NAME "=" name = Data
                 BPROCEDURE "=" prc = Data
    { mk_include name prc nil nil }
  | "<" BINCLUDE BPROCEDURE "=" prc = Data
                 NAME "=" name = Data
    { mk_include name prc nil nil }

Include:
  (* include constructor *)
  | incl = Include_constr ">"
    { incl }
  (* add arg *)
  | incl = Include
    "<" BARG NAME "=" name = Data
             VALUE "=" data = Data "/>"
    { add_include_arg (mk_arg name data) incl }
  | incl = Include
    "<" BARG VALUE "=" data = Data
             NAME "=" name = Data "/>"
    { add_include_arg (mk_arg name data) incl }
  (* add fpp_with *)
  | incl = Include
    "<" BWITH FROM "=" from = Data
              BTO "=" with_to = Data "/>"
    { add_include_fpp_with (mk_fpp_with from with_to) incl }
  | incl = Include
    "<" BWITH BTO "=" with_to = Data
              FROM "=" from = Data "/>"
    { add_include_fpp_with (mk_fpp_with from with_to) incl }

(* exceptions *)
Exceptions:
    { [] }
  | "<" BEXCEPTION exc = Exception "/>"
    excs = Exceptions
    { exc :: excs }

Exception:
    { empty_exception }
  | COND "=" data = Data exc = Exception
    { set_exception_cond (Some data) exc }
  | BDEROUTE "=" data = Data exc = Exception
    { set_exception_deroute (Some data) exc }
  | EXEC "=" data = Data exc = Exception
    { set_exception_exec (Some data) exc }

(* block *)

Blocks:
    { [] }
  | blk = Block "</" BBLOCK ">" blks = Blocks
    { blk :: blks }
  | "<" BBLOCK blk = Block_constr "/>" blks = Blocks
    { blk :: blks }

Block_constr:
    { empty_block }
  | NAME "=" name = Data blk = Block_constr 
    { set_block_name (Some name) blk }
  | PRE_CALL "=" data = Data blk = Block_constr 
    { set_block_pre_call (Some data) blk }
  | POST_CALL "=" data = Data blk = Block_constr 
    { set_block_post_call (Some data) blk }
  | ON_ENTER "=" data = Data blk = Block_constr 
    { set_block_on_enter (Some data) blk }
  | ON_EXIT "=" data = Data blk = Block_constr 
    { set_block_on_exit (Some data) blk }
  | STRIP_BUTTON "=" data = Data blk = Block_constr 
    { set_block_strip_button (Some data) blk }
  | STRIP_ICON "=" data = Data blk = Block_constr 
    { set_block_strip_icon (Some data) blk }
  | GROUP "=" data = Data blk = Block_constr 
    { set_block_group (Some data) blk }
  | KEY "=" data = Data blk = Block_constr 
    { set_block_key (Some data) blk }
  | DESCRIPTION "=" data = Data blk = Block_constr 
    { set_block_description (Some data) blk }

Block:
  (* block constructor *)
  | "<" BBLOCK blk = Block_constr ">" { blk }
  (* append a forbidden_deroute *)
  | blk = Block fbd = Forbidden_Deroute
    { app_block_fbd fbd blk }
  (* append a stage *)
  | blk = Block stg = Fp_stage
    { app_block_stage stg blk }
  (* append a exception*)
  | blk = Block "<" BEXCEPTION excl = Exception "/>"
    { app_block_exception excl blk }

Forbidden_Deroute:
  | "<" BFORBIDDEN_DEROUTE BTO "=" dest = Data "/>"
    { mk_fbd None dest }
  | "<" BFORBIDDEN_DEROUTE BTO "=" dest = Data 
                           COND "=" data = Data "/>"
    { mk_fbd (Some data) dest }
  | "<" BFORBIDDEN_DEROUTE COND "=" data = Data
                           BTO "=" dest = Data "/>"
    { mk_fbd (Some data) dest }

Fp_stage_list:
    { [] }
  | st = Fp_stage sts = Fp_stage_list
    { st :: sts }

Fp_stage:
  (* nav stage *)
  | "<" BHOME "/>" { Home_stage }
  | "<" BHEADING param = Nav_stage_param "/>" { Heading_stage param }
  | "<" BATTITUDE param = Nav_stage_param "/>" { Attitude_stage param }
  | "<" BMANUAL param = Nav_stage_param "/>" { Manual_stage param }
  | "<" BGO param = Nav_stage_param "/>" { Go_stage param }
  | "<" BXYZ param = Nav_stage_param "/>" { Xyz_stage param }
  | "<" BCIRCLE param = Nav_stage_param "/>" { Circle_stage param }
  | "<" BEIGHT param = Nav_stage_param "/>" { Eight_stage param }
  | "<" BOVAL param = Nav_stage_param "/>" { Oval_stage param }
  | "<" BSURVEY_RECT param = Nav_stage_param "/>" 
      { Survey_rectangle_stage param }
  | "<" BSTAY param = Nav_stage_param "/>" { Stay_stage param }
  | "<" BFOLLOW param = Nav_stage_param "/>" { Follow_stage param }
  | "<" BPATH param = Nav_stage_param "/>" { Path_stage param }
  | "<" BGUIDED param = Nav_stage_param "/>" { Guided_stage param }
  (* logic stage *)
  | "<" BRETURN RESET_STAGE "=" data = Data "/>"
    { Return_stage (mk_return data) }
  | "<" BRETURN "/>"
    { Return_stage (mk_return "false")}
  | "<" BDEROUTE BBLOCK "=" data = Data "/>"
    { Deroute_stage (mk_deroute data) }
  | "<" BSET VAR "=" var = Data
             VALUE "=" value = Data "/>"
    { Set_stage (mk_set var value) }
  | "<" BSET VALUE "=" value = Data
             VAR "=" var = Data "/>"
    { Set_stage (mk_set var value) }
  | "<" BCALL call = Call "/>"
    { Call_stage call }
  | "<" BCALL_ONCE call = Call "/>"
    { Call_once_stage (call_once_from_call call) }
  (* loop stage *)
  | "<" BWHILE cond = While_cond "/>"
    { While_stage cond nil }
  | "<" BWHILE cond = While_cond ">"
      sts = Fp_stage_list
    "</" BWHILE ">"
    { While_stage cond sts }
  | "<" BFOR param = For_param "/>"
    { For_stage param nil }
  | "<" BFOR param = For_param ">"
      sts = Fp_stage_list
    "</" BFOR ">"
    { For_stage param sts }
 
Nav_stage_param:
    { empty_stage }
  | ROLL "=" data = Data param = Nav_stage_param
    { set_stage_roll (Some data) param }
  | PITCH "=" data = Data param = Nav_stage_param
    { set_stage_pitch (Some data) param }
  | YAW "=" data = Data param = Nav_stage_param
    { set_stage_yaw (Some data) param }
  | CLIMB "=" data = Data param = Nav_stage_param
    { set_stage_climb (Some data) param }
  | WP "=" data = Data param = Nav_stage_param
    { set_stage_wp (Some data) param }
  | WP2 "=" data = Data param = Nav_stage_param
    { set_stage_wp2 (Some data) param }
  | WPTS "=" data = Data param = Nav_stage_param
    { set_stage_wpts (Some data) param }
  | WP_QDR "=" data = Data param = Nav_stage_param
    { set_stage_wp_qdr (Some data) param }
  | WP_DIST "=" data = Data param = Nav_stage_param
    { set_stage_wp_dist (Some data) param }
  | FROM "=" data = Data param = Nav_stage_param
    { set_stage_from (Some data) param }
  | FROM_QDR "=" data = Data param = Nav_stage_param
    { set_stage_from_qdr (Some data) param }
  | FROM_DIST "=" data = Data param = Nav_stage_param
    { set_stage_from_dist (Some data) param }
  | COURSE "=" data = Data param = Nav_stage_param
    { set_stage_course (Some data) param }
  | RADIUS "=" data = Data param = Nav_stage_param
    { set_stage_radius (Some data) param }
  | CENTER "=" data = Data param = Nav_stage_param
    { set_stage_center (Some data) param }
  | TURN_AROUND "=" data = Data param = Nav_stage_param
    { set_stage_turn_around (Some data) param }
  | ORIENTATION "=" data = Data param = Nav_stage_param
    { set_stage_orientation (Some data) param }
  | GRID "=" data = Data param = Nav_stage_param
    { set_stage_grid (Some data) param }
  | COMMANDS "=" data = Data param = Nav_stage_param
    { set_stage_commands (Some data) param }
  | FLAGS "=" data = Data param = Nav_stage_param
    { set_stage_flags (Some data) param }
  | AC_ID "=" data = Data param = Nav_stage_param
    { set_stage_ac_id (Some data) param }
  | DISTANCE "=" data = Data param = Nav_stage_param
    { set_stage_distance (Some data) param }
  | VMODE "=" data = Data param = Nav_stage_param
    { set_stage_vmode (Some data) param }
  | HMODE "=" data = Data param = Nav_stage_param
    { set_stage_hmode (Some data) param }
  | ALT "=" data = Data param = Nav_stage_param
    { set_stage_alt (Some data) param }
  | HEIGHT "=" data = Data param = Nav_stage_param
    { set_stage_height (Some data) param }
  | THROTTLE "=" data = Data param = Nav_stage_param
    { set_stage_throttle (Some data) param }
  | PRE_CALL "=" data = Data param = Nav_stage_param
    { set_stage_pre_call (Some data) param }
  | POST_CALL "=" data = Data param = Nav_stage_param
    { set_stage_post_call (Some data) param }
  | NAV_TYPE "=" data = Data param = Nav_stage_param
    { set_stage_nav_type (Some data) param }
  | NAV_PARAMS "=" data = Data param = Nav_stage_param
    { set_stage_nav_params (Some data) param }
  | UNTIL "=" data = Data param = Nav_stage_param
    { set_stage_until (Some data) param }
  | APPRTIME "=" data = Data param = Nav_stage_param
    { set_stage_approaching_time (Some data) param }
  | EXCEEDTIME "=" data = Data param = Nav_stage_param
    { set_stage_exceeding_time (Some data) param }

Call:
    { empty_call }
  | FUN "=" data = Data call = Call
    { set_call_fun (Some data) call }
  | UNTIL "=" data = Data call = Call
    { set_call_until (Some data) call }
  | LOOP "=" data = Data call = Call
    { set_call_loop (Some data) call }
  | BREAK "=" data = Data call = Call
    { set_call_break (Some data) call }

While_cond:
    { None }
  | COND "=" data = Data
    { Some data }
  
For_param:
    { empty_for }
  | VAR "=" data = Data fr = For_param
    { set_for_var (Some data) fr }
  | BTO "=" data = Data fr = For_param
    { set_for_to (Some data) fr }
  | FROM "=" data = Data fr = For_param
    { set_for_from (Some data) fr }