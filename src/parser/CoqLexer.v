
(** Hand-written lexer for natural numbers, idents, parens and + - * / *)

Require Import BinNat Ascii String.
From VFP Require Import Parser.

Import MenhirLibParser.Inter.
Open Scope string_scope.
Open Scope char_scope.
Open Scope bool_scope.

(** No such thing as an empty buffer, instead we use
    an infinite stream of EOF *)

CoFixpoint TheEnd : buffer := Buf_cons (EOF tt) TheEnd.

Inductive Erropt {A : Type} :=
  | SomeErr (a : A)
  | ErrorErr (msg : string).

Definition erropt_map {A B : Type } 
                      (f : A -> B)
                      (a : @Erropt A) 
                        : @Erropt B :=
  match a with
  | SomeErr v => SomeErr (f v)
  | ErrorErr msg => ErrorErr msg
  end.

Fixpoint ntail n s :=
  match n, s with
  | 0, _ => s
  | _, EmptyString => s
  | S n, String _ s => ntail n s
  end.

(** Comparison on characters *)

Definition Data_limiter := """".

Definition ascii_eqb c c' := (N_of_ascii c =? N_of_ascii c')%N.
Definition ascii_leb c c' := (N_of_ascii c <=? N_of_ascii c')%N.

Definition is_digit c := (("0" <=? c) && (c <=? "9"))%char.

Definition is_alpha c :=
  ((("a" <=? c) && (c <=? "z")) ||
   (("A" <=? c) && (c <=? "Z")) ||
   (c =? "_") ||
   (c =? "!"))%char.

Definition to_lower c :=
  if (("A" <=? c) && (c <=? "Z"))
  then (ascii_of_N ((N_of_ascii c) + 32))
  else c.

Fixpoint to_lower_string s :=
  match s with
  | EmptyString => EmptyString
  | String c s => String (to_lower c) (to_lower_string s)
  end.

Fixpoint identsize s :=
  match s with
  | EmptyString => 0
  | String c s =>
    if is_alpha c || is_digit c then S (identsize s)
    else 0
  end.

Fixpoint Data_size s :=
  match s with
  | EmptyString => 0
  | String c s' => if (c =? Data_limiter) then 0
                  else S (Data_size s')
  end.

Fixpoint find_string s t :=
  if prefix t s then 0
  else match s with
       | String c s => S (find_string s t)
       | EmptyString => 0
  end.

Fixpoint Comment_size s :=
  if (prefix "-->" s) then 3
  else match s with
  | EmptyString => 0
  | String _ s' => S (Comment_size s')
  end.

Fixpoint readnum acc s :=
  match s with
  | String "0" s => readnum (acc*10) s
  | String "1" s => readnum (acc*10+1) s
  | String "2" s => readnum (acc*10+2) s
  | String "3" s => readnum (acc*10+3) s
  | String "4" s => readnum (acc*10+4) s
  | String "5" s => readnum (acc*10+5) s
  | String "6" s => readnum (acc*10+6) s
  | String "7" s => readnum (acc*10+7) s
  | String "8" s => readnum (acc*10+8) s
  | String "9" s => readnum (acc*10+9) s
  | _ => (acc,s)
  end.

Definition Dictionary (s : string) : @Erropt token :=
  match to_lower_string s with
  | "yaw"%string => SomeErr (YAW tt)
  | "y"%string => SomeErr (Y tt)
  | "x"%string => SomeErr (X tt)
  | "wp_qdr"%string => SomeErr (WP_QDR tt)
  | "wp_dist"%string => SomeErr (WP_DIST tt)
  | "wpts"%string => SomeErr (WPTS tt)
  | "wp_frame"%string => SomeErr (WPF tt)
  | "wp"%string => SomeErr (WP tt)
  | "wp1"%string => SomeErr (WP tt)
  | "wp2"%string => SomeErr (WP2 tt)
  | "vmode"%string => SomeErr (VMODE tt)
  | "vars"%string => SomeErr (VARS tt)
  | "var"%string => SomeErr (VAR tt)
  | "values"%string => SomeErr (VALUES tt)
  | "value"%string => SomeErr (VALUE tt)
  | "utm_y0"%string => SomeErr (UTMY0 tt)
  | "utm_x0"%string => SomeErr (UTMX0 tt)
  | "until"%string => SomeErr (UNTIL tt)
  | "unit"%string => SomeErr (UNIT tt)
  | "type"%string => SomeErr (TYPE tt)
  | "turn_around"%string => SomeErr (TURN_AROUND tt)
  | "throttle"%string => SomeErr (THROTTLE tt)
  | "strip_icon"%string => SomeErr (STRIP_ICON tt)
  | "strip_button"%string => SomeErr (STRIP_BUTTON tt)
  | "step"%string => SomeErr (STEP tt)
  | "shortname"%string => SomeErr (SHORTNAME tt)
  | "security_height"%string => SomeErr (SHEIGHT tt)
  | "roll"%string => SomeErr (ROLL tt)
  | "reset_stage"%string => SomeErr (RESET_STAGE tt)
  | "radius"%string => SomeErr (RADIUS tt)
  | "qfu"%string => SomeErr (QFU tt)
  | "pre_call"%string => SomeErr (PRE_CALL tt)
  | "post_call"%string => SomeErr (POST_CALL tt)
  | "pitch"%string => SomeErr (PITCH tt)
  | "p1"%string => SomeErr (WP tt)
  | "p2"%string => SomeErr (WP2 tt)
  | "orientation"%string => SomeErr (ORIENTATION tt)
  | "on_exit"%string => SomeErr (ON_EXIT tt)
  | "on_enter"%string => SomeErr (ON_ENTER tt)
  | "nav_type"%string => SomeErr (NAV_TYPE tt)
  | "nav_params"%string => SomeErr (NAV_PARAMS tt)
  | "name"%string => SomeErr (NAME tt)
  | "min"%string => SomeErr (MIN tt)
  | "max_dist_from_home"%string => SomeErr (MDFH tt)
  | "max"%string => SomeErr (MAX tt)
  | "loop"%string => SomeErr (LOOP tt)
  | "lon0"%string => SomeErr (LON0 tt)
  | "lon"%string => SomeErr (LON tt)
  | "lat0"%string => SomeErr (LAT0 tt)
  | "lat"%string => SomeErr (LAT tt)
  | "key"%string => SomeErr (KEY tt)
  | "init"%string => SomeErr (INIT tt)
  | "id"%string => SomeErr (ID tt)
  | "hmode"%string => SomeErr (HMODE tt)
  | "home_mode_height"%string => SomeErr (HMHEIGHT tt)
  | "height"%string => SomeErr (HEIGHT tt)
  | "handler"%string => SomeErr (HANDLER tt)
  | "grid"%string => SomeErr (GRID tt)
  | "group"%string => SomeErr (GROUP tt)
  | "geofence_sector"%string => SomeErr (GEOSECTOR tt)
  | "geofence_max_height"%string => SomeErr (GEOMAXH tt)
  | "geofence_max_alt"%string => SomeErr (GEOMAXALT tt)
  | "ground_alt"%string => SomeErr (GALT tt)
  | "fun"%string => SomeErr (FUN tt)
  | "from_qdr"%string => SomeErr (FROM_QDR tt)
  | "from_dist"%string => SomeErr (FROM_DIST tt)
  | "from"%string => SomeErr (FROM tt)
  | "flags"%string => SomeErr (FLAGS tt)
  | "file"%string => SomeErr (FILE tt)
  | "exec"%string => SomeErr (EXEC tt)
  | "exceeding_time"%string => SomeErr (EXCEEDTIME tt)
  | "distance"%string => SomeErr (DISTANCE tt)
  | "description"%string => SomeErr (DESCRIPTION tt)
  | "default_value"%string => SomeErr (DEFAULT_VALUE tt)
  | "course"%string => SomeErr (COURSE tt)
  | "cond"%string => SomeErr (COND tt)
  | "commands"%string => SomeErr (COMMANDS tt)
  | "color"%string => SomeErr (COLOR tt)
  | "climb"%string => SomeErr (CLIMB tt)
  | "center"%string => SomeErr (CENTER tt)
  | "xyz"%string => SomeErr (BXYZ tt)
  | "with"%string => SomeErr (BWITH tt)
  | "while"%string => SomeErr (BWHILE tt)
  | "waypoints"%string => SomeErr (BWAYPOINTS tt)
  | "waypoint"%string => SomeErr (BWAYPOINT tt)
  | "variables"%string => SomeErr (BVARIABLES tt)
  | "to"%string => SomeErr (BTO tt)
  | "survey_rectangle"%string => SomeErr (BSURVEY_RECT tt)
  | "stay"%string => SomeErr (BSTAY tt)
  | "set"%string => SomeErr (BSET tt)
  | "sectors"%string => SomeErr (BSECTORS tt)
  | "sector"%string => SomeErr (BSECTOR tt)
  | "return"%string => SomeErr (BRETURN tt)
  | "break"%string => SomeErr (BREAK tt)
  | "procedure"%string => SomeErr (BPROCEDURE tt)
  | "path"%string => SomeErr (BPATH tt)
  | "param"%string => SomeErr (BPARAM tt)
  | "oval"%string => SomeErr (BOVAL tt)
  | "modules"%string => SomeErr (BMODULES tt)
  | "module"%string => SomeErr (BMODULE tt)
  | "manual"%string => SomeErr (BMANUAL tt)
  | "kml"%string => SomeErr (BKML tt)
  | "includes"%string => SomeErr (BINCLUDES tt)
  | "include"%string => SomeErr (BINCLUDE tt)
  | "home"%string => SomeErr (BHOME tt)
  | "heading"%string => SomeErr (BHEADING tt)
  | "header"%string => SomeErr (BHEADER tt)
  | "guided"%string => SomeErr (BGUIDED tt)
  | "go"%string => SomeErr (BGO tt)
  | "forbidden_deroute"%string => SomeErr (BFORBIDDEN_DEROUTE tt)
  | "for"%string => SomeErr (BFOR tt)
  | "follow"%string => SomeErr (BFOLLOW tt)
  | "flight_plan"%string => SomeErr (BFLIGHT_PLAN tt)
  | "exceptions"%string => SomeErr (BEXCEPTIONS tt)
  | "exception"%string => SomeErr (BEXCEPTION tt)
  | "eight"%string => SomeErr (BEIGHT tt)
  | "deroute"%string => SomeErr (BDEROUTE tt)
  | "define"%string => SomeErr (BDEFINE tt)
  | "corner"%string => SomeErr (BCORNER tt)
  | "configure"%string => SomeErr (BCONFIGURE tt)
  | "circle"%string => SomeErr (BCIRCLE tt)
  | "call_once"%string => SomeErr (BCALL_ONCE tt)
  | "call"%string => SomeErr (BCALL tt)
  | "blocks"%string => SomeErr (BBLOCKS tt)
  | "block"%string => SomeErr (BBLOCK tt)
  | "variable"%string => SomeErr (BVARIABLE tt)
  | "attitude"%string => SomeErr (BATTITUDE tt)
  | "arg"%string => SomeErr (BARG tt)
  | "abi_binding"%string => SomeErr (BABI_BINDING tt)
  | "approaching_time"%string => SomeErr (APPRTIME tt)
  | "alt_unit_coef"%string => SomeErr (ALT_UNIT_COEF tt)
  | "alt_unit"%string => SomeErr (ALT_UNIT tt)
  | "alt"%string => SomeErr (ALT tt)
  | "ac_id"%string => SomeErr (AC_ID tt)
  | EmptyString => ErrorErr "emptystring"
  | _ => ErrorErr s
  end.

Fixpoint lex_string_cpt n s :=
  match n with
  | 0 => SomeErr TheEnd
  | S n =>
    match s with
    | EmptyString => SomeErr TheEnd
    | String c s' =>
      match c with
      | " " => lex_string_cpt n s'
      | "009" => lex_string_cpt n s' (* \t *)
      | "010" => lex_string_cpt n s' (* \n *)
      | "013" => lex_string_cpt n s' (* \r *)
      | "=" => erropt_map (Buf_cons (EQ tt)) (lex_string_cpt n s')
      | "<" => 
        match s' with
        | String "/" s'' => erropt_map (Buf_cons (SCB tt))
                                       (lex_string_cpt n s'')
        | String "!" (String "-" (String "-" s'')) => 
              let k := Comment_size s'' in
              let s := ntail k s'' in
              lex_string_cpt n s
        | _ => erropt_map (Buf_cons (SB tt)) 
                          (lex_string_cpt n s')
        end
      | "/" => 
        match s' with
        | String ">" s'' => erropt_map (Buf_cons (EEB tt))
                                       (lex_string_cpt n s'')
        | _ => ErrorErr "single '/'"%string
        end
      | ">" => erropt_map (Buf_cons (EB tt)) (lex_string_cpt n s')
      | _ => (* string data *)
        if (c =? Data_limiter) then
          let k := Data_size s' in
          let data := substring 0 k s' in
          let s'' := ntail (S k) s' in
          erropt_map (Buf_cons (Data data)) (lex_string_cpt n s'')
        else
          let k := identsize s in
          let id := substring 0 k s in
          let s := ntail k s in
          match (Dictionary id) with
          | SomeErr (BHEADER tt) => 
                        match s with
                        | String ">" _ =>
                        let m := find_string s "</header>"%string in
                        let header := substring 1 (m-1) s in
                        let s := ntail (m + 8) s in
                        erropt_map (fun x => Buf_cons (BHEADER tt)
                                             (Buf_cons (EB tt)
                                             (Buf_cons (Data header) 
                                             (Buf_cons (SCB tt)
                                             (Buf_cons (BHEADER tt)
                                             x)))))
                                   (lex_string_cpt n s)
                        | _ => erropt_map (Buf_cons (BHEADER tt))
                                          (lex_string_cpt n s)
                        end
          | SomeErr tk => erropt_map (Buf_cons tk) (lex_string_cpt n s)
          | ErrorErr msg => ErrorErr msg
          end
      end
    end
  end.

Definition lex_string s := 
  let tks :=
    let n := find_string s "<flight_plan" in
    if Nat.eqb n (length s) then 
      let n := find_string s "<procedure" in
      let s := ntail n s in
      lex_string_cpt (length s) s
    else
      let s := ntail n s in
      lex_string_cpt (length s) s
     in
  match tks with
  | SomeErr v => Some v
  | ErrorErr msg => Some (Buf_cons (Errormsg msg) TheEnd)
  end.


