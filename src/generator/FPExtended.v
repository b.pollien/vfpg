From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List Program.Equality Program.Wf
                        BinNums BinaryString FunInd Program
                        Classes.RelationClasses Program.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

From VFP Require Import CommonLemmas
                        BasicTypes 
                        FPNavigationMode
                        FlightPlanGeneric
                        FlightPlan
                        FlightPlanExtended.
Import FP FP_E_WF.

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Import ListNotations.

Set Implicit Arguments.


(** * Equivalence relation between fpe_stages *)

(** Definition of equivalence relation between fpe stage *)
(** The equivalence ignore the numbering of the stages *)
Inductive equiv_fpe_stage: FP_E.fp_stage -> FP_E.fp_stage -> Prop :=
| equiv_fpe_while: forall ids ids' p p',
  get_while_cond p = get_while_cond p'
  -> get_end_while_id p - (get_while_id p + 1)
      = get_end_while_id p' - (get_while_id p' + 1)
  -> equiv_fpe_stage (FP_E_WF.WHILE ids p) (FP_E.WHILE ids' p')
| equiv_fpe_end_while: forall ids ids' p p' b b',
  get_while_cond p = get_while_cond p'
  -> equiv_fpe_stages b b'
  -> equiv_fpe_stage (FP_E.END_WHILE ids p b) (FP_E.END_WHILE ids' p' b')
| equiv_fpe_set: forall ids ids' p,
  equiv_fpe_stage (FP_E.SET ids p) (FP_E.SET ids' p)
| equiv_fpe_call: forall ids ids' p,
  equiv_fpe_stage (FP_E.CALL ids p) (FP_E.CALL ids' p)
| equiv_fpe_deroute: forall ids ids' p,
  equiv_fpe_stage (FP_E.DEROUTE ids p) (FP_E.DEROUTE ids' p)
| equiv_fpe_return: forall ids ids' p,
  equiv_fpe_stage (FP_E.RETURN ids p) (FP_E.RETURN ids' p)
| equiv_fpe_nav_init: forall ids ids' mode,
  equiv_fpe_stage (FP_E.NAV_INIT ids mode) (FP_E.NAV_INIT ids' mode)
| equiv_fpe_nav: forall ids ids' mode u,
  equiv_fpe_stage (FP_E.NAV ids mode u) (FP_E.NAV ids' mode u)
| equiv_fpe_default: forall ids ids',
  equiv_fpe_stage (FP_E.DEFAULT ids) (FP_E.DEFAULT ids')
with  equiv_fpe_stages:
  list FP_E.fp_stage -> list FP_E.fp_stage -> Prop :=
| equiv_fpe_nil:
  equiv_fpe_stages [] []
| equiv_fpe_cons: forall s s' stages stages',
  equiv_fpe_stage s s'
  -> equiv_fpe_stages stages stages'
  -> equiv_fpe_stages (s :: stages) (s' :: stages').

Ltac simpl_equiv_fpe :=
  try apply equiv_fpe_cons;
  try apply equiv_fpe_nil;
  try apply equiv_fpe_while;
  try apply equiv_fpe_end_while;
  try apply equiv_fpe_set;
  try apply equiv_fpe_call;
  try apply equiv_fpe_deroute;
  try apply equiv_fpe_return;
  try apply equiv_fpe_nav_init;
  try apply equiv_fpe_nav;
  try apply equiv_fpe_default.

(** ** Proof that equiv_fp_stage is a equivalence relation *)
Lemma equiv_fpe_stage_refl: Reflexive equiv_fpe_stage.
Proof.
  move => s; induction s using fp_stage_ind'; simpl_equiv_fpe; try by [].
  induction block; try apply Forall_cons_iff in H;
    try destruct H as [Ha Hblock]; simpl_equiv_fpe; try by [].
  by apply IHblock.
Qed.

Lemma equiv_fpe_stages_not:
  forall s stages,
    ~ (equiv_fpe_stages (s :: stages) []).
Proof.
  move => s stages H;
  remember (s :: stages) as stages1; remember [] as stages2.
  destruct H eqn:H'; try by [].
Qed.

Lemma equiv_fpe_stage_sym: Symmetric equiv_fpe_stage.
Proof.
  move => s.
  induction s using fp_stage_ind'; 
  try (move => s' Hs; inversion Hs; simpl_equiv_fpe; try by []).
  generalize b' H5; clear H5 Hs H3.
  induction block; move => b'' Hs'; inversion Hs'.
  - apply equiv_fpe_nil.
  - apply Forall_cons_iff in H; destruct H as [Ha Hblock];
      simpl_equiv_fpe; try by [].
    * by apply Ha.
    * apply IHblock; try by [].
Qed.

Lemma equiv_fpe_stage_trans: Transitive equiv_fpe_stage.
Proof.
  move => s1' s2. generalize s1'.
  induction s2 using fp_stage_ind'; move => s1 s3 Hs1 Hs3;
  inversion Hs1; inversion Hs3; simpl_equiv_fpe.
  transitivity (get_while_cond params); by [].
  by rewrite H3. by rewrite -H10.
  generalize b b'0 H11 H5. clear H9 H11 H5 H4 Hs1 Hs3.
  induction block; move => b1 b2 Hb1 Hb2;
    inversion Hb1; inversion Hb2.
  - apply equiv_fpe_nil.
  - apply Forall_cons_iff in H; destruct H as [Ha Hblock];
      simpl_equiv_fpe; try by [].
    * by apply Ha.
    * apply IHblock; try by [].
  Qed.

#[global]
Instance equiv_fpe_stage_Equiv : Equivalence equiv_fpe_stage.
Proof. split;
  [apply equiv_fpe_stage_refl |
   apply equiv_fpe_stage_sym |
   apply equiv_fpe_stage_trans].
Qed.

(** ** Proof that equiv_fp_stages is a equivalence relation *)
Lemma equiv_fpe_stages_refl: Reflexive equiv_fpe_stages.
Proof.
  move => s; induction s as [| a s IHs]; simpl_equiv_fpe; try by [].
  reflexivity.
Qed.

Lemma equiv_fpe_stages_sym: Symmetric equiv_fpe_stages.
Proof.
  move => s; induction s as [|a s IHs] => [s' Hs | s' Hs];
    inversion Hs; simpl_equiv_fpe.
  - by symmetry.
  - by apply IHs.
Qed.

Lemma equiv_fpe_stages_trans: Transitive equiv_fpe_stages.
Proof.
  move => s1' s2; generalize s1'; induction s2 as [| a s2 IHs2]
    => s1 s3 Hs1 Hs3; inversion Hs1; inversion Hs3; simpl_equiv_fpe.
  - transitivity a; by [].
  - by apply IHs2.
Qed.

#[global]
Instance equiv_fpe_stages_Equiv : Equivalence equiv_fpe_stages.
Proof. split;
  [apply equiv_fpe_stages_refl |
   apply equiv_fpe_stages_sym |
   apply equiv_fpe_stages_trans].
Qed.

Lemma equiv_fpe_stages_app:
  forall s1 s1' s2 s2',
    equiv_fpe_stages s1 s1'
    -> equiv_fpe_stages s2 s2'
    -> equiv_fpe_stages (s1 ++ s2) (s1' ++ s2').
Proof.
  induction s1 as [|a s1 IHs1]; 
  move => [| a' s'] s2 s2' Hs1 Hs2; inversion Hs1.
  - apply Hs2.
  - apply (equiv_fpe_cons H2 (IHs1 s' s2 s2' H4 Hs2)).
Qed.

(** ** Property about stages are being well numbered *)
Inductive well_numbered:
  stage_id -> (list FP_E.fp_stage * stage_id)-> Prop :=
| well_numbered_nil: forall ids, 
  well_numbered ids ([], ids)
| well_numbered_cons: forall ids ids' s stages,
  ids = get_stage_id s
  -> well_numbered (ids + 1) (stages, ids')
  -> well_numbered ids (s :: stages, ids').

(** ** Lemmas about well_numbred *)

Lemma well_numbered_app:
  forall s1 s2 ids ids' ids'',
    well_numbered ids (s1, ids')
    -> well_numbered ids' (s2, ids'')
    -> well_numbered ids (s1 ++ s2, ids'').
Proof.
  induction s1; move => s2 ids ids' ids'' Hw1 Hw2.
  - by inversion Hw1.
  - inversion Hw1. apply well_numbered_cons; try by []. 
    apply IHs1 with (ids' := ids'); try by [].
Qed.

Ltac simpl_well_numbered :=
  try repeat (apply well_numbered_cons; try by []);
  try apply well_numbered_nil.

Lemma well_numbered_length:
  forall stages ids,
    (exists ids', well_numbered ids (stages, ids'))
    -> well_numbered ids (stages, ids + (length stages)).
Proof.
  induction stages as [|s stages IHs] => [ids | ids].
  - rewrite //= -plus_n_O => H. simpl_well_numbered.
  - move => [ids' Hids]. inversion Hids. simpl_well_numbered.
    rewrite //=.
    replace (ids + S (length stages)) with ((ids + 1) + (length stages));
    try lia. apply IHs.
    by exists ids'.
Qed.

Lemma well_numbered_get_stage_id:
    forall stages ids ids' d,
      well_numbered ids (stages, ids')
      -> (forall i, i < length stages
          -> FP_E.get_stage_id (nth i stages d) 
            = i + ids).
Proof.
  induction stages as [| s stages IHs]; 
    move => ids ids' d Hw i Hlt; inversion Hw.
  - rewrite //= in Hlt; inversion Hlt.
  - destruct i as [|i]; rewrite //=.
    replace (S (i + ids)) with (i + (ids + 1)); try lia.
    apply IHs with (ids' := ids'); try by [].
    rewrite //= in Hlt; lia.
Qed.

(** * Extension of stages *)

(** Definition of the function that will convert the flight plan into the extended flight plan *)

Section BLOCK_ID.
  (** Id of the block being extended *)
  Variable idb: block_id.

  (** Type of the function that takes the current stage id and returns  *)
  (** the list of extend stages and the next stage id availlable.       *)
  Definition extended_stages_sig :=
    stage_id -> (list FP_E.fp_stage * stage_id).

  (** Properties that a extended_stages function is well-formed *)
  Definition extended_stages_wf (e: extended_stages_sig) :=
    (forall ids ids', equiv_fpe_stages (fst (e ids)) (fst (e ids')))
    /\ (forall ids, well_numbered ids (e ids)).

  Definition extended_stages :=
    {e: extended_stages_sig | extended_stages_wf e}.

  (** Get the list of FP_E.fp_stage generated by the function [extended] *)(** starting by the current stage id [ids]                             *)
  Definition extract_stages (extended: extended_stages)
                              (ids: stage_id):  list FP_E.fp_stage :=
    fst ((`extended) ids).

  Program Definition extend_nil: extended_stages :=
    fun ids => (nil, ids).
  Next Obligation.
    rewrite /extended_stages_wf //=; split; intros;
    try simpl_equiv_fpe; try simpl_well_numbered.
  Qed.

  (** Appends two extended_stages *)
  Program Definition app_stages (e1 e2: extended_stages):
                                            extended_stages :=
    fun ids =>
      let (stages, ids') := `e1 ids in
      let (stages', ids') := `e2 ids' in 
      (stages ++ stages', ids').
  Next Obligation.
    rewrite /extended_stages_wf;
    destruct e1 as [e1 [He1 Hn1]]. destruct e2 as [e2 [He2 Hn2]];
    rewrite //=; split.
    (** Equiv stages *)
    move => ids ids'.
    destruct (e1 ids) as [s1 ids1] eqn:Hs1;
    destruct (e2 ids1) as [s2 ids2] eqn:Hs2;
    destruct (e1 ids') as [s1' ids1'] eqn:Hs1';
    destruct (e2 ids1') as [s2' ids2'] eqn:Hs2'; rewrite //=.
    have He1' := He1 ids ids'; rewrite Hs1 Hs1' //= in He1'.
    have He2' := He2 ids1 ids1'; rewrite Hs2 Hs2' //= in He2'.
    apply (equiv_fpe_stages_app He1' He2').

    (** Well numbered *)
    move => ids.
    destruct (e1 ids) as [s1 ids1] eqn:Hs1;
    destruct (e2 ids1) as [s2 ids2] eqn:Hs2.
    rewrite //=. apply well_numbered_app with (ids':= ids1); try by [].
    - by rewrite -Hs1.
    - by rewrite -Hs2.
  Qed.

  Notation "e1 >> e2" := (app_stages e1 e2) (at level 10).

  (** ** Lemmas about extended stages *)

  Lemma extended_stages_length: 
    forall (e: extended_stages) ids1 ids2,
    length (extract_stages e ids1)
      = length (extract_stages e ids2).
  Proof.
    rewrite /extract_stages; move => [e [Heb Hnb]] //= ids1 ids2.
    remember (fst (e ids1)) as stages1.
    remember (fst (e ids2)) as stages2.
    have Heb' := Heb ids1 ids2.
    rewrite -Heqstages1 -Heqstages2 in Heb'.
    clear Heqstages1 Heqstages2.
    generalize stages2 Heb'; clear Heb';
    induction stages1; move => stages2' Heb''; inversion Heb''.
    - by [].
    - by rewrite //= (IHstages1 stages').
  Qed.

  Lemma well_numbered_id:
  forall stages ids ids' ids'',
    well_numbered ids (stages, ids')
    -> well_numbered ids (stages, ids'')
    -> ids' = ids''.
  Proof.
    induction stages as [|s stages IHs] 
      => [ids ids' ids'' H H' | ids ids' ids'' H H'].
    - inversion H; inversion H'. by rewrite -H2 -H4.
    - inversion H; inversion H'.
      by apply IHs with (ids := ids + 1).
  Qed.

  Lemma extended_stages_length_ids:
    forall (e: extended_stages) ids stages ids',
      (`e) ids = (stages, ids')
      -> length stages = ids' - ids.
  Proof.
    move => [e [He Hn]] ids stages ids' Hres.
    have Hn' := Hn ids. rewrite //= in Hres. rewrite //= Hres in Hn'.
    have H: well_numbered ids (stages, ids + (length stages)).
    { apply well_numbered_length. by exists ids'. }
    have H': ids' = ids + length stages.
    { by apply well_numbered_id with (stages := stages) (ids := ids). }
    rewrite H'. lia.
  Qed.

  Lemma extended_stages_well_numbered:
    forall (e: extended_stages) ids stages ids',
      (`e) ids = (stages, ids')
      -> (forall i, i < length stages
          -> FP_E.get_stage_id (nth i stages (DEFAULT ids'))
             = i + ids).
  Proof.
    move => [e [He Hn]] ids stages ids' Hres i Hlt;
    have Hn' := Hn ids; rewrite //= in Hres; rewrite //= Hres in Hn'.
    by apply well_numbered_get_stage_id with (ids' := ids').
  Qed.

  Lemma well_numbered_preserved:
    forall (e: extended_stages) ids1 ids2 ids1' ids2' block1 block2,
      (` e) ids1 = (block1, ids1')
      -> (` e) ids2 = (block2, ids2')
      -> ids1' - ids1 = ids2' - ids2.
  Proof.
    move => e ids1 ids2 ids1' ids2' block1 block2 H1 H2.
    have H1' := H1; apply extended_stages_length_ids in H1;
    have H2' := H2; apply extended_stages_length_ids in H2.
    have He1 : block1 = fst ((` e) ids1). by rewrite H1'.
    have He2 : block2 = fst ((` e) ids2). by rewrite H2'.
    rewrite -H1 -H2 He1 He2. apply extended_stages_length.
  Qed.

  Lemma well_numbered_le:
    forall (e: extended_stages) ids stages ids',
      (` e) ids = (stages, ids')
      -> ids <= ids'.
  Proof.
    move => [e [He Hn]] ids stages ids' Hres.
    have Hn' := Hn ids. rewrite //= in Hres. rewrite //= Hres in Hn'.
    have H: well_numbered ids (stages, ids + (length stages)).
    { apply well_numbered_length. by exists ids'. }
    have H': ids' = ids + length stages.
    { by apply well_numbered_id with (stages := stages) (ids := ids). }
    rewrite H'. lia.
  Qed.

  (** ** Definition of the function that extend the stages *)

  Program Definition extend_while (params: fp_params_while)
                            (block: extended_stages): extended_stages :=
    fun ids =>
      (* Flatten the while loop *)
      let (block', ids') := block (ids + 1) in
      let params_e := mk_end_while idb ids ids' params in
      (((WHILE ids params_e) :: block')
        ++ ((END_WHILE ids' params_e block') :: nil), ids' + 1).
  Next Obligation.
    rewrite /extended_stages_wf;
    destruct block as [block' [Heb Hnb]] eqn:Hb; rewrite //=; split.
    (* Equiv stages *)
    move => ids1 ids2.
    destruct (block' (ids1 + 1)) as [block1' ids1'] eqn:Hb1;
    destruct (block' (ids2 + 1)) as [block2' ids2'] eqn:Hb2.
    rewrite //=; simpl_equiv_fpe; rewrite //=.
    apply well_numbered_preserved
      with (e:= block) (block1 := block1') (block2 := block2')
        (ids1 := ids1 + 1) (ids2 := ids2 + 1); rewrite Hb; try by [].
    apply equiv_fpe_stages_app.
    - have Heb' := Heb (ids1 + 1) (ids2 + 1).
      by rewrite Hb1 Hb2 //= in Heb'.
    - simpl_equiv_fpe. by [].
    - have Heb' := Heb (ids1 + 1) (ids2 + 1).
      by rewrite Hb1 Hb2 in Heb'. 

    (* Well numbered *)
    move => ids.
    destruct (block' (ids + 1)) as [block'' ids'] eqn:Hb';
    remember (mk_end_while idb ids ids' params) as p.
    apply well_numbered_app with (ids' := ids')
                                 (s1 := WHILE ids p :: block'');
                                 simpl_well_numbered.
    by rewrite -Hb'.
  Qed.

  Program Definition extend_nav_init (mode: fp_navigation_mode)
                              (until: option c_cond): extended_stages :=
    fun ids =>
      (* Adding NAV_INIT stage*)
      ((NAV_INIT ids mode) 
      :: (NAV (ids + 1) mode until) 
      :: nil, ids + 2).
  Next Obligation.
    rewrite /extended_stages_wf //=; split.

    (* Equiv stages *)
    move => ids ids'.
    apply (equiv_fpe_cons (equiv_fpe_nav_init ids ids' mode)).
    apply (equiv_fpe_cons (equiv_fpe_nav (ids + 1) (ids' + 1) mode until)).
    apply equiv_fpe_nil.

    (* Well numbered *)
    move => ids. simpl_well_numbered.
    have H : ids + 1 + 1 = ids + 2. lia.
    rewrite H; apply well_numbered_nil.
  Qed.

  Program Definition extend_set (params: fp_params_set): extended_stages :=
    fun ids => ([FP_E.SET ids params], ids + 1).
  Next Obligation.
    rewrite /extended_stages_wf //=; split.
    move => ids ids'; simpl_equiv_fpe.
    move => ids; simpl_well_numbered.
  Qed.

  Program Definition extend_call (params: fp_params_call): extended_stages :=
    fun ids => ([FP_E.CALL ids params], ids + 1).
  Next Obligation.
    rewrite /extended_stages_wf //=; split.
    move => ids ids'; simpl_equiv_fpe.
    move => ids; simpl_well_numbered.
  Qed.

  Program Definition extend_deroute (params: fp_params_deroute): extended_stages :=
    fun ids => ([FP_E.DEROUTE ids params], ids + 1).
  Next Obligation.
    rewrite /extended_stages_wf //=; split.
    move => ids ids'; simpl_equiv_fpe.
    move => ids; simpl_well_numbered.
  Qed.

  Program Definition extend_return (params: fp_params_return): extended_stages :=
    fun ids => ([FP_E.RETURN ids params], ids + 1).
  Next Obligation.
    rewrite /extended_stages_wf //=; split.
    move => ids ids'; simpl_equiv_fpe.
    move => ids; simpl_well_numbered.
  Qed.

  Program Definition extend_nav (nav_mode: fp_navigation_mode)
                          (until: option c_cond): extended_stages :=
    fun ids => ([FP_E.NAV ids nav_mode until], ids + 1).
  Next Obligation.
    rewrite /extended_stages_wf //=; split.
    move => ids ids'; simpl_equiv_fpe.
    move => ids; simpl_well_numbered.
  Qed.

  Program Definition extend_default: extended_stages :=
    fun ids => ([DEFAULT ids], ids + 1).
  Next Obligation.
    rewrite /extended_stages_wf //=; split; intros;
    try simpl_equiv_fpe; try simpl_well_numbered.
  Qed.

  (** Type of the function that converts a stage into an extended_stages *)
  Definition extend_stage_fun := FP.fp_stage -> extended_stages.

  (** Function that takes a extend_stage_fun, the list of stage to *)
  (** extend and a extend_list for the stage already extended. The *)
  (** function generates the extension of the stages passed in     *)
  (** parameters.                                                  *)
  Definition extend_list_stages (extend_stage: extend_stage_fun) 
                                (next_stages: extended_stages)
                                (stages: list FP.fp_stage):
                                extended_stages :=
    fold_right (fun s n => (extend_stage s) >> n) next_stages stages.

  (** Implementation of a function with extend_stage_fun type *)
  Program Fixpoint extend_stage (stage: FP.fp_stage): extended_stages :=
    match stage with
    | (FP.WHILE params block)  =>
      (* Flatten the while loop *)
      let block' := extend_list_stages extend_stage extend_nil block in
      extend_while params block'
    | (FP.NAV mode until true) => extend_nav_init mode until
    | (FP.NAV mode until false) => extend_nav mode until
    | (FP.SET params) => extend_set params
    | (FP.CALL params) => extend_call params
    | (FP.DEROUTE params) => extend_deroute params
    | (FP.RETURN params) => extend_return params
    end.

  (** ** Expension of stages *)
  (** Return the list of extended stages without the default stage *)
  Definition extend_stages (stages: list FP.fp_stage):
                                    list FP_E.fp_stage :=
    let extend_stages :=
      extend_list_stages extend_stage extend_nil stages
    in
      extract_stages extend_stages 0.

  (** Return the list of extended stages with the default stage *)
  Definition extend_stages_default (stages: list FP.fp_stage):
                                    list FP_E.fp_stage :=
    let extend_stages :=
      extend_list_stages extend_stage extend_default stages
    in
      extract_stages extend_stages 0.

  Lemma extend_stage_wf_hyp:
    forall s,
      extended_stages_wf (` (extend_stage s)).
  Proof.
    move => s. by destruct (extend_stage s) as [s' Hs].
  Qed.

  Lemma extend_list_stages_wf:
    forall es block,
      extended_stages_wf (` (extend_list_stages extend_stage es block)).
  Proof.
    move => es block.
    by destruct (extend_list_stages extend_stage es block).
  Qed.


  (** ** Lemmas for the property wf_while *)


  (** Offset correspond the first stage_id of the list *)
  Definition prop_wf_while (offset:stage_id) (stages: list FP_E.fp_stage): Prop :=
    forall d ids ids' params,
      (exists n, d = DEFAULT n)
      -> nth ids stages d = WHILE ids' params
      -> ids + offset = get_while_id params
        /\ (get_end_while_id params) > ids + offset
        /\ nth ((get_end_while_id params) - offset) stages d
            = END_WHILE (get_end_while_id params) params 
              (subseq stages ((get_while_id params) - offset)
                                  ((get_end_while_id params) - offset))
        /\ ids + offset = ids'
        /\ (get_end_while_id params) - offset < length stages
        /\ idb = (get_while_block_id params).

  Lemma extend_stages_prop_wf_while_generic:
    forall stages,
      Forall 
        (fun stage => forall ids0,
          prop_wf_while ids0 (extract_stages (extend_stage stage) ids0))
        stages
    -> forall ids,
    prop_wf_while ids
      (extract_stages
        (extend_list_stages extend_stage extend_nil stages) ids).
  Proof.
    induction stages as [|s stages IHs]; move => Hstages;
    rewrite /extend_stages /extend_list_stages
            /extract_stages /prop_wf_while
      //= => ids0 d ids ids' params Hd; try by [].
    - destruct Hd as [n Hd].
      destruct ids => Hnth; by rewrite Hnth in Hd.
    - destruct ((` (extend_stage s)) ids0) as [stages1 ids1] eqn:Hs.
      replace (fold_right _ _ stages) with
        (extend_list_stages extend_stage extend_nil stages); try by [].
      destruct ((` (extend_list_stages _ extend_nil _)) ids1)
        as [stages2 ids2] eqn:Hn => //= Hnth.

      (* Split the 2 cases *)
      destruct (Nat.le_gt_cases (length stages1) ids) as [Hge | Hlt].

      (* The while is in the second part *)
      * rewrite app_nth2 in Hnth; try lia.
        remember (ids - Datatypes.length stages1) as ids3.
        apply Forall_inv_tail in Hstages.
        have IHs' := IHs Hstages ids1; rewrite /prop_wf_while in IHs'.

        replace stages2 with
          (extract_stages
            (extend_list_stages extend_stage extend_nil stages)
              ids1) in Hnth;
          try by rewrite /extract_stages Hn.
        apply IHs' with (ids := ids3) (ids' := ids') (params := params)
          in Hd; try by [].

        have Hids1: ids1 = ids0 + length stages1.
        { have Hs' := Hs.
          apply extended_stages_length_ids in Hs.
          apply well_numbered_le in Hs'; lia. }

        destruct Hd as [Hlw [Hlt' [Hend [Hid Hl]]]];
          repeat (split; try by []; try lia).
        + rewrite app_nth2; try lia.
          replace (get_end_while_id params - ids0 - length stages1)
                with (get_end_while_id params - ids1);
                try (rewrite Hids1; lia).
          rewrite /extract_stages Hn //= in Hend.
          rewrite Hend Hids1. f_equal.
          rewrite subseq_app2; try lia.
          by repeat (rewrite Nat.sub_add_distr; try lia).
        + rewrite app_length; rewrite /extract_stages Hn //= in Hl; lia.
      (* The while is in the first part *)
      * rewrite app_nth1 in Hnth; try lia.
        replace stages1 
          with (extract_stages (extend_stage s) ids0) in Hnth;
          try by rewrite /extract_stages Hs.
        apply Forall_inv in Hstages.
        have Hprop := Hstages ids0 d ids ids' params Hd Hnth.

        destruct Hprop as [Hlw [Hlt' [Hend [Hid [Hl Hidb]]]]];
          repeat (split; try by []);
          rewrite /extract_stages Hs //= in Hl; last first.
          rewrite app_length; lia.

        rewrite /extract_stages Hs //= in Hend.
        rewrite app_nth1; try lia.
        rewrite Hend; f_equal; apply subseq_app1; try lia.
  Qed.

  Lemma extend_stage_prop_wf_while:
    forall stage ids0,
      prop_wf_while ids0 (extract_stages (extend_stage stage) ids0).
  Proof.
    induction stage using FP.fp_stage_ind';
      rewrite //= => ids0 d ids ids' params' Hd Hnth.

    (* While proof *)
    rewrite /extract_stages //= in Hnth.
    destruct (` (extend_list_stages _ _ block)) as [block' ids''] eqn:Hex.

    have Hlt_ids: ids'' > ids0. { apply well_numbered_le in Hex; lia. }

    rewrite //= in Hnth; destruct ids as [|ids1] eqn:Hids.


    (* ids = 0 -> WHILE is the WHILE extended *)
    injection Hnth as Hids' Hparams'; rewrite -Hparams' Hids' //=;
      rewrite /extract_stages /extend_while //= -Hids' Hex //=.
    repeat (rewrite //=; split; try by []).
    - have Hex':= Hex; apply extended_stages_length_ids in Hex.
      destruct (ids'' - ids0) eqn:Hids0; try lia.
      replace n with (length block'); try lia.
      by rewrite nth_middle Nat.sub_diag /subseq //=
                 drop_0 Nat.sub_0_r take_length.
    - apply extended_stages_length_ids in Hex.
      rewrite app_length Hex //=. lia.

    (* ids != 0 -> WHILE is in the block *)
    apply extend_stages_prop_wf_while_generic with (ids:=ids0 + 1) in H.
    have Hnth' := Hnth. apply nth_remove_app in Hnth';
      try (destruct Hd as [n' Hd]; by rewrite Hd);
      try by [].
    rewrite Hnth' in Hnth; clear Hnth'.
    rewrite /prop_wf_while /extract_stages Hex //= in H.
    apply (H d ids1 ids' params' Hd) in Hnth.
    destruct Hnth as [Hlw [Hlt [Hnth [Hids' Hlew]]]].
    repeat (split; try lia);
      rewrite /extract_stages /extend_while //= Hex //=.
    - destruct (get_end_while_id params' - ids0) eqn:Hdec; try lia.
      replace n with (get_end_while_id params' - (ids0 + 1)); try lia.
      remember (END_WHILE ids'' _ _) as end_while.
      have Hnth' := Hnth; apply nth_remove_app'
          with (a := end_while) in Hnth'; try by [].
      * rewrite Hnth' Hnth /Hdec /=; f_equal.
        rewrite subseq_cons; try lia.
        rewrite -subseq_app1; try lia; try by [].
        f_equal; try lia.
      * rewrite Heqend_while => Heq. inversion Heq.
        apply extended_stages_length_ids in Hex; lia.
      * destruct Hd as [n' Hd]; by rewrite Hd.
    - rewrite app_length. lia.

    all: try destruct init.
    all: try (destruct ids; rewrite //= in Hnth; destruct ids;
              destruct Hd as [n Hd]; by rewrite Hd in Hnth).
    rewrite //= in Hnth; do 3 (destruct ids; try by []).
    all: destruct Hd as [n Hd]; by rewrite Hd in Hnth.
  Qed.


  Lemma extend_stages_prop_wf_while_general:
      forall stages ids,
      prop_wf_while ids
        (extract_stages
          (extend_list_stages extend_stage extend_nil stages) ids).
  Proof.
    move => stages.
    apply extend_stages_prop_wf_while_generic.
    apply forall_to_Forall.
    apply extend_stage_prop_wf_while.
  Qed.

  Lemma extend_stages_prop_wf_while:
    forall stages,
      prop_wf_while 0 (extend_stages stages).
  Proof.
    rewrite /extend_stages => stages.
    apply extend_stages_prop_wf_while_general.
  Qed.

  (** ** Lemmas for the property wf_end_while *)

  (** Offset correspond the first stage_id of the list *)
  Definition prop_wf_end_while (offset:stage_id)
                                (stages: list FP_E.fp_stage): Prop :=
    forall d ids ids' params block,
      (exists n, d = DEFAULT n)
      -> nth ids stages d = END_WHILE ids' params block
      -> ids + offset = get_end_while_id params
        /\ get_while_id params < get_end_while_id params
        /\ get_while_id params >= offset
        /\ nth ((get_while_id params) - offset) stages d
            = WHILE (get_while_id params) params
        /\ ids + offset = ids'
        /\ idb = (get_while_block_id params).

  Lemma extend_stages_prop_wf_end_while_generic:
    forall stages,
      Forall 
        (fun stage => forall ids0,
        prop_wf_end_while ids0 (extract_stages (extend_stage stage) ids0))
        stages
    -> forall ids,
    prop_wf_end_while ids
      (extract_stages
        (extend_list_stages extend_stage extend_nil stages) ids).
  Proof.
    induction stages as [|s stages IHs]; move => Hstages;
    rewrite /extend_stages /extend_list_stages
            /extract_stages /prop_wf_end_while
      //= => ids0 d ids ids' params block Hd; try by [].
    - destruct Hd as [n Hd].
      destruct ids => Hnth; by rewrite Hnth in Hd.
    - destruct ((` (extend_stage s)) ids0) as [stages1 ids1] eqn:Hs.
      replace (fold_right _ _ stages) with
        (extend_list_stages extend_stage extend_nil stages); try by [].
      destruct ((` (extend_list_stages _ extend_nil _)) ids1)
        as [stages2 ids2] eqn:Hn => //= Hnth.

      (* Split the 2 cases *)
      destruct (Nat.le_gt_cases (length stages1) ids) as [Hge | Hlt].

      (* The while is in the second part *)
      * rewrite app_nth2 in Hnth; try lia.
        remember (ids - Datatypes.length stages1) as ids3.
        apply Forall_inv_tail in Hstages.
        have IHs' := IHs Hstages ids1;
          rewrite /prop_wf_end_while in IHs'.

        replace stages2 with
          (extract_stages
            (extend_list_stages extend_stage extend_nil stages)
              ids1) in Hnth;
          try by rewrite /extract_stages Hn.
        apply IHs' with (ids := ids3) (ids' := ids') (params := params) (block := block)
          in Hd; try by [].

        have Hids1: ids1 = ids0 + length stages1.
        { have Hs' := Hs.
          apply extended_stages_length_ids in Hs.
          apply well_numbered_le in Hs'; lia. }

        destruct Hd as [Hlew [Hlte [Hltp [Hnth' Hids]]]];
          repeat (split; try by []; try lia).
        apply extended_stages_length_ids in Hs.
        rewrite app_nth2; try lia.
        replace (get_while_id params - ids0 - length stages1)
              with (get_while_id params - ids1);
              try (rewrite Hids1; lia).
        rewrite /extract_stages Hn //= in Hnth'.

      (* The while is in the first part *)
      * rewrite app_nth1 in Hnth; try lia.
        replace stages1 
          with (extract_stages (extend_stage s) ids0) in Hnth;
          try by rewrite /extract_stages Hs.
        apply Forall_inv in Hstages.
        have Hprop := Hstages ids0 d ids ids' params block Hd Hnth.

        destruct Hprop as [Hlew [Hlte [Hltp [Hnth' Hids]]]];
          repeat (split; try by []; try lia).
        rewrite /extract_stages Hs //= in Hnth'.
        rewrite app_nth1; try lia.
        rewrite Hnth'; f_equal; apply subseq_app1; try lia.
  Qed.

  Lemma extend_stage_prop_wf_end_while:
    forall stage ids0,
      prop_wf_end_while ids0 (extract_stages (extend_stage stage) ids0).
  Proof.
    induction stage using FP.fp_stage_ind';
      rewrite //= => ids0 d ids ids' params' block' Hd Hnth.

    (* While proof *)
    rewrite /extract_stages //= in Hnth.
    destruct (` (extend_list_stages _ _ block)) as [block'' ids''] eqn:Hex.

    have Hlt_ids: ids'' > ids0. { apply well_numbered_le in Hex; lia. }

    have Hnth_all := Hnth.
    rewrite //= in Hnth. destruct ids as [|ids1] eqn:Hids.

    (* ids = 0 -> NOT POSSIBLE *)
    by [].

    (* ids != 0 -> WHILE is in the block *)
    have Hd' := Hd; destruct Hd as [n Hd].
    apply extend_stages_prop_wf_end_while_generic
      with (ids:=ids0 + 1) in H.
    have H' := Hnth. apply nth_app_split in H'; try by rewrite Hd.
    destruct H' as [Hend | Hmiddle].

    (* End while at the end *)
    - rewrite app_nth2 Hend in Hnth; try lia.
      rewrite Nat.sub_diag //= in Hnth.
      injection Hnth as Hids' Hparams Hblock.
      rewrite -Hparams Hids' /extract_stages /extend_while //= Hex //=
              Nat.sub_diag Hids'.
      apply extended_stages_length_ids in Hex.
      repeat (split; try lia).

    (* End while in the middle *)
    - rewrite /prop_wf_end_while /extract_stages Hex //= in H.
      rewrite app_nth1 in Hnth; try lia.
      apply (H d ids1 ids' params' block' Hd') in Hnth.
      destruct Hnth as [Hlew [Hlte [Hltp [Hnth' Hids']]]].
        repeat (split; try lia).
    rewrite /extract_stages /extend_while //= Hex //=.
    destruct (get_while_id params' - ids0) eqn:Hdec; try lia.
    replace n0 with (get_while_id params' - (ids0 + 1)); try lia.
    by rewrite app_nth1; try lia.

    all: try destruct init.
    all: try (destruct ids; rewrite //= in Hnth; destruct ids;
              destruct Hd as [n Hd]; by rewrite Hd in Hnth).
    rewrite //= in Hnth; do 3 (destruct ids; try by []).
    all: destruct Hd as [n Hd]; by rewrite Hd in Hnth.
  Qed.

  Lemma extend_stages_prop_wf_end_while_general:
      forall stages ids,
      prop_wf_end_while ids
        (extract_stages
          (extend_list_stages extend_stage extend_nil stages) ids).
  Proof.
    move => stages.
    apply extend_stages_prop_wf_end_while_generic.
    apply forall_to_Forall.
    apply extend_stage_prop_wf_end_while.
  Qed.

  Lemma extend_stages_prop_wf_end_while:
    forall stages,
      prop_wf_end_while 0 (extend_stages stages).
  Proof.
    rewrite /extend_stages => stages.
    apply extend_stages_prop_wf_end_while_general.
  Qed.

  (** ** Lemmas for the property wf_default_last *)

  Lemma extend_stages_app_default_general:
    forall stages ids,
      fst ((` (extend_list_stages extend_stage extend_default stages)) ids)
      = fst ((` (extend_list_stages extend_stage extend_nil stages)) ids)
        ++ [DEFAULT (snd ((` (extend_list_stages extend_stage
                                        extend_nil stages)) ids)) ].
  Proof.
    induction stages as [|s stages IHs]; move => //= ids.
    destruct ((` (extend_stage s)) ids) as [stages' ids'] eqn:Hes.
    destruct ((` (extend_list_stages _ extend_default _)) ids') eqn:Hd.
    destruct ((` (extend_list_stages _ extend_nil _)) ids') eqn:Hn.
    have IHs' := IHs ids'. 
    rewrite Hd Hn //= in IHs'. by rewrite IHs' //= app_assoc.
  Qed.

  Lemma extend_stages_app_default:
    forall stages,
      extend_stages_default stages 
        = (extend_stages stages) 
          ++ [DEFAULT (length (extend_stages stages))].
  Proof.
    rewrite /extend_stages_default 
            /extend_stages
            /extract_stages => stages.
    destruct ((` (extend_list_stages _ extend_nil _)) 0)
      as [stages_e ids]eqn:Hd.

    have Hs := extend_list_stages_wf extend_nil stages.
    rewrite /extended_stages_wf in Hs; destruct Hs as [_ Hw].
    have Hlength : 
      well_numbered 0 (stages_e, 0 + Datatypes.length stages_e).
    { apply well_numbered_length. exists ids. by rewrite -Hd. } 
    have Hw0 := Hw 0; rewrite Hd in Hw0.
    apply (well_numbered_id Hw0) in Hlength.

    by rewrite extend_stages_app_default_general Hd Hlength.
  Qed.

  (** ** Lemmas for the property wf_no_default *)

  Lemma extend_stage_ids_lt:
    forall s ids stages ids',
    (` (extend_stage s) ids) = (stages, ids')
    -> exists ids'', ids' = ids'' + 1.
  Proof.
    rewrite /extend_stage;
    induction s using FP.fp_stage_ind'; rewrite //= => ids stages ids' Hr.

    (* While *)
    destruct (` (extend_list_stages _ _ _)) as [stages0 ids0] eqn:He in Hr.
    injection Hr as Hs Hids; rewrite -Hids; by exists ids0.

    all: try destruct init; injection Hr as Hs Hids.
    all: try (exists ids; by rewrite -Hids).
    rewrite -Hids; exists (ids + 1); lia.
  Qed.

  Lemma extend_stage_no_default:
    forall stage ids,
      Forall (fun s => forall n, s <> FP_E.DEFAULT n )
            (extract_stages (extend_stage stage) ids).
  Proof.
    rewrite /extract_stages /extend_stage.
    induction stage using FP.fp_stage_ind'; rewrite //= => ids;
      try destruct init; try apply Forall_cons; try apply Forall_nil;
      try by [].
    - remember (` (extend_list_stages _ _ _)) as e;
      destruct e as [block' ids'] eqn:He.
        (* Remove While *)
        apply Forall_cons; try by [].

        (* Remove block' *)
        apply Forall_app; split.

        (* block' *)
        rewrite //= in H.
        replace block'  with (fst (e (ids + 1))); last first.
          by rewrite He.

        rewrite Heqe. clear He Heqe. generalize ids.
        induction block as [|s block IHs] => [ids'' | ids''].
        * rewrite //=.
        * have Hs:= Forall_inv H (ids'' + 1).
          apply Forall_inv_tail in H. rewrite //=.
          destruct (` (_ s )) as [stages0 ids0] eqn:Hs'.
          destruct (` (extend_list_stages _ _ _)) as [stages1 ids1] eqn:Hx.
          apply Forall_app; split; try by [].
          replace stages1 with (fst (stages1, ids1)); rewrite -Hx.
          apply extend_stage_ids_lt in Hs'. destruct Hs' as [ids1' Hs'];
            rewrite Hs'.
          apply IHs; try by []. by rewrite Hx.

    all: apply Forall_cons; try by apply Forall_nil.
    all: by [].
  Qed.

  Lemma extend_stages_no_default_general:
    forall stages ids,
      Forall (fun s => forall n, s <> FP_E.DEFAULT n )
            (extract_stages
              (extend_list_stages extend_stage extend_nil stages) ids).
  Proof.
    induction stages as [|s stages IHs];
      rewrite /extend_stages /extend_list_stages /extract_stages 
        //= => ids; try by [].
    - rewrite /extract_stages /extend_nil //=.
    - destruct ((` (extend_stage s)) ids) as [stages0 ids0] eqn:Hs.
      replace (fold_right _ _ stages) with
        (extend_list_stages extend_stage extend_nil stages); try by [].
      destruct ((` (extend_list_stages _ extend_nil _)) ids0)
        as [stages1 ids1] eqn:Hn.
    rewrite //=. apply Forall_app; split.
    - replace (stages0) with (extract_stages (extend_stage s) ids).
      apply extend_stage_no_default.
      by rewrite /extract_stages Hs.
    - replace (stages1)
      with (extract_stages
        (extend_list_stages extend_stage extend_nil stages) ids0);
        try by [].
      by rewrite /extract_stages Hn.
  Qed.

  Lemma extend_stages_no_default:
    forall stages,
      Forall (fun s => forall n, s <> FP_E.DEFAULT n )
            (extend_stages stages).
  Proof.
    rewrite /extend_stages => stages.
    apply extend_stages_no_default_general.
  Qed.

  (** ** Lemmas about the numbering of the stages *)
  Lemma extend_stages_well_numbered:
    forall ids stages,
      ids < length (extend_stages stages)
      -> FP_E.get_stage_id
          (nth ids (extend_stages stages)
                   (DEFAULT (Datatypes.length (extend_stages stages))))
      = ids.
  Proof.
    rewrite /extend_stages
            /extract_stages
    => ids stages Hlt.
    destruct ((` (extend_list_stages _ _ stages)) 0)
      as [stages_e ids_e] eqn:Hs; have Hs' := Hs.
    apply extended_stages_well_numbered with (i := ids) in Hs;
      try by [].
    apply extended_stages_length_ids in Hs'.
    by rewrite //= Hs' Nat.sub_0_r Hs Nat.add_0_r.
  Qed.

End BLOCK_ID.

(** Expension of a block *)
Definition extend_block (b: FP.fp_block): FP_E.fp_block := 
  mk_fp_block
    (FP.get_block_name b)
    (FP.get_block_id b)
    (extend_stages_default (FP.get_block_id b) (FP.get_block_stages b))
    (FP.get_block_exceptions b)
    (FP.get_block_pre_call b)
    (FP.get_block_post_call b)
    (FP.get_block_on_enter b)
    (FP.get_block_on_exit b).

(** Expension of blocks *)
Definition extend_blocks (b: FP.fp_blocks): FP_E.fp_blocks :=
  map extend_block b.

(** Function that compute the extended flight plan not well-formed *)
Definition extend_flight_plan_not_wf (fp: FP.flight_plan):
                                                    FP_E_WF.flight_plan :=
  mk_flight_plan (FP.get_fp_forbidden_deroutes fp)
                 (FP.get_fp_exceptions fp)
                 (extend_blocks (FP.get_fp_blocks fp)).

(** Lemma about the default block*)
Lemma extend_default_block_id_not_wf:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> FP_E.get_default_block_id fpe
        = FP.get_default_block_id fp.
Proof.
  rewrite /FP.get_default_block_id
          /FP_E.get_default_block_id
          /extend_flight_plan_not_wf
  => fp [fb e bs] Hfp //=; injection Hfp as Hfb He Hb; subst.
  induction (FP.get_fp_blocks fp) as [|b bs IHb]; try by [].
  by rewrite /= IHb.
Qed.

Lemma extend_default_block_not_wf:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> (FP_E.get_default_block fpe)
        = extend_block (FP.get_default_block fp).
Proof.
  rewrite /FP.get_default_block
          /FP_E.get_default_block
          /extend_flight_plan_not_wf
  => fp fpe Hfp /=.
  rewrite (extend_default_block_id_not_wf Hfp).
  destruct fpe as [fb e bs]; injection Hfp as Hfb He Hb; subst.
  by [].
Qed.

(** ** Lemmas about the extension of blocks *)

Lemma extend_block_applied_not_wf:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> forall id,
        FP_E.get_block fpe id = extend_block (FP.get_block fp id).
Proof.
  rewrite /FP.get_block
          /FP_E.get_block
          /extend_flight_plan_not_wf
  => fp fpe Hfp.

  rewrite (extend_default_block_not_wf Hfp).
  destruct fpe as [fb e bs]; injection Hfp as Hfb He Hbs; subst.

  induction (FP.get_fp_blocks fp) as [| b bs IHb];
    move => [|id]; rewrite //=.
Qed.

Lemma extend_stages_applied_not_wf:
  forall fp fpe id b,
    fpe = extend_flight_plan_not_wf fp
    -> b = (FP.get_block fp id)
    -> FP_E.get_stages fpe id
        = (extend_stages_default (FP.get_block_id b)
                                 (FP.get_block_stages b)).
Proof.
  rewrite /FP_E.get_stages => fp fpe id b Hfp Hb.
  rewrite (extend_block_applied_not_wf Hfp id) -Hb.
  by destruct b.
Qed.

Lemma extend_unchange_block_id:
  forall fpe fp id,
    fpe = extend_flight_plan_not_wf fp
    -> FP.get_block_id (FP.get_block fp id)
        = get_block_id (get_block fpe id).
Proof.
  move => fpe fp id Hfp.
  rewrite (extend_block_applied_not_wf Hfp id).
  by destruct (FP.get_block fp id).
Qed.

Lemma extend_wf_while:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> wf_while fpe.
Proof.
  rewrite /wf_while
          /get_stage
          /default_stage
          /default_stage_id => fp fpe Hfp idb ids ids' params.
  remember (FP.get_block fp idb) as b.
  rewrite (extend_stages_applied_not_wf Hfp Heqb)
          extend_stages_app_default
          app_length Nat.add_sub => Hnth.
  remember (DEFAULT _) as d; have Hnth' := Hnth.
  have Hd: exists n, d = DEFAULT n.
  { exists (length
    (extend_stages (FP.get_block_id b) (FP.get_block_stages b))).
    by rewrite Heqd. }
  have Hne : forall id p, d <> WHILE id p.
  { by rewrite Heqd. }
  rewrite (nth_remove_app (Hne ids' params)) 
          /default_stage /default_stage_id /get_stages in Hnth;
    try by []; clear Hnth'.

  (* Get the property *)
  remember (FP.get_block_stages b) as stages.
  have Hprop := extend_stages_prop_wf_while Hd Hnth;
    rewrite /prop_wf_while in Hprop.

  rewrite Nat.sub_0_r -plus_n_O in Hprop.
  destruct Hprop as [Hlw [Hlt [Hend [Hid [Hl Hidb]]]]];
    repeat (split; try by []).

  remember (END_WHILE _ _ _) as end_while.
  have Hne': d <> end_while. { by rewrite Heqd Heqend_while. }
  rewrite (nth_remove_app' Hne')
          /subseq_stages; try by [].
  rewrite (extend_stages_applied_not_wf Hfp Heqb)
          extend_stages_app_default
          Hend Heqend_while
          -Heqstages -Heqd.
  f_equal. rewrite Nat.sub_0_r. apply subseq_app1; try by [].
  rewrite -Hlw; lia.
  by rewrite -Hidb Heqb (extend_unchange_block_id _ Hfp).
Qed.

Lemma extend_wf_end_while:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> wf_end_while fpe.
Proof.
  rewrite /wf_end_while
          /get_stage
          /default_stage
          /default_stage_id => fp fpe Hfp idb ids ids' params block.
  remember (FP.get_block fp idb) as b.
  rewrite (extend_stages_applied_not_wf Hfp Heqb)
    extend_stages_app_default
    app_length Nat.add_sub => Hnth.
  remember (DEFAULT _) as d; have Hnth' := Hnth.
  have Hd: exists n, d = DEFAULT n.
  { exists (length
  (extend_stages (FP.get_block_id b) (FP.get_block_stages b))).
  by rewrite Heqd. }
  have Hne : d <> END_WHILE ids' params block.
  { by rewrite Heqd. } 
  rewrite (nth_remove_app Hne) 
        /default_stage /default_stage_id /get_stages in Hnth;
    try by []; clear Hnth'.

  (* Get the property *)
  remember (FP.get_block_stages b) as stages.
  have Hprop := extend_stages_prop_wf_end_while Hd Hnth;
  rewrite /prop_wf_while in Hprop.

  rewrite Nat.sub_0_r -plus_n_O in Hprop.
  destruct Hprop as [Hlew [Hlte [Hltp [Hnth' [Hids' Hidb]]]]].
  repeat (split; try by []; try lia).
  have Hne' : d <> WHILE (get_while_id params) params.
  { by rewrite Heqd. }
  rewrite (nth_remove_app' Hne'); try by [].
  by rewrite -Hidb Heqb (extend_unchange_block_id _ Hfp).
Qed.

Lemma extend_wf_no_default:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> wf_no_default fpe.
Proof.
  move => fp fpe Hfp.
  rewrite /wf_no_default
          /FP_E.get_stage
          /FP_E.default_stage
          /FP_E.default_stage_id => idb ids.
  remember (FP.get_block fp idb) as b.
  rewrite (extend_stages_applied_not_wf Hfp Heqb)
          extend_stages_app_default
          app_length Nat.add_sub => Hl.
  rewrite app_nth1; try by [].
  remember (extend_stages _ _) as l.
  have Hno := extend_stages_no_default
                (FP.get_block_id b) (FP.get_block_stages b);
    rewrite -Heql in Hno.
  have Hforall := Forall_nth
          (fun s : FP_E.fp_stage => 
            forall n : stage_id, s <> FP_E.DEFAULT n) l.
  destruct Hforall as [Hf _].
  apply Hf; try by [].
Qed.

Lemma extend_wf_default_last:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> wf_default_last fpe.
Proof.
  move => fp fpe Hfp.
  rewrite /wf_default_last
          /FP_E.get_stage 
          /FP_E.default_stage
          /FP_E.default_stage_id => idb.
  remember (FP.get_block fp idb) as b.
  by rewrite (extend_stages_applied_not_wf Hfp Heqb)
             extend_stages_app_default
             app_length Nat.add_sub nth_middle.
Qed.

Lemma extend_wf_stages_gt_0:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> wf_stages_gt_0 fpe.
Proof.
  rewrite /extend_flight_plan_not_wf.
  move => fp fpe Hfp.
  rewrite /wf_default_last
          /FP_E.get_stage 
          /FP_E.default_stage
          /FP_E.default_stage_id => idb.
  remember (FP.get_block fp idb) as b.
  rewrite (extend_stages_applied_not_wf Hfp Heqb)
             extend_stages_app_default
             app_length //=; lia.
Qed.


Lemma extend_wf_numbering:
  forall fp fpe,
    fpe = extend_flight_plan_not_wf fp
    -> wf_numbering fpe.
Proof.
  move => fp fpe Hfp.
  rewrite /wf_numbering
          /FP_E.get_stage
          /FP_E.default_stage
          /FP_E.default_stage_id => idb ids.
  remember (FP.get_block fp idb) as b.
  rewrite (extend_stages_applied_not_wf Hfp Heqb)
              extend_stages_app_default
              app_length Nat.add_sub => Hne.
  remember (length (extend_stages _ _)) as len.
  have Hne' := Hne len.
  remember (DEFAULT len) as d.

  (* Get the property *)
  remember (FP.get_block_stages b) as stages.
  rewrite nth_remove_app_ne; try by [].
  rewrite Heqd Heqlen.

  apply extend_stages_well_numbered.
  by apply nth_app_ne_impl_lt with (d := d).
Qed.

(** Main function for the computation of the well-formed flight plan *)
Program Definition extend_flight_plan (fp: FP.flight_plan): FP_E_WF.flight_plan_wf :=
  extend_flight_plan_not_wf fp.
Next Obligation.
  (** Manage generaly the extended flight plan *)
  remember (extend_flight_plan_not_wf fp) as fpe.
  have Hfp := Heqfpe; rewrite /extend_flight_plan_not_wf in Heqfpe.
  destruct fpe as [fb excpt blocks] eqn:Hfpe;
    rewrite -Hfpe; rewrite -Hfpe in Hfp.
  injection Heqfpe as Hfb Hexcpt Hblocks.

  by apply (create_wf_fp_e (extend_wf_while Hfp)
                                   (extend_wf_end_while Hfp)
                                   (extend_wf_no_default Hfp)
                                   (extend_wf_default_last Hfp)
                                   (extend_wf_stages_gt_0 Hfp)
                                   (extend_wf_numbering Hfp)).

Qed.


(** ** Lemma about the default block*)
Lemma extend_default_block:
  forall fp fpe,
    fpe = extend_flight_plan fp
    -> (FP_E.get_default_block (` fpe))
        = extend_block (FP.get_default_block fp).
Proof.
  rewrite /extend_flight_plan => fp [fpe Hfpe] Hfp.
  injection Hfp. apply extend_default_block_not_wf.
Qed.

(** ** Lemmas about the extension of blocks *)

Lemma extend_block_applied:
  forall fp fpe id,
    fpe = extend_flight_plan fp
    -> (FP_E.get_block (` fpe) id) 
         = extend_block (FP.get_block fp id).
Proof.
  rewrite /extend_flight_plan => fp [fpe Hfpe] id Hfp.
  injection Hfp. move => Hfp'.
  by apply extend_block_applied_not_wf.
Qed.

Lemma extend_stages_applied:
  forall fp fpe id b,
    fpe = extend_flight_plan fp
    -> b = (FP.get_block fp id)
    -> FP_E.get_stages (` fpe) id
        = (extend_stages_default (FP.get_block_id b)
                                 (FP.get_block_stages b)).
Proof.
  rewrite /extend_flight_plan => fp [fpe Hfpe] id b Hfp.
  injection Hfp. apply extend_stages_applied_not_wf.
Qed.

(** ** Lemmas to access to unchanged elements of the flight plan block *)

(** Definition of block unchanged *)
Record FPE_block_unchanged (b: FP.fp_block) (b_e: fp_block) :=
    create_FPE_block_unchanged {
  unchanged_block_name: 
      FP.get_block_name b = get_block_name b_e;
  unchanged_block_id:
      FP.get_block_id b = get_block_id b_e;
  unchanged_block_exceptions:
      FP.get_block_exceptions b = get_block_exceptions b_e;
  unchanged_block_pre_call:
       FP.get_block_pre_call b = get_block_pre_call b_e;
  unchanged_block_post_call:
       FP.get_block_post_call b = get_block_post_call b_e;
  unchanged_block_on_enter:
      FP.get_block_on_enter b = get_block_on_enter b_e;
  unchanged_block_on_exit:
       FP.get_block_on_exit b = get_block_on_exit b_e;
}.

Lemma extend_block_unchanged:
  forall b,
    FPE_block_unchanged b (extend_block b).
Proof. by []. Qed.

Lemma extend_blocks_default_unchanged:
  forall fp fpe,
  fpe = extend_flight_plan fp
  -> FPE_block_unchanged (FP.get_default_block fp)
                         (FP_E.get_default_block (` fpe)).
Proof.
  move => fp fpe Hfp. 
  rewrite (extend_default_block Hfp).
  by apply extend_block_unchanged.
Qed.

Lemma extend_unchanged:
  forall fp fpe id,
    fpe = extend_flight_plan fp
    -> FPE_block_unchanged (FP.get_block fp id)
                           (FP_E.get_block (` fpe) id).
Proof.
  move => fp fpe id Hfp.
  rewrite (extend_block_applied id Hfp).
  by apply extend_block_unchanged.
Qed.

Lemma FPE_unchanged_pre_call:
  forall fp fpe id,
    fpe = extend_flight_plan fp
    -> FP.Def.get_block_pre_call (FP.get_block fp id)
      = FP_E_WF.Def.get_block_pre_call (FP_E.get_block (` fpe) id).
Proof.
  move => fp fpe id He. apply (extend_unchanged id) in He.
  apply (unchanged_block_pre_call He).
Qed.

Lemma FPE_unchanged_post_call:
  forall fp fpe id,
    fpe = extend_flight_plan fp
    -> FP.Def.get_block_post_call (FP.get_block fp id)
        =FP_E_WF.Def.get_block_post_call (FP_E.get_block (` fpe) id).
Proof.
  move => fp fpe id He. apply (extend_unchanged id) in He.
  apply (unchanged_block_post_call He).
Qed.

Lemma eq_on_enter:
  forall fp fpe id,
    fpe = extend_flight_plan fp
    -> FP.on_enter fp id
      = FP_E_WF.on_enter (` fpe) id.
Proof.
  rewrite /FP.on_enter
          /FP_E_WF.on_enter
          /FP.get_code_on_enter
          /get_code_on_enter
          /FP.Def.get_block_on_enter
          /Def.get_block_on_enter 
   => fp fpe id He. apply (extend_unchanged id) in He.
   by rewrite (unchanged_block_on_enter He) /FP_E.get_block.
Qed.

Lemma eq_on_exit:
  forall fp fpe id,
    fpe = extend_flight_plan fp
    -> FP.Common.on_exit fp id
      = FP_E_WF.Common.on_exit (` fpe) id.
Proof.
rewrite /FP.on_exit
        /FP_E_WF.on_exit
        /FP.get_code_on_exit
        /get_code_on_exit
        /FP.Def.get_block_on_exit
        /Def.get_block_on_exit
  => fp fpe id He. apply (extend_unchanged id) in He.
  by rewrite (unchanged_block_on_exit He) /FP_E.get_block.
Qed.

(** Lemmas to access to unchanged exceptions of the flight plan *)

Lemma extend_block_exceptions_unchanged:
  forall b b_e,
    b_e = extend_block b
    -> FP.get_block_exceptions b = FP_E.get_block_exceptions b_e.
Proof.
  move => b b_e H. destruct b, b_e. by injection H.
Qed.

Lemma FPE_get_local_exceptions:
  forall fp fpe id,
    fpe = extend_flight_plan fp
    -> FP.get_block_exceptions (FP.get_block fp id) =
        FP_E.get_block_exceptions (FP_E.get_block (` fpe) id).
Proof.
  move => fp fpe id He. apply (extend_unchanged id) in He.
  apply (unchanged_block_exceptions He).
Qed.

Lemma extend_eq_nb_blocks:
  forall fp fpe,
    fpe = extend_flight_plan fp
    -> FP.get_nb_blocks fp = FP_E_WF.get_nb_blocks (` fpe).
Proof.
  rewrite /extend_flight_plan
          /FP.get_nb_blocks
          /FP_E_WF.get_nb_blocks
          /extend_block
  => fp [fpe Hwf] //= Hfp. inversion Hfp => //=.
  by rewrite List.map_length.
Qed.
