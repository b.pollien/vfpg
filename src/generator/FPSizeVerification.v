From Coq Require Import Arith ZArith Psatz Bool
                        String List Program.Equality Program Program.Wf.

(** Importation of two needed axioms *)
Require Import FunctionalExtensionality.
Require Import ProofIrrelevance.


Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Require Import Relations.
Require Import Recdef.

From VFP Require Import BasicTypes  
                                 FlightPlanGeneric
                                 FlightPlanExtended
                                 ClightGeneration
                                 CommonLemmasNat8 CommonSSRLemmas
                                 CommonStringLemmas FlightPlanSized.
Import FP_E_WF.

Import Clightdefs.ClightNotations.
Local Open Scope clight_scope.
Local Open Scope string_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.
Import ListNotations.
Set Implicit Arguments.

(*************************************************************)
(** File that verified that the blocks are well-numbered,    *)
(** that there is no more than 256 stages and blocks and     *)
(** that all user id's point to user block                   *)
(*************************************************************)

(** * Generic property to verify if a value is representable on 8 bits *)
Definition test_lt_256 (val: nat) (err: err_msg): list err_msg :=
  if 256 <=? val then  [ err ]
  else [ ].

Lemma verif_test_lt_256:
  forall val err,
    test_lt_256 val err = []
    -> is_nat8 val.
Proof.
  rewrite /test_lt_256 /is_nat8 => val err H.
  destruct (_ <=? _) eqn:Hlt.
  - by inversion H.
  - by apply leb_complete_conv.
Qed.

(** * Generic property to verify if a value is a user id *)
Definition test_is_user (fp:flight_plan) (val: nat) (err: err_msg): list err_msg :=
  if (get_nb_blocks fp) - 1 <=? val then  [ err ]
  else [ ].

Lemma verif_test_is_user:
  forall fp val err,
    test_is_user fp val err = []
    -> is_user_id fp val.
Proof.
  rewrite /test_is_user /is_user_id => fp val err H.
  destruct (_ <=? _) eqn:Hlt.
  - by inversion H.
  - by apply leb_complete_conv.
Qed.

(** * Generic property to verify if a value is a correct block id *)
Definition test_correct_block_id (fp: flight_plan)
                                 (val: block_id)
                                 (err: err_msg): list err_msg :=
  if (get_nb_blocks fp) <=? val then [err]
  else [ ].

Lemma verif_test_correct_block_id:
  forall fp val err,
    test_correct_block_id fp val err = []
    -> correct_block_id fp val.
Proof.
  rewrite /test_correct_block_id /correct_block_id => fp val err H.
  destruct (_ <=? _) eqn:Hlt.
  - by inversion H.
  - by apply leb_complete_conv.
Qed.

(** * Verification that there is less than 256 blocks*)

(** Error message when there is to much block. *)
Definition to_much_block: err_msg :=
  ERROR "The flight plan contains more than 255 blocks !". 

Definition test_nb_blocks_lt_256 (fp: flight_plan) : list err_msg := 
  test_lt_256 (get_nb_blocks fp) to_much_block.

(** * Verification of the exceptions *)

(** Error message when there is a exception not well numbered. *)
Definition incorrect_excpt (ex: fp_exception): err_msg :=
  ERROR ("The exception (" ++ (get_expt_cond ex)
               ++ ", " ++ (string_of_nat (get_expt_block_id ex))
               ++ ", " ++ (string_of_optcode (get_expt_exec ex))
               ++ ") is incorrect !").

Definition test_correct_excpt (fp:flight_plan) (ex: fp_exception): list err_msg :=
  (test_is_user fp (get_expt_block_id ex) (incorrect_excpt ex)).

Definition test_correct_excpts (fp:flight_plan) (exs: list fp_exception): list err_msg :=
  fold_right (fun ex res => (test_correct_excpt fp ex) ++ res) [ ] exs.

(** * Verification of the forbidden deroutes *)

(** Error message when there is a forbidden deroute not well numbered. *)
Definition incorrect_fbd_deroute (fbd: fp_forbidden_deroute): err_msg :=
  ERROR ("The forbidden deroute (" ++ (string_of_nat (get_fbd_from fbd))
               ++ ", " ++ (string_of_nat (get_fbd_to fbd))
               ++ ", " ++ (string_of_optcode (get_fbd_only_when fbd))
               ++ ") is incorrect !").

Definition test_correct_fbd_deroute (fp: flight_plan)
                                    (fbd: fp_forbidden_deroute):
                                    list err_msg :=
  (test_is_user fp (get_fbd_from fbd) (incorrect_fbd_deroute fbd))
  ++ (test_is_user fp (get_fbd_to fbd) (incorrect_fbd_deroute fbd)).

Definition test_correct_fbd_deroutes (fp: flight_plan)
                                     (fbds: list fp_forbidden_deroute):
                                                            list err_msg :=
  fold_right (fun fbd res => (test_correct_fbd_deroute fp fbd) ++ res) [ ] fbds.

(** * Verification of the deroutes *)

(** Error message when the deroute has a incorrect block id. *)
Definition incorrect_deroute (params: fp_params_deroute): err_msg :=
  ERROR ("The deroute stage '" ++ (get_deroute_block params)
               ++ "' is incorrect !").

Definition test_incorrect_deroute (fp: flight_plan) (stage: fp_stage): list err_msg :=
  match stage with
  | FP_E.DEROUTE id params => 
    test_is_user fp (get_deroute_block_id params) (incorrect_deroute params)
  | _ => [ ]
  end.

Definition test_stages_deroute (fp: flight_plan) (block: fp_block): list err_msg :=
  fold_right (fun s res => (test_incorrect_deroute fp s) ++ res)
            [] (get_block_stages block).


(** * Verification that blocks are well-numbered, contains less than 256
    stages, and the exception/deroute must be correct *)

(** Error message when a block is not well numbered *)
Definition block_not_well_numbered (block: fp_block)
                                              (ex: block_id): err_msg :=
  ERROR ("The block '" ++ (get_block_name block) 
            ++ "' is not well numbered (has the id "
            ++ (string_of_nat (get_block_id block))
            ++ " but " ++  (string_of_nat ex) ++ " was expected)").

(** Error message when there is to much block. *)
Definition to_much_stage (block: fp_block): err_msg :=
  ERROR ("The block '" ++ (get_block_name block)
               ++ "' contains more than 255 stages !").
  
Definition test_block_well_numbered
                (block: fp_block) (id: block_id): list err_msg :=
  if (get_block_id block) =? id then []
  else [ block_not_well_numbered block id].

Definition test_nb_stage_lt_256 (block: fp_block) : list err_msg :=
  test_lt_256 (length (get_block_stages block)) (to_much_stage block).

Definition test_well_sized_block (fp: flight_plan)
                                        (res: (block_id * list err_msg))
                                        (block: fp_block):
                                        (block_id * list err_msg) :=
  let (id, errs) := res in
  (id + 1, 
    (errs
    ++(test_block_well_numbered block id)
    ++ (test_nb_stage_lt_256 block)
    ++ (test_correct_excpts fp (get_block_exceptions block))
    ++ (test_stages_deroute fp block))%list
  ).

Definition test_well_sized_blocks (fp: flight_plan): list err_msg :=
  snd (fold_left (test_well_sized_block fp) (get_fp_blocks fp) (0, [])).

(** * Analysis of the Flight plan*)

Definition size_analysis (fp: flight_plan_wf) : list err_msg := 
  let fp := proj1_sig fp in
  (test_nb_blocks_lt_256 fp)
  ++ (test_well_sized_blocks fp)
  ++ (test_correct_excpts fp (get_fp_exceptions fp))
  ++ (test_correct_fbd_deroutes fp (get_fp_forbidden_deroutes fp)).

(** * Verification that the functions generate the correct properties *)

(** ** Verification of the exceptions *)

Lemma verif_test_correct_excpt:
  forall exc fp,
    test_correct_excpt fp exc = []
    -> correct_excpt fp exc.
Proof.
  rewrite /test_correct_excpt /correct_excpt => fp exc H.
  apply (verif_test_is_user H).
Qed.

Lemma verif_test_correct_excpts_gen:
  forall fp excs,
    test_correct_excpts fp excs = []
    -> Forall (correct_excpt fp) excs.
Proof.
  move => fp.
  induction excs as [|exc excs IHexcs]; move => H; try by [].
  rewrite /test_correct_excpts //= in H. 
  destruct (app_eq_nil _ _ H) as [Hexc Hexcs].
  
  apply Forall_cons.
  - by apply verif_test_correct_excpt.
  - by apply IHexcs.
Qed.

Lemma verif_test_correct_excpts:
  forall fp,
    test_correct_excpts fp (get_fp_exceptions fp) = []
    -> Forall (correct_excpt fp) (get_fp_exceptions fp).
Proof. move => fp. by apply verif_test_correct_excpts_gen. Qed.

Lemma verif_test_correct_excpts_loc:
  forall fp block,
    test_correct_excpts fp (get_block_exceptions block) = []
    -> Forall (correct_excpt fp) (get_block_exceptions block).
Proof. move => fp block. by apply verif_test_correct_excpts_gen. Qed.

(** ** Verification nb block < 256 *)

Lemma verif_nb_blocks_lt_256:
  forall fp,
    test_nb_blocks_lt_256 fp = []
    -> nb_blocks_lt_256 fp.
Proof.
  rewrite /test_nb_blocks_lt_256 /nb_blocks_lt_256 => fp.
  by apply verif_test_lt_256.
Qed.

(** ** Verification that blocks are well-numbered *)

(** *** Verification of the numbering of the block *)

Lemma verif_block_well_numbered:
  forall block idb,
    test_block_well_numbered block idb = []
    -> get_block_id block = idb.
Proof.
  rewrite /test_block_well_numbered /get_block_id => block idb H.
  destruct block. destruct (_ =? _) eqn:Heq.
  - by apply Nat.eqb_eq.
  - by inversion H.
Qed.

Lemma verif_correct_block_numbered_gen:
  forall fp block idb,
    test_well_sized_block fp (idb, []) block = (S idb, [])
    -> get_block_id block = idb.
Proof.
  rewrite /test_well_sized_block //= => fp block idb H.
  inversion H as [[Hidb He]].

  apply app_eq_nil in He. destruct He as [Hblock He].
  by apply verif_block_well_numbered.
Qed.

(** *** Verification of well_numbered_blocks *)

Lemma fold_right_well_sized_empty:
  forall fp blocks d res,
    snd (fold_left (test_well_sized_block fp) blocks (d, res))
        = []
    -> res = nil.
Proof.
  induction blocks as [|block blocks IHb];
    move => d res H; rewrite //= in H.
  by destruct (app_eq_nil _ _ (IHb _ _ H)).
Qed.

Lemma fold_right_test_well_sized:
  forall fp d block blocks,
    snd (fold_left (test_well_sized_block fp) (block :: blocks) (d, []))
      = []
    -> test_well_sized_block fp (d, []) block =  (S d, [])
        /\ snd (fold_left (test_well_sized_block fp) blocks (S d, []))
              = [].
Proof.
  move => fp d block blocks H.
  rewrite //= in H. have H' := fold_right_well_sized_empty H.
  split; rewrite //=.
  - f_equal; try by []. lia.
  - rewrite H' in H. by replace (S d) with (d + 1) by lia.
Qed.

Lemma verif_well_numbered_blocks_gen:
  forall fp blocks default d,
    snd (fold_left (test_well_sized_block fp) blocks (d, [])) = []
    -> forall i : nat,
        get_block_id default = (d + Datatypes.length blocks)
        -> i < Datatypes.length blocks + 1
        -> get_block_id (nth i blocks default) = (d + i).
Proof.
  induction blocks as [|block blocks IHb];
    move => default d Ht i Hd Hlt.
  - inversion Hlt as [H|n H]; subst.
    * subst. by rewrite /= in Hd.
    * inversion H.
  - destruct (fold_right_test_well_sized Ht) as [Hb Hbs].
    destruct i as [|i]; rewrite //=.
    * rewrite Nat.add_0_r. 
      by apply verif_correct_block_numbered_gen with fp.
    * have Hadd: d + S i = S d  + i by lia.
      rewrite Hadd. apply IHb; try by [].
      + by rewrite /= Nat.add_succ_r in Hd.
      + rewrite //= in Hlt. lia.
Qed.

Lemma verif_well_numbered_blocks:
  forall fp,
    test_well_sized_blocks fp = []
    -> well_numbered_blocks fp.
Proof.
  rewrite /test_well_sized_blocks /get_nb_blocks 
            /get_block => fp Ht i Hlt.
  have H := @verif_well_numbered_blocks_gen fp
                  (get_fp_blocks fp) (get_default_block fp) 0 Ht i.
  rewrite -> Nat.add_0_l in H. by apply H.
Qed.

(** ** Verification that block are well-size *)

(** *** Verification of the number of stages *)

Lemma verif_nb_stage_lt_256 :
  forall block,
    test_nb_stage_lt_256 block = []
    -> is_nat8 (Datatypes.length (get_block_stages block)).
Proof.
  rewrite /test_nb_stage_lt_256 => block.
  by apply verif_test_lt_256.
Qed.

(** *** Verification of the numbering of the block *)

Lemma verif_stage_deroute:
  forall fp ids params ,
    test_incorrect_deroute fp (DEROUTE ids params) = []
    -> is_user_id fp (get_deroute_block_id params).
Proof.
  rewrite /test_incorrect_deroute => fp ids params.
  eapply verif_test_is_user.
Qed.

Lemma verif_stages_deroute:
  forall fp block,
    test_stages_deroute fp block = []
    -> correct_deroutes fp block.
Proof.
  rewrite /test_stages_deroute /correct_deroutes => fp block.

  induction (get_block_stages block) as [|stage stages IHs];
    move => H [|ids] ids' idd params Hnth;
    try destruct (app_eq_nil _ _ H) as [Ht H']; rewrite //= in Hnth;
    try by inversion Hnth.
  - subst stage. by apply (verif_stage_deroute Ht).
  - by apply (IHs H' _ _ _ _ Hnth).
Qed.

(** ***  Verification of well_sized_block *)

Lemma verif_correct_block_gen:
  forall fp block idb,
    test_well_sized_block fp (idb, []) block = (S idb, [])
    -> well_sized_block fp block.
Proof.
  rewrite /test_well_sized_block //= => fp block idb H.
  inversion H as [[Hidb He]].

  apply app_eq_nil in He. destruct He as [Hblock He].
  apply app_eq_nil in He. destruct He as [Hstage He].
  apply app_eq_nil in He. destruct He as [Hexcs Hderoute].

  split.
  - by apply verif_nb_stage_lt_256.
  - by apply verif_test_correct_excpts_loc.
  - by apply verif_stages_deroute.
Qed.

Lemma verif_correct_default_block:
  forall fp, well_sized_block fp (get_default_block fp).
Proof.
  rewrite /get_default_block
          /get_default_block_id
  => fp. split; rewrite //=.
  - rewrite /is_nat8. ssrlia.
  - move => [|[|[|ids]]] ids' idd params Hnth;
    inversion Hnth.
Qed.

Lemma verif_correct_blocks_gen:
  forall fp blocks default d,
    snd (fold_left (test_well_sized_block fp) blocks (d, [])) = []
    -> forall i : nat,
        well_sized_block fp default
        -> i < Datatypes.length blocks + 1
        -> well_sized_block fp (nth i blocks default).
Proof.
  intros fp.
  induction blocks as [|block blocks IHb];
    move => default d Ht i Hd Hlt.
  - inversion Hlt as [H|n H]; subst.
    * subst. by rewrite /= in Hd.
    * inversion H.
  - destruct (fold_right_test_well_sized Ht) as [Hb Hbs].
    destruct i as [|i]; rewrite //=.
    * by apply (verif_correct_block_gen Hb).
    * apply (IHb default (S d) Hbs); try by [].
      to_nat Hlt. ssrlia.
Qed.

Lemma verif_correct_blocks_id:
  forall fp,
    test_well_sized_blocks fp = []
    -> forall i, well_sized_block fp (get_block fp i).
Proof.
  rewrite /test_well_sized_blocks /get_nb_blocks 
            /get_block => fp Ht i.

  destruct (le_lt_dec (Datatypes.length (get_fp_blocks fp) + 1) i)
    as [Hge | Hlt].

  (* Get the default block *)
  rewrite nth_overflow.
  - apply verif_correct_default_block.
  - ssrlia.

  (* Get a block*)
  have H := @verif_correct_blocks_gen fp
                  (get_fp_blocks fp) (get_default_block fp) 0 Ht i.
  apply H.
  - apply verif_correct_default_block.
  - by [].
Qed.

(** ** Verification of the forbidden deroutes *)

Lemma verif_test_fbd_deroute:
  forall fp fbd,
    test_correct_fbd_deroute fp fbd = []
    -> correct_fbd_deroute fp fbd.
Proof.
    rewrite /test_correct_fbd_deroute => fp exc H.
    destruct (app_eq_nil _ _ H) as [Hfrom Hto].

    split.
    - by apply (verif_test_is_user Hfrom).
    - by apply (verif_test_is_user Hto).
Qed.

Lemma verif_test_correct_fbd_deroutes:
  forall fp,
    test_correct_fbd_deroutes fp (get_fp_forbidden_deroutes fp) = []
    -> Forall (correct_fbd_deroute fp) (get_fp_forbidden_deroutes fp).
Proof.
  move => fp. 
  induction (get_fp_forbidden_deroutes fp) as [|fbd fbds IHfbds];
    move => H; try by [].

  rewrite /test_correct_fbd_deroutes //= in H. 
  destruct (app_eq_nil _ _ H) as [Hfbd Hfbds].
  
  apply Forall_cons.
  - by apply verif_test_fbd_deroute.
  - by apply IHfbds.
Qed.

(** ** Global Proofs *)

Theorem fp_size_verified':
  forall fp, size_analysis fp = nil -> verified_fp_e_wf fp.
Proof.
  move => [[fbds excs blocks] Hfp] H.
  apply app_eq_nil in H. destruct H as [Hblock H].
  apply app_eq_nil in H. destruct H as [Hblocks H].
  apply app_eq_nil in H. destruct H as [Hexcs Hfbds].

  split.
  - by apply verif_nb_blocks_lt_256.
  - by apply verif_well_numbered_blocks.
  - by apply verif_correct_blocks_id.
  - by apply verif_test_correct_excpts.
  - by apply verif_test_correct_fbd_deroutes.
Qed.

Definition size_verification (fp: flight_plan_wf): res_size_analysis.
Proof.
  remember (size_analysis fp) as res; symmetry in Heqres.
  destruct res.
  - apply OK. eexists. apply (fp_size_verified' Heqres).
  - apply ERR. exact (e :: res).
Defined.

Lemma irrelevant_analysis_OK:
  forall fpe (Hverified : verified_fp_e_wf fpe),
    (fun f : size_analysis fpe = nil =>
      OK
        (exist
          (fun fpe' : flight_plan_wf =>
            verified_fp_e_wf fpe') fpe (fp_size_verified' f)))
    = (fun f : size_analysis fpe = nil =>
      OK
        (exist
          (fun fpe' : flight_plan_wf =>
            verified_fp_e_wf fpe') fpe Hverified)).
Proof.
  move => fpe Hverified.
  apply functional_extensionality => x.
  f_equal. by apply subset_eq_compat.
Qed.

Lemma irrelevant_analysis_ERR:
  forall fpe e l (Hnverified : size_analysis fpe = e:: l),
    (fun f : size_analysis fpe = nil =>
      OK
        (exist
          (fun fpe' : flight_plan_wf =>
            verified_fp_e_wf fpe') fpe (fp_size_verified' f)))
    = (fun f : size_analysis fpe = nil =>
      ERR (e:: l)).
Proof.
  move => fpe e l Hnverified.
  apply functional_extensionality => H.
  by rewrite H in Hnverified.
Qed.

Lemma size_verification_unchange_fp:
forall fpe fps,
  size_verification fpe = OK fps
  -> fpe = proj1_sig fps.
Proof.
  move => fpe fps Hs.
  destruct (size_analysis fpe) as [| n l] eqn:H.
  - have Hverified := fp_size_verified' H.
    rewrite /size_verification
             (irrelevant_analysis_OK Hverified) H in Hs.
    by inversion Hs.
  - rewrite /size_verification
      (irrelevant_analysis_ERR H) H in Hs.
    by inversion Hs.
Qed.

Lemma correct_deroutes_gen:
  forall fp idb,
    verified_fp_e fp
    -> forall ids ids' params,
        get_stage fp idb ids = DEROUTE ids' params
        -> is_user_id fp (get_deroute_block_id params).
Proof.
  rewrite /get_stage /get_stages /default_stage
    => fp idb Hsize ids ids' params H.
  apply ((get_correct_deroute ((get_well_sized_blocks Hsize) _))
            _ _ _ _ H).
Qed.
