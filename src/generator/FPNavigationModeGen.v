From Coq Require Import Arith ZArith Psatz Bool String List Program.

From VFP Require Import BasicTypes TmpVariables
                        ClightGeneration CommonFPDefinition
                        FPNavigationMode.

From compcert Require Import Coqlib Integers Floats AST Ctypes
                             Cop Clight Clightdefs.
Import Clightdefs.ClightNotations.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope clight_scope.
Local Open Scope dec_uint_scope.
Local Open Scope list_scope.
Local Open Scope string_scope.

(** Definition of the functions to generates C light code *)
(** for the FP modes of Paparazzi.                        *)

(** Function that generates a function call statement    *)
(** Its take the type and the parameters of the function *)
(** and add the nav parameters if it is defined          *)

Definition gen_fun_call_stmt (f_name: ident) (f_type: type)
                                (f_sig_type: typelist)
                                  (params: list expr )
                                    (pc: fp_params_nav_call): statement := 
  match get_nav_nav_params pc with
  | Some p =>
    let nav_params := parse_c_code_float p in
    let f_sig_type := add_last_tl f_sig_type tfloat in
    let f_expr := Evar f_name (Tfunction f_sig_type f_type cc_default) in
    Scall None f_expr (params ++ (nav_params :: nil))
  | None =>
    let f_expr := Evar f_name (Tfunction f_sig_type f_type cc_default) in
    Scall None f_expr params
  end.


(** * Init Code Nav *)

Definition init_code_nav_ident (m: fp_navigation_mode): ident :=
    let code := 
      match m with
      | EIGHT _ _ _ => eight_init_code
      | OVAL _ _ _ => oval_init_code
      | SURVEY_RECTANGLE _ pc => rectangle_init_code pc
      | _ => ""  
      end
    in
    arbitrary_ident code.

(** Generates the statements for the init case *)
Definition gen_fp_nav_init_call (nav_mode: fp_navigation_mode):
                                                        statement :=
  let code := init_code_nav_ident nav_mode in
  match nav_mode with
  | EIGHT ps pm pc
  | OVAL ps pm pc =>
    (gen_call_void_fun code)
  | SURVEY_RECTANGLE ps pc =>
    let wp1 := gen_uint8_const (get_srect_wp1 ps) in
    let wp2 := gen_uint8_const (get_srect_wp2 ps) in
    let grid := parse_c_code_uint8 (get_srect_grid ps) in
    let o := convertOrientation (get_srect_orientation ps) in
    (gen_fun_call_stmt code tvoid
      (Tcons tuchar (Tcons tuchar (Tcons tuchar (Tcons tuchar Tnil))))
        (wp1 :: wp2 :: grid :: o :: nil) pc)
  | _ => Sskip
 end.

Definition gen_fp_nav_init_stmt (nav_mode: fp_navigation_mode):
                                                        statement :=
  (gen_fp_nav_init_call nav_mode)
  @+@ nextStageAndBreak.

(** * Nav cond functions *)

(* Nav cond is of the form:
if (nav_cond) {
  NextStageAndBreakFrom(wp);
}
else {
  NAV_CODE(function)
}
*)

(** Function that return the statement that test the condition *)
(** and write the result in the tmp_NavCond variable if the    *)
(** stage need a nav condition. It return also the wp id for   *)
(** the NextStageAndBreakFrom.                                 *)
Definition tmp_NavCond := create_ident tmp_NavCond_str.

(* Function to generate the expression for the approaching time *)

Definition approaching_time_expr (p: fp_params_go): expr :=
  match get_go_approaching_time p with
  | AT a => parse_c_code_float a
  | ET e => parse_c_code_float ("-" ++ e)
  | CARROT => parse_c_code_float CARROT_str
  end.

Definition nav_cond_stmt (m: fp_navigation_mode):
  option (statement * wp_id) :=
  match m with 
  | GO p _ p_call =>
    let wp := get_go_wp p in
    let wp_expr := gen_uint8_const wp in
    let a_t := approaching_time_expr p in
    let nav := get_nav_type p_call in
    match get_go_from p with
    | Some last =>
      let l_expr := gen_uint8_const last in
      let id :=
        arbitrary_ident (nav ++ "ApproachingFrom")
      in
      let expr_call :=
        Evar id
          (Tfunction (Tcons tuchar (Tcons tuchar (Tcons tfloat Tnil)))
          tbool cc_default)
      in
      Some
      (Scall (Some tmp_NavCond) expr_call (wp_expr :: l_expr :: a_t ::nil),
      wp)
    | None =>
      let id :=
        arbitrary_ident (nav ++ "Approaching")
      in
      let expr_call :=
        Evar id
          (Tfunction (Tcons tuchar (Tcons tfloat Tnil)) tbool cc_default)
      in
      Some (Scall (Some tmp_NavCond) expr_call (wp_expr :: a_t :: nil), wp)
    end
  | _ => None
  end.

(** * Pre/Post call code *)

(** Generates the statement for the pre_call/post_call *)
Definition gen_fp_nav_pre_call (mode: fp_navigation_mode): statement :=
  match get_fp_pnav_call mode with
  | Some pc => parse_c_code_option (get_nav_pre_call pc)
  | None => Sskip
  end.

Definition gen_fp_nav_post_call (mode: fp_navigation_mode): statement :=
  match get_fp_pnav_call mode with
  | Some pc => parse_c_code_option (get_nav_post_call pc)
  | None => Sskip
  end.

(** * Nav Mode*)

(** ** Generates until statement: *)
(** if (cond) {post_call; NextStageAndBreak; }*)
Definition gen_fp_nav_until (nav_mode: fp_navigation_mode)
                            (until: option c_cond): statement :=
  match until with
  | None => Sskip
  | Some cond =>
    Sifthenelse
      (parse_c_code_cond cond)
      ((gen_fp_nav_post_call nav_mode)
      @+@ (nextStageAndBreak))
      (Sskip)
  end.

(** ** Generation of the pitch code *)

(** Generates the statement $(id)(9600*throttle) *)
Definition throttle_call (id: ident)
                            (throttle: throttle): statement :=
  let prod := gen_mul_expr (parse_c_code_float throttle)
                                  (gen_uint8_const (Nat.of_num_uint 9600))
                                  tfloat
  in 
  gen_call_eparams_funf id prod.


(** Generates $(nav)VerticalAutoPitchMode($(throttle)); *)
Definition verticalAutoPitchMode (nav: nav_type) (throttle: throttle) :=
  let id := arbitrary_ident (verticalAutoPitchMode_str nav) in
  throttle_call id throttle.

(** Generates $(nav)VerticalAutoThrottleMode(RadOfDeg($(pitch))); *)
(* ident for the tmp_RadOfDeg variable*)
Definition tmp_RadOfDeg := create_ident tmp_RadOfDeg_str.
(* Expression for the function RadOfDeg*)
Definition RadOfDeg :=
  Evar (arbitrary_ident RadOfDeg_str)
    (Tfunction (Tcons tfloat Tnil) tfloat cc_default).
Definition verticalAutoThrottleMode (nav: nav_type)
                                    (pitch: vpitch): statement :=
  let id :=
    arbitrary_ident (verticalAutoThrottleMode_str nav)
  in
  let expr_call :=
    Evar id (Tfunction (Tcons tfloat Tnil) tvoid cc_default)
  in
  (** tmp_RadOfDeg = RadofDeg(vpitch);*)
  (Scall (Some tmp_RadOfDeg) RadOfDeg
    ((parse_c_code_float pitch) :: nil))
  (** $(nav)VerticalAutoThrottleMode(tmp);*)
  @+@
  (Scall None expr_call ((gen_float_tempvar tmp_RadOfDeg) :: nil)).

Definition gen_fp_pitch (nav: nav_type) (pitch: pitch): statement :=
  match pitch with
  | AUTO t => verticalAutoPitchMode nav t
  | SOME p => verticalAutoThrottleMode nav p
  | NO_PITCH => Sskip
  end.

(** ** Generation of the vmode code *)

(** Generates $(nav)VerticalClimbMode($(c)); *)
Definition verticalClimbMode (nav: nav_type) (c: climb) :=
  let id := arbitrary_ident (verticalClimbMode_str nav) in
  gen_call_eparams_funf id (parse_c_code_float c).

(** Generates $(nav)VerticalAltitudeMode($(a), ?); *)
(* ident for the tmp_AltitudeMode variable*)
Definition tmp_AltitudeMode := create_ident tmp_AltitudeMode_str.
(* Expression for the function Height*)
Definition Height :=
  Evar (arbitrary_ident Height_str) (Tfunction (Tcons tfloat Tnil) tfloat cc_default).
(* Expression for the function WaypointAlt*)
Definition WaypointAlt :=
  Evar (arbitrary_ident WaypointAlt_str)
    (Tfunction (Tcons tuchar Tnil) tfloat cc_default).
Definition verticalAltitudeMode (nav: nav_type) (a: alt) :=
  (* Generates the expression to add in parameters and a statement that
     to initialize de expression *)
  let (expr_params, stmt_init) :=
    match a with
    | ABS a =>
      (parse_c_code_float a, Sskip)
    | REL h =>
      (gen_float_tempvar tmp_AltitudeMode,
      Scall (Some tmp_AltitudeMode) Height
        ((parse_c_code_float h) :: nil))
    | WP wp =>
      (gen_float_tempvar tmp_AltitudeMode,
      Scall (Some tmp_AltitudeMode) WaypointAlt
        ((gen_uint8_const wp) :: nil)
      )
    end
  in
  let id := arbitrary_ident (verticalAltitudeMode_str nav) in
  let expr_call :=
    Evar id (Tfunction (Tcons tfloat (Tcons tfloat Tnil)) tvoid cc_default)
  in
  stmt_init
  @+@
  (Scall None expr_call (expr_params
                          :: gen_0float_const 
                          :: nil)).

(** Generates $(nav)Glide($(l_wp), $(wp)); *)
Definition glide (nav: nav_type) (wp: wp_id) (l_wp: last_wp):= 
  let id := arbitrary_ident (glide_str nav) in
  let expr_call :=
    Evar id (Tfunction (Tcons tuchar (Tcons tuchar Tnil)) tvoid cc_default)
  in
  (Scall None expr_call ((parse_c_code_uint8 l_wp)
                          :: (gen_uint8_const wp) 
                          :: nil)).

(** Generates $(nav)VerticalThrottleMode($(throttle)); *)
Definition verticalThrottleMode (nav: nav_type) (t: throttle) := 
  let id := arbitrary_ident (verticalThrottleMode_str nav) in
  throttle_call id t.

(** Generates statement for the vmode *)
Definition gen_fp_vmode (nav: nav_type) (vmode: vmode): statement :=
  match vmode with
    | CLIMB c => verticalClimbMode nav c
    | ALT a => verticalAltitudeMode nav a
    | XYZ_VM => Sskip
    | GLIDE wp l_wp => glide nav wp l_wp
    | THROTTLE t => verticalThrottleMode nav t
    | NO_VMODE => Sskip
  end.

Definition gen_fp_nav_mode_params (nav_call: fp_params_nav_call)
                                  (nav_mode: fp_params_nav_mode):
                                  statement := 
  let nav := get_nav_type nav_call in
  (gen_fp_pitch nav (get_nav_pitch nav_mode))
  @+@ (gen_fp_vmode nav (get_nav_vmode nav_mode)).

(** ** Generation of the hmode code *)

Definition gen_fp_hmode (h: hmode) (pc: fp_params_nav_call): statement :=
  match h with
  | ROUTE wp_id l_wp_id =>
    let wp := gen_uint8_const wp_id in
    let l_wp := gen_uint8_var (arbitrary_ident l_wp_id) in
    let f_id := arbitrary_ident (segment_str pc) in
    gen_fun_call_stmt f_id tvoid (Tcons tuchar (Tcons tuchar Tnil))
      (l_wp :: wp :: nil) pc
  | DIRECT wp_id =>
    let wp := gen_uint8_const wp_id in
    let f_id := arbitrary_ident (gotoWaypoint_str pc) in
    gen_fun_call_stmt f_id tvoid (Tcons tuchar Tnil) (wp :: nil) pc
  end.

(** ** Functions for the generation of the stages *)


Definition gen_fp_heading (ps: fp_params_heading) (pm: fp_params_nav_mode)
                                    (pc: fp_params_nav_call): statement :=
  let c := parse_c_code_float (get_heading_course ps) in
  let f_id := arbitrary_ident (heading_code pc) in
  let tmp_var := gen_float_tempvar tmp_RadOfDeg in
  let call_stmt :=
    gen_fun_call_stmt f_id tvoid (Tcons tfloat Tnil) (tmp_var :: nil) pc
  in
  (** tmp_RadOfDeg = RadofDeg(vpitch);*)
  (Scall (Some tmp_RadOfDeg) RadOfDeg (c :: nil))
  @+@ call_stmt
  @+@ gen_fp_nav_mode_params pc pm.

Definition gen_fp_attitude (ps: fp_params_attitude)
    (pm: fp_params_nav_mode) (pc: fp_params_nav_call): statement :=
  let r := parse_c_code_float (get_attitude_roll ps) in
  let f_id := arbitrary_ident (attitude_code pc) in
  let tmp_var := gen_float_tempvar tmp_RadOfDeg in
  let call_stmt :=
    gen_fun_call_stmt f_id tvoid (Tcons tfloat Tnil) (tmp_var :: nil) pc
  in
  (** tmp_RadOfDeg = RadofDeg(roll);*)
  (Scall (Some tmp_RadOfDeg) RadOfDeg (r :: nil))
  @+@ call_stmt
  @+@ gen_fp_nav_mode_params pc pm.

Definition gen_fp_manual (ps: fp_params_manual) (pm: fp_params_nav_mode)
                                    (pc: fp_params_nav_call): statement :=
  let r := parse_c_code_float (get_manual_roll ps) in
  let p := parse_c_code_float (get_manual_pitch ps) in
  let y := parse_c_code_float (get_manual_yaw ps) in
  let f_id := arbitrary_ident (manual_code pc) in
  let call_stmt :=
    gen_fun_call_stmt f_id tvoid
      (Tcons tfloat (Tcons tfloat (Tcons tfloat Tnil)))
        (r :: p :: y :: nil) pc
  in
  call_stmt
  @+@ gen_fp_nav_mode_params pc pm.

Definition gen_fp_go (ps: fp_params_go) (pm: fp_params_nav_mode)
                                    (pc: fp_params_nav_call): statement :=
  (gen_fp_hmode (get_go_hmode ps) pc)
  @+@ gen_fp_nav_mode_params pc pm.

Definition gen_fp_xyz (ps: fp_params_xyz) (pm: fp_params_nav_mode)
  (pc: fp_params_nav_call): statement :=
  let r := parse_c_code_float ps in
  let f_id := arbitrary_ident xyz_code in
  let f_xyz := 
    Evar f_id (Tfunction (Tcons tfloat Tnil) tvoid cc_default)
  in
  (Scall None f_xyz (r :: nil))
  @+@gen_fp_nav_mode_params pc pm.


Definition gen_fp_circle (ps: fp_params_circle) (pm: fp_params_nav_mode)
                                    (pc: fp_params_nav_call): statement :=
  let wp := gen_uint8_const (get_circle_wp ps) in
  let r := parse_c_code_float (get_circle_radius ps) in
  let f_id := arbitrary_ident (circle_code pc) in
  let call_stmt :=
    gen_fun_call_stmt f_id tvoid
      (Tcons tuchar (Tcons tfloat Tnil))
        (wp :: r :: nil) pc
  in
  (gen_fp_nav_mode_params pc pm)
  @+@ call_stmt.

Definition gen_fp_stay (ps: fp_params_stay) (pm: fp_params_nav_mode)
  (pc: fp_params_nav_call): statement :=
  (gen_fp_hmode (get_stay_hmode ps) pc)
  @+@ gen_fp_nav_mode_params pc pm.

Definition gen_fp_follow (ps: fp_params_follow)
                              (pc: fp_params_nav_call): statement :=
  let id := parse_c_code_uint8 (get_follow_ac_id ps) in
  let d := parse_c_code_float (get_follow_distance ps) in
  let h := parse_c_code_float (get_follow_height ps) in
  let f_id := arbitrary_ident (follow_code pc) in
  gen_fun_call_stmt f_id tvoid 
    (Tcons tuchar (Tcons tfloat (Tcons tfloat Tnil)))
      (id :: d :: h :: nil) pc.


Definition gen_fp_survey_rectangle (ps: fp_params_survey_rectangle)
                                    (pc: fp_params_nav_call): statement :=
  let wp1 := gen_uint8_const (get_srect_wp1 ps) in
  let wp2 := gen_uint8_const (get_srect_wp2 ps) in
  let f_id := arbitrary_ident (survey_rectangle_code pc) in
  let f := 
    Evar f_id
    (Tfunction (Tcons tuchar (Tcons tuchar Tnil)) tvoid cc_default)
  in
  Scall None f (wp1 :: wp2 :: nil).

Definition gen_fp_eight (ps: fp_params_eight) (pm: fp_params_nav_mode)
  (pc: fp_params_nav_call): statement :=
  let c := gen_uint8_const (get_eight_center ps) in
  let t := gen_uint8_const (get_eight_turn_around ps) in
  let r := parse_c_code_float (get_eight_radius ps) in
  let f_id := arbitrary_ident eight_code in
  let f_eight := 
    Evar f_id
      (Tfunction (Tcons tuchar (Tcons tuchar (Tcons tfloat Tnil)))
        tvoid cc_default)
  in
  gen_fp_nav_mode_params pc pm
  @+@ (Scall None f_eight (c :: t :: r :: nil)).

Definition gen_fp_oval (ps: fp_params_oval) (pm: fp_params_nav_mode)
                                    (pc: fp_params_nav_call): statement :=
  let p1 := gen_uint8_const (get_oval_p1 ps) in
  let p2 := gen_uint8_const (get_oval_p2 ps) in
  let r  := parse_c_code_float (get_oval_radius ps) in
  let f_id := arbitrary_ident oval_code in
  let f_oval := 
    Evar f_id
      (Tfunction (Tcons tuchar (Tcons tuchar (Tcons tfloat Tnil)))
        tvoid cc_default)
  in
  gen_fp_nav_mode_params pc pm
  @+@ (Scall None f_oval (p1 :: p2 :: r :: nil)).

Definition gen_fp_guided (ps: fp_params_guided)
                              (pc: fp_params_nav_call): statement :=
  let flags := parse_c_code_uint8 (get_guided_flags ps) in
  let cmds := parse_c_code_float (get_guided_commands ps) in
  let f_id := arbitrary_ident (guided_code pc) in
    gen_fun_call_stmt f_id tvoid
    (Tcons tuchar (Tcons tfloat Tnil))
    (flags :: cmds :: nil) pc.

Definition gen_fp_home := parse_c_code home_code.

(** Generates statements for the nav_mode *)
Definition gen_fp_nav_code (nav_mode: fp_navigation_mode): statement :=
  match nav_mode with
  | HEADING ps pm pc => gen_fp_heading ps pm pc
  | ATTITUDE ps pm pc => gen_fp_attitude ps pm pc
  | MANUAL ps pm pc => gen_fp_manual ps pm pc
  | GO ps pm pc => gen_fp_go ps pm pc
  | XYZ ps pm pc => gen_fp_xyz ps pm pc
  | CIRCLE ps pm pc => gen_fp_circle ps pm pc
  | STAY ps pm pc => gen_fp_stay ps pm pc
  | FOLLOW ps pc => gen_fp_follow ps pc
  | SURVEY_RECTANGLE ps pc => gen_fp_survey_rectangle ps pc
  | EIGHT ps pm pc => gen_fp_eight ps pm pc
  | OVAL ps pm pc => gen_fp_oval ps pm pc
  | GUIDED ps pc => gen_fp_guided ps pc
  | HOME => gen_fp_home
  end.

(** Generates statements for the nav_mode considering *)
(** the condition.                                          *)
Definition gen_fp_nav_cond (nav_mode: fp_navigation_mode): statement :=
  match nav_cond_stmt nav_mode with
  | Some (n_cond_stmt, wp) => 
    n_cond_stmt
    @+@ 
    (Sifthenelse
      (* if (tmp_NavCond) *)
      (gen_bool_tempvar tmp_NavCond)
      (* { post_call; NextStageAndBreakFrom(wp); }*)
      ((gen_fp_nav_post_call nav_mode)
      @+@(nextStageAndBreakFrom wp))
      (* else { NAV_CODE }*)
      (gen_fp_nav_code nav_mode)
    )
  | _ => gen_fp_nav_code nav_mode
  end.


(** Generates the statements for the main case *)
Definition gen_fp_nav_stmt (nav_mode: fp_navigation_mode)
                           (until: option c_cond):
                           statement :=
  (gen_fp_nav_pre_call nav_mode)
  @+@ (gen_fp_nav_cond nav_mode)
  @+@ (gen_fp_nav_until nav_mode until)
  @+@ (gen_fp_nav_post_call nav_mode)
  @+@ Sbreak.
