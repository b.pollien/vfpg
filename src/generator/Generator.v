From VFP Require Import BasicTypes
                        FlightPlanGeneric FlightPlan 
                        FlightPlanExtended FlightPlanSized
                        FPNavigationModeGen
                        FPExtended GvarsVerification
                        FBDerouteAnalysis FPSizeVerification
                        AuxFunctions ClightGeneration
                        CommonFPDefinition CommonFPSimplified.
From GEN Require Import CommonFP.

From compcert Require Import Coqlib Integers Floats AST Ctypes
                             Cop Clight Clightdefs.
Import Clightdefs.ClightNotations.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope nat_scope.
Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope clight_scope.

Import FP FP_E.

(** Get a flight plan and generate the C light *)

(** Generation function for the exceptions *)

Definition gen_exception (e: fp_exception): statement :=
  let deroute := get_expt_block_id e in
  (* nav_block != id_block *)
  let test_b := gen_neq_expr tmpNavBlock (gen_uint8_const deroute) in
  let cond := parse_c_code_cond (get_expt_cond e) in
  let (stmt_c, expr_c) := gen_and_stmt_expr test_b cond in
  tmpNavBlock_stmt
  @+@ stmt_c
  @+@ (Sifthenelse
        (* if (test_b && cond ) {... *)
        expr_c
        (* exec; goto(block); return; *)
        ( (parse_c_code_option (get_expt_exec e))
          @+@ (gen_call_iparams_fun CommonFP._nav_goto_block deroute)
          @+@ (Sreturn None)
        )
        Sskip).


Definition gen_exceptions (exceptions: fp_exceptions): statement :=
  let concat_ex e stmt := (gen_exception e) @+@ stmt in
  fold_right concat_ex Sskip exceptions.

(** Generation of all th stages *)

(** Generation of a statement corresponding to a set stage *)
Definition gen_fp_set (params: fp_params_set): statement :=
  (Sassign (gen_int_var_str(get_set_var params))
           (parse_c_code_int (get_set_value params)))
  @+@ nextStage.

(** Generation of a statement corresponding to a call stage *)
Definition gen_fp_call (params: fp_params_call): statement :=
  let call := parse_c_code_cond (get_call_fun params) in
  let break :=
    if (get_call_break params) then nextStageAndBreak else nextStage in
  if (get_call_loop params) then
    (** if until NextStageAndBreak; *)
    let until :=
      match get_call_until params with
      | None => Sskip
      | Some u => Sifthenelse (parse_c_code_cond u) nextStageAndBreak Sskip
      end in
    Sifthenelse
      (gen_neg_bool call) (** if (!call) *)
      break (** then { NextStage(AndBreak);}*)
      (until @+@ Sbreak)
  else
    Ssequence (parse_c_code (get_call_fun params)) break.

(** Generation of a statement corresponding to a deroute stage *)
Definition gen_fp_deroute (params: fp_params_deroute): statement :=
  nextStage
  @+@ gen_call_iparams_fun CommonFP._nav_goto_block
                      (get_deroute_block_id params)
  @+@ Sbreak.

(** Generation of a statement corresponding to a return stage *)
Definition gen_fp_return (params: fp_params_return): statement :=
  gen_call_iparams_fun CommonFP._Return
                      (nat_of_bool params)
  @+@ Sbreak.

(** Generation function of labels*)
Definition get_while_label (params: params_while): label :=
  let idb := get_while_block_id params in
  let ids := get_while_id params in
  gen_label "while" idb ids.

Definition get_end_while_label (params: params_while): label :=
  let idb := get_while_block_id params in
  let ids := get_end_while_id params in
  gen_label "endwhile" idb ids.

(** Generation of the first cases corresponding to a while stage *)
Definition gen_fp_while (id: nat) (params: params_while):
                                          statement :=
  let l_while := get_while_label params in
  let l_wend := get_end_while_label params in
  (* [l]: set_nav_stage(id); *)
  (Slabel l_while (setNavStage id))
  @+@ Sifthenelse
      (** if (!cond) *)
      (gen_neg_bool (parse_c_code_cond (get_while_cond params))) 
      (Sgoto l_wend) (** then { goto(end_while);}*)
      (nextStageAndBreak).

Definition gen_fp_end_while (params: params_while): statement :=
  (Sgoto (get_while_label params)) 
  @+@ (Slabel (get_end_while_label params)
                  (setNavStage ((get_end_while_id params) + 1))).

Definition gen_fp_default: statement := nextBlock @+@ Sbreak.

Definition gen_fp_stage (stage : fp_stage): statement :=
  let case_stmt (ids: stage_id) (stmt: statement) :=
    (setNavStage ids) @+@ stmt
  in 
  match stage with
  | WHILE ids params => gen_fp_while ids params
  | END_WHILE ids params _ => case_stmt ids (gen_fp_end_while params)
  | SET ids params => case_stmt ids (gen_fp_set params)
  | CALL ids params => case_stmt ids (gen_fp_call params)
  | DEROUTE ids params => case_stmt ids (gen_fp_deroute params)
  | RETURN ids params => case_stmt ids (gen_fp_return params)
  | NAV_INIT ids nav_mode => case_stmt ids (gen_fp_nav_init_stmt nav_mode)
  | NAV ids nav_mode until => 
        case_stmt ids (gen_fp_nav_stmt nav_mode until)
  | DEFAULT ids => case_stmt ids (gen_fp_default)
  end.

Definition gen_fp_cases (stage : fp_stage)
                                (l_stmt: labeled_statements):
                                labeled_statements :=
  let ids := get_stage_id stage in
  let stmt := gen_fp_stage stage in
  let case_stmt := gen_case ids stmt l_stmt in
  match stage with
    | DEFAULT ids => LScons None stmt l_stmt
    | _ => case_stmt
  end.

Definition gen_fp_stages (stages: list fp_stage):
                                                labeled_statements :=
  fold_right gen_fp_cases LSnil stages.

(** Generation of all the block *)
Definition gen_fp_block (block: fp_block): statement :=
    let idb := get_block_id block in
    let get_nav_stage :=
      Evar CommonFP._get_nav_stage (Tfunction Tnil tuchar cc_default)
    in
    (* pre_call *)
    (parse_c_code_option (get_block_pre_call block))
    @+@ (gen_exceptions (get_block_exceptions block))
    (* Tmp var to get nav_stage*)
    @+@ tmpNavStage_stmt
    @+@((** Switch for stages*)
        Sswitch (tmpNavStage)
                (gen_fp_stages (get_block_stages block))
        )
    (* fp_post_call *)
    @+@ (parse_c_code_option (get_block_post_call block))
    @+@ Sbreak.

Definition gen_fp_blocks (blocks: list fp_block)
                          (default_block: fp_block): labeled_statements := 
    let default := LScons None (gen_fp_block default_block) LSnil in
    let case (b: fp_block) (c: labeled_statements) := 
      LScons (Some (Z.of_nat (get_block_id b))) (gen_fp_block b) c in
    fold_right case default blocks.

Definition gen_block_switch (fp: flight_plan): statement :=
  (** Tmp var for nav_block *)
  tmpNavBlock_stmt
  @+@ (** Main switch for block*)
    Sswitch (tmpNavBlock)
            (gen_fp_blocks (get_fp_blocks fp)
                           (get_default_block fp)).

(** Definition of the list of tmp variables *)
Definition tmp_variables :=
  (tmp_nav_block, tuchar)
  :: (tmp_nav_stage, tuchar)
  :: (tmp_cond, tbool)
  :: (tmp_RadOfDeg, tfloat)
  :: (tmp_AltitudeMode, tfloat)
  :: (tmp_NavCond, tbool)
  :: nil.

(** Definition of the auto_nav function *)
Definition gen_fp_auto_nav (fp:flight_plan) := {|
  fn_return := tvoid;
  fn_callconv := cc_default;
  fn_params := nil;
  fn_vars := nil;
  fn_temps := tmp_variables;
  fn_body :=
  (** Global exception *)
  (gen_exceptions (get_fp_exceptions fp))
  (** Block switch *)
  @+@ (gen_block_switch fp)
|}.

(** Definition for the global variable NB_BLOCKS *)
Definition gvar_nb_blocks (fp : flight_plan) := {|
  gvar_info := tuchar;
  gvar_init := AST.Init_int8 ((Int.repr (Z.of_nat (get_nb_blocks fp)))) :: nil;
  gvar_readonly := true;
  gvar_volatile := false
|}.

(** Generics definition needed *)

Definition composites : list composite_definition :=
nil.

Definition public_idents : list ident := _auto_nav :: nil.

(** Global definition of the functions *)

Definition global_definitions (fp: flight_plan): list gdef :=
    (CommonFP._nb_blocks, Gvar (gvar_nb_blocks fp))
    :: (_on_enter_block, Gfun(Internal (gen_fp_on_enter_block fp)))
    :: (_on_exit_block, Gfun(Internal (gen_fp_on_exit_block fp)))
    :: (_forbidden_deroute, Gfun(Internal (gen_fp_forbidden_deroute fp)))
    :: (_auto_nav, Gfun(Internal (gen_fp_auto_nav fp)))
    :: nil.

(** Definition of the return type in order to manage errors *)
Variant res_gen :=
| CODE (res: Clight.program * list err_msg)
| ERROR (errs: list err_msg).

(** Definition of the function to generate auto_nav function *)

Definition generate_flight_plan (fp: FP.flight_plan) (gvars: list string):
                                res_gen :=
  let fpe := extend_flight_plan fp in
  let deroute_analysis := fb_deroute_analysis fp in
  match gvars_definition gvars with 
  | GVARS gvars => 
    match size_verification fpe with
    | OK fps => 
        let gdefs :=  (proj1_sig gvars)
                        ++ (global_definitions (get_fp fps)) in
        CODE (mkprogram composites gdefs public_idents _auto_nav Logic.I,
                  deroute_analysis)
    | ERR errors => ERROR (deroute_analysis ++ errors)
    end
  | ERR_GVARS errors => ERROR (deroute_analysis ++ errors)
  end.
