From VFP Require Import BasicTypes
                        ClightGeneration
                        CommonLemmas
                        CommonFPSimplified
                        .
From GEN Require Import CommonFP.
From Coq Require Import  FunInd Program Program.Equality
                                 Program.Wf.

From compcert Require Import Coqlib AST Ctypes
                             Cop Clight Clightdefs.
Import Clightdefs.ClightNotations.
Import Pos.
Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope nat_scope.
Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope clight_scope.


(** * Declaration of the gvars from the list of ident and verify that the
    names does not overlap with reserverd idents *)

(** ** Properties of overlapping *)

(** The variables defined must be Gvar elements generated with the
    function [gen_globaldef_var]. *)
Record correct_gvar (var: gdef) := create_correct_gvar {
  is_gvar: exists id v, var = (# id, Gvar v);
  def_var: forall id v,
    var = (#id, Gvar v)
      -> var = gen_globaldef_var id
}.

(** The variables declared must be different and must not overlap with the
    reserved idents used for the execution of the flight plan. *)
Record correct_gvars (vars: list gdef) := create_correct_gvars {
  no_repet: list_norepet ((List.map fst vars) ++ reserved_idents);
  all_correct_gvar: Forall correct_gvar vars
}.

Definition cgvars :=
    {gvars: list gdef | correct_gvars gvars}.

(** ** Verification functions *)

(** Error messages *)
Definition double_decl (var: ident): err_msg :=
  ERROR ("Double declaration of the variable '"
               ++ (string_of_ident var)
               ++ "' !").

Definition use_reserved_ident (var: ident): err_msg :=
  ERROR ("The name  '" ++ (string_of_ident var)
               ++ "' is a reserved ident used for the execution of the flight plan ! ").

(** Result of the gvars analysis *)
Variant res_gvars_analysis :=
    | GVARS (gvars: cgvars)
    | ERR_GVARS (err: list err_msg).

(** Verification functions *)

(** Verify that [id] is not in the list [lid]. If it is the case an error
  is returned using the function [err_fun]. *)
Fixpoint test_in_var (id: ident) (lid: list ident)
                          (err_fun: ident -> err_msg): list err_msg :=
  match lid with
  | nil => nil
  | id' :: lid' => 
      if (id =? id')%positive then [:: err_fun id ]
      else test_in_var id lid' err_fun
  end.

(** Verification of [correct_gvars] properties *)
Fixpoint test_in_vars (lid: list ident): list err_msg :=
  match lid with
  | nil => nil
  | id :: lid' =>
    (test_in_var id lid' double_decl)
    ++ (test_in_var id reserved_idents use_reserved_ident)
    ++ (test_in_vars lid')
  end.

Definition gvars_verification (gvars: list gdef): list err_msg :=
  test_in_vars (List.map fst gvars).

(** ** Lemmas to verify the functions *)

Lemma test_in_var_verification:
  forall func lid id,
    [::] = test_in_var id lid func
    -> ~ In id lid.
Proof.
  induction lid as [|id' lid IH]; move => id H //=.
  rewrite /test_in_var in H. apply and_not_or.
  destruct ((id =? id')%positive) eqn:Heq.
  - by inversion H.
  - split. 
    + rewrite eqb_sym in Heq. by apply eqb_neq.
    + by apply IH.
Qed.

Lemma gvars_verification_cons:
  forall id lid,
    [::] = test_in_vars (id :: lid)
    -> [::] = test_in_vars lid
        /\ [::] = test_in_var id lid double_decl
        /\ [::] = test_in_var id reserved_idents use_reserved_ident.
Proof.
  move => id lid H.
  rewrite /test_in_vars in H. symmetry in H.
  destruct (app_eq_nil _ _ H) as [Hd H'].
  destruct (app_eq_nil _ _ H') as [Hr Hrec].
  rewrite Hd Hr. by  repeat split.
Qed.

Lemma no_repet_idents_verification:
  forall lid,
    [::] = test_in_vars lid
    -> list_norepet (lid ++ reserved_idents).
Proof.
  induction lid as [|id lid IH]; move => H.
  - by apply norepet_reserved_idents.
  - destruct (gvars_verification_cons H) as [Hgvars [Hd Hr]].
    destruct ((proj1 (list_norepet_app _ _)) (IH Hgvars))
      as [Hgvars' [Hr' Hdis]].
    apply list_norepet_app; split; [|split].
    + apply list_norepet_cons => //.
      by apply (test_in_var_verification Hd).
    + by apply norepet_reserved_idents.
    + apply list_disjoint_cons_l => //.
      by apply (test_in_var_verification Hr).
Qed.

Lemma no_repet_gvars_verification:
  forall gvars,
    [::] = gvars_verification gvars
    -> list_norepet ((List.map fst gvars) ++ reserved_idents).
Proof.
  rewrite /gvars_verification => gvars.
  by apply no_repet_idents_verification.
Qed.

Lemma conversion_impl_correct_gvar:
  forall id, correct_gvar (gen_globaldef_var id).
Proof.
  rewrite /gen_globaldef_var => id. split.
  - exists id; eexists. by [].
  - move => id' v H. injection H as Hid Hv.
    by rewrite /gen_globaldef_var Hid Hv.
Qed.

Lemma conversion_impl_correct_gvars:
  forall lid, Forall correct_gvar (List.map gen_globaldef_var lid).
Proof.
  induction lid as [| id lid IH] => //=.
  apply Forall_cons.
  - by apply conversion_impl_correct_gvar.
  - by apply IH.
Qed.

(** ** Main function *)
Program Definition gvars_definition (gvars: list string): res_gvars_analysis :=
    let gvars := List.map gen_globaldef_var gvars in
    match gvars_verification gvars with
    | nil => GVARS gvars
    | e :: errors => ERR_GVARS (e :: errors)
    end.
Next Obligation.
  split.
  - by apply no_repet_gvars_verification.
  - by apply conversion_impl_correct_gvars.
Qed.

(** * Usefull lemmas *)

Lemma correct_gvars_hd:
  forall (var: gdef) vars,
  correct_gvars (var :: vars)
  -> correct_gvar var.
Proof.
  move => var vars H.
  by apply (Forall_inv (all_correct_gvar H)).
Qed.

Lemma correct_gvars_tail:
  forall (var: gdef) vars,
    correct_gvars (var :: vars)
    -> correct_gvars vars.
Proof.
  move => var vars [Hrepet Hg]; split.
  - by inversion Hrepet.
  - by inversion Hg.
Qed.

Lemma no_nb_blocks_in_cgvars:
  forall (gvars: cgvars) var funcs,
    In  (CommonFP._nb_blocks, Gvar var) funcs
    -> In (CommonFP._nb_blocks, Gvar var)
          (` gvars ++ funcs).
Proof.
  destruct gvars as [vars Hg].
  induction vars as [|var' vars IH]; move => //= var funcs HIn.
  right. apply IH => //. by apply (correct_gvars_tail Hg).
Qed.

Lemma no_on_enter_in_cgvars:
  forall (gvars: cgvars) func funcs,
    In  (_on_enter_block, Gfun func) funcs
    -> In (_on_enter_block, Gfun func)
          (` gvars ++ funcs).
Proof.
  destruct gvars as [vars Hg].
  induction vars as [|var vars IH]; move => //= func funcs HIn.
  right. apply IH => //. by apply (correct_gvars_tail Hg).
Qed.

Lemma no_on_exit_in_cgvars:
  forall (gvars: cgvars) func funcs,
    In  (_on_exit_block, Gfun func) funcs
    -> In (_on_exit_block, Gfun func)
          (` gvars ++ funcs).
Proof.
  destruct gvars as [vars Hg].
  induction vars as [|var vars IH]; move => //= func funcs HIn.
  right. apply IH => //. by apply (correct_gvars_tail Hg).
Qed.

Lemma no_forbidden_deroute_in_cgvars:
  forall (gvars: cgvars) func funcs,
    In  (_forbidden_deroute, Gfun func) funcs
    -> In (_forbidden_deroute, Gfun func)
          (` gvars ++ funcs).
Proof.
  destruct gvars as [vars Hg].
  induction vars as [|var vars IH]; move => //= func funcs HIn.
  right. apply IH => //. by apply (correct_gvars_tail Hg).
Qed.

Lemma no_auto_nav_in_cgvars:
  forall (gvars: cgvars) func funcs,
    In  (_auto_nav, Gfun func) funcs
    -> In (_auto_nav, Gfun func)
          (` gvars ++ funcs).
Proof.
  destruct gvars as [vars Hg].
  induction vars as [|var vars IH]; move => //= func funcs HIn.
  right. apply IH => //. by apply (correct_gvars_tail Hg).
Qed.

Lemma access_gvar:
  forall var,
    correct_gvar var
    -> exists id, var = gen_globaldef_var id.
Proof.
  move => var Hvar.
  destruct (is_gvar Hvar) as [id [v Heq]].
  exists id. by apply (def_var Hvar Heq).
Qed. 
