From VFP Require Import BasicTypes
                        FlightPlan FlightPlanGeneric.

From Coq Require Import String List ZArith.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope nat_scope.

Import FP.

(*******************************************************************)
(** File that provides an analysis for the forbidden deroute.      *)
(** It generates an error if there is a deroute to a block that    *)
(** is forbidden. The same error is generated id the next block is *)
(** forbidden.                                                     *)
(*******************************************************************)

(** Warning message when the next block is forbidden. *)
Definition next_forbidden (b: block_name) (c: option c_cond): err_msg :=
  WARNING
    ("The next block of "
    ++ b
    ++ " is forbidden"
    ++ (
    match c with
    | Some cond => " (under the condition " ++ cond ++ ")."
    | None => "."
    end)). 

(** Warning message when there is a derroute that is forbidden. *)
Definition deroute_forbidden (from: block_name) (to: block_name)
                                      (c: option c_cond): err_msg :=
  WARNING
    ("There is deroute from "
    ++ from
    ++ " to "
    ++ to
    ++ " that is forbidden"
    ++ (
    match c with
    | Some cond => " (under the condition " ++ cond ++ ")."
    | None => "."
    end)).

Definition exc_forbidden_error (from : block_name) 
                                  (to: block_name)
                                  (exc_c : c_cond)
                                  (fbd_c : option c_cond)
                                    : err_msg :=
  WARNING
    ((
      match from with
      | EmptyString => "There is a global exception to "
      | _ => "There is an exception from " ++ from ++ " to " 
      end
    )
    ++ to
    ++ " that is forbidden (exception condition : "
    ++ exc_c
    ++ (
      match fbd_c with
      | Some cond => "; forbidden deroute condition : " ++ cond ++ ")."
      | None => ")."
      end
    )).

(** Definition of forbidden deroute element (the to and the condition) *)
Definition fbd_el := (block_id * option c_cond) % type.

(** Add to the list [l] the id of [fb].to if [fb].from is the id of [b] *)
Definition update_fbd_list (b: fp_block) (fb: fp_forbidden_deroute)
                                    (l: list fbd_el): list fbd_el :=
  if (get_block_id b) =? (get_fbd_from fb) then
    (get_fbd_to fb, get_fbd_only_when fb) :: l
  else
    l.

(** Returns the list of forbidden block with their cond *)
(** from the block [b].                                 *)
Definition list_fbd_block (b: fp_block) (fbs: fp_forbidden_deroutes):
                                                          list fbd_el :=
  fold_right (update_fbd_list b) nil fbs.

(** Return the fbd_el corresponding to the [id] if it is found. *)
Definition find_fbd_el (idb: block_id) (l : list fbd_el): option fbd_el :=
  let aux :=
    fun x => match x with | (id, _) => idb =? id end
  in
  find aux l.

(** Computes the id of the next block [b] *)
Definition next_block (b: fp_block): block_id :=
  let idb := get_block_id b in
  if idb <? 255 then
    (get_block_id b) + 1
  else 255.

(** Test if the next block of [b] is in the list [l]    *)
(** Return a list with the corresponding error messages *)
Definition test_fbd_next (b: fp_block) (l: list fbd_el): list err_msg :=
  match find_fbd_el (next_block b) l with
  | None => nil
  | Some (_, cond) => (next_forbidden (get_block_name b) cond) :: nil
  end.

(** Test if the stage is a forbidden deroute that is in the list [l] *)
Definition test_fbd_stage (b: fp_block) (l: list fbd_el) (s: fp_stage):
                                                          list err_msg :=
  match s with
  | DEROUTE p =>
    match find_fbd_el (get_deroute_block_id p) l with
    | None => nil
    | Some (_, cond) =>
      (deroute_forbidden
        (get_block_name b) (get_deroute_block p) cond
      ) :: nil
    end
  | _ => nil
  end.

(** Test if there is deroute stage in the block that are in the list [l] *)
Definition test_fbd_deroute (b: fp_block) (l: list fbd_el): list err_msg :=
  flat_map (test_fbd_stage b l) (get_block_stages b).

Definition test_exception (from : block_name)
                           (to : block_name)
                            (exc : fp_exception)
                            (l : list fbd_el)
                            : list err_msg :=
  match find_fbd_el (get_expt_block_id exc) l with
  | None => nil
  | Some (_, cond) =>
    (exc_forbidden_error from to (get_expt_cond exc) cond) :: nil
  end.

Fixpoint test_exceptions (fp : flight_plan)
                            (from : block_name)
                            (excs : list fp_exception)
                            (l : list fbd_el)
                            : list err_msg :=
  match excs with
  | nil => nil
  | exc :: t =>
    let to := (get_block_name (get_block fp 
                              (get_expt_block_id exc))) in
    (test_exception from to exc l) ++ (test_exceptions fp from t l)
  end.

Definition test_block_exceptions (fp : flight_plan)
                                  (b : fp_block)
                                  (l : list fbd_el)
                                  : list err_msg :=
  let from := get_block_name b in
  let excs := ((get_block_exceptions b) 
              ++ (get_fp_exceptions fp))%list in
  test_exceptions fp from excs l.
  

(** Return the list of error messages of forbidden deroute for the *)
(** block[b] and the list [fbs].                                   *)
Definition list_err_fbd_block (fp : flight_plan) 
                              (fbs: fp_forbidden_deroutes)
                              (b: fp_block)
                                : list err_msg :=
  let l := list_fbd_block b fbs in
  (test_fbd_next b l) 
  ++ (test_fbd_deroute b l)
  ++ (test_block_exceptions fp b l).


(** The main function for the analysis.                      *)
(** This function returns None is no errors are found.       *)
(** In the others case, it returns a list of founded errors. *)
Definition fb_deroute_analysis (fp: flight_plan) : list err_msg :=
  let fbs := get_fp_forbidden_deroutes fp in
  let blocks := get_fp_blocks fp in
  flat_map (list_err_fbd_block fp fbs) blocks.
