From Coq Require Import String.
Local Open Scope string_scope.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

(** * Temporary variables                                               *)
(**  Definition of all the names for the temporary variables used       *)
(**  during the generation of the code.                                 *)

(** Temporary variables to get nav_block.   *)
(** Used in Generator.v (uint8_t variable)  *)
Definition tmp_nav_block_str := "tmp_nav_block".

(** Temporary variables to get nav_stage.   *)
(** Used in Generator.v (uint8_t variable)  *)
Definition tmp_nav_stage_str := "tmp_nav_stage".

(** Temporary cond variable used to compute the && for the exceptions. *)
(** Used in ClightGeneration.v (bool variable)                         *)
Definition tmp_cond_str := "tmp_cond".

(** Temporary variable used for the call of RadOfDeg. *)
(** Used in FPNavigationMode.v (float variable)       *)
Definition tmp_RadOfDeg_str := "tmp_RadOfDeg".

(** Temporary variable used for the call in AltitudeMode. *)
(** Used in FPNavigationMode.v (float variable)           *)
Definition tmp_AltitudeMode_str := "tmp_AltitudeMode".

(** Temporary variable used for the call of nav condition. *)
(** Used in FPNavigationMode.v (bool variable)             *)
Definition tmp_NavCond_str := "tmp_NavCond".