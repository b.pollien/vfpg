From VFP Require Import BasicTypes TmpVariables CommonStringLemmas.
From compcert Require Import Coqlib Integers Floats AST Ctypes
                             Cop Clight Clightdefs Events Values.
Import Clightdefs.ClightNotations.
Require Import DecimalString.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope Z_scope.
Local Open Scope string_scope.
Local Open Scope clight_scope.

(** * Common Definition for the generation of Clight code. *)

(** ** Conversion function between Clight and Coq. *)

(** Converts a nat into a Clight Int *)
Definition int_of_nat (n: nat) :=
  Int.repr (Z.of_nat n).

(** Converts a nat into a float *)
Definition float_of_nat (n: nat) :=
  Float.of_bits (Int64.repr 0).

(** Converts an option C code into string *)
Definition string_of_optcode (code: option c_code) :=
  match code with
  | Some code => "Some " ++ code
  | None => "None"
  end.

(** Plus 1 for Int *)
Definition int_plus1 (v: int) :=
  (Int.add v (Int.repr 1)).

(** ** Clight Gen functions *)

(** Set of functions that generate Clight code *)

(** Function that generates an ident from a string of a known *)
(** function or variable.                                     *)
Parameter create_ident: string -> ident.

(** Opposite function of create_ident used for errors message *)
Parameter string_of_ident: ident -> string.

(** Function that generates an ident from a string of a unknown *)
(** function or variable.                                       *)
Parameter arbitrary_ident: string -> ident.

(** Remark: [create_ident] and [arbitrary_ident] are OCaml functions as
  the strings are saved in a hashtable in order to be well printed during
  the conversion of the Clight into C code. During extraction, they will be
  both linked to the same functions. *)

(** The body of the function [create_ident] and [arbitrary_ident] *)
(** are not known, but this function is considered injective.     *)
Axiom create_ident_injective: 
  forall s1 s2, create_ident s1 = create_ident s2 -> s1 = s2.

Axiom arbitrary_ident_injective: 
  forall s1 s2, arbitrary_ident s1 = arbitrary_ident s2 -> s1 = s2.

(** Notation to convert a string into an ident *)
Notation "# s" := (create_ident s) (at level 1).
Notation "## s" := (arbitrary_ident s) (at level 1).

(** Lemma about negation of ident *)
Lemma neg_ident:
  forall s s', s <> s' -> #s <> #s'.
Proof.
  move => s s' H H'. by apply create_ident_injective in H'.
Qed.

#[global]
Hint Resolve neg_ident : core.

(** Creation a sequence of statement *)
Notation "A @+@ B" := (Ssequence A B) (at level 90, right associativity).

(** Generates a uint8 const *)
Definition gen_uint8_const (i: nat): expr := 
  Econst_int (int_of_nat i) tuchar.

(** Generates a 0 float const *)
Definition gen_0float_const: expr := 
  Econst_float (float_of_nat 0) tfloat.

(** Converts a bool into a integer *)
Definition nat_of_bool (b: bool): nat := 
  if b then 1%nat else 0%nat.

(** Convert a bool into a int *)
Definition int_of_bool (b: bool): int :=
  int_of_nat (nat_of_bool b).

(** Generates a bool const *)
Definition gen_bool_const (b: bool): expr :=
  Ecast (Econst_int (int_of_bool b) tint) tbool.

(** Creates a Int Val *)
Definition create_val (val: nat): Values.val :=
  Vint (int_of_nat val).

  (** Creates a bool Val *)
Definition create_bool_val (val: bool): Values.val :=
  Vint (int_of_bool val).

(** Generates a uint8 var *)
Definition gen_uint8_var (id:ident): expr := Evar id tuchar.
Definition gen_uint8_tempvar (id:ident): expr := Etempvar id tuchar.
Definition gen_uint8_var_str (s:string): expr :=
  gen_uint8_var (create_ident s).

(** Generates a int var *)
Definition gen_int_var (id:ident): expr := Evar id tint.
Definition gen_int_var_str (s:string): expr :=
  gen_int_var (create_ident s).

(** Generates a bool var *)
Definition gen_bool_var (id:ident): expr := Evar id tbool.
Definition gen_bool_tempvar (id:ident): expr := Etempvar id tbool.

(** Generates a float var *)
Definition gen_float_var (id:ident): expr := Evar id tfloat.
Definition gen_float_tempvar (id:ident): expr := Etempvar id tfloat.
Definition gen_float_var_str (s:string): expr :=
  gen_float_var (create_ident s).

(** Generates a void function *)
Definition gen_void_fun (id: ident): expr :=
  Evar id (Tfunction Tnil tvoid cc_default).

Definition gen_void_fun_param8 (id: ident): expr :=
  Evar id (Tfunction (Tcons tuchar Tnil) tvoid cc_default).

Definition gen_void_fun_param_float (id: ident): expr :=
  Evar id (Tfunction (Tcons tfloat Tnil) tvoid cc_default).

Definition gen_void_fun_paramb (id: ident): expr :=
  Evar id (Tfunction (Tcons tbool Tnil) tvoid cc_default).

(** Generates a bool function *)
Definition gen_bool_fun (id: ident): expr :=
  Evar id (Tfunction Tnil tbool cc_default).

(** Generates a int function *)
Definition gen_int_fun (id: ident): expr :=
  Evar id (Tfunction Tnil tint cc_default).

(** Generates a int var *)
Definition gen_int_expr (id: ident): expr :=
  Evar id tint.

(** Generates a neg expression on bool*)
Definition gen_neg_bool (e: expr): expr :=
  Eunop Onotbool e tint.

(** Generates a neg expression  on float *)
Definition gen_neg_float (e: expr): expr :=
  Eunop Oneg e tfloat.

(** Generates a != expression on int *)
Definition gen_neq_expr (e1: expr) (e2: expr): expr :=
  Ebinop One e1 e2 tint.

(** Generates a multiplication expression *)
Definition gen_mul_expr (e1: expr) (e2: expr) (t:type): expr :=
  Ebinop Omul e1 e2 t.


(** Generates && between two expr *)
(** Warning && do not exist, it is necessary to convert to a statement *)
(** that will compute the result in a tmp var, and then the result can *)
(** access with the expr.                                              *)
(** WARNING 2: the function use a temporary var, do not use twice this *)
(** function.                                                          *)
Definition tmp_cond := create_ident tmp_cond_str.
Definition gen_and_stmt_expr (e1: expr) (e2: expr): statement * expr :=
  let stmt :=
    Sifthenelse e1
      (Sset tmp_cond (Ecast e2 tbool))
      (Sset tmp_cond (gen_bool_const false)) in
  let expr := Etempvar tmp_cond tbool in
  (stmt, expr).

(** Generates a call statement from an expression *)
Definition gen_call (f: expr): statement :=
  (Scall None f nil).

(** Generates a call to a void function *)
Definition gen_call_void_fun (id: ident): statement := 
  gen_call (gen_void_fun id).

(** Generates a call to a void function with one expr parameter *)
Definition gen_call_eparams_fun8 (id: ident) (e: expr): statement :=
  (Scall None (gen_void_fun_param8 id) (e :: nil)).

Definition gen_call_eparams_funf (id: ident) (e: expr): statement :=
  (Scall None (gen_void_fun_param_float id) (e :: nil)).

Definition gen_call_eparams_funb (id: ident) (e: expr): statement :=
  (Scall None (gen_void_fun_paramb id) (e :: nil)).

(** Generates a call to a void function with one integer parameters *)
Definition gen_call_iparams_fun (id: ident) (v: nat): statement :=
  gen_call_eparams_fun8 id (gen_uint8_const v).

(** Generates a call to a void function with one bool parameters *)
Definition gen_call_bparams_fun (id: ident) (b: bool): statement :=
  gen_call_eparams_funb id (gen_bool_const b).

(** Gen a case labeled statement *)
Definition gen_case (id: nat) (stmt: statement)
                  (l_stmt: labeled_statements): labeled_statements :=
  LScons (Some (Z.of_nat id)) stmt l_stmt.

(** Add at the end the type to the given typelist *)
Fixpoint add_last_tl (l: typelist) (t: type) : typelist :=
  match l with
  | Tnil => (Tcons t Tnil)
  | Tcons t' tl => Tcons t' (add_last_tl tl t)
  end.

(** Definition of the return bool expressions *)
Definition return_false := Sreturn (Some (gen_bool_const false)).
Definition return_true := Sreturn (Some (gen_bool_const true)).

(** Generates a label : [l]_[idb]_[ids] *)
Definition gen_label (l: string) (idb: block_id) (ids: stage_id): label :=
  let string_idb := string_of_nat idb in
  let string_ids := string_of_nat ids in
  create_ident (l ++ "_" ++ string_idb ++ "_" ++ string_ids).

(** Generate a global definition for a variable in the for *)
Definition gen_globaldef_var (name: string):
                    (ident * globdef fundef type) :=
  let ident := create_ident name in
  let def := {|
    gvar_info := tint;
    gvar_init := (Init_space 4 :: nil);
    gvar_readonly := false;
    gvar_volatile := false
  |} in
  (ident, Gvar def).

(** Create a syscall trace *)
Definition mk_syscall_trace (s: string) : trace :=
  Event_syscall s nil (EVint (Int.repr 0)) :: nil.

(** ** C parsers *)
(** Set of functions that get c_code (string) and generate Clight code *)

(** Function to parse C code and generate a bool expression *)
Definition parse_c_code_cond (code: c_code): expr :=
  (** For now, creates a function call where the function is the C code *)
  gen_bool_fun (arbitrary_ident code).

(** Function to parse C code and generate a int expression *)
Definition parse_c_code_int (code: c_code): expr :=
  (** For now, creates a function call where the function is the C code *)
  gen_int_expr (arbitrary_ident code).

(** Function to parse C code and generate a uint8 expression *)
Definition parse_c_code_uint8 (code: c_code): expr :=
  (** For now, creates a function call where the function is the C code *)
  gen_uint8_var (arbitrary_ident code).

(** Function to parse C code and generate a uint8 expression *)
Definition parse_c_code_float (code: c_code): expr :=
  (** For now, creates a function call where the function is the C code *)
  gen_float_var (arbitrary_ident code).

(** Parse c code and generate a statement *)
(** Used in pre/post call, pre/post nav call, on_exit, on_enter and call*)
Definition parse_c_code (code: c_code): statement :=
  (** For now, creates a function call where the function is the C code *)
  (Scall None (gen_void_fun (arbitrary_ident code)) nil).

(** Parse c code option and generate code if possible *)
Definition parse_c_code_option (code: option c_code): statement :=
  match code with
  | None => Sskip
  | Some c => parse_c_code c
  end.
