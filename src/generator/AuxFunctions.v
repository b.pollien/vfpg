From VFP Require Import BasicTypes FlightPlanGeneric
                                FlightPlanExtended
                                ClightGeneration.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

(* From Coq Require Import String List ZArith. *)
From compcert Require Import Coqlib Integers Floats AST Ctypes
                             Cop Clight Clightdefs.
Import Clightdefs.ClightNotations.
Local Open Scope string_scope.
Local Open Scope clight_scope.
Local Open Scope nat_scope.

Import FP_E.

(** * List of function to generate the code for auxilary functions of the
    Flight Plan:
    - on_enter_block
    - on_exit_block
    - forbidden_deroute *)

(** ** Common part for the generation of on enter and on exit *)

Section COMMON_GEN.
  Variable get_code: fp_block -> option c_code.

  (** Generation of the code in the case *)
  Definition gen_case_code_on (code: c_code): statement :=
    (* $pre_call_block;*)
    parse_c_code code
    (* break; *)
    @+@ Sbreak.
 
  (** Generation of the case corresponding to the code of the current
      block *)
      Definition gen_case_on (block: fp_block)
                        (l_stmt: labeled_statements): labeled_statements :=
        match get_code block with
        | Some code => 
          LScons
            (* case $block_id:*)
            (Some (Z.of_nat (get_block_id block)))
            (gen_case_code_on code)
            l_stmt
        | None => l_stmt
        end.

End COMMON_GEN.

(** ** On enter block function *)
Definition block_var := #"block".

(** Definition of the on_enter_block function *)
Definition gen_fp_on_enter_block (fp:flight_plan) := {|
  fn_return := tvoid;
  fn_callconv := cc_default;
  fn_params := (block_var, tuchar) :: nil;
  fn_vars := nil;
  fn_temps := nil;
  fn_body :=
  Sswitch
    (gen_uint8_tempvar block_var)
    (fold_right
      (gen_case_on get_block_on_enter)
      (* Default: *)
      (LScons None Sbreak LSnil)
      (get_fp_blocks fp)
      )
|}.

(** ** On exit block function *)

(** Definition of the on_exit_block function *)
Definition gen_fp_on_exit_block (fp:flight_plan) := {|
  fn_return := tvoid;
  fn_callconv := cc_default;
  fn_params := (block_var, tuchar) :: nil;
  fn_vars := nil;
  fn_temps := nil;
  fn_body :=
  Sswitch
    (gen_uint8_tempvar block_var)
    (fold_right
      (gen_case_on get_block_on_exit)
      (* Default: *)
      (LScons None Sbreak LSnil)
      (get_fp_blocks fp)
      )
|}.

(** ** Forbidden deroute function *)
Definition _from := #"from".
Definition _to := #"to".

(** Generate the statement that test the to and the condition *)
Definition gen_fbd_test (fb: fp_forbidden_deroute): statement :=
  Sifthenelse
    (** _to == $(fb.to)*)
    (Ebinop
      Oeq
      (gen_uint8_tempvar _to)
      (gen_uint8_const (get_fbd_to fb))
      (tbool))
    (** Verify if there is a condition *)
    (match get_fbd_only_when fb with
     | Some cond =>
        Sifthenelse
          (parse_c_code_cond cond)
          (return_true)
          Sskip
      | None => return_true
    end)
    (** else { skip; } *)
    Sskip.



(** Generation of the case corresponding to the forbidden_deroute of the
  current block *)
Definition test_from (from: nat) := fun x => (get_fbd_from x) =? from.
Function gen_forbidden_deroute_case (fbds: fp_forbidden_deroutes)
                  (l_stmt: labeled_statements) {measure length fbds}: labeled_statements :=
  match fbds with
  | fb :: _ =>
    let from := get_fbd_from fb in
    (** Get only forbidden_deroute with the same from *)
    let (filtered, fbds) := 
      partition (test_from from) fbds
    in
    LScons
    (* case $from:*)
    (Some (Z.of_nat from))
    (* test of the conditions ;*)
    (fold_right (fun x y => x @+@ y) Sbreak (map gen_fbd_test filtered))
    (gen_forbidden_deroute_case fbds l_stmt)
  | nil => l_stmt
  end.
Proof.
  intros.
  destruct filtered.
  * simpl in teq0. destruct (partition (test_from (get_fbd_from fb)) l).
    unfold test_from in teq0. rewrite Nat.eqb_refl in teq0.
    discriminate.
  * apply partition_length in teq0.
    rewrite teq0. simpl. lia.
Qed.

(** Definition of the auto_nav function *)
Definition gen_fp_forbidden_deroute (fp:flight_plan) := {|
  fn_return := tbool;
  fn_callconv := cc_default;
  fn_params := (_from, tuchar) :: (_to, tuchar) :: nil;
  fn_vars := nil;
  fn_temps := nil;
  fn_body :=
  (Sswitch
    (gen_uint8_tempvar _from)
    (gen_forbidden_deroute_case
      (get_fp_forbidden_deroutes fp)
      (* Default: *)
      (LScons None Sbreak LSnil)
    )) @+@ return_false
|}.
