Require Extraction.

From VFP Require Import BasicTypes
                        FPNavigationMode
                        FlightPlanGeneric
                        FlightPlan
                        FlightPlanExtended
                        Generator
                        FlightPlanParsed
                        Importer
                        FPPUtils.

From GEN Require Import CommonCCode.

From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(** Module imported for the extraction *)
From Coq Require Import BinInt.

From compcert Require Import AST Machregs Ctypes Csyntax Initializers
                             Ctyping Clight Floats.

(* Standard lib *)
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlString.

(** Coqlib *)
Extract Inlined Constant Coqlib.proj_sumbool => "(fun x -> x)".

(** Datatypes *)
Extract Inlined Constant Datatypes.fst => "fst".
Extract Inlined Constant Datatypes.snd => "snd".

(** Save ident names for the printer *)
Extract Constant ClightGeneration.create_ident => "fun x -> Camlcoq.intern_string (Camlcoq.camlstring_of_coqstring x)".
Extract Constant ClightGeneration.string_of_ident => "fun x -> Camlcoq.coqstring_of_camlstring (Hashtbl.find Camlcoq.string_of_atom x)".
Extract Constant ClightGeneration.arbitrary_ident => "fun x -> Camlcoq.intern_string (Camlcoq.camlstring_of_coqstring x)".

(** Archi *)
Extract Constant Archi.win64 => true.

Extraction Blacklist Int String List ssrnat.

Extract Inlined Constant Defs.F2R => "fun _ -> assert false".
Extract Inlined Constant Binary.FF2R => "fun _ -> assert false".
Extract Inlined Constant Binary.B2R => "fun _ -> assert false".
Extract Inlined Constant Bracket.inbetween_loc => "fun _ -> assert false".

(** Needed in Coq 8.4 to avoid problems with Function definitions. *)
Set Extraction AccessOpaque.

(** MatComp *)
From mathcomp Require seq.

Extract Inductive prod => "(*)"  [ "(,)" ].
Extract Inlined Constant seq.size_subseq_leqif => "fun _ -> assert false".
Extract Inlined Constant ssrnat.leqif => "fun _ -> assert false".
Extract Inlined Constant ssrnat.eqnP => "fun _ -> assert false".

Extraction Inline seq.take seq.drop.

(** Axioms of ClightGeneration *)
Extract Inlined Constant 
  ClightGeneration.create_ident_injective => "fun _ -> assert false".

Cd "extracted".

Separate Extraction
    BinPos.Pos.pred
    AST.builtin_arg AST.builtin_res AST.builtin_arg_constraint
    String.string_dec
    Archi.win64

    Ctypes.typlist_of_typelist Ctypes.signature_of_type
    Ctypes.layout_struct

    Ctyping.typecheck_program
    Ctyping.epostincr Ctyping.epostdecr Ctyping.epreincr Ctyping.epredecr
    Ctyping.eselection

    Machregs.mregs_for_operation Machregs.mregs_for_builtin
    Machregs.two_address_op Machregs.is_stack_reg
    Machregs.destroyed_at_indirect_call Machregs.register_names
    Machregs.register_by_name

    Initializers.transl_init Initializers.constval

    Clight.type_of_function

    Floats.Float32.of_bits
    Floats.Float32.from_parsed Floats.Float.from_parsed

    FlightPlanGeneric
    FlightPlan
    FPNavigationMode
    Generator.generate_flight_plan

    FlightPlanParsed
    FPPUtils
    Importer.string2fp
    commonC.

Cd "..".
