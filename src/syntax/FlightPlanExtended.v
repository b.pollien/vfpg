From Coq Require Import Bool String List Arith PeanoNat Lia.


From VFP Require Import BasicTypes FPNavigationMode CommonLemmas
                                 CommonSSRLemmas FlightPlanGeneric.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.

Set Implicit Arguments.

(** * Flight Plan Extended *)

(** Definition of the extended version of the Coq flight plan. This *)
(** version is an intermediate representation for the generator     *)
(** where the stages are extended.                                  *)

Module FP_E.

  (** Definition of the numerotated version of the flight plan *)

  (** ** Parameters while *)
  Record params_while := create_fp_end_while {
    get_while_block_id: block_id;
    get_while_id: stage_id;
    get_end_while_id: stage_id;
    get_while_cond: fp_params_while;
  }.
  Definition mk_end_while := create_fp_end_while.

  (** ** Stages *)
  Inductive fp_stage :=
  WHILE (id: stage_id) (params: params_while)
  | END_WHILE (id: stage_id) 
              (params: params_while)
              (body: list fp_stage)
  | SET (id: stage_id) (params: fp_params_set)
  | CALL (id: stage_id) (params: fp_params_call)
  | DEROUTE (id: stage_id) (params: fp_params_deroute)
  | RETURN (id: stage_id) (params: fp_params_return)
  | NAV_INIT (id: stage_id) (nav_mode: fp_navigation_mode)
  | NAV (id: stage_id)
        (nav_mode: fp_navigation_mode)
        (until: option c_cond)
  | DEFAULT (id: stage_id).

  Definition get_stage_id (s: fp_stage) :=
    match s with
    | WHILE n _ => n
    | END_WHILE n _ _ => n
    | SET n _ => n
    | CALL n _ => n
    | DEROUTE n _ => n
    | RETURN n _ => n
    | NAV_INIT n _ => n
    | NAV n _ _ => n
    | DEFAULT n => n
    end.

  (** ** Blocks *)
  Record fp_block := create_fp_block {
    get_block_name: block_name;
    get_block_id: block_id;
    get_block_stages: list fp_stage;
    get_block_exceptions: fp_exceptions;
    get_block_pre_call: option c_code;
    get_block_post_call: option c_code;
    get_block_on_enter: option c_code;
    get_block_on_exit: option c_code;
  }.
  Definition mk_fp_block := create_fp_block.

  Module Def <: GENERIC_FP.
    Definition fp_stage := fp_stage.
    Definition fp_block := fp_block.

    (** Default stage brings Home*)
    Definition default_block (id: block_id):=
      mk_fp_block
        "HOME" id
        (NAV 0 HOME None :: DEFAULT 1 :: nil)
        nil None None None None.

    Definition get_block_stages := get_block_stages.
    Definition get_block_pre_call := get_block_pre_call.
    Definition get_block_post_call := get_block_post_call.
    Definition get_block_on_enter := get_block_on_enter.
    Definition get_block_on_exit := get_block_on_exit.
  End Def.

  Module Common := COMMON_FP_FUN Def.
  Include Common.

  (** Get the list of stage from an block id *)
  Definition get_stages (fp: flight_plan) (id: block_id): list fp_stage :=
    get_block_stages (get_block fp id).

  (** Default stage *)
  Definition default_stage_id (fp: flight_plan) (id: block_id): stage_id :=
    (length (get_stages fp id) - 1).
  Definition default_stage (fp: flight_plan) (id: block_id): fp_stage :=
    DEFAULT (default_stage_id fp id).

  (** Get stage *)
  Definition get_stage (fp:flight_plan) (idb: block_id)
                                  (ids: stage_id): fp_stage :=
    List.nth ids (get_stages fp idb) (default_stage fp idb).

  (** Induction principle of extended stages *)
  Section fp_stage_ind'.
    Variable P : fp_stage -> Prop.

    Hypothesis WHILE_case:
      forall ids (params : params_while), P (WHILE ids params).

    Hypothesis END_WHILE_case:
      forall ids (p : params_while) (block : list fp_stage),
        Forall P block -> P (END_WHILE ids p block).

    Hypothesis CALL_case:
      forall ids (params : fp_params_call), P (CALL ids params).

    Hypothesis SET_case:
      forall ids (params : fp_params_set), P (SET ids params).

    Hypothesis DEROUTE_case:
      forall ids (params : fp_params_deroute), P (DEROUTE ids params).

    Hypothesis RETURN_case :
      forall ids (params : fp_params_return), P (RETURN ids params).

    Hypothesis NAV_INIT_case:
      forall ids (nav_mode : fp_navigation_mode),
        P (NAV_INIT ids nav_mode).

    Hypothesis NAV_case:
      forall ids (nav_mode : fp_navigation_mode) (until : option c_cond),
                            P (NAV ids nav_mode until).

    Hypothesis DEFAULT_case:
      forall ids, P (DEFAULT ids).

    Definition prog_ind' (ir: forall s, P s) : forall b, Forall P b :=
      list_ind (Forall P)
                (Forall_nil _)
                (fun s l => @Forall_cons _ _ s l (ir s)).

    Fixpoint fp_stage_ind' (s : fp_stage) : P s :=
      match s with
        | END_WHILE ids p b => 
            END_WHILE_case ids p (prog_ind' fp_stage_ind' b)
        | WHILE ids p => WHILE_case ids p
        | CALL ids p => CALL_case ids p
        | SET ids p => SET_case ids p
        | DEROUTE ids p => DEROUTE_case ids p
        | RETURN ids p => RETURN_case ids p
        | NAV_INIT ids m => NAV_INIT_case ids m
        | NAV ids m u => NAV_case ids m u
        | DEFAULT ids=> DEFAULT_case ids
      end.

  End fp_stage_ind'.

End FP_E.

(** * Flight Plan Extended Well-Formed *)

(** A Flight Plan extended is well-formed if it is correctly numebered,
  the stages are correctly positioned (there is only one [default stage]
  per block), ... See [wf_fp_e] definition. *)

Definition subseq_stages (fp: FP_E.flight_plan) (idb: block_id)
                              (from: stage_id) (to: stage_id):
                                list FP_E.fp_stage :=
  subseq (FP_E.get_stages fp idb ) from to.

Module FP_E_WF.
  Include FP_E.

  (** If there is a [while] stage in a block, then it must have another
      [end_while] stage at the position [get_end_while_id params]. *)
  Definition wf_while (fp: flight_plan): Prop :=
    forall idb ids ids' params, 
        get_stage fp idb ids = WHILE ids' params
        -> ids = get_while_id params 
         /\ (get_end_while_id params) > ids
         /\ get_stage fp idb (get_end_while_id params) 
            = END_WHILE (get_end_while_id params) params 
              (subseq_stages fp idb (get_while_id params)
                                      (get_end_while_id params))
         /\ ids = ids'
         /\ (get_block_id (get_block fp idb)) = get_while_block_id params.

  (** If there is a [end_while] stage in a block, then it must have a
      [while] stage at the position [get_while_id params]. *)
  Definition wf_end_while (fp: flight_plan): Prop :=
    forall idb ids ids' params block,
            get_stage fp idb ids = END_WHILE ids' params block
            -> get_end_while_id params = ids
              /\ get_stage fp idb (get_while_id params) 
                  = WHILE (get_while_id params) params
              /\ ids = ids'
              /\ (get_block_id (get_block fp idb))
                    = get_while_block_id params.

  (** There is no [default] stage in a block except at the end. *)
  Definition wf_no_default (fp : flight_plan): Prop :=
    forall idb ids, 
      ids < FP_E.default_stage_id fp idb
      -> forall id, 
          FP_E.get_stage fp idb ids <> DEFAULT id .

  (** There is always a [default] stage at the end of every block. *)
  Definition wf_default_last (fp : flight_plan): Prop :=
    forall idb,
      FP_E.get_stage fp idb (FP_E.default_stage_id fp idb) 
        = (FP_E.default_stage fp idb).

  (** There is at least 1 stage in every block. *)
  Definition wf_stages_gt_0 (fp: flight_plan): Prop :=
    forall idb,
      length (get_stages fp idb) > 0.

  (** Every stage is well numbered. *)
  Definition wf_numbering (fp : flight_plan): Prop :=
    forall idb ids,
          (forall n, FP_E.get_stage fp idb ids <> DEFAULT n)
          -> FP_E.get_stage_id (FP_E.get_stage fp idb ids) = ids.

  (** Definition of a Flight Plan Extended well-formed. *)
  Record wf_fp_e (fp : flight_plan) := create_wf_fp_e {
    get_wf_while: wf_while fp;
    get_wf_end_while: wf_end_while fp;
    get_wf_no_default: wf_no_default fp;
    get_wf_default_last: wf_default_last fp;
    get_wf_stages_gt_0: wf_stages_gt_0 fp;
    get_wf_numbering: wf_numbering fp;
  }.

  Definition flight_plan_wf := {fp : flight_plan | wf_fp_e fp}.

  (** ** Usefull lemmas about Flight Plan Extended Well-formed *)
  Lemma ids_ge_default:
    forall fp idb ids,
      wf_fp_e fp
      -> ids >= FP_E.default_stage_id fp idb
      -> FP_E.get_stage fp idb ids = FP_E.default_stage fp idb.
  Proof.
    intros fp idb ids Hwf Hd.
    have Hfp := (get_wf_default_last Hwf) idb.
    apply Nat.lt_eq_cases in Hd; destruct Hd as [Hlt | Heq].
    - unfold FP_E.get_stage.
      rewrite nth_overflow; try reflexivity.
      unfold FP_E.default_stage_id in Hlt; lia.
    - rewrite <- Heq; auto.
  Qed.

  Lemma get_id_default:
    forall fp idb ids id,
      wf_fp_e fp
      -> FP_E.get_stage fp idb ids = FP_E.DEFAULT id
      -> id = FP_E.default_stage_id fp idb.
  Proof.
    move => fp idb ids id Hwf Hc.
    destruct (le_lt_dec (FP_E.default_stage_id fp idb) ids) as [Hge | Hlt].
    - apply (ids_ge_default Hwf) in Hge.
      rewrite Hge /FP_E.default_stage in Hc. by inversion Hc.
    - apply (get_wf_no_default Hwf) in Hlt. have H := Hlt id.
      by rewrite Hc in H. 
  Qed.

  Lemma stage_wf_lt_default:
    forall fp idb ids,
    wf_fp_e fp
    -> FP_E.get_stage fp idb ids <> FP_E.default_stage fp idb
    -> ids < FP_E.default_stage_id fp idb.
  Proof.
    intros fp idb ids Hwf Hd.
    have Hfp := (get_wf_no_default Hwf) idb ids.
    destruct (ids <? FP_E.default_stage_id fp idb) eqn:H.
    - apply Nat.ltb_lt. auto.
    - apply Nat.ltb_nlt in H. apply not_lt in H.
      apply (ids_ge_default Hwf) in H. contradiction.
  Qed.

  Lemma stage_wf_lt_len_numeroted:
    forall fp idb ids,
      wf_fp_e fp
      -> ids <=  FP_E.default_stage_id fp idb
      -> FP_E.get_stage_id (FP_E.get_stage fp idb ids) = ids.
  Proof.
    move => fp idb ids Hwf Hlen.
    destruct ((proj1 (Nat.lt_eq_cases _ _ )) Hlen) as [H|H].
    - apply (get_wf_numbering Hwf).
      by apply (get_wf_no_default Hwf).
    - by rewrite H (get_wf_default_last Hwf).
  Qed.

  Lemma wf_stages_plus_default:
    forall fp idb,
      let stages := get_block_stages (FP_E.get_block fp idb) in
        wf_fp_e fp 
        -> stages 
            = (seq.take ((length stages) - 1) stages) 
                  ++ ((default_stage fp idb) :: nil).
  Proof.
    move => fp idb stages Hwf.
    have Hnd := (get_wf_no_default Hwf) idb.
    have Hd := (get_wf_default_last Hwf) idb.
    have Hlen := (get_wf_stages_gt_0 Hwf) idb.

    rewrite /FP_E.get_stage /FP_E.get_stages in Hnd;
    rewrite /FP_E.get_stage /FP_E.get_stages in Hd;
    rewrite /FP_E.get_stages in Hlen;
    rewrite /FP_E.default_stage_id /FP_E.get_stages in Hnd.
    rewrite /FP_E.default_stage_id /FP_E.get_stages in Hd; subst stages.

    induction (get_block_stages (FP_E.get_block fp idb))
      as [|stage stages IHs].
    - rewrite //= in Hlen. lia.
    - rewrite //=. destruct stages as [|stage' stages'] eqn:Hs.

      rewrite /FP_E.default_stage_id //= in Hd; by rewrite Hd.

      rewrite takeinus_one. rewrite  -app_comm_cons -IHs.
      all: try (rewrite //=; lia).

      { move => ids Hlt id.
        have H: S ids
                     < Datatypes.length (stage :: stage' :: stages') - 1.
        rewrite //=. rewrite //= in Hlt. lia.

        have Hnd' := Hnd (S ids) H id. rewrite //= in Hnd'. }

        rewrite //= in Hd. by rewrite //= Nat.sub_0_r.
  Qed.

  (** ** Some remarks about get_fp_blocks *)

  Remark nb_blocks_to_length:
    forall fpe id,
      id < get_nb_blocks fpe - 1
      -> id < length (get_fp_blocks fpe).
  Proof.
    rewrite /get_nb_blocks => fpe id. ssrlia.
  Qed.

  Remark get_fp_blocks_eq:
    forall fpe,
      Datatypes.length (FP_E.get_fp_blocks fpe)
        = Datatypes.length (FP_E_WF.get_fp_blocks fpe).
  Proof. ssrlia. Qed.

  Remark get_nb_blocks_eq:
    forall fpe,
      FP_E.get_nb_blocks fpe
        = FP_E_WF.get_nb_blocks fpe.
  Proof. ssrlia. Qed.

End FP_E_WF.
