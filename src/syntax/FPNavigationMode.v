From Coq Require Import String.
From VFP Require Import BasicTypes.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.

(** Definition of the navigation mode of the autopilot
    Currently, this file define navigation that are specific 
    to Paparazzi autopilot.
    This file contains the common definition needed for the semantics and 
    the generation functions
*)

(** Definition for pitch *)
Variant pitch :=
| AUTO (val: throttle)
| SOME (val: vpitch) (** Default value*)
| NO_PITCH.

(** Definition for vmode *)
Variant vmode :=
| CLIMB (val: climb)
(** Default value with WaypointAlt(wp)*)
| ALT (val: alt) (** Can be a value or a function call (ex: Height(3))*)
| XYZ_VM
| GLIDE (wp: wp_id) (last_wp: last_wp)
| THROTTLE (val: throttle)
| NO_VMODE.

(** Parameters for the Heading function *)

Record fp_params_heading := create_fp_params_heading {
  get_heading_course: course;
  (** until params needed *)
}.
Definition mk_fp_pheading := create_fp_params_heading.

(** Parameters for the Attitude function *)

Record fp_params_attitude := create_fp_params_attitude {
  get_attitude_roll: vroll;
}.
Definition mk_fp_pattitude := create_fp_params_attitude.

(** Parameters for the Manual function *)

Record fp_params_manual := create_fp_params_manual {
  get_manual_pitch: vpitch;
  get_manual_roll: vroll;
  get_manual_yaw: vyaw;
}.
Definition mk_fp_pmanual := create_fp_params_manual.

(** Parameters for the Go function *)

Record fp_params_go := create_fp_params_go {
  get_go_wp: wp_id;
  get_go_from: option wp_id;
  get_go_hmode: hmode;
  get_go_approaching_time: approaching_time;
}.
Definition mk_fp_pgo := create_fp_params_go.

(** Parameter for the XYZ function *)

Definition fp_params_xyz := radius.

(** Parameters for the Circle function *)

Record fp_params_circle := create_fp_params_circle {
  get_circle_wp: wp_id;
  get_circle_radius: radius;
}.
Definition mk_fp_pcircle := create_fp_params_circle.

(** Parameters for the Stay function *)

Record fp_params_stay := create_fp_params_stay {
  get_stay_wp: wp_id;
  get_stay_hmode: hmode;
}.
Definition mk_fp_pstay := create_fp_params_stay.

(** Parameters for the Follow function *)

Record fp_params_follow := create_fp_params_follow {
  get_follow_ac_id: ac_id;
  get_follow_distance: meter;
  get_follow_height: meter;
}.
Definition mk_fp_pfollow := create_fp_params_follow.


(** Parameters for the Survay_rectangle function *)

Record fp_params_survey_rectangle := create_fp_params_survey_rectangle {
  get_srect_grid: meter;
  get_srect_wp1: wp_id;
  get_srect_wp2: wp_id;
  get_srect_orientation: orientation;
}.
Definition mk_fp_psrect := create_fp_params_survey_rectangle.

(** Parameters for the Eight function *)

 Record fp_params_eight := create_fp_params_eight {
  get_eight_center: wp_id;
  get_eight_turn_around: wp_id;
  get_eight_radius: radius;
}.
Definition mk_fp_peight := create_fp_params_eight.


(** Parameters for the Oval function *)

Record fp_params_oval := create_fp_params_oval {
  get_oval_p1: wp_id;
  get_oval_p2: wp_id;
  get_oval_radius: radius;
}.
Definition mk_fp_poval := create_fp_params_oval.

(** Parameters for the Guided function *)

Record fp_params_guided := create_fp_params_guided {
  get_guided_commands: commands;
  get_guided_flags: flags;
}.
Definition mk_fp_pguided := create_fp_params_guided.

(** Common params for nav modes *)

Record fp_params_nav_mode := create_fp_params_nav_mode {
  get_nav_pitch: pitch;
  get_nav_vmode: vmode;
}.
Definition mk_fp_pnav_mode := create_fp_params_nav_mode.

(** Call parameters for nav mode *)
Record fp_params_nav_call := create_fp_params_nav_call {
  get_nav_type: nav_type;
  get_nav_nav_params: option nav_params;
  get_nav_pre_call: option c_code;
  get_nav_post_call: option c_code;
}.
Definition mk_fp_pnav_call := create_fp_params_nav_call.


(** Definition of the navigation modes *)

Variant fp_navigation_mode :=
  HEADING (params:fp_params_heading)
          (params_mode:fp_params_nav_mode)
          (params_call:fp_params_nav_call)
| ATTITUDE (params:fp_params_attitude)
           (params_mode:fp_params_nav_mode)
           (params_call:fp_params_nav_call)
| MANUAL (params:fp_params_manual)
         (params_mode:fp_params_nav_mode)
         (params_call:fp_params_nav_call)
| GO (params:fp_params_go)
     (params_mode:fp_params_nav_mode)
     (params_call:fp_params_nav_call)
| XYZ (params:fp_params_xyz)
      (params_mode:fp_params_nav_mode)
      (params_call:fp_params_nav_call)
| CIRCLE (params:fp_params_circle)
         (params_mode:fp_params_nav_mode)
         (params_call:fp_params_nav_call)
| STAY (params:fp_params_stay)
       (params_mode:fp_params_nav_mode)
       (params_call:fp_params_nav_call)
| FOLLOW (params:fp_params_follow)
         (params_call:fp_params_nav_call)
| SURVEY_RECTANGLE (params:fp_params_survey_rectangle)
                   (params_call:fp_params_nav_call)
| EIGHT (params:fp_params_eight)
        (params_mode:fp_params_nav_mode)
        (params_call:fp_params_nav_call)
| OVAL (params:fp_params_oval)
       (params_mode:fp_params_nav_mode)
       (params_call:fp_params_nav_call)
 | GUIDED (params: fp_params_guided)
                (params_call:fp_params_nav_call)
| HOME.

(** * Definition for getter *)

Definition get_fp_pnav_mode (nav_mode: fp_navigation_mode):
                                    option fp_params_nav_mode :=
  match nav_mode with
  | HEADING _ pm _ => Some pm
  | ATTITUDE _ pm _ => Some pm
  | MANUAL _ pm _ => Some pm
  | GO _ pm _ => Some pm
  | XYZ _ pm _ => Some pm
  | CIRCLE _ pm _ => Some pm
  | STAY _ pm _ => Some pm
  | FOLLOW _ _ => None
  | SURVEY_RECTANGLE _ _ => None
  | EIGHT _ pm _ => Some pm
  | OVAL _ pm _ => Some pm
  | GUIDED _ _ => None
  | HOME => None
  end.

Definition get_fp_pnav_call (nav_mode: fp_navigation_mode):
  option fp_params_nav_call :=
match nav_mode with
| HEADING _ _ pc => Some pc
| ATTITUDE _ _ pc => Some pc
| MANUAL _ _ pc => Some pc
| GO _ _ pc => Some pc
| XYZ _ _ pc => Some pc
| CIRCLE _ _ pc => Some pc
| STAY _ _ pc => Some pc
| FOLLOW _ pc => Some pc
| SURVEY_RECTANGLE _ pc => Some pc
| EIGHT _ _ pc => Some pc
| OVAL _ _ pc => Some pc
| GUIDED _ pc => Some pc
| HOME => None
end.


(** * Definition for the initialisation of the nav mode *)

(** Function that return if the nav mode needs an init case *)
Definition nav_need_init (m: fp_navigation_mode): bool :=
  match m with 
  | EIGHT _ _ _  | OVAL _ _ _ | SURVEY_RECTANGLE _ _ => true
  | _ => false
  end.

(** Function that return true if the nav mode require a nav cond *)
(* Nav cond is of the form:
if (nav_cond) {
  NextStageAndBreakFrom(wp);
}
else {
  NAV_CODE(function)
}
*)
Definition nav_need_cond (m: fp_navigation_mode): bool :=
  match m with
  | GO _ _ _ => true
  | _ => false
  end.

(** * Common Definition for the semantics and the Clight generation *)

(** ** Init code *)
Definition eight_init_code := "nav_eight_init".
Definition oval_init_code := "nav_oval_init".
Definition rectangle_init_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++"SurveyRectangleInit".


(** ** Vmode code *)

(** pitch *)
Definition CARROT_str := "CARROT".
Definition verticalAutoPitchMode_str (nav: nav_type) :=
  nav ++ "VerticalAutoPitchMode".
Definition verticalAutoThrottleMode_str (nav: nav_type)  :=
  nav ++ "VerticalAutoThrottleMode".
Definition RadOfDeg_str := "RadOfDeg".

(** VerticalClimbMode *)
Definition verticalClimbMode_str (nav: nav_type) :=
  nav ++ "VerticalClimbMode".

Definition Height_str := "Height".
Definition WaypointAlt_str := "WaypointAlt".
Definition verticalAltitudeMode_str (nav: nav_type)  :=
  nav ++ "VerticalAltitudeMode".
Definition glide_str (nav: nav_type)  :=
    nav ++ "Glide".
Definition verticalThrottleMode_str (nav: nav_type)  :=
  nav ++ "VerticalThrottleMode".

(** ** Hmode code *)
Definition segment_str (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "Segment".
Definition gotoWaypoint_str (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "GotoWaypoint".


(** Definition of the nav function name *)
Definition heading_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "Heading".
Definition attitude_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "Attitude".
Definition manual_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "SetManual".
Definition xyz_code := "Goto3D".
Definition circle_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "CircleWaypoint".
Definition stay_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "GotoXY".
Definition follow_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "Follow".
Definition survey_rectangle_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "SurveyRectangle".
Definition eight_code := "Eight".
Definition oval_code := "Oval".
Definition guided_code (pc: fp_params_nav_call) :=
  (get_nav_type pc) ++ "Guided".
Definition home_code := "nav_home".
