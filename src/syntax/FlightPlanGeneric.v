From Coq Require Import Lia Nat.


From VFP Require Import BasicTypes.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.

Set Implicit Arguments.

(** * Definition of a generic Flight Plan *)

(** ** Forbidden deroute *)

Record fp_forbidden_deroute := create_fp_fbd {
  get_fbd_from: block_id;
  get_fbd_to: block_id;
  get_fbd_only_when: option c_cond;
}.
Definition mk_fp_fbd := create_fp_fbd.

Definition fp_forbidden_deroutes := list fp_forbidden_deroute.

(** ** Exceptions *)

Record fp_exception := create_fp_exception {
  get_expt_cond: c_cond;
  get_expt_block_id: block_id;
  get_expt_exec: option c_code;
}.
Definition mk_fp_exception := create_fp_exception.

Definition fp_exceptions := list fp_exception.

(** ** Definition of parameters for every stages *)

(** Parameters for the while stage *)

Definition fp_params_while := c_cond.

(** Parameters for the set stage *)

Record fp_params_set := create_fp_pset {
  get_set_var: variable_name;
  get_set_value: c_value;
}.
Definition mk_fp_pset := create_fp_pset.

(** Parameters for the call stage *)

Record fp_params_call := create_fp_pcall {
  get_call_fun: c_code;
  get_call_until: option c_cond;
  get_call_loop: bool; (** default true *)
  get_call_break: bool; (** default false *)
}.
Definition mk_fp_pcall := create_fp_pcall.

(** Parameters for the deroute stage *)

Record fp_params_deroute := create_fp_pderoute {
  get_deroute_block: block_name;
  get_deroute_block_id: block_id;
}.
Definition mk_fp_pderoute := create_fp_pderoute.


Definition fp_params_return := bool. (** reset_stage : default value false*)

(** Conversion of a option C code into a trace *)
Definition opt_c_code_to_trace (exec: option c_code): fp_trace :=
  match exec with
  | None => nil
  | Some e => (C_CODE e) :: nil
  end.

(** * Generic definition of a Flight plan *)

Module Type GENERIC_FP.

  Parameter fp_stage : Set.
  Parameter fp_block : Set.

  (*** Generation of a default block with a specific id *)
  Parameter default_block: block_id -> fp_block.

  Parameter get_block_stages: fp_block -> list fp_stage.
  Parameter get_block_pre_call: fp_block -> option c_code.
  Parameter get_block_post_call: fp_block -> option c_code.
  Parameter get_block_on_enter: fp_block -> option c_code.
  Parameter get_block_on_exit: fp_block -> option c_code.

End GENERIC_FP.

(** Module type for common definitions *)
Module Type COMMON_FP_FUN_SIG (G_FP: GENERIC_FP).
  Import G_FP.

  Definition fp_blocks:= list fp_block.

  Parameter flight_plan: Set.
  Parameter get_fp_forbidden_deroutes:
    flight_plan -> fp_forbidden_deroutes.
  Parameter get_fp_exceptions: flight_plan -> fp_exceptions.
  Parameter get_fp_blocks: flight_plan -> fp_blocks.
  Parameter mk_flight_plan:
    fp_forbidden_deroutes -> fp_exceptions -> fp_blocks -> flight_plan.

  Parameter get_nb_blocks: flight_plan -> nat.
  Axiom nb_blocks_ne_0 :
    forall (fp: flight_plan), get_nb_blocks fp > 0.

  Parameter get_default_block_id: flight_plan -> block_id.
  Parameter get_default_block: flight_plan -> fp_block.

  Parameter get_block: flight_plan -> block_id -> fp_block.

  Parameter get_code_block_pre_call: fp_block -> fp_trace.
  Parameter get_code_block_post_call: fp_block -> fp_trace.
  Parameter get_code_on_enter: fp_block -> fp_trace.
  Parameter get_code_on_exit :fp_block -> fp_trace.

  Parameter on_enter: flight_plan -> block_id -> fp_trace.
  Parameter on_exit: flight_plan -> block_id -> fp_trace.

  Parameter normalise_block_id: flight_plan -> block_id -> block_id.
End COMMON_FP_FUN_SIG.


(** * Implementation of Common functions/definitions for Flight Plan *)
Module COMMON_FP_FUN (G_FP: GENERIC_FP) <: COMMON_FP_FUN_SIG G_FP.
  Import G_FP.

  (** Blocks *)
  Definition fp_blocks := list fp_block.

  (** Flight plan *)
  Record flight_plan_def := create_flight_plan {
    get_fp_forbidden_deroutes: fp_forbidden_deroutes;
    get_fp_exceptions: fp_exceptions;
    get_fp_blocks: fp_blocks;
  }.
  Definition flight_plan := flight_plan_def.
  Definition mk_flight_plan := create_flight_plan.

  (** * Definitions of functions  *)

  (** Definitions of getter functions for all the elements of the flight 
      plan *)

  (** ** Flight plan *)

  (** ** Functions for blocks *)
  Definition get_nb_blocks (fp: flight_plan): nat :=
    length (get_fp_blocks fp) + 1. (** Default block *)

  Lemma nb_blocks_ne_0:
    forall (fp: flight_plan), get_nb_blocks fp > 0.
  Proof.
    intro fp. unfold get_nb_blocks. lia.
  Qed.

  (** Generate the default block, positionned at the end *)
  Definition get_default_block_id (fp: flight_plan): block_id :=
    length (get_fp_blocks fp).

  Definition get_default_block (fp: flight_plan): fp_block :=
    default_block (get_default_block_id fp).

  (** Get a block corresponding to the id. 
      If there no corresponding block to the id the default 
      block is returned. 
    *)
  Definition get_block (fp: flight_plan) (id: block_id): fp_block :=
    List.nth id (get_fp_blocks fp) (get_default_block fp).

  (** Getter for block params *)
  Definition get_code_block_pre_call (block: fp_block): fp_trace := 
    opt_c_code_to_trace (get_block_pre_call block).

  Definition get_code_block_post_call (block: fp_block): fp_trace := 
    opt_c_code_to_trace (get_block_post_call block).

  Definition get_code_on_enter (block: fp_block): fp_trace := 
    opt_c_code_to_trace (get_block_on_enter block).

  Definition get_code_on_exit (block: fp_block): fp_trace := 
    opt_c_code_to_trace (get_block_on_exit block).

  (** Getter for enter/exit code of a block *)
  Definition on_enter (fp: flight_plan) (id: block_id): fp_trace :=
    get_code_on_enter (get_block fp id).

  Definition on_exit (fp: flight_plan) (id: block_id): fp_trace :=
    get_code_on_exit (get_block fp id).

  (** Return the minimal value between the last_block id
        and the new block id *)
  Definition normalise_block_id (fp: flight_plan) (new_id: block_id): block_id := 
    let nb_blocks := get_nb_blocks fp in
    if (nb_blocks <=? new_id)%nat then
      nb_blocks - 1
    else new_id.

  Lemma default_block_id_lt_nb_blocks:
    forall fp,
      get_default_block_id fp < get_nb_blocks fp.
  Proof.
    rewrite /get_default_block_id /get_nb_blocks. lia.
  Qed.

End COMMON_FP_FUN.
