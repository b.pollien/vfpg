From Coq Require Import Arith List Program Lia.

From VFP Require Import BasicTypes FlightPlanGeneric 
                        FlightPlanExtended CommonLemmasNat8.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.

Import FP_E_WF.

Set Implicit Arguments.

(** Definition of dependent type for a well-sized flight plan *)
(** A flight plan must contains less then 256 stages,         *)
(** the exceptions, forbidden_deroutes and deroutes must      *)
(** refer to correct block id.                                *)

(** * Properties about a block id are nat8 or correct block id *)

(** Property about correct block id *)
Definition correct_block_id (fp: flight_plan) (id: block_id): Prop :=
  (id < get_nb_blocks fp).

Definition is_user_id (fp: flight_plan) (id: block_id): Prop :=
  (id < get_nb_blocks fp - 1).

Lemma normalise_block_id_is_correct : 
  forall (fp: flight_plan) (id: block_id),
  correct_block_id fp (normalise_block_id fp id).
Proof.
  intros fp id. unfold normalise_block_id.
  destruct (get_nb_blocks fp <=? id) eqn:E. 
  - unfold correct_block_id. unfold get_nb_blocks. lia.
  - apply leb_complete_conv in E. apply E.
Qed.

Lemma user_id_is_correct :
  forall (fp: flight_plan) (id: block_id),
  is_user_id fp id ->
    correct_block_id fp id.
Proof.
  intros fp id Huser. unfold correct_block_id.
  unfold is_user_id in Huser.
  lia.
Qed. 

(** ** Properties about exceptions *)

Definition correct_excpt (fp: flight_plan) (ex: fp_exception): Prop :=
  is_user_id fp (get_expt_block_id ex).

(** ** Properties about forbidden deroutes *)

Record correct_fbd_deroute (fp: flight_plan) (fbd: fp_forbidden_deroute) :=
  create_correct_fbd_deroute {
    get_correct_from: is_user_id fp (get_fbd_from fbd);
    get_correct_to: is_user_id fp (get_fbd_to fbd);
  }.

(** ** Properties about deroute *)

Definition correct_deroutes (fp: flight_plan) (block: fp_block): Prop :=
  forall ids ids' idd params,
    List.nth ids (get_block_stages block) (DEFAULT idd) 
      = DEROUTE ids' params
    -> is_user_id fp (get_deroute_block_id params).

(** * Property that blocks are well numbered *)
Definition well_numbered_blocks (fp: flight_plan): Prop :=
  forall i, i < get_nb_blocks fp
    -> (get_block_id (get_block fp i)) = i.

(** * Global property that blocks contain less
        than 256 stages, and the exception/deroute must be correct *)

Record well_sized_block (fp: flight_plan) (block: fp_block) :=
  create_well_sized_block {
      get_nb_stage8: is_nat8 (length (get_block_stages block));
      get_correct_lexcpts:
      Forall (correct_excpt fp) (get_block_exceptions block);
      get_correct_deroute: correct_deroutes fp block;
  }.

(** * Properties about the flight plan *)

(** ** Property about that there is less than 256 blocks*)
Definition nb_blocks_lt_256 (fp: flight_plan): Prop :=
    is_nat8 (get_nb_blocks fp).

(** ** Global property about the flight plan. *)
(** It must contains less than 256 blocks, the block must be *)
(** well-size, and the global exceptions and forbidden deroutes *)
(** must be correct.     *)

Record verified_fp_e (fp: flight_plan) := create_verified_fp_e {
    get_nb_blocks8: nb_blocks_lt_256 fp;
    get_well_numbered_blocks: well_numbered_blocks fp;
    get_well_sized_blocks : forall i, well_sized_block fp (get_block fp i);
    get_correct_gexcpts: Forall (correct_excpt fp) (get_fp_exceptions fp);
    get_correct_fbd: Forall (correct_fbd_deroute fp)
                            (get_fp_forbidden_deroutes fp)
  }.

Definition verified_fp_e_wf (fp: flight_plan_wf) :=
  verified_fp_e (` fp).

Definition flight_plan_sized
    := {fp: flight_plan_wf | verified_fp_e_wf fp}.

(** Getter for the flight plan *)
Definition get_fp (fp: flight_plan_sized): flight_plan :=
  ` (` fp).

(** Result of the size analysis *)
Variant res_size_analysis :=
    | OK (fp: flight_plan_sized)
    | ERR (err: list err_msg).

Lemma user_id_is_nat_8 :
  forall (fps : flight_plan_sized) (id : block_id),
  is_user_id (` (` fps)) id -> is_nat8 id.
Proof.
  intros fps id.
  rewrite /is_user_id /is_nat8.
  have H8 := get_nb_blocks8 (proj2_sig fps).
  rewrite /nb_blocks_lt_256 /is_nat8 in H8.
  lia.
Qed.