From Coq Require Import String.

(** This file containing all basic types used to define flight plan *)
(** and for the verification of the preservation of the semantics.  *)

(** ** Basics Types for Flight Plans  *)

(** *** Name *)
Definition name := string.
Definition block_name := name.
Definition variable_name := name.

(** *** Identifiers *)

 (** Id for blocks *)
Definition block_id := nat.

(** Id for stages *)
Definition stage_id := nat.

(** Id for waypoints *)
Definition wp_id := nat.
Definition last_wp := string.

(** *** Definitions for stages *)
Definition course := string.
Variant hmode :=
  | ROUTE (wp: wp_id) (l_wp: last_wp)
  (** Default *)
  | DIRECT (wp: wp_id).
Definition throttle := string.
Definition climb := string.
Definition vpitch := string.
Definition vroll := string.
Definition vyaw := string.
Definition nav_type := string.
Definition nav_params := string.
Definition ac_id := string.
Definition commands := string.
Definition flags := string.
Variant orientation :=
| NS
| WE.

(** Represent a numerical value in meter *)
Definition meter := string. 
Definition radius := string.

(** ***Variant for the representation of the altitude *)
Variant alt :=
(** Absolute altitude value *)
| ABS (alt: string)
(** Relative value from the ground *)
| REL (height: string)
(** Altitude of a waypoints *)
| WP (id: wp_id).


(** *** Variant to represent the different possibility of approaching time *)
Variant approaching_time :=
(** Approaching time *)
| AT (a_t: string)
(** Exceeding time (-et) *)
| ET (e_t: string)
(** CARROT constant *)
| CARROT.

(** ** Execution *)

(** C Code *)
Definition code := string.
Definition c_code := code.
(** Condition for exceptions, loops or until *)
Definition c_cond := code.
(** Value for set, the code must return a value *)
Definition c_value := code.

(** ** Error message *)

Variant err_msg :=
| WARNING (w: string)
| ERROR (e: string).

(** Outputs/Trace *)

Variant fp_event :=
(** Evaluation of a condition *)
| COND (cond: c_cond) (res: bool)
(** Execution of an arbitrary C code *)
| C_CODE (c_code: c_code)
| SKIP.
Definition fp_trace := list fp_event.
