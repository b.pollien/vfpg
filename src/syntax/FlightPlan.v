From Coq Require Import String List.


From VFP Require Import BasicTypes FPNavigationMode
                        FlightPlanGeneric.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope nat_scope.
Local Open Scope list_scope.

Set Implicit Arguments.

(********************************************************************)
(** Definition of the Coq flight plan                               *)
(** These structures represent a subset of the initial flight plan. *)
(** They contains all the information needed to generate the        *)
(** navigation function and the associated functions.               *)
(** The definition of all the paramaters can be found in the        *)
(** paparazzi documentation:                                        *)
(** (https://wiki.paparazziuav.org/wiki/Flight_Plans)               *)
(** The FP version is close to the XML definition of the flight     *)
(** plan.                                                           *)
(********************************************************************)

Module FP.

  (** Definition of the not numbered version of the flight plan *)

  (** ** Stages *)
  Inductive fp_stage :=
  WHILE (params: fp_params_while) (body: list fp_stage)
  | SET (params: fp_params_set)
  | CALL (params: fp_params_call)
  | DEROUTE (params: fp_params_deroute)
  | RETURN (params: fp_params_return)
  | NAV (nav_mode: fp_navigation_mode)
        (until: option c_cond)
        (init: bool).

  (** ** Blocks *)
  Record fp_block := create_fp_block {
    get_block_name: block_name;
    get_block_id: block_id;
    get_block_stages: list fp_stage;
    get_block_exceptions: fp_exceptions;
    get_block_pre_call: option c_code;
    get_block_post_call: option c_code;
    get_block_on_enter: option c_code;
    get_block_on_exit: option c_code;
  }.
  Definition mk_fp_block := create_fp_block.



  Module Def <: GENERIC_FP.
    Definition fp_stage := fp_stage.
    Definition fp_block := fp_block.

    (** Default stage brings Home*)
    Definition default_block (id: block_id):=
      mk_fp_block
        "HOME" id
        (NAV HOME None (nav_need_init HOME) :: nil)
        nil None None None None.

    Definition get_block_stages := get_block_stages.
    Definition get_block_pre_call := get_block_pre_call.
    Definition get_block_post_call := get_block_post_call.
    Definition get_block_on_enter := get_block_on_enter.
    Definition get_block_on_exit := get_block_on_exit.
  End Def.

  Module Common := COMMON_FP_FUN Def.
  Include Common.

  (** Get the list of stage from an block id *)
  Definition get_stages (fp: flight_plan) (id: block_id) : list fp_stage :=
    get_block_stages (get_block fp id).

  (** Definition of a inductive principle for the stages *)
  Section fp_stage_ind'.
    Variable P : fp_stage -> Prop.

    Hypothesis WHILE_case:
      forall (p : fp_params_while) (block : list fp_stage),
        Forall P block -> P (WHILE p block).

    Hypothesis CALL_case:
      forall params : fp_params_call, P (CALL params).

    Hypothesis SET_case:
      forall params : fp_params_set, P (SET params).

    Hypothesis DEROUTE_case:
      forall params : fp_params_deroute, P (DEROUTE params).

    Hypothesis RETURN_case :
      forall params : fp_params_return, P (RETURN params).

    Hypothesis NAV_case:
      forall (nav_mode : fp_navigation_mode) (until : option c_cond)
                (init : bool), P (NAV nav_mode until init).

    Definition prog_ind' (ir: forall s, P s) : forall b, Forall P b :=
      list_ind (Forall P)
               (Forall_nil _)
               (fun s l => @Forall_cons _ _ s l (ir s)).

    Fixpoint fp_stage_ind' (s : fp_stage) : P s :=
      match s with
        | WHILE p b => 
            WHILE_case p (prog_ind' fp_stage_ind' b)
        | CALL p => CALL_case p
        | SET p => SET_case p
        | DEROUTE p => DEROUTE_case p
        | RETURN p => RETURN_case p
        | NAV m u i => NAV_case m u i
      end.

  End fp_stage_ind'.

End FP.


