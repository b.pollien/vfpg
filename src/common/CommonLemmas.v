From Coq Require Import Arith ZArith Psatz Bool
                        String List Program.Equality Program.Wf.
From compcert Require Import Coqlib.
Require Import Relations.
Require Import Recdef.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.
Import ListNotations.

(** Common lemmas manipulating list, Prop and nat *)

Section List.
  Variable A: Type.
  Variable B: Type.

  Lemma cons_last:
    forall (e e' d : A) l,
      List.last (e :: e' :: l) d = List.last (e' :: l) d.
  Proof.
    by [].
  Qed.

  Lemma map_last:
    forall (f: A -> B) d0 l0 e,
      e = List.last l0 d0
      -> f e = List.last (List.map f l0) (f d0).
  Proof.
    move => f d0 [|e' l0] e.
    - move => He; by rewrite He.
    - generalize e e'; induction l0 as [| e'' l0' IHl0'];
      move => e0 e0' He.
      * by rewrite He.
      * rewrite cons_last in He.
        apply (IHl0' e0 e'' He).
  Qed.

  Lemma map_last_split:
    forall (f: A -> B) d0 d1 l0 l1 e,
      d1 = f d0
      -> e = List.last l0 d0
      -> l1 = map f l0
      -> f e = List.last l1 d1.
  Proof.
    move => f d0 d1 l0 l1 e Hd He Hl.
    rewrite Hl Hd. by apply map_last.
  Qed.

  Lemma app_eq_nil:
    forall (l l': list A),
      l ++ l' = l
      -> l' = [].
  Proof.
    induction l; try by []; intros.
    rewrite //= in H. injection H. apply IHl.
  Qed.

  Lemma app_eq_refl:
    forall (l: list A) l' l'',
      l ++ l' = l ++ l'' <-> l' = l''.
  Proof.
    induction l as [|e l IHl]; move => l' l''; split; move => Heq;
      try by rewrite Heq.
    - rewrite //= in Heq.
    - inversion Heq. by apply IHl.
  Qed.

  Lemma nth_remove_app:
    forall (a s d: A) n l,
      a <> s
      -> d <> s
      -> List.nth n (l ++ [a]) d = s 
      -> List.nth n (l ++ [a]) d = List.nth n l d.
  Proof.
    move => a s d;
    induction n as [|n IHs]; move => [|e l] He Hd Hnth; try by [].
    - by destruct n.
    - rewrite //= IHs; try by [].
  Qed.

  Lemma nth_remove_app':
    forall (a s d: A) n l,
      a <> s
      -> d <> s
      -> List.nth n l d = s 
      -> List.nth n (l ++ [a]) d = List.nth n l d.
  Proof.
    move => a s d;
    induction n as [|n IHs]; move => [|e l] He Hd Hnth; try by [].
    rewrite //= IHs; try by [].
  Qed.

  Lemma nth_remove_app_ne:
    forall (d: A) n l,
      List.nth n (l ++ [d]) d <> d
      -> List.nth n (l ++ [d]) d = List.nth n l d.
  Proof.
    move => d;
    induction n as [|n IHs]; move => [|e l] Hnth; try by [].
    - by destruct n.
    - rewrite //= IHs; try by [].
  Qed.

  Lemma nth_app_ne_impl_lt:
    forall (d: A) n l,
      List.nth n (l ++ [d]) d <> d
      -> n < length l.
  Proof.
    move => d;
    induction n as [|n IHs]; move => [|e l] Hnth; try by [].
    - rewrite //=; lia.
    - by destruct n.
    - rewrite //= in Hnth. apply IHs in Hnth.
      rewrite //=; lia.
  Qed.

  Lemma forall_to_Forall:
    forall (P: A -> Prop),
      (forall a, P a)
      -> (forall (l: list A), Forall P l).
  Proof.
    move => P HP; induction l as [|a' l IHl]; try by [].
    apply Forall_cons_iff; split; by [].
  Qed.

  Lemma nth_app_split:
    forall n l (a d s:A) ,
      d <> s
      -> List.nth n (l ++ [a]) d = s
      -> n = length l \/ n < length l.
  Proof.
    induction n as [|n IHn]; destruct l as [|e l']; 
      rewrite //= => a d s Hne Hnth; try by [].
    - by left.
    - right; lia.
    - destruct n; by rewrite Hnth in Hne.
    - apply (IHn l' a d s Hne) in Hnth; destruct Hnth as [Heq | Hlt].
      * left; by rewrite Heq.
      * right; lia.
  Qed.

  Lemma and_not_or:
    forall P Q : Prop, 
      ~ P /\ ~ Q
      -> ~ (P \/ Q).
  Proof.
    move => P Q [Hp Hq] H'.
    by destruct H'.
  Qed.


  (** ** List Functions Definitions *)

  Definition subseq (stages: list A) (from: nat) (to: nat):
                              list A :=
    take (to - (from + 1))
        (drop (1 + from) stages).

  Lemma drop_0:
    forall (l: list A), drop 0 l = l.
  Proof. destruct l; by []. Qed.

  Lemma take_0:
    forall (l: list A), take 0 l = [].
  Proof. destruct l; by []. Qed.

  Lemma take_cat:
    forall n (l l': list A),
      n <= length l
      -> take n (l ++ l') = take n l.
  Proof.
    induction n as [|n IHn]; move => l l' Hl.
    - by repeat rewrite take_0.
    - destruct l as [|e l].
      * by apply Nat.nlt_0_r in Hl.
      * rewrite //= IHn. by []. rewrite //= in Hl; lia.
  Qed.

  Lemma take_length:
    forall (l l': list A),
      take (length l) (l ++ l') = l.
  Proof.
    induction l as [| e l IHl]; move => l'; rewrite //=.
    - by destruct l'.
    - by rewrite IHl.
  Qed.

  Lemma subseq_cons:
    forall e l i j,
      i > 0
      -> j >= i
      -> subseq (e :: l) i j = subseq l (i - 1) (j - 1).
  Proof.
    move => e l i j Hi Hle.
    rewrite /subseq //=.
    replace (j - 1 - (i - 1 + 1)) with (j - (i + 1)); try lia.
    replace (S (i - 1)) with i; try lia.
    by [].
  Qed.

  Lemma subseq_app1:
  forall l l' i j,
    j >= i
    -> j < length l
    -> subseq l i j = subseq (l ++ l') i j.
  Proof.
    induction l as [|e l IHl]; move => l' i j Hlt Hlen.
    - by apply Nat.nlt_0_r in Hlen.
    - rewrite /subseq //=.
      rewrite /subseq in IHl.
      destruct i.
      * do 2 rewrite drop_0 //=. rewrite take_cat; try by [].
        rewrite //= in Hlen; lia.
      * destruct j; try by do 2 rewrite take_0.
        replace (S j - (S i + 1)) with (j - (i + 1)); try lia.
        apply IHl; rewrite //= in Hlen; try lia.
  Qed.

  Lemma subseq_app2:
  forall l l' i j,
    j >= i
    -> i >= length l
    -> subseq (l++l') i j = subseq l' (i - length l) (j - length l).
  Proof.
    induction l as [|e l IHl]; move => l' i j Hlt Hlen.
    - by do 2 (rewrite //= Nat.sub_0_r).
    - have Hpred: forall i j, i - S j = (i - 1) - j by lia.
      rewrite subseq_cons //=; try lia.
      rewrite (Hpred i (length l)) (Hpred j (length l)) IHl;
        try by []; try lia.
      all: rewrite //= in Hlen; lia.
  Qed.

  (** ** Properties about partition *)
  Remark partition_nil_test_true:
    forall (f: A -> bool) e l l1,
      partition f (e :: l) = (l1,nil)
      -> f e = true.
  Proof.
    move => f e l l1 H.
    destruct (f e) eqn:Ht; try by [].
    rewrite //= Ht in H. destruct (partition f l).
    inversion H.
  Qed.

  Remark partition_test_false:
    forall (f: A -> bool) e e' l l1 l2,
    f e' = false
    -> partition f (e' :: l) = (l1, e :: l2)
    -> e = e'.
  Proof.
    move => f e e' l l1 l2 Ht Hp.
    destruct (partition f l) as [l1' l2'] eqn:Hp'.
    rewrite //=  Hp' Ht in Hp; by inversion Hp.
  Qed.

  Remark partition_cons1_inv:
    forall (f: A -> bool) e l l1 l2,
      f e = true
      -> partition f (e :: l) = (e:: l1, l2)
      -> partition f l = (l1, l2).
  Proof.
    move => f e l l1 l2 Ht Hp.
    destruct (partition f l) eqn:Hpl.
    by rewrite //= Hpl Ht in Hp; inversion Hp.
  Qed. 

  Remark partition_cons2_inv:
    forall (f: A -> bool) e l l1 l2,
      f e = false
      -> partition f (e :: l) = (l1, e :: l2)
      -> partition f l = (l1, l2).
  Proof.
    move => f e l l1 l2 Ht Hp.
    destruct (partition f l) eqn:Hpl.
    by rewrite //= Hpl Ht in Hp; inversion Hp.
  Qed.

  Remark partition_false_len:
    forall (f: A -> bool) e l l1 l2,
    f e = true
    -> partition f (e :: l) = (l1, l2)
    -> length l1 > 0.
  Proof.
    move => f e l l1 l2 Ht Hp.
    destruct (partition f l) as [l1' l2'] eqn:Hl'.
    rewrite //= Ht Hl' in Hp; inversion Hp.
    simpl. lia.
  Qed.

  Lemma partition_forall:
    forall (P: A -> bool) l l1 l2,
      partition P l = (l1, l2)
      -> Forall P l1.
  Proof.
    move => P; induction l as [| e l' IHl]; move => l1 l2 Hp.
    - by inversion Hp.
    - destruct (partition P l') as [l1' l2'] eqn:Hp';
      destruct (P e) eqn:HP.
      + rewrite //= Hp' HP in Hp; inversion Hp; subst l1 l2.
        apply Forall_cons; try by []. eapply IHl; by reflexivity.
      + rewrite //= Hp' HP in Hp; inversion Hp ; subst l1 l2.  
        eapply IHl; by reflexivity.
  Qed.

  Lemma Forall_P_partition:
  forall (P: A -> Prop) (f: A -> bool) l l1 l2,
    Forall P l
    -> partition f l = (l1, l2)
    -> Forall P l1.
  Proof.
  move => P f; induction l as [| e l' IHl]; move => l1 l2 HP Hp.
  - by inversion Hp.
  - destruct (partition f l') as [l1' l2'] eqn:Hp';
    destruct (f e) eqn:Hres.
    + rewrite //= Hp' Hres in Hp; inversion Hp; subst l1 l2.
      apply Forall_cons; try by []. apply (Forall_inv HP).
      eapply IHl. apply (Forall_inv_tail HP). by reflexivity.
    + rewrite //= Hp' Hres in Hp; inversion Hp ; subst l1 l2.  
      eapply IHl. apply (Forall_inv_tail HP). by reflexivity.
  Qed.

  Record disjoint_function (f f': A -> bool) :=
      create_disjoint_function {
    get_disjoint: forall e, f e = true -> f' e = false;
    get_disjoint': forall e, f' e = true -> f e = false;
  }.

  Lemma partition_eq:
    forall (f: A -> bool) f' l2' l l1 l2 l1',
    disjoint_function f f'
    -> partition f l = (l1, l2)
    -> partition f' l = (l1', l2')
    -> exists l2'', partition f l2' = (l1, l2'').
  Proof.
    move => f f'; induction l2' as [|e l2' IHl2];
      induction l as [|e' l' IHl']; move =>  l1 l2 l1' Hd Hp Hp';
      have Hne := get_disjoint Hd; have Hne' := get_disjoint' Hd.
    - inversion Hp'; inversion Hp. by exists nil.
    - have Ht := (partition_nil_test_true Hp').
      destruct (partition f l') as [l1'' l2''] eqn:Hpl.
      rewrite (partition_cons2 _ _ _ Hpl (Hne' _ Ht)) in Hp;
      inversion Hp; subst l1 l2.
      destruct (partition f' l') as [l0 l0'] eqn:Hpl'.
        rewrite (partition_cons1 _ _ _ Hpl' Ht) in Hp';
        inversion Hp'; subst l1' l0'.
      eapply (IHl' _ _ _ Hd); try reflexivity. 
    - inversion Hp'. 
    - destruct (f' e') eqn:Ht'.
      + destruct (partition f l') as [l1'' l2''] eqn:Hpl;
          rewrite //= Hpl (Hne' _ Ht') in Hp; inversion Hp; subst l1 l2.
        destruct (partition f' l') as [l0 l0'] eqn:Hpl';
          rewrite //= Hpl' Ht' in  Hp'; inversion Hp'; subst l1' l0'.
        eapply (IHl' _ _ _ Hd); try reflexivity.
      + have He := partition_test_false Ht' Hp'; subst e'.
          have Hpl' := partition_cons2_inv Ht' Hp'.
          destruct (f e) eqn:Ht;
            destruct (partition f l') as [l1'' l2''] eqn:Hpl;
            rewrite //= Ht Hpl in Hp; inversion Hp; subst l1 l2;
            destruct (IHl2 _ _ _ _ Hd Hpl Hpl') as [l2_f Hp_f].
          * eexists. by apply (partition_cons1 _ _ _ Hp_f Ht).
          * eexists. by rewrite //= Hp_f Ht.
  Qed.

  Lemma In_app_rev:
    forall (x: A) l l',
      In x (l ++ l')
      -> In x (l' ++ l).
  Proof.
    move => x l l' H.
    apply in_or_app.
    destruct (in_app_or _ _ _ H) as [H' | H'].
    by right. by left.
  Qed.

  Lemma list_norepet_append_triple:
    forall (a b c: list A),
      list_norepet (a ++ b ++ c)
      -> list_norepet (b ++ a ++ c).
  Proof.
    move => a b c H.
    rewrite -> app_assoc.
    apply list_norepet_append_commut.
    rewrite -> app_assoc.
    apply list_norepet_append_commut.

    apply list_norepet_app in H.
    destruct H as [Ha [Hbc Hd]].

    apply list_norepet_append => //.
    - by apply list_norepet_append_commut.
    - move => x y HIa HIbc.
      apply In_app_rev in HIbc.
      by apply (Hd _ _ HIa HIbc).
  Qed.

  Lemma partition_ind:
    forall (P: list A -> list A -> Prop) f,
    P nil nil 
    -> (forall e l l1 l2, 
          f e = true
          -> partition f (e :: l) = (l1, l2)
          -> P (e :: l) l1)
    -> (forall e, f e = false
          -> (exists f',
                disjoint_function f f'
                /\ f' e = true
                /\forall l l1 l2 l1' l2',
                  partition f l = (l1, l2)
                  -> partition f' (e:: l) = (l1', l2')
                  -> P l2' l1
                  -> P (e :: l) l1))
    -> forall l, forall l1 l2, partition f l = (l1, l2) -> P l l1.
  Proof.
    move => P f Hnil Hconstt Hconstf.

    apply (induction_ltof1 (list A) (@length _) 
      (fun l => forall l1 l2, partition f l = (l1, l2) -> P l l1));
    move => [|e l] IHl l1 l2 Hp.

    - by inversion Hp.
    - destruct (f e) eqn:Ht.
      + apply (Hconstt _ _ _ _ Ht Hp).
      + destruct (Hconstf e Ht) as [f' [Hf' [Hf_true Hconstf']]]. 

        destruct (partition f l) as [l1' l2'] eqn:Hpl.
        destruct l2 as [|e' l2''] eqn:Hl2; have Hp' := Hp;
          rewrite //= Hpl Ht in Hp';  inversion Hp'; subst l1' e' l2''.

        destruct (partition f' (e :: l)) as [l1'' l2''] eqn:Hpf'.

        destruct (partition_eq Hf' Hp Hpf') as [ll2'_f Hl2']. 

        eapply (Hconstf' _ _ _ _ _ Hpl Hpf').

        eapply IHl.
        * rewrite /ltof //=. have Hlt:= partition_false_len Hf_true Hpf'.
            apply  partition_length in Hpf', Hl2'.
            rewrite //= in Hpf', Hl2'; rewrite Hpf'. lia.
        * apply Hl2'.
  Qed.

End List.
