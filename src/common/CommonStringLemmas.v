From Coq Require Import String Ascii DecimalNat Lia Arith.
Require Import DecimalString.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope nat_scope.
Local Open Scope string_scope.

(** Common functions and lemmas about string *)

(** * String conversion functions *)

(** Converts a nat into a string *)
Definition string_of_nat (n: nat) :=
  NilEmpty.string_of_uint (Nat.to_uint n).

(** * Common string lemmas *)

Lemma string_length_app:
  forall s1 s2,
    String.length (s1 ++ s2)
    = String.length s1 + String.length s2.
Proof. 
  induction s1 as [|c s1 IHs1]; rewrite //= => s2.
  rewrite IHs1. lia.
Qed.

Lemma string_nil_app:
  forall s1 s2, 
  s2 = s1 ++ s2
  <-> s1 = "".
Proof.
  induction s1; move => s2; split; move => H; try by [].
  have Hl := (string_length_app (String a s1) s2).
  rewrite -H //= in Hl. lia.
Qed.

Lemma ne_string_cons:
  forall c s1 s2,
    String c (s1 ++ s2) <> s2.
Proof.
  move => c s1 s2 H.
  have Heq: String c (s1 ++ s2) =  ((String c s1) ++ s2)%string by [].
  have Hl := (string_length_app (String c s1) s2).
  rewrite -Heq H //= in Hl. lia.
Qed.

Lemma string_app_assoc:
  forall s1 s2 s3,
    s1 ++ s2 ++ s3 = (s1 ++ s2) ++ s3.
Proof.
  induction s1 as [| c1 s1 IHs1]; try by [].
  move => s2 s3. by rewrite //= IHs1.
Qed.

Lemma eq_app_fst_string:
  forall s1 s2 s2',
  s2 = s2' <-> s1 ++ s2 = s1 ++ s2'.
Proof.
  move => s1 s2 s2'; split; move => H.
  - by rewrite H.
  - induction s1 as [| a s1 IHs1]; try by [].
    rewrite //= in H; inversion H as [[Hs]].
    apply (IHs1 Hs).
Qed.

Lemma ne_app_fst_string:
  forall s1 s2 s2',
  s2 <> s2' <-> s1 ++ s2 <> s1 ++ s2'.
Proof.
  move => s1 s2 s2'; split; move => H H'.
  - by apply eq_app_fst_string in H'.
  - by apply (proj1 (eq_app_fst_string s1 s2 s2')) in H'.
Qed.

Lemma eq_app_lst_string:
  forall s1 s1' s2,
  s1 = s1' <-> s1 ++ s2 = s1' ++ s2.
Proof.
  move => s1 s1' s2; split; move => H.
  - by rewrite H.
  - generalize dependent s1'. induction s1 as [|c s1 IHs1]; move => s1' H.
    * rewrite //= in H. by apply string_nil_app in H.
    * destruct s1' as [|c' s1'].
      + rewrite //= in H. by apply ne_string_cons in H.
      + rewrite //= in H; inversion H as [[Hc Hs]].
         apply IHs1 in Hs; by subst s1'.
Qed.

Lemma ne_app_lst_string:
  forall s1 s1' s2,
    s1 <> s1' -> s1 ++ s2 <> s1' ++ s2.
Proof.
  move => s1 s1' s2; move => H H'.
  by apply eq_app_lst_string in H'.
Qed.

(** * Properties about nat conversion*)

(** ** Properties about uint *)

Lemma eq_string_to_uint:
  forall n n',
    n = n' <-> NilEmpty.string_of_uint n = NilEmpty.string_of_uint n'.
Proof.
  move => n n'; split.
  - move => H; by rewrite H.
  - generalize dependent n';
    induction n
      as [|n IHn|n IHn|n IHn|n IHn|n IHn|n IHn|n IHn|n IHn|n IHn|n IHn];
      rewrite //= => n' H; destruct n'; rewrite //= in H;
      inversion H as [[Hs]]; try by rewrite (IHn _ Hs).
Qed.

(** ** Properties about string_of_nat *)

Lemma eq_string_of_nat:
  forall n n',
    n = n' <-> string_of_nat n = string_of_nat n'.
Proof.
  move => n n'; split; move => H.
  - by rewrite H.
  - apply eq_string_to_uint in H.
    by apply Unsigned.to_uint_inj.
Qed.

Lemma ne_string_of_nat:
  forall n n',
    n <> n' <-> (string_of_nat n) <> (string_of_nat n').
Proof.
  move => n n'; split; move => H H'.
  - by apply eq_string_of_nat in H'.
  - by apply (proj1 (eq_string_of_nat n n')) in H'.
Qed.

Lemma get_nb_ne_underscore:
  forall n i,
    get i (string_of_nat n) <> Some "_"%char.
Proof. 
  rewrite /string_of_nat => n. elim: (Nat.to_uint n) => // u IH i /=.
  all: case: i => //.
Qed.

Lemma get_le_none:
  forall s i,
    String.length s <= i -> get i s = None.
Proof.
  move => s. elim: s => [// | a s IH i /= H].
  case: i H => [H' | n H']; first by lia.
  apply IH. lia.
Qed.

Lemma eq_app_string_of_nat:
  forall n n' s s',
    string_of_nat n ++ "_" ++ s = string_of_nat n' ++ "_" ++ s'
    ->  string_of_nat n ++ "_" =  string_of_nat n' ++ "_".
Proof.
  move => n n' s s' H.
  apply get_correct => i.
  set ln := String.length ((string_of_nat n) ++ "_").
  set ln' := String.length ((string_of_nat n') ++ "_").
  have H': ln = ln'.
    { have [[Hlt | //] | Hge] := lt_eq_lt_dec ln ln'.
      - have Hget :
          get (Nat.pred ln) (string_of_nat n ++ "_" ++ s) = Some "_"%char.
        { have -> : Nat.pred ln = 0 + String.length (string_of_nat n).
          * rewrite plus_O_n /ln string_length_app //=. lia.
          * by rewrite -append_correct2. }

        have Hget':
          get (Nat.pred ln) (string_of_nat n' ++ "_" ++ s')
           <> Some "_"%char.
        { rewrite -append_correct1.
          * apply get_nb_ne_underscore. rewrite /ln string_length_app //=.
          * rewrite /ln /ln' ?string_length_app //= in Hlt. lia. }

        rewrite H in Hget. by rewrite Hget in Hget'.

        - have Hget :
            get (Nat.pred ln') (string_of_nat n' ++ "_" ++ s')
              = Some "_"%char.
          { have -> : Nat.pred ln' = 0 + String.length (string_of_nat n').
            * rewrite plus_O_n /ln' string_length_app //=. lia.
            *  by rewrite -append_correct2. }

          have Hget': get (Nat.pred ln') (string_of_nat n ++ "_" ++ s) <> Some "_"%char.
          { rewrite -append_correct1.
            * apply get_nb_ne_underscore.
              rewrite /ln' string_length_app //=.
            * rewrite /ln /ln' ?string_length_app //= in Hge. lia. }

          rewrite H in Hget'. by rewrite Hget in Hget'.
       }

  have [Hge | Hlt] := (le_lt_dec ln i).
    by rewrite !get_le_none -/ln' -?H'. 
  have /get_correct /(_ i) := H.
  rewrite ?string_app_assoc.
  rewrite -append_correct1 //.
  by rewrite -[RHS]append_correct1 // -/ln -/ln' -?H'.
Qed.

Lemma ne_app_string_of_nat:
  forall n n' s s',
  string_of_nat n ++ "_"
  <> string_of_nat n' ++ "_"
  -> string_of_nat n ++ "_" ++ s
      <> string_of_nat n' ++ "_" ++ s'.
Proof.
  move => n n' s s' H H'. by apply eq_app_string_of_nat in H'.
Qed.
