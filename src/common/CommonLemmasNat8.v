From Coq Require Import Arith ZArith Psatz Bool
                        String List Program.Equality Program.Wf.
From VFP Require Import ClightGeneration CommonSSRLemmas.
From compcert Require Import Coqlib Integers AST Ctypes
                             Cop Clight Clightdefs
                             Events Values.
Require Import Relations.
Require Import Recdef.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.
Import ListNotations.

(** Common lemmas manipulating nat representable on 8 bits *)

(** Main property *)
Definition is_nat8 (n: nat):= n < 256.

(** 0 is nat8 *)
Remark zero8:
  is_nat8 0.
Proof. rewrite /is_nat8. lia. Qed.

(** Decr is nat 8 *)
Remark decr8:
  forall n,
    is_nat8 (n + 1) -> is_nat8 n.
Proof. rewrite /is_nat8. lia. Qed.

(** Incr is nat 8*)
Remark incr8:
  forall n, n < 255 -> is_nat8 (n + 1).
Proof. rewrite /is_nat8. lia. Qed.

(** Nat 8 to boolean condition *)
Remark is_nat8_to_bool:
  forall id,
    is_nat8 id
    -> (id <? 256) = true.
  Proof.
  rewrite /is_nat8 => id Hlt. to_nat Hlt.
  by apply Nat.ltb_lt.
  Qed.

(** Identity zero_ext_8 *)
Lemma id_zero_ext8:
  forall n,
    is_nat8 n
    -> Int.zero_ext 8 (ClightGeneration.int_of_nat n) =
        ClightGeneration.int_of_nat n.
Proof.
  rewrite /is_nat8 => n Hnat8. rewrite //= /Int.zero_ext.
  f_equal.
  rewrite Zbits.Zzero_ext_mod; try lia.
  rewrite Zmod_small.
  - rewrite Int.repr_unsigned. reflexivity.
  - have H1 : (0 <= (Z.of_nat n))%Z. { lia. }
      have H2: ((Z.of_nat n) < 256%Z)%Z. 
      { replace 256%Z with (Z.of_nat 256); try lia. }
      rewrite Int.unsigned_repr.
      * have H3: two_p 8 = 256%Z by []. lia.
      * have H4: (256%Z < Int.max_unsigned)%Z by []. lia.
Qed.

(** Loading a nat8 variable is identity *)
Lemma id_load_nat8:
  forall n,
    is_nat8 n
    -> (create_val n) = (Val.load_result Mint8unsigned (create_val n)).
Proof.
  rewrite /is_nat8 => n Hlt. rewrite //=.
  rewrite id_zero_ext8; try by [].
Qed.

(** Cast int to int *)
Lemma cast_int_to_int8:
  forall n,
    is_nat8 n
    -> (int_of_nat n) =  cast_int_int I8 Unsigned (int_of_nat n).
Proof.
  rewrite /is_nat8 => n Hlt. rewrite //= .
  rewrite id_zero_ext8; try by [].
Qed.

Lemma cast_int_to_int32:
  forall n,
    is_nat8 n
    -> (int_of_nat n) =  cast_int_int I32 Signed (int_of_nat n).
Proof.
  rewrite /is_nat8 => n Hlt. rewrite //= .
Qed.

Lemma cast_int_int8_create_val:
  forall n,
    is_nat8 n
    -> create_val n = Vint (cast_int_int I8 Unsigned (int_of_nat n)).
Proof.
  rewrite /is_nat8 => n Hlt. rewrite //= .
  rewrite id_zero_ext8; try by [].
Qed.

(** Equality between Z and Int *)
Lemma ZeqUInt:
  forall id,
    (Z.of_nat id <=  Int.max_unsigned)%Z
    -> (Z.of_nat id) = (Int.unsigned (int_of_nat id)).
Proof.
  move  => id Hmax.
  rewrite /int_of_nat Int.unsigned_repr; try by [].
  ssrlia.
Qed.

Lemma ZeqInt:
  forall id,
    (Z.of_nat id <=  Int.max_signed)%Z
    -> (Z.of_nat id) = (Int.signed (int_of_nat id)).
Proof.
  move  => id Hmax.
  rewrite /int_of_nat Int.signed_repr; try by []; split.
  have Hmin: (0 <= Z.of_nat id)%Z by lia.
  rewrite /Int.min_signed //=. all: lia.
Qed.

(** Inegality to Int.max_unsigned *)
Lemma max_unsigned_256:
  forall n,
    is_nat8 n
    -> (Z.of_nat n <= Int.max_unsigned)%Z.
Proof. 
  rewrite /is_nat8 => n Hlt.
  apply (Z.le_trans) with (m := (256)%Z); ssrlia. 
Qed.

(** Inegality to Int.max_signed *)
Lemma max_signed_256:
  forall n,
    is_nat8 n
    -> (Z.of_nat n <= Int.max_signed)%Z.
Proof. 
  rewrite /is_nat8 => n Hlt.
  apply (Z.le_trans) with (m := (256)%Z); ssrlia. 
Qed.

(** Cast a uint8 value to int *)
Lemma sem_cast8:
  forall v m,
    is_nat8 v
    -> sem_cast (create_val v) tuchar (Tint I32 Signed noattr) m
        = Some (create_val v).
Proof.
  move => v m H8. by [].
Qed.

(* Cast const int value *)
Remark  sem_cast_one:
  forall m,
    sem_cast (Vint (Int.repr 1)) tint (Tint I32 Signed noattr) m
      = Some (Vint (Int.repr 1)).
Proof.
  move => m. by [].
Qed.

(** int_of_nat is bijectif for nat8 *)
Lemma int_of_nat_eq8:
  forall n n',
    is_nat8 n
    -> is_nat8 n'
    -> n = n' <-> int_of_nat n = int_of_nat n'.
Proof.
  rewrite /is_nat8 => n n' H8 H8'; split; move => Heq.
  - by rewrite Heq.
  - apply Nat2Z.inj. do 2 rewrite ZeqUInt. rewrite Heq. by [].
    all: try rewrite Int.unsigned_repr; try split.
    all: try by apply max_unsigned_256.
    ssrlia.
Qed.

Lemma int_of_nat_ne8:
  forall n n',
    is_nat8 n
    -> is_nat8 n'
    -> n <> n' <-> int_of_nat n <> int_of_nat n'.
Proof.
  move => n n' H8 H8'; split; move => Hne Heq;
    by apply int_of_nat_eq8 in Heq.
Qed.

(* coq lt to Int lt *)
Lemma destruct_int_lt:
  forall n n',
    is_nat8 n
    -> is_nat8 n'
    -> n < n'
    -> Int.lt (int_of_nat n) (int_of_nat n') = true.
Proof.
  rewrite /Int.lt /int_of_nat => n n' H8 H8' Hlt.
  rewrite -?ZeqInt; try apply max_signed_256; try by [].
  rewrite zlt_true; try by [].  by apply inj_lt.
Qed. 

Lemma destruct_int_lt_ne:
  forall n n',
    is_nat8 n
    -> is_nat8 n'
    -> n <= n'
    -> Int.lt (int_of_nat n') (int_of_nat n) = false.
Proof.
  rewrite /Int.lt /int_of_nat => n n' H8 H8' Hlt.
  rewrite -?ZeqInt; try apply max_signed_256; try by [].
  rewrite zlt_false; try by []. apply Z.le_ge. to_nat Hlt. ssrlia.
Qed. 


(* Incrementation of a int value *)
Lemma incr_int_val:
  forall val,
    is_nat8 (val + 1)
    -> Int.add (int_of_nat val) (Int.repr 1)
        = int_of_nat (val + 1).
Proof.
  rewrite /is_nat8 => val H8.
  rewrite //= /Int.add //=.
  replace ( (Int.unsigned (int_of_nat val) + Int.unsigned (Int.repr 1))%Z)
    with (Z.of_nat (val + 1)). by [].
  replace (Int.unsigned (Int.repr 1)) with (1)%Z.
  rewrite -?ZeqUInt. lia.
  - apply max_unsigned_256. rewrite /is_nat8. to_nat H8. ssrlia.
  - by [].
Qed. 

(* Decrementation of a int value *)
Lemma decr_int_val:
  forall val,
    (val > 0)%nat
    -> is_nat8 val
    -> Int.sub (int_of_nat val) (Int.repr 1)
        = int_of_nat (val - 1).
Proof.
  rewrite /is_nat8 => val Hlt H8.
  rewrite //= /Int.sub //=.
  replace ( (Int.unsigned (int_of_nat val) - Int.unsigned (Int.repr 1))%Z)
    with (Z.of_nat (val - 1)). by [].
  replace (Int.unsigned (Int.repr 1)) with (1)%Z.
  rewrite -?ZeqUInt. lia.
  - apply max_unsigned_256. rewrite /is_nat8. to_nat H8. ssrlia.
Qed. 
