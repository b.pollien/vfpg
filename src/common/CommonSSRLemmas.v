From Coq Require Import Arith ZArith Psatz Bool Ascii
                        String List Program.Equality Program.Wf
                        BinNums BinaryString FunInd Program
                        Classes.RelationClasses Init.

From VFP Require Import BasicTypes CommonLemmas.

Require Import Relations.
Require Import Recdef.

Set Warnings "-parsing".
From mathcomp Require Import ssreflect ssrbool seq ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Set Warnings "parsing".

Local Open Scope string_scope.
Local Open Scope list_scope.
Local Open Scope nat_scope.
Local Open Scope bool_scope.
Import ListNotations.

(** Common lemmas manipulating seq and ssrnat *)

(** Tactics to converts ssrnat properties into Coq in order to apply lia. *)
Ltac ssrlia :=
    rewrite //= -?Nat.sub_1_r -?plusE -?multE -?minusE -?(rwP ltP) -?(rwP leP);
    try lia.

Ltac to_nat H:=
  rewrite //= -?Nat.sub_1_r -?plusE -?multE -?minusE -?(rwP ltP) -?(rwP leP) in H.

Lemma lt_impl_lt_0:
  forall n m, m < n -> 0 < n.
Proof. 
  induction m; intros; try by [].
  apply IHm. to_nat H. ssrlia.
Qed.

Remark ge_false_impl_le:
  forall n m,
    (n <= m) = false
    -> (m < n)%coq_nat.
Proof.
  move => n m Hle. by rewrite (rwP ltP) ltnNge Hle.
Qed.

Remark ge_true_impl_ge:
  forall n m,
    (n <= m) = true
    -> (n <= m)%coq_nat.
Proof.
  move => n m Hge. by rewrite (rwP leP) /is_true.
Qed.

Lemma plus0n:
  forall n, 0 + n = n.
Proof. ssrlia. Qed.

Section List.
  Variable A: Type.

  Lemma size_eq_length:
    forall (l: list A),
      size l = Datatypes.length l.
  Proof.
    induction l as [|x l]; by [].
  Qed.

  Lemma drop_impl_nth:
    forall (e:A) n l l' d,
      e :: l = drop n l'
      -> e = List.nth n l' d.
    Proof.
      induction n as [|n' IHn]; intros.
      - rewrite drop0 in H. by rewrite -H.
      - destruct l'; try by [].
        rewrite //= in H. rewrite //=. by apply IHn with (l := l).
  Qed. 

  Lemma drop_incr:
    forall l n (e:A) l',
      e :: l' = drop n l
      -> l' = drop (n + 1) l.
  Proof.
    induction l; intros.
    - rewrite //= in H.
    - rewrite -plusE Nat.add_1_r //=. destruct n.
    * rewrite drop0. rewrite drop0 in H. by injection H.
    * rewrite //= in H. rewrite -Nat.add_1_r.
        apply (IHl n e l' H).
  Qed.

  Lemma nth_find:
    forall l l' n d (e: A),
      d <> e
      -> List.nth n l d = e
      -> List.nth n (l ++ l') d = e.
  Proof.
    induction l as [| e' l IHl]; move => l' n d e Hn Hl.
    - rewrite //= in Hl. destruct n; by rewrite Hl in Hn.
    - destruct n; rewrite //=; rewrite //= in Hl.
      by apply IHl.
  Qed.

  Lemma nth_drop_impl_size:
    forall l n d (e: A),
        d <> e
        -> List.nth n l d = e
        -> drop (1 + n) l = []
        -> size l = 1 + n.
  Proof.
    induction l as [|e' l IHl]; move => n d e Hde Hn Hd.
    - destruct n; rewrite //= in Hn.
    - destruct n; rewrite //= in Hd; rewrite //= in Hn.
        * destruct l; by [].
        * apply (IHl n d e Hde Hn) in Hd. rewrite //= Hd. ssrlia.
  Qed.

  Lemma drop_oversize_app:
    forall l l' n d (e: A),
        d <> e
        -> List.nth n l d = e
        -> drop (1 + n) l = []
        -> drop (1 + n) (l ++ l') = l'.
  Proof.
    move => l l' n d e Hde Hn Hd.
    apply (drop_size_cat l' (nth_drop_impl_size Hde Hn Hd)).
  Qed.

  Lemma nth_impl_le_size:
    forall n l e (d:A),
      d <> e
      -> List.nth n l d = e
      -> n < size l.
  Proof.
    induction n as [|n IHn]; move => l e d Hne Hnth;
      destruct l as [|e' l]; try by [].
    rewrite //=. apply IHn with (e := e) (d := d); try by [].
  Qed.

  Lemma nth_impl_0_le_size:
    forall n l e (d:A),
      d <> e
      -> List.nth n l d = e
      -> 0 < size l.
  Proof.
    induction n as [|n IHn]; move => l e d Hne Hnth;
    destruct l as [|e' l]; try by [].
  Qed.

  Lemma drop_cat_le:
    forall n (s1 s2: seq A),
      n <= size s1
      -> drop n (s1 ++ s2) = (drop n s1) ++ s2.
  Proof.
    induction n as [|n IHn]; move => s1 s2 Hsize.
    - by repeat rewrite drop0.
    - destruct s1 as [| e s1].
      * inversion Hsize.
      * apply IHn; rewrite //= in Hsize.
  Qed.

  Lemma list_eq_take_nth_drop:
    forall n l e (d: A),
      d <> e
      -> List.nth n l d = e
      -> l = (take n l)
          ++ [:: e]
          ++ (drop (1 + n) l).
  Proof.
    induction n as [|n IHn]; move => l e d Hne Hnth;
      destruct l as [| e' l]; try by [].
    - by rewrite -Hnth take0 //= drop0.
    - rewrite //= [in LHS](IHn l e d) //=.
  Qed.

  Lemma drop_cons_nth:
    forall d n l l' (e: A),
      e <> d
      -> List.nth n l d = e
      -> drop (n.+1) l = l'
      -> drop n l = e :: l'.
  Proof.
    induction n as [|n IHn]; move => l l' e Hne Hnth Hdrop.
    - rewrite drop0; destruct l as[|e' l].
      * rewrite -Hnth //= in Hne.
      * by rewrite -Hdrop -Hnth //= drop0.
    - destruct l as[|e' l].
      * rewrite -Hnth //= in Hne.
      * rewrite //=. apply IHn; try by [].
  Qed.

  Lemma cons_drop_nth:
    forall n l l' (e: A),
    drop n l = e :: l'
    -> drop (n.+1) l = l'.
  Proof.
    induction n as [|n IHn]; destruct l as [|e' l]; move => l' e Hdrop.
    * by rewrite drop0 in Hdrop.
    * rewrite drop0 in Hdrop. injection Hdrop as He Hl.
      by rewrite He Hl //= drop0.
    * inversion Hdrop.
    * rewrite //=. apply IHn with (e := e); try by [].
  Qed.


  Lemma nth_app_size:
    forall (d: A) i l l' (e e': A),
      e <> d
      -> e <> e'
      -> l = l' ++ [:: e']
      -> List.nth i l d = e
      -> i + 1 <= size l'.
  Proof.
    induction i as [|i IHi]; destruct l as [|e'' l];
      move => l' e e' Hne Hne' Hl Hnth.
    - destruct l'; discriminate Hl.
    - destruct l' as [| el' l']; injection Hl as He Hl.
      * by rewrite -Hnth -He in Hne'.
      * ssrlia.
    - destruct l'; discriminate Hl.
    - destruct l' as [| el' l'].
      * injection Hl as He Hl. rewrite Hl //= in Hnth.
        destruct i; by rewrite -Hnth in Hne. 
      * rewrite //= addn1 //= ltnS -addn1.
        apply IHi with (l := l) (e := e) (e' := e'); try by [].
        rewrite //= in Hl. by injection Hl as He Hl.
  Qed.

  Lemma drop_drop_app:
    forall d j i l l' (e e':A),
      e <> d
      -> e <> e'
      -> j > i
      -> List.nth j l d = e
      -> drop (i.+1) l = l' ++ [:: e']
      -> drop (j + 1) l = (drop (1 + (j - (i + 1))) l') ++ [:: e'].
  Proof.
    induction j as [|j IHj]; move => i l l' e e' Hne Hne' Hlt Hnth Hdrop.
    - ssrlia.
    - destruct l as [| e'' l].
      * rewrite -Hnth //= in Hne.
      * rewrite //=. rewrite //= in Hdrop. rewrite //= in Hnth.
        destruct i as [| i]; destruct l' as [|el' l'].
        + rewrite drop0 //= in Hdrop. rewrite Hdrop //= in Hnth.
          destruct j. by rewrite -Hnth in Hne'.
          destruct j; rewrite -Hnth //= in Hne.
        + rewrite drop0 in Hdrop. rewrite Hdrop.
          have Hj: 1 + (j.+1 - (0 + 1)) = j + 1. { ssrlia. }
          rewrite Hj. apply drop_cat_le.
          apply nth_app_size with (l := l) (d := d) (e := e) (e' := e');
            try by [].
        + apply IHj with (e := e); try by [].
        + apply IHj with (e := e); try by [].
  Qed.

  Lemma take_nil_eq_0:
    forall (l: seq A) n,
      l <> [:: ]
      -> [:: ] = take n l
      -> n = 0.
  Proof. 
    induction l as [| e l IHl]; move => n Hne Ht.
    - contradiction Hne.
    - destruct n as [|n]; try by [].
  Qed.

  Lemma cat_app:
    forall (l l': seq A),
      cat l l' = app l l'.
  Proof. induction l as [| e l IHl]; intros; try by []. Qed.

  Lemma drop_extract:
    forall j i b (l: seq A) l',
      j > i
      -> l' <> [:: ]
      -> b = take (j - (i + 1)) (drop i.+1 l)
      -> drop j l = l'
      -> drop i.+1 l = b ++ l'.
  Proof.
    induction j as [| j IHj]; move => i b l l' Hlt Hne Hex Hd.
    - inversion Hlt.
    - destruct l.
      * rewrite -Hd //= in Hne.
      * rewrite //= in Hd. rewrite //=. destruct i.
        + rewrite //= drop0 in Hex.
          have Hj: j.+1 - (0 + 1) = j.
          { rewrite subSS //=. destruct j; try by []. }
          rewrite drop0 Hex Hj -Hd. symmetry; apply (cat_take_drop j l).
        + apply IHj; try by []. 
  Qed.

  Lemma list_cons_app:
    forall (e:A) l l' l'',
      e :: (l ++ l') ++ l'' = e :: l ++ l' ++ l''.
  Proof. move => e l l' l''; by rewrite app_assoc. Qed.

  Lemma take_app_impl_size:
    forall n (l l': seq A),
      l = take n (l ++ l')
      -> l' <> [::]
      -> size l = n.
  Proof.
    induction n as [|n IHn]; destruct l as [|e l]; 
      move => l' Ht Hne; try by [].
    - destruct l'; try by [].
    - apply eq_S. apply IHn with (l' := l'); try by [].
      by injection Ht.
  Qed.

  Lemma takeinus_one:
    forall n (e: A) l,
      (n > 0)%coq_nat
      -> seq.take n (e :: l) = e :: (seq.take (n - 1)%coq_nat l). 
  Proof.
    destruct n as [| n]; try lia.

    intros; by rewrite //= Nat.sub_0_r. 
  Qed.
 
  Lemma nth_of_app:
    forall n l l' (d:A),
      n >= size l
      -> List.nth n (l ++ l') d = List.nth (n - size l) l' d.
  Proof.
    induction n as [| n IHn]; destruct l as [|e l];
      move => l' d Hsize; try by [].
    rewrite //= subSS. by apply IHn.
  Qed.

  Lemma succ_not_lt:
    forall n, n.+1 < n = false.
  Proof. induction n; try by []. Qed.

  Lemma minusnn:
    forall n, n - n = 0.
  Proof. ssrlia. Qed.

  Lemma len_drop:
    forall (l: list A) n d,
      n < length l
      -> drop n l = [:: (List.nth n l d) ]++ (drop (n.+1) l).
  Proof.
    induction l as [| x l IHl]; try by [].
    move => n d Hlen; destruct n as [| n'].
    - by rewrite //= drop0.
    - rewrite //=. apply IHl. ssrlia.
  Qed.

  Lemma list_nth_take:
    forall l n (d: A) i,
      i < n
      -> List.nth i (take n l) d = List.nth i l d.
  Proof.
    induction l as [|x l' IHl]; rewrite //= => n d i Hlt.

    destruct n.
    - discriminate Hlt.
    - rewrite //=. destruct i; rewrite //=.
      apply IHl. ssrlia.
  Qed.

  Lemma drop_oversize':
    forall (l: list A) n,
      drop n l = nil
      -> Datatypes.length l <= n.
  Proof.
    induction l as [| e l' IHl]; try by [].
    destruct n; try by [].
    rewrite //=. apply IHl.
  Qed.

  Lemma drop_take_not_empty:
    forall l' l n' n (e:A),
      e :: l = drop n (take n' l')
      -> n < n'.
  Proof.
    induction l' as [|e' l' IHl']; try by [].
    destruct n'.
    - destruct n; try by [].
    - destruct n; try by [].
      rewrite //= => e. apply IHl'.
  Qed.

  Lemma reduce_nth:
    forall n (e:A) l d,
      n <> 0
      -> List.nth n (e::l) d = List.nth n.-1 l d.
  Proof.
    induction n as [|n IHn]; try by [].
  Qed.

  Lemma reduce_drop:
    forall n (e:A) l,
      n <> 0
      -> drop n (e :: l) = drop n.-1 l.
  Proof.
    induction n as [|n IHn]; try by [].
  Qed.

  Lemma app_split_eq:
    forall l l' (d d': A),
    l = l' /\ d = d'
    <-> l ++ [:: d] = l' ++ [:: d'].
  Proof.
    move => l l' d d'; split.
    - move => [Hl Hd]; by rewrite Hl Hd.
    - generalize dependent l'. induction l as [|x l IHl]; move => l' Hl.
      * destruct l'.
        + by inversion Hl.
        + inversion Hl as [[H H']]. by apply app_cons_not_nil in H'. 
      * destruct l' as [|x' l'].
        + inversion Hl as [[H H']]. symmetry in H'.
          by apply app_cons_not_nil in H'.
        + inversion Hl as [[H H']]. apply IHl in H'.
          by destruct H'; subst x' l' d'.
  Qed.

  Lemma length_ge_0:
    forall l n (e: A) l',
      drop n l = e :: l'
      ->  Datatypes.length l > 0.
  Proof.
    induction l as [| e' l IHl]; move => n e l' Hd.
    - destruct n; inversion Hd.
    - by [].
  Qed.

  Lemma drop_single_element:
    forall l n (e: A),
    drop n l = [:: e]
    -> n = (length l).-1. 
  Proof.
    induction l as [| e l IHl]; move => n e' Hd.
    - inversion Hd.
    - destruct n; rewrite //=.
      * by inversion Hd.
      * rewrite //= in Hd. rewrite (IHl _ _ Hd). 
        have H: Datatypes.length l > 0 by apply (length_ge_0 Hd).
        to_nat H. ssrlia.
  Qed.
End List.
