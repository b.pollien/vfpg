MKFILE_PATH = $(abspath $(lastword $(MAKEFILE_LIST)))
MKFILE_DIR = $(patsubst %/,%,$(dir $(MKFILE_PATH)))

ARCH=x86

# CompCert flags
COMPCERTDIR=CompCert
COMPCERTFLAGS=$(SILENT) -C $(MKFILE_DIR)/CompCert
COMPCERT_INCLUDES=lib cfrontend backend common driver cparser debug $(ARCH)
COMPCERT_LIB=$(COMPCERTDIR)/lib/
#,$(COMPCERTDIR)/cfrontend/,$(COMPCERTDIR)/cparser,$(COMPCERTDIR)/x86,$(COMPCERTDIR)/backend,$(COMPCERTDIR)/common,$(COMPCERTDIR)/driver

# Paparazzi
PPRZLIB=paparazzi/sw/lib/ocaml
export PAPARAZZI_HOME=$(MKFILE_DIR)/paparazzi
export PAPARAZZI_SRC=$(PAPARAZZI_HOME)

# Frondend
FRONTEND=frontend

# Generated folders
EXTRACTED=extracted
GEN=generated

# ocamlbuild flags
VERBOSITY=-verbose 1
FLAGS= -r -Is $(FRONTEND),$(EXTRACTED),$(PPRZLIB),$(COMPCERT_LIB) -X ocaml-generator -libs str,unix -pkg xml-light -no-hygiene -use-ocamlfind $(VERBOSITY)
TARGET=native
BUILDDIR=_build

# Main file
VFPG=vfpg
