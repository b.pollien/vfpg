From GEN Require Import CommonFP.
From compcert Require Import Coqlib Integers Floats AST Ctypes Cop Clight Clightdefs.
Import Clightdefs.ClightNotations.
Local Open Scope Z_scope.
Local Open Scope string_scope.
Local Open Scope clight_scope.


Definition global_definitions : list (ident * globdef fundef type) :=
( (_nav_init_stage,
    Gfun(External (EF_external "nav_init_stage"
                    (mksignature nil AST.Tvoid cc_default)) Tnil tvoid
      cc_default)) ::
  (_on_enter_block,
    Gfun(External (EF_external "on_enter_block"
                    (mksignature (AST.Tint :: nil) AST.Tvoid cc_default))
      (Tcons tuchar Tnil) tvoid cc_default)) ::
  (_on_exit_block,
    Gfun(External (EF_external "on_exit_block"
                    (mksignature (AST.Tint :: nil) AST.Tvoid cc_default))
      (Tcons tuchar Tnil) tvoid cc_default)) ::
  (_forbidden_deroute,
    Gfun(External (EF_external "forbidden_deroute"
                    (mksignature (AST.Tint :: AST.Tint :: nil)
                      AST.Tint8unsigned cc_default))
      (Tcons tuchar (Tcons tuchar Tnil)) tbool cc_default)) ::
  (_nb_blocks, Gvar v_nb_blocks) :: (_stage_time, Gvar v_stage_time) ::
  (_block_time, Gvar v_block_time) :: (_nav_stage, Gvar v_nav_stage) ::
  (_nav_block, Gvar v_nav_block) :: (_last_block, Gvar v_last_block) ::
  (_last_stage, Gvar v_last_stage) ::
  (_private_nav_stage, Gvar v_private_nav_stage) ::
  (_private_nav_block, Gvar v_private_nav_block) ::
  (_private_last_block, Gvar v_private_last_block) ::
  (_private_last_stage, Gvar v_private_last_stage) ::
  (_init_function, Gfun(Internal f_init_function)) ::
  (_get_nav_block, Gfun(Internal f_get_nav_block)) ::
  (_get_nav_stage, Gfun(Internal f_get_nav_stage)) ::
  (_get_last_block, Gfun(Internal f_get_last_block)) ::
  (_get_last_stage, Gfun(Internal f_get_last_stage)) ::
  (_set_nav_block, Gfun(Internal f_set_nav_block)) ::
  (_set_nav_stage, Gfun(Internal f_set_nav_stage)) ::
  (_NextBlock, Gfun(Internal f_NextBlock)) ::
  (_NextStage, Gfun(Internal f_NextStage)) ::
  (_Return, Gfun(Internal f_Return)) ::
  (_nav_init_block, Gfun(Internal f_nav_init_block)) ::
  (_nav_goto_block, Gfun(Internal f_nav_goto_block)) :: nil).

Definition public_idents : list ident :=
    nil.
    
Definition commonC : Clight.program := 
      mkprogram composites global_definitions public_idents _main Logic.I.
