/*
 * Copyright (C) 2007-2009  ENAC, Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * @file subsystems/navigation/common_flight_plan.c
 * Common flight_plan functions shared between fixedwing and rotorcraft.
 */

#include "common_flight_plan.h"


/** In s */
uint16_t stage_time, block_time;

uint8_t nav_stage, nav_block;

/** To save the current block/stage to enable return */
uint8_t last_block, last_stage;

static uint8_t private_nav_stage, private_nav_block;
static uint8_t private_last_block, private_last_stage;

void init_function(void) {
  private_nav_stage = 0;
  nav_stage = 0;
  private_nav_block = 0;
  nav_block = 0;
  private_last_stage = 0;
  last_stage = 0;
  private_last_block = 0;
  last_block = 0;
}

uint8_t get_nav_block() {
  return private_nav_block;
}
uint8_t get_nav_stage() {
  return private_nav_stage;
}
uint8_t get_last_block() {
  return private_last_block;
}
uint8_t get_last_stage() {
  return private_last_stage;
}

void set_nav_block(uint8_t b){
  if (b >= nb_blocks) {
    private_nav_block = nb_blocks - 1;
  }
  else {
    private_nav_block = b;
  }
  nav_block = private_nav_block;
}

void set_nav_stage(uint8_t s) {
  private_nav_stage = s;
  nav_stage = s;
}

void NextBlock() {
  if (private_nav_block < nb_blocks - 1) {
    nav_goto_block(private_nav_block + 1);
  } else {
    nav_goto_block(private_nav_block);
  }
}

void NextStage() { 
  private_nav_stage++;
  nav_stage = private_nav_stage;
  InitStage();
}

void Return(uint8_t x) {
  on_exit_block(private_nav_block);
  private_nav_block = private_last_block;
  nav_block = private_nav_block;
  if (x == 1) {
    private_nav_stage = 0;
  } else {
    private_nav_stage = private_last_stage;
  }
  nav_stage = private_nav_stage;
  block_time = 0;
  InitStage();
  on_enter_block(private_nav_block); 
}

void nav_init_block(void)
{
  if (private_nav_block >= nb_blocks) {
    private_nav_block = nb_blocks - 1;
    nav_block = private_nav_block;
  }
  private_nav_stage = 0;
  nav_stage = private_nav_stage;
  block_time = 0;
  InitStage();
  on_enter_block(private_nav_block); 
}

void nav_goto_block(uint8_t b)
{
  if (forbidden_deroute(private_nav_block, b)) { // If the deroute is forbidden
    // The goto is not taken
    return; // FIXME: Go to an error block?
  }
  if (b != private_nav_block) {
    /* To avoid a loop in a the current block */
    private_last_block = private_nav_block;
    last_block = private_last_block;
    private_last_stage = private_nav_stage;
    last_stage = private_last_stage;
  }
  on_exit_block(private_nav_block);
  private_nav_block = b;
  nav_block = private_nav_block;
  nav_init_block();
}
