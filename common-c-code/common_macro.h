
#ifndef COMMON_FLIGHT_PLAN_H
#define COMMON_FLIGHT_PLAN_H

#include <stdint.h>

#define InitStage() nav_init_stage();

#define Block(x) case x: set_nav_block(x); // Modified

#define GotoBlock(b) nav_goto_block(b)

#define Stage(s) case s: set_nav_stage(s); // Modified

#define NextStageAndBreak() { NextStage(); break; }
#define NextStageAndBreakFrom(wp) { last_wp = wp; NextStageAndBreak(); }

#define Label(x) label_ ## x:
#define Goto(x) { goto label_ ## x; }

#define And(x, y) ((x) && (y))
#define Or(x, y) ((x) || (y))
#define Min(x,y) (x < y ? x : y)
#define Max(x,y) (x > y ? x : y)
#define LessThan(_x, _y) ((_x) < (_y))
#define MoreThan(_x, _y) ((_x) > (_y))

/** Time in s since the entrance in the current block */
#define NavBlockTime() (block_time)

/** In s */
extern uint16_t stage_time, block_time;

/* The modification of the following variables do not affect
 * the behavior of the flight plan 
 */
extern uint8_t nav_stage, nav_block;
extern uint8_t last_block, last_stage;