(** Generation of the common flight plan file from the *)
(** Clight file into C code                            *)

open PrintClight
open FlightPlan
open Camlcoq
open Format

let read_file_to_string (path: string): string =
  let rec read_stream stream =
    try
      let line = input_line stream in
      line :: (read_stream stream)
    with End_of_file ->
      []
  in
  let stream = open_in path in
  String.concat "\n" (read_stream stream)

let () =
  let common_macro_file, out_c_file, out_h_file =
     try Sys.argv.(1), Sys.argv.(2), Sys.argv.(3)
     with _ ->
      failwith "Error: please provide a path to the common macro file, the outpute c file and h file"
     in
  (** Print Clight and call Cgen *)

  let out_c = open_out out_c_file in
  let out_h = open_out out_h_file in
  let common_macro = read_file_to_string common_macro_file in
  print_program Clight1 (formatter_of_out_channel out_c) CommonCCode.commonC;

  Printf.fprintf out_h "%s\n" common_macro;

  (** Closing the output file *)
  close_out out_c;

  (** Post processing of the generated file *)
  Postproc_common_fp.postproc out_c_file out_h;

  Printf.fprintf out_h "\n#ifndef GEN \n\
    #include \"flight_plan.h\" \n\
    #endif \n\
    \n\
    #endif /* COMMON_FLIGHT_PLAN_H */";

  close_out out_h