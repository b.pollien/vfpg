(** Configuration file needed to replace the configuration from CompCert *)
(** Remove the need of a compcert.ini file                               *)
let arch="x86"
let stdlib_path="/not_exist"
let abi="linux"
let system="linux"
let model="64"
let elf_target = true
let has_standard_headers=true
