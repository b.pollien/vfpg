open Integers
open Printf
open BasicTypes
open FPNavigationMode

(*****************************************************)
(* Definition of common functions:                   *)
(* - Conversion function between coq and ocaml types *)
(* - Parsing functions                               *)
(* - Access to elements of an xml structure.         *)
(*************************************************** *)

(** Renaming common conversion functions *)
let sof = string_of_float
let soi = string_of_int

(** Conversion function between Coq and Caml *)
let cs = Camlcoq.coqstring_of_camlstring
let sc = Camlcoq.camlstring_of_coqstring

(** Functions for the waypoints *)
let index_of_blocks = ref []

let get_index_waypoint = fun x l ->
  try
    List.assoc x l
  with
    Not_found -> Printf.printf "11 %s\n" (sc x); failwith (sprintf "Unknown waypoint: %s" (sc x))


(** Common parsing functions *)
let check_expressions = ref false

let parse = fun s ->
  let e = Fp_proc.parse_expression s in
  if !check_expressions then begin
    let unexpected = fun kind x ->
      fprintf stderr
        "Parsing error in '%s': unexpected %s: '%s' \n" s kind x;
      exit 1 in
    begin
      try
        Expr_syntax.check_expression e
      with
        Expr_syntax.Unknown_operator x -> unexpected "operator" x
      | Expr_syntax.Unknown_ident x -> unexpected "ident" x
      | Expr_syntax.Unknown_function x -> unexpected "function" x
    end
  end;
  Expr_syntax.sprint ~call_assoc:("IndexOfBlock", !index_of_blocks) e

let parsed_attrib = fun elem f a->
  match f elem with
  | None -> failwith ("Error : attribut is missing "^a)
  | Some v -> 
    begin
      try
        (parse (sc v))
      with
        _ -> failwith ("Error: Impossible to parse the attribute "^a)
    end


(** Conversion function between Coq and Caml *)

let ni = Camlcoq.Nat.of_int

let nat_of_string v = ni (int_of_string v)


let float_attrib = fun f elem name ->
  match (f elem) with
  | None ->
      failwith ("can't get attribut " ^ name)
  | Some l -> try
                float_of_string (sc l)
               with
                  Failure err ->
                    failwith (sprintf "can't convert %s to float value (%s)"
                    name err)
                
(** Parse and return a Coq string *)
let parse_coq_string = fun s ->
  try
    cs (parse s)
  with
    _ -> failwith ("Error: Impossible to parse the attribute : "^s)


(** Parse an attribute and return a Coq string *)
let parse_coq_attr = fun elem f a ->
  cs (parsed_attrib elem f a)

let parse_coq_attr_option = fun elem f a ->
  match f elem with
  | Some v -> Some (cs (parse (sc v)))
  | None   -> None

(** Function to add parenthesis around a string *)
let add_parenthesis = fun s ->
  cs ("("^(sc s)^")")


(** Access to the attribute of the XML structure *)
let get_attrib = fun x f n ->
  match f x with
  | Some v -> v
  | None   ->
     failwith ("Impossible to find the attribute "^n)

let get_attrib_option_par = fun x f ->
  match f x with
  | Some res -> Some (add_parenthesis res)
  | None     -> None

let get_attrib_bool = fun x f a d->
  match f x with
  | None -> d
  | Some v ->
    begin match sc v with
      | "TRUE" -> true
      | "FALSE" -> false
      | err -> failwith ("ERROR: FP parameter "^err^" must be TRUE or FALSE")
    end

let get_attrib_bool_1 = fun x a ->
  try
    if (Xml.attrib x a) = "1"
    then true
    else false
  with Xml.No_attribute _ | Xml.Not_element _ -> false

let get_attrib_or_default = fun f x def ->
  try get_attrib x f ""
  with _ -> def

let get_index_waypoint_nat = fun wp index_of_waypoints ->
  ni (get_index_waypoint wp index_of_waypoints)

let get_attrib_wp = fun s f n index_of_waypoints ->
  let wp_name = match f s with
    | Some v -> v
    | _ -> failwith ("Impossible to find the attribute "^n)
  in 
  get_index_waypoint_nat wp_name index_of_waypoints
