
open Printf
open Latlong
open Common_def
open FlightPlanParsed
open FPPUtils
module G2D = Geometry_2d

(** process relative waypoint *)
(* TODO try inverting order of calculation so taht absolut waypoint can be use for qdr *)
let g2D_of_waypoint = fun wp ->
  let name = get_attrib wp get_waypoint_name "waypoint name" in
  let x = match get_waypoint_x wp with
          | Some x -> x
          | None ->
            failwith "only relativ waypoint can be use for with qdr"
  and y = match get_waypoint_y wp with
          | Some y -> y
          | None -> 
            failwith "only relativ waypoint can be use for with qdr" in
  try
    { G2D.x2D = float_of_string (sc x); y2D = float_of_string (sc y) }
  with 
    _ -> failwith (sprintf "invalid coord for waypoint %s" (sc name))

let new_waypoint = fun wp qdr dist waypoints ->
  let wp_old = List.find 
                  (fun x -> 
                    try
                      let name = sc (get_attrib x
                                                get_waypoint_name
                                                "name of waypoint") in
                      String.equal name wp
                    with _ -> false)
                  !waypoints in
  let wp2D = g2D_of_waypoint wp_old in
  let a = (Deg>>Rad)(90. -. qdr) in
  let xy = G2D.vect_add wp2D (G2D.polar2cart { G2D.r2D = dist; theta2D = a }) in
  let x = sof xy.G2D.x2D
  and y = sof xy.G2D.y2D in
  let name = sprintf "%s_%.0f_%.0f" wp qdr dist in
  let nv_waypoint = mk_waypoint (Some (cs name))
                                (Some (cs x)) (Some (cs y))
                                None None
                                (get_waypoint_alt wp_old)
                                (get_waypoint_height wp_old) in
  waypoints := nv_waypoint :: !waypoints;
  name

exception No_QDR
let replace_wp = fun param waypoints ->
  let wp = get_attrib param get_stage_wp "waypoint" in
  try
    let qdr = match get_stage_wp_qdr param with
              | None -> raise No_QDR
              | Some qdr -> 
                  try
                    float_of_string (sc qdr)
                  with
                    _ -> failwith "float expected fo qdr value"
    and dist = match get_stage_wp_dist param with
              | None -> raise No_QDR 
              | Some dist -> 
                  try
                    float_of_string (sc dist)
                  with
                    _ -> failwith "float expected fo qdr value" in

    let name = new_waypoint (sc wp) qdr dist waypoints in

    let param = set_stage_wp (Some (cs name)) param in
    let param = set_stage_wp_qdr None param in
    let param = set_stage_wp_dist None param in
    param
  with
    Not_found -> 
      failwith (sprintf "Invalid waypoint while parsing stage (%s)"
                (sc wp))
    | No_QDR -> param

let replace_from = fun param waypoints ->
  try
    let wp = match get_stage_from param with
             | Some wp -> wp
             | None -> raise No_QDR 
    and qdr = match get_stage_from_qdr param with
              | None -> raise No_QDR 
              | Some qdr -> 
                  try
                    float_of_string (sc qdr)
                  with
                    _ -> failwith "float expected fo qdr value"
    and dist = match get_stage_from_dist param with
              | None -> raise No_QDR  
              | Some dist -> 
                  try
                    float_of_string (sc dist)
                  with
                    _ -> failwith "float expected fo qdr value" in

    let name = new_waypoint (sc wp) qdr dist waypoints in

    let param = set_stage_from (Some (cs name)) param in
    let param = set_stage_from_qdr None param in
    let param = set_stage_from_dist None param in
    param
  with
    Not_found -> 
      failwith "Invalid waypoint while parsing stage"
    | No_QDR -> param


(* I'm not sure why only these stage are match 
   According to my .dtd only go and circle can have relative
   waypoint. why them? *)
let process_stage = fun stage waypoints ->
  let rec aux = fun stage ->
    match stage with
      | Go_stage param -> 
          let param = replace_from (replace_wp param waypoints)
                                   waypoints in
        Go_stage param
      | Stay_stage param -> 
          let param = replace_from (replace_wp param waypoints)
                                   waypoints in
        Stay_stage param
      | Circle_stage param -> 
          let param = replace_from (replace_wp param waypoints)
                                   waypoints in
        Circle_stage param

      | While_stage (param, list) ->
        While_stage (param, List.map aux list)
      | For_stage (param, list) ->
        For_stage (param, List.map aux list)
      | _ -> stage in
  aux stage


let process_relative_waypoints = fun fpparsed ->
  let fpp_elem = get_fpp_elements fpparsed in
  let waypoints = get_attrib fpp_elem get_waypoints "waypoints"
  and blocks_list = get_attrib_or_default get_blocks fpp_elem [] in

  let waypoints_list = ref (get_waypoint_list waypoints) in

  let blocks_list =
    List.map
      (fun block ->
        let new_stages =
          List.map
            (fun stage -> process_stage stage waypoints_list)
            (get_block_stages block) in
        mk_block (get_block_name block)
           (get_block_pre_call block)
           (get_block_post_call block)
           (get_block_on_enter block)
           (get_block_on_exit block)
           (get_block_strip_button block)
           (get_block_strip_icon block)
           (get_block_group block)
           (get_block_key block)
           (get_block_description block)
           (get_block_exceptions block)
           (get_block_fbd block)
           new_stages
      )
      blocks_list in

  let new_waypoints = 
    mk_waypoints (get_waypoints_utm_x0 waypoints)
                 (get_waypoints_utm_y0 waypoints)
                 !waypoints_list in
  let new_element = set_fpp_blocks (Some blocks_list)
                                   fpp_elem in
  let new_element = set_fpp_waypoints (Some new_waypoints)
                                   new_element in
  mk_fpp (get_fpp_info fpparsed) new_element
