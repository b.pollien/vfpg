open FlightPlan.FP
open BasicTypes
open Common_def
open FPNavigationMode
open Printf
open Preproc
open FlightPlanParsed
open FPPUtils

(** * Module to convert a FPP stage into a FP Nav Stage *)

(** Function that gets an orientation *)
let get_orientation = fun param ->
  let orientation = match get_stage_orientation param with
                    | Some v -> sc v
                    | None   -> "NS" in
  match orientation with
  | "NS" -> NS
  | "WE" -> WE
  | orientation ->
    failwith
      (sprintf
         "Unknown survey orientation (NS or WE): %s"
         orientation)

(** Function that generates the nav type *)
let convert_nav_type = fun param ->
  match get_stage_nav_type param with
  | Some v -> v
  | None   -> (cs "Nav")

(* Function to test if hmode and vmode are compatible *)
let test_vmode_hmode_compatible = fun hmode vmode ->
  match hmode with
  | ROUTE _-> false
  | _ -> 
    begin
      match get_nav_vmode vmode with
      | GLIDE _ -> true
      | _ -> false
    end

(** Function to get and verify the throttle parameter *)
exception THROTTLE_ERROR
let pprz_throttle = fun param ->
  let throttle = parsed_attrib param get_stage_throttle "throttle" in
  begin
    try
      let g = float_of_string throttle in
      if g < 0. || g > 1. then
        raise THROTTLE_ERROR
    with
      THROTTLE_ERROR ->
      failwith "Error: throttle must be > 0 and < 1"
    | Failure _ ->
      (* Error Throttle: No possible check on expression *)
      (* It can be a constant *)
      ()
  end;
  cs throttle

(** Function that generates the nav pitch *)
let convert_nav_pitch = fun ?(manual=false) param ->
  if manual then NO_PITCH
  else
    let pitch = match get_stage_pitch param with
                | Some pitch -> sc pitch
                | None       -> "0.0" in
    match pitch with
    | "auto" -> AUTO (pprz_throttle param)
    | pitch -> SOME (parse_coq_string pitch)

(** Function that generatees the vmode *)
let convert_nav_vmode = fun param pitch index_of_wp wp last_wp ->
  let vmode = match get_stage_vmode param with
              | Some mode -> sc mode
              | None      -> "alt" in
  match vmode with
  | "climb" -> CLIMB (parse_coq_attr param get_stage_climb "climb")
  | "alt" ->
    let alt =
      try
        let a = parsed_attrib param get_stage_alt "alt" in
        begin
          try
            check_altitude (float_of_string a) 
                           "convertion vmode alt"
          with
          (* Impossible to check the altitude on an expression: *)
            Failure _ -> ()
        end;
        ABS (parse_coq_string a)
      with _ ->
      try
        let h = parsed_attrib param get_stage_height "height" in
        begin
          try
            check_altitude ((float_of_string h) +. !ground_alt)
                            "convertion vmode height"
          with
          (* Impossible to check the altitude on an expression: *)
            Failure _ -> ()
        end;
        REL (parse_coq_string h)
      with _ ->
        if (sc wp) = ""
        then failwith "alt or waypoint required in alt vmode"
        else WP (get_index_waypoint_nat wp index_of_wp) in
    ALT alt
  | "xyz" -> XYZ_VM (* Handled in Goto3D() *)
  | "glide" -> (GLIDE ((get_index_waypoint_nat wp index_of_wp),
                       last_wp))
  | "throttle" ->
    begin
      match pitch with
      | AUTO _ ->
        failwith "auto pich mode not compatible with vmode=throttle";
      | _ -> THROTTLE (pprz_throttle param)
    end
  | x -> failwith (sprintf "Unknown vmode '%s'" x)

(** Function that generates the hmode *)
let convert_hmode = fun param wp last_wp index_of_waypoints ->
  let wp_id = get_index_waypoint_nat wp index_of_waypoints in
  let hmode = match get_stage_hmode param with
              | Some v -> sc v
              | None   -> "direct" in
  match hmode with
  | "route" ->
    if sc last_wp = "last_wp" then
          fprintf stderr "NOTICE: Deprecated use of 'route' using last waypoint in hmode stage\n";
    ROUTE (wp_id, last_wp)
  | "direct" -> DIRECT wp_id
  | x -> failwith (sprintf "Unknown hmode '%s'" x)

(** Function that generates the nav params *)
let convert_nav_params = fun param ->
  get_stage_nav_params param

(** Function that generates the pre call *)
let convert_pre_call = fun param ->
  get_attrib_option_par param get_stage_pre_call

(** Function that generates the post call *)
let convert_post_call = fun param ->
  get_attrib_option_par param get_stage_post_call

(** Creates a fp params nav mode *)
let convert_fp_params_nav_mode = fun ?(manual=false) param wp wp_last index_of_waypoints ->
  let nav_pitch = convert_nav_pitch ~manual:manual param in
  let nav_vmode =
    convert_nav_vmode param nav_pitch index_of_waypoints wp wp_last in
  mk_fp_pnav_mode nav_pitch nav_vmode

(** Creates a fp params nav call *)
let convert_fp_params_nav_call = fun param  -> 
  let nav_type = convert_nav_type param in
  let nav_params = convert_nav_params param in
  let nav_pre_call = convert_pre_call param in
  let nav_post_call = convert_post_call param in
  mk_fp_pnav_call nav_type nav_params nav_pre_call nav_post_call

(** Creates a fp nav stage with all the option possible *)
let convert_fp_nav = fun mode until  -> 
  [NAV (mode, until, (nav_need_init mode))]

(** Creates a fp nav stage with only pre call, post call and nav type*)

let convert_fp_heading = fun stage index_of_wp ->
  let until = parse_coq_attr stage get_stage_until "until" in
  let params = mk_fp_pheading (parse_coq_attr stage
                                              get_stage_course
                                              "course") in
  let params_mode = convert_fp_params_nav_mode stage 
                                               (cs "") 
                                               (cs "")
                                               index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = HEADING (params, params_mode, params_call) in
  convert_fp_nav mode (Some until)

let convert_fp_follow = fun stage ->
  let params =
    mk_fp_pfollow
      (get_attrib stage get_stage_ac_id "ac_id")
      (get_attrib stage get_stage_distance "distance")
      (get_attrib stage get_stage_height "height") in
  let params_call = convert_fp_params_nav_call stage in
  let mode = FOLLOW (params, params_call) in
  convert_fp_nav mode None

let convert_fp_attitude = fun stage index_of_wp ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let params = mk_fp_pattitude (parse_coq_attr stage
                                               get_stage_roll
                                               "roll") in
  let params_mode = convert_fp_params_nav_mode stage
                                               (cs "")
                                               (cs "")
                                               index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = ATTITUDE (params, params_mode, params_call) in
  convert_fp_nav mode until

let convert_fp_manual = fun stage index_of_wp ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let params =
    mk_fp_pmanual
      (parse_coq_attr stage get_stage_pitch "pitch")
      (parse_coq_attr stage get_stage_roll "roll")
      (parse_coq_attr stage get_stage_yaw "yaw") in
  let params_mode = convert_fp_params_nav_mode ~manual:true
                                               stage
                                               (cs "")
                                               (cs "")
                                               index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = MANUAL (params, params_mode, params_call) in
  convert_fp_nav mode until

let convert_fp_go = fun stage index_of_waypoints ->
  let wp_name = match get_stage_wp stage with
                | Some v -> v
                | None   ->
                  failwith "ERROR: wp must be specified for go stage" in
  let wp =
    try
      get_index_waypoint_nat wp_name index_of_waypoints
    with
      ExtXml.Error _ -> failwith "ERROR: wp must be specified for go stage"
  in
  let at = get_stage_approaching_time stage in
  let et = get_stage_exceeding_time stage in
  let from, last_wp =
    try
      let last_wp_name = match get_stage_from stage with
                         | Some v -> v
                         | None -> failwith "option None" in
      let from = get_index_waypoint last_wp_name index_of_waypoints in
      Some (ni from), (soi from)
    with _ -> None, "last_wp" in
  let at = match at, et with
    | Some a, None -> AT (parse_coq_string (sc a))
    | None, Some e -> ET (parse_coq_string (sc e))
    | None, None -> CARROT
    | _, _ -> failwith "Error: 'approaching_time' and 'exceeding_time' attributes are not compatible"
  in
  let hmode = convert_hmode stage wp_name (cs last_wp) 
                                        index_of_waypoints in
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let params =
    mk_fp_pgo
      wp
      from
      hmode
      at in
  let params_mode =
    convert_fp_params_nav_mode stage wp_name (cs last_wp)
      index_of_waypoints in
  let params_call = convert_fp_params_nav_call stage in
  if test_vmode_hmode_compatible hmode params_mode then
        failwith "glide vmode requires route hmode";
  let mode = GO (params, params_mode, params_call) in
  convert_fp_nav mode until


let convert_fp_stay = fun stage index_of_wp ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let wp_name = get_attrib stage get_stage_wp "wp" in
  let hmode = convert_hmode stage wp_name (cs "") index_of_wp in
  let wp = get_index_waypoint_nat wp_name index_of_wp in
  let params = mk_fp_pstay wp hmode in
  let params_mode =
    convert_fp_params_nav_mode stage wp_name (cs "") index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = STAY (params, params_mode, params_call) in
  convert_fp_nav mode until

let convert_fp_xyz = fun stage index_of_wp ->
  let params =
    try parse_coq_attr stage get_stage_radius "radius"
    with _ -> cs "100.0"
  in
  (* force vmode to xyz *)
  let stage = set_stage_vmode (Some (cs "xyz")) stage in 
  let params_mode = convert_fp_params_nav_mode stage
                                               (cs "")
                                               (cs "")
                                               index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = XYZ (params, params_mode, params_call) in
  convert_fp_nav mode None

let convert_fp_home = 
  convert_fp_nav HOME None

let convert_fp_circle = fun stage index_of_wp ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let wp = get_attrib stage get_stage_wp "wp" in
  let params =
    mk_fp_pcircle
      (get_index_waypoint_nat wp index_of_wp)
      (parse_coq_attr stage get_stage_radius "radius") in
  let params_mode = convert_fp_params_nav_mode stage
      wp (cs "") index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = CIRCLE (params, params_mode, params_call) in
  convert_fp_nav mode until

let convert_fp_eight = fun stage index_of_wp ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let center = get_attrib stage get_stage_center "center" in
  let params =
    mk_fp_peight
      (get_attrib_wp stage get_stage_center "center" index_of_wp)
      (get_attrib_wp stage get_stage_turn_around "turn_around" index_of_wp)
      (parse_coq_attr stage get_stage_radius "radius") in
  let params_mode =
    convert_fp_params_nav_mode stage center (cs "") index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = EIGHT (params, params_mode, params_call) in
  convert_fp_nav mode until

let convert_fp_oval = fun stage index_of_wp ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let p1 = get_attrib stage get_stage_wp "p1" in
  let params =
    mk_fp_poval
      (get_attrib_wp stage get_stage_wp "p1" index_of_wp)
      (get_attrib_wp stage get_stage_wp2 "p2" index_of_wp)
      (parse_coq_attr stage get_stage_radius "radius") in
  let params_mode =
    convert_fp_params_nav_mode stage p1 (cs "") index_of_wp in
  let params_call = convert_fp_params_nav_call stage in
  let mode = OVAL (params, params_mode, params_call) in
  convert_fp_nav mode until

let convert_fp_survey_rectangle = fun stage index_of_wp ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let wp1 = get_attrib stage get_stage_wp "wp1" in
  let wp2 = get_attrib stage get_stage_wp2 "wp2" in
  let params =
    mk_fp_psrect
      (parse_coq_attr stage get_stage_grid "grid")
      (get_index_waypoint_nat wp1 index_of_wp)
      (get_index_waypoint_nat wp2 index_of_wp)
      (get_orientation stage) in
  let params_call = convert_fp_params_nav_call stage in
  let mode = SURVEY_RECTANGLE (params, params_call) in
  convert_fp_nav mode until

let convert_fp_guided = fun stage ->
  let until = parse_coq_attr_option stage get_stage_until "until" in
  let flags = match get_stage_flags stage with
              | Some v -> v
              | None -> cs "0" in
  let params =
    mk_fp_pguided
      (get_attrib stage get_stage_commands "commands") 
      flags in
  let params_call = convert_fp_params_nav_call stage in
  let mode = GUIDED (params, params_call) in
  convert_fp_nav mode until
