open PrintClight
open FlightPlan
open Camlcoq
open Format

let () =
  let fp_file, file_out =
     try Sys.argv.(1), Sys.argv.(2)
     with _ ->
      failwith "Error: please choose a flight plan XML and an output file"
     in

  (** Generation of the header using the a modified version of the old *)
  (** generator.                                                       *)
  let out = open_out file_out in
  (** Read the XML flight plan*)
  let fp_parse = Preproc.from_file fp_file in

  (** Pre-process the flight plan*)
  let (fp_parse, ref0, index_of_waypoints) = Preproc.preprocess_fp fp_parse in

  (** Generates the list of global variables *)
  let (fp_parse, gvars) = Preproc.gen_gvar_list fp_parse in

  (** Generation of the header *)
  Printer.flight_plan_header fp_parse ref0 fp_file index_of_waypoints out;

  (** Generate and print the functions of the flight plan *)
  Camlcoq_vfpg.gen_flight_plan fp_parse index_of_waypoints gvars out;

  (** Print the footer of the file*)
  Printer.flight_plan_footer out;

  (** Closing the output file *)
  close_out out;

  (** Post processing of the generated file *)
  Postproc.postproc file_out