
open Printf
open Latlong
open ClightGeneration
open Common_def
open Expr_syntax
open Importer
open FlightPlanParsed
open FPPUtils
open Fp_proc

module OT = Ocaml_tools

(* parse and process includes *)

let read_file_to_string (path: string): string =
  let rec read_stream stream =
    try
      let line = input_line stream in
      line :: (read_stream stream)
    with End_of_file ->
      []
  in
  let stream = open_in path in
  String.concat "\n" (read_stream stream)

let subst_expression = fun (name, env) e ->
  let rec sub = fun e ->
    match e with
        Ident i -> Ident (try List.assoc i env with Not_found -> i)
      | Int _ | Float _ | Field _ -> e
      | Call ("IndexOfBlock", [Ident s]) -> Call ("IndexOfBlock", [ Ident ("'"^name^"."^s^"'") ]) (* prefix block name *)
      | Call (i, es) -> Call (i, List.map sub es)
      | CallOperator (i, es) -> CallOperator (i, List.map sub es)
      | Index (i,e) -> Index (i,sub e)
      | Deref (e,f) -> Deref (sub e, f) in
  sub e


let transform_expression = fun env e ->
  let e' = subst_expression env e in
  Expr_syntax.sprint e'

let transform_value = fun value env ->
  transform_expression env (Fp_proc.parse_expression value)

let transform_attrib = fun ?(require=false) env a ->
  match a with
  | Some v -> Some (cs (transform_value (sc v) env))
  | None -> if require 
            then failwith "require attribut is missing"
            else None

let prefix_or_deroute prefix reroutes from =
  try List.assoc from reroutes with
              Not_found -> prefix from

let transform_exception = fun prefix reroutes env exc ->
  let deroute = get_attrib exc get_exception_deroute "deroute" in
  let deroute = prefix_or_deroute prefix
                                  reroutes
                                  deroute
  and cond = transform_attrib ~require:true 
                              env 
                              (get_exception_cond exc)
  and exec = transform_attrib env
                              (get_exception_exec exc)
  in
  mk_exception cond (Some deroute) exec

let transform_fpb = fun prefix reroutes env fbd ->
  let cond = transform_attrib env
                              (get_fbd_cond fbd)
  and deroute = prefix_or_deroute prefix reroutes (get_fbd_to fbd)
  in
  mk_fbd cond deroute
          
let transform_stage_param = fun env param ->
  let roll = transform_attrib env
                                (get_stage_roll param)
  and pitch = transform_attrib env
                                (get_stage_pitch param)
  and yaw = transform_attrib env
                                (get_stage_yaw param)
  and climb = transform_attrib env
                                (get_stage_climb param)
  and wp = get_stage_wp param
  and wp2 = get_stage_wp2 param
  and wpts = get_stage_wpts param
  and wp_qdr= transform_attrib env
                                (get_stage_wp_qdr param)
  and wp_dist= transform_attrib env
                                (get_stage_wp_dist param)
  and from = get_stage_from param
  and from_qdr= transform_attrib env
                                (get_stage_from_qdr param)
  and from_dist= transform_attrib env
                                (get_stage_from_dist param)
  and course = get_stage_course param
  and radius = transform_attrib env
                                (get_stage_radius param)
  and center = get_stage_center param
  and turn_around = get_stage_turn_around param
  and orientation = transform_attrib env
                                (get_stage_orientation param)
  and grid = transform_attrib env
                                (get_stage_grid param)
  and commands = transform_attrib env
                                (get_stage_commands param)
  and flags = get_stage_flags param
  and ac_id = get_stage_ac_id param
  and distance = transform_attrib env
                                (get_stage_distance param)
  and vmode = transform_attrib env
                                (get_stage_vmode param)
  and hmode = transform_attrib env
                                (get_stage_hmode param)
  and alt = transform_attrib env
                                (get_stage_alt param)
  and height = transform_attrib env
                                (get_stage_height param)
  and throttle = transform_attrib env
                                (get_stage_throttle param)
  and pre_call = transform_attrib env
                                (get_stage_pre_call param)
  and post_call = transform_attrib env
                                (get_stage_post_call param)
  and nav_type = transform_attrib env
                                (get_stage_nav_type param)
  and nav_params = transform_attrib env
                                (get_stage_nav_params param)
  and until = transform_attrib env
                                (get_stage_until param)
  and approaching_time = transform_attrib env
                                (get_stage_approaching_time param)
  and exceeding_time = transform_attrib env
                                (get_stage_exceeding_time param)
      in
  mk_stage_param roll pitch yaw climb wp wp2 wpts wp_qdr wp_dist from
                 from_qdr from_dist course radius center turn_around
                 orientation grid commands flags ac_id distance vmode
                 hmode alt height throttle pre_call post_call nav_type
                 nav_params until approaching_time exceeding_time

let transform_stage = fun prefix reroutes env stage ->
  let rec tr = fun stage ->
    match stage with
    | While_stage (cond, list) ->
          let cond = transform_attrib env cond
          and list = List.map tr list in
        While_stage (cond, list)
    | For_stage (param, list) ->
          let var = transform_attrib ~require:true
                                     env 
                                     (get_for_var param)
          and from = transform_attrib ~require:true
                                      env 
                                      (get_for_from param)
          and t = transform_attrib ~require:true
                                   env 
                                   (get_for_to param) in
          let param = mk_for var from t
          and list = List.map tr list in
        For_stage (param, list)
    | Return_stage (return) -> 
        Return_stage return
    | Deroute_stage (drt) -> 
        Deroute_stage (prefix_or_deroute prefix
                                                  reroutes
                                                  drt)
    | Set_stage (param) ->
        Set_stage (mk_set (get_set_var param) 
                          (cs (transform_value (sc (get_set_value
                                                param))
                                            env
                                             ))) 
    | Call_stage (call_param) ->
          let call_fun = transform_attrib env (get_call_fun call_param)
          and until = transform_attrib env (get_call_until call_param)
          and loop = transform_attrib env (get_call_loop call_param)
          and break = transform_attrib env (get_call_break call_param)
          in
        Call_stage (mk_call call_fun until loop break)
    | Call_once_stage (call_param) ->
          let call_fun = transform_attrib env (get_call_once_fun call_param)
          and break = transform_attrib env (get_call_once_break call_param)
          in
          Call_once_stage (mk_call_once call_fun break)
    | Heading_stage (param) ->
      let param = transform_stage_param env param in
      Heading_stage (param)
    | Attitude_stage (param) ->
      let param = transform_stage_param env param in
      Attitude_stage param 
    | Manual_stage (param) ->
      let param = transform_stage_param env param in
      Manual_stage param 
    | Go_stage (param) ->
      let param = transform_stage_param env param in
      Go_stage param 
    | Xyz_stage (param) ->
      let param = transform_stage_param env param in
      Xyz_stage param 
    | Circle_stage (param) ->
      let param = transform_stage_param env param in
      Circle_stage param 
    | Eight_stage (param) ->
      let param = transform_stage_param env param in
      Eight_stage param 
    | Stay_stage (param) ->
      let param = transform_stage_param env param in
      Stay_stage param 
      (* these stages weren't in the original match case of the old paparazzi *)
    | Home_stage ->
      Home_stage
    | Follow_stage (param) ->
      let param = transform_stage_param env param in
      Follow_stage (param)
    | Survey_rectangle_stage (param) ->
      let param = transform_stage_param env param in
      Survey_rectangle_stage (param)
    | Oval_stage (param) ->
      let param = transform_stage_param env param in
      Oval_stage (param)
    | Path_stage (param) ->
      let param = transform_stage_param env param in
      Path_stage (param)
    | Guided_stage (param) ->
      let param = transform_stage_param env param in
      Guided_stage (param)
    in
  tr stage

let transform_block = fun prefix reroutes env block ->
  let stages = List.map (transform_stage prefix reroutes env)
                        (get_block_stages block) in
  let excs = List.map (transform_exception prefix reroutes env)
                      (get_block_exceptions block) in
  let fbds = List.map (transform_fpb prefix reroutes env)
                      (get_block_fbd block) in 
  let name = prefix (get_attrib block get_block_name "block name") in
  mk_block (Some name)
           (get_block_pre_call block)
           (get_block_post_call block)
           (get_block_on_enter block)
           (get_block_on_exit block)
           (get_block_strip_button block)
           (get_block_strip_icon block)
           (get_block_group block)
           (get_block_key block)
           (get_block_description block)
           excs
           fbds
           stages

let parse_include dir fpparsed incl =
  let f =
    let procedure = get_include_procedure incl in
    try Ocaml_tools.find_file [dir; PPRZEnv.flight_plans_path] (sc procedure) with
        Not_found ->
          failwith (sprintf "parse_include: %s not found\n"
                    (sc procedure)) in
  let proc_name = get_include_name incl in
  let prefix = fun x -> cs ((sc proc_name) ^ "." ^ (sc x)) in

  let reroutes = List.map (fun x -> 
                              (get_fpp_with_from x, get_fpp_with_to x))
                          (get_include_fpp_withs incl)
  and args_assocs = List.map (fun x -> 
                                (sc (get_arg_name x),
                                 sc (get_arg_value x)))
                             (get_include_args incl) in

  try
    let proc = match string2fp (cs (read_file_to_string f)) with
               | Some (Proc proc) -> proc
               | Some (Err msg) -> 
                  failwith (sprintf "parsing error : unknown token : %s in procedure %s" (sc msg) (sc proc_name))
               | Some (Fpp _) -> 
                  failwith (sprintf "paring error : expected procedure, got flight_plan (procedure : %s)" (sc proc_name))
               | None -> failwith
                  (sprintf "error while reading '%s' procedure" (sc proc_name)) in
    let params = get_proc_params proc in

    (* Build the environment with arguments and default values *)
    let make_assoc = fun param ->
      let name = sc (get_proc_param_name param) in
      try
        (name, List.assoc name args_assocs)
      with
          Not_found ->
            match get_proc_param_default_value param with
            | Some v -> (name, sc v)
            | None -> failwith 
                      (sprintf "Value required for param '%s' in %s"
                      name (sc proc_name)) in
    let env =  List.map make_assoc params in

    let waypoints = get_attrib_or_default get_proc_waypoints proc
                                  (mk_waypoints None None [])
    and exceptions = get_attrib_or_default get_proc_global_exceptions proc []
    and blocks = get_attrib_or_default get_proc_blocks proc []
    and sectors = get_attrib_or_default get_proc_sectors proc []
    and variables = get_attrib_or_default get_proc_variables proc []
    and header =  get_attrib_or_default get_proc_header proc (cs "") in

    let exceptions = List.map (transform_exception prefix reroutes ((sc proc_name), env)) exceptions
    and blocks = List.map (transform_block prefix reroutes ((sc proc_name), env)) blocks in
    
    let old_element = get_fpp_elements fpparsed in

    let opt_append l1 l2 = match l1 with
                          | Some l -> Some (l @ l2)
                          | None   -> Some l2 in

    let old_waypoints = match (get_waypoints old_element) with
                        | Some wpt -> wpt
                        | None -> mk_waypoints None None [] in
    let new_waypoints = 
      mk_waypoints (get_waypoints_utm_x0 old_waypoints)
                   (get_waypoints_utm_y0 old_waypoints)
                   ((get_waypoint_list old_waypoints) @
                    (get_waypoint_list waypoints)) in

    let new_elems = 
      mk_fpp_elements (opt_append (get_header old_element) header)
                       (Some new_waypoints)
                       (opt_append (get_sectors old_element) sectors)
                       (opt_append (get_variables old_element) variables)
                       (get_modules old_element)
                       (get_includes old_element)
                       (opt_append (get_global_exceptions old_element) exceptions)
                       (opt_append (get_blocks old_element) blocks) in

    mk_fpp (get_fpp_info fpparsed) new_elems
  with
      Failure msg -> fprintf stderr "Error: %s\n" msg; exit 1

let process_includes = fun dir fpparsed -> 
  let includes = get_attrib_or_default get_includes 
                                       (get_fpp_elements fpparsed)
                                       []
  and elem_no_include = set_fpp_includes None 
                                         (get_fpp_elements fpparsed) in
  let fpparsed_no_include = mk_fpp (get_fpp_info fpparsed)
                                   elem_no_include in
  List.fold_left (parse_include dir) fpparsed_no_include includes


