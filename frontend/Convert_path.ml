
open Common_def
open FlightPlanParsed
open FPPUtils

let regexp_path = Str.regexp "[ \t,]+"


let rec stage_process_path = fun stage rest ->
  match stage with
  | Path_stage (param) ->
    let wpts = get_attrib param get_stage_wpts "wpts" in
    let waypoints = Str.split regexp_path (sc wpts) in
    let rec loop = function
      | [] -> failwith "Waypoint expected in path stage"
      | [wp] -> (* Just go to this single point *)
        let param = set_stage_wp (Some (cs wp)) param in
        (Go_stage param)::rest
      | wp1::wp2::ps ->
        let param = set_stage_wp (Some (cs wp2)) param in
        let param = set_stage_from (Some (cs wp1)) param in 
        let param = set_stage_hmode (Some (cs "route")) param in
        (Go_stage param)::
          if ps = [] then rest else loop (wp2::ps) in
    loop waypoints
  | While_stage (param, list) ->
    let list = List.fold_right stage_process_path list [] in
    (While_stage (param, list))::rest
  | For_stage (param, list) ->
    let list = List.fold_right stage_process_path list [] in
    (For_stage (param, list))::rest
  | _ ->
    stage::rest

let block_process_path = fun block ->
  let stages = get_block_stages block in
  let new_stages = List.fold_right stage_process_path stages [] in
  mk_block (get_block_name block)
           (get_block_pre_call block)
           (get_block_post_call block)
           (get_block_on_enter block)
           (get_block_on_exit block)
           (get_block_strip_button block)
           (get_block_strip_icon block)
           (get_block_group block)
           (get_block_key block)
           (get_block_description block)
           (get_block_exceptions block)
           (get_block_fbd block)
           new_stages


let process_paths = fun fpparsed ->
  let blocks = get_attrib_or_default get_blocks
                          (get_fpp_elements fpparsed) 
                          [] in
  let blocks = List.map block_process_path blocks in
  let new_element = set_fpp_blocks (Some blocks)
                                   (get_fpp_elements fpparsed) in
  mk_fpp (get_fpp_info fpparsed) new_element