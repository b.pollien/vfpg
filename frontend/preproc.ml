(*
 * Flight plan preprocessing (from XML to C)
 *
 * Copyright (C) 2004-2008 ENAC, Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 *)

open Printf
open Latlong
open ClightGeneration
open Common_def
open Expr_syntax
open Importer
open FlightPlanParsed
open FPPUtils

module G2D = Geometry_2d
module OT = Ocaml_tools

type t = {
  filename: string;
  settings: variable list;
  modules: modules;
  flight_plan_parsed: fpp;
}


(* From mapFP.ml to avoid complex linking *)
let georef_of_fpp = fun fpparsed ->
  let s_lat0 = get_attrib (get_fpp_info fpparsed) get_fpp_lat0 "lat0"
              in
  let s_lon0 = get_attrib (get_fpp_info fpparsed) get_fpp_lon0 "lon0"
              in
  try
    let lat0 = Latlong.deg_of_string (sc s_lat0)
    and lon0 = Latlong.deg_of_string (sc s_lon0) in
    { posn_lat = (Deg>>Rad)lat0; posn_long = (Deg>>Rad)lon0 }
  with
    Failure _ -> 
      failwith
        "Impossible to convert into float the lat0 and lon0 parameters"


let name_of = fun wp -> ExtXml.attrib wp "name"

type ref_type = UTM | LTP

type ref0 = {
  frame : ref_type;
  utm0 : utm;
  ecef0 : ecef;
  geo0 : geographic
}

let ground_alt = ref 0.
let security_height = ref 0.
let fp_wgs84 = ref { posn_lat = 0.; posn_long = 0.}

let check_altitude_srtm = fun a x wgs84 ->
  Srtm.add_path (PPRZEnv.paparazzi_home ^ "/data/srtm");
  try
    let srtm_alt = float (Srtm.of_wgs84 wgs84) in
    (* Not fully correct, Flightplan "alt" is not alt as we know it *)
    if a < srtm_alt then begin
      fprintf stderr
        "MAJOR NOTICE: below SRTM ground altitude (%.0f<%.0f) in %s\n" a
        srtm_alt x
    end
  with Srtm.Tile_not_found _ ->
    fprintf stderr "No SRTM data found to check altitude.\n"

let check_altitude = fun a x ->
  if a < !ground_alt +. !security_height then begin
    fprintf stderr
      "NOTICE: low altitude (%.0f<%.0f+%.0f) in %s\n"
      a !ground_alt !security_height x
  end


(** Computes "x" and "y" attributes if "lat" and "lon" are available *)
exception Not_lat_lon_waypoint
let localize_waypoint = fun rel_utm_of_wgs84 waypoint ->
  try
    let lat = match get_waypoint_lat waypoint with
              | Some lat -> sc lat
              | None -> raise Not_lat_lon_waypoint
    and lon = match get_waypoint_lon waypoint with
              | Some lon -> sc lon
              | None -> raise Not_lat_lon_waypoint in
    let (x, y) =
      rel_utm_of_wgs84
        (Latlong.make_geo_deg
           (Latlong.deg_of_string lat)
           (Latlong.deg_of_string lon)) in
    let x = sprintf "%.2f" x and y = sprintf "%.2f" y in
    let waypoint = set_waypoint_x (Some (cs x)) waypoint in
    set_waypoint_y (Some (cs y)) waypoint
  with
    Not_lat_lon_waypoint ->
    waypoint

let wgs84_of_x_y = fun ref0 x y alt ->
  match ref0.frame with
  | UTM ->
    Latlong.of_utm Latlong.WGS84 (Latlong.utm_add ref0.utm0 (x, y))
  | LTP ->
    let ned = make_ned [|y; x; -. (float_of_string alt -. !ground_alt)|] in
    let ecef = ecef_of_ned ref0.ecef0 ned in
    let (geo, _) = Latlong.geo_of_ecef Latlong.WGS84 ecef in
    geo

let print_waypoint_utm = fun out ref0 default_alt waypoint ->
  let name = sc (get_attrib waypoint get_waypoint_name 
                            "name of waypoint") in
  let (x, y) = 
      match (get_waypoint_x waypoint, get_waypoint_y waypoint) with
      | Some x, Some y ->
            (float_of_string (sc x), float_of_string (sc y))
      | _ -> 
          failwith 
            (sprintf "invalid waypoint %s, can't calculate x y" name)
  and alt =
    try 
      let height = match get_waypoint_height waypoint with
                   | Some h -> float_of_string (sc h)
                   | None -> failwith "option none" in
      sof (height +. !ground_alt)
    with _ -> default_alt
  in
  let alt = try 
              match get_waypoint_alt waypoint with
              | Some a -> (sc a)
              | None -> alt
            with _ -> alt in
  check_altitude (float_of_string alt) name;
  let (x, y) = match ref0.frame with
    | UTM -> (x, y)
    | LTP ->
      let wgs84 = wgs84_of_x_y ref0 x y alt in
      let utm = utm_of ~zone:ref0.utm0.utm_zone Latlong.WGS84 wgs84 in
      (utm.utm_x -. ref0.utm0.utm_x, utm.utm_y -. ref0.utm0.utm_y)
  in
  fprintf out " {%.1f, %.1f, %s},\\\n" x y alt

let print_waypoint_enu = fun out ref0 default_alt waypoint ->
  let (x, y) = 
      match (get_waypoint_x waypoint, get_waypoint_y waypoint) with
      | Some x, Some y ->
            (float_of_string (sc x), float_of_string (sc y))
      | _ -> 
          failwith "invalid waypoint, can't calculate x y"
  and alt =
    try 
      let height = match get_waypoint_height waypoint with
                   | Some h -> float_of_string (sc h)
                   | None -> failwith "option none" in
      sof (height +. !ground_alt)
    with _ -> default_alt
  in
  let alt = try 
              match get_waypoint_alt waypoint with
              | Some a -> (sc a)
              | None -> alt
            with _ -> alt in
  let ned = match ref0.frame with
    | UTM ->
      let ecef =
        Latlong.ecef_of_geo Latlong.WGS84
          (Latlong.of_utm Latlong.WGS84 (Latlong.utm_add ref0.utm0 (x, y)))
          (float_of_string alt)
      in
      Latlong.array_of_ned (Latlong.ned_of_ecef ref0.ecef0 ecef)
    | LTP ->
      [|y; x; -. (float_of_string alt -. !ground_alt)|]
  in
  fprintf out
    " {%.2f, %.2f, %.2f}, /* ENU in meters  */ \\\n"
    ned.(1) ned.(0) (-.ned.(2))

let convert_angle = fun rad -> Int64.of_float (1e7 *. (Rad>>Deg)rad)

let print_waypoint_lla = fun out ref0 default_alt waypoint ->
  let (x, y) = 
      match (get_waypoint_x waypoint, get_waypoint_y waypoint) with
      | Some x, Some y ->
            (float_of_string (sc x), float_of_string (sc y))
      | _ -> 
          failwith "invalid waypoint, can't calculate x y"
  and alt =
    try 
      let height = match get_waypoint_height waypoint with
                   | Some h -> float_of_string (sc h)
                   | None -> failwith "option none" in
      sof (height +. !ground_alt)
    with _ -> default_alt
  in
  let alt = try 
              match get_waypoint_alt waypoint with
              | Some a -> (sc a)
              | None -> alt
            with _ -> alt in
  let wgs84 = wgs84_of_x_y ref0 x y alt in
    fprintf out
      " {.lat=%Ld, .lon=%Ld, .alt=%.0f}, /* 1e7deg, 1e7deg, mm (above NAV_MSL0, local msl=%.2fm) */ \\\n"
      (convert_angle wgs84.posn_lat) (convert_angle wgs84.posn_long)
      (1000. *. float_of_string alt) (Egm96.of_wgs84 wgs84)

let print_waypoint_lla_wgs84 = fun out ref0 default_alt waypoint ->
  let name = sc (get_attrib waypoint
                            get_waypoint_name
                            "waypoint name") in
  let (x, y) = 
      match (get_waypoint_x waypoint, get_waypoint_y waypoint) with
      | Some x, Some y ->
            (float_of_string (sc x), float_of_string (sc y))
      | _ -> 
          failwith "invalid waypoint, can't calculate x y"
  and alt =
    try 
      let height = match get_waypoint_height waypoint with
                   | Some h -> float_of_string (sc h)
                   | None -> failwith "option none" in
      sof (height +. !ground_alt)
    with _ -> default_alt
  in
  let alt = try 
              match get_waypoint_alt waypoint with
              | Some a -> (sc a)
              | None -> alt
            with _ -> alt in
  let wgs84 = wgs84_of_x_y ref0 x y alt in
  if Srtm.available wgs84 then
    check_altitude_srtm (float_of_string alt) name wgs84;
  let alt = float_of_string alt +. Egm96.of_wgs84 wgs84 in
  fprintf out 
    " {.lat=%Ld, .lon=%Ld, .alt=%.0f}, /* 1e7deg, 1e7deg, mm (above WGS84 ref ellipsoid) */ \\\n"
    (convert_angle wgs84.posn_lat) (convert_angle wgs84.posn_long)
    (1000. *. alt)

let print_waypoint_global = fun out waypoint ->
  match (get_waypoint_lat waypoint, get_waypoint_lon waypoint) with
  | Some _, Some _ -> fprintf out " TRUE, \\\n"
  | _ -> fprintf out " FALSE, \\\n"

let get_index_block = fun x ->
  try
    List.assoc (sc x) !index_of_blocks
  with
    Not_found -> failwith (sprintf "Unknown block: '%s'"  (sc x))

let element = fun a b c -> Xml.Element (a, b, c)
let init_block = mk_block (Some (cs "INIT_BLOCK"))
                          None None
                          None None
                          None None
                          None None
                          None
                          []
                          []
                          []

let index_blocks = fun b_list ->
  let block = ref (-1) in
  List.iter
    (fun b ->
       incr block;
       let name = get_attrib b get_block_name "name of block" in
       if List.mem_assoc (sc name) !index_of_blocks then
         failwith
          (Printf.sprintf
            "Error in flight plan: Block '%s' defined twice"
            (sc name));
       index_of_blocks := ((sc name), !block) :: !index_of_blocks;)
    b_list

let c_suffix =
  let r = Str.regexp "^[a-zA-Z0-9_]*$" in
  fun s -> Str.string_match r s 0

let define_waypoints_indices = fun out wpts ->
  let i = ref 0 in
  List.iter (fun w ->
      let n = sc (get_attrib w
                             get_waypoint_name 
                             "waypoint name") in
      if c_suffix n then
        Xml2h.define_out out (sprintf "WP_%s" n) (soi !i);
      incr i)
    wpts

let dummy_waypoint =
  mk_waypoint (Some (cs "dummy"))
              (Some (cs "0"))
              (Some (cs "0"))
              None None
              None None

let home = fun waypoints ->
  let rec loop = function
      [] -> failwith "Waypoint 'HOME' required"
    | w::ws ->
      if sc (get_attrib w get_waypoint_name "waypoint name") = "HOME"
      then
        match (get_waypoint_x w, get_waypoint_y w) with
        | Some x, Some y ->
              (float_of_string (sc x), float_of_string (sc y))
        | _ -> 
          failwith "invalid 'HOME' waypoint, can't calculate x y"
      else
        loop ws in
  loop waypoints


let check_distance = fun (hx, hy) max_d wp ->
  let name = get_attrib wp get_waypoint_name "waypoint name" in
  let (x, y) = match (get_waypoint_x wp, get_waypoint_y wp) with
              | Some x, Some y ->
                    (float_of_string (sc x), float_of_string (sc y))
              | _ -> 
            failwith "invalid waypoint, can't calculate x y" in
  let d = sqrt ((x-.hx)**2. +. (y-.hy)**2.) in
  if d > max_d then
    fprintf stderr
      "\nWarning: Waypoint '%s' too far from HOME (%.0f>%.0f)\n\n"
      (sc name) d max_d


(* Check coherence between global ref and waypoints ref *)
(* Returns a patched xml with utm_x0 and utm_y0 set *)
let check_geo_ref = fun wgs84 fpparsed ->
  let utm0 = utm_of WGS84 wgs84 in

  let max_d = min 1000. 
                  (float_attrib get_fpp_max_dist_from_home
                                (get_fpp_info fpparsed)
                                "'max_dist_from_home' of flight plan") in
  let check_zone = fun u ->
    if (utm_of WGS84 (of_utm WGS84 u)).utm_zone <> utm0.utm_zone then
      prerr_endline "Warning: You are close (less than twice the max distance) to an UTM zone border ! The navigation will not work unless the GPS receiver configured to send the POSLLH message." in
  check_zone { utm0 with utm_x = utm0.utm_x +. 2.*.max_d };
  check_zone { utm0 with utm_x = utm0.utm_x -. 2.*.max_d };

  let wpts = get_attrib (get_fpp_elements fpparsed)
                        get_waypoints "waypoints"
    in
  let wpts = mk_waypoints (Some (cs (sof utm0.utm_x))) 
                          (Some (cs (sof utm0.utm_y)))
                          (get_waypoint_list wpts) in
  let elems = set_fpp_waypoints (Some wpts) 
                                (get_fpp_elements fpparsed) in
  mk_fpp (get_fpp_info fpparsed) elems

let parse_wpt_sector = fun indexes waypoints sector ->
  let sector_name = get_attrib sector get_sector_name "name" in
  let p2D_of = fun c ->
    let name = c in
    try
      let wp = List.find (fun wp -> (get_attrib wp
                                                get_waypoint_name 
                                                "waypoint name")
                                                    = name)
               waypoints in
      let i = soi (get_index_waypoint name indexes) in
      let err = "coordonate of waypoint " ^ (sc
                                              (get_attrib wp
                                                  get_waypoint_name
                                                  "waypoint name")) in
      let x = float_attrib get_waypoint_x wp err
      and y = float_attrib get_waypoint_x wp err in
      (i, {G2D.x2D = x; G2D.y2D = y })
    with
      Not_found ->
        failwith 
          (sprintf "Error: corner '%s' of sector '%s' not found"
            (sc name) (sc sector_name))
  in
  (sector_name, List.map p2D_of (get_sector_corners sector))


(**
 *
 * Preprocessing function
 *
 **)
(* FIXME: for later, get rid of those references? *)
let reinit = fun () ->
  index_of_blocks := [];
  check_expressions := false

let preprocess_fp = fun flight_plan ->

  reinit ();
  let fpparsed = flight_plan.flight_plan_parsed in
  fp_wgs84 := georef_of_fpp fpparsed;
  let fpparsed = check_geo_ref !fp_wgs84 fpparsed in

  let dir = Filename.dirname flight_plan.filename in
  let fpparsed = Proc_Include.process_includes dir fpparsed in
  let fpparsed = Convert_path.process_paths fpparsed in
  let fpparsed = Convert_waypoints.process_relative_waypoints fpparsed
    in

  (* Add a safety last HOME block *)
  let blocks = get_attrib_or_default get_blocks (get_fpp_elements fpparsed) [] in
  let blocks = init_block :: blocks in
  index_blocks blocks;
  let waypoints = get_attrib (get_fpp_elements fpparsed) 
                             get_waypoints 
                             "waypoints" in
  let waypoints_list = get_waypoint_list waypoints in

  let frame = match get_fpp_wp_frame (get_fpp_info fpparsed) with
              | Some wp_frame -> sc wp_frame
              | None          -> "UTM" in
  let frame = match String.uppercase_ascii frame with
    | "UTM" -> UTM
    | "LTP" -> LTP
    | _ ->
      failwith
        ("Error: unknown wp_frame \"" ^ frame ^ "\". Use \"utm\" or \"ltp\"")
  in
  begin
    (* DO NOT REMOVE *)
    try
      ground_alt := float_attrib get_fpp_ground_alt 
                                (get_fpp_info fpparsed)
                                "ground_alt"
    with
      Failure _ -> 
        failwith
          "Impossible to convert into float the ground_alt parameter"
  end;
  let ref0 = {
    frame;
    utm0 = utm_of WGS84 !fp_wgs84;
    ecef0 = Latlong.ecef_of_geo Latlong.WGS84 !fp_wgs84 !ground_alt;
    geo0 = !fp_wgs84
  } in

  let waypoints_list = match frame with
    | UTM ->
      let rel_utm_of_wgs84 = fun wgs84 ->
        (* force utm zone to be the same that reference point *)
        let utm = utm_of ~zone:ref0.utm0.utm_zone WGS84 wgs84 in
        (utm.utm_x -. ref0.utm0.utm_x, utm.utm_y -. ref0.utm0.utm_y) in
      List.map (localize_waypoint rel_utm_of_wgs84) waypoints_list
    | LTP -> waypoints_list
  in
  let waypoints_list = dummy_waypoint :: waypoints_list in

  (* index of waypoints *)
  let index_of_waypoints =
    let i = ref (-1) in
    List.map (fun w -> 
                incr i; 
                let name = (get_attrib w 
                                       get_waypoint_name
                                      "waypoint name") in
                (name ,!i)) 
              waypoints_list
  in

  let fpp_elem = get_fpp_elements fpparsed in
  let fpp_elem = set_fpp_blocks (Some blocks) fpp_elem in
  let fpp_elem = 
    set_fpp_waypoints (Some (mk_waypoints 
                                    (get_waypoints_utm_x0 waypoints)
                                    (get_waypoints_utm_y0 waypoints)
                                    waypoints_list))
                      fpp_elem in

  let fpparsed = mk_fpp (get_fpp_info fpparsed)
                fpp_elem in

  (fpparsed, ref0, index_of_waypoints)

(** * Management of global variables *)

(** Function to generate unique name for the for variables *)
let gen_for_vname =
  let x = ref 0 in
  fun p -> incr x; sprintf "%s_%d" p !x

(** Function to update children of an Xml.Element *)
let update_child = fun child xml ->
  match xml with
  | Xml.Element(t, attr, _) -> Xml.Element(t, attr, child)
  | x -> raise (Xml.Not_element x)

(** Operator to fix op < and <= that not exist in the paparazzi parser *)
let update_op = fun op ->
  match op with
  | "<" -> "@LT"
  | "<=" -> "@LEQ"
  | _ -> op

(** Function that analyse a string. If it is a valid C expression, *)
(** we look for all $IDENT and replace the IDENT that are equal to *)
(** [old_name] into [new_name]. In the other case it return the    *)
(** initial string.                                                *)
let update_var_name_expr = fun old_name new_name s ->
  let rec update_name = function
    | Ident i when i.[0] = '$' ->
      let name = String.sub i 1 (String.length i - 1) in
      if name = old_name then
        (sprintf "$%s" (new_name), true)
      else
        (sprintf "%s" i, false)
    | Ident i -> (sprintf "%s" i, false)
    | Int i -> (sprintf "%d" i, false)
    | Float i -> (sprintf "%f" i, false)
    | CallOperator (op, [e1;e2]) ->
      let (s1, r1) = update_name e1 in
      let (s2, r2) = update_name e2 in
        (sprintf "(%s%s%s)" s1 (update_op op) s2, r1 || r2)
    | CallOperator (op, [e1]) ->
      let (s1, r1) = update_name e1 in 
        (sprintf "%s(%s)" op s1, r1)
    | CallOperator (_,_) -> failwith "Operator should be binary or unary"
    | Call (i, es) ->
        let (ses, res) = List.split (List.map update_name es) in
        (sprintf "%s(%s)" i (String.concat "," ses),
          List.exists (fun x -> x) res)
    | Index (i,e) ->
      let (s, r) = update_name e in
      (sprintf "%s[%s]" i s, r)
    | Field (i,f) -> (sprintf "%s.%s" i f, false)
    | Deref (e,f) ->
      let (s, r) = update_name e in
      (sprintf "(%s)->%s" s f, r)
  in
  match s with
  | None -> None
  | Some s ->
      try
        let lexbuf = Lexing.from_string (sc s) in
        let e = Expr_parser.expression Expr_lexer.token lexbuf in
        let (s', r ) = update_name e in
        if r then (Some (cs s')) else (Some s)
      with
        _ -> Some s

let update_var_name_param = fun old_name new_name param ->
  let roll = update_var_name_expr old_name new_name
                                (get_stage_roll param)
  and pitch = update_var_name_expr old_name new_name
                                (get_stage_pitch param)
  and yaw = update_var_name_expr old_name new_name
                                (get_stage_yaw param)
  and climb = update_var_name_expr old_name new_name
                                (get_stage_climb param)
  and wp = update_var_name_expr old_name new_name
                                (get_stage_wp param)
  and wp2 = update_var_name_expr old_name new_name
                                (get_stage_wp2 param)
  and wpts = update_var_name_expr old_name new_name
                                (get_stage_wpts param)
  and wp_qdr= update_var_name_expr old_name new_name
                                (get_stage_wp_qdr param)
  and wp_dist= update_var_name_expr old_name new_name
                                (get_stage_wp_dist param)
  and from = update_var_name_expr old_name new_name
                                (get_stage_from param)
  and from_qdr= update_var_name_expr old_name new_name
                                (get_stage_from_qdr param)
  and from_dist= update_var_name_expr old_name new_name
                                (get_stage_from_dist param)
  and course = update_var_name_expr old_name new_name
                                (get_stage_course param)
  and radius = update_var_name_expr old_name new_name
                                (get_stage_radius param)
  and center = update_var_name_expr old_name new_name
                                (get_stage_center param)
  and turn_around = update_var_name_expr old_name new_name
                                (get_stage_turn_around param)
  and orientation = update_var_name_expr old_name new_name
                                (get_stage_orientation param)
  and grid = update_var_name_expr old_name new_name
                                (get_stage_grid param)
  and commands = update_var_name_expr old_name new_name
                                (get_stage_commands param)
  and flags = update_var_name_expr old_name new_name
                                (get_stage_flags param)
  and ac_id = update_var_name_expr old_name new_name
                                (get_stage_ac_id param)
  and distance = update_var_name_expr old_name new_name
                                (get_stage_distance param)
  and vmode = update_var_name_expr old_name new_name
                                (get_stage_vmode param)
  and hmode = update_var_name_expr old_name new_name
                                (get_stage_hmode param)
  and alt = update_var_name_expr old_name new_name
                                (get_stage_alt param)
  and height = update_var_name_expr old_name new_name
                                (get_stage_height param)
  and throttle = update_var_name_expr old_name new_name
                                (get_stage_throttle param)
  and pre_call = update_var_name_expr old_name new_name
                                (get_stage_pre_call param)
  and post_call = update_var_name_expr old_name new_name
                                (get_stage_post_call param)
  and nav_type = update_var_name_expr old_name new_name
                                (get_stage_nav_type param)
  and nav_params = update_var_name_expr old_name new_name
                                (get_stage_nav_params param)
  and until = update_var_name_expr old_name new_name
                                (get_stage_until param)
  and approaching_time = update_var_name_expr old_name new_name
                                (get_stage_approaching_time param)
  and exceeding_time = update_var_name_expr old_name new_name
                                (get_stage_exceeding_time param)
      in
  mk_stage_param roll pitch yaw climb wp wp2 wpts wp_qdr wp_dist from
                 from_qdr from_dist course radius center turn_around
                 orientation grid commands flags ac_id distance vmode hmode alt height throttle pre_call post_call nav_type
                 nav_params until approaching_time exceeding_time

(** Function to update all the variables in the XML *)
let rec update_var_name_stage = fun old_name new_name stage ->
  match stage with
  | While_stage (cond, list) ->
        let cond = update_var_name_expr old_name new_name cond
        and list = List.map (update_var_name_stage old_name new_name)
                            list in
      While_stage (cond, list)
  | For_stage (param, list) ->
        let var = update_var_name_expr old_name new_name 
                                   (get_for_var param)
        and from = update_var_name_expr old_name new_name 
                                    (get_for_from param)
        and t = update_var_name_expr old_name new_name 
                                 (get_for_to param) in
        let param = mk_for var from t
        and list = List.map (update_var_name_stage old_name new_name)
                            list in
      For_stage (param, list)
  | Return_stage (return) -> 
      let return =  
      match (update_var_name_expr old_name new_name (Some return)) with
      | None -> return
      | Some s -> s
      in Return_stage return
  | Deroute_stage (drt) -> 
      Deroute_stage (drt)
  | Set_stage (param) ->
      let var = update_var_name_expr old_name
                                      new_name
                                      (Some (get_set_var param))
      and value = update_var_name_expr old_name
                                      new_name
                                      (Some (get_set_value param)) in
      let var = match var with
                | Some v -> v
                | None -> get_set_var param
      and value = match value with
                | Some v -> v
                | None -> get_set_value param in
      Set_stage (mk_set var value)
                        
  | Call_stage (call_param) ->
        let call_fun = get_call_fun call_param
        and until = update_var_name_expr old_name new_name (get_call_until call_param)
        and loop = update_var_name_expr old_name new_name (get_call_loop call_param)
        and break = update_var_name_expr old_name new_name (get_call_break call_param)
        in
      Call_stage (mk_call call_fun until loop break)
  | Call_once_stage (call_param) ->
        let call_fun = get_call_once_fun call_param
        and break = update_var_name_expr old_name new_name (get_call_once_break call_param)
        in
        Call_once_stage (mk_call_once call_fun break)
  | Heading_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Heading_stage param
  | Attitude_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Attitude_stage param 
  | Manual_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Manual_stage param 
  | Go_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Go_stage param 
  | Xyz_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Xyz_stage param 
  | Circle_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Circle_stage param 
  | Eight_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Eight_stage param 
  | Stay_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Stay_stage param 
  | Follow_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Follow_stage param 
  | Survey_rectangle_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Survey_rectangle_stage param 
  | Oval_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Oval_stage param 
  | Path_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Path_stage param 
  | Guided_stage (param) ->
    let param = update_var_name_param old_name new_name param in
    Guided_stage param 
  | Home_stage -> Home_stage

(** Generates the list of global variables for the for stage *)
let gen_gvar_list = fun fpparsed ->
  let rec get_gvar_stage = fun stage ->
    match stage with
    | For_stage (param, list) ->
      let old_v = sc (get_attrib param get_for_var "var") in
      let v = gen_for_vname old_v in
      let var = Expr_syntax.c_var_of_ident v in
      let to_var = var ^ "_to" in
      let param = set_for_var (Some (cs var)) param in
      (**  let stage = ExtXml.subst_attrib "var_to" to_var stage in **)
      let (list, gvars) = List.split (List.map get_gvar_stage
                                          list) in
      let list = List.map (update_var_name_stage old_v v) list in
      (For_stage (param, list), (cs var) :: (cs to_var) :: (List.flatten gvars))
    | While_stage (cond, list) ->
        let (stages, gvars) = List.split (List.map get_gvar_stage
                                          list) in
        (While_stage (cond, stages), (List.flatten gvars))
    | _ -> (stage, [])
  and get_gvar_block = fun block ->
    let stages = get_block_stages block in
    let (stages, gvars) = List.split (List.map get_gvar_stage stages) in
    let gvars = List.flatten gvars in
    let block = set_block_stage stages block in
    (block, gvars)
  in
  let blocks = match get_blocks (get_fpp_elements fpparsed) with
               | Some l -> l
               | None   -> [] in
  let (blocks, gvars) = List.split (List.map get_gvar_block blocks) in
  let gvars = List.flatten gvars in
  let fpp_elem = set_fpp_blocks (Some blocks) (get_fpp_elements fpparsed)
  in
    (mk_fpp (get_fpp_info fpparsed) fpp_elem, gvars)

(* parsing flight plan parsed to flight plan*)


let from_parsed (flight_plan : fpp) name =
  let vars = get_attrib_or_default get_variables
                                   (get_fpp_elements flight_plan)
                                   [] in
  let test var attrib =
          match (attrib var) with
          | None -> false
          | Some _ -> true in
  let testvar var = (test var get_variable_min) &&
                    (test var get_variable_max) &&
                    (test var get_variable_step) in
  let sets = List.fold_left 
                (fun s e -> 
                    match e with
                    | Abi_binding _ -> s
                    | Var var       -> if testvar var
                                          then var :: s
                                          else s)
                [] vars in
  let mods = get_attrib_or_default get_modules 
                                   (get_fpp_elements flight_plan) 
                                   [] in
  { filename = name; 
    settings = sets; 
    modules = mods; 
    flight_plan_parsed = flight_plan }


let from_file = fun filename -> 
  match string2fp (cs (Proc_Include.read_file_to_string filename)) with
  | None    -> failwith "invalid flight_plan given"
  | Some (Err msg) -> failwith ("parsing error : unknown token : " ^ (sc msg))
  | Some (Proc _) -> failwith "paring error : expected flight plan, got procedure"
  | Some (Fpp fp1) -> from_parsed fp1 filename
