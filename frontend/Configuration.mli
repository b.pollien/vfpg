(** Configuration file needed to replace the configuration from CompCert *)
(** Remove the need of a compcert.ini file                               *)

val arch : string
val stdlib_path : string
val abi : string
val system : string
val model : string
val elf_target : bool
val has_standard_headers : bool
