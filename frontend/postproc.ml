(** Post-processing on the generated file *)
(** Post-processing:
    - remove the declaration pf 
    - add static inline 
    - remove $
    - remove unecesary () *)

open Format
open Printf

(** Remove the declaration of the nav function *)
let remove_decl line =
  match line with
  | "void on_enter_block(unsigned char);"
  | "void on_exit_block(unsigned char);"
  | "_Bool forbidden_deroute(unsigned char, unsigned char);"
  | "void auto_nav(void);" ->
    None
  | _ -> Some line

(** Add static inline to auto_nav function *)
let add_static_inline line =
  match line with
  | "void auto_nav(void)" -> Some ("static inline " ^ line)
  | _ -> Some line

(** Remove $ *)
let remove_dollars line =
  let r = Str.regexp "\\$" in
  Some (Str.replace_first r "" line)

(** Remove unecessary () for call *)
let remove_call_parenthesis line =
  let r = Str.regexp ")();" in
  Some (Str.replace_first r ");" line)

(** Remove register keyword *)
let remove_register line =
  let r = Str.regexp "register " in
  Some (Str.replace_first r "" line)

(** The list of all the post-processing to apply *)
let list_postproc =
  [remove_decl;
   add_static_inline;
   remove_dollars;
   remove_call_parenthesis;
   remove_register]

(** Post-process a line *)
let postproc_line line =
  (* Use option type in order to remove line if None is return *)
  let call_postproc s f =
    match s with
    | Some s -> f s
    | None -> None
  in
  List.fold_left call_postproc (Some line) list_postproc

(** Function to read a line of a file *)
let input_line_opt ic =
  try Some (input_line ic)
  with End_of_file -> None

(** Main function for the post-processing *)
let postproc file_out =
  (* Open the generated file *)
  let ic = open_in file_out in

  (* Open a temporary file to write the result of the post-processing *)
  let tmp_file = file_out ^ ".tmp" in
  let oc = open_out tmp_file in
  let rec aux () =
    match input_line_opt ic with
    | Some line ->
      begin
        match postproc_line line with
        | Some line ->
            output_string oc line; output_char oc '\n';
            aux ()
        | None -> aux ()
      end
    | None ->
        close_in ic;
        close_out oc;
        Sys.remove file_out;
        Sys.rename tmp_file file_out
  in
  aux ()