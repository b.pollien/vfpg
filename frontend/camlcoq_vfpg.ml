open BasicTypes
open FlightPlanGeneric
open FlightPlan
open FlightPlan.FP
open Preproc
open Integers
open Common_def
open Convert_fp_mode
open PrintClight
open Format
open Printf
open FlightPlanParsed

(** Conversion of the XML flight plan to Coq flight plan *)

let add_par s =
  match s with
  | Some s -> Some (cs ( "(" ^ (sc s) ^ ")"))
  | None   -> None

let convert_fb_deroute = fun id der ->
  let to_name = get_fbd_to der in
  let fb_to = ni (get_index_block to_name) in
  let cond = get_fbd_cond der in
  mk_fp_fbd id fb_to cond


let convert_fp_excp = fun e ->
  let cond = parse_coq_attr e get_exception_cond "cond" in
  let deroute = get_attrib e get_exception_deroute "deroute" in
  let id = ni (get_index_block deroute) in
  let exec = add_par (get_exception_exec e) in
  mk_fp_exception cond id exec

let convert_fp_excps = fun list_fpp_exc ->
  List.map (convert_fp_excp) list_fpp_exc

let convert_fp_global_excps = fun fpparsed ->
  let global_excps =
    get_attrib_or_default get_global_exceptions
                          (get_fpp_elements fpparsed)
                          [] in
  convert_fp_excps global_excps

let rec convert_fp_stages = fun index_of_waypoints stages ->
  List.concat (List.map (convert_fp_stage index_of_waypoints) stages)
and
  (** Convert a fpp stage into a list of FP Stage *)
  convert_fp_stage = fun index_of_waypoints stage ->
  begin
    match stage with
    | Return_stage (ret) -> if (sc ret) = "1"
                            then [RETURN true]
                            else [RETURN false]
    | Deroute_stage (der) ->
      let id = ni (get_index_block der) in
      [DEROUTE (mk_fp_pderoute der id)]
    | While_stage (cond, list) ->
      let cond = match cond with
                 | Some v -> v
                 | None   -> (cs "true") in
      let cond = add_parenthesis (cs (parse (sc cond)))
      and stages = convert_fp_stages index_of_waypoints list in
      [WHILE (cond, stages)]
    | Set_stage (stage) ->
      let name = get_attrib stage (fun x -> Some (get_set_var x)) "var"
      and value = parse_coq_attr stage 
                                 (fun x -> Some (get_set_value x))
                                 "value" in
      [SET (mk_fp_pset name value)]
    | Call_stage (stage) ->
      let func = add_parenthesis (get_attrib stage get_call_fun "fun")
      and until = parse_coq_attr_option stage get_call_until "until"
      and loop = get_attrib_bool stage get_call_loop "loop" true
      and break = get_attrib_bool stage get_call_break "break" false in
      [CALL (mk_fp_pcall func until loop break)]
    | Call_once_stage (stage) ->
      let func = get_attrib stage get_call_once_fun "fun"
      and break = get_attrib_bool stage get_call_once_break "break" false in
      [CALL (mk_fp_pcall func None false break)]
    | For_stage (stage, list) ->
      let var = sc (get_attrib stage get_for_var "var")
      and from = parse_coq_attr stage get_for_from "from"
      and to_expr = parse_coq_attr stage get_for_to "to" in
      let to_var = var ^ "_to"
      and fp_stages = 
        (convert_fp_stages index_of_waypoints list)
        @ [SET (mk_fp_pset (cs var) (cs (var ^ " + 1")))] in
      let cond = add_parenthesis (cs (sprintf "%s <= %s" var to_var)) in
        [SET (mk_fp_pset (cs var) from); 
         SET (mk_fp_pset (cs to_var) to_expr);
         WHILE (cond, fp_stages)]
    | Heading_stage (stage) -> 
          convert_fp_heading stage index_of_waypoints
    | Follow_stage (stage) -> 
          convert_fp_follow stage
    | Attitude_stage (stage) ->
          convert_fp_attitude stage index_of_waypoints
    | Manual_stage (stage) ->
          convert_fp_manual stage index_of_waypoints
    | Go_stage (stage) ->
          convert_fp_go stage index_of_waypoints
    | Stay_stage (stage) ->
          convert_fp_stay stage index_of_waypoints
    | Xyz_stage (stage) ->
          convert_fp_xyz stage index_of_waypoints
    | Home_stage -> convert_fp_home
    | Circle_stage (stage) ->
          convert_fp_circle stage index_of_waypoints
    | Eight_stage (stage) ->
          convert_fp_eight stage index_of_waypoints
    | Oval_stage (stage) ->
          convert_fp_oval stage index_of_waypoints
    | Survey_rectangle_stage (stage) ->
          convert_fp_survey_rectangle stage index_of_waypoints
    | Guided_stage (stage) -> 
          convert_fp_guided stage
    | Path_stage (stage) ->
          failwith "Internal Error, path are converted in go stages"
  end

(** Generate an fp_block from the block *)
let convert_fp_block = fun index_of_waypoints block ->
  let name = get_attrib block get_block_name "block name" in
  let index = ni (get_index_block name)
  and pre = add_par (get_block_pre_call block)
  and post = add_par (get_block_post_call block)
  and enter = add_par (get_block_on_enter block)
  and exit = add_par (get_block_on_exit block)
  and excpts = get_block_exceptions block
  and fb_deroutes = get_block_fbd block
  and stages = get_block_stages block in
  let fb_deroutes = List.map (convert_fb_deroute index) fb_deroutes in
  let stages = convert_fp_stages index_of_waypoints stages
  and excpts = convert_fp_excps excpts in
  let block =
    mk_fp_block name index stages excpts pre post enter exit
  in
    (block, fb_deroutes)

(** Generate an fp_blocks from the blocks *)
let convert_fp_blocks = fun index_of_waypoints blocks ->
  let blocks, fb_deroutes
    = List.split (List.map (convert_fp_block index_of_waypoints) blocks) in
  let fb_deroutes = List.flatten fb_deroutes in
  (blocks, fb_deroutes)

(** Function that convert an XML flight plan into a Coq flight plan *)
let fp_coq_of_fpp = fun fpparsed index_of_waypoints ->
  let excps = convert_fp_global_excps fpparsed in
  let blocks = get_attrib_or_default get_blocks 
                                     (get_fpp_elements fpparsed) 
                                     [] in
  let blocks, fb_deroutes = convert_fp_blocks index_of_waypoints
                                              blocks in
  mk_flight_plan fb_deroutes excps blocks

(** Function to iter of err_msg *)
let iter_err_msg = fun msg ->
  match msg with
  | WARNING msg -> printf "[WARNING] %s\n" (sc msg)
  | ERROR msg -> fprintf stderr "[ERROR] %s\n" (sc msg)

(** Function that convert the preprocess [fpparsed] flight plan using 
  the generated [index_of_waypoints] and write the resulting C code in
  the [out] file *)
let gen_flight_plan = fun fpparsed index_of_waypoints gvars out ->
  let fp_coq = fp_coq_of_fpp fpparsed index_of_waypoints in
  match Generator.generate_flight_plan fp_coq gvars with
  | CODE (code, errs) ->
      List.iter iter_err_msg errs;
      print_program Clight1 (formatter_of_out_channel out) code
  | ERROR errs -> List.iter iter_err_msg errs; exit 1
