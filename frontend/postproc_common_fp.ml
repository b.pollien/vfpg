(** Post-processing on the generated file *)
(** Post-processing:
    - seperate decalration into .h
    - set private variable
    - remove $
    - remove t'1 *)

open Format
open Printf

let replace_string_lib =
  ("unsigned char", "uint8_t") :: ("unsigned short", "uint16_t") :: []

let replace_string line =
  let rec aux list l =
    match list with
    | [] -> Some l
    | (s, e) :: t -> aux t (Str.global_replace (Str.regexp s) e l)
  in aux replace_string_lib line

(** Remove the declaration of function *)
let remove_decl line out_h =
  let r = Str.regexp "\\([a-zA-Z_0-9]+ \\)+[a-zA-Z_0-9]+([a-zA-Z_0-9, ]+);" in
  if Str.string_match r line 0
    then 
      begin
        match replace_string line with
        | Some l -> fprintf out_h "%s\n" l; 
        None
        | None -> ();
        None
      end
    else Some line

let private_vars =
  ["unsigned char private_nav_stage;"; 
   "unsigned char private_nav_block;";
   "unsigned char private_last_block;";
   "unsigned char private_last_stage;"]

(** Add static to private vars *)
let add_static line =
  if List.mem line private_vars
    then Some ("static " ^ line)
  else Some line 

(** Remove $ *)
let remove_dollars line =
  let r = Str.regexp "\\$" in
  Some (Str.replace_first r "" line)

let fbd_temp_lines =
  ("  register _Bool t'1;", None) :: ("[ ]*t'1 = [a-zA-Z_]+([a-zA-Z_ ]+, \\$b);", None) 
  :: ("[ ]*if (t'1) {", Some "  if(forbidden_deroute(private_nav_block, b)) {") :: []

let replace_fbd_temp line =
  let rec aux list =
    match list with
    | [] -> Some line
    | (s, r) :: t -> if Str.string_match (Str.regexp s) line 0 then
                      r
                    else aux t
  in aux fbd_temp_lines

  (** The list of all the post-processing to apply *)
  let list_postproc =
    [add_static;
     remove_dollars;
     replace_string;
     replace_fbd_temp]

(** Post-process a line *)
let postproc_line line out_h =
  (* Use option type in order to remove line if None is return *)
  let call_postproc s f =
    match s with
    | Some s -> f s
    | None -> None
  in
  match remove_decl line out_h with
  | Some line -> List.fold_left call_postproc (Some line) list_postproc
  | None -> None

(** Function to read a line of a file *)
let input_line_opt ic =
  try Some (input_line ic)
  with End_of_file -> None

(** Main function for the post-processing *)
let postproc file_out out_h =
  (* Open the generated file *)
  let ic = open_in file_out in

  (* Open a temporary file to write the result of the post-processing *)
  let tmp_file = file_out ^ ".tmp" in
  let oc = open_out tmp_file in
  let rec aux () =
    match input_line_opt ic with
    | Some line ->
      begin
        match postproc_line line out_h with
        | Some line ->
            output_string oc line; output_char oc '\n';
            aux ()
        | None -> aux ()
      end
    | None ->
        close_in ic;
        close_out oc;
        Sys.remove file_out;
        Sys.rename tmp_file file_out
  in
  aux ()