open Printf
open Latlong
open Common_def
open Preproc
open FlightPlanParsed

(* Header name *)
let h_name = "FLIGHT_PLAN_H"

(** Formatting output with a margin *)
let margin = ref 0
let step = 2

let right () = margin := !margin + step
let left () = margin := !margin - step

let lprintf = fun out f ->
  fprintf out "%s" (String.make !margin ' ');
  fprintf out f

let inside_function = fun name -> "Inside" ^ String.capitalize_ascii name

(** Print the function that test if a position is inside a polygon *)
let print_inside_polygon_global = fun out name ->
  lprintf out "uint8_t i, j;\n";
  lprintf out "bool c = false;\n";
  (* build array of wp id *)
  lprintf out "const uint8_t nb_pts = %s_NB;\n" name;
  lprintf out "const uint8_t wps_id[] = %s;\n\n" name;
  (* start algo *)
  lprintf out "for (i = 0, j = nb_pts - 1; i < nb_pts; j = i++) {\n";
  right ();
  lprintf out
    "if (((WaypointY(wps_id[i]) > _y) != (WaypointY(wps_id[j]) > _y)) &&\n";
  lprintf out
    "   (_x < (WaypointX(wps_id[j])-WaypointX(wps_id[i])) * (_y-WaypointY(wps_id[i])) / (WaypointY(wps_id[j])-WaypointY(wps_id[i])) + WaypointX(wps_id[i]))) {\n";
  right ();
  lprintf out "if (c == TRUE) { c = FALSE; } else { c = TRUE; }\n";
  left();
  lprintf out "}\n";
  left();
  lprintf out "}\n";
  lprintf out "return c;\n"

let print_inside_sector = fun out (s, pts) ->
  let (ids, _) = List.split pts in
  let name = "SECTOR_"^(String.uppercase_ascii (sc s)) in
  Xml2h.define_out out (name^"_NB") (soi (List.length pts));
  Xml2h.define_out out name ("{ "^(String.concat ", " ids)^" }");
  lprintf out
    "static inline bool %s(float _x, float _y) {\n" (inside_function (sc s));
  right ();
  print_inside_polygon_global out name;
  left ();
  lprintf out "}\n\n"

(** FP variables and ABI auto bindings *)
type fp_var = 
| FP_var of (string * string * string) 
| FP_binding of (string * string list option * string * string option)

(* get a Hashtbl of ABI messages (name, field_types) *)
let extract_abi_msg = fun filename class_ ->
  let xml = ExtXml.parse_file filename in
  try
    let xml_class = 
      ExtXml.child ~select:(fun x -> Xml.attrib x "name" = class_)
        xml "msg_class" in
    let t = Hashtbl.create (List.length (Xml.children xml_class)) in
    List.iter (fun x ->
        let name = ExtXml.attrib x "name"
        and field_types =
          List.map (fun field -> ExtXml.attrib field "type")
            (Xml.children x)
        in
        Hashtbl.add t name field_types
      ) (Xml.children xml_class);
    t
  with
    Not_found -> failwith (sprintf "No msg_class '%s' found" class_)

(**
  * Parse Variables fpp field
*)
let parse_variables = fun list ->
  let some_attrib_or_none = fun v f cb ->
    try Some (cb (f v)) with _ -> None
  in
  List.map (fun var ->
      match var with
      | Var variable ->
        let v = sc (get_attrib variable 
                               get_variable_var
                               "variable var")
        and t = match get_variable_type variable with
                | Some t -> sc t
                | None   -> "float"
        and i = match get_variable_init variable with
                | Some i -> sc i
                | None   -> "0" in
        FP_var (v, t, i)
      | Abi_binding abi ->
        let n = sc (get_attrib abi get_abi_binding_name "name")
        and vs = match get_abi_binding_vars abi with
                 | Some vars -> 
                      some_attrib_or_none vars sc
                        (fun x -> Str.split (Str.regexp "[ ]*,[ ]*") x)
                 | None -> None
        and i = match get_abi_binding_id abi with
                | Some i -> sc i
                | None   -> "ABI_BROADCAST"
        and h = match get_abi_binding_handler abi with
                | Some handle ->
                        some_attrib_or_none handle sc
                                                    (fun x -> x) 
                | None -> None in
        begin match vs, h with
          | Some _, Some _ | None, None ->
            failwith "Gen_flight_plan: either 'vars' or 'handler' should be defined, not both"
          | _, _ -> FP_binding (n, vs, i, h)
        end
    ) list

let print_var_decl out = function
  | FP_var (v, t, _) -> lprintf out "extern %s %s;\n" t v
  | _ -> () (* ABI variables are not public *)

let print_var_impl out abi_msgs = function
  | FP_var (v, t, i) -> lprintf out "%s %s = %s;\n" t v i
  | FP_binding (n, Some vs, _, None) ->
    lprintf out "static abi_event FP_%s_ev;\n" n;
    let field_types = Hashtbl.find abi_msgs n in
    List.iter2
      (fun abi_t user -> 
        if not (user = "_") 
        then lprintf out "static %s %s;\n" abi_t user)
      field_types vs
  | FP_binding (n, None, _, Some _) ->
    lprintf out "static abi_event FP_%s_ev;\n" n
  | _ -> ()

(** Print auto init bindings function *)
let print_auto_init_bindings = fun out abi_msgs variables ->
  let print_cb = function
    | FP_binding (n, Some vs, _, None) ->
      let field_types = Hashtbl.find abi_msgs n in
      lprintf out 
        "static void FP_%s_cb(uint8_t sender_id __attribute__((unused))" n;
      List.iteri (fun i v ->
          if v = "_" 
          then lprintf out ", %s _unused_%d __attribute__((unused))"
                  (List.nth field_types i) i
          else lprintf out ", %s _%s" (List.nth field_types i) v
        ) vs;
      lprintf out ") {\n";
      List.iter (fun v ->
          if not (v = "_") then lprintf out "  %s = _%s;\n" v v
        ) vs;
      lprintf out "}\n\n"
    | _ -> ()
  in
  let print_bindings = function
    | FP_binding (n, _, i, None) ->
      lprintf out "  AbiBindMsg%s(%s, &FP_%s_ev, FP_%s_cb);\n" n i n n
    | FP_binding (n, _, i, Some h) ->
      lprintf out "  AbiBindMsg%s(%s, &FP_%s_ev, %s);\n" n i n h
    | _ -> ()
  in
  List.iter print_cb variables;
  lprintf out "static inline void auto_nav_init(void) {\n";
  List.iter print_bindings variables;
  lprintf out "}\n\n"


(**
 * Print flight plan header
*)

exception NoValue
let trim_header s =
  match ((String.index_opt s '\n'), (String.rindex_opt s '\n')) with
  | None, None | Some _, None | None, Some _ -> s
  | Some n, Some m -> try String.sub s (n + 1) (m-n - 1) with _ -> ""

let flight_plan_header =
        fun fpparsed ref0 xml_file index_of_waypoints out ->
  let fpp_elem = get_fpp_elements fpparsed in
  let waypoints = get_waypoint_list 
                    (get_attrib fpp_elem get_waypoints "waypoints")
  and variables = get_attrib_or_default get_variables fpp_elem []
  and blocks = get_attrib_or_default get_blocks fpp_elem []
  in

  fprintf out
    "/* This file has been generated by the VFPG from %s */\n"
    xml_file;
  fprintf out "/* Version %s */\n" (PPRZEnv.get_paparazzi_version ());
  fprintf out "/* Please DO NOT EDIT */\n\n";

  fprintf out "#ifndef %s\n" h_name;
  Xml2h.define_out out h_name "";
  fprintf out "\n";

  (* include general headers *)
  fprintf out "#include \"std.h\"\n";
  fprintf out "#include \"generated/modules.h\"\n";
  fprintf out "#include \"modules/core/abi.h\"\n";
  fprintf out "#include \"autopilot.h\"\n\n";
  (* print variables and ABI bindings declaration *)

  let variables = parse_variables variables in
  let abi_msgs =
    extract_abi_msg (PPRZEnv.paparazzi_home ^ "/conf/abi.xml") "airborne"
  in
  List.iter (fun v -> print_var_decl out v) variables;
  fprintf out "\n";

  (* add custom header part *)
  let header = match get_header fpp_elem with
  | Some header -> trim_header (sc header)
  | None        -> "" in
  if header <> "" then fprintf out "%s\n\n" header;

  let fpp_info = get_fpp_info fpparsed in

  let name = sc (get_attrib fpp_info get_fpp_name "flight plan name")
  in
  Xml2h.define_string_out out "FLIGHT_PLAN_NAME" name;

  (* flight plan header *)
  let get_float = fun x -> match x fpp_info with
                           | Some v -> float_of_string (sc v)
                           | None -> raise NoValue in
  let qfu = try get_float get_fpp_qfu
            with NoValue -> 0.
  and mdfh = try get_float get_fpp_max_dist_from_home
             with NoValue -> 
                  failwith "max distance from home required"
  and alt = sc (get_attrib fpp_info
                           get_fpp_alt
                           "flight plan alt") in
  begin
    try
      security_height := get_float get_fpp_security_height;
    with NoValue -> 
      failwith "security height require in flight plan"
  end;

  (* check altitudes *)
  begin
    try
      if !security_height < 0. then
        begin
          fprintf stderr
            "\nError: Security height cannot be negative (%.0f)\n"
              !security_height;
          exit 1;
        end
    with _ -> ()
  end;
  let home_mode_height =
    try
      max (get_float get_fpp_home_mode_height) !security_height
    with _ -> !security_height in
  begin
    try
      check_altitude (float_of_string alt) name;
      check_altitude_srtm (float_of_string alt) name !fp_wgs84;

      (* print general defines *)
      Xml2h.define_out out "NAV_DEFAULT_ALT"
        (sprintf "%.0f /* nominal altitude of the flight plan */"
                                            (float_of_string alt))
    with
      Failure _ -> 
        failwith "Impossible to convert into float the alt FP parameter"
  end;
  Xml2h.define_out out "NAV_UTM_EAST0" (sprintf "%.0f" ref0.utm0.utm_x);
  Xml2h.define_out out "NAV_UTM_NORTH0" (sprintf "%.0f" ref0.utm0.utm_y);
  Xml2h.define_out out "NAV_UTM_ZONE0" (sprintf "%d" ref0.utm0.utm_zone);
  Xml2h.define_out out "NAV_LAT0"
    (sprintf "%Ld /* 1e7deg */" (convert_angle !fp_wgs84.posn_lat));
  Xml2h.define_out out "NAV_LON0"
    (sprintf "%Ld /* 1e7deg */" (convert_angle !fp_wgs84.posn_long));
  Xml2h.define_out out "NAV_ALT0"
    (sprintf "%.0f /* mm above msl */" (1000. *. !ground_alt));
  Xml2h.define_out out "NAV_MSL0"
    (sprintf "%.0f /* mm, EGM96 geoid-height (msl) over ellipsoid */"
      (1000. *. Egm96.of_wgs84 !fp_wgs84));

  Xml2h.define_out out "QFU" (sprintf "%.1f" qfu);

  let (hx, hy) = home waypoints in
  List.iter (check_distance (hx, hy) mdfh) waypoints;
  define_waypoints_indices out waypoints;
  begin
    try
      Xml2h.define_out out "WAYPOINTS_UTM" "{ \\";
      List.iter (print_waypoint_utm out ref0 alt) waypoints;
      lprintf out "};\n";
      Xml2h.define_out out "WAYPOINTS_ENU" "{ \\";
      List.iter (print_waypoint_enu out ref0 alt) waypoints;
      lprintf out "};\n";
      Xml2h.define_out out "WAYPOINTS_LLA" "{ \\";
      List.iter (print_waypoint_lla out ref0 alt) waypoints;
      lprintf out "};\n";
      Xml2h.define_out out "WAYPOINTS_LLA_WGS84" "{ \\";
      List.iter (print_waypoint_lla_wgs84 out ref0 alt) waypoints;
      lprintf out "};\n";
      Xml2h.define_out out "WAYPOINTS_GLOBAL" "{ \\";
      List.iter (print_waypoint_global out) waypoints;
      lprintf out "};\n";
    with
      Failure _ -> 
        failwith
          "Impossible to convert into float the alt parameter for WP"
  end;
  Xml2h.define_out out "NB_WAYPOINT"
    (soi (List.length waypoints));

  Xml2h.define_out out "FP_BLOCKS" "{ \\";
  List.iter 
    (fun b -> 
      let name = sc (get_attrib b get_block_name "block name") in
      fprintf out " \"%s\" , \\\n" name)
    blocks;
  lprintf out " \"HOME\" , \\\n}\n";
  Xml2h.define_out out "NB_BLOCKS" (soi (List.length blocks + 1));

  Xml2h.define_out out "GROUND_ALT" (sof !ground_alt);
  Xml2h.define_out out "GROUND_ALT_CM"
    (sprintf "%.0f" (100.*. !ground_alt));
  Xml2h.define_out out "SECURITY_HEIGHT" (sof !security_height);
  Xml2h.define_out out "SECURITY_ALT"
    (sof (!security_height +. !ground_alt));
  Xml2h.define_out out "HOME_MODE_HEIGHT" (sof home_mode_height);
  Xml2h.define_out out "MAX_DIST_FROM_HOME" (sof mdfh);

  (* geofencing warnings and errors *)
  begin
    try
      let geofence_max_alt = get_float get_fpp_geofence_max_alt in
      if geofence_max_alt < !ground_alt then
        begin
          fprintf stderr
            "\nError: Geofence max altitude below ground alt (%.0f < %.0f)\n"
            geofence_max_alt !ground_alt;
          exit 1;
        end
      else if geofence_max_alt < (!ground_alt +. !security_height) then
        begin
          fprintf stderr
            "\nError: Geofence max altitude below security height (%.0f < (%.0f+%.0f))\n"
            geofence_max_alt !ground_alt !security_height;
          exit 1;
        end
      else if geofence_max_alt < (!ground_alt +. home_mode_height) then
        begin
          fprintf stderr
            "\nError: Geofence max altitude below ground alt + home mode height (%.0f < (%.0f+%.0f))\n"
            geofence_max_alt !ground_alt home_mode_height;
          exit 1;
        end
      else if geofence_max_alt < (float_of_string alt) then
        fprintf stderr
          "\nWarning: Geofence max altitude below default waypoint alt (%.0f < %.0f)\n"
          geofence_max_alt (float_of_string alt);
      Xml2h.define_out out "GEOFENCE_MAX_ALTITUDE" (sof geofence_max_alt);
      fprintf stderr
        "\nWarning: Geofence max altitude set to %.0f\n" geofence_max_alt;
    with
      _ -> ()
  end;

  begin 
    try
      let geofence_max_height = get_float get_fpp_geofence_max_height in
      if geofence_max_height < !security_height then
        begin
          fprintf stderr
            "\nError: Geofence max height below security height (%.0f < %.0f)\n"
            geofence_max_height !security_height;
          exit 1;
        end
      else if geofence_max_height < home_mode_height then
        begin
          fprintf stderr
            "\nError: Geofence max height below home mode height (%.0f < %.0f)\n"
            geofence_max_height home_mode_height;
          exit 1;
        end
      else if (geofence_max_height +. !ground_alt) < (float_of_string alt)
      then
        fprintf stderr
          "\nWarning: Geofence max AGL below default waypoint AGL (%.0f < %.0f)\n"
          (geofence_max_height +. !ground_alt) (float_of_string alt);
      Xml2h.define_out out "GEOFENCE_MAX_HEIGHT" (sof geofence_max_height);
      fprintf stderr
        "\nWarning: Geofence max AGL set to %.0f\n" geofence_max_height;
    with
      _ -> ()
  end;

  lprintf out "\n";

  (* print sectors *)
  lprintf out "\n#ifndef FBW\n\n"; (* workaround to hide sector functions on FBW side *)
  let filter_sector = function
    | Sector s -> [s]
    | Kml _ -> []
  in
  let sectors = match (get_sectors fpp_elem) with
    | Some l -> List.flatten (List.map filter_sector l)
    | None   -> []
  in

    
  List.iter (
    fun x ->
      match get_sector_type x with
      | Some _ ->
          failwith
            "Error: attribute \"type\" on flight plan tag \"sector\" is deprecated and must be removed. All sectors are now dynamics.\n"
      | _ -> ()
   ) sectors;
  let sectors =
    List.map (parse_wpt_sector index_of_waypoints waypoints) sectors in
  List.iter (print_inside_sector out) sectors;

  (* geofencing sector *)
  begin
    try
      let geofence_sector = 
        match get_fpp_geofence_sector fpp_info with
        | Some data -> sc data
        | None      -> failwith "option None" in
      lprintf out
        "\n#define InGeofenceSector(_x, _y) %s(_x, _y)\n"
        (inside_function geofence_sector)
    with
      _ -> ()
  end;
  lprintf out "\n#endif\n"; (* workaround to hide sector functions on FBW side *)

  (* start "C" part *)
  lprintf out "\n#ifdef NAV_C\n\n";

  (* print variables and ABI initialization *)
  List.iter (fun v -> print_var_impl out abi_msgs v) variables;
  lprintf out "\n";
  print_auto_init_bindings out abi_msgs variables

(**
 * Print flight plan footer
*)
let flight_plan_footer = fun out ->
  lprintf out "#endif // NAV_C\n";
  Xml2h.finish_out out h_name
