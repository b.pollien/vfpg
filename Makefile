COQ_PROJ := _CoqProject
COQ_MAKEFILE := Makefile.coq
COQ_MAKE := +$(MAKE) -f $(COQ_MAKEFILE)

MENHIR := menhir
M_INTERPRET := --interpret --interpret-show-cst
M_COMPILE := --coq --coq-no-version-check

ifneq "$(COQBIN)" ""
        COQBIN := $(COQBIN)/
else
        COQBIN := $(dir $(shell which coqc))
endif
export COQBIN

include variables.mk

all html gallinahtml doc deps.dot deps.png: $(COQ_MAKEFILE) Makefile
	$(COQ_MAKE) $@

# # Menhir parser
interpret:
	cp ./src/parser/Parser.vy ./src/parser/Parser.mly
	$(MENHIR) $(M_INTERPRET) ./src/parser/Parser.mly

src/parser/Parser.v: ./src/parser/Parser.vy
	$(MENHIR) $(M_COMPILE) ./src/parser/Parser.vy

# # CompCert
compcert: $(COMPCERTDIR)/Makefile.config
	@echo "${bold}Building CompCert...${normal}"
	$(MAKE) $(COMPCERTFLAGS)
	@echo "${bold}OK.${normal}"

# # Extraction of CompCert
extraction: compcert
	@echo "${bold}Extracting CompCert Ocaml code...${normal}"
	mkdir -p $(EXTRACTED)
	cp -f $(COMPCERT_INCLUDES:%=$(COMPCERTDIR)/%/*.ml*)\
		$(EXTRACTED)
	cp frontend/Configuration.ml extracted/Configuration.ml 
	cp frontend/Configuration.mli extracted/Configuration.mli
	@echo "${bold}OK.${normal}"

%.vo: %.v
	@rm -f doc/$(*F).glob
	@echo "COQC $*.v"
	@$(COQC) -dump-glob docs_coq/$(*F).glob $*.v

CoqMakefile: Makefile _CoqProject
	coq_makefile INSTALLDEFAULTROOT = $(MKFILE_DIR) -f _CoqProject -o CoqMakefile

$(COQ_MAKEFILE): $(COQ_PROJ) $(GEN)/CommonFP.v extraction src/parser/Parser.v
	$(COQBIN)coq_makefile -f $< -o $@ -docroot docs/html

$(VFPG): $(COQ_MAKEFILE) all
	@echo  "${bold}Verified Flight Plan Generator...${normal}"
	ocamlbuild $(FLAGS) $(VFPG).$(TARGET)
	@echo "${bold}OK.${normal}"

CommonFP: $(COQ_MAKEFILE) all $(GEN)/CommonFP.v
	@echo  "${bold}Clight -> C of common_flight_plan...${normal}"
	ocamlbuild $(FLAGS) common_fp_clight.$(TARGET)
	mkdir -p out
	./common_fp_clight.$(TARGET) ./common-c-code/common_macro.h ./out/CommonC.c ./out/CommonC.h
	@echo "${bold}OK.${normal}"

build: $(VFPG)

tests: $(VFPG)
	./tests/regression-tests.sh $(VFPG).$(TARGET) tests/regression-tests
	./tests/behaviour-tests.sh $(VFPG).$(TARGET) tests/behaviour-tests
	./tests/new-features-tests.sh $(VFPG).$(TARGET) tests/new-features-tests
	./tests/build-tests.sh $(VFPG).$(TARGET) paparazzi/conf/flight_plans tests/build-tests/non-working-fp.txt
	./tests/error-msg-tests.sh $(VFPG).$(TARGET) tests/error-msg-tests
	./tests/c-light-generated.sh $(GEN) ./tools/patch_common_fp.sh

rtests: $(VFPG)
	./tests/regression-tests.sh $(VFPG).$(TARGET) tests/regression-tests

btests: $(VFPG)
	TRACE_B=1 ./tests/behaviour-tests.sh $(VFPG).$(TARGET) tests/behaviour-tests

nftests: $(VFPG)
	./tests/new-features-tests.sh $(VFPG).$(TARGET) tests/new-features-tests

bdtests: $(VFPG)
	./tests/build-tests.sh $(VFPG).$(TARGET) paparazzi/conf/flight_plans tests/build-tests/non-working-fp.txt

etests: $(VFPG)
	./tests/error-msg-tests.sh $(VFPG).$(TARGET) tests/error-msg-tests

ctests:
	./tests/c-light-generated.sh $(GEN) ./tools/patch_common_fp.sh

documentation: $(VFPG)
	mkdir -p docs/html
	coq2html -base VFP -short-names -d docs/html \
		src/common/*.glob src/common/*.v \
		src/generator/*.glob src/generator/*.v \
		src/semantics/*.glob src/semantics/*.v \
		src/syntax/*.glob src/syntax/*.v \
		src/verification/*.glob src/verification/*.v \
		src/*.v

clean::
	-$(COQ_MAKE) clean
	rm -f $(VFPG).$(TARGET)
	rm -rf extracted out
	rm -rf src/.*.aux
	rm -rf compcert.ini
	rm -rf docs/html
	rm -rf html
	rm -rf src/parser/Parser.v
	rm -f .CoqMakefile.d Makefile.coq Makefile.coq.conf .lia.cache \
	    CoqMakefile.conf depend.* generated/.CommonFP.aux
	ocamlbuild -clean

realclean: clean
	$(MAKE) $(COMPCERTFLAGS) $<

.PHONY: extraction compcert run html gallinahtml doc clean realclean
